function getDaysInOneMonth(year, month) {
  const d = new Date(year, parseInt(month, 10), 0);
  return d.getDate();
}

export {
  getDaysInOneMonth,
};
