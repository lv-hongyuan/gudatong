export const APP_NAME = 'RnBaseFramework';

export const APP_TYPE = 'ship';

export function getTugboatUseWayName(useWay) {
  switch (useWay) {
    case 10:
      return '靠泊';
    case 20:
      return '离泊';
    case 30:
      return '移泊';
    default:
      return '移泊';
  }
}

export function getTugboatApplyType(applyType) {
  switch (applyType) {
    case 1:
      return '大风影响';
    case 2:
      return '港口强制';
    case 3:
      return '潮水因素';
    case 4:
      return '码头条件';
    default:
      return '移泊';
  }
}

export const ShipOperation = {
  DAOGANG: 10, // 实际到港
  XIAMAO: 20, // 下锚时间
  QIMAO: 30, // 起锚时间
  YINSHUI1: 40, // 引水上船
  KAOBO: 50, // 开始靠泊
  KAOBOWC: 55, // 靠泊完成
  KAISHIZUIYE: 60, // 开始作业
  ZUOYEWANCHENG: 70, // 作业完成
  BANGZAJS: 75, // 绑扎结束
  KAISHIJIELAN: 80, // 开始离泊
  LIBO: 90, // 离泊完成
  YINSHUI2: 95, // 引水下船
  ZHENGHANG: 100, // 定速正航
  WANCHENG: 110, // 完成
};

export function getOperationName(state) {
  switch (state) {
    case ShipOperation.DAOGANG:
      return '到达锚地';
    case ShipOperation.XIAMAO:
      return '下锚时间';
    case ShipOperation.QIMAO:
      return '起锚时间';
    case ShipOperation.YINSHUI1:
      return '引水上船';
    case ShipOperation.KAOBO:
      return '第一缆上桩';
    case ShipOperation.KAOBOWC:
      return '靠泊完成';
    case ShipOperation.KAISHIZUIYE:
      return '开始作业';
    case ShipOperation.ZUOYEWANCHENG:
      return '作业完成';
    case ShipOperation.BANGZAJS:
      return '绑扎完成';
    case ShipOperation.KAISHIJIELAN:
      return '开始离泊';
    case ShipOperation.LIBO:
      return '解最后一缆';
    case ShipOperation.YINSHUI2:
      return '引水下船';
    case ShipOperation.ZHENGHANG:
      return '定速航行';
    case ShipOperation.WANCHENG:
      return '完成';
    default:
      return null;
  }
}

// 天气现象
export const weatherList = [
  '晴天', '少云', '多云', '阴天', '雾', '霾', '湿霾',
  '潮湿天', '毛毛雨', '小雨', '雨', '阵雨', '雷雨',
  '雷电', '雪', '冰雹', '飑', '台风', '恶劣天气'];

// 蒲氏风级
export const windScaleList = [
  {
    id: 0,
    value: '0级/无风',
  }, {
    id: 1,
    value: '1级/软风',
  }, {
    id: 2,
    value: '2级/轻风',
  }, {
    id: 3,
    value: '3级/微风',
  }, {
    id: 4,
    value: '4级/和风',
  }, {
    id: 5,
    value: '5级/清劲风',
  }, {
    id: 6,
    value: '6级/强风',
  }, {
    id: 7,
    value: '7级/疾风',
  }, {
    id: 8,
    value: '8级/大风',
  }, {
    id: 9,
    value: '9级/烈风',
  }, {
    id: 10,
    value: '10级/狂风',
  }, {
    id: 11,
    value: '11级/暴风',
  }, {
    id: 12,
    value: '12级/飓风',
  }];

// 风力
export const windPowerList = [
  {
    id: 0,
    // value: '0.<1KT',
    value : '无风/0.<1KT',
  }, {
    id: 1,
    // value: '1.1-3KTS',
    value : '软风/1.1-3KTS',
  }, {
    id: 2,
    // value: '2.4-6KTS',
    value : '轻风/2.4-6KTS',
  }, {
    id: 3,
    // value: '3.7-10KTS',
    value : '微风/3.7-10KTS',
  }, {
    id: 4,
    // value: '4.11-16KTS',
    value : '和风/4.11-16KTS',
  }, {
    id: 5,
    // value: '5.17-21KTS',
    value : '清劲风/5.17-21KTS',
  }, {
    id: 6,
    // value: '6.22-27KTS',
    value : '强风/6.22-27KTS',
  }, {
    id: 7,
    // value: '7.28-33KTS',
    value : '疾风/7.28-33KTS',
  }, {
    id: 8,
    // value: '8.34-40KTS',
    value : '大风/8.34-40KTS',
  }, {
    id: 9,
    // value: '9.41-47KTS',
    value : '烈风/9.41-47KTS',
  }, {
    id: 10,
    // value: '10.48-55KTS',
    value : '狂风/10.48-55KTS',
  }, {
    id: 11,
    // value: '11.56-63KTS',
    value : '暴风/11.56-63KTS',
  }, {
    id: 12,
    // value: '12.>=64KTS',
    value : '飓风/12.>=64KTS',
  }];

// 国际浪级
export const waveList = [
  {
    id: 0,
    value: '0级/无浪',
  }, {
    id: 1,
    value: '1级/微浪',
  }, {
    id: 2,
    value: '2级/小浪',
  }, {
    id: 3,
    value: '3级/轻浪',
  }, {
    id: 4,
    value: '4级/中浪',
  }, {
    id: 5,
    value: '5级/大浪',
  }, {
    id: 6,
    value: '6级/巨浪',
  }, {
    id: 7,
    value: '7级/狂浪',
  }, {
    id: 8,
    value: '8级/狂涛',
  }, {
    id: 9,
    value: '9级/怒涛',
  }];

// 国际涌级
export const surgeLevelList = [
  {
    id: 0,
    value: '0级/无涌',
  }, {
    id: 1,
    value: '1级/弱涌+短或中涌',
  }, {
    id: 2,
    value: '2级/弱涌+长涌',
  }, {
    id: 3,
    value: '3级/中涌+短涌',
  }, {
    id: 4,
    value: '4级/中涌+中涌',
  }, {
    id: 5,
    value: '5级/中涌+长涌',
  }, {
    id: 6,
    value: '6级/强涌+短涌',
  }, {
    id: 7,
    value: '7级/强涌+中涌',
  }, {
    id: 8,
    value: '8级/强涌+长涌',
  }, {
    id: 9,
    value: '9级/乱涌',
  }, {
    id: 10,
    value: '弱涌波长在100米以下',
  }, {
    id: 11,
    value: '中涌在100~200米',
  }, {
    id: 12,
    value: '强涌在200米以上',
  }];

  export const shipState = [
    {
      id: 0,
      value: '未到港',
    }, {
      id: 10,
      value: '到港',
    }, {
      id: 20,
      value: '下锚',
    }, {
      id: 30,
      value: '起锚',
    }, {
      id: 40,
      value: '引水上船',
    }, {
      id: 50,
      value: '第一缆上桩',
    }, {
      id: 55,
      value: '靠泊完成',
    }, {
      id: 60,
      value: '开始作业',
    }, {
      id: 70,
      value: '作业完成',
    }, {
      id: 75,
      value: '绑扎完成',
    }, {
      id: 80,
      value: '开始离泊',
    }, {
      id: 90,
      value: '解最后一缆',
    }];

// 船报类型
export const positionReportTypeList = [
  {
    id: '0900时船报',
    value: '0900时船报',
  }, {
    id: '1500时船报',
    value: '1500时船报',
  },
];

// 风向
export const windDirectionList = [
  'N', 'NNE', 'NE', 'ENE', 'E', 'ESE', 'SE', 'SSE',
  'S', 'SSW', 'SW', 'WSW', 'W', 'WNW', 'NW', 'NNW',
];

export const ReviewState = {
  UnderReviewed: 10,
  Approved: 20,
  Dismissed: 30,
};

export function reviewTitleFromState(state) {
  switch (state) {
    case ReviewState.UnderReviewed:
      return '审批中';
    case ReviewState.Approved:
      return '已批准';
    case ReviewState.Dismissed:
      return '已驳回';
    default:
      return '未审批';
  }
}

export const ReadState = {
  unRead: 0,
  read: 1,
  allState: 2,
};

export function reviewReadFromState(state) {
  switch (state) {
    case ReadState.unRead:
      return '未读';
    case ReadState.read:
      return '已读';
    case ReadState.allState:
      return '全部';
    default:
      return '未知状态';
  }
}

// 特种箱箱型
export const contTypeList = ['20', '40'];

export const APP_LOGOUT = 'APP_LOGOUT';

export const APP_NEED_UPDATE = 'APP_NEED_UPDATE';

export const LineType = {
  ZHI_XIAN: '支线',
};

export const ShipEventRange = {
  SHIP_SAILING: 11, // "航行中停航原因"
  SHIP_ANCHORING: 12, // "航行锚泊漂航原因"
  SHIP_BERTHING: 13, // "船上靠泊后"
  PORT_BERTHING: 21, // "未直靠原因"
  PORT_OPERATION: 22, // "影响作业原因"
  PORT_UN_BERTHING: 23, // "影响离泊原因"
};

export const ReviewTheState = {
  shipStaging: 10,
  shipSubmit: 20,
  shipSendToSite: 30,
  portApproved: 40,
  // portDismissed: 50,
  shippingDepartmentApproved: 60,
  shippingDepartmentDismissed: 70,
};

export function reviewStateFromState(state) {
  switch (state) {
    case ReviewTheState.shipStaging:
      return '未提交';
    case ReviewTheState.shipSubmit:
      return '已提交';
    case ReviewTheState.shipSendToSite:
      return '未审核';
    case ReviewTheState.portApproved:
      return '待审核';
    // case ReviewTheState.portDismissed:
    //   return '现场驳回';
    case ReviewTheState.shippingDepartmentApproved:
      return '已批准';
    case ReviewTheState.shippingDepartmentDismissed:
      return '已驳回';
    default:
      return '未知状态';
  }
}

export const WaterDiversionState = {
  ShipTo: 0,
  useWaterDiversion: 1,
};

export function WaterDiversionFromState(state) {
  switch (state) {
    case WaterDiversionState.ShipTo:
      return '船舶自行';
    case WaterDiversionState.useWaterDiversion:
      return '使用引水';
    default:
      return '';
  }
}
