// import React from 'react'
// import { ActivityIndicator, View, Text, StyleSheet } from 'react-native'
// import { Location, Initializer } from 'react-native-baidumap-sdk/lib/js/index'
// import Permissions from 'react-native-permissions'
// import { Platform } from 'react-native'
// import AlertView from '../../components/Alert'
// import Overlay from 'teaset/components/Overlay/Overlay'
//
// const styles = StyleSheet.create({
//   container: {
//     padding: 20,
//     borderRadius: 10,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center'
//   },
//   text: {
//     marginTop: 10,
//   }
// })
//
// class LocationRequest {
//
//   visible: boolean
//   single: boolean
//   resolve: Function
//   reject: Function
//
//   constructor (resolve, reject, single = true, timeout = 5000) {
//     this.resolve = resolve
//     this.reject = reject
//     this.single = single
//     this.visible = true
//
//     if (this.single && timeout) {
//       this.timeoutAction = setTimeout(() => {
//         this.errorCallback('timeout')
//       }, timeout)
//     }
//   }
//
//   clear () {
//     this.timeoutAction && clearTimeout(this.timeoutAction)
//     this.visible = false
//   }
//
//   successCallback (...agrs) {
//     this.visible && this.resolve && this.resolve(...agrs)
//     if (this.single) {
//       this.clear()
//     }
//   }
//
//   errorCallback (...agrs) {
//     this.visible && this.reject && this.reject(...agrs)
//     if (this.single) {
//       this.clear()
//     }
//   }
// }
//
// let popView
//
// const locationRequestList: LocationRequest[] = []
//
// function onLocation (location) {
//
//   if (locationRequestList.length > 0) {
//     const tempList = []
//     locationRequestList.forEach((request) => {
//       switch (location.locationType) {
//         case 61:
//         case 66:
//         case 68:
//         case 161:
//           request.successCallback(location)
//           break
//         case 62:
//           request.errorCallback('无法获取有效定位依据，定位失败，请检查运营商网络或者WiFi网络是否正常开启，尝试重新请求定位')
//           break
//         case 63:
//           request.errorCallback('网络异常，没有成功向服务器发起请求，请确认手机网络是否通畅，尝试重新请求定位')
//           break
//         default:
//           request.errorCallback('定位失败')
//       }
//       if (request.single) {
//         request.clear()
//       } else {
//         tempList.push(request)
//       }
//     })
//     locationRequestList.splice(0, locationRequestList.length)
//     locationRequestList.concat(tempList)
//   }
//
//   if (locationRequestList.length === 0) {
//     Location.stop()
//   }
// }
//
// let listener
// let initFlag = false
//
// // 初始化定位SDK
// async function initSDK () {
//   if (!initFlag) {
//     await Initializer.init('tsI6S2TzGRbdgVQ1ndV6LOA50vpfe64o')
//     initFlag = true
//   }
// }
//
// // 添加定位监听
// async function setupLocationListener () {
//   await Location.init()
//   if (!listener) {
//     Location.setOptions({gps: true})
//     listener = Location.addLocationListener(onLocation)
//   }
//   Location.start()
// }
//
// export async function getCurrentLocation (timeout = 115000) {
//   let loadingView = showLoading()
//   try {
//     // 判断定位权限
//     const response = await Permissions.request('location')
//
//     if (response === 'authorized') {
//       // 初始化定位SDK
//       await initSDK()
//
//       // 添加定位监听
//       await setupLocationListener()
//
//       // 添加定位请求
//       return new Promise((resolve, reject) => {
//         const locationRequest = new LocationRequest(
//           (location) => {
//             hideLoading(loadingView)
//             resolve(location)
//           },
//           (error) => {
//             hideLoading(loadingView)
//             reject(error)
//           }, true, timeout)
//         locationRequestList.push(locationRequest)
//       })
//     } else {
//       showPermissionWarning()
//       hideLoading(loadingView)
//       return Promise.reject(new Error('没有定位权限'))
//     }
//   } catch (error) {
//     hideLoading(loadingView)
//     return Promise.reject(error)
//   }
// }
//
// function showPermissionWarning () {
//   if (Platform.OS === 'ios') {
//     const canOpenSettings = Permissions.canOpenSettings()
//     AlertView.show({
//       message: '需要定位权限，请到设置中打开',
//       showCancel: canOpenSettings,
//       confirmAction: () => {
//         canOpenSettings && Permissions.openSettings()
//       }
//     })
//   } else {
//     AlertView.show({
//       message: '需要定位权限，请到设置中打开',
//     })
//   }
// }
//
// function showLoading () {
//   popView && popView.close()
//   const overlayView = (
//     <Overlay.PopView modal={true}
//                      ref={(ref) => {
//                        popView = ref
//                      }}
//                      style={{alignItems: 'center', justifyContent: 'center'}}
//     >
//       <View style={styles.container}>
//         <ActivityIndicator/>
//         <Text style={styles.text}>定位中</Text>
//       </View>
//     </Overlay.PopView>
//   )
//   return Overlay.show(overlayView)
// }
//
// function hideLoading (loadingView) {
//   Overlay.hide(loadingView)
// }
