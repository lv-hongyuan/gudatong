import RNFS from 'react-native-fs';
import request from '../utils/request';
import ApiFactory from './Api';

export default function uploadImage(
  image: { fileName: string, fileSize: string, path: string },
  businessType: string,
  businessId: string,
) {
  return RNFS.readFile(image.path, 'base64')
    .then(imageData => request(ApiFactory.uploadFile(
      businessType,
      businessId,
      {
        fileName: image.fileName,
        fileSize: image.fileSize,
        fileData: imageData, // Android or iOS
      },
    )));
}
