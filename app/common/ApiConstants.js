import { AsyncStorage } from 'react-native';
// import { truncate } from 'fs';

export const HostKey = 'HostKey';
// 正式地址
let host = 'shipapi.zhonggu56.com';
// 测试地址
// let host = '172.16.1.94:88';
// let host = '192.168.100.136:8080'
// let host = '121.46.233.83:8180'
const apiVersion = 'v1';

export const EnableServerChange = true;

const ApiConstants = {
  get api() {
    const site = `http://${host}/app/ship/`;
    return `${site}api/${apiVersion}/`;
  },
  get host() {
    return host;
  },
  set host(newHost) {
    host = newHost;
    AsyncStorage.setItem(HostKey, newHost);
  },
};
export default ApiConstants;
