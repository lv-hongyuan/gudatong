

import Realm from 'realm';

class OfflineTask extends Realm.Object {
}

OfflineTask.schema = {   //航线船报/抵港确报/离线船报/离线确报的本地数据存储  
  name: 'OfflineTask',
  properties: {
    type: 'string',     //存储的数据 key值  
    data: 'string?',    //储存的数据
    date: 'int',
  },
};

class OfflineWeatherEvent extends Realm.Object {
}

OfflineWeatherEvent.schema = {
  name: 'OfflineWeatherEvent',
  primaryKey: 'id',
  properties: {
    id: 'int',
    data: 'string?',
    date: 'int',
  },
};

class OfflineVoyageEvent extends Realm.Object {
}

OfflineVoyageEvent.schema = {   //突发事件/离线突发事件 的本地数据存储
  name: 'OfflineVoyageEvent',
  primaryKey: 'id',
  properties: {
    id: 'int',
    data: 'string?',
    date: 'int',
  },
};

export default new Realm({ schema: [OfflineTask, OfflineWeatherEvent, OfflineVoyageEvent,] });
