const StorageKeys = {
  API_TOKEN: '@APPLICATION/API_TOKEN', // 应用程序API token，用来访问API使用
  USER: '@APPLICATION/USER', // 保存用户信息
  IS_SAIL: '@IS_SAIL', // 是否在航行状态
  DYN: '@DYN', // 是否在航行状态
};

export default StorageKeys;
