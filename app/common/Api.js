/**
 * Created by xu-jzh on 2017/7/18.
 */
import ApiConstants from './ApiConstants';

export const header = {
  content: {},
};

const ApiFactory = {

  // 登陆
  login(account) {
    return fetch(`${ApiConstants.api}login`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(account),
    });
  },
  // 获取船舶动态
  logout() {
    return fetch(`${ApiConstants.api}logOut`, {
      method: 'GET',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
    });
  },
  // 获取船舶动态
  shipDynShow() {
    return fetch(`${ApiConstants.api}shipDynShow`, {
      method: 'GET',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
    });
  },
  // 发送验证码
  sendVerificationCode(phoneNum) {
    return fetch(`${ApiConstants.api}sendVerificationCode`, {
      method: 'POST',
      headers: {
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(phoneNum),
    });
  },
  // 上传文件
  uploadFile(businessType, businessId, data) {
    return fetch(`${ApiConstants.api}/uploadFile/${businessType}/${businessId}`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },
  // 用户信息验证
  shipUserAut(userProfile) {
    return fetch(`${ApiConstants.api}shipUserAut`, {
      method: 'PUT',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(userProfile),
    });
  },
  // 用户信息认证情况获取
  getAutDetail() {
    return fetch(`${ApiConstants.api}getAutDetail`, {
      method: 'GET',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
    });
  },
  // 更新船舶动态
  inputDyn(portState) {
    return fetch(`${ApiConstants.api}inputDyn`, {
      method: 'PUT',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(portState),
    });
  },

  //历史船报
  HistoryRecord(data){
    return fetch(`${ApiConstants.api}getSailDynList`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },

  //历史船报详情
  GetSailDynDetail(data){
    return fetch(`${ApiConstants.api}getSailDynDetail`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },

  // 船舶到港
  toPort(data) {
    return fetch(`${ApiConstants.api}toPort`, {
      method: 'PUT',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },

  // 创建航海日记
  createSailDyn(navigateRecord) {
    return fetch(`${ApiConstants.api}createSailDyn`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(navigateRecord),
    });
  },

  //! 状态回退
  returnShipState(data) {
    return fetch(`${ApiConstants.api}backShipState`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },
  //!批量标记已读
  upDataMultiple(data){
    return fetch(`${ApiConstants.api}readNotices`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
      body: data,
    });
  },
  // 获取航海日记
  getLastSailDyn(data) {
    return fetch(`${ApiConstants.api}getLastSailDyn`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },
  //获取抵港确报信息
  getArrivePort(data){
    return fetch(`${ApiConstants.api}getLastArrivalConfirmOperation`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },

  //提交确报信息
  submitArrivalConfirmOperation(data){
    return fetch(`${ApiConstants.api}submitArrivalConfirmOperation`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },

  //提交所有离线船报+确报
  createOffLineData(data){
    return fetch(`${ApiConstants.api}createOffLineData`,{
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    })
  },

  //抵港确报获取列表
  getArrivalConfirmOperationList(data){
    return fetch(`${ApiConstants.api}getArrivalConfirmOperationList`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },

  //获取未读消息数量
  getNotReadNoticeNum(){
    return fetch(`${ApiConstants.api}getNotReadNoticeNum`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
      // body: JSON.stringify(data),
    });
  },

  //抵港确报详情
  getArrivalConfirmOperationDetail(data){
    return fetch(`${ApiConstants.api}getArrivalConfirmOperationDetail`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },

  // 下港预报
  nextPortEta({
    nextPopId, wharf, berth, time,
  }) {
    return fetch(`${ApiConstants.api}nextPortEta`, {
      method: 'PUT',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        nextPopId, wharf, berth, time,
      }),
    });
  },
  // 获取下港预报
  nextPortEtaShow(popId) {
    return fetch(`${ApiConstants.api}nextPortEtaShow/${popId}`, {
      method: 'GET',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
      // body: JSON.stringify({}),
    });
  },
  // 上传预配图
  upLoadPlanCargo(data) {
    return fetch(`${ApiConstants.api}upLoadPlanCargo`, {
      method: 'PUT',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },

  // 获取预配图
  upLoadPlanShow(popId) {
    return fetch(`${ApiConstants.api}upLoadPlanShow/${popId}`, {
      method: 'GET',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
    });
  },

  // 获取实载图
  realLoadShow(popId) {
    return fetch(`${ApiConstants.api}realLoadShow/${popId}`, {
      method: 'GET',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
    });
  },

  // 实载图上传
  realLoadDiagram(data) {
    return fetch(`${ApiConstants.api}realLoadDiagram`, {
      method: 'PUT',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },

  // 获取绑扎情况
  bindShow(popId) {
    return fetch(`${ApiConstants.api}bindShow/${popId}`, {
      method: 'GET',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
    });
  },

  // 绑扎情况
  bindSituation(data) {
    return fetch(`${ApiConstants.api}bindSituation`, {
      method: 'PUT',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },

  // 引水记录
  createApplyDiv(data) {
    return fetch(`${ApiConstants.api}createApplyDiv`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },

  // 引水记录列表
  findApplyDivs(popId) {
    return fetch(`${ApiConstants.api}findApplyDivs/${popId}`, {
      method: 'GET',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
    });
  },

  // 引水记录编辑
  updateApplyDiv(data) {
    return fetch(`${ApiConstants.api}updateApplyDiv`, {
      method: 'PUT',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },
  // 引水记录删除
  deleteApplyDiv(id) {
    return fetch(`${ApiConstants.api}deleteApplyDiv/${id}`, {
      method: 'DELETE',
      headers: {
        ...header.content,
        Accept: 'application/json',
      },
    });
  },

  // 拖轮申请
  createApplyTug(data) {
    return fetch(`${ApiConstants.api}createApplyTug`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },

  // 拖轮申请列表
  findApplyTugs(popId) {
    return fetch(`${ApiConstants.api}findApplyTugs/${popId}`, {
      method: 'GET',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
    });
  },

  // 拖轮申请编辑
  updateApplyTug(data) {
    return fetch(`${ApiConstants.api}updateApplyTug`, {
      method: 'PUT',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },
  // 删除拖轮申请
  deleteApplyTug(id) {
    return fetch(`${ApiConstants.api}deleteApplyTug/${id}`, {
      method: 'DELETE',
      headers: {
        ...header.content,
        Accept: 'application/json',
      },
    });
  },

  // 新建劳务费申请
  createApplyLaborFee(data) {
    return fetch(`${ApiConstants.api}createApplyLaborFee`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },

  // 获取劳务费
  findApplyLaborFee(popId) {
    return fetch(`${ApiConstants.api}findApplyLaborFee/${popId}`, {
      method: 'GET',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
    });
  },

  // 劳务费编辑
  updateApplyLaborFee(data) {
    return fetch(`${ApiConstants.api}updateApplyLaborFee`, {
      method: 'PUT',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },
  // 删除劳务费
  deleteApplyLaborFee(id) {
    return fetch(`${ApiConstants.api}deleteApplyLaborFee/${id}`, {
      method: 'DELETE',
      headers: {
        ...header.content,
        Accept: 'application/json',
      },
    });
  },

  // 创建拖轮使用记录
  createTugUse(data) {
    return fetch(`${ApiConstants.api}createTugUse`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },

  // 获取拖轮使用记录
  findTugUses(popId) {
    return fetch(`${ApiConstants.api}findTugUses/${popId}`, {
      method: 'GET',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
    });
  },

  // 编辑拖轮使用记录
  updateTugUse(data) {
    return fetch(`${ApiConstants.api}updateTugUse`, {
      method: 'PUT',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },
  // 删除拖轮使用记录
  deleteTugUses(id) {
    return fetch(`${ApiConstants.api}deleteTugUses/${id}`, {
      method: 'DELETE',
      headers: {
        ...header.content,
        Accept: 'application/json',
      },
    });
  },

  // 创建移泊记录
  createShiftRecord(data) {
    return fetch(`${ApiConstants.api}createShiftRecord`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },

  // 获取移泊记录列表
  findShiftRecords(popId) {
    return fetch(`${ApiConstants.api}findShiftRecords/${popId}`, {
      method: 'GET',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
    });
  },

  // 更新移泊记录
  updateShiftRecord(data) {
    return fetch(`${ApiConstants.api}updateShiftRecord`, {
      method: 'PUT',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },
  // 删除移泊记录
  deleteShiftRecord(id) {
    return fetch(`${ApiConstants.api}deleteShiftRecord/${id}`, {
      method: 'DELETE',
      headers: {
        ...header.content,
        Accept: 'application/json',
      },
    });
  },

  // 创建特种箱记录
  createSpecialCont(data) {
    return fetch(`${ApiConstants.api}createSpecialCont`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },

  // 获取特种箱记录
  findSpecialCont(popId) {
    return fetch(`${ApiConstants.api}findSpecialConts/${popId}`, {
      method: 'GET',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
    });
  },

  // 编辑特种箱记录
  updateSpecialCont(data) {
    return fetch(`${ApiConstants.api}updateSpecialCont`, {
      method: 'PUT',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },
  // 查找特种箱记录
  findOneSpecialCont(id) {
    return fetch(`${ApiConstants.api}findOneSpecialCont/${id}`, {
      method: 'GET',
      headers: {
        ...header.content,
        Accept: 'application/json',
      },
    });
  },

  // 删除特种箱记录
  deleteSpecialCont(id) {
    return fetch(`${ApiConstants.api}deleteSpecialCont/${id}`, {
      method: 'DELETE',
      headers: {
        ...header.content,
        Accept: 'application/json',
      },
    });
  },

  // 创建倒箱记录
  createInvertedBoxRecord(data) {
    return fetch(`${ApiConstants.api}createInvertedBoxRecord`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },

  // 获取倒箱记录
  findInvertedBoxRecords(popId) {
    return fetch(`${ApiConstants.api}findInvertedBoxRecords/${popId}`, {
      method: 'GET',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
    });
  },

  // 编辑倒箱记录
  updateInvertedBoxRecord(data) {
    return fetch(`${ApiConstants.api}updateInvertedBoxRecord`, {
      method: 'PUT',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },
  // 查找倒箱记录
  findOneInvertedBoxRecord(id) {
    return fetch(`${ApiConstants.api}findOneInvertedBoxRecord/${id}`, {
      method: 'GET',
      headers: {
        ...header.content,
        Accept: 'application/json',
      },
    });
  },

  // 删除倒箱记录
  deleteInvertedBoxRecord(id) {
    return fetch(`${ApiConstants.api}deleteInvertedBoxRecord/${id}`, {
      method: 'DELETE',
      headers: {
        ...header.content,
        Accept: 'application/json',
      },
    });
  },

  // 检查更新
  checkUpdate({ appKey, type, versionCode }) {
    return fetch(`${ApiConstants.api}getMaxOne`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ appKey, type, versionCode }),
    });
  },

  // 上传app版本信息
  uploadAppInfo({
    appKey, appVersion, sysVersion, phoneModel, userId,
  }) {
    return fetch(`${ApiConstants.api}saveUserApp`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        appKey, appVersion, sysVersion, phoneModel, userId,
      }),
    });
  },

  // 新建修船申请
  createShipRepairApplay(data) {
    return fetch(`${ApiConstants.api}createShipRepairApplay`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },

  // 编辑修船申请
  updateShipRepairApplay(data) {
    return fetch(`${ApiConstants.api}updateShipRepairApplay`, {
      method: 'PUT',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },

  // 获得指定修船申请
  getShipRepairApplay(id) {
    return fetch(`${ApiConstants.api}getShipRepairApplay/${id}`, {
      method: 'GET',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
  },

  // 删除修船申请
  deleteShipRepairApplay(id) {
    return fetch(`${ApiConstants.api}deleteShipRepairApplay/${id}`, {
      method: 'DELETE',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
  },

  // 获得修船申请列表
  getShipRepairApplayList() {
    return fetch(`${ApiConstants.api}getShipRepairApplayList`, {
      method: 'GET',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
  },

  // 新建加油申请
  createAddOilPlan(data) {
    return fetch(`${ApiConstants.api}createAddOilPlan`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },

  // 编辑加油申请
  updateAddOilPlan(data) {
    return fetch(`${ApiConstants.api}updateAddOilPlan`, {
      method: 'PUT',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },

  // 获得指定加油申请
  getAddOilPlan(id) {
    return fetch(`${ApiConstants.api}getAddOilPlan/${id}`, {
      method: 'GET',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
  },

  // 获取节拖奖励列表
  getShipTugRewardList(data) {
    return fetch(`${ApiConstants.api}getShipTugRewardList`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },

  // 新建节拖奖励
  checkShipTugRewardRepeat(data) {
    return fetch(`${ApiConstants.api}checkShipTugRewardRepeat`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },

    //节拖奖励详细页面
    getShipTugRewardById(data) {
        return fetch(`${ApiConstants.api}/getShipTugRewardById`, {
            method: 'POST',
            headers: {
                ...header.content,
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        });
    },
  //节拖奖励暂存
  getSaveShipTugReward(data) {
    return fetch(`${ApiConstants.api}/saveShipTugReward`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },

  //节拖奖励提交
  getSubmitShipTugReward(data) {
    return fetch(`${ApiConstants.api}/submitShipTugReward`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },

  //节拖奖励802点击确定发送暂存确认
  getConfirmSaveShipTugReward(data) {
    return fetch(`${ApiConstants.api}/confirmSaveShipTugReward`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },

  // 获取节拖奖励question列表
  getShipRewardTarget() {
    return fetch(`${ApiConstants.api}getShipRewardTarget`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      // body: JSON.stringify(data),
    });
  },

  // 删除加油申请
  deleteAddOilPlan(id) {
    return fetch(`${ApiConstants.api}deleteAddOilPlan/${id}`, {
      method: 'DELETE',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
  },

  // 获得加油申请列表
  getAddOilPlanList() {
    return fetch(`${ApiConstants.api}getAddOilPlanList`, {
      method: 'GET',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
  },

  // 航次指令列表
  getNoticeList(data) {
    return fetch(`${ApiConstants.api}getNoticeList`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },

  // 公告栏列表
  getNoticeListAll(data) {
    return fetch(`${ApiConstants.api}getNoticeListAll`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },

  // 公告置为已读
  getPublishNoticeRead(data) {
    return fetch(`${ApiConstants.api}getPublishNoticeRead`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },

  // 已读一条航次指令
  readOne(id) {
    return fetch(`${ApiConstants.api}readOne/${id}`, {
      method: 'PUT',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
  },

  // 航次指令全部置为已读
  readAll() {
    return fetch(`${ApiConstants.api}readAll`, {
      method: 'PUT',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
  },

  // 靠泊计划
  getBerthPlan(popId) {
    return fetch(`${ApiConstants.api}BerthPlan/${popId}`, {
      method: 'GET',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
  },

  // 新建冷藏箱记录
  createRefrigerator(data) {
    return fetch(`${ApiConstants.api}createRefrigerator`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },

  // 编辑冷藏箱记录
  updateRefrigerator(data) {
    return fetch(`${ApiConstants.api}updateRefrigerator`, {
      method: 'PUT',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },

  // 获得指定冷藏箱记录
  getRefrigerator(id) {
    return fetch(`${ApiConstants.api}getRefrigerator/${id}`, {
      method: 'GET',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
  },

  // 删除指定冷藏箱记录
  deleteRefrigerator(id) {
    return fetch(`${ApiConstants.api}deleteRefrigerator/${id}`, {
      method: 'DELETE',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
  },

  // 获取冷藏箱记录记录列表
  getRefrigeratorByPop(popId) {
    return fetch(`${ApiConstants.api}findRefrigeratorByPop/${popId}`, {
      method: 'GET',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
  },

  // 新建危险品记录
  createDangerousGoods(data) {
    return fetch(`${ApiConstants.api}createDangerousGoods`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },

  // 编辑危险品记录
  updateDangerousGoods(data) {
    return fetch(`${ApiConstants.api}updateDangerousGoods`, {
      method: 'PUT',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },

  // 获得指定危险品记录
  getDangerousGoods(id) {
    return fetch(`${ApiConstants.api}getDangerousGoods/${id}`, {
      method: 'GET',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
  },

  // 删除指定危险品记录
  deleteDangerousGoods(id) {
    return fetch(`${ApiConstants.api}deleteDangerousGoods/${id}`, {
      method: 'DELETE',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
  },

  // 获取危险品记录列表
  getDangerousGoodsByPop(popId) {
    return fetch(`${ApiConstants.api}findDangerousGoodsByPop/${popId}`, {
      method: 'GET',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
  },

  // 修改阶段时间
  updateTime(data) {
    return fetch(`${ApiConstants.api}updateTime`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },

  // 获取船舶证书信息
  getShipCertificate() {
    return fetch(`${ApiConstants.api}getShipCertificate`, {
      method: 'GET',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
  },

  // 获取船舶证书信息
  saveShipCertificate(data) {
    return fetch(`${ApiConstants.api}saveShipCertificate`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },

  // 获取抵港报信息
  getReachReport(popId) {
    return fetch(`${ApiConstants.api}getReachReport/${popId}`, {
      method: 'GET',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
  },

  // 获取靠泊报信息
  getBerthReport(popId) {
    return fetch(`${ApiConstants.api}getBerthReport/${popId}`, {
      method: 'GET',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
  },

  // 获取离泊报信息
  getLeaveReport(popId) {
    return fetch(`${ApiConstants.api}getLeaveReport/${popId}`, {
      method: 'GET',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
  },

  // 抵港报、靠泊报、离泊报暂存
  saveReport(data) {
    return fetch(`${ApiConstants.api}saveReport`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },

  // 下港列表
  getNextPortSelect(popId) {
    return fetch(`${ApiConstants.api}getNextPortSelect/${popId}`, {
      method: 'GET',
      headers: {
        ...header.content,
        Accept: 'application/json',
      },
    });
  },

  // 创建意见反馈
  createFeedBack(data) {
    return fetch(`${ApiConstants.api}createFeedBack`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },

  // 预计装载计划
  planLoad(popId) {
    return fetch(`${ApiConstants.api}planLoad/${popId}`, {
      method: 'GET',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
  },

  // 获取帮助列表
  getHandbookList(data) {
    return fetch(`${ApiConstants.api}getHandbookList`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },

  // 获取帮助详情
  getOneHandbook(id) {
    return fetch(`${ApiConstants.api}getOneHandbook/${id}`, {
      method: 'GET',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
  },

  // 获取特殊箱列表
  getSpecialBoxList(popId) {
    return fetch(`${ApiConstants.api}getSpecialBoxList/${popId}`, {
      method: 'GET',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
  },

  // 删除特殊箱
  deleteSpecialBox(id) {
    return fetch(`${ApiConstants.api}deleteSpecialBox/${id}`, {
      method: 'DELETE',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
  },

  // 新建特殊箱
  createSpecialBox(data) {
    return fetch(`${ApiConstants.api}createSpecialBox`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },

  // 修改特殊箱
  updateSpecialBox(data) {
    return fetch(`${ApiConstants.api}updateSpecialBox`, {
      method: 'PUT',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },

  // 获取一条特殊箱
  getOneSpecialBox(id) {
    return fetch(`${ApiConstants.api}getOneSpecialBox/${id}`, {
      method: 'GET',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
  },

  // 获取突发事件记录
  findShipEvents(popId, range) {
    return fetch(`${ApiConstants.api}findShipEvents/${popId}/${range}`, {
      method: 'GET',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
    });
  },

  // 获取事件类型
  findShipEventTypes(range) {
    return fetch(`${ApiConstants.api}findShipEventTypes/${range}`, {
      method: 'GET',
      headers: {
        ...header.content,
        Accept: 'application/json',
      },
    });
  },

  // 创建突发事件
  createShipEvent(data) {
    return fetch(`${ApiConstants.api}creatShipEvent`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },

  // 编辑突发事件
  updateShipEvent(data) {
    return fetch(`${ApiConstants.api}updateShipEvent`, {
      method: 'PUT',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  },

  // 删除突发事件
  deleteShipEvent(id) {
    return fetch(`${ApiConstants.api}deleteShipEvent/${id}`, {
      method: 'DELETE',
      headers: {
        ...header.content,
        Accept: 'application/json',
      },
    });
  },
};

export default ApiFactory;
