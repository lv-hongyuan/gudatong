const ShipInfoEventTypes = [
  {
    parentCode: 0,
    text: '船舶原因',
    code: 1,
  },
  {
    parentCode: 1,
    text: '船舶构造',
    code: 11,
  },
  {
    parentCode: 1,
    text: '设备故障',
    code: 12,
  },
  {
    parentCode: 1,
    text: '海事安检',
    code: 13,
  },
  {
    parentCode: 1,
    text: '船员不配合',
    code: 14,
  },
  {
    parentCode: 1,
    text: '船东欠薪',
    code: 15,
  },
  {
    parentCode: 1,
    text: '船员迟回船',
    code: 16,
  },
  {
    parentCode: 1,
    text: '船报不准',
    code: 17,
  },
  {
    parentCode: 1,
    text: '绑扎及解绑',
    code: 18,
  },
  {
    parentCode: 1,
    text: '船舶补给',
    code: 19,
  },
  {
    parentCode: 0,
    text: '码头原因',
    code: 2,
  },
  {
    parentCode: 2,
    text: '外贸优先',
    code: 21,
  },
  {
    parentCode: 2,
    text: '码头设备故障',
    code: 22,
  },
  {
    parentCode: 2,
    text: '码头临时关闭',
    code: 23,
  },
  {
    parentCode: 2,
    text: '码头工班不足',
    code: 24,
  },
  {
    parentCode: 2,
    text: '码头拖车不足',
    code: 25,
  },
  {
    parentCode: 2,
    text: '码头作业效率慢',
    code: 26,
  },
  {
    parentCode: 2,
    text: '港口活动',
    code: 27,
  },
  {
    parentCode: 0,
    text: '自然原因',
    code: 3,
  },
  {
    parentCode: 3,
    text: '大雾',
    code: 31,
  },
  {
    parentCode: 3,
    text: '大风',
    code: 32,
  },
  {
    parentCode: 3,
    text: '候潮',
    code: 33,
  },
  {
    parentCode: 3,
    text: '台风',
    code: 34,
  },
  {
    parentCode: 3,
    text: '冰情',
    code: 35,
  },
  {
    parentCode: 3,
    text: '港口水深受限',
    code: 36,
  },
  {
    parentCode: 3,
    text: '临时交通管制',
    code: 37,
  },
  {
    parentCode: 0,
    text: '公司原因',
    code: 4,
  },
  {
    parentCode: 4,
    text: '无货可装',
    code: 41,
  },
  {
    parentCode: 4,
    text: '货物未进场',
    code: 42,
  },
  {
    parentCode: 4,
    text: '办事处申报未衔接',
    code: 43,
  },
  {
    parentCode: 4,
    text: '不同航线集中到港',
    code: 44,
  },
  {
    parentCode: 4,
    text: '同航线集中到港',
    code: 45,
  },
  {
    parentCode: 4,
    text: '与其他公司集中到港',
    code: 46,
  },
  {
    parentCode: 0,
    text: '上下线类型',
    code: 5,
  },
  {
    parentCode: 5,
    text: '计划上线',
    code: 51,
  },
  {
    parentCode: 5,
    text: '计划下线',
    code: 52,
  },
  {
    parentCode: 5,
    text: '临时上线',
    code: 53,
  },
  {
    parentCode: 5,
    text: '临时下线',
    code: 54,
  },
  {
    parentCode: 0,
    text: '锚泊漂航',
    code: 6,
  },
  {
    parentCode: 6,
    text: '等航道',
    code: 61,
  },
  {
    parentCode: 6,
    text: '等引水',
    code: 62,
  },
  {
    parentCode: 6,
    text: '等潮水',
    code: 63,
  },
  {
    parentCode: 6,
    text: '等泊',
    code: 64,
  },
  {
    parentCode: 6,
    text: '等货',
    code: 65,
  },
  {
    parentCode: 6,
    text: '天气因素',
    code: 66,
  },
  {
    parentCode: 6,
    text: '船舶故障',
    code: 67,
  },
];

export default ShipInfoEventTypes;
