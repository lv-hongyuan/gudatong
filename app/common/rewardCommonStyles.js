import { Dimensions } from 'react-native';
import myTheme from '../Themes';

const { width } = Dimensions.get('window');
const isSmall = width < 375;

let commonStyles;
commonStyles = {
  inputGroup: isSmall ? {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: 'transparent',
    // paddingLeft: 10,
    // paddingRight: 10,
  } : {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: 'transparent',
    height: 35,
    marginTop: 10,
  },
  inputItem: isSmall ? {
    // width: '100%',
    // height: 30,
    flex: 1,
    marginBottom: 5,
    marginTop: 5,
    borderBottomWidth: 0,
    paddingLeft: 0,
    // marginLeft: 10,
    // marginRight: 10,
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
    textAlign: 'center',
    textAlignVertical: 'center'
  } : {
    flex: 1,
    // height: 30,
    borderBottomWidth: 0,
    paddingLeft: 0,
    // marginLeft: 10,
    // marginRight: 10,
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
    textAlign: 'center',
    textAlignVertical: 'center'
  },
  fixItem: {
    marginLeft: 10,
    marginRight: 10,
    flex: isSmall ? 0 : 1,
  },
  inputLabel: {
    fontSize: 14,
    color: '#535353',
  },
  readOnlyInputLabel: {
    fontSize: 14,
    color: '#969696',
  },
  input: {
    flex: 1,
    fontSize: 14,
    height: '100%',
    color: '#969696',
    paddingVertical: 0,
    paddingLeft: 0,
    paddingRight: 0,
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
  },
  text: {
    flex: 1,
    fontSize: 14,
    color: '#969696',
    // paddingVertical: 0,
    paddingLeft: 5,
    paddingRight: 5,
  },
  tail: {
    flex: 0,
    fontSize: 14,
    color: '#969696',
    paddingVertical: 0,
    paddingLeft: 0,
    paddingRight: 0,
  },
  contentLabel: {
    fontSize: 14,
    color: '#535353',
    paddingLeft: 5,
  },
  swipeRow: {
    paddingTop: 0,
    paddingBottom: 0,
    paddingLeft: 0,
    paddingRight: 0,
    borderBottomWidth: 0,
  },
};

export default commonStyles;
