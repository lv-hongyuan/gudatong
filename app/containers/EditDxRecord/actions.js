import {
  CREATE_DX_RECORD,
  UPDATE_DX_RECORD,
  GET_NEXT_PORT_LIST,
} from './constants';
import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';

export const createDxRecordRoutine = createRoutine(
  CREATE_DX_RECORD,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const createDxRecordPromise = promisifyRoutine(createDxRecordRoutine);

export const updateDxRecordRoutine = createRoutine(
  UPDATE_DX_RECORD,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const updateDxRecordPromise = promisifyRoutine(updateDxRecordRoutine);

export const getNextPortListRoutine = createRoutine(
  GET_NEXT_PORT_LIST,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const getNextPortListPromise = promisifyRoutine(getNextPortListRoutine);
