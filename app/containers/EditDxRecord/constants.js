/*
 *
 * EditDxRecord constants
 *
 */
export const CREATE_DX_RECORD = 'app/EditDxRecord/CREATE_DX_RECORD';

export const UPDATE_DX_RECORD = 'app/EditDxRecord/UPDATE_DX_RECORD';

export const GET_NEXT_PORT_LIST = 'app/EditDxRecord/GET_NEXT_PORT_LIST';
