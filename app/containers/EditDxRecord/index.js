import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Container, Content, InputGroup } from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import HeaderButtons from 'react-navigation-header-buttons';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { StyleSheet } from 'react-native';
import _ from 'lodash';
import {
  createDxRecordPromise,
  updateDxRecordPromise,
  getNextPortListPromise,
} from './actions';
import {
  makeSelectPopId,
  makeData,
  makeOnoBack,
  makeIsEdit,
  makePortList,
  makeSelectPortName,
} from './selectors';
import { errorMessage } from '../ErrorHandler/actions';
import commonStyles from '../../common/commonStyles';
import InputItem from '../../components/InputItem';
import screenHOC from '../screenHOC';
import stringToNumber from '../../utils/stringToNumber';
import { MaskType } from '../../components/InputItem/TextInput';

const styles = StyleSheet.create({
  input: {
    flex: 1,
    fontSize: 14,
    height: 50,
    color: '#535353',
    paddingVertical: 0,
    paddingLeft: 5,
    paddingRight: 5,
  },
});

/**
 * 倒箱记录
 * Created by jianzhexu on 2018/3/23
 */
@screenHOC
class EditDxRecord extends React.PureComponent {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.getParam('title', '创建倒箱记录'),
    headerRight: (
      <HeaderButtons>
        <HeaderButtons.Item
          title="保存"
          buttonStyle={{ fontSize: 14, color: '#ffffff' }}
          onPress={() => {
            const handleSubmitFunc = navigation.getParam('submit', undefined);
            if (handleSubmitFunc) {
              requestAnimationFrame(() => {
                handleSubmitFunc();
              });
            }
          }}
        />
      </HeaderButtons>
    ),
  });
  static propTypes = {
    navigation: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    updateDxRecord: PropTypes.func.isRequired,
    createDxRecord: PropTypes.func.isRequired,
    onGoBack: PropTypes.func.isRequired,
    portName: PropTypes.string.isRequired,
    data: PropTypes.object.isRequired,
    isEdit: PropTypes.bool.isRequired,
  };

  constructor(props) {
    super(props);
    const data = props.data || {};
    this.state = {
      id: data.id,
      popId: data.popId,
      xl: data.contNumber,
      reason: data.reason,
    };
    this.props.navigation.setParams({ submit: this.handleSubmit });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.data && nextProps.data !== this.props.data) {
      const data = nextProps.data || {};
      this.setState({
        id: data.id,
        popId: data.popId,
        xl: data.contNumber,
        reason: data.reason,
      });
    }
  }

  handleSubmit = () => {
    const param = {
      id: this.state.id,
      popId: this.state.popId,
      xl: stringToNumber(this.state.xl),
      reason: this.state.reason,
    };
    if (!param.xl) {
      this.props.dispatch(errorMessage('请填写倒箱数量'));
      return;
    }
    if (_.isEmpty(param.reason)) {
      this.props.dispatch(errorMessage('请填写倒箱原因'));
      return;
    }
    if (this.props.isEdit) {
      this.props.updateDxRecord(param).then(() => {
        if (this.props.onGoBack) this.props.onGoBack();
        this.props.navigation.goBack();
      }).catch(() => {});
    } else {
      this.props.createDxRecord(param).then(() => {
        if (this.props.onGoBack) this.props.onGoBack();
        this.props.navigation.goBack();
      }).catch(() => {});
    }
  };

  render() {
    return (
      <Container style={{
        backgroundColor: '#FFFFFF',
      }}
      >
        <Content style={{
          padding: 10,
        }}
        >
          <InputGroup style={commonStyles.inputGroup}>
            <InputItem
              style={styles.input}
              label="当前港口:"
              value={this.props.portName}
              editable={false}
            />
          </InputGroup>
          <InputGroup style={commonStyles.inputGroup}>
            <InputItem
              label="倒箱数量:"
              value={this.state.xl}
              maskType={MaskType.INTEGER}
              onChangeText={(text) => {
                this.setState({ xl: text });
              }}
              keyboardType="numeric"
            />
          </InputGroup>
          <InputGroup style={commonStyles.inputGroup}>
            <InputItem
              label="倒箱原因:"
              value={this.state.reason}
              onChangeText={(text) => {
                this.setState({ reason: text });
              }}
            />
          </InputGroup>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  popId: makeSelectPopId(),
  portName: makeSelectPortName(),
  isEdit: makeIsEdit(),
  data: makeData(),
  portList: makePortList(),
  onGoBack: makeOnoBack(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      createDxRecord: createDxRecordPromise,
      updateDxRecord: updateDxRecordPromise,
      getNextPortListPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EditDxRecord);
