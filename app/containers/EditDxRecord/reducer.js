import {
  createDxRecordRoutine,
  updateDxRecordRoutine,
  getNextPortListRoutine,
} from './actions';

const initState = {
  parentData: [],
  childData: [],
  portList: [],
};

export default function (state = initState, action) {
  switch (action.type) {
    // 创建倒箱记录
    case createDxRecordRoutine.TRIGGER:
      return { ...state, loading: true };
    case createDxRecordRoutine.SUCCESS:
      return state;
    case createDxRecordRoutine.FAILURE:
      return { ...state, error: action.payload };
    case createDxRecordRoutine.FULFILL:
      return { ...state, loading: false };

    // 编辑倒箱记录
    case updateDxRecordRoutine.TRIGGER:
      return { ...state, loading: true };
    case updateDxRecordRoutine.SUCCESS:
      return state;
    case updateDxRecordRoutine.FAILURE:
      return { ...state, error: action.payload };
    case updateDxRecordRoutine.FULFILL:
      return { ...state, loading: false };

    // 获取下港列表
    case getNextPortListRoutine.TRIGGER:
      return { ...state, loading: true };
    case getNextPortListRoutine.SUCCESS:
      return { ...state, portList: action.payload };
    case getNextPortListRoutine.FAILURE:
      return { ...state, error: action.payload };
    case getNextPortListRoutine.FULFILL:
      return { ...state, loading: false };

    default:
      return state;
  }
}
