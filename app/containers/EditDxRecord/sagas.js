import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import {
  createDxRecordRoutine,
  updateDxRecordRoutine,
  getNextPortListRoutine,
} from './actions';
import { errorMessage } from '../ErrorHandler/actions';
import { getPopId } from '../ShipWork/sagas';

function* getNextPortList(action) {
  console.log(action);
  try {
    yield put(getNextPortListRoutine.request());
    const popId = yield call(getPopId);
    const response = yield call(
      request,
      ApiFactory.getNextPortSelect(popId),
    );
    console.log('getNextPortList', response.toString());
    yield put(getNextPortListRoutine.success(response.dtoList));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(getNextPortListRoutine.failure(e));
  } finally {
    yield put(getNextPortListRoutine.fulfill());
  }
}

export function* getNextPortListSaga() {
  yield takeLatest(getNextPortListRoutine.TRIGGER, getNextPortList);
}

function* createDxRecord(action) {
  console.log(action);
  try {
    yield put(createDxRecordRoutine.request());
    const popId = yield call(getPopId);
    const {
      xl,
      reason,
    } = action.payload;
    const response = yield call(
      request,
      ApiFactory.createInvertedBoxRecord({
        popId,
        xl,
        reason,
      }),
    );
    console.log('createDxRecord', response.toString());
    yield put(createDxRecordRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(createDxRecordRoutine.failure(e));
  } finally {
    yield put(createDxRecordRoutine.fulfill());
  }
}

export function* createDxRecordSaga() {
  yield takeLatest(createDxRecordRoutine.TRIGGER, createDxRecord);
}

function* updateDxRecord(action) {
  console.log(action);
  try {
    yield put(updateDxRecordRoutine.request());
    const popId = yield call(getPopId);
    const {
      id,
      xl,
      reason,
    } = action.payload;
    const response = yield call(
      request,
      ApiFactory.updateInvertedBoxRecord({
        id,
        popId,
        xl,
        reason,
      }),
    );
    console.log('updateDxRecord', response.toString());
    yield put(updateDxRecordRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(updateDxRecordRoutine.failure(e));
  } finally {
    yield put(updateDxRecordRoutine.fulfill());
  }
}

export function* updateDxRecordSaga() {
  yield takeLatest(updateDxRecordRoutine.TRIGGER, updateDxRecord);
}

// All sagas to be loaded
export default [
  getNextPortListSaga,
  createDxRecordSaga,
  updateDxRecordSaga,
];
