import { createSelector } from 'reselect/es';

const selectShipStateDomain = () => state => state.shipWork.data;
const selectEditDxRecordDomain = () => state => state.editDxRecord;

const makeSelectPopId = () => createSelector(
  selectShipStateDomain(),
  (subState) => {
    console.debug(subState.popId);
    return subState.popId;
  },
);

const makeIsEdit = () => createSelector(
  (state, props) => props.navigation.state.params,
  (subState) => {
    console.debug(subState.isEdit);
    return subState.isEdit;
  },
);

const makeData = () => createSelector(
  (state, props) => props.navigation.state.params,
  (subState) => {
    console.debug(subState.data);
    return subState.data;
  },
);

const makePortList = () => createSelector(
  selectEditDxRecordDomain(),
  (subState) => {
    console.debug(subState.portList);
    return subState.portList;
  },
);

const makeOnoBack = () => createSelector(
  (state, props) => props.navigation.state.params,
  (subState) => {
    console.debug(subState.onGoBack);
    return subState.onGoBack;
  },
);

const makeSelectPortName = () => createSelector(
  selectShipStateDomain(),
  subState => subState.portName,
);

const makeSelectShipName = () => createSelector(
  selectShipStateDomain(),
  subState => subState.shipName,
);

export {
  makeSelectPopId,
  makeIsEdit,
  makeData,
  makePortList,
  makeOnoBack,
  makeSelectPortName,
  makeSelectShipName,
};
