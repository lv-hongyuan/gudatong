/*
 *
 * CertificateManagement constants
 *
 */
export const GET_CERTIFICATE_MANAGEMENT = 'app/CertificateManagement/GET_CERTIFICATE_MANAGEMENT';
export const UPDATE_CERTIFICATE_MANAGEMENT = 'app/CertificateManagement/UPDATE_CERTIFICATE_MANAGEMENT';

export const CertificateType = {
  Nationality: '国籍证书',
  LoadLine: '载重线证书',
  Tonnage: '吨位证书',
  Airworthiness: '适航证书',
  OilProof: '防油污证书',
  DangerousGoodsFitness: '危险品适装证书',
  MinimumSafeManning: '最低安全配员证书',
  CargoShipConstructionSafety: '货船构造安全证书',
  CargoShipEquipmentSafety: '货船设备安全证书',
};
