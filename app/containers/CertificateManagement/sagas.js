import { call, put, takeLatest, all } from 'redux-saga/effects';
import AppStorage from '../../utils/AppStorage';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import { getCertificateManagementRoutine, updateCertificateManagementRoutine } from './actions';
import uploadImage from '../../common/uploadImage';

function* getCertificateManagement(action) {
  console.log(action);
  try {
    yield put(getCertificateManagementRoutine.request());
    const response = yield call(request, ApiFactory.getShipCertificate({}));
    console.log('getShipCertificate', response);
    yield put(getCertificateManagementRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(getCertificateManagementRoutine.failure(e));
  } finally {
    yield put(getCertificateManagementRoutine.fulfill());
  }
}

export function* getCertificateManagementSaga() {
  yield takeLatest(getCertificateManagementRoutine.TRIGGER, getCertificateManagement);
}

function* updateCertificate(item) {
  const {
    finalInspectionDay,
    certificatePic,
    issuingDay,
    effectiveDay,
    type,
  } = item;
  const param = {
    finalInspectionDay,
    certificatePic,
    issuingDay,
    effectiveDay,
    type,
    shipId: AppStorage.shipId,
    shipName: AppStorage.shipName,
  };
  if (certificatePic) {
    if (certificatePic.path) {
      const response = yield call(uploadImage, certificatePic, 'certificatePic', AppStorage.shipId);
      param.certificatePic = response.id;
    } else {
      param.certificatePic = certificatePic.substr(certificatePic.lastIndexOf('/') + 1);
    }
  }
  const response = yield call(request, ApiFactory.saveShipCertificate(param));
  console.log('getCertificateManagement', response.toString());
  return response;
}

function* updateCertificateManagement(action) {
  console.log(action);
  try {
    yield put(updateCertificateManagementRoutine.request());
    const changeList = action.payload || [];
    yield all(changeList.map(param => call(updateCertificate, param)));
    yield put(updateCertificateManagementRoutine.success());
  } catch (e) {
    console.log(e.message);
    yield put(updateCertificateManagementRoutine.failure(e));
  } finally {
    yield put(updateCertificateManagementRoutine.fulfill());
  }
}

export function* updateCertificateManagementSaga() {
  yield takeLatest(updateCertificateManagementRoutine.TRIGGER, updateCertificateManagement);
}

// All sagas to be loaded
export default [
  getCertificateManagementSaga,
  updateCertificateManagementSaga,
];
