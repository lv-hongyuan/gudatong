import { getCertificateManagementRoutine, updateCertificateManagementRoutine } from './actions';

const initState = {
  list: undefined,
};

export default function (state = initState, action) {
  switch (action.type) {
    case getCertificateManagementRoutine.TRIGGER:
      return { ...state, loading: true };
    case getCertificateManagementRoutine.SUCCESS:
      return { ...state, list: action.payload };
    case getCertificateManagementRoutine.FAILURE:
      return { ...state, error: action.payload };
    case getCertificateManagementRoutine.FULFILL:
      return { ...state, loading: false };

    case updateCertificateManagementRoutine.TRIGGER:
      return { ...state, loading: true };
    case updateCertificateManagementRoutine.SUCCESS:
      return { ...state };
    case updateCertificateManagementRoutine.FAILURE:
      return { ...state, error: action.payload };
    case updateCertificateManagementRoutine.FULFILL:
      return { ...state, loading: false };

    default:
      return state;
  }
}
