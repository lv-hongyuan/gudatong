import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { Container, Content, View, Form, InputGroup, Item, Label } from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import HeaderButtons from 'react-navigation-header-buttons';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import AlertView from '../../components/Alert';
import { makeList } from './selectors';
import DatePullSelector from '../../components/DatePullSelector';
import myTheme from '../../Themes';
import screenHOC from '../screenHOC';
import commonStyles from '../../common/commonStyles';
import { getCertificateManagementPromise, updateCertificateManagementPromise } from './actions';
import SelectImageView from '../../components/SelectImageView';
import { DATE_WHEEL_TYPE } from '../../components/DateTimeWheel';

const styles = StyleSheet.create({
  container: {
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#DF001B',
    backgroundColor: '#ffffff',
    padding: 5,
  },
  inputGroup: {
    borderWidth: 0,
    borderColor: 'transparent',
    height: 40,
  },
  item: {
    flex: 1,
    height: '100%',
    borderBottomWidth: 0,
    paddingLeft: 0,
    marginLeft: 10,
    marginRight: 10,
    justifyContent: 'space-between',
  },
  titleLabel: {
    fontSize: 16,
    lineHeight: 16,
    color: '#DF001B',
    padding: 5,
  },
  inputLabel: {
    fontSize: 14,
    lineHeight: 15,
    paddingTop: 1,
    color: '#535353',
  },
});

/**
 *  证书管理页面
 * Created by jianzhexu on 2018/3/26
 */
@screenHOC
class CertificateManagement extends React.PureComponent {
  static navigationOptions = ({ navigation }) => ({
    title: '证书管理',
    headerRight: (
      <HeaderButtons>
        <HeaderButtons.Item
          title="保存"
          buttonStyle={{ fontSize: 14, color: '#ffffff' }}
          onPress={() => {
            const handleSubmitFunc = navigation.getParam('submit', undefined);
            if (handleSubmitFunc) {
              handleSubmitFunc();
            }
          }}
        />
      </HeaderButtons>
    ),
  });
  static propTypes = {
    navigation: PropTypes.object.isRequired,
    getCertificateManagement: PropTypes.func.isRequired,
    updateCertificateManagement: PropTypes.func.isRequired,
    list: PropTypes.array,
  };
  static defaultProps = {
    list: [],
  };

  constructor(props) {
    super(props);
    this.state = {
      list: props.list,
    };
    this.props.navigation.setParams({ submit: this.handleSubmit });
  }

  componentDidMount() {
    this.props.getCertificateManagement()
      .then(() => {
        this.setState({ list: this.props.list });
      })
      .catch(() => {
        AlertView.show({
          message: '获取证书信息失败',
          confirmAction: () => {
            this.props.navigation.goBack();
          },
        });
      });
  }

  setListData(type, params) {
    this.setState({
      list: this.state.list.map((item) => {
        if (item.type === type) {
          return { ...item, ...params };
        }
        return item;
      }),
    });
  }

  handleSubmit = () => {
    const changeList = this.state.list.filter((item) => {
      const oldItem = this.props.list.find(oriItem => item.type === oriItem.type);
      if (!oldItem) return true;

      return (
        oldItem.finalInspectionDay !== item.finalInspectionDay ||
        oldItem.certificatePic !== item.certificatePic ||
        oldItem.issuingDay !== item.issuingDay ||
        oldItem.effectiveDay !== item.effectiveDay
      );
    });
    if (changeList.length === 0) {
      this.props.navigation.goBack();
      return;
    }
    this.props.updateCertificateManagement(changeList)
      .then(() => {
        this.props.navigation.goBack();
      })
      .catch((error) => {
        AlertView.show({
          message: error.message,
          confirmAction: () => {
            this.props.navigation.goBack();
          },
        });
      });
  };

  render() {
    const { list } = this.state;
    return (
      <Container>
        <Content style={{ backgroundColor: '#FFF' }} contentInsetAdjustmentBehavior="scrollableAxes">
          <Form>
            {list.map((item) => {
              const {
                finalInspectionDay,
                certificatePic,
                issuingDay,
                effectiveDay,
                type,
              } = item;
              return (
                <View style={styles.container} key={type}>
                  <Label style={styles.titleLabel}>{type}</Label>
                  <InputGroup style={styles.inputGroup}>
                    <Item style={[styles.item, { borderBottomWidth: myTheme.borderWidth }]}>
                      <Label style={styles.inputLabel}>发证日期:</Label>
                      <DatePullSelector
                        type={DATE_WHEEL_TYPE.DATE}
                        textStyle={{ textAlign: 'right' }}
                        value={issuingDay || null}
                        onChangeValue={(value) => {
                          this.setListData(type, { issuingDay: value });
                        }}
                      />
                    </Item>
                  </InputGroup>
                  <InputGroup style={styles.inputGroup}>
                    <Item style={[styles.item, { borderBottomWidth: myTheme.borderWidth }]}>
                      <Label style={styles.inputLabel}>有效期至:</Label>
                      <DatePullSelector
                        type={DATE_WHEEL_TYPE.DATE}
                        textStyle={{ textAlign: 'right' }}
                        value={effectiveDay || null}
                        onChangeValue={(value) => {
                          this.setListData(type, { effectiveDay: value });
                        }}
                      />
                    </Item>
                  </InputGroup>
                  <InputGroup style={styles.inputGroup}>
                    <Item style={[styles.item, { borderBottomWidth: myTheme.borderWidth }]}>
                      <Label style={styles.inputLabel}>最后年检日期:</Label>
                      <DatePullSelector
                        type={DATE_WHEEL_TYPE.DATE}
                        textStyle={{ textAlign: 'right' }}
                        value={finalInspectionDay || null}
                        onChangeValue={(value) => {
                          this.setListData(type, { finalInspectionDay: value });
                        }}
                      />
                    </Item>
                  </InputGroup>
                  <InputGroup style={[styles.inputGroup, { height: 'auto', padding: 10 }]}>
                    <Item style={[commonStyles.inputItem, {
                      justifyContent: 'flex-start',
                      height: 100,
                      borderBottomWidth: 0,
                    }]}
                    >
                      <Label style={commonStyles.inputLabel}>证件图片:</Label>
                      <SelectImageView
                        source={certificatePic}
                        onPickImages={(images) => {
                          const image = images[0];
                          this.setListData(type, {
                            certificatePic: {
                              fileName: image.uri.split('/').reverse()[0],
                              fileSize: image.size,
                              path: image.uri,
                              uri: image.uri,
                            },
                          });
                        }}
                        onDelete={() => {
                          this.setListData(type, { certificatePic: null });
                        }}
                      />
                    </Item>
                  </InputGroup>
                </View>
              );
            })}
          </Form>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  list: makeList(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getCertificateManagement: getCertificateManagementPromise,
      updateCertificateManagement: updateCertificateManagementPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CertificateManagement);
