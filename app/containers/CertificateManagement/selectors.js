import { createSelector } from 'reselect/es';

const selectCertificateManagementDomain = () => state => state.certificateManagement;

const makeList = () => createSelector(
  selectCertificateManagementDomain(),
  (subState) => {
    console.debug(subState.list);
    return subState.list;
  },
);

export {
  makeList,
};
