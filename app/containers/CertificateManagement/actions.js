import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_CERTIFICATE_MANAGEMENT, UPDATE_CERTIFICATE_MANAGEMENT } from './constants';

export const getCertificateManagementRoutine = createRoutine(
  GET_CERTIFICATE_MANAGEMENT,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const getCertificateManagementPromise = promisifyRoutine(getCertificateManagementRoutine);

export const updateCertificateManagementRoutine = createRoutine(
  UPDATE_CERTIFICATE_MANAGEMENT,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const updateCertificateManagementPromise = promisifyRoutine(updateCertificateManagementRoutine);
