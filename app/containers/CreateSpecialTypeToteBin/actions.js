import {
  CREATE_SPECIAL_TYPE_TOTE_BIN_ACTION,
  CREATE_SPECIAL_TYPE_TOTE_BIN_ACTION_RESULT,
  UPDATE_SPECIAL_TYPE_TOTE_BIN_ACTION,
  UPDATE_SPECIAL_TYPE_TOTE_BIN_ACTION_RESULT,
} from './constants';

export function createSpecialTypeToteBinAction(data) {
  return {
    type: CREATE_SPECIAL_TYPE_TOTE_BIN_ACTION,
    payload: data,
  };
}

export function createSpecialTypeToteBinResultAction(data, success) {
  return {
    type: CREATE_SPECIAL_TYPE_TOTE_BIN_ACTION_RESULT,
    payload: {
      data,
      success,
    },
  };
}

export function updateSpecialTypeToteBinAction(data) {
  return {
    type: UPDATE_SPECIAL_TYPE_TOTE_BIN_ACTION,
    payload: data,
  };
}

export function updateSpecialTypeToteBinResultAction(data, success) {
  return {
    type: UPDATE_SPECIAL_TYPE_TOTE_BIN_ACTION_RESULT,
    payload: {
      data,
      success,
    },
  };
}
