import React from 'react';
import PropTypes from 'prop-types';
import { Keyboard, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { Container, Content, View, InputGroup, Item, Label } from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import HeaderButtons from 'react-navigation-header-buttons';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import _ from 'lodash';
import { makeSelectPopId, makeSelectPortName, makeSelectCreateSpecialTypeToteBinSuccess, makeData } from './selectors';
import { createSpecialTypeToteBinAction, updateSpecialTypeToteBinAction } from './actions';
import { errorMessage } from '../ErrorHandler/actions';
import commonStyles from '../../common/commonStyles';
import InputItem from '../../components/InputItem';
import stringToNumber from '../../utils/stringToNumber';
import screenHOC from '../screenHOC';
import { contTypeList } from '../../common/Constant';
import Selector from '../../components/Selector';
import { MaskType } from '../../components/InputItem/TextInput';
import validContNumber from '../../utils/validContNumber';
import { makePortList } from '../EditDxRecord/selectors';
import { getNextPortListPromise } from '../EditDxRecord/actions';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    paddingLeft: 10,
    paddingTop: 10,
    paddingBottom: 10,
    paddingRight: 10,
  },
});

/**
 * 创建特种箱
 * Created by jianzhexu on 2018/3/29
 */
@screenHOC
class CreateSpecialTypeToteBin extends React.PureComponent {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.getParam('title', '创建特种箱'),
    headerRight: (
      <HeaderButtons>
        <HeaderButtons.Item
          title="保存"
          buttonStyle={{ fontSize: 14, color: '#ffffff' }}
          onPress={() => {
            const handleSubmitFunc = navigation.getParam('submit', undefined);
            if (handleSubmitFunc) {
              requestAnimationFrame(() => {
                handleSubmitFunc();
              });
            }
          }}
        />
      </HeaderButtons>
    ),
  });
  static propTypes = {
    navigation: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    getNextPortList: PropTypes.func.isRequired,
    data: PropTypes.object.isRequired,
    success: PropTypes.bool.isRequired,
    popId: PropTypes.number.isRequired,
    portList: PropTypes.array.isRequired,
  };

  constructor(props) {
    super(props);

    const data = props.data || {};
    this.state = {
      id: data.id,
      popId: data.popId,
      portName: data.portName,
      voyageId: data.voyageId,
      contNo: data.contNo,
      bay: data.bay,
      type: data.type,
      wideLeft: data.wideLeft,
      wideRight: data.wideRight,
      longFront: data.longFront,
      longBehind: data.longBehind,
      height: data.height,
      weight: data.weight,
      contType: data.contType,
      isUpdating: false,
    };
    this.props.navigation.setParams({ submit: this.handleSubmit });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.data !== this.props.data) {
      const { data } = nextProps;
      this.setState({
        id: data.id,
        popId: data.popId,
        portName: data.portName,
        voyageId: data.voyageId,
        contNo: data.contNo,
        bay: data.bay,
        type: data.type,
        wideLeft: data.wideLeft,
        wideRight: data.wideRight,
        longFront: data.longFront,
        longBehind: data.longBehind,
        height: data.height,
        weight: data.weight,
        contType: data.contType,
      });
    }

    if (nextProps.success && this.state.isUpdating) {
      const goBackCallback = this.props.navigation.getParam('onGoBack', undefined);
      if (goBackCallback) {
        goBackCallback();
      }
      this.props.navigation.goBack();
      this.setState({
        isUpdating: false,
      });
    }
  }

  onPressPort = () => {
    Keyboard.dismiss();
    this.props.getNextPortList()
      .then(() => {
        this.select.showPicker();
      })
      .catch(() => {});
  };

  handleSubmit = () => {
    this.setState({
      isUpdating: true,
    }, () => {
      const isEdit = this.props.navigation.getParam('type', undefined);
      const {
        id,
        popId,
        portName,
        voyageId,
        contNo,
        bay,
        type,
        wideLeft,
        wideRight,
        longFront,
        longBehind,
        height,
        weight,
        contType,
      } = this.state;

      const param = {
        popId: popId || this.props.popId,
        portName,
        voyageId,
        contNo,
        bay,
        type,
        wideLeft: stringToNumber(wideLeft) || 0,
        wideRight: stringToNumber(wideRight) || 0,
        longFront: stringToNumber(longFront) || 0,
        longBehind: stringToNumber(longBehind) || 0,
        height: stringToNumber(height) || 0,
        weight: stringToNumber(weight) || 0,
        contType,
      };
      if (_.isEmpty(contNo)) {
        this.props.dispatch(errorMessage('请填写箱号'));
        return;
      }
      if (!validContNumber(contNo)) {
        this.props.dispatch(errorMessage('请填写正确的箱号'));
        return;
      }
      if (_.isEmpty(this.state.contType)) {
        this.props.dispatch(errorMessage('请选择箱型'));
        return;
      }
      if (isEdit) {
        param.id = id;
        this.props.dispatch(updateSpecialTypeToteBinAction(param));
      } else {
        this.props.dispatch(createSpecialTypeToteBinAction(param));
      }
    });
  };

  render() {
    return (
      <Container>
        <Content>
          <View style={styles.container}>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="箱号:"
                maxLength={11}
                value={this.state.contNo}
                onChangeText={(text) => {
                  this.setState({
                    contNo: text.trim(),
                  });
                }}
              />
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>卸货港:</Label>
                <Selector
                  ref={(ref) => {
                    this.select = ref;
                  }}
                  onPress={this.onPressPort}
                  value={this.state.portName}
                  items={this.props.portList}
                  pickerTitle="请选择卸货港"
                  getItemValue={item => item.text}
                  getItemText={item => item.text}
                  // pickerType="popover"
                  onSelected={(item) => {
                    this.setState({ portName: item.text });
                  }}
                />
              </Item>
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="贝位:"
                value={this.state.bay}
                onChangeText={(text) => {
                  this.setState({ bay: text.trim() });
                }}
              />
              <InputItem
                label="超宽左:"
                value={this.state.wideLeft}
                onChangeText={(text) => {
                  this.setState({ wideLeft: text });
                }}
                keyboardType="numeric"
                maskType={MaskType.FLOAT}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="类型:"
                value={this.state.type}
                onChangeText={(text) => {
                  this.setState({ type: text.trim() });
                }}
              />
              <InputItem
                label="超宽右:"
                value={this.state.wideRight}
                onChangeText={(text) => {
                  this.setState({ wideRight: text });
                }}
                keyboardType="numeric"
                maskType={MaskType.FLOAT}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="重量:"
                value={this.state.weight}
                onChangeText={(text) => {
                  this.setState({ weight: text });
                }}
                keyboardType="numeric"
                maskType={MaskType.FLOAT}
              />
              <InputItem
                label="超长前:"
                value={this.state.longFront}
                onChangeText={(text) => {
                  this.setState({ longFront: text });
                }}
                keyboardType="numeric"
                maskType={MaskType.FLOAT}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="超高:"
                value={this.state.height}
                onChangeText={(text) => {
                  this.setState({ height: text });
                }}
                keyboardType="numeric"
                maskType={MaskType.FLOAT}
              />
              <InputItem
                label="超长后:"
                value={this.state.longBehind}
                onChangeText={(text) => {
                  this.setState({ longBehind: text });
                }}
                keyboardType="numeric"
                maskType={MaskType.FLOAT}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>箱型:</Label>
                <Selector
                  value={this.state.contType}
                  items={contTypeList}
                  pickerTitle="请选择箱型"
                  getItemValue={item => item}
                  getItemText={item => item}
                  // pickerType="popover"
                  onSelected={(item) => {
                    this.setState({ contType: item });
                  }}
                />
              </Item>
            </InputGroup>
          </View>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  data: makeData(),
  success: makeSelectCreateSpecialTypeToteBinSuccess(),
  portName: makeSelectPortName(),
  popId: makeSelectPopId(),
  portList: makePortList(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getNextPortList: getNextPortListPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateSpecialTypeToteBin);
