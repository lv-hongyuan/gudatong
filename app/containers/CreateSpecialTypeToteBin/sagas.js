import { call, put, takeLatest } from 'redux-saga/effects';
import { CREATE_SPECIAL_TYPE_TOTE_BIN_ACTION, UPDATE_SPECIAL_TYPE_TOTE_BIN_ACTION } from './constants';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import { createSpecialTypeToteBinResultAction, updateSpecialTypeToteBinResultAction } from './actions';
import { errorMessage } from '../ErrorHandler/actions';

function* createSpecialTypeToteBin(action) {
  console.log(action);
  try {
    const response = yield call(request, ApiFactory.createSpecialCont(action.payload));
    yield put(createSpecialTypeToteBinResultAction(response, true));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(createSpecialTypeToteBinResultAction(undefined, false));
  }
}

export function* createSpecialTypeToteBinSaga() {
  yield takeLatest(CREATE_SPECIAL_TYPE_TOTE_BIN_ACTION, createSpecialTypeToteBin);
}

function* updateSpecialTypeToteBin(action) {
  console.log(action);
  try {
    const response = yield call(request, ApiFactory.updateSpecialCont(action.payload));
    yield put(updateSpecialTypeToteBinResultAction(response, true));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(updateSpecialTypeToteBinResultAction(undefined, false));
  }
}

export function* updateSpecialTypeToteBinSaga() {
  yield takeLatest(UPDATE_SPECIAL_TYPE_TOTE_BIN_ACTION, updateSpecialTypeToteBin);
}

// All sagas to be loaded
export default [
  createSpecialTypeToteBinSaga, updateSpecialTypeToteBinSaga,
];
