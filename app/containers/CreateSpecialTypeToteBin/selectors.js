import { createSelector } from 'reselect/es';

const selectCreateSpecialTypeToteBinDomain = () => state => state.createSpecialTypeToteBin;
const selectShipStateDomain = () => state => state.shipWork.data;

const makeData = () => createSelector(
  (state, props) => props.navigation.state.params,
  subState => subState.data,
);

const makeSelectCreateSpecialTypeToteBinSuccess = () => createSelector(
  selectCreateSpecialTypeToteBinDomain(),
  (subState) => {
    console.debug(subState.success);
    return subState.success;
  },
);

const makeSelectPortName = () => createSelector(
  selectShipStateDomain(),
  subState => subState.portName,
);

const makeSelectPopId = () => createSelector(
  selectShipStateDomain(),
  (subState) => {
    console.debug(subState.success);
    return subState.popId;
  },
);

export {
  makeData,
  makeSelectCreateSpecialTypeToteBinSuccess,
  selectCreateSpecialTypeToteBinDomain,
  makeSelectPortName,
  makeSelectPopId,
};
