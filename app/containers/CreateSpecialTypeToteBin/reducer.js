import {
  CREATE_SPECIAL_TYPE_TOTE_BIN_ACTION_RESULT,
  UPDATE_SPECIAL_TYPE_TOTE_BIN_ACTION_RESULT,
  UPDATE_SPECIAL_TYPE_TOTE_BIN_ACTION,
  CREATE_SPECIAL_TYPE_TOTE_BIN_ACTION,
} from './constants';

const initState = {
  success: false,
};

export default function (state = initState, action) {
  switch (action.type) {
    case UPDATE_SPECIAL_TYPE_TOTE_BIN_ACTION:
    case CREATE_SPECIAL_TYPE_TOTE_BIN_ACTION:
      return { ...state, success: false };
    case CREATE_SPECIAL_TYPE_TOTE_BIN_ACTION_RESULT:
    case UPDATE_SPECIAL_TYPE_TOTE_BIN_ACTION_RESULT:
      return { ...state, success: action.payload.success };
    default:
      return state;
  }
}
