import { createSelector } from 'reselect/es';

const selectTugboatApplyListDomain = () => state => state.tugboatApplyList;

const makeSelectGetTugboatApplyListSuccess = () => createSelector(
  selectTugboatApplyListDomain(),
  (subState) => {
    console.debug(subState.success);
    return subState.success;
  },
);

const makeSelectTugboatApplyList = () => createSelector(
  selectTugboatApplyListDomain(),
  (subState) => {
    console.debug(subState.success);
    return subState.data;
  },
);

export {
  makeSelectGetTugboatApplyListSuccess,
  selectTugboatApplyListDomain,
  makeSelectTugboatApplyList,
};
