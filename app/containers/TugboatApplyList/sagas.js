import { call, put, takeLatest } from 'redux-saga/effects';
import { GET_TUGBOAT_APPLY_LIST_ACTION, DELETE_TUGBOAT_APPLY_ACTION } from './constants';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import { getTugboatApplyListResultAction, deleteTugboatApplyResultAction } from './actions';
import { errorMessage } from '../ErrorHandler/actions';
import { getPopId } from '../ShipWork/sagas';

function* getTugboatApplyList(action) {
  console.log(action);
  try {
    const popId = yield call(getPopId);
    const response = yield call(request, ApiFactory.findApplyTugs(popId));
    yield put(getTugboatApplyListResultAction(response, true));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(getTugboatApplyListResultAction(undefined, false));
  }
}

export function* getTugboatApplyListSaga() {
  yield takeLatest(GET_TUGBOAT_APPLY_LIST_ACTION, getTugboatApplyList);
}

function* deleteTugboatApply(action) {
  console.log(action);
  try {
    const response = yield call(request, ApiFactory.deleteApplyTug(action.payload));
    yield put(deleteTugboatApplyResultAction(response, true));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(deleteTugboatApplyResultAction(undefined, false));
  }
}

export function* deleteTugboatApplySaga() {
  yield takeLatest(DELETE_TUGBOAT_APPLY_ACTION, deleteTugboatApply);
}

// All sagas to be loaded
export default [
  getTugboatApplyListSaga,
  deleteTugboatApplySaga,
];
