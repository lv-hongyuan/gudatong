import {
  GET_TUGBOAT_APPLY_LIST_ACTION,
  GET_TUGBOAT_APPLY_LIST_ACTION_RESULT,
  DELETE_TUGBOAT_APPLY_ACTION_RESULT,
  DELETE_TUGBOAT_APPLY_ACTION,
} from './constants';

const initState = {
  success: false,
  data: [],
};

export default function (state = initState, action) {
  switch (action.type) {
    case DELETE_TUGBOAT_APPLY_ACTION:
    case GET_TUGBOAT_APPLY_LIST_ACTION:
      return { ...state, success: false };
    case GET_TUGBOAT_APPLY_LIST_ACTION_RESULT:
      return {
        ...state,
        success: action.payload.success,
        data: action.payload.data ? action.payload.data.dtoList : [],
      };
    case DELETE_TUGBOAT_APPLY_ACTION_RESULT:
      return { ...state, success: action.payload.success };
    default:
      return state;
  }
}
