import {
  GET_TUGBOAT_APPLY_LIST_ACTION,
  GET_TUGBOAT_APPLY_LIST_ACTION_RESULT,
  DELETE_TUGBOAT_APPLY_ACTION,
  DELETE_TUGBOAT_APPLY_ACTION_RESULT,
} from './constants';

export function getTugboatApplyListAction(data) {
  return {
    type: GET_TUGBOAT_APPLY_LIST_ACTION,
    payload: data,
  };
}

export function getTugboatApplyListResultAction(data, success) {
  return {
    type: GET_TUGBOAT_APPLY_LIST_ACTION_RESULT,
    payload: {
      data,
      success,
    },
  };
}

export function deleteTugboatApplyAction(data) {
  return {
    type: DELETE_TUGBOAT_APPLY_ACTION,
    payload: data,
  };
}

export function refreshTugboatApplyListAction(data) {
  return {
    type: GET_TUGBOAT_APPLY_LIST_ACTION,
    payload: data,
    noLoading: true,
  };
}

export function deleteTugboatApplyResultAction(success) {
  return {
    type: DELETE_TUGBOAT_APPLY_ACTION_RESULT,
    payload: {
      success,
    },
  };
}
