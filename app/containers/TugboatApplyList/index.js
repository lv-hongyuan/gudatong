import React from 'react';
import PropTypes from 'prop-types';
import { FlatList, RefreshControl } from 'react-native';
import { connect } from 'react-redux';
import { Container, View } from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import HeaderButtons from 'react-navigation-header-buttons';
import TugboatApplyItem from '../../components/TugboatApplyItem';
import { ROUTE_CREATE_TUGBOAT_APPLY } from '../../RouteConstant';
import { OpType } from '../CreateTugboatApply/constants';
import { makeSelectGetTugboatApplyListSuccess, makeSelectTugboatApplyList } from './selectors';
import { deleteTugboatApplyAction, getTugboatApplyListAction, refreshTugboatApplyListAction } from './actions';
import { errorMessage } from '../ErrorHandler/actions';
import myTheme from '../../Themes';
import EmptyView from '../../components/EmptyView';
import screenHOC from '../screenHOC';
import { ReviewState, reviewTitleFromState } from '../../common/Constant';

/**
 * 拖轮申请列表
 * Created by jianzhexu on 2018/4/3
 */
@screenHOC
class TugboatApplyList extends React.PureComponent {
  static navigationOptions = ({ navigation }) => ({
    title: '拖轮申请记录',
    headerRight: (
      <HeaderButtons>
        <HeaderButtons.Item
          title="创建"
          buttonStyle={{ fontSize: 14, color: '#ffffff' }}
          onPress={() => {
            const reload = navigation.getParam('reload', undefined);
            navigation.navigate(ROUTE_CREATE_TUGBOAT_APPLY, {
              title: '创建拖轮申请',
              onGoBack: () => {
                if (reload) {
                  reload();
                }
              },
            });
          }}
        />
      </HeaderButtons>
    ),
  });
  static propTypes = {
    navigation: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    success: PropTypes.bool.isRequired,
    data: PropTypes.array.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      isDelete: false,
      refreshing: false,
    };
    this.props.navigation.setParams({ reload: this.reLoadingPage });
  }

  componentWillMount() {
    this.reLoadingPage();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.success && this.state.isDelete) {
      this.setState({ isDelete: false }, () => {
        this.reLoadingPage();
      });
    }
    if (this.state.refreshing) {
      this.setState({ refreshing: false });
    }
  }

  reLoadingPage = () => {
    if (this.state.refreshing) {
      requestAnimationFrame(() => {
        this.props.dispatch(refreshTugboatApplyListAction());
      });
    } else {
      requestAnimationFrame(() => {
        this.props.dispatch(getTugboatApplyListAction());
      });
    }
  };

  itemPress = (data) => {
    if (data.opType === OpType.APPLY) {
      if (data.state === ReviewState.Approved || data.state === ReviewState.Dismissed) {
        this.props.dispatch(errorMessage(`申请${reviewTitleFromState(data.state)}，无法编辑`));
        return;
      }
      this.props.navigation.navigate(ROUTE_CREATE_TUGBOAT_APPLY, {
        title: '编辑拖轮申请',
        onGoBack: () => {
          this.reLoadingPage();
        },
        type: OpType.APPLY,
        data,
      });
    } else {
      this.props.navigation.navigate(ROUTE_CREATE_TUGBOAT_APPLY, {
        title: '编辑拖轮使用记录',
        onGoBack: () => {
          this.reLoadingPage();
        },
        type: OpType.USE,
        data,
      });
    }
  };

  deleteItem = (data) => {
    if (data.opType === OpType.APPLY) {
      if (data.state === ReviewState.Approved || data.state === ReviewState.Dismissed) {
        this.props.dispatch(errorMessage(`申请${reviewTitleFromState(data.state)}，无法删除`));
        return;
      }
    }
    this.setState({ isDelete: true }, () => {
      this.props.dispatch(deleteTugboatApplyAction(data.id));
    });
  };

  renderItem = ({ item }) => (
    <TugboatApplyItem
      applyReasons={item.applyReasons}
      applyType={item.applyType}
      tugNum={item.tugNum}
      id={item.id}
      portName={item.portName}
      rejectReasons={item.rejectReasons ? item.rejectReasons : ''}
      state={item.state}
      useWay={item.useWay}
      onDelete={this.deleteItem}
      onItemPress={this.itemPress}
      popId={item.popId}
      opType={item.opType}
    />);

  renderEmptyView = () => (<EmptyView />);

  render() {
    return (
      <Container theme={myTheme}>
        <FlatList
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => {
                this.setState({ refreshing: true }, () => {
                  this.reLoadingPage();
                });
              }}
            />
          }
          contentInsetAdjustmentBehavior="scrollableAxes"
          style={{ flex: 1, backgroundColor: '#e0e0e0' }}
          keyExtractor={item => `${item.id}`}
          data={this.props.data}
          ListEmptyComponent={this.renderEmptyView}
          initialNumToRender={10}
          renderItem={this.renderItem}
          ItemSeparatorComponent={() => (<View style={{ height: 5 }} />)}
          ListFooterComponent={() => (<View style={{ height: 5 }} />)}
          ListHeaderComponent={() => (<View style={{ height: 5 }} />)}
        />
      </Container>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  success: makeSelectGetTugboatApplyListSuccess(),
  data: makeSelectTugboatApplyList(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(TugboatApplyList);
