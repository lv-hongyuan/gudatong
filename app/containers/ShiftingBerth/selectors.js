import { createSelector } from 'reselect/es';

const selectShiftingBerthDomain = () => state => state.shiftingBerth;

const makeSelectShiftingBerthListSuccess = () => createSelector(
  selectShiftingBerthDomain(),
  (subState) => {
    console.debug(subState.success);
    return subState.success;
  },
);
const makeSelectShiftingBerthList = () => createSelector(
  selectShiftingBerthDomain(),
  (subState) => {
    console.debug(subState.success);
    return subState.data;
  },
);
export {
  makeSelectShiftingBerthListSuccess,
  selectShiftingBerthDomain,
  makeSelectShiftingBerthList,
};
