import React from 'react';
import PropTypes from 'prop-types';
import { FlatList, RefreshControl } from 'react-native';
import { connect } from 'react-redux';
import { Container, View } from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import HeaderButtons from 'react-navigation-header-buttons';
import {
  ROUTE_CREATE_SHIFTING_BERTH,
} from '../../RouteConstant';
import ShiftingBerthItem from '../../components/ShiftingBerthItem';
import { deleteShiftingBerthAction, getShiftingBerthListAction, refreshShiftingBerthAction } from './actions';
import myTheme from '../../Themes';
import { makeSelectShiftingBerthList, makeSelectShiftingBerthListSuccess } from './selectors';
import EmptyView from '../../components/EmptyView';
import screenHOC from '../screenHOC';

/**
 * 移泊记录列表
 * Created by jianzhexu on 2018/3/26
 */
@screenHOC
class ShiftingBerth extends React.PureComponent {
  static navigationOptions = ({ navigation }) => ({
    title: '移泊记录',
    headerRight: (
      <HeaderButtons>
        <HeaderButtons.Item
          title="创建"
          buttonStyle={{ fontSize: 14, color: '#ffffff' }}
          onPress={() => {
            const reload = navigation.getParam('reload', undefined);
            navigation.navigate(ROUTE_CREATE_SHIFTING_BERTH, {
              title: '创建移泊记录',
              onGoBack: () => {
                if (reload) {
                  reload();
                }
              },
            });
          }}
        />
      </HeaderButtons>
    ),
  });
  static propTypes = {
    navigation: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    data: PropTypes.array.isRequired,
    success: PropTypes.bool.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      isDelete: false,
      refreshing: false,
    };
    this.props.navigation.setParams({ reload: this.reLoadingPage });
  }

  componentWillMount() {
    this.reLoadingPage();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.success && this.state.isDelete) {
      this.setState({ isDelete: false }, () => {
        this.reLoadingPage();
      });
    }
    if (this.state.refreshing) {
      this.setState({ refreshing: false });
    }
  }

  reLoadingPage = () => {
    if (this.state.refreshing) {
      requestAnimationFrame(() => {
        this.props.dispatch(refreshShiftingBerthAction());
      });
    } else {
      requestAnimationFrame(() => {
        this.props.dispatch(getShiftingBerthListAction());
      });
    }
  };
  itemPress = (data) => {
    this.props.navigation.navigate(ROUTE_CREATE_SHIFTING_BERTH, {
      title: '编辑移泊记录',
      onGoBack: () => {
        this.reLoadingPage();
      },
      type: 1,
      data,
    });
  };

  deleteItem = (id) => {
    this.setState({ isDelete: true }, () => {
      this.props.dispatch(deleteShiftingBerthAction(id));
    });
  };

  renderItem = ({ item }) => (<ShiftingBerthItem
    tugNum={item.tugNum}
    id={item.id}
    portName={item.portName}
    shiftTimeEnd={item.shiftTimeEnd}
    shiftTimeStart={item.shiftTimeStart}
    onDelete={this.deleteItem}
    onItemPress={this.itemPress}
    popId={item.popId}
  />);

  renderEmptyView = () => (<EmptyView />);

  render() {
    return (
      <Container theme={myTheme}>
        <FlatList
          contentInsetAdjustmentBehavior="scrollableAxes"
          style={{ flex: 1, backgroundColor: '#e0e0e0' }}
          keyExtractor={item => `${item.id}`}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => {
                this.setState({ refreshing: true }, () => {
                  this.reLoadingPage();
                });
              }}
            />
          }
          data={this.props.data}
          ListEmptyComponent={this.renderEmptyView}
          initialNumToRender={10}
          renderItem={this.renderItem}
          ItemSeparatorComponent={() => (<View style={{ height: 5 }} />)}
          ListFooterComponent={() => (<View style={{ height: 5 }} />)}
          ListHeaderComponent={() => (<View style={{ height: 5 }} />)}
        />
      </Container>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  success: makeSelectShiftingBerthListSuccess(),
  data: makeSelectShiftingBerthList(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ShiftingBerth);
