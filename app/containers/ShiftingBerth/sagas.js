import { call, put, takeLatest } from 'redux-saga/effects';
import { GET_SHIFTING_BERTH_LIST_ACTION, DELETE_SHIFTING_BERTH_ACTION } from './constants';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import { getShiftingBerthListResultAction, deleteShiftingBerthResultAction } from './actions';
import { errorMessage } from '../ErrorHandler/actions';
import { getPopId } from '../ShipWork/sagas';

function* getShiftingBerthList(action) {
  console.log(action);
  try {
    const popId = yield call(getPopId);
    const response = yield call(request, ApiFactory.findShiftRecords(popId));
    console.log(response);
    yield put(getShiftingBerthListResultAction(response, true));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(getShiftingBerthListResultAction(undefined, false));
  }
}

export function* getShiftingBerthListSaga() {
  yield takeLatest(GET_SHIFTING_BERTH_LIST_ACTION, getShiftingBerthList);
}

function* deleteShiftingBerth(action) {
  console.log(action);
  try {
    const response = yield call(request, ApiFactory.deleteShiftRecord(action.payload));
    yield put(deleteShiftingBerthResultAction(response, true));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(deleteShiftingBerthResultAction(undefined, false));
  }
}

export function* deleteShiftingBerthSaga() {
  yield takeLatest(DELETE_SHIFTING_BERTH_ACTION, deleteShiftingBerth);
}

// All sagas to be loaded
export default [
  getShiftingBerthListSaga,
  deleteShiftingBerthSaga,
];
