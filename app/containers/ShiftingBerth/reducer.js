import {
  GET_SHIFTING_BERTH_LIST_ACTION_RESULT,
  DELETE_SHIFTING_BERTH_ACTION_RESULT,
  DELETE_SHIFTING_BERTH_ACTION,
  GET_SHIFTING_BERTH_LIST_ACTION,
} from './constants';

const initState = {
  success: false,
  data: [],
};

export default function (state = initState, action) {
  switch (action.type) {
    case DELETE_SHIFTING_BERTH_ACTION:
    case GET_SHIFTING_BERTH_LIST_ACTION:
      return { ...state, success: false };
    case GET_SHIFTING_BERTH_LIST_ACTION_RESULT:
      return {
        ...state,
        success: action.payload.success,
        data: action.payload.data ? action.payload.data.dtoList : [],
      };
    case DELETE_SHIFTING_BERTH_ACTION_RESULT:
      return { ...state, success: action.payload.success };
    default:
      return state;
  }
}
