import {
  GET_SHIFTING_BERTH_LIST_ACTION,
  GET_SHIFTING_BERTH_LIST_ACTION_RESULT,
  DELETE_SHIFTING_BERTH_ACTION,
  DELETE_SHIFTING_BERTH_ACTION_RESULT,
} from './constants';

export function getShiftingBerthListAction() {
  return {
    type: GET_SHIFTING_BERTH_LIST_ACTION,
  };
}

export function getShiftingBerthListResultAction(data, success) {
  return {
    type: GET_SHIFTING_BERTH_LIST_ACTION_RESULT,
    payload: {
      data,
      success,
    },
  };
}

export function deleteShiftingBerthAction(data) {
  return {
    type: DELETE_SHIFTING_BERTH_ACTION,
    payload: data,
  };
}

export function refreshShiftingBerthAction() {
  return {
    type: GET_SHIFTING_BERTH_LIST_ACTION,
    noLoading: true,
  };
}

export function deleteShiftingBerthResultAction(success) {
  return {
    type: DELETE_SHIFTING_BERTH_ACTION_RESULT,
    payload: {
      success,
    },
  };
}
