import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  APP_INIT,
  SAVE_USER_INFO,
  BIND_REGISTRATION_ID,
} from './constants';

export const appInitRoutine = createRoutine(APP_INIT);

export const saveUserInfoRoutine = createRoutine(SAVE_USER_INFO);

export const bindRegistrationIDRoutine = createRoutine(BIND_REGISTRATION_ID);
export const bindRegistrationIDPromiseCreator = promisifyRoutine(bindRegistrationIDRoutine);
