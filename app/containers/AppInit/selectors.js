import { createSelector } from 'reselect/es';

const selectAppDomain = () => state => state.app;

const makeAppInit = () =>
  createSelector(selectAppDomain(), (subState) => {
    console.debug(subState.appInit);
    return subState.appInit;
  });

const makeIsLogin = () =>
  createSelector(selectAppDomain(), (subState) => {
    console.debug(subState.user);
    return !!subState.user;
  });

const makeUserInfo = () =>
  createSelector(selectAppDomain(), (subState) => {
    console.debug(subState.user);
    return subState.user || {};
  });

export {
  makeAppInit,
  makeIsLogin,
  makeUserInfo,
};
