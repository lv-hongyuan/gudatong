import React from 'react';
import PropTypes from 'prop-types';
import { View, StatusBar, AsyncStorage, NetInfo, Platform } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect/es';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { appInitRoutine } from './actions';
import { makeAppInit } from './selectors';
import { ROUTE_AUTH_LOADING, ROUTE_OFFLINE } from '../../RouteConstant';
import { uploadAllOfflineNavigationRecordPromise } from '../OfflineNavigationRecordList/actions';
import { uploadAllOfflineArrivePortRecordPromise } from '../OfflineArrivePortRecordList/actions';
import { uploadAllVoyageEventPromise } from '../VoyageEventList/actions';
import { makeSelectIsSail } from '../ShipWork/selectors';
import NavigatorService from '../../NavigatorService';
import StorageKeys from '../../common/StorageKeys';
import Loading from '../../components/Loading';
import { Initializer,Location } from 'react-native-baidumap-sdk'

const platform = Platform.OS;
class AppInit extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
    };
  }

  componentDidMount() {
    Initializer.init('vtX5XoRXkKmlWYsvrlF00kQNKn70Cj62').catch(e => console.error(e))
    Location.init()
    this.props.appInitRoutine();
    NetInfo.isConnected.fetch().done((isConnected) => {
      if (isConnected) {
        AsyncStorage.getItem(StorageKeys.IS_SAIL)
          .then((isSail) => {
            if (isSail === '0') {  //刚进入app如果app处于航行状态切已接入网络，则上传离线船报
              this.props.uploadAllOfflineNavigationRecord()
                .then(() => { })
                .catch(() => { });

              //上传离线的停航原因
              this.props.uploadAllVoyageEvent()
                .then(() => { })
                .catch(() => { });
            }
          })
          .catch((e) => { console.log(e) })
      }
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isConnected !== this.props.isConnected) {
      AsyncStorage.getItem(StorageKeys.IS_SAIL)
        .then((isSail) => {
          // 航行状态时切换
          if (isSail === '0') this.onNetChange(nextProps.isConnected);
        })
        .catch(() => {
          alert('上传数据失败')
          this.onNetChange(nextProps.isConnected);
        });
    }
  }

  componentWillUnmount() {
    this.timer && clearTimeout(this.timer)
  }

  //全局监听网路变化
  onNetChange(isConnected) {
    this.setState({isLoading:true})
    this.timer = setTimeout(()=>{
      this.setState({isLoading:false})
      if (isConnected) {
        //上传所有离线的航线船报和抵港确报
        this.props.uploadAllOfflineNavigationRecord()
          .then(() => {})
          .catch(() => {});
        //上传离线的停航原因
        this.props.uploadAllVoyageEvent()
          .then(() => {})
          .catch(() => {});
        NavigatorService.navigate(ROUTE_AUTH_LOADING);
      } else {
        NavigatorService.navigate(ROUTE_OFFLINE);
      }
    },2500)
  }

  render() {
    return (
      <View style={{ backgroundColor: '#DC001B', flex: 1,paddingTop:platform == 'android' ? 25 : 0}}>
        <StatusBar backgroundColor="#DC001B" barStyle="light-content" />
        {this.props.appInit && this.props.children}
        {this.state.isLoading && <Loading />}
      </View>
    );
  }
}

AppInit.propTypes = {
  appInitRoutine: PropTypes.func.isRequired,
  uploadAllOfflineNavigationRecord: PropTypes.func.isRequired,
  uploadAllVoyageEvent: PropTypes.func.isRequired,
  appInit: PropTypes.bool.isRequired,
  isConnected: PropTypes.bool.isRequired,
  children: PropTypes.any.isRequired,
};

const mapStateToProps = createStructuredSelector({
  appInit: makeAppInit(),
  isConnected: state => state.network.isConnected,
  isSail: makeSelectIsSail(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators({
      appInitRoutine,
    }, dispatch),
    ...bindPromiseCreators({
      uploadAllOfflineNavigationRecord: uploadAllOfflineNavigationRecordPromise,
      uploadAllVoyageEvent: uploadAllVoyageEventPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AppInit);
