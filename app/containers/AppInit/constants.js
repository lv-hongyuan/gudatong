/*
 *
 * Auth constants
 *
 */
export const APP_INIT = 'app/Auth/APP_INIT';
export const SAVE_USER_INFO = 'app/Auth/SAVE_USER_INFO';
export const BIND_REGISTRATION_ID = 'app/Auth/BIND_REGISTRATION_ID';
