import { call, put, takeLatest, fork } from 'redux-saga/effects';
import { AsyncStorage } from 'react-native';
import ApiConstants, { EnableServerChange, HostKey } from '../../common/ApiConstants';
import StorageKeys from '../../common/StorageKeys';
import request from '../../utils/request';
import {
  appInitRoutine, bindRegistrationIDRoutine,
  saveUserInfoRoutine,
} from './actions';
import { errorMessage } from '../ErrorHandler/actions';
import { header } from '../../common/Api';
import ApiFactory from '../../common/Api';
import AppStorage from '../../utils/AppStorage';
import uploadAppInfo from '../../common/uploadAppInfo';

// app初始化
function* appInit(action) {
  console.log(action);
  try {
    yield put(appInitRoutine.request());
    // 获取本地userInfo
    const userInfoStr = yield call(AsyncStorage.getItem, StorageKeys.USER);
    const userInfo = userInfoStr && JSON.parse(userInfoStr);

    if (EnableServerChange) {
      // 获取存储的Host
      const host = yield call(AsyncStorage.getItem, HostKey);
      if (host) {
        ApiConstants.host = host;
      }
    }

    // 设置token
    header.content.token = userInfo && userInfo.tokenId;
    // 上传app版本信息
    yield fork(uploadAppInfo, userInfo && userInfo.userId);
    yield put(appInitRoutine.success(userInfo));
  } catch (e) {
    console.log(e.message);
    yield put(appInitRoutine.failure(e));
  } finally {
    yield put(appInitRoutine.fulfill());
  }
}

export function* appInitSaga() {
  yield takeLatest(appInitRoutine.TRIGGER, appInit);
}

// 保存用户数据
export function* saveUserInfo(action) {
  console.log(action);
  try {
    yield put(saveUserInfoRoutine.request());
    const userInfo = action.payload;
    if (userInfo) {
      yield call(AsyncStorage.setItem, StorageKeys.USER, JSON.stringify(userInfo));
    } else {
      yield call(AsyncStorage.removeItem, StorageKeys.USER);
    }
    yield put(saveUserInfoRoutine.success(userInfo));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(saveUserInfoRoutine.failure(e));
  } finally {
    yield put(saveUserInfoRoutine.fulfill());
  }
}

export function* saveUserInfoSaga() {
  yield takeLatest(saveUserInfoRoutine.TRIGGER, saveUserInfo);
}

// 绑定RegistrationID
function* bindRegistrationID(action) {
  console.log(action);
  try {
    yield put(bindRegistrationIDRoutine.request());
    //发送绑定的DeviceKey
    // const response = yield call(request, ApiFactory.bindRegistrationID(action.payload));
    // console.log(response);
    const userInfo = AppStorage.userInfo && { ...AppStorage.userInfo, deviceKey: action.payload };
    if (userInfo) {
      yield call(AsyncStorage.setItem, StorageKeys.USER, JSON.stringify(userInfo));
    }
    yield put(bindRegistrationIDRoutine.success(action.payload));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(bindRegistrationIDRoutine.failure(e));
  } finally {
    yield put(bindRegistrationIDRoutine.fulfill());
  }
}

export function* bindRegistrationIDSaga() {
  yield takeLatest(bindRegistrationIDRoutine.TRIGGER, bindRegistrationID);
}

// All sagas to be loaded
export default [appInitSaga, saveUserInfoSaga, bindRegistrationIDSaga];
