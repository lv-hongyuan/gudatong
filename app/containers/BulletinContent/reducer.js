import {getPublishNoticeReadRoutine} from "./actions";
import {RefreshState} from "../../components/RefreshListView";

const defaultState = {
  loadingError: null,
  isLoading: false,
  workDetail: null,
};

export default function (state = defaultState, action) {
  switch (action.type) {

    case getPublishNoticeReadRoutine.TRIGGER:
      return { ...state, loading: true, refreshState: RefreshState.HeaderRefreshing };
    case getPublishNoticeReadRoutine.SUCCESS:
      return { ...state, workDetail: action.payload, refreshState: RefreshState.NoMoreData };
    case getPublishNoticeReadRoutine.FAILURE:
      return {
        ...state, workDetail: [], error: action.payload, refreshState: RefreshState.Failure,
      };
    case getPublishNoticeReadRoutine.FULFILL:
      return { ...state, loading: false };

    default:
      return state;
  }
}
