
//  公告栏详情  /  航次指令详情

import React from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet, View, Dimensions, DeviceEventEmitter
} from 'react-native';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect/es';
import {
  Container, Content, Form, Text,
} from 'native-base';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { getPublishNoticeReadPromise } from './actions';
import myTheme from '../../Themes';
import screenHOC from '../screenHOC';
import {defaultFormat} from "../../utils/DateFormat";
import {makeList, makeRefreshState, makeIsLoading }from "./selectors";
let deviceWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
  form: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 8,
    paddingTop: 20,
    flex: 1,
  },
  contentView: {
    width: deviceWidth-40,
    textAlign:'center',
  },
  contentText: {
    fontSize:18,
    color: '#535353',
    lineHeight:22,
  },
  khText: {
    fontSize:18,
    color: '#EB7C3B',
    lineHeight:22,
  },
  dateView: {
    marginTop:7,
    // width: deviceWidth-30,
  },
  publisherText: {
    textAlign:'right',
    paddingRight: 20,
    fontSize:16,
    color: '#535353',
    width: deviceWidth-20,
  },
  dateText: {
    textAlign:'right',
    paddingRight: 20,
    fontSize:16,
    color: '#535353',
    width: deviceWidth-20,
  }
});

@screenHOC
class BulletinContent extends React.PureComponent {
  constructor(props) {
    super(props);
    this.params = this.props.navigation.state.params || {};
    console.log(this.params);
      //isPush 0:不是来自推送的消息 1:推送过来的消息
      this.state = {
          detailContent: '',
          publishDate: '',
          publisher: '',
          lineType: '',
          feemon: '',
          businessWeek: '',
          issueSequence: '',
          type: '',
          headContent:'',
          footContent:'',
          isContent: false,
          isPush: this.params.isPush,
      }
  }

  componentDidMount() {
    this.loadData(this.params.id)
    this.listener = DeviceEventEmitter.addListener('refreshMessageState',(params)=>{
      this.loadData(params.id)
    });
  }
  componentWillUnmount() {
    this.listener.remove();
  }

  //加载公告详情
  loadData(id){
    if(this.params.item && this.params.item.state == 0){
      DeviceEventEmitter.emit('getPush',{id,state:this.state.isPush})
    }
    this.props.getPublishNoticeRead({
      id: id,
      isPush:this.state.isPush,
    }).then(() => {
      if(this.params.isPush == 1 || this.params.item.state == 0){
        //如果是推送消息或者未读信息，刷新角标状态和列表数据
        DeviceEventEmitter.emit('refreshCornerMarkState')
      }
      if(this.props.workDetail != {}){
        this.stringChange(this.props.workDetail.content)
        this.setState({
          publishDate: this.props.workDetail.publishDate,
          publisher: this.props.workDetail.publisher,
          lineType: this.props.workDetail.lineType,
          feemon: this.props.workDetail.feemon,
          businessWeek: this.props.workDetail.businessWeek,
          issueSequence: this.props.workDetail.issueSequence,
          type: this.props.workDetail.type,
          title:this.props.workDetail.title,
        })
      }
    })
        .catch(() => {
    });
  }

  //文字处理（换行空格等）
  stringChange(detailContent){
      if (detailContent != null && detailContent.indexOf('\n') != -1){
          //字符串拆分从\n开始
          let headString = detailContent.split('\n')[0] + '\n';
          let footString = detailContent.split('\n')[1].trim();
          // let str = headString + '\n' + '&emsp;&emsp;' + footString;
          this.setState({headContent:headString,footContent:footString,isContent:true})
      }else if(detailContent){
        this.setState({detailContent})
      }
  }
  render() {
    const { detailContent, headContent, footContent, publishDate, publisher, isContent } = this.state;
    return (
      <Container theme={myTheme} style={{ backgroundColor: '#FFFFFF' }}>
        <Content contentInsetAdjustmentBehavior="scrollableAxes">
            <Form style={styles.form}>
              {isContent ? <Text style={styles.contentText} selectable={true}>
              {headContent}&emsp;&emsp;{footContent}
              </Text> : <Text style={styles.contentText} selectable={true}>
                {detailContent}
              </Text>}
            <View style={styles.dateView}>
              <Text style={styles.publisherText} selectable={true}>
                {publisher}
              </Text>
            </View>
            <View style={styles.dateView}>
              <Text style={styles.dateText} selectable={true}>
                {!!publishDate && defaultFormat(publishDate)}
              </Text>
            </View>
          </Form>

        </Content>
      </Container>
    );
  }
}

BulletinContent.navigationOptions = ({ navigation }) => ({
  title: navigation.getParam('title'),
});

BulletinContent.propTypes = {
  getPublishNoticeRead: PropTypes.func.isRequired,
  workDetail: PropTypes.object,
};
BulletinContent.defaultProps = {
  workDetail: {},
};
const mapStateToProps = createStructuredSelector({
  isLoading: makeIsLoading(),
  refreshState: makeRefreshState(),
  workDetail: makeList(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getPublishNoticeRead :getPublishNoticeReadPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(BulletinContent);
