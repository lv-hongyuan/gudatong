import { createSelector } from 'reselect/es';

const selectBulletinContentDomain = () => state => state.bulletinContent;

const makeIsLoading = () => createSelector(selectBulletinContentDomain(), (subState) => {
  console.debug(subState.isLoading);
  return subState.isLoading;
});

const makeRefreshState = () => createSelector(selectBulletinContentDomain(), (subState) => {
  console.debug(subState.refreshState);
  return subState.refreshState;
});

const makeList = () => createSelector(selectBulletinContentDomain(), (subState) => {
  console.debug(subState);
  return subState.workDetail;
});

export { makeIsLoading, makeRefreshState, makeList };
