import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../ErrorHandler/actions';
import {getPublishNoticeReadRoutine} from "./actions";

function* getPublishNoticeRead(action) {
  console.log(action);
  try {
    yield put(getPublishNoticeReadRoutine.request());
    const response = yield call(request, ApiFactory.getPublishNoticeRead(action.payload));
    console.log('得到数据+++',response);
    yield put(getPublishNoticeReadRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    console.log('报错了');
    yield put(errorMessage(e.message));
    yield put(getPublishNoticeReadRoutine.failure(e));
  } finally {
    yield put(getPublishNoticeReadRoutine.fulfill());
  }
}

export function* getPublishNoticeReadSaga() {
  yield takeLatest(getPublishNoticeReadRoutine.TRIGGER, getPublishNoticeRead);
}

// All sagas to be loaded
export default [getPublishNoticeReadSaga];
