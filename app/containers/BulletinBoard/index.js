//******

  //公告栏页面

//****** *
import React from 'react';
import PropTypes from 'prop-types';
import {DeviceEventEmitter, Keyboard, SafeAreaView, Text, TouchableOpacity, Modal, ActivityIndicator} from 'react-native';
import { connect } from 'react-redux';
import {
  Button,
  Container, InputGroup,
  Item,
  Label,
  View,
} from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { getAnnouncementListPromise, getPublishNoticeReadPromise, getMultiplePromise, cleanSelectArrPromise, upDataMultiplePromise } from './actions';
import { ROUTE_BULLETIN_CONTENT } from '../../RouteConstant';
import { makeList, makeRefreshState,makeselectArr } from './selectors';
import myTheme from '../../Themes';
import screenHOC from '../screenHOC';
import AnnouncementItem from './components/AnnouncementItem';
import AlertView from '../../components/Alert';
import RefreshListView from '../../components/RefreshListView';
import DatePullSelector from '../../components/DatePullSelector';
import commonStyles from '../../common/commonStyles';
import { DATE_WHEEL_TYPE } from '../../components/DateTimeWheel';
import _ from "lodash";
import { ReadState, reviewReadFromState,} from "../../common/Constant";
import InputItem from "../../components/InputItem";
import Selector from "../../components/Selector";
import HeaderButtons from "react-navigation-header-buttons";
import Svg from "../../components/Svg";
import {NavigationActions} from "react-navigation";
import {makeLoading} from "./selectors";

const firstPageNum = 1;     //默认第一页
const DefaultPageSize = 20; //一次请求20条数据

const styles = {
  container: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignContent: 'stretch',
  },
  col: {
    backgroundColor: 'white',
    width: 80,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
    paddingBottom: 10,
    borderRightWidth: myTheme.borderWidth,
    borderBottomWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  touchCol: {
    backgroundColor: 'white',
    width: 80,
  },
  touchContent: {
    flex: 1,
    backgroundColor: 'white',
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
    paddingBottom: 10,
    borderRightWidth: myTheme.borderWidth,
    borderBottomWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  text: {
    color: myTheme.inputColor,
    textAlign: 'center',
    flex: 1,
  },
  searchInput: {
    borderWidth: 1,
    borderRadius: 4,
    borderColor: myTheme.borderColor,
    minHeight: 30,
    paddingLeft: 5,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  searchItem: {
    flex: 0,
    width: '49.5%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 0,
    minHeight: 30,
    paddingLeft: 5,
  },
  background: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.1)',
  },
};

@screenHOC
class BulletinBoard extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      pageSize: DefaultPageSize,  //展示行数
			page: firstPageNum,
      startTime: null,
      endTime: null,
      showFilter: false,
      reviewState: ReadState.allState,  //!默认读取状态
      inputReviewState: ReadState.allState,  //!默认读取状态显示的文字
      searchTitle:'',
      searchContent:'',
      inputSearchTitle: '',
      inputSearchContent:'',
      showMultiple: false,  //!是否开启了多选
      refresh: false,      //!当前状态为未读时，开启多选是否刷新列表
      indicatorAnimating: false, //!遮罩层
      defaultText: true,  //!底部按钮默认值  true为全选  false为取消全选
      unRead: 0
    };
  }

  componentDidMount() {
    this.listView.beginRefresh();
    this.props.navigation.setParams({
      exitSearch: () => {
        this.setShowFilter(false);
      },
    });

    this.props.navigation.setParams({
      openMultiple: (Multiple) => {
        this._openMultiple(Multiple);
      },
    });

    this.listener = DeviceEventEmitter.addListener('getPush',(param)=>{
      this.setState({ unRead: this.state.unRead - 1 })
      this.props.getPublishNoticeRead({
        id: param.id,
        isPush:param.state,
      })
        .then(()=>{console.log('已读状态刷新成功')})
        .catch(() => {});
    });

  }
  componentWillUnmount(){
    this.listener.remove();
  }
  onHeaderRefresh = () => {
    this.loadList(false);
  };

  onFooterRefresh = () => {
    this.loadList(true);
  };

  //点击查看公告
  onSelect = (item) => {
    if (!this.state.showMultiple) {
      //!未开启多选进入消息详情界面
      console.log('123456');
      
      this.props.navigation.navigate(
        ROUTE_BULLETIN_CONTENT,
        {
          id: item.id,
          item,
          title: item.title,
          isPush: 0,
        },
      );
    } else {
      //!开启多选后点击选中该数据
      item.state == 0 && this.props.getMultiplePromise({
        item,
        list: this.props.list,
        selectArr: this.props.selectArr
      })
        .then((data) => {
          if (data.selectArr.length < this.state.unRead) {
            this.setState({ defaultText: true })
          } else if (data.selectArr.length == this.state.unRead) {
            this.setState({ defaultText: false })
          }
        })
        .catch(() => { });
    }
  };

  //!点击右侧多选按钮
  _openMultiple(Multiple) {
    this.setState({
      showMultiple: !this.state.showMultiple,  //!开启多选后隐藏读取状态入口
      defaultText: true    //!重制全选按钮文字
    })
    this.props.navigation.setParams({
      showMultiple: !Multiple,  //!开启多选后改变右上角文字
      showBackButton: !Multiple //!开启多选后左侧返回按钮隐藏
    });
    this.props.cleanSelectArrPromise({      //!重制已经选中的数据 
      list: this.props.list,
      key: false,
      selectArr: this.props.selectArr
    }).catch((e) => { console.log('数据上传失败', e) })
  }
  
  //!多选标记为已读
  upDataMultiple(data) {
    this.setState({ indicatorAnimating: true })
    const ids = data.toString()
    this.props.upDataMultiplePromise({ ids })   //!上传所有选中数据
      .then(() => {
        this.loadList(false) //!刷新列表数据
        this.props.cleanSelectArrPromise({   //!清空已选数据
          list: this.props.list,
          key: false,
          selectArr: this.props.selectArr
        })
      })
      .then(() => {
        DeviceEventEmitter.emit('refreshCornerMarkState')
        this.props.navigation.setParams({
          showMultiple: true,  //!将右上角文字重制为多选
          showBackButton: true //!显示返回按钮
        });
        this.setState({
          showMultiple: false,  //!展示读取状态入口/隐藏下方按钮（全选/标记已读）
          defaultText: true
        })
      })
      .catch((e) => {
        this.setState({
          indicatorAnimating: false,
        })
      })
  }  

  //!全选数据
  selectDataAll() {
    if (this.state.defaultText) {
      this.props.cleanSelectArrPromise({
        list: this.props.list,
        key: true,
        selectArr: this.props.selectArr
      })
    } else {
      this.props.cleanSelectArrPromise({
        list: this.props.list,
        key: false,
        selectArr: this.props.selectArr
      })
    }

  }  
  loadList(loadMore) {
    const { page, pageSize } = this.state;
    this.props.getAnnouncementListPromise({
      page: loadMore ? page : 1,
			pageSize,
      fromDateTime: this.state.startTime,
      toDateTime: this.state.endTime,
      title: this.state.searchTitle,
      content: this.state.searchContent,
      state: this.state.reviewState,
      type: 10
    })
      .then(({page, list})=>{
        let unRead = 0
        for(var i = 0;i < list.length;i++) {
          if(list[i].state == 0) unRead++
        }
        this.setState({
          page: page + 1,
          indicatorAnimating: false,
          unRead:unRead
        });
      })
      .catch(() => {
        this.setState({
          indicatorAnimating: false,
        })  
      });
  }

  renderItem = ({ item }) => (
    <AnnouncementItem item={item} onSelect={this.onSelect} showMultiple={this.state.showMultiple}/>
  );

  renderFilter() {
    return this.state.showFilter && (
        <View style={[styles.container, {
          position: 'absolute', top: 0, left: 0, bottom: 0, right: 0,
        }]}
        >
          <SafeAreaView style={{ width: '100%' }}>
            <View style={{ padding: 10 }}>
              <InputGroup style={commonStyles.inputGroup}>
                <InputItem
                    autoFocus
                    label="查询标题:"
                    returnKeyType="search"
                    value={this.state.inputSearchTitle}
                    clearButtonMode="while-editing"
                    onChangeText={(text) => {
                      this.setState({ inputSearchTitle: text });
                    }}
                    onSubmitEditing={this.doSearch}
                    onFocus={() => {
                      this.setShowFilter(true);
                    }}
                />
              </InputGroup>
              <InputGroup style={commonStyles.inputGroup}>
                <InputItem
                    label="查询内容:"
                    returnKeyType="search"
                    value={this.state.inputSearchContent}
                    clearButtonMode="while-editing"
                    onChangeText={(text) => {
                      this.setState({ inputSearchContent: text });
                    }}
                    onSubmitEditing={this.doSearch}
                    onFocus={() => {
                      this.setShowFilter(true);
                    }}
                />
              </InputGroup>

              <InputGroup style={commonStyles.inputGroup}>
                <Item style={[commonStyles.inputItem]}
                >
                  <Label style={commonStyles.inputLabel}>起始时间:</Label>
                    <DatePullSelector
                        value={this.state.startTime}
                        type={DATE_WHEEL_TYPE.DATE}
                        onChangeValue={(value) => {
                        this.setState({ startTime: value }, () => {
                          // this.listView.beginRefresh();
                        });
                      }}
                    />
                </Item>
              </InputGroup>
              <InputGroup style={commonStyles.inputGroup}>
                <Item style={[commonStyles.inputItem]}
                >
                  <Label style={commonStyles.inputLabel}>截止时间:</Label>
                  <DatePullSelector
                      value={this.state.endTime}
                      type={DATE_WHEEL_TYPE.DATE}
                      onChangeValue={(value) => {
                        this.setState({ endTime: value }, () => {
                        // this.listView.beginRefresh();
                        });
                      }}
                  />
                </Item>
              </InputGroup>
              <InputGroup style={commonStyles.inputGroup}>
                <Item style={commonStyles.inputItem}>
                  <Label style={commonStyles.inputLabel}>读取状态:</Label>
                  <Selector
                      value={this.state.inputReviewState}
                      items={Object.values(ReadState)}
                      getItemValue={item => item}
                      getItemText={item => reviewReadFromState(item)}
                      onSelected={(item) => {
                        this.setState({ inputReviewState: item });
                      }}
                  />
                </Item>
              </InputGroup>
            </View>
            <Button
                onPress={this.doSearch}
                block
                style={{
                  height: 45, margin: 10, justifyContent: 'center', backgroundColor: '#DC001B',
                }}
            >
              <Text style={{ color: '#ffffff' }}>搜索</Text>
            </Button>
          </SafeAreaView>
        </View>
    );
  }
  setInputValue(callBack) {
    this.setState({
      inputSearchTitle: this.state.searchTitle,
      inputSearchContent: this.state.searchContent,
      inputReviewState: this.state.reviewState,
    }, callBack);
  }
  setShowFilter = (showFilter) => {
    this.props.navigation.setParams({
      showFilter,
    });
    this.setState({ showFilter });
  };
  //点击查询按钮刷新数据
  doSearch = () => {
    Keyboard.dismiss();
    this.setSearchValue();
    this.setShowFilter(false);
    this.listView.beginRefresh();
  };
  setSearchValue(callBack) {
    console.log('setSearchValue++++',this.state.inputSearchTitle,'!!',this.state.inputSearchContent,'@@@',this.state.inputReviewState)
    this.setState({
      searchTitle: this.state.inputSearchTitle,
      searchContent: this.state.inputSearchContent,
      reviewState: this.state.inputReviewState,
    }, callBack);
  }
  renderSearch() {
    return (
        <View style={styles.container}>
          <SafeAreaView style={{ width: '100%', flexDirection: 'row' }}>
            <View style={{ padding: 10, flex: 1 }}>
              <TouchableOpacity
                  activeOpacity={1}
                  style={styles.searchInput}
                  onPress={() => {
                    this.setInputValue();
                    this.setShowFilter(true);
                  }}
              >
                {!_.isEmpty(this.state.searchTitle) && (
                    <View style={styles.searchItem}>
                      <Label style={commonStyles.inputLabel}>查询标题:</Label>
                      <Text style={commonStyles.text}>{this.state.searchTitle}</Text>
                    </View>
                )}
                {!_.isEmpty(this.state.searchContent) && (
                    <View style={styles.searchItem}>
                      <Label style={commonStyles.inputLabel}>查询内容:</Label>
                      <Text style={commonStyles.text}>{this.state.searchContent}</Text>
                    </View>
                )}
                {!_.isEmpty(this.state.startTime) && (
                    <View style={styles.searchItem}>
                      <Label style={commonStyles.inputLabel}>起始时间:</Label>
                      <Text style={commonStyles.text}>{this.state.searchContent}</Text>
                    </View>
                )}
                {!_.isEmpty(this.state.endTime) && (
                    <View style={styles.searchItem}>
                      <Label style={commonStyles.inputLabel}>截止时间:</Label>
                      <Text style={commonStyles.text}>{this.state.searchContent}</Text>
                    </View>
                )}
                {!_.isNil(this.state.reviewState) && (
                    <View style={styles.searchItem}>
                      <Label style={commonStyles.inputLabel}>读取状态:</Label>
                      <Text style={commonStyles.text}>{reviewReadFromState(this.state.reviewState)}</Text>
                    </View>
                )}
                {/* {_.isEmpty(this.state.shipName) && _.isEmpty(this.state.voyageCode) && ( */}
                {/* <View style={[styles.searchItem, {width: '100%'}]}> */}
                {/* <Text style={commonStyles.text}>{'点击输入要搜索的船名/航次'}</Text> */}
                {/* </View> */}
                {/* )} */}
              </TouchableOpacity>
            </View>
          </SafeAreaView>
        </View>
    );
  }

  render() {
    return (
      <Container theme={myTheme} style={{ backgroundColor: '#E0E0E0' }}>
        {/*{this.renderFilter()}*/}
        {!this.state.showMultiple && this.renderSearch()}
        <RefreshListView
          ref={(ref) => { this.listView = ref; }}
          contentInsetAdjustmentBehavior="scrollableAxes"
          style={{ width: '100%' }}
          data={this.props.list || []}
          keyExtractor={item => `${item.id}`}
          renderItem={this.renderItem}
          ListHeaderComponent={() => (<View style={{ height: 5 }} />)}
          ItemSeparatorComponent={() => (<View style={{ height: 5 }} />)}
          refreshState={this.props.refreshState}
          onHeaderRefresh={this.onHeaderRefresh}
          onFooterRefresh={this.onFooterRefresh}
        />
        {this.state.showMultiple &&
          <View style={{
            width: '100%', height: 50, justifyContent: 'space-between',
            flexDirection: 'row', backgroundColor: '#eee'
          }}
          >
            <TouchableOpacity
              style={{
                backgroundColor: 'rgb(221,10,37)', width: '50%',
                height: 50, justifyContent: 'center', alignItems: 'center',
                borderRightWidth: 1, borderRightColor: '#FFF'
              }}
              activeOpacity={1}
              onPress={() => {
                //!全选按钮
                this.setState({ defaultText: !this.state.defaultText })
                this.selectDataAll()
              }}
            >
              <Text style={{ color: '#fff', fontSize: 17 }}>{this.state.defaultText ? '全选' : '取消全选'}</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={{
                backgroundColor: 'rgb(227,10,37)', height: 50, width: '50%',
                justifyContent: 'center', alignItems: 'center'
              }}
              onPress={() => {
                //!上传标记为已读的信息
                if (this.props.selectArr && this.props.selectArr.length > 0) {
                  this.upDataMultiple(this.props.selectArr)
                } else {
                  AlertView.show({
                    message: '至少选中一条',
                    confirmAction: () => { },
                  });
                }

              }}
            >
              <Text style={{ color: '#fff', fontSize: 17 }}>置为已读</Text>
            </TouchableOpacity>
          </View>
        }
        {this.renderFilter()}
        <Modal
          animationType="none"
          transparent={true}
          visible={this.state.indicatorAnimating}
        >
          <View style={styles.background}>
            <ActivityIndicator size="large" color={'rgb(221,65,50)'} />
          </View>
        </Modal>
      </Container>
    );
  }
}
//根据showFilter判断左边导航栏显示取消还是返回按钮
BulletinBoard.navigationOptions = ({navigation}) => ({
  title: '公告栏',
  headerLeft:    //!开启多选时隐藏左侧返回按钮
    <HeaderButtons>
      {navigation.getParam('showBackButton', true) ?
        navigation.getParam('showFilter', false) ? (   //!进入读取状态界面时左侧按钮显示为：‘取消’
          <HeaderButtons.Item
            title="取消"
            buttonStyle={{ fontSize: 14, color: '#ffffff' }}
            onPress={() => {
              const exitSearch = navigation.getParam('exitSearch');
              exitSearch();
            }}
          />
        ) : (
            <HeaderButtons.Item
              title=""
              buttonWrapperStyle={{ padding: 10 }}
              ButtonElement={<Svg icon="icon_back" size="20" color="white" />}
              onPress={() => {
                navigation.dispatch(NavigationActions.back());
              }}
            />
          ) : null}
    </HeaderButtons>
  ,
  headerRight: (    //!右侧多选按钮
    <HeaderButtons>
      {navigation.getParam('showFilter', false) ? null :
        navigation.getParam('showMultiple', true) ?
          <HeaderButtons.Item
            title="多选"
            buttonStyle={{ fontSize: 16, color: '#ffffff' }}
            onPress={() => {
              const openMultiple = navigation.getParam('openMultiple');
              openMultiple(true);
            }}
          /> :
          <HeaderButtons.Item
            title='取消'
            buttonStyle={{ fontSize: 16, color: '#ffffff' }}
            onPress={() => {
              const openMultiple = navigation.getParam('openMultiple');
              openMultiple(false);
            }}
          />
      }

    </HeaderButtons>
  ),
});

BulletinBoard.propTypes = {
  navigation: PropTypes.object.isRequired,
  getAnnouncementListPromise: PropTypes.func.isRequired,
  list: PropTypes.array,
  refreshState: PropTypes.number.isRequired,
  getPublishNoticeRead: PropTypes.func.isRequired,
};
BulletinBoard.defaultProps = {
  list: [],
  selectArr: []
};

const mapStateToProps = createStructuredSelector({
  refreshState: makeRefreshState(),
  list: makeList(),
  isLoading: makeLoading(),
  selectArr: makeselectArr(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getAnnouncementListPromise,
      getPublishNoticeRead: getPublishNoticeReadPromise,
      getMultiplePromise,
      cleanSelectArrPromise,
      upDataMultiplePromise
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(BulletinBoard);
