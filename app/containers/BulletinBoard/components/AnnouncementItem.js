import React from 'react';
import PropTypes from 'prop-types';
import {StyleSheet, TouchableHighlight, Dimensions, Image} from 'react-native';
import { Text, View } from 'native-base';
import myTheme from '../../../Themes';
import { defaultFormat } from '../../../utils/DateFormat';
import Svg from "../../../components/Svg";
import Ionicons from 'react-native-vector-icons/Ionicons';

let deviceWidth = Dimensions.get('window').width;
const styles = StyleSheet.create({
  container: {
    alignContent: 'stretch',
    backgroundColor: 'white',
      height:35,
  },
  contentContainer: {
    borderWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
  },
  text: {
    color: '#535353',
    padding: 5,
    width: deviceWidth-200,
    marginLeft: 5,
      fontSize: 14,
  },
  roundView: {
    width: 20,
    height: 20,
    // borderRadius: 2.5,
    // borderWidth:0,
    marginLeft: 5,
    // backgroundColor: 'red',
    marginTop:3,
  },
    arrowView: {
        width: 15,
        height:15,
    },
  noRoundView: {
    width: 20,
    height: 20,
    // borderRadius: 2.5,
    // borderWidth:0,
    marginLeft: 5,
  },
  dateText: {
    marginRight: 10,
    textAlign: 'right',
    color: '#535353',
      fontSize: 14,
  },
  leftView: {
    alignItems:'center',
    flexDirection: 'row',
    marginRight: 5,
  },
  dateView: {
    alignItems:'center',
  },
  Ionicons: {
    height: 20,
    marginRight:3.5,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

class AnnouncementItem extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      select:false
    };
  }

  onPressEvent=()=>{
    if (this.props.onSelect) {
      this.props.onSelect(this.props.item);
    }
  };
  componentWillReceiveProps(nextProps){
    this.setState({select:nextProps.item.select})
  }

  render() {
    //!multiple是否显示左方多选按钮
    const showMultiple = this.props.showMultiple    
    const { content, title, publishDate, state, isTop} = this.props.item || {};
    return (
        <TouchableHighlight 
          onPress={this.onPressEvent}
          activeOpacity={showMultiple ? 1 : 0.8}
        >
      <View style={styles.container}>
        <View style={{ flexDirection: 'row' ,flex:1,alignItems:'center',justifyContent:'space-between',}}>
          <View style={[styles.leftView]}>
            {/*{state == 0 ? (isTop == 0 ? <Svg icon="wd" size="20" style={[styles.roundView]}/> :<Svg icon="jiantou" size="19" style={[styles.noRoundView]}/>):null}*/}
            {/*{state != 0 ? (isTop == 0 ? <Svg icon="yd" size="20" style={[styles.roundView]}/> :<Svg icon="ydzd" size="19" style={[styles.noRoundView]}/>):null}*/}
            <View style={{height:35,width:15,}}>
            {isTop == 1 ? <Svg icon="zhiding-2" size="19" style={styles.arrowView}/> : <View style={styles.arrowView}></View>}
            </View>
            {showMultiple?
              <View style={styles.Ionicons}>
                <Ionicons
                  name={this.state.select ? 'ios-radio-button-on' : 'ios-radio-button-off'}
                  size={20}
                  color={state == 0 ? 'rgb(221,65,50)' :'rgb(190,190,190)'} >
                </Ionicons>
              </View>:
              state == 0 ? <Svg icon="wd" size="20" style={[styles.roundView]}/> : <Svg icon="yd" size="20" style={[styles.roundView]}/>
            }
            <View style={[styles.dateView]}>
              <Text style={[styles.text]} numberOfLines = {1}>
                {title}
              </Text>
            </View>
          </View>
          <Text style={[styles.dateText]} numberOfLines = {1}>
            {!!publishDate && defaultFormat(publishDate)}
          </Text>
        </View>
      </View>
        </TouchableHighlight>
    );
  }
}

AnnouncementItem.propTypes = {
  item: PropTypes.object.isRequired,
  onSelect: PropTypes.func,
};
AnnouncementItem.defaultProps = {
  onSelect: undefined,
  showMultiple:false,
};

export default AnnouncementItem;
