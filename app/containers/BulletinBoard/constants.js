/*
 *
 * BulletinBoard constants
 *
 */
export const GET_ANNOUNCEMENT_LIST = 'app/BulletinBoard/GET_ANNOUNCEMENT_LIST';
export const GET_PUBLISH_NOTICE_READ_ID = 'app/BulletinBoard/GET_PUBLISH_NOTICE_READ_ID';
export const GETMUITIPLE = 'app/bulletinBoad/GETMUITIPLE'; //!多选
export const CLEANSELECTARR = 'app/bulletinBorad/CLEANSELECTARR';  //!清空多选
export const UPDATEMULTIPLE = 'app/bulletinBorad/UPDATEMULTIPLE';  //!上传标记为已读的消息
