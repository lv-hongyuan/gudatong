import React from 'react';
import {BackHandler, View, StyleSheet,ToastAndroid } from 'react-native';
import { NavigationActions } from 'react-navigation';
import NavigatorService from '../NavigatorService';
// import Loading from '../components/LoadingCmponent'

const styles = StyleSheet.create({
  screen: {
    flex: 1,
  },
});

/*
 * 配合react-navigation使用
 * 定义了屏幕显示的生命周期，以及Android的返回按钮事件
 */
function screenHOC(WrappedComponent: React.Component) {
  class ScreenComponent extends WrappedComponent {
    // android点击返回按钮
    onBackPressed: Function;
    // 屏幕将要显示
    willFocus: Function;
    // 屏幕显示
    didFocus: Function;
    // 屏幕将要隐藏
    willBlur: Function;
    // 屏幕隐藏
    didBlur: Function;

    // android点击返回按钮
    _onBackPressed = () => {
      if (this.onBackPressed) {
        if (this.lastBackPressed && this.lastBackPressed + 2000 >= Date.now()) {
          //最近2秒内按过back键，可以退出应用。
          BackHandler.exitApp();//直接退出APP
        }else{
          this.lastBackPressed = Date.now();
          ToastAndroid.show('再按一次退出应用', 1000);//提示
          return true;
        }
      }
      NavigatorService.back();
      return true;
    };

    // 屏幕将要显示
    _willFocus = () => {
      this.willFocus && this.willFocus();
    };

    // 屏幕显示
    _didFocus = () => {
      BackHandler.addEventListener('hardwareBackPress', this._onBackPressed);
      this.didFocus && this.didFocus();
    };

    // 屏幕将要隐藏
    _willBlur = () => {
      BackHandler.removeEventListener('hardwareBackPress', this._onBackPressed);
      this.willBlur && this.willBlur();
    };

    // 屏幕隐藏
    _didBlur = () => {
      this.didBlur && this.didBlur();
    };

    // // 载入状态显示
    // showLoading (fullScreenLoading = false) {
    //   this.setState({_loading: true, _fullScreenLoading: fullScreenLoading})
    // }
    //
    // // 载入状态隐藏
    // hideLoading () {
    //   this.setState({_loading: false})
    // }
    //
    // renderLoading () {
    //   return !!super.renderLoading ? super.renderLoading() : <Loading visible={this.state._loading}/>
    // }
    //
    // renderScreenLoading () {
    //   return (!!this.state && this.state._loading && !this.state._fullScreenLoading) && this.renderLoading()
    // }
    //
    // renderFullScreenLoading () {
    //   return !!this.state && this.state._fullScreenLoading && (
    //     <Modal
    //       visible={this.props.isLoading}
    //       transparent={true}
    //       onRequestClose={() => {
    //       }}>
    //       {this.renderLoading()}
    //     </Modal>
    //   )
    // }

    componentDidMount() {
      super.componentDidMount && super.componentDidMount();
      this._willFocusSubscription = this.props.navigation.addListener('willFocus', this._willFocus);
      this._didFocusSubscription = this.props.navigation.addListener('didFocus', this._didFocus);
      this._willBlurSubscription = this.props.navigation.addListener('willBlur', this._willBlur);
      this._didBlurSubscription = this.props.navigation.addListener('didBlur', this._didBlur);
    }

    componentWillReceiveProps(nextProps, ...args) {
      super.componentWillReceiveProps && super.componentWillReceiveProps(nextProps, ...args);
      if (nextProps.isLoading !== this.props.isLoading) {

      }
    }

    componentWillUnmount() {
      super.componentWillUnmount && super.componentWillUnmount();
      this._willFocusSubscription.remove();
      this._didFocusSubscription.remove();
      this._willBlurSubscription.remove();
      this._didBlurSubscription.remove();
    }

    render() {
      return (
        <View style={styles.screen}>
          {super.render()}
          {/* {this.renderScreenLoading()} */}
          {/* {this.renderFullScreenLoading()} */}
        </View>
      );
    }
  }

  const { displayName, name } = WrappedComponent;
  const wrappedComponentName = displayName || name || 'Component';
  ScreenComponent.displayName = `Screen(${wrappedComponentName})`;

  return ScreenComponent;
}

export default screenHOC;
