import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import {
  // createSpecialBoxRecordRoutine,
  getSpecialBoxRecordRoutine,
  updateSpecialBoxRecordRoutine,
  deleteSpecialBoxRecordRoutine,
} from './actions';
import { errorMessage } from '../ErrorHandler/actions';
import { getPopId } from '../ShipWork/sagas';

// function* createSpecialBoxRecord(action) {
//   console.log(action);
//   try {
//     yield put(createSpecialBoxRecordRoutine.request());
//     const data = action.payload;
//     const response = yield call(request, ApiFactory.createSpecialBox(data));
//     yield put(createSpecialBoxRecordRoutine.success(response));
//   } catch (e) {
//     console.log(e.message);
//     yield put(errorMessage(e.message));
//     yield put(createSpecialBoxRecordRoutine.failure(e));
//   } finally {
//     yield put(createSpecialBoxRecordRoutine.fulfill());
//   }
// }
//
// export function* createSpecialBoxRecordSaga() {
//   yield takeLatest(createSpecialBoxRecordRoutine.TRIGGER, createSpecialBoxRecord);
// }

function* getSpecialBoxRecord(action) {
  console.log(action);
  try {
    yield put(getSpecialBoxRecordRoutine.request());
    const popId = yield call(getPopId);
    const response = yield call(request, ApiFactory.getSpecialBoxList(popId));
    console.log('getSpecialBoxRecord', response);
    yield put(getSpecialBoxRecordRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(getSpecialBoxRecordRoutine.failure(e));
  } finally {
    yield put(getSpecialBoxRecordRoutine.fulfill());
  }
}

export function* getSpecialBoxRecordSaga() {
  yield takeLatest(getSpecialBoxRecordRoutine.TRIGGER, getSpecialBoxRecord);
}

function* updateSpecialBoxRecord(action) {
  console.log(action);
  try {
    yield put(updateSpecialBoxRecordRoutine.request());
    const data = action.payload;
    const popId = yield call(getPopId);
    data.popid = popId;
    const response = yield call(request, ApiFactory.updateSpecialBox(data));
    yield put(updateSpecialBoxRecordRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(updateSpecialBoxRecordRoutine.failure(e));
  } finally {
    yield put(updateSpecialBoxRecordRoutine.fulfill());
  }
}

export function* updateSpecialBoxRecordSaga() {
  yield takeLatest(updateSpecialBoxRecordRoutine.TRIGGER, updateSpecialBoxRecord);
}

function* deleteSpecialBoxRecord(action) {
  console.log(action);
  try {
    yield put(deleteSpecialBoxRecordRoutine.request());
    const response = yield call(request, ApiFactory.deleteSpecialBox(action.payload));
    yield put(deleteSpecialBoxRecordRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(deleteSpecialBoxRecordRoutine.failure(e));
  } finally {
    yield put(deleteSpecialBoxRecordRoutine.fulfill());
  }
}

export function* deleteSpecialBoxRecordSaga() {
  yield takeLatest(deleteSpecialBoxRecordRoutine.TRIGGER, deleteSpecialBoxRecord);
}

// All sagas to be loaded
export default [
  // createSpecialBoxRecordSaga,
  getSpecialBoxRecordSaga,
  updateSpecialBoxRecordSaga,
  deleteSpecialBoxRecordSaga,
];
