import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  // CREATE_SPECIAL_BOX_ACTION,
  GET_SPECIAL_BOX_ACTION,
  UPDATE_SPECIAL_BOX_ACTION,
  DELETE_SPECIAL_BOX_ACTION,
} from './constants';

// export const createSpecialBoxRecordRoutine = createRoutine(
//   CREATE_SPECIAL_BOX_ACTION,
//   updates => updates,
//   () => ({ globalLoading: true }),
// );
// export const createSpecialBoxRecordPromise = promisifyRoutine(createSpecialBoxRecordRoutine);

export const getSpecialBoxRecordRoutine = createRoutine(
  GET_SPECIAL_BOX_ACTION,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const getSpecialBoxRecordPromise = promisifyRoutine(getSpecialBoxRecordRoutine);

export const updateSpecialBoxRecordRoutine = createRoutine(
  UPDATE_SPECIAL_BOX_ACTION,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const updateSpecialBoxRecordPromise = promisifyRoutine(updateSpecialBoxRecordRoutine);

export const deleteSpecialBoxRecordRoutine = createRoutine(
  DELETE_SPECIAL_BOX_ACTION,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const deleteSpecialBoxRecordPromise = promisifyRoutine(deleteSpecialBoxRecordRoutine);
