import {
  // createSpecialBoxRecordRoutine,
  getSpecialBoxRecordRoutine,
  updateSpecialBoxRecordRoutine,
  deleteSpecialBoxRecordRoutine,
} from './actions';

const initState = {
  success: false,
  data: undefined,
};

export default function (state = initState, action) {
  switch (action.type) {
    // case createSpecialBoxRecordRoutine.TRIGGER:
    //   return { ...state, loading: true };
    // case createSpecialBoxRecordRoutine.SUCCESS:
    //   return state;
    // case createSpecialBoxRecordRoutine.FAILURE:
    //   return { ...state, error: action.payload };
    // case createSpecialBoxRecordRoutine.FULFILL:
    //   return { ...state, loading: false };

    case getSpecialBoxRecordRoutine.TRIGGER:
      return { ...state, loading: true };
    case getSpecialBoxRecordRoutine.SUCCESS:
      return { ...state, data: action.payload };
    case getSpecialBoxRecordRoutine.FAILURE:
      return { ...state, error: action.payload };
    case getSpecialBoxRecordRoutine.FULFILL:
      return { ...state, loading: false };

    case updateSpecialBoxRecordRoutine.TRIGGER:
      return { ...state, loading: true };
    case updateSpecialBoxRecordRoutine.SUCCESS:
      return state;
    case updateSpecialBoxRecordRoutine.FAILURE:
      return { ...state, error: action.payload };
    case updateSpecialBoxRecordRoutine.FULFILL:
      return { ...state, loading: false };

    case deleteSpecialBoxRecordRoutine.TRIGGER:
      return { ...state, loading: true };
    case deleteSpecialBoxRecordRoutine.SUCCESS:
      return state;
    case deleteSpecialBoxRecordRoutine.FAILURE:
      return { ...state, error: action.payload };
    case deleteSpecialBoxRecordRoutine.FULFILL:
      return { ...state, loading: false };

    default:
      return state;
  }
}
