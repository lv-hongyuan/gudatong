import { createSelector } from 'reselect/es';

const selectServiceFeeDomain = () => state => state.specialBox;
const selectShipStateDomain = () => state => state.shipWork.data;

const makeSelectPortName = () => createSelector(
  selectShipStateDomain(),
  subState => subState.portName,
);

const makeSelectVoyageCode = () => createSelector(
  selectShipStateDomain(),
  subState => subState.voyageCode,
);
const makeSelectPopId = () => createSelector(
  selectShipStateDomain(),
  (subState) => {
    console.debug(subState.success);
    return subState.popId;
  },
);

const makeSelectData = () => createSelector(
  selectServiceFeeDomain(),
  subState => subState.data,
);

export {
  selectServiceFeeDomain,
  makeSelectPortName,
  makeSelectPopId,
  makeSelectVoyageCode,
  makeSelectData,
};
