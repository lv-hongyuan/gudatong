/*
 *
 * SpecialBox constants
 *
 */
// export const CREATE_SPECIAL_BOX_ACTION = 'app/SpecialBox/CREATE_SPECIAL_BOX_ACTION';

export const GET_SPECIAL_BOX_ACTION = 'app/SpecialBox/GET_SPECIAL_BOX_ACTION';

export const UPDATE_SPECIAL_BOX_ACTION = 'app/SpecialBox/UPDATE_SPECIAL_BOX_ACTION';

export const DELETE_SPECIAL_BOX_ACTION = 'app/SpecialBox/DELETE_SPECIAL_BOX_ACTION';

export const SpecialBoxType = [
  '超 长',
  '超 宽',
  '超 高',
  '超 重',
];
