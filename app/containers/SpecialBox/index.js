import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { Container, Content, Form, Item, Label, InputGroup } from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import HeaderButtons from 'react-navigation-header-buttons';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import _ from 'lodash';
import Selector from '../../components/Selector';
import { SpecialBoxType } from './constants';
import {
  makeSelectPortName,
  makeSelectVoyageCode,
  makeSelectData,
} from './selectors';
import {
  // createSpecialBoxRecordPromise,
  getSpecialBoxRecordPromise,
  updateSpecialBoxRecordPromise,
  deleteSpecialBoxRecordPromise,
} from './actions';
import commonStyles from '../../common/commonStyles';
import stringToNumber from '../../utils/stringToNumber';
import InputItem from '../../components/InputItem';
import myTheme from '../../Themes';
import screenHOC from '../screenHOC';
import { MaskType } from '../../components/InputItem/TextInput';

const styles = StyleSheet.create({
  form: {
    backgroundColor: '#FFFFFF',
    padding: 10,
    marginBottom: 10,
  },
  title: {
    paddingLeft: 15,
    paddingBottom: 10,
    paddingTop: 5,
    fontSize: 14,
    color: 'red',
    borderBottomWidth: myTheme.borderWidth,
    borderColor: '#c9c9c9',
  },
  item: {
    borderBottomColor: 'transparent',
    minHeight: 50,
  },
  bottomButtonContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: '#ffffff',
  },
});

const ShowModal = {
  IDLE: 0,
  CREATE: 1,
  EDIT: 2,
};

/**
 * 特殊箱记录
 * Created by jianzhexu on 2018/3/26
 */
@screenHOC
class SpecialBox extends React.PureComponent {
  static navigationOptions = ({ navigation }) => ({
    title: '特殊箱记录',
    headerRight: (
      <HeaderButtons>
        <HeaderButtons.Item
          title="保存"
          buttonStyle={{ fontSize: 14, color: '#ffffff' }}
          onPress={() => {
            const handleSubmitFunc = navigation.getParam('submit', undefined);
            if (handleSubmitFunc) {
              requestAnimationFrame(() => {
                handleSubmitFunc();
              });
            }
          }}
        />
      </HeaderButtons>
    ),
  });

  static propTypes = {
    navigation: PropTypes.object.isRequired,
    // createSpecialBoxRecordPromise: PropTypes.func.isRequired,
    getSpecialBoxRecordPromise: PropTypes.func.isRequired,
    updateSpecialBoxRecordPromise: PropTypes.func.isRequired,
    // deleteSpecialBoxRecordPromise: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      id: undefined,
      s20xl: undefined,
      s40xl: undefined,
      sType: undefined,
      rXl: undefined,
      d20xl: undefined,
      d40xl: undefined,
      showModal: ShowModal.IDLE,
    };
    this.props.navigation.setParams({ submit: this.handleSubmit });
  }

  componentDidMount() {
    this.props.getSpecialBoxRecordPromise()
      .then((data) => {
        // 有数据
        this.setState({ ...data, showModal: !_.isNil(data.id) ? ShowModal.EDIT : ShowModal.CREATE });
      })
      .catch(() => {});
  }

  handleSubmit = () => {
    const param = {
      s20xl: stringToNumber(this.state.s20xl) || 0,
      s40xl: stringToNumber(this.state.s40xl) || 0,
      sType: this.state.sType,
      d20xl: stringToNumber(this.state.d20xl) || 0,
      d40xl: stringToNumber(this.state.d40xl) || 0,
      rXl: stringToNumber(this.state.rXl) || 0,
    };
    // if (this.state.showModal === ShowModal.CREATE) {
    //   this.props.createSpecialBoxRecordPromise(param)
    //     .then(() => {
    //       this.props.navigation.goBack();
    //     })
    //     .catch(() => {});
    // } else if (this.state.showModal === ShowModal.EDIT) {
    if (!_.isNil(this.state.id)) {
      param.id = this.state.id;
    } else {
      param.id = 0;
    }
    this.props.updateSpecialBoxRecordPromise(param)
      .then(() => {
        this.props.navigation.goBack();
      })
      .catch(() => {});
    // } else {
    //   this.props.navigation.goBack();
    // }
  };

  // deleteServiceFee = () => {
  //   this.props.deleteSpecialBoxRecordPromise(this.state.id)
  //     .then(() => {
  //       this.props.navigation.goBack();
  //     })
  //     .catch(() => {});
  // };

  render() {
    const showDelete = this.state.showModal === ShowModal.EDIT;
    const contentInset = {
      top: 0, left: 0, bottom: showDelete ? 40 : 0, right: 0,
    };
    return (
      <Container>
        <Content
          contentInset={contentInset}
          contentInsetAdjustmentBehavior="scrollableAxes"
        >
          <Form style={styles.form}>
            <Label style={styles.title}>特种箱</Label>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="小箱数量:"
                value={this.state.s20xl}
                onChangeText={(text) => {
                  this.setState({ s20xl: text });
                }}
                keyboardType="numeric"
                maskType={MaskType.INTEGER}
              />
              <InputItem
                label="大箱数量:"
                value={this.state.s40xl}
                onChangeText={(text) => {
                  this.setState({ s40xl: text });
                }}
                keyboardType="numeric"
                maskType={MaskType.INTEGER}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup} paddingBottom={10}>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>特箱类型:</Label>
                <Selector
                  value={this.state.sType}
                  items={SpecialBoxType}
                  pickerTitle="请选择类型"
                  getItemValue={item => item}
                  getItemText={item => item}
                  // pickerType="popover"
                  onSelected={(item) => {
                    this.setState({ sType: item });
                  }}
                />
              </Item>
            </InputGroup>
          </Form>
          <Form style={styles.form}>
            <Label style={styles.title}>冷藏箱</Label>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="箱量:"
                value={this.state.rXl}
                onChangeText={(text) => {
                  this.setState({ rXl: stringToNumber(text) || 0 });
                }}
                keyboardType="numeric"
                maskType={MaskType.INTEGER}
              />
            </InputGroup>
          </Form>
          <Form style={styles.form}>
            <Label style={styles.title}>危险品</Label>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="小箱数量:"
                value={this.state.d20xl}
                onChangeText={(text) => {
                  this.setState({ d20xl: stringToNumber(text) || 0 });
                }}
                keyboardType="numeric"
                maskType={MaskType.INTEGER}
              />
              <InputItem
                label="大箱数量:"
                value={this.state.d40xl}
                onChangeText={(text) => {
                  this.setState({ d40xl: stringToNumber(text) || 0 });
                }}
                keyboardType="numeric"
                maskType={MaskType.INTEGER}
              />
            </InputGroup>
          </Form>
        </Content>
        {/* {showDelete && ( */}
        {/* <SafeAreaView style={styles.bottomButtonContainer}> */}
        {/* <Button */}
        {/* onPress={() => { */}
        {/* this.deleteServiceFee(); */}
        {/* }} */}
        {/* full */}
        {/* > */}
        {/* <Text>清除记录</Text> */}
        {/* </Button> */}
        {/* </SafeAreaView> */}
        {/* )} */}
      </Container>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  portName: makeSelectPortName(),
  voyageCode: makeSelectVoyageCode(), // 航次
  data: makeSelectData(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      // createSpecialBoxRecordPromise,
      getSpecialBoxRecordPromise,
      updateSpecialBoxRecordPromise,
      deleteSpecialBoxRecordPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SpecialBox);
