import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_HELP_LIST } from './constants';

export const getHelpListRoutine = createRoutine(GET_HELP_LIST);
export const getHelpListPromise = promisifyRoutine(getHelpListRoutine);
