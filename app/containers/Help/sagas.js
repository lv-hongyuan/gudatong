import { call, put, takeLatest } from 'redux-saga/effects';
import _ from 'lodash';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../ErrorHandler/actions';
import { getHelpListRoutine } from './actions';

function* getHelpList(action) {
  console.log(action);
  const { page, pageSize, ...rest } = (action.payload || {});
  try {
    yield put(getHelpListRoutine.request());
    const param = {
      filter: {
        logic: 'and',
        filters: [],
      },
      page,
      pageSize,
    };
    Object.keys(rest).forEach((key) => {
      const value = rest[key];
      if (!_.isEmpty(value)) {
        param.filter.filters.push({
          field: key,
          operator: 'eq',
          value,
        });
      }
    });
    const response = yield call(request, ApiFactory.getHandbookList(param));
    console.log(response);
    const { totalElements, content } = (response.dtoList || {});
    yield put(getHelpListRoutine.success({
      page, pageSize, list: (content || []), totalElements,
    }));
  } catch (error) {
    console.log(error.message);
    yield put(errorMessage(error.message));
    yield put(getHelpListRoutine.failure({ page, pageSize, error }));
  } finally {
    yield put(getHelpListRoutine.fulfill());
  }
}

export function* getHelpListSaga() {
  yield takeLatest(getHelpListRoutine.TRIGGER, getHelpList);
}

// All sagas to be loaded
export default [getHelpListSaga];
