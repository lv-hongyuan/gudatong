import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Keyboard, SafeAreaView, StyleSheet, TouchableOpacity } from 'react-native';
import { createStructuredSelector } from 'reselect/es';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { Button, Container, InputGroup, Item, Text, View } from 'native-base';
import { NavigationActions } from 'react-navigation';
import HeaderButtons from 'react-navigation-header-buttons';
import _ from 'lodash';
import { makeSelectRefreshState, makeHelpList } from './selectors';
import myTheme from '../../Themes';
import RefreshListView from '../../components/RefreshListView';
import screenHOC from '../screenHOC';
import HelpItem from './components/HelpItem';
import {
  getHelpListPromise,
} from './actions';
import commonStyles from '../../common/commonStyles';
import InputItem from '../../components/InputItem';
import Svg from '../../components/Svg';
import { ROUTE_HELP_DETAIL } from '../../RouteConstant';

const firstPageNum = 1;
const DefaultPageSize = 10;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignContent: 'stretch',
    borderBottomWidth: myTheme.borderWidth,
    borderBottomColor: myTheme.borderColor,
  },
  searchInput: {
    borderWidth: 1,
    borderRadius: 4,
    borderColor: myTheme.borderColor,
    minHeight: 30,
    paddingLeft: 5,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  searchItem: {
    flex: 0,
    width: '49.5%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 0,
    minHeight: 30,
    paddingLeft: 5,
  },
});

@screenHOC
class Help extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      wordName: '',
      inputWordName: '',
      page: firstPageNum,
      pageSize: DefaultPageSize,
      displayMode: true,
    };
  }

  componentDidMount() {
    this.onFooterRefresh();
    this.props.navigation.setParams({
      exitSearch: () => {
        this.changeDisplayMode(true);
      },
    });
  }

  // didFocus = () => {
  //   // 每次进页面刷新列表
  //   this.onFooterRefresh()
  // }

  onHeaderRefresh = () => {
    // 开始上拉翻页
    this.loadList(false);
  };

  onFooterRefresh = () => {
    // 开始下拉刷新
    this.loadList(true);
  };

  setSearchValue(callBack) {
    this.setState({
      wordName: this.state.inputWordName,
    }, callBack);
  }

  setInputValue(callBack) {
    this.setState({
      inputWordName: this.state.wordName,
    }, callBack);
  }

  changeDisplayMode = (displayMode) => {
    this.props.navigation.setParams({
      displayMode,
    });
    this.setState({ displayMode });
  };

  showPopDetail = (item) => {
    this.props.navigation.navigate(ROUTE_HELP_DETAIL, {
      id: item.id,
    });
  };

  loadList(loadMore: boolean) {
    const {
      wordName,
      page,
      pageSize,
    } = this.state;
    this.props.getHelpListPromise({
      wordName,
      page: loadMore ? page : firstPageNum,
      pageSize,
      loadMore,
    })
      .then(({ page }) => {
        this.setState({ page: page + 1 });
      })
      .catch(() => {});
  }

  renderItem = ({ item }) => (
    <HelpItem
      item={item}
      onPress={this.showPopDetail}
    />
  );

  renderFilter() {
    return (
      <View style={styles.container}>
        {this.state.displayMode ? (
          <SafeAreaView style={{ width: '100%', flexDirection: 'row' }}>
            <View style={{ padding: 10, flex: 1 }}>
              <TouchableOpacity
                activeOpacity={1}
                style={styles.searchInput}
                onPress={() => {
                  this.setInputValue();
                  this.changeDisplayMode(false);
                }}
              >
                {!_.isEmpty(this.state.wordName) && (
                  <View style={styles.searchItem}>
                    <Text style={commonStyles.text}>{this.state.wordName}</Text>
                  </View>
                )}
                {_.isEmpty(this.state.wordName) && (
                  <Item
                    style={[commonStyles.inputItem, { borderBottomWidth: 0 }]}
                    onPress={() => {
                      this.setInputValue();
                      this.changeDisplayMode(false);
                    }}
                  >
                    <Text style={commonStyles.text}>点击输入要搜索的关键字</Text>
                  </Item>
                )}
              </TouchableOpacity>
            </View>
          </SafeAreaView>
        ) : (
          <SafeAreaView style={{ width: '100%' }}>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                autoFocus
                label="关键字:"
                returnKeyType="search"
                value={this.state.inputWordName}
                clearButtonMode="while-editing"
                onChangeText={(text) => {
                  this.setState({ inputWordName: text });
                }}
                onSubmitEditing={() => {
                  Keyboard.dismiss();
                  this.setSearchValue();
                  this.changeDisplayMode(true);
                  this.listView.beginRefresh();
                }}
                onFocus={() => {
                  this.changeDisplayMode(false);
                }}
              />
            </InputGroup>
            <Button
              onPress={() => {
                Keyboard.dismiss();
                this.setSearchValue();
                this.changeDisplayMode(true);
                this.setState({
                  displayMode: true,
                }, () => {
                  this.listView.beginRefresh();
                });
              }}
              block
              style={{
                height: 45, margin: 10, justifyContent: 'center', backgroundColor: '#DC001B',
              }}
            >
              <Text style={{ color: '#ffffff' }}>搜索</Text>
            </Button>
          </SafeAreaView>
        )}
      </View>
    );
  }

  render() {
    const sepLine = () => <View style={{ height: myTheme.borderWidth, backgroundColor: myTheme.borderColor }} />;
    return (
      <Container theme={myTheme} style={{ backgroundColor: '#E0E0E0' }}>
        {this.renderFilter()}
        <SafeAreaView style={{ flex: 1 }}>
          <RefreshListView
            ref={(ref) => { this.listView = ref; }}
            style={{ width: '100%' }}
            data={this.props.list || []}
            keyExtractor={(item, index) => `${index}`}
            renderItem={this.renderItem}
            refreshState={this.props.refreshState}
            onHeaderRefresh={this.onHeaderRefresh}
            onFooterRefresh={this.onFooterRefresh}
            ListHeaderComponent={sepLine}
            ItemSeparatorComponent={sepLine}
          />
          {!this.state.displayMode && (
            <View style={{
              position: 'absolute', width: '100%', height: '100%', backgroundColor: '#ffffff',
            }}
            />
          )}
        </SafeAreaView>
      </Container>
    );
  }
}

Help.navigationOptions = ({ navigation }) => ({
  title: '帮助手册',
  headerLeft: (
    <HeaderButtons color="white">
      {navigation.getParam('displayMode', true) ? (
        <HeaderButtons.Item
          title=""
          buttonWrapperStyle={{ padding: 10 }}
          ButtonElement={<Svg icon="icon_back" size="20" color="white" />}
          onPress={() => {
            navigation.dispatch(NavigationActions.back());
          }}
        />
      ) : (
        <HeaderButtons.Item
          title="取消"
          buttonStyle={{ fontSize: 14, color: '#ffffff' }}
          onPress={() => {
            const exitSearch = navigation.getParam('exitSearch');
            exitSearch();
          }}
        />
      )}
    </HeaderButtons>
  ),
});

Help.defaultProps = {
  list: [],
};

Help.propTypes = {
  navigation: PropTypes.object.isRequired,
  getHelpListPromise: PropTypes.func.isRequired,
  list: PropTypes.array,
  refreshState: PropTypes.number.isRequired,
};

const mapStateToProps = createStructuredSelector({
  list: makeHelpList(),
  refreshState: makeSelectRefreshState(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getHelpListPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Help);
