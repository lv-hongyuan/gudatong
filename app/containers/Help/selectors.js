import { createSelector } from 'reselect/es';

const selectHelpDomain = () => state => state.help;

const makeHelpList = () =>
  createSelector(selectHelpDomain(), (subState) => {
    console.debug(subState);
    return subState.list;
  });

const makeSelectRefreshState = () =>
  createSelector(selectHelpDomain(), (subState) => {
    console.debug(subState);
    return subState.refreshState;
  });

export {
  makeHelpList,
  makeSelectRefreshState,
};
