import { getHelpListRoutine } from './actions';
import { RefreshState } from '../../components/RefreshListView';

const defaultState = {
  list: [],
  refreshState: RefreshState.Idle,
};

export default function (state = defaultState, action) {
  switch (action.type) {
    // 获取帮助列表（带分页）
    case getHelpListRoutine.TRIGGER: {
      const { loadMore } = action.payload;
      return {
        ...state,
        loading: true,
        refreshState: loadMore ? RefreshState.FooterRefreshing : RefreshState.HeaderRefreshing,
      };
    }
    case getHelpListRoutine.SUCCESS: {
      const { page, pageSize, list } = action.payload;
      return {
        ...state,
        list: page === 1 ? list : state.list.concat(list),
        refreshState: list.length < pageSize ? RefreshState.NoMoreData : RefreshState.Idle,
      };
    }
    case getHelpListRoutine.FAILURE: {
      const { page, error } = action.payload;
      return {
        ...state, list: page === 1 ? [] : state.list, error, refreshState: RefreshState.Failure,
      };
    }
    case getHelpListRoutine.FULFILL: {
      return { ...state, loading: false };
    }

    default:
      return state;
  }
}
