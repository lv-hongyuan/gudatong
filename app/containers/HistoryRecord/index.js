import React from 'react';
import PropTypes from 'prop-types';
import { Text, StyleSheet } from 'react-native';
import { createStructuredSelector } from 'reselect/es';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { makeList, makeRefreshState } from './selectors';
import { getHistoryRecordListPromise } from './actions';
import { connect } from 'react-redux';
import HistoryRecordItem from './components/HistoryRecordItem';
import RefreshListView from '../../components/RefreshListView';
import { Container, View, } from 'native-base';
import myTheme from '../../Themes';
import screenHOC from '../screenHOC';
import _ from "lodash";
import HeaderButtons from "react-navigation-header-buttons";
import Svg from "../../components/Svg";
import { NavigationActions } from "react-navigation";
import { ROUTE_HISTORY_RECORD_DETAIL} from '../../RouteConstant';
const styles = StyleSheet.create({
    title: {
        height: 35,
        width: '100%',
        backgroundColor: '#fff',
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 0.5,
        borderBottomColor: '#eee',
    },
    titletext: {
        borderRightColor: '#eee',
        borderRightWidth: 1,
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    }
})

const firstPageNum = 1;
const DefaultPageSize = 10;

@screenHOC
class HistoryRecordView extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            pageSize: DefaultPageSize,  //展示行数
            page: firstPageNum,
        };
    }

    componentDidMount() {
        this.listView.beginRefresh();
    }
    componentWillUnmount() {
    }

    loadList(loadMore = false) {
        const { page, pageSize } = this.state;
        this.props.getHistoryRecordListPromise({ page: loadMore ? page : 1, pageSize })
            .then(({ page }) => {
                this.setState({ page: page + 1 });
            })
            .catch(() => {
            });
    }

    // 上拉加载
    onHeaderRefresh = () => {
        this.loadList(false);
    };

    // 下拉刷新
    onFooterRefresh = () => {
        this.loadList(true)
    }

    renderItem = ({ item }) => {
        return (
            <HistoryRecordItem
                item={item}
                onSelect={this.onSelect}
            />
        )
    };

    onSelect = (item) => {
        let num = item.opTime.replace(/[^0-9]/ig,"");
        console.log(num);
        this.props.navigation.navigate(
            ROUTE_HISTORY_RECORD_DETAIL,
            {
                id: item.id,
                title:num
            },
        );
    };

    render() {
        return (
            <Container theme={myTheme} style={{backgroundColor:'#E0E0E0'}}>
                <View style={styles.title}>
                    <View style={[styles.titletext, { width: '20%' }]}><Text style={{color: myTheme.inputColor,}}>航次</Text></View>
                    <View style={[styles.titletext, { width: '40%' }]}><Text style={{color: myTheme.inputColor,}}>航线</Text></View>
                    <View style={[styles.titletext, { width: '40%', borderRightWidth: 0 }]}><Text style={{color: myTheme.inputColor}}>提交时间</Text></View>
                </View>
                <RefreshListView
                    ref={(ref) => { this.listView = ref; }}
                    data={this.props.list || []}
                    keyExtractor={item => `${item.id}`}
                    renderItem={this.renderItem}
                    refreshState={this.props.refreshState}
                    onHeaderRefresh={this.onHeaderRefresh}
                    onFooterRefresh={this.onFooterRefresh}
                />
            </Container>
        );
    }
}


HistoryRecordView.navigationOptions = ({ navigation }) => ({
    title: '历史船报',
    headerLeft: (
        <HeaderButtons>
            <HeaderButtons.Item
                title=""
                buttonWrapperStyle={{ padding: 10 }}
                ButtonElement={<Svg icon="icon_back" size="20" color="white" />}
                onPress={() => {
                    navigation.dispatch(NavigationActions.back());
                }}
            />
        </HeaderButtons>
    ),
});

HistoryRecordView.propTypes = {
    navigation: PropTypes.object.isRequired,
    getHistoryRecordListPromise: PropTypes.func.isRequired,
    list: PropTypes.array,
    refreshState: PropTypes.number.isRequired,
};
HistoryRecordView.defaultProps = {
    list: [],
};

const mapStateToProps = createStructuredSelector({
    refreshState: makeRefreshState(),
    list: makeList(),
});

function mapDispatchToProps(dispatch) {
    return {
        ...bindPromiseCreators({
            getHistoryRecordListPromise,
        }, dispatch),
        dispatch,
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(HistoryRecordView);
