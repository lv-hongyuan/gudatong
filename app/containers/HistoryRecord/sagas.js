import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../ErrorHandler/actions';
import { getHistoryRecordListRoutine } from './actions';

function* HistoryRecord(action) {
    console.log(action);
    const { page, pageSize } = (action.payload || {});

    try {
        yield put(getHistoryRecordListRoutine.request());
        const param = {
            filter: {
                logic: 'and',
                filters: [],
            },
            page,
            pageSize,
        };
        console.log('param:', param)
        const response = yield call(request, ApiFactory.HistoryRecord(param));
        // console.log('列表数据：', response);
        yield put(getHistoryRecordListRoutine.success({
            page, 
            pageSize,
            list:(response.dtoList ? response.dtoList.content : []),
        }));
    } catch (e) {
        console.log(e.message);
        yield put(errorMessage(e.message));
        yield put(getHistoryRecordListRoutine.failure({ page, pageSize, e }));
    } finally {
        yield put(getHistoryRecordListRoutine.fulfill());
    }
}

export function* getHistoryRecordListSaga() {
    yield takeLatest(getHistoryRecordListRoutine.TRIGGER, HistoryRecord);
}

// All sagas to be loaded
export default [getHistoryRecordListSaga];
