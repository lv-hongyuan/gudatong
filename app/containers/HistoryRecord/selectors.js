import { createSelector } from 'reselect/es';

const historyRecordDomain = () => state => state.historyRecord;

const makeList = () => createSelector(historyRecordDomain(), (subState) => {
  console.debug(subState);
  return subState.list;
});

const makeRefreshState = () => createSelector(historyRecordDomain(), (subState) => {
  console.debug(subState.refreshState);
  return subState.refreshState;
});


export { makeList, makeRefreshState};