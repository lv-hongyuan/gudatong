import React from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, StyleSheet } from 'react-native';
import { Text, View } from 'native-base';
import CarouselLabel from '../../../common/CarouselLabel';
import myTheme from '../../../Themes';
const styles = StyleSheet.create({
    items: {
        flexDirection: 'row',
        borderBottomWidth: 0.5,
        borderBottomColor: '#eee',
        alignItems: 'center',
        borderTopWidth: 0.5,
        borderTopColor: '#eee',
        backgroundColor:'#fff'
    },
    text: {
        borderRightWidth: 1,
        borderRightColor: '#eee',
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
    }
});

class HistoryRecordItem extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {};
    }

    onPressEvent = () => {
        if (this.props.onSelect) {
            this.props.onSelect(this.props.item);
        }
    };

    render() {
        const {
            voyageCode,     //  航次 --> 1911s
            line,           //  航线 --> 上海-【广州】
            opTime,         //  提交时间
            isTimely        //  录入时间是否过期
        } = (this.props.item || {});
        const textcolor = isTimely == 1 ? '#e33' : myTheme.inputColor
        return (
            <TouchableOpacity
                activeOpacity={0.5}
                style={styles.items}
                onPress={() => {
                    this.props.onSelect(this.props.item);
                }}
            >
                <View style={[styles.text, { width: '20%' }]}><Text style={{ color: textcolor }}>{voyageCode}</Text></View>
                <View style={[styles.text, { width: '40%',paddingHorizontal: 5, }]}>
                    {
                        line.length > 11 ? <CarouselLabel
                            bgViewStyle={{
                                overflow: 'hidden',
                                width: '100%',
                            }}
                            textContainerHeight={30}
                            speed={45}
                            text={line ? line : '暂无数据......'}
                            textStyle={{
                                fontSize: 13,
                                color: textcolor,
                                lineHeight: 30,
                                width: line.length * 17,
                            }}
                        /> :
                            <Text style={{color: textcolor}}>{line}</Text>
                    }
                </View>
                <View style={{ width: '40%', alignItems: 'center', }}><Text style={{ color: textcolor }}>{opTime}</Text></View>
            </TouchableOpacity>
        );
    }
}

HistoryRecordItem.propTypes = {
    item: PropTypes.object.isRequired,
    onSelect: PropTypes.func,
};
HistoryRecordItem.defaultProps = {
    onSelect: undefined,
};

export default HistoryRecordItem;
