import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_HISTORY_RECORD_LIST } from './constants';

export const getHistoryRecordListRoutine = createRoutine(GET_HISTORY_RECORD_LIST);
export const getHistoryRecordListPromise = promisifyRoutine(getHistoryRecordListRoutine);