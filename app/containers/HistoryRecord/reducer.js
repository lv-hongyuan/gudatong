import { getHistoryRecordListRoutine } from './actions';
import fixIdList from '../../utils/fixIdList';
import { RefreshState } from '../../components/RefreshListView';
import _ from "lodash";

const initState = {
  loading: false,
  list: [],
  refreshState: RefreshState.Idle,
};

export default function (state = initState, action) {
  switch (action.type) {
    case getHistoryRecordListRoutine.TRIGGER:{
      const { page } = action.payload;
      return { 
        ...state, 
        loading: true, 
        refreshState: page > 1 ? RefreshState.FooterRefreshing : RefreshState.HeaderRefreshing, 
      };
    }
    case getHistoryRecordListRoutine.SUCCESS:{
      const { page, pageSize, list } = action.payload;
      console.log('列表数据：',fixIdList(action.payload))
      return { 
        ...state, 
        list: page == 1 ? fixIdList(list) : fixIdList(state.list.concat(list)),
        refreshState: list.length < pageSize ? RefreshState.NoMoreData : RefreshState.Idle
      };
    }
    case getHistoryRecordListRoutine.FAILURE:{
      const { page, error } = action.payload;
      return {
        ...state, 
        list: page === 1 ? [] : state.list,
        error, 
        refreshState: RefreshState.Failure,
      };
    }
      
    case getHistoryRecordListRoutine.FULFILL:
      return { ...state, loading: false };

    default:
      return state;
  }
}
