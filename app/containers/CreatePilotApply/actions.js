import {
  CREATE_PILOT_APPLY_ACTION,
  CREATE_PILOT_APPLY_ACTION_RESULT,
  UPDATE_PILOT_APPLY_ACTION,
  UPDATE_PILOT_APPLY_ACTION_RESULT,
} from './constants';

export function createPilotApplyAction(data) {
  return {
    type: CREATE_PILOT_APPLY_ACTION,
    payload: data,
  };
}

export function createPilotApplyResultAction(data, success) {
  return {
    type: CREATE_PILOT_APPLY_ACTION_RESULT,
    payload: {
      data,
      success,
    },
  };
}

export function updatePilotApplyAction(data) {
  return {
    type: UPDATE_PILOT_APPLY_ACTION,
    payload: data,
  };
}

export function updatePilotApplyResultAction(success) {
  return {
    type: UPDATE_PILOT_APPLY_ACTION_RESULT,
    payload: {
      success,
    },
  };
}

