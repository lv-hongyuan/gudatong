import {
  CREATE_PILOT_APPLY_ACTION_RESULT,
  UPDATE_PILOT_APPLY_ACTION_RESULT,
  CREATE_PILOT_APPLY_ACTION,
  UPDATE_PILOT_APPLY_ACTION,
} from './constants';

const initState = {
  success: false,
};

export default function (state = initState, action) {
  switch (action.type) {
    case CREATE_PILOT_APPLY_ACTION:
    case UPDATE_PILOT_APPLY_ACTION:
      return { ...state, success: false };
    case CREATE_PILOT_APPLY_ACTION_RESULT:
    case UPDATE_PILOT_APPLY_ACTION_RESULT:
      return { ...state, success: action.payload.success };
    default:
      return state;
  }
}
