import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { Container, Content, Item, Label, InputGroup, View } from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import HeaderButtons from 'react-navigation-header-buttons';
import { makeSelectCreatePilotApplySuccess, makeSelectPortName, makeSelectPopId } from './selectors';
import CommonDataPickerDialog from '../../components/CommonDataPickerDialog';
import Switch from '../../components/Switch';
import { createPilotApplyAction, updatePilotApplyAction } from './actions';
import commonStyles from '../../common/commonStyles';
import InputItem from '../../components/InputItem';
import SelectButton from '../../components/SelectButton';
import screenHOC from '../screenHOC';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    paddingLeft: 20,
    paddingTop: 10,
    paddingBottom: 10,
    paddingRight: 20,
  },
});

/**
 *
 * 创建编辑引水记录
 * Created by jianzhexu on 2018/4/3
 */
@screenHOC
class CreatePilotApply extends React.PureComponent {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.getParam('title', '编辑引水记录'),
    headerRight: (
      <HeaderButtons>
        <HeaderButtons.Item
          title="保存"
          buttonStyle={{ fontSize: 14, color: '#ffffff' }}
          onPress={() => {
            const handleSubmitFunc = navigation.getParam('submit', undefined);
            if (handleSubmitFunc) {
              requestAnimationFrame(() => {
                handleSubmitFunc();
              });
            }
          }}
        />
      </HeaderButtons>
    ),
  });

  static propTypes = {
    navigation: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    success: PropTypes.object.isRequired,
    popId: PropTypes.number.isRequired,
    portName: PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      useWay: 10,
      isInit: false,
      applyType: 1,
      applyReasons: '',
      portName: '',
      isUpdating: false,
      showUseWay: false,
      id: 0,
      popId: 0,
    };
    this.props.navigation.setParams({ submit: this.handleSubmit });
  }

  componentWillMount() {
    this.setState({
      isInit: true,
    });
  }

  componentWillReceiveProps(nextProps) {
    const data = nextProps.navigation.getParam('data', undefined);
    if (data && this.state.isInit) {
      this.setState({
        useWay: data.useWay,
        isInit: false,
        applyType: data.applyType,
        applyReasons: data.applyReasons,
        portName: data.portName,
        id: data.id,
        popId: data.popId,
      });
    }

    if (nextProps.success && this.state.isUpdating) {
      const goBackCallback = this.props.navigation.getParam('onGoBack', undefined);
      if (goBackCallback) {
        goBackCallback();
      }
      this.props.navigation.goBack();
      this.setState({
        isUpdating: false,
      });
    }
  }

  handleSubmit = () => {
    this.setState({
      isUpdating: true,
    }, () => {
      const type = this.props.navigation.getParam('type', undefined);

      if (type) {
        this.props.dispatch(updatePilotApplyAction({
          popId: this.state.popId,
          portName: this.state.portName,
          useWay: this.state.useWay,
          applyType: this.state.applyType,
          applyReasons: this.state.applyReasons,
          id: this.state.id,
        }));
      } else {
        this.props.dispatch(createPilotApplyAction({
          popId: this.props.popId,
          portName: this.props.portName,
          useWay: this.state.useWay,
          applyType: this.state.applyType,
          applyReasons: this.state.applyReasons,
        }));
      }
    });
  };
  renderUseWay = () => (<CommonDataPickerDialog
    title="选择使用途径"
    data={[{ name: '离泊', value: 20 }, { name: '靠泊', value: 10 }]}
    value={this.state.useWay}
    onItemPress={(value) => {
      this.setState({
        useWay: value.value,
        showUseWay: false,
      });
    }}
    onClose={() => {
      this.setState({
        showUseWay: false,
      });
    }}
    visible={this.state.showUseWay}
  />);

  render() {
    return (
      <Container>
        <Content>
          {this.renderUseWay()}
          <View style={styles.container}>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="发生港口:"
                value={this.state.portName ? this.state.portName : this.props.portName}
                placeholder=""
                editable={false}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>使用途径:</Label>
                <SelectButton
                  value={this.state.useWay === 10 ? '靠泊' : '离泊'}
                  onPress={() => {
                    this.setState({
                      showUseWay: true,
                    });
                  }}
                />
              </Item>
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <Item style={[commonStyles.inputItem, { borderBottomWidth: 0 }]}>
                <Label style={commonStyles.inputLabel}>是否强制:</Label>
                <Switch
                  onPress={() => {
                    this.setState(state => (
                      { applyType: state.applyType === 1 ? 2 : 1 }
                    ));
                  }}
                  value={this.state.applyType === 2}
                />
              </Item>
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="申请原因:"
                value={this.state.applyReasons}
                editable={!this.state.readonly}
                onChangeText={(text) => {
                  this.setState({ applyReasons: text });
                }}
              />
            </InputGroup>
          </View>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  success: makeSelectCreatePilotApplySuccess(),
  portName: makeSelectPortName(),
  popId: makeSelectPopId(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CreatePilotApply);
