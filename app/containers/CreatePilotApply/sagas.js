import { call, put, takeLatest } from 'redux-saga/effects';
import { CREATE_PILOT_APPLY_ACTION, UPDATE_PILOT_APPLY_ACTION } from './constants';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import { createPilotApplyResultAction, updatePilotApplyResultAction } from './actions';
import { errorMessage } from '../ErrorHandler/actions';

function* createPilotApply(action) {
  console.log(action);
  try {
    console.log(action);
    const response = yield call(request, ApiFactory.createApplyDiv(action.payload));
    console.log(response);
    yield put(createPilotApplyResultAction(response, true));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(createPilotApplyResultAction(undefined, false));
  }
}

export function* createPilotApplySaga() {
  yield takeLatest(CREATE_PILOT_APPLY_ACTION, createPilotApply);
}

function* updatePilotApply(action) {
  console.log(action);
  try {
    const response = yield call(request, ApiFactory.updateApplyDiv(action.payload));
    yield put(updatePilotApplyResultAction(response, true));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(updatePilotApplyResultAction(undefined, false));
  }
}

export function* updatePilotApplySaga() {
  yield takeLatest(UPDATE_PILOT_APPLY_ACTION, updatePilotApply);
}

// All sagas to be loaded
export default [
  createPilotApplySaga,
  updatePilotApplySaga,
];
