import { createSelector } from 'reselect/es';

const selectCreatePilotApplyDomain = () => state => state.createPilotApply;
const selectShipStateDomain = () => state => state.shipWork.data;

const makeSelectCreatePilotApplySuccess = () => createSelector(
  selectCreatePilotApplyDomain(),
  (subState) => {
    console.debug(subState.success);
    return subState.success;
  },
);

const makeSelectPortName = () => createSelector(
  selectShipStateDomain(),
  subState => subState.portName,
);

const makeSelectPopId = () => createSelector(
  selectShipStateDomain(),
  (subState) => {
    console.debug(subState.success);
    return subState.popId;
  },
);
export {
  makeSelectCreatePilotApplySuccess,
  selectCreatePilotApplyDomain,
  makeSelectPortName,
  makeSelectPopId,
};
