/*
 *
 * CreateVoyageEvent constants
 *
 */
export const CREATE_VOYAGE_EVENT_ACTION = 'app/CreateVoyageEvent/CREATE_VOYAGE_EVENT_ACTION';
export const UPDATE_VOYAGE_EVENT_ACTION = 'app/CreateVoyageEvent/UPDATE_VOYAGE_EVENT_ACTION';
export const GET_VOYAGE_EVENT_TYPE_ACTION = 'app/CreateVoyageEvent/GET_VOYAGE_EVENT_TYPE_ACTION';
