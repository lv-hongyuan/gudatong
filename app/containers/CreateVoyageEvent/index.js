import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Container, Content, Item, Label, InputGroup } from 'native-base';
import { SafeAreaView, View } from 'react-native';
import { createStructuredSelector } from 'reselect/es';
import HeaderButtons from 'react-navigation-header-buttons';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import _ from 'lodash';
import { ShipEventRange } from '../../common/Constant';
import Selector from '../../components/Selector';
import {
  createVoyageEventPromise,
  updateVoyageEventPromise,
  getVoyageEventTypePromise,
} from './actions';
import {
  makeVoyageEventTypes,
  makeIsEdit,
  makeOnoBack,
} from './selectors';
import { makeSelectPopId } from '../ShipWork/selectors';
import { errorMessage } from '../ErrorHandler/actions';
import commonStyles from '../../common/commonStyles';
import InputItem from '../../components/InputItem';
import stringToNumber from '../../utils/stringToNumber';
import DatePullSelector from '../../components/DatePullSelector';
import screenHOC from '../screenHOC';
import CoordinateInput from '../../components/CoordinateInput';

/**
 * 停航原因
 * Created by jianzhexu on 2018/3/23
 */
@screenHOC
class CreateVoyageEvent extends React.PureComponent {
  constructor(props) {
    super(props);

    const data = props.navigation.getParam('data', {});
    this.state = {
      popId: this.props.popId,
      id: data.id,
      longD: data.longD,
      longF: data.longF,
      latD: data.latD,
      latF: data.latF,
      hidePlace: data.hidePlace,
      startTime: data.startTime, // 事发时间
      endTime: data.endTime, // 结束时间
      typeChildCode: data.typeChildCode, // 事件子类型
      typeChildText: data.typeChildText, // 事件子类型
      describe: data.describe, // 事件描述
      dbId: data.dbId,
    };
  }

  componentDidMount() {
    this.props.navigation.setParams({ submit: this.handleSubmit });
    if (!this.props.isEdit) {
      this.props.getVoyageEventTypes()
        .then(() => {}).catch(() => {});
    }
  }

  onBtnType = () => {
    if (this.props.types.length > 0) {
      this.selector.showPicker();
    } else {
      this.props.getVoyageEventTypes()
        .then(() => {
          this.selector.showPicker();
        }).catch(() => {});
    }
  };

  onChangeType = (item) => {
    if (this.state.typeChildCode !== item.code) {
      this.setState({
        typeChildCode: item.code,
        typeChildText: item.text,
      });
    }
  };

  handleSubmit = () => {
    const param = {
      dbId: this.state.dbId,
      id: this.state.id,
      popId: this.state.popId,
      longD: stringToNumber(this.state.longD),
      longF: stringToNumber(this.state.longF),
      latD: stringToNumber(this.state.latD),
      latF: stringToNumber(this.state.latF),
      startTime: this.state.startTime,
      endTime: this.state.endTime,
      hidePlace: this.state.hidePlace,
      typeChildCode: this.state.typeChildCode,
      typeChildText: this.state.typeChildText,
      describe: this.state.describe,
      range: ShipEventRange.SHIP_SAILING,
    };
    if (_.isNil(param.latD)) {
      this.props.dispatch(errorMessage('请填写纬度'));
      return;
    }
    if (_.isNil(param.longD)) {
      this.props.dispatch(errorMessage('请填写经度'));
      return;
    }
    if((param.latD + param.latF / 60 > 90) || (param.latF >= 60)){
      this.props.dispatch(errorMessage('纬度输入有误！'));
      return;
    }
    if((param.longD + param.longF / 60 > 180) || (param.longF >= 60)){
      this.props.dispatch(errorMessage('经度输入有误！'));
      return;
    }
    if (_.isNil(param.typeChildCode)) {
      this.props.dispatch(errorMessage('请选择事件类型'));
      return;
    }
    if (_.isNil(param.hidePlace)) {
      this.props.dispatch(errorMessage('请抛填写锚位置'));
      return;
    }
    if (param.typeChildText === '其他原因' && _.isEmpty(param.describe)) {
      this.props.dispatch(errorMessage('请填写事件描述'));
      return;
    }
    if (!param.startTime) {
      this.props.dispatch(errorMessage('请填写事发时间'));
      return;
    }
    if (param.endTime && param.endTime < param.startTime) {
      this.props.dispatch(errorMessage('结束时间不能早于开始时间'));
      return;
    }
    if (this.props.isEdit) {
      this.props.updateVoyageEvent(param).then(() => {
        if (this.props.onGoBack) this.props.onGoBack();
        this.props.navigation.goBack();
      }).catch(() => {});
    } else {
      param.popId = this.props.popId;
      this.props.createVoyageEvent(param).then(() => {
        if (this.props.onGoBack) this.props.onGoBack();
        this.props.navigation.goBack();
      }).catch(() => {});
    }
  };

  render() {
    return (
      <Container>
        <Content>
          <SafeAreaView style={{ backgroundColor: '#FFFFFF' }}>
            <View style={{
              paddingLeft: 20,
              paddingTop: 10,
              paddingBottom: 10,
              paddingRight: 20,
            }}
            >
              <InputGroup style={commonStyles.inputGroup}>
                <Item style={commonStyles.inputItem}>
                  <Label style={commonStyles.inputLabel}>纬度:</Label>
                  <CoordinateInput
                    type="la"
                    style={commonStyles.input}
                    degrees={this.state.latD}
                    points={this.state.latF}
                    onChangeValue={(degrees, points) => {
                      this.setState({ latD: degrees, latF: points });
                    }}
                  />
                </Item>
                <Item style={commonStyles.inputItem}>
                  <Label style={commonStyles.inputLabel}>经度:</Label>
                  <CoordinateInput
                    type="lo"
                    style={commonStyles.input}
                    degrees={this.state.longD}
                    points={this.state.longF}
                    onChangeValue={(degrees, points) => {
                      this.setState({ longD: degrees, longF: points });
                    }}
                  />
                </Item>
              </InputGroup>
              <InputGroup style={commonStyles.inputGroup}>
                <Item style={commonStyles.inputItem}>
                  <Label
                    style={this.props.isEdit ? commonStyles.readOnlyInputLabel : commonStyles.inputLabel}
                  >事件类型:
                  </Label>
                  <Selector
                    value={this.state.typeChildText}
                    items={this.props.types || []}
                    disabled={this.props.isEdit}
                    ref={(ref) => {
                      this.selector = ref;
                    }}
                    onPress={this.onBtnType}
                    pickerTitle="请选择事件类型"
                    getItemValue={item => item.text}
                    getItemText={item => item.text}
                    onSelected={this.onChangeType}
                  />
                </Item>
              </InputGroup>
              <InputGroup style={commonStyles.inputGroup}>
                <InputItem
                  label="抛锚位置:"
                  value={this.state.hidePlace}
                  editable={!this.state.readonly}
                  onChangeText={(text) => {
                    this.setState({ hidePlace: text });
                  }}
                />
              </InputGroup>
              <InputGroup style={commonStyles.inputGroup}>
                <InputItem
                  label="事件描述:"
                  value={this.state.describe}
                  editable={!this.state.readonly}
                  onChangeText={(text) => {
                    this.setState({ describe: text });
                  }}
                />
              </InputGroup>
              <InputGroup style={commonStyles.inputGroup}>
                <Item style={commonStyles.inputItem}>
                  <Label style={commonStyles.inputLabel}>开始时间:</Label>
                  <DatePullSelector
                    value={this.state.startTime || null}
                    onChangeValue={(value) => {
                      this.setState({ startTime: value });
                    }}
                  />
                </Item>
              </InputGroup>
              <InputGroup style={commonStyles.inputGroup}>
                <Item style={commonStyles.inputItem}>
                  <Label style={commonStyles.inputLabel}>结束时间:</Label>
                  <DatePullSelector
                    value={this.state.endTime || null}
                    minValue={this.state.startTime}
                    onChangeValue={(value) => {
                      this.setState({ endTime: value });
                    }}
                  />
                </Item>
              </InputGroup>
            </View>
          </SafeAreaView>
        </Content>
      </Container>
    );
  }
}

CreateVoyageEvent.navigationOptions = ({ navigation }) => ({
  title: navigation.getParam('title', '创建停航原因'),
  headerRight: (
    <HeaderButtons>
      <HeaderButtons.Item
        title="保存"
        buttonStyle={{ fontSize: 14, color: '#ffffff' }}
        onPress={() => {
          const handleSubmitFunc = navigation.getParam('submit', undefined);
          if (handleSubmitFunc) {
            requestAnimationFrame(() => {
              handleSubmitFunc();
            });
          }
        }}
      />
    </HeaderButtons>
  ),
});

CreateVoyageEvent.propTypes = {
  navigation: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
  getVoyageEventTypes: PropTypes.func.isRequired,
  updateVoyageEvent: PropTypes.func.isRequired,
  createVoyageEvent: PropTypes.func.isRequired,
  popId: PropTypes.number.isRequired,
  isEdit: PropTypes.bool.isRequired,
  types: PropTypes.array,
  onGoBack: PropTypes.func,
};

CreateVoyageEvent.defaultProps = {
  types: [],
  onGoBack: undefined,
};

const mapStateToProps = createStructuredSelector({
  popId: makeSelectPopId(),
  types: makeVoyageEventTypes(),
  isEdit: makeIsEdit(),
  onGoBack: makeOnoBack(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      createVoyageEvent: createVoyageEventPromise,
      updateVoyageEvent: updateVoyageEventPromise,
      getVoyageEventTypes: getVoyageEventTypePromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateVoyageEvent);
