import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  CREATE_VOYAGE_EVENT_ACTION,
  UPDATE_VOYAGE_EVENT_ACTION,
  GET_VOYAGE_EVENT_TYPE_ACTION,
} from './constants';

export const createVoyageEventRoutine = createRoutine(
  CREATE_VOYAGE_EVENT_ACTION,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const createVoyageEventPromise = promisifyRoutine(createVoyageEventRoutine);

export const updateVoyageEventRoutine = createRoutine(
  UPDATE_VOYAGE_EVENT_ACTION,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const updateVoyageEventPromise = promisifyRoutine(updateVoyageEventRoutine);

export const getVoyageEventTypeRoutine = createRoutine(
  GET_VOYAGE_EVENT_TYPE_ACTION,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const getVoyageEventTypePromise = promisifyRoutine(getVoyageEventTypeRoutine);
