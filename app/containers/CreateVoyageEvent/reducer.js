import {
  createVoyageEventRoutine,
  updateVoyageEventRoutine,
  getVoyageEventTypeRoutine,
} from './actions';

const initState = {
  emergency: undefined,
  isLoading: false,
  loadingError: undefined,
  navBack: false,
  types: [],
  subTypes: [],
};

export default function (state = initState, action) {
  switch (action.type) {
    // 获取突发事件类型
    case getVoyageEventTypeRoutine.TRIGGER:
      return { ...state, isLoading: true, loadingError: undefined };
    case getVoyageEventTypeRoutine.SUCCESS:
      return { ...state, types: action.payload };
    case getVoyageEventTypeRoutine.FAILURE:
      return { ...state, types: [], loadingError: action.payload };
    case getVoyageEventTypeRoutine.FULFILL:
      return { ...state, isLoading: false };

    // 创建突发事件
    case createVoyageEventRoutine.TRIGGER:
      return { ...state, isLoading: true, loadingError: undefined };
    case createVoyageEventRoutine.SUCCESS:
      return { ...state };
    case createVoyageEventRoutine.FAILURE:
      return { ...state };
    case createVoyageEventRoutine.FULFILL:
      return { ...state, isLoading: false };

    // 更新突发事件
    case updateVoyageEventRoutine.TRIGGER:
      return { ...state, isLoading: true, loadingError: undefined };
    case updateVoyageEventRoutine.SUCCESS:
      return { ...state };
    case updateVoyageEventRoutine.FAILURE:
      return { ...state };
    case updateVoyageEventRoutine.FULFILL:
      return { ...state, isLoading: false };

    default:
      return state;
  }
}
