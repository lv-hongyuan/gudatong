import { call, put, select, takeLatest } from 'redux-saga/effects';
import { ShipEventRange } from '../../common/Constant';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import {
  createVoyageEventRoutine,
  updateVoyageEventRoutine,
  getVoyageEventTypeRoutine,
} from './actions';
import { errorMessage } from '../ErrorHandler/actions';
import realm from '../../common/realm';
import ShipInfoEventTypes from '../../common/ShipInfoEventTypes';

const selectConnected = state => state.network.isConnected;

function* createVoyageEvent(action) {
  console.log(action);
  try {
    yield put(createVoyageEventRoutine.request());
    const {
      popId,
      longD,
      longF,
      latD,
      latF,
      startTime,
      endTime,
      hidePlace,
      typeChildCode,
      typeChildText,
      range,
      describe,
    } = action.payload;
    const isConnected = yield select(selectConnected);

    if (isConnected) {
      const response = yield call(
        request,
        ApiFactory.createShipEvent({
          popId,
          longD,
          longF,
          latD,
          latF,
          startTime,
          endTime,
          hidePlace,
          typeChildCode, // 事件子类型
          typeChildText, // 事件子类型
          range,
          describe, // 事件描述
        }),
      );
      console.log('createVoyageEvent', response.toString());
      yield put(createVoyageEventRoutine.success(response));
    } else {
      realm.write(() => {
        // Create a OfflineVoyageEvent object
        const nextId = (realm.objects('OfflineVoyageEvent').max('id') || 0) + 1;
        realm.create('OfflineVoyageEvent', {
          id: nextId,
          data: JSON.stringify({
            popId,
            longD,
            longF,
            latD,
            latF,
            startTime,
            endTime,
            hidePlace,
            typeChildCode, // 事件子类型
            typeChildText, // 事件子类型
            range,
            describe, // 事件描述
          }),
          date: new Date().getTime(),
        });
      });
      yield put(createVoyageEventRoutine.success({}));
    }
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(createVoyageEventRoutine.failure(e));
  } finally {
    yield put(createVoyageEventRoutine.fulfill());
  }
}

export function* createVoyageEventSaga() {
  yield takeLatest(createVoyageEventRoutine.TRIGGER, createVoyageEvent);
}

function* updateVoyageEvent(action) {
  console.log(action);
  try {
    yield put(updateVoyageEventRoutine.request());
    const {
      popId,
      longD,
      longF,
      latD,
      latF,
      startTime,
      endTime,
      hidePlace,
      typeChildCode,
      typeChildText,
      range,
      describe,
      id,
      dbId,
    } = action.payload;

    const offLineTask = Number.isInteger(dbId) && realm.objects('OfflineVoyageEvent').filtered(`id = ${dbId}`)[0];

    const isConnected = yield select(selectConnected);

    if (isConnected) {
      const response = yield call(
        request,
        ApiFactory.updateShipEvent({
          popId,
          longD,
          longF,
          latD,
          latF,
          startTime,
          endTime,
          hidePlace,
          typeChildCode,
          typeChildText,
          range,
          describe,
          id,
        }),
      );
      console.log('updateVoyageEvent', response.toString());
      yield put(updateVoyageEventRoutine.success(response));
      if (offLineTask) {
        realm.write(() => {
          realm.delete(offLineTask);
        });
      }
    } else {
      if (offLineTask) {
        realm.write(() => {
          offLineTask.data = JSON.stringify({
            popId,
            longD,
            longF,
            latD,
            latF,
            startTime,
            endTime,
            hidePlace,
            typeChildCode,
            typeChildText,
            range,
            describe,
            id,
          });
        });
      }
      yield put(updateVoyageEventRoutine.success());
    }
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(updateVoyageEventRoutine.failure(e));
  } finally {
    yield put(updateVoyageEventRoutine.fulfill());
  }
}

export function* updateVoyageEventSaga() {
  yield takeLatest(updateVoyageEventRoutine.TRIGGER, updateVoyageEvent);
}

function* getVoyageEventType(action) {
  console.log(action);
  try {
    yield put(getVoyageEventTypeRoutine.request());
    const isConnected = yield select(selectConnected);
    if (isConnected) {
      const response = yield call(
        request,
        ApiFactory.findShipEventTypes(ShipEventRange.SHIP_SAILING),
      );
      console.log('getVoyageEventType', response.toString());
      yield put(getVoyageEventTypeRoutine.success(response.dtoList));
    } else {
      yield put(getVoyageEventTypeRoutine.success(ShipInfoEventTypes ));
    }
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(getVoyageEventTypeRoutine.failure(e));
  } finally {
    yield put(getVoyageEventTypeRoutine.fulfill());
  }
}

export function* getVoyageEventTypeSaga() {
  yield takeLatest(getVoyageEventTypeRoutine.TRIGGER, getVoyageEventType);
}

// All sagas to be loaded
export default [
  createVoyageEventSaga,
  updateVoyageEventSaga,
  getVoyageEventTypeSaga,
];
