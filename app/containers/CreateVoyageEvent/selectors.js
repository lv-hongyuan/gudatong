import { createSelector } from 'reselect/es';

const selectCreateVoyageEventDomain = () => state => state.createVoyageEvent;

const makeVoyageEventTypes = () => createSelector(selectCreateVoyageEventDomain(), (subState) => {
  console.log(subState);
  return subState.types || [];
});

const makeIsEdit = () => createSelector(
  (state, props) => props.navigation.state.params,
  (subState) => {
    console.debug(subState.isEdit);
    return !!subState.isEdit;
  },
);

const makeOnoBack = () => createSelector(
  (state, props) => props.navigation.state.params,
  (subState) => {
    console.debug(subState.onGoBack);
    return subState.onGoBack;
  },
);

export {
  makeVoyageEventTypes,
  makeIsEdit,
  makeOnoBack,
};
