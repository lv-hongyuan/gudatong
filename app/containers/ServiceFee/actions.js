import {
  CREATE_SERVICE_FEE_ACTION,
  CREATE_SERVICE_FEE_ACTION_RESULT,
  GET_SERVICE_FEE_ACTION,
  GET_SERVICE_FEE_ACTION_RESULT,
  UPDATE_SERVICE_FEE_ACTION,
  UPDATE_SERVICE_FEE_ACTION_RESULT,
  DELETE_SERVICE_FEE_ACTION,
  DELETE_SERVICE_FEE_ACTION_RESULT,
} from './constants';

export function createServiceFeeAction(data) {
  return {
    type: CREATE_SERVICE_FEE_ACTION,
    payload: { data },
  };
}

export function createServiceFeeActionResultAction(data, success) {
  return {
    type: CREATE_SERVICE_FEE_ACTION_RESULT,
    payload: {
      data,
      success,
    },
  };
}

export function getServiceFeeAction(data) {
  return {
    type: GET_SERVICE_FEE_ACTION,
    payload: data,
  };
}

export function getServiceFeeActionResultAction(data, success) {
  return {
    type: GET_SERVICE_FEE_ACTION_RESULT,
    payload: {
      data,
      success,
    },
  };
}

export function updateServiceFeeAction(data, file) {
  return {
    type: UPDATE_SERVICE_FEE_ACTION,
    payload: { data, file },
  };
}

export function updateServiceFeeActionResultAction(data, success) {
  return {
    type: UPDATE_SERVICE_FEE_ACTION_RESULT,
    payload: {
      data,
      success,
    },
  };
}

export function deleteServiceFeeAction(data) {
  return {
    type: DELETE_SERVICE_FEE_ACTION,
    payload: data,
  };
}

export function deleteServiceFeeActionResultAction(data, success) {
  return {
    type: DELETE_SERVICE_FEE_ACTION_RESULT,
    payload: {
      data,
      success,
    },
  };
}
