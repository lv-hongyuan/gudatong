import {
  CREATE_SERVICE_FEE_ACTION_RESULT,
  GET_SERVICE_FEE_ACTION_RESULT,
  GET_SERVICE_FEE_ACTION,
  CREATE_SERVICE_FEE_ACTION,
  UPDATE_SERVICE_FEE_ACTION,
  DELETE_SERVICE_FEE_ACTION,
  UPDATE_SERVICE_FEE_ACTION_RESULT,
  DELETE_SERVICE_FEE_ACTION_RESULT,
} from './constants';

const initState = {
  success: false,
  data: {},
};

export default function (state = initState, action) {
  switch (action.type) {
    case CREATE_SERVICE_FEE_ACTION:
    case UPDATE_SERVICE_FEE_ACTION:
    case DELETE_SERVICE_FEE_ACTION:
    case GET_SERVICE_FEE_ACTION:
      return { ...state, success: false };
    case GET_SERVICE_FEE_ACTION_RESULT:
      return { ...state, success: action.payload.success, data: action.payload.data };
    case CREATE_SERVICE_FEE_ACTION_RESULT:
    case UPDATE_SERVICE_FEE_ACTION_RESULT:
    case DELETE_SERVICE_FEE_ACTION_RESULT:
      return { ...state, success: action.payload.success };
    default:
      return state;
  }
}
