import { call, put, takeLatest } from 'redux-saga/effects';
import {
  CREATE_SERVICE_FEE_ACTION,
  GET_SERVICE_FEE_ACTION,
  UPDATE_SERVICE_FEE_ACTION,
  DELETE_SERVICE_FEE_ACTION,
} from './constants';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import {
  createServiceFeeActionResultAction,
  deleteServiceFeeActionResultAction,
  getServiceFeeActionResultAction,
  updateServiceFeeActionResultAction,
} from './actions';
import { errorMessage } from '../ErrorHandler/actions';
import uploadImage from '../../common/uploadImage';
import { getPopId } from '../ShipWork/sagas';

function* createServiceFee(action) {
  console.log(action);
  try {
    const { data } = action.payload;
    const { stowagePic, feeApplayPic, pic } = data;
    if (stowagePic) {
      if (stowagePic.path) {
        const popId = yield call(getPopId);
        const response = yield call(uploadImage, stowagePic, 'stowagePic', popId);
        data.stowagePic = response.id;
      } else {
        data.stowagePic = stowagePic.substr(stowagePic.lastIndexOf('/') + 1);
      }
    }
    if (feeApplayPic) {
      if (feeApplayPic.path) {
        const popId = yield call(getPopId);
        const response = yield call(uploadImage, feeApplayPic, 'feeApplayPic', popId);
        data.feeApplayPic = response.id;
      } else {
        data.feeApplayPic = feeApplayPic.substr(feeApplayPic.lastIndexOf('/') + 1);
      }
    }
    if (pic) {
      if (pic.path) {
        const popId = yield call(getPopId);
        const response = yield call(uploadImage, pic, 'pic', popId);
        data.pic = response.id;
      } else {
        data.pic = pic.substr(pic.lastIndexOf('/') + 1);
      }
    }
    const response = yield call(request, ApiFactory.createApplyLaborFee(data));
    yield put(createServiceFeeActionResultAction(response, true));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(createServiceFeeActionResultAction(undefined, false));
  }
}

export function* createServiceFeeSaga() {
  yield takeLatest(CREATE_SERVICE_FEE_ACTION, createServiceFee);
}

function* getServiceFee(action) {
  console.log(action);
  try {
    const popId = yield call(getPopId);
    const response = yield call(request, ApiFactory.findApplyLaborFee(popId));
    yield put(getServiceFeeActionResultAction(response, true));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(getServiceFeeActionResultAction(undefined, false));
  }
}

export function* getServiceFeeSaga() {
  yield takeLatest(GET_SERVICE_FEE_ACTION, getServiceFee);
}

function* updateServiceFee(action) {
  console.log(action);
  try {
    const { data } = action.payload;
    const { stowagePic, pic } = data;
    if (stowagePic) {
      if (stowagePic.path) {
        const popId = yield call(getPopId);
        const response = yield call(uploadImage, stowagePic, 'stowagePic', popId);
        data.stowagePic = response.id;
      } else {
        data.stowagePic = stowagePic.substr(stowagePic.lastIndexOf('/') + 1);
      }
    }
    if (pic) {
      if (pic.path) {
        const popId = yield call(getPopId);
        const response = yield call(uploadImage, pic, 'pic', popId);
        data.pic = response.id;
      } else {
        data.pic = pic.substr(pic.lastIndexOf('/') + 1);
      }
    }
    const response = yield call(request, ApiFactory.updateApplyLaborFee(data));
    yield put(updateServiceFeeActionResultAction(response, true));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(updateServiceFeeActionResultAction(undefined, false));
  }
}

export function* updateServiceFeeSaga() {
  yield takeLatest(UPDATE_SERVICE_FEE_ACTION, updateServiceFee);
}

function* deleteServiceFee(action) {
  console.log(action);
  try {
    const response = yield call(request, ApiFactory.deleteApplyLaborFee(action.payload));
    yield put(deleteServiceFeeActionResultAction(response, true));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(deleteServiceFeeActionResultAction(undefined, false));
  }
}

export function* deleteServiceFeeSaga() {
  yield takeLatest(DELETE_SERVICE_FEE_ACTION, deleteServiceFee);
}

// All sagas to be loaded
export default [
  createServiceFeeSaga, getServiceFeeSaga, deleteServiceFeeSaga, updateServiceFeeSaga,
];
