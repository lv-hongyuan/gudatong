import { createSelector } from 'reselect/es';

const selectServiceFeeDomain = () => state => state.serviceFee;
const selectShipStateDomain = () => state => state.shipWork.data;

const makeSelectCreateServiceFeeSuccess = () => createSelector(
  selectServiceFeeDomain(),
  (subState) => {
    console.debug(subState.success);
    return subState.success;
  },
);

const makeSelectPortName = () => createSelector(
  selectShipStateDomain(),
  subState => subState.portName,
);

const makeSelectVoyageCode = () => createSelector(
  selectShipStateDomain(),
  subState => subState.voyageCode,
);
const makeSelectPopId = () => createSelector(
  selectShipStateDomain(),
  (subState) => {
    console.debug(subState.success);
    return subState.popId;
  },
);

const makeSelectData = () => createSelector(
  selectServiceFeeDomain(),
  subState => subState.data,
);
export {
  makeSelectCreateServiceFeeSuccess,
  selectServiceFeeDomain,
  makeSelectPortName,
  makeSelectPopId,
  makeSelectVoyageCode,
  makeSelectData,
};
