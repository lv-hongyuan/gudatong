import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, SafeAreaView } from 'react-native';
import { connect } from 'react-redux';
import { Button, Container, Content, Form, Item, Label, Text, InputGroup } from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import HeaderButtons from 'react-navigation-header-buttons';
import {
  makeSelectPopId,
  makeSelectPortName,
  makeSelectCreateServiceFeeSuccess,
  makeSelectVoyageCode,
  makeSelectData,
} from './selectors';
import { createServiceFeeAction, deleteServiceFeeAction, getServiceFeeAction, updateServiceFeeAction } from './actions';
import SelectImageView from '../../components/SelectImageView';
import commonStyles from '../../common/commonStyles';
import stringToNumber from '../../utils/stringToNumber';
import InputItem from '../../components/InputItem';
import myTheme from '../../Themes';
import screenHOC from '../screenHOC';
import { MaskType } from '../../components/InputItem/TextInput';

const styles = StyleSheet.create({
  form: {
    backgroundColor: '#FFFFFF',
    padding: 10,
    marginBottom: 10,
  },
  title: {
    paddingLeft: 15,
    paddingBottom: 10,
    paddingTop: 5,
    fontSize: 14,
    color: 'red',
    borderBottomWidth: myTheme.borderWidth,
    borderColor: '#c9c9c9',
  },
  item: {
    borderBottomColor: 'transparent',
    minHeight: 50,
  },
  bottomButtonContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: '#ffffff',
  },
});

/**
 * 劳务费申请
 * Created by jianzhexu on 2018/3/26
 */
@screenHOC
class ServiceFee extends React.PureComponent {
  static navigationOptions = ({ navigation }) => ({
    title: '劳务费申请',
    headerRight: (
      <HeaderButtons>
        <HeaderButtons.Item
          title="保存"
          buttonStyle={{ fontSize: 14, color: '#ffffff' }}
          onPress={() => {
            const handleSubmitFunc = navigation.getParam('submit', undefined);
            if (handleSubmitFunc) {
              requestAnimationFrame(() => {
                handleSubmitFunc();
              });
            }
          }}
        />
      </HeaderButtons>
    ),
  });

  static propTypes = {
    navigation: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    success: PropTypes.bool.isRequired,
    data: PropTypes.object.isRequired,
    voyageCode: PropTypes.string.isRequired,
    popId: PropTypes.number.isRequired,
    portName: PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      isInit: false,
      isCreating: false,
      isDeleting: false,
      isUpdating: false,
      id: undefined,
      con20: undefined,
      con40: undefined,
      longrod: undefined,
      shortrod: undefined,
      refrigerator: undefined,
      stowagePic: undefined,
      feeApplayPic: undefined,
      applyReasons: undefined,
      state: 0,
      applyMoney: undefined,
      applyCause: undefined,
      pic: undefined,
    };
    this.props.navigation.setParams({ submit: this.handleSubmit });
  }

  componentWillMount() {
    this.setState({
      isInit: true,
    }, () => {
      this.props.dispatch(getServiceFeeAction());
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.success && this.state.isInit) {
      this.setState({ isInit: false });
      const { data } = nextProps;
      if (data.state !== 0) {
        // 有数据
        this.setState({ ...data });
      }
    }

    if (nextProps.success && this.state.isCreating) {
      this.setState({ isCreating: false });
      this.props.navigation.goBack();
    }

    if (nextProps.success && this.state.isUpdating) {
      this.setState({ isUpdating: false });
      this.props.navigation.goBack();
    }

    if (nextProps.success && this.state.isDeleting) {
      this.setState({ isDeleting: false });
      this.props.navigation.goBack();
    }
  }

  handleSubmit = () => {
    const param = {
      popId: this.props.popId,
      con20: stringToNumber(this.state.con20) || 0,
      con40: stringToNumber(this.state.con40) || 0,
      longrod: stringToNumber(this.state.longrod) || 0,
      stowagePic: this.state.stowagePic,
      feeApplayPic: this.state.feeApplayPic,
      shortrod: stringToNumber(this.state.shortrod) || 0,
      refrigerator: stringToNumber(this.state.refrigerator) || 0,
      applyReasons: this.state.applyReasons,
      applyMoney: stringToNumber(this.state.applyMoney) || 0,
      applyCause: this.state.applyCause,
      pic: this.state.pic,
    };
    if (this.state.state === 0) {
      this.setState({
        isCreating: true,
      }, () => {
        this.props.dispatch(createServiceFeeAction(param));
      });
    } else if (this.state.state === 10) {
      this.setState({
        isUpdating: true,
      }, () => {
        param.id = this.state.id;
        this.props.dispatch(updateServiceFeeAction(param));
      });
    } else {
      this.props.navigation.goBack();
    }
  };

  deleteServiceFee = () => {
    this.setState({
      isDeleting: true,
    }, () => {
      this.props.dispatch(deleteServiceFeeAction(this.state.id));
    });
  };

  render() {
    const showDelete = this.state.state === 10;
    return (
      <Container>
        <Content
          contentInsetAdjustmentBehavior="scrollableAxes"
          contentContainerStyle={{ paddingBottom: showDelete ? 40 : 0 }}
        >
          <Form style={styles.form}>
            <Label style={styles.title}>劳务费申请</Label>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="发生航次:"
                value={this.props.voyageCode}
                placeholder=""
                editable={false}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="发生港口:"
                value={this.props.portName}
                placeholder=""
                editable={false}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="绑扎小柜:"
                value={this.state.con20}
                onChangeText={(text) => {
                  this.setState({ con20: text });
                }}
                keyboardType="numeric"
                maskType={MaskType.FLOAT}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="绑扎大柜:"
                value={this.state.con40}
                onChangeText={(text) => {
                  this.setState({ con40: text });
                }}
                keyboardType="numeric"
                maskType={MaskType.FLOAT}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="长杆绑扎:"
                value={this.state.longrod}
                onChangeText={(text) => {
                  this.setState({ longrod: text });
                }}
                keyboardType="numeric"
                maskType={MaskType.FLOAT}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup} paddingBottom={10}>
              <InputItem
                label="短杆绑扎:"
                value={this.state.shortrod}
                onChangeText={(text) => {
                  this.setState({ shortrod: text });
                }}
                keyboardType="numeric"
                maskType={MaskType.FLOAT}
              />
            </InputGroup>
          </Form>
          <Form style={styles.form}>
            <Label style={styles.title}>冷柜费申请</Label>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="冷柜数量:"
                value={this.state.refrigerator}
                onChangeText={(text) => {
                  this.setState({ refrigerator: text });
                }}
                keyboardType="numeric"
                maskType={MaskType.INTEGER}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="申请原因:"
                value={this.state.applyReasons}
                onChangeText={(text) => {
                  this.setState({ applyReasons: text });
                }}
              />
            </InputGroup>
            <Item style={styles.item}>
              <Label style={{ color: '#535353', width: 100 }}>巡视检查表:</Label>
              <SelectImageView
                style={{
                  marginTop: 10,
                  width: 100,
                  height: 100,
                  marginLeft: 5,
                }}
                title="选择冷柜图照片"
                source={this.state.stowagePic}
                onPickImages={(images) => {
                  const image = images[0];
                  this.setState({
                    stowagePic: {
                      fileName: image.uri.split('/').reverse()[0],
                      fileSize: image.size,
                      path: image.uri,
                      uri: image.uri,
                    },
                  });
                }}
                onDelete={() => {
                  this.setState({
                    stowagePic: undefined,
                  });
                }}
              />
            </Item>
            <Item style={styles.item}>
              <Label style={{ color: '#535353', width: 100 }}>管理费申请表:</Label>
              <SelectImageView
                style={{
                  marginTop: 10,
                  width: 100,
                  height: 100,
                  marginLeft: 5,
                }}
                title="选择冷柜图照片"
                source={this.state.feeApplayPic}
                onPickImages={(images) => {
                  const image = images[0];
                  this.setState({
                    feeApplayPic: {
                      fileName: image.uri.split('/').reverse()[0],
                      fileSize: image.size,
                      path: image.uri,
                      uri: image.uri,
                    },
                  });
                }}
                onDelete={() => {
                  this.setState({
                    feeApplayPic: undefined,
                  });
                }}
              />
            </Item>
          </Form>
          <Form style={styles.form}>
            <Label style={styles.title}>特殊劳务费申请</Label>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="申请金额:"
                value={this.state.applyMoney}
                onChangeText={(text) => {
                  this.setState({ applyMoney: text });
                }}
                keyboardType="numeric"
                maskType={MaskType.INTEGER}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="申请原因:"
                value={this.state.applyCause}
                onChangeText={(text) => {
                  this.setState({ applyCause: text });
                }}
              />
            </InputGroup>
            <Item style={styles.item}>
              <Label style={{ color: '#535353' }}>图&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;片:</Label>
              <SelectImageView
                style={{
                  marginTop: 10,
                  width: 100,
                  height: 100,
                  marginLeft: 5,
                }}
                title="选择照片"
                source={this.state.pic}
                onPickImages={(images) => {
                  const image = images[0];
                  this.setState({
                    pic: {
                      fileName: image.uri.split('/').reverse()[0],
                      fileSize: image.size,
                      path: image.uri,
                      uri: image.uri,
                    },
                  });
                }}
                onDelete={() => {
                  this.setState({
                    pic: undefined,
                  });
                }}
              />
            </Item>
          </Form>
        </Content>
        {showDelete && (
          <SafeAreaView style={styles.bottomButtonContainer}>
            <Button
              onPress={() => {
                this.deleteServiceFee();
              }}
              full
            >
              <Text>删除申请</Text>
            </Button>
          </SafeAreaView>
        )}
      </Container>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  success: makeSelectCreateServiceFeeSuccess(),
  popId: makeSelectPopId(),
  portName: makeSelectPortName(),
  voyageCode: makeSelectVoyageCode(), // 航次
  data: makeSelectData(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ServiceFee);
