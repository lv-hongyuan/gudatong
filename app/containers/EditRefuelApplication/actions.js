import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  CREATE_REPAIR_APPLICATION,
  UPDATE_REPAIR_APPLICATION,
} from './constants';

export const createRefuelApplicationRoutine = createRoutine(
  CREATE_REPAIR_APPLICATION,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const createRefuelApplicationPromise = promisifyRoutine(createRefuelApplicationRoutine);

export const updateRefuelApplicationRoutine = createRoutine(
  UPDATE_REPAIR_APPLICATION,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const updateRefuelApplicationPromise = promisifyRoutine(updateRefuelApplicationRoutine);
