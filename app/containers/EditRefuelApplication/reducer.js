import {
  createRefuelApplicationRoutine,
  updateRefuelApplicationRoutine,
} from './actions';

const initState = {
  parentData: [],
  childData: [],
};

export default function (state = initState, action) {
  switch (action.type) {
    // 创建加油申请
    case createRefuelApplicationRoutine.TRIGGER:
      return { ...state, loading: true };
    case createRefuelApplicationRoutine.SUCCESS:
      return state;
    case createRefuelApplicationRoutine.FAILURE:
      return { ...state, error: action.payload };
    case createRefuelApplicationRoutine.FULFILL:
      return { ...state, loading: false };

    // 编辑加油申请
    case updateRefuelApplicationRoutine.TRIGGER:
      return { ...state, loading: true };
    case updateRefuelApplicationRoutine.SUCCESS:
      return state;
    case updateRefuelApplicationRoutine.FAILURE:
      return { ...state, error: action.payload };
    case updateRefuelApplicationRoutine.FULFILL:
      return { ...state, loading: false };

    default:
      return state;
  }
}
