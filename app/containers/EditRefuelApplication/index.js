import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Container, Content, Item, Label, InputGroup, Text } from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import HeaderButtons from 'react-navigation-header-buttons';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import _ from 'lodash';
import {
  createRefuelApplicationPromise,
  updateRefuelApplicationPromise,
} from './actions';
import {
  makeSelectPopId,
  makeData,
  makeSelectPortName,
  makeOnoBack,
  makeIsEdit, makeSelectShipName,
} from './selectors';
import { errorMessage } from '../ErrorHandler/actions';
import commonStyles from '../../common/commonStyles';
import InputItem from '../../components/InputItem';
import stringToNumber from '../../utils/stringToNumber';
import DatePullSelector from '../../components/DatePullSelector';
import screenHOC from '../screenHOC';
import { currentSuitableTime } from '../../utils/suitableDateTime';
import { MaskType } from '../../components/InputItem/TextInput';

/**
 * 加油申请
 * Created by jianzhexu on 2018/3/23
 */
@screenHOC
class EditRefuelApplication extends React.PureComponent {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.getParam('title', '创建加油申请'),
    headerRight: (
      <HeaderButtons>
        <HeaderButtons.Item
          title="保存"
          buttonStyle={{ fontSize: 14, color: '#ffffff' }}
          onPress={() => {
            const handleSubmitFunc = navigation.getParam('submit', undefined);
            if (handleSubmitFunc) {
              requestAnimationFrame(() => {
                handleSubmitFunc();
              });
            }
          }}
        />
      </HeaderButtons>
    ),
  });
  static propTypes = {
    navigation: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    data: PropTypes.object.isRequired,
    shipId: PropTypes.number.isRequired,
    shipName: PropTypes.string.isRequired,
    isEdit: PropTypes.bool.isRequired,
    onGoBack: PropTypes.func.isRequired,
    updateRefuelApplication: PropTypes.func.isRequired,
    createRefuelApplication: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);

    const data = props.data || {};
    this.state = {
      id: data.id,
      shipId: data.shipId || this.props.shipId,
      shipName: data.shipName || this.props.shipName,
      portId: data.portId,
      portName: data.portName,
      etaTime: props.data ? (data.etaTime || null) : currentSuitableTime(), // 预抵时间
      oil180: data.oil180,
      oil120: data.oil120,
      oil0: data.oil0,
      state: data.state,
    };
    this.props.navigation.setParams({ submit: this.handleSubmit });
  }

  componentWillMount() {

  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.data && nextProps.data !== this.props.data) {
      const data = nextProps.data || {};
      this.setState({
        id: data.id,
        shipId: data.shipId || this.props.shipId,
        shipName: data.shipName || this.props.shipName,
        portId: data.portId,
        portName: data.portName,
        etaTime: data.etaTime, // 预抵时间
        oil180: data.oil180,
        oil120: data.oil120,
        oil0: data.oil0,
        state: data.state,
      });
    }
  }

  handleSubmit = () => {
    const param = {
      id: this.state.id,
      popId: this.state.popId,
      shipId: this.state.shipId,
      shipName: this.state.shipName,
      portId: this.state.portId,
      portName: this.state.portName,
      etaTime: this.state.etaTime, // 预抵时间
      oil180: stringToNumber(this.state.oil180),
      oil120: stringToNumber(this.state.oil120),
      oil0: stringToNumber(this.state.oil0),
      state: this.state.state,
    };
    if (_.isEmpty(param.portName)) {
      this.props.dispatch(errorMessage('请填写港口'));
      return;
    }
    if (!param.etaTime) {
      this.props.dispatch(errorMessage('请填写预抵时间'));
      return;
    }
    if (!param.oil180 && !param.oil120 && !param.oil0) {
      this.props.dispatch(errorMessage('请填写加油量'));
      return;
    }
    if (this.props.isEdit) {
      this.props.updateRefuelApplication(param).then(() => {
        if (this.props.onGoBack) this.props.onGoBack();
        this.props.navigation.goBack();
      }).catch(() => {});
    } else {
      this.props.createRefuelApplication(param).then(() => {
        if (this.props.onGoBack) this.props.onGoBack();
        this.props.navigation.goBack();
      }).catch(() => {});
    }
  };

  render() {
    return (
      <Container style={{
        backgroundColor: '#FFFFFF',
      }}
      >
        <Content style={{ padding: 10 }}>
          <InputGroup style={commonStyles.inputGroup}>
            <InputItem
              label="加油港口:"
              value={this.state.portName}
              onChangeText={(text) => {
                this.setState({
                  portName: text.trim(),
                });
              }}
            />
          </InputGroup>
          <InputGroup style={commonStyles.inputGroup}>
            <InputItem
              label="中文船名:"
              value={this.state.shipName ? this.state.shipName : this.props.shipName}
              placeholder=""
              editable={false}
            />
          </InputGroup>
          <InputGroup style={commonStyles.inputGroup}>
            <Item style={commonStyles.inputItem}>
              <Label style={commonStyles.inputLabel}>预抵时间:</Label>
              <DatePullSelector
                value={this.state.etaTime || null}
                onChangeValue={(value) => {
                  this.setState({ etaTime: value });
                }}
              />
            </Item>
          </InputGroup>
          <InputGroup style={commonStyles.inputGroup}>
            <InputItem
              label="180#:"
              value={this.state.oil180 || null}
              onChangeText={(text) => {
                this.setState({ oil180: text });
              }}
              keyboardType="numeric"
              maskType={MaskType.FLOAT}
              rightItem={<Text style={commonStyles.tail}>吨</Text>}
            />
          </InputGroup>
          <InputGroup style={commonStyles.inputGroup}>
            <InputItem
              label="120#:"
              value={this.state.oil120 || null}
              onChangeText={(text) => {
                this.setState({ oil120: text });
              }}
              keyboardType="numeric"
              maskType={MaskType.FLOAT}
              rightItem={<Text style={commonStyles.tail}>吨</Text>}
            />
          </InputGroup>
          <InputGroup style={commonStyles.inputGroup}>
            <InputItem
              label="0#:"
              value={this.state.oil0 || null}
              onChangeText={(text) => {
                this.setState({ oil0: text });
              }}
              keyboardType="numeric"
              maskType={MaskType.FLOAT}
              rightItem={<Text style={commonStyles.tail}>吨</Text>}
            />
          </InputGroup>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  portName: makeSelectPortName(),
  shipName: makeSelectShipName(),
  popId: makeSelectPopId(),
  isEdit: makeIsEdit(),
  data: makeData(),
  onGoBack: makeOnoBack(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      createRefuelApplication: createRefuelApplicationPromise,
      updateRefuelApplication: updateRefuelApplicationPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EditRefuelApplication);
