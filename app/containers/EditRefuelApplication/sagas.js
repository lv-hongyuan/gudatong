import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import {
  createRefuelApplicationRoutine,
  updateRefuelApplicationRoutine,
} from './actions';
import { errorMessage } from '../ErrorHandler/actions';

function* createRefuelApplication(action) {
  console.log(action);
  try {
    yield put(createRefuelApplicationRoutine.request());
    const {
      portId,
      portName,
      shipId,
      shipName,
      etaTime,
      oil180,
      oil120,
      oil0,
      state,
    } = action.payload;
    const response = yield call(
      request,
      ApiFactory.createAddOilPlan({
        portId,
        portName,
        shipId,
        shipName,
        etaTime,
        oil180,
        oil120,
        oil0,
        state,
      }),
    );
    console.log('createRefuelApplication', response.toString());
    yield put(createRefuelApplicationRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(createRefuelApplicationRoutine.failure(e));
  } finally {
    yield put(createRefuelApplicationRoutine.fulfill());
  }
}

export function* createRefuelApplicationSaga() {
  yield takeLatest(createRefuelApplicationRoutine.TRIGGER, createRefuelApplication);
}

function* updateRefuelApplication(action) {
  console.log(action);
  try {
    yield put(updateRefuelApplicationRoutine.request());
    const {
      id,
      portId,
      portName,
      shipId,
      shipName,
      etaTime,
      oil180,
      oil120,
      oil0,
      state,
    } = action.payload;
    const response = yield call(
      request,
      ApiFactory.updateAddOilPlan({
        id,
        portId,
        portName,
        shipId,
        shipName,
        etaTime,
        oil180,
        oil120,
        oil0,
        state,
      }),
    );
    console.log('updateRefuelApplication', response.toString());
    yield put(updateRefuelApplicationRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(updateRefuelApplicationRoutine.failure(e));
  } finally {
    yield put(updateRefuelApplicationRoutine.fulfill());
  }
}

export function* updateRefuelApplicationSaga() {
  yield takeLatest(updateRefuelApplicationRoutine.TRIGGER, updateRefuelApplication);
}

// All sagas to be loaded
export default [
  createRefuelApplicationSaga,
  updateRefuelApplicationSaga,
];
