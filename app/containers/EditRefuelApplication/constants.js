/*
 *
 * EditRefuelApplication constants
 *
 */
export const CREATE_REPAIR_APPLICATION = 'app/EditRefuelApplication/CREATE_REPAIR_APPLICATION';

export const UPDATE_REPAIR_APPLICATION = 'app/EditRefuelApplication/UPDATE_REPAIR_APPLICATION';
