import { createSelector } from 'reselect/es';

const selectAnchorageDomain = () => state => state.anchorage;

const makeRange = () => createSelector(
    (state, props) => props.navigation.state.params,
    (subState) => {
        console.debug(subState && subState.range);
        return subState && subState.range;
    },
);

const makeSelectGetAnchorageListSuccess = () => createSelector(
  selectAnchorageDomain(),
  (subState) => {
    console.debug(subState.success);
    return subState.success;
  },
);

const makeSelectGetAnchorageListData = () => createSelector(
  selectAnchorageDomain(),
  (subState) => {
    console.debug(subState.success);
    return subState.data;
  },
);

export {
  makeSelectGetAnchorageListSuccess,
  selectAnchorageDomain,
  makeSelectGetAnchorageListData,
  makeRange,
};
