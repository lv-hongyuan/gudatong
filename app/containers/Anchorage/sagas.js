import { call, put, takeLatest } from 'redux-saga/effects';
import { ShipEventRange } from '../../common/Constant';
import { GET_ANCHORAGE_LIST_ACTION, DELETE_ANCHORAGE_LIST_ACTION } from './constants';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import { getAnchorageListResultAction, deleteAnchorageListResultAction } from './actions';
import { errorMessage } from '../ErrorHandler/actions';
import { getPopId } from '../ShipWork/sagas';

function* getAnchorageList(action) {
  console.log(action);
  try {
    const popId = yield call(getPopId);
    // const response = yield call(request, ApiFactory.findShipEvents(popId, ShipEventRange.SHIP_ANCHORING));
    const response = yield call(request, ApiFactory.findShipEvents(popId, action.payload.range));
    yield put(getAnchorageListResultAction(response, true));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(getAnchorageListResultAction(undefined, false));
  }
}

export function* getAnchorageListSaga() {
  yield takeLatest(GET_ANCHORAGE_LIST_ACTION, getAnchorageList);
}

function* deleteAnchorage(action) {
  console.log(action);
  try {
    const response = yield call(request, ApiFactory.deleteShipEvent(action.payload));
    yield put(deleteAnchorageListResultAction(response, true));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(deleteAnchorageListResultAction(undefined, false));
  }
}

export function* deleteAnchorageSaga() {
  yield takeLatest(DELETE_ANCHORAGE_LIST_ACTION, deleteAnchorage);
}

// All sagas to be loaded
export default [
  getAnchorageListSaga,
  deleteAnchorageSaga,
];
