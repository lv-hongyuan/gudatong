import {
  GET_ANCHORAGE_LIST_ACTION_RESULT,
  DELETE_ANCHORAGE_LIST_ACTION,
  DELETE_ANCHORAGE_LIST_ACTION_RESULT,
  GET_ANCHORAGE_LIST_ACTION,
} from './constants';

const initState = {
  success: false,
  data: [],
};

export default function (state = initState, action) {
  switch (action.type) {
    case DELETE_ANCHORAGE_LIST_ACTION:
    case GET_ANCHORAGE_LIST_ACTION:
      return { ...state, success: false };
    case GET_ANCHORAGE_LIST_ACTION_RESULT:
      return {
        ...state,
        success: action.payload.success,
        data: action.payload.data ? action.payload.data.dtoList : [],
      };
    case DELETE_ANCHORAGE_LIST_ACTION_RESULT:
      return { ...state, success: action.payload.success };
    default:
      return state;
  }
}
