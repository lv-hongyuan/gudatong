import {
  GET_ANCHORAGE_LIST_ACTION,
  GET_ANCHORAGE_LIST_ACTION_RESULT,
  DELETE_ANCHORAGE_LIST_ACTION,
  DELETE_ANCHORAGE_LIST_ACTION_RESULT,
} from './constants';

export function getAnchorageListAction(range) {
  return {
    type: GET_ANCHORAGE_LIST_ACTION,
    payload: { range },
  };
}

export function refreshAnchorageListAction(range) {
  return {
    type: GET_ANCHORAGE_LIST_ACTION,
    noLoading: true,
    payload: { range },
  };
}

export function getAnchorageListResultAction(data, success) {
  return {
    type: GET_ANCHORAGE_LIST_ACTION_RESULT,
    payload: {
      data,
      success,
    },
  };
}

export function deleteAnchorageListAction(data) {
  return {
    type: DELETE_ANCHORAGE_LIST_ACTION,
    payload: data,
  };
}

export function deleteAnchorageListResultAction(data, success) {
  return {
    type: DELETE_ANCHORAGE_LIST_ACTION_RESULT,
    payload: {
      data,
      success,
    },
  };
}
