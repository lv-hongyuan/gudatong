import React from 'react';
import PropTypes from 'prop-types';
import { FlatList, RefreshControl } from 'react-native';
import { connect } from 'react-redux';
import { Container, View } from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import HeaderButtons from 'react-navigation-header-buttons';
import AnchorageItem from '../../components/AnchorageItem';
import { ROUTE_EDIT_ANCHORAGE_EVENT } from '../../RouteConstant';
import { makeSelectGetAnchorageListSuccess, makeSelectGetAnchorageListData } from './selectors';
import { getAnchorageListAction, refreshAnchorageListAction, deleteAnchorageListAction } from './actions';
import EmptyView from '../../components/EmptyView';
import screenHOC from '../screenHOC';
import { errorMessage } from '../ErrorHandler/actions';
import { makeRange } from './selectors';
import { ShipEventRange } from '../../common/Constant';

/**
 * 锚泊/漂航列表
 * Created by jianzhexu on 2018/3/26
 */
@screenHOC
class Anchorage extends React.PureComponent {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.getParam('title', '港内停航原因'),
    headerRight: (
      <HeaderButtons>
        <HeaderButtons.Item
          title="创建"
          buttonStyle={{ fontSize: 14, color: '#ffffff' }}
          onPress={() => {
            const reload = navigation.getParam('reload', undefined);
            navigation.navigate(ROUTE_EDIT_ANCHORAGE_EVENT, {
              title: navigation.getParam('editTitle', '创建锚泊/漂航记录'),
              range: navigation.getParam('range'),
              onGoBack: () => {
                if (reload) {
                  reload();
                }
              },
            });
          }}
        />
      </HeaderButtons>
    ),
  });

  static propTypes = {
    navigation: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    success: PropTypes.bool.isRequired,
    data: PropTypes.array.isRequired,
    range: PropTypes.number,
  };

  static defaultProps = {
    range: ShipEventRange.SHIP_ANCHORING,
  };

  constructor(props) {
    super(props);
    this.state = {
      isDelete: false,
      refreshing: false,
    };
    this.props.navigation.setParams({
      reload: this.reLoadingPage,
      title: props.range === ShipEventRange.SHIP_ANCHORING ? '港内停航原因' : '突发事件',
      editTitle: props.range === ShipEventRange.SHIP_ANCHORING ? '新建港内停航原因' : '创建突发事件记录',
      range: props.range,
    });
  }

  componentWillMount() {
    this.reLoadingPage();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.success && this.state.isDelete) {
      this.setState({ isDelete: false }, () => {
        this.reLoadingPage();
      });
    }
    if (this.state.refreshing) {
      this.setState({ refreshing: false });
    }
  }

  reLoadingPage = () => {
    if (this.state.refreshing) {
      requestAnimationFrame(() => {
        this.props.dispatch(refreshAnchorageListAction(this.props.range));
      });
    } else {
      requestAnimationFrame(() => {
        this.props.dispatch(getAnchorageListAction(this.props.range));
      });
    }
  };

  itemPress = (data) => {
    //事件结束后,不可编辑
    // if (data.endTime > 0) {
    //   this.props.dispatch(errorMessage('事件已结束，无法编辑'));
    //   return;
    // }
    this.props.navigation.navigate(ROUTE_EDIT_ANCHORAGE_EVENT, {
      // title: '编辑锚泊/漂航记录',
      title: this.props.range === ShipEventRange.SHIP_ANCHORING ? '编辑港内停航原因' : '编辑突发事件',
      isEdit: true,
      range: this.props.range,
      onGoBack: () => {
        this.reLoadingPage();
      },
      data,
    });
  };

  deleteItem = (data) => {
    if (data.endTime > 0) {
      this.props.dispatch(errorMessage('事件已结束，无法删除'));
      return;
    }
    this.setState({ isDelete: true }, () => {
      this.props.dispatch(deleteAnchorageListAction(data.id));
    });
  };

  renderItem = ({ item }) => (<AnchorageItem
    typeChildText={item.typeChildText}
    describe={item.describe}
    typeChildCode={item.typeChildCode}
    startTime={item.startTime}
    endTime={item.endTime}
    hidePlace={item.hidePlace}
    portName={item.portName}
    onDelete={this.deleteItem}
    onItemPress={this.itemPress}
    popId={item.popId}
    id={item.id}
  />);

  renderEmptyView = () => (<EmptyView />);

  render() {
    return (
      <Container>
        <FlatList
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => {
                this.setState({ refreshing: true }, () => {
                  this.reLoadingPage();
                });
              }}
            />
          }
          contentInsetAdjustmentBehavior="scrollableAxes"
          style={{ flex: 1, backgroundColor: '#e0e0e0' }}
          keyExtractor={item => `${item.id}`}
          data={this.props.data}
          ListEmptyComponent={this.renderEmptyView}
          initialNumToRender={10}
          renderItem={this.renderItem}
          ItemSeparatorComponent={() => (<View style={{ height: 5 }} />)}
          ListFooterComponent={() => (<View style={{ height: 5 }} />)}
          ListHeaderComponent={() => (<View style={{ height: 5 }} />)}
        />
      </Container>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  success: makeSelectGetAnchorageListSuccess(),
  data: makeSelectGetAnchorageListData(),
  range: makeRange(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Anchorage);
