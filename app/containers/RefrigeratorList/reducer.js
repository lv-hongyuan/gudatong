import {
  getRefrigeratorListRoutine,
  deleteRefrigeratorRoutine,
} from './actions';

const initState = {
  data: [],
};

export default function (state = initState, action) {
  switch (action.type) {
    case getRefrigeratorListRoutine.TRIGGER:
      return { ...state, loading: true };
    case getRefrigeratorListRoutine.SUCCESS:
      return { ...state, data: action.payload.dtoList };
    case getRefrigeratorListRoutine.FAILURE:
      return { ...state, error: action.payload };
    case getRefrigeratorListRoutine.FULFILL:
      return { ...state, loading: false };

    case deleteRefrigeratorRoutine.TRIGGER:
      return { ...state, loading: true };
    case deleteRefrigeratorRoutine.SUCCESS: {
      const id = action.payload;
      const list = state.data || [];
      const index = list.findIndex(item => item.id === id);
      if (index !== -1) {
        list.splice(index, 1);
      }
      return { ...state, data: list };
    }
    case deleteRefrigeratorRoutine.FAILURE:
      return { ...state, error: action.payload };
    case deleteRefrigeratorRoutine.FULFILL:
      return { ...state, loading: false };

    default:
      return state;
  }
}
