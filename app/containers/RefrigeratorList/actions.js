import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  GET_REFRIGERATOR_LIST,
  DELETE_REFRIGERATOR,
} from './constants';

export const getRefrigeratorListRoutine = createRoutine(
  GET_REFRIGERATOR_LIST,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const getRefrigeratorListPromise = promisifyRoutine(getRefrigeratorListRoutine);

export const deleteRefrigeratorRoutine = createRoutine(
  DELETE_REFRIGERATOR,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const deleteRefrigeratorPromise = promisifyRoutine(deleteRefrigeratorRoutine);
