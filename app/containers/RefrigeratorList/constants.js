/*
 *
 * RefrigeratorList constants
 *
 */
export const GET_REFRIGERATOR_LIST = 'app/RefrigeratorList/GET_REFRIGERATOR_LIST';

export const DELETE_REFRIGERATOR = 'app/RefrigeratorList/DELETE_REFRIGERATOR';
