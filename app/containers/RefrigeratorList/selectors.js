import { createSelector } from 'reselect/es';

const selectRefrigeratorListDomain = () => state => state.refrigeratorList;

const makeRefrigeratorList = () => createSelector(
  selectRefrigeratorListDomain(),
  (subState) => {
    console.debug(subState.data);
    return subState.data;
  },
);

export {
  makeRefrigeratorList,
};
