import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import { getRefrigeratorListRoutine, deleteRefrigeratorRoutine } from './actions';
import { errorMessage } from '../ErrorHandler/actions';
import { getPopId } from '../ShipWork/sagas';

function* getRefrigeratorList(action) {
  console.log(action);
  try {
    yield put(getRefrigeratorListRoutine.request());
    const popId = yield call(getPopId);
    const response = yield call(
      request,
      ApiFactory.getRefrigeratorByPop(popId),
    );
    console.log('getRefrigeratorList', response.toString());
    yield put(getRefrigeratorListRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(getRefrigeratorListRoutine.failure(e));
  } finally {
    yield put(getRefrigeratorListRoutine.fulfill());
  }
}

export function* getRefrigeratorListSaga() {
  yield takeLatest(getRefrigeratorListRoutine.TRIGGER, getRefrigeratorList);
}

function* deleteRefrigerator(action) {
  console.log(action);
  try {
    yield put(deleteRefrigeratorRoutine.request());
    const id = action.payload;
    const response = yield call(
      request,
      ApiFactory.deleteRefrigerator(id),
    );
    console.log('deleteRefrigerator', response.toString());
    yield put(deleteRefrigeratorRoutine.success(id));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(deleteRefrigeratorRoutine.failure(e));
  } finally {
    yield put(deleteRefrigeratorRoutine.fulfill());
  }
}

export function* deleteRefrigeratorSaga() {
  yield takeLatest(deleteRefrigeratorRoutine.TRIGGER, deleteRefrigerator);
}

// All sagas to be loaded
export default [
  getRefrigeratorListSaga,
  deleteRefrigeratorSaga,
];
