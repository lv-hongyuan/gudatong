import React from 'react';
import PropTypes from 'prop-types';
import { FlatList, RefreshControl } from 'react-native';
import { connect } from 'react-redux';
import { Container, View } from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import HeaderButtons from 'react-navigation-header-buttons';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { ROUTE_EDIT_REFRIGERATOR_RECORD } from '../../RouteConstant';
import { makeSelectIsSail } from '../ShipWork/selectors';
import { getRefrigeratorListPromise, deleteRefrigeratorPromise } from './actions';
import myTheme from '../../Themes';
import { makeRefrigeratorList } from './selectors';
import EmptyView from '../../components/EmptyView';
import screenHOC from '../screenHOC';
import RefrigeratorItem from '../../components/RefrigeratorItem';

/**
 * 冷藏箱记录页面
 * Created by jianzhexu on 2018/3/29
 */
@screenHOC
class RefrigeratorList extends React.PureComponent {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.getParam('title', '冷藏箱记录'),
    headerRight: (
      <HeaderButtons>
        <HeaderButtons.Item
          title="创建"
          buttonStyle={{ fontSize: 14, color: '#ffffff' }}
          onPress={navigation.getParam('onBtnCreate')}
        />
      </HeaderButtons>
    ),
  });
  static propTypes = {
    navigation: PropTypes.object.isRequired,
    getRefrigeratorList: PropTypes.func.isRequired,
    deleteRefrigerator: PropTypes.func.isRequired,
    data: PropTypes.array.isRequired,
    isSail: PropTypes.bool.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
    };
    this.props.navigation.setParams({ onBtnCreate: this.onBtnCreate.bind(this) });
  }

  componentDidMount() {
    this.reLoadingPage();
  }

  onBtnCreate = () => {
    this.props.navigation.navigate(ROUTE_EDIT_REFRIGERATOR_RECORD, {
      title: '创建冷藏箱记录',
      onGoBack: () => {
        this.reLoadingPage();
      },
      isEdit: false,
      isVoyage: this.props.isSail,
    });
  };

  reLoadingPage = () => {
    requestAnimationFrame(() => {
      this.props.getRefrigeratorList()
        .then(() => {
          this.setState({ refreshing: false });
        })
        .catch(() => {
          this.setState({ refreshing: false });
        });
    });
  };

  itemPress = (data) => {
    this.props.navigation.navigate(ROUTE_EDIT_REFRIGERATOR_RECORD, {
      title: '编辑冷藏箱记录',
      onGoBack: () => {
        this.reLoadingPage();
      },
      isEdit: true,
      data,
      isVoyage: this.props.isSail,
    });
  };

  deleteItem = (id) => {
    this.props.deleteRefrigerator(id)
      .then(() => {
        this.reLoadingPage();
      })
      .catch(() => {
      });
  };

  renderItem = ({ item }) => (
    <RefrigeratorItem
      id={item.id}
      popId={item.popId}
      contNumber={item.contNumber}
      unloadingPort={item.unloadingPort}
      shipLoadPosition={item.shipLoadPosition}
      temperature={item.temperature}
      ventilationOpening={item.ventilationOpening}
      contType={item.contType}
      onDelete={this.deleteItem}
      onItemPress={this.itemPress}
    />
  );

  renderEmptyView = () => (<EmptyView />);

  render() {
    return (
      <Container theme={myTheme}>
        <FlatList
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => {
                this.setState({ refreshing: true });
                this.reLoadingPage();
              }}
            />
          }
          contentInsetAdjustmentBehavior="scrollableAxes"
          style={{ flex: 1, backgroundColor: '#e0e0e0' }}
          keyExtractor={item => `${item.id}`}
          data={this.props.data}
          ListEmptyComponent={this.renderEmptyView}
          initialNumToRender={10}
          renderItem={this.renderItem}
          ItemSeparatorComponent={() => (<View style={{ height: 5 }} />)}
          ListFooterComponent={() => (<View style={{ height: 5 }} />)}
          ListHeaderComponent={() => (<View style={{ height: 5 }} />)}
        />
      </Container>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  data: makeRefrigeratorList(),
  isSail: makeSelectIsSail(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getRefrigeratorList: getRefrigeratorListPromise,
      deleteRefrigerator: deleteRefrigeratorPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(RefrigeratorList);
