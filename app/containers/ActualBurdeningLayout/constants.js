/*
 *
 * Login constants
 *
 */
export const UPLOAD_ACTUAL_LAYOUT_ACTION = 'app/ActualBurdeningLayout/UPLOAD_ACTUAL_LAYOUT_ACTION';
export const UPLOAD_ACTUAL_LAYOUT_ACTION_RESULT = 'app/ActualBurdeningLayout/UPLOAD_ACTUAL_LAYOUT_ACTION_RESULT';
export const GET_ACTUAL_LAYOUT_ACTION = 'app/ActualBurdeningLayout/GET_ACTUAL_LAYOUT_ACTION';
export const GET_ACTUAL_LAYOUT_ACTION_RESULT = 'app/ActualBurdeningLayout/GET_ACTUAL_LAYOUT_ACTION_RESULT';

