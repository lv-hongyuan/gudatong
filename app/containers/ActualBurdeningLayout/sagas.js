import { call, put, takeLatest } from 'redux-saga/effects';
import { GET_ACTUAL_LAYOUT_ACTION, UPLOAD_ACTUAL_LAYOUT_ACTION } from './constants';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import { uploadActualLayoutResultAction, getActualLayoutResultAction } from './actions';
import { errorMessage } from '../ErrorHandler/actions';
import uploadImage from '../../common/uploadImage';
import { getPopId } from '../ShipWork/sagas';

function* realLoadDiagram(action) {
  console.log(action);
  try {
    const {
      reLoadPic,
      reLoadStabilePic,
      draughtFrontPic,
      draughtMidPic,
      draughtAfterPic,
    } = action.payload;
    const popId = yield call(getPopId);
    const param = { popId };

    if (reLoadPic) {
      if (reLoadPic.path) {
        const response = yield call(uploadImage, reLoadPic, 'reLoadPic', popId);
        param.reLoadPic = response.id;
      } else {
        param.reLoadPic = reLoadPic.substr(reLoadPic.lastIndexOf('/') + 1);
      }
    }
    if (reLoadStabilePic) {
      if (reLoadStabilePic.path) {
        const response = yield call(uploadImage, reLoadStabilePic, 'reLoadStabilePic', popId);
        param.reLoadStabilePic = response.id;
      } else {
        param.reLoadStabilePic = reLoadStabilePic.substr(reLoadStabilePic.lastIndexOf('/') + 1);
      }
    }
    if (draughtFrontPic) {
      if (draughtFrontPic.path) {
        const response = yield call(uploadImage, draughtFrontPic, 'draughtFrontPic', popId);
        param.draughtFrontPic = response.id;
      } else {
        param.draughtFrontPic = draughtFrontPic.substr(draughtFrontPic.lastIndexOf('/') + 1);
      }
    }
    if (draughtMidPic) {
      if (draughtMidPic.path) {
        const response = yield call(uploadImage, draughtMidPic, 'draughtMidPic', popId);
        param.draughtMidPic = response.id;
      } else {
        param.draughtMidPic = draughtMidPic.substr(draughtMidPic.lastIndexOf('/') + 1);
      }
    }
    if (draughtAfterPic) {
      if (draughtAfterPic.path) {
        const response = yield call(uploadImage, draughtAfterPic, 'draughtAfterPic', popId);
        param.draughtAfterPic = response.id;
      } else {
        param.draughtAfterPic = draughtAfterPic.substr(draughtAfterPic.lastIndexOf('/') + 1);
      }
    }
    const response = yield call(request, ApiFactory.realLoadDiagram(param));
    console.log(response);
    yield put(uploadActualLayoutResultAction(response, true));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(uploadActualLayoutResultAction(undefined, false));
  }
}

export function* realLoadDiagramSaga() {
  yield takeLatest(UPLOAD_ACTUAL_LAYOUT_ACTION, realLoadDiagram);
}

function* realLoadShow(action) {
  console.log(action);
  try {
    const popId = yield call(getPopId);
    const response = yield call(request, ApiFactory.realLoadShow(popId));
    console.log(response);
    yield put(getActualLayoutResultAction(response, true));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(getActualLayoutResultAction(undefined, false));
  }
}

export function* realLoadShowSaga() {
  yield takeLatest(GET_ACTUAL_LAYOUT_ACTION, realLoadShow);
}

// All sagas to be loaded
export default [
  realLoadDiagramSaga,
  realLoadShowSaga,
];
