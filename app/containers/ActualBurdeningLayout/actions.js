import {
  UPLOAD_ACTUAL_LAYOUT_ACTION,
  UPLOAD_ACTUAL_LAYOUT_ACTION_RESULT,
  GET_ACTUAL_LAYOUT_ACTION,
  GET_ACTUAL_LAYOUT_ACTION_RESULT,
} from './constants';

export function uploadActualLayoutAction(data) {
  return {
    type: UPLOAD_ACTUAL_LAYOUT_ACTION,
    payload: data,
  };
}

export function uploadActualLayoutResultAction(data, success) {
  return {
    type: UPLOAD_ACTUAL_LAYOUT_ACTION_RESULT,
    payload: {
      data,
      success,
    },
  };
}

export function getActualLayoutAction() {
  return {
    type: GET_ACTUAL_LAYOUT_ACTION,
  };
}

export function getActualLayoutResultAction(data, success) {
  return {
    type: GET_ACTUAL_LAYOUT_ACTION_RESULT,
    payload: {
      data,
      success,
    },
  };
}
