import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { Container, Content, Form, Item, Label } from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import HeaderButtons from 'react-navigation-header-buttons';
import {
  makeSelectReLoadPic,
  makeSelectReLoadStabilePic,
  makeSelectDraughtFrontPic,
  makeSelectDraughtMidPic,
  makeSelectDraughtAfterPic,
  makeSelectUploadActualLayoutSuccess,
} from './selectors';
import { getActualLayoutAction, uploadActualLayoutAction } from './actions';
import {
  makeSelectPortName,
  makeSelectShipName,
  makeSelectVoyageCode,
} from '../BeforehandLayout/selectors';
import SelectImageView from '../../components/SelectImageView';
import commonStyles from '../../common/commonStyles';
import screenHOC from '../screenHOC';

const styles = StyleSheet.create({
  item: {
    borderBottomColor: 'transparent',
    marginBottom: 10,
  },
  imageStyle: {
    width: 144,
    height: 144,
  },
  imageTouch: {
    width: 144,
    height: 144,
  },
});

/**
 * 实载图上传主页面
 * Created by jianzhexu on 2018/3/29
 */
@screenHOC
class ActualBurdeningLayout extends React.PureComponent {
  static navigationOptions = ({ navigation }) => ({
    title: '实载图上传',
    headerRight: (
      <HeaderButtons>
        <HeaderButtons.Item
          title="保存"
          buttonStyle={{ fontSize: 14, color: '#ffffff' }}
          onPress={() => {
            const handleSubmitFunc = navigation.getParam('submit', undefined);
            if (handleSubmitFunc) {
              requestAnimationFrame(() => {
                handleSubmitFunc();
              });
            }
          }}
        />
      </HeaderButtons>
    ),
  });

  static propTypes = {
    navigation: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    success: PropTypes.bool.isRequired,
    reLoadPic: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
    reLoadStabilePic: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
    draughtFrontPic: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
    draughtMidPic: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
    draughtAfterPic: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
    shipName: PropTypes.string.isRequired,
    voyageCode: PropTypes.string.isRequired,
    portName: PropTypes.string.isRequired,
  };
  static defaultProps = {
    reLoadPic: undefined,
    reLoadStabilePic: undefined,
    draughtFrontPic: undefined,
    draughtMidPic: undefined,
    draughtAfterPic: undefined,
  };

  constructor(props) {
    super(props);
    this.state = {
      reLoadPic: null,
      reLoadStabilePic: null,
      draughtFrontPic: null,
      draughtMidPic: null,
      draughtAfterPic: null,
      isInit: true,
      isUploading: false,
    };
    this.props.navigation.setParams({ submit: this.handleSubmit });
  }

  componentDidMount() {
    this.props.dispatch(getActualLayoutAction());
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.success && this.state.isUploading) {
      const goBackCallback = this.props.navigation.getParam('onGoBack', undefined);
      if (goBackCallback) {
        goBackCallback();
      }
      this.props.navigation.goBack();
      this.setState({
        isUploading: false,
      });
    }

    if (nextProps.success && this.state.isInit) {
      this.setState({
        reLoadPic: nextProps.reLoadPic,
        reLoadStabilePic: nextProps.reLoadStabilePic,
        draughtFrontPic: nextProps.draughtFrontPic,
        draughtMidPic: nextProps.draughtMidPic,
        draughtAfterPic: nextProps.draughtAfterPic,
        isInit: false,
      });
    }
  }

  handleSubmit = () => {
    this.setState({
      isUploading: true,
    }, () => {
      this.props.dispatch(uploadActualLayoutAction({
        reLoadPic: this.state.reLoadPic,
        reLoadStabilePic: this.state.reLoadStabilePic,
        draughtFrontPic: this.state.draughtFrontPic,
        draughtMidPic: this.state.draughtMidPic,
        draughtAfterPic: this.state.draughtAfterPic,
      }));
    });
  };

  render() {
    const { shipName, voyageCode, portName } = this.props;
    return (
      <Container>
        <Content style={{
          backgroundColor: '#FFFFFF',
          padding: 10,
        }}
        >
          <Form style={{ paddingBottom: 10 }}>
            <Item style={styles.item}>
              <Label style={commonStyles.inputLabel}>船&nbsp;&nbsp;&nbsp;&nbsp;名:</Label>
              <Label style={commonStyles.contentLabel}>{shipName}</Label>
            </Item>
            <Item style={styles.item}>
              <Label style={commonStyles.inputLabel}>航&nbsp;&nbsp;&nbsp;&nbsp;次:</Label>
              <Label style={commonStyles.contentLabel}>{voyageCode}</Label>
            </Item>
            <Item style={styles.item}>
              <Label style={commonStyles.inputLabel}>港&nbsp;&nbsp;&nbsp;&nbsp;口:</Label>
              <Label style={commonStyles.contentLabel}>{portName}</Label>
            </Item>
            <Item style={styles.item}>
              <Label style={commonStyles.inputLabel}>配载图:</Label>
              <SelectImageView
                title="选择配载图照片"
                source={this.state.reLoadPic}
                onPickImages={(images) => {
                  const image = images[0];
                  this.setState({
                    reLoadPic: {
                      fileName: image.uri.split('/').reverse()[0],
                      fileSize: image.size,
                      path: image.uri,
                      uri: image.uri,
                    },
                  });
                }}
                onDelete={() => {
                  this.setState({
                    reLoadPic: undefined,
                  });
                }}
              />
            </Item>
            <Item style={styles.item}>
              <Label style={commonStyles.inputLabel}>稳性表:</Label>
              <SelectImageView
                title="选择稳性表照片"
                source={this.state.reLoadStabilePic}
                onPickImages={(images) => {
                  const image = images[0];
                  this.setState({
                    reLoadStabilePic: {
                      fileName: image.uri.split('/').reverse()[0],
                      fileSize: image.size,
                      path: image.uri,
                      uri: image.uri,
                    },
                  });
                }}
                onDelete={() => {
                  this.setState({
                    reLoadStabilePic: undefined,
                  });
                }}
              />
            </Item>
            <Item style={styles.item}>
              <Label style={commonStyles.inputLabel}>吃水前:</Label>
              <SelectImageView
                title="选择吃水前照片"
                source={this.state.draughtFrontPic}
                onPickImages={(images) => {
                  const image = images[0];
                  this.setState({
                    draughtFrontPic: {
                      fileName: image.uri.split('/').reverse()[0],
                      fileSize: image.size,
                      path: image.uri,
                      uri: image.uri,
                    },
                  });
                }}
                onDelete={() => {
                  this.setState({
                    draughtFrontPic: undefined,
                  });
                }}
              />
            </Item>
            <Item style={styles.item}>
              <Label style={commonStyles.inputLabel}>吃水中:</Label>
              <SelectImageView
                title="选择吃水中照片"
                source={this.state.draughtMidPic}
                onPickImages={(images) => {
                  const image = images[0];
                  this.setState({
                    draughtMidPic: {
                      fileName: image.uri.split('/').reverse()[0],
                      fileSize: image.size,
                      path: image.uri,
                      uri: image.uri,
                    },
                  });
                }}
                onDelete={() => {
                  this.setState({
                    draughtMidPic: undefined,
                  });
                }}
              />
            </Item>
            <Item style={styles.item}>
              <Label style={commonStyles.inputLabel}>吃水后:</Label>
              <SelectImageView
                title="选择吃水后照片"
                source={this.state.draughtAfterPic}
                onPickImages={(images) => {
                  const image = images[0];
                  this.setState({
                    draughtAfterPic: {
                      fileName: image.uri.split('/').reverse()[0],
                      fileSize: image.size,
                      path: image.uri,
                      uri: image.uri,
                    },
                  });
                }}
                onDelete={() => {
                  this.setState({
                    draughtAfterPic: undefined,
                  });
                }}
              />
            </Item>
          </Form>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  success: makeSelectUploadActualLayoutSuccess(),
  portName: makeSelectPortName(),
  voyageCode: makeSelectVoyageCode(),
  shipName: makeSelectShipName(),
  reLoadStabilePic: makeSelectReLoadStabilePic(),
  reLoadPic: makeSelectReLoadPic(),
  draughtFrontPic: makeSelectDraughtFrontPic(),
  draughtMidPic: makeSelectDraughtMidPic(),
  draughtAfterPic: makeSelectDraughtAfterPic(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ActualBurdeningLayout);
