import { createSelector } from 'reselect/es';

const selectActualBurdeningLayoutDomain = () => state => state.actualBurdeningLayout;
const selectShipStateDomain = () => state => state.shipWork.data;

const makeSelectUploadActualLayoutSuccess = () => createSelector(
  selectActualBurdeningLayoutDomain(),
  (subState) => {
    console.debug(subState.success);
    return subState.success;
  },
);

const makeSelectShipName = () => createSelector(
  selectShipStateDomain(),
  subState => subState.shipName,
);

const makeSelectVoyageCode = () => createSelector(
  selectShipStateDomain(),
  subState => subState.voyageCode,
);

const makeSelectPortName = () => createSelector(
  selectShipStateDomain(),
  subState => subState.portName,
);
const makeSelectReLoadPic = () => createSelector(
  selectActualBurdeningLayoutDomain(),
  subState => subState.data && subState.data.reLoadPic,
);
const makeSelectReLoadStabilePic = () => createSelector(
  selectActualBurdeningLayoutDomain(),
  subState => subState.data && subState.data.reLoadStabilePic,
);
const makeSelectDraughtFrontPic = () => createSelector(
  selectActualBurdeningLayoutDomain(),
  subState => subState.data && subState.data.draughtFrontPic,
);
const makeSelectDraughtMidPic = () => createSelector(
  selectActualBurdeningLayoutDomain(),
  subState => subState.data && subState.data.draughtMidPic,
);
const makeSelectDraughtAfterPic = () => createSelector(
  selectActualBurdeningLayoutDomain(),
  subState => subState.data && subState.data.draughtAfterPic,
);

export {
  selectActualBurdeningLayoutDomain,
  makeSelectUploadActualLayoutSuccess,
  makeSelectShipName,
  makeSelectVoyageCode,
  makeSelectPortName,
  makeSelectReLoadPic,
  makeSelectReLoadStabilePic,
  makeSelectDraughtFrontPic,
  makeSelectDraughtMidPic,
  makeSelectDraughtAfterPic,
};
