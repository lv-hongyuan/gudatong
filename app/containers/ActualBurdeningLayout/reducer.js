import {
  UPLOAD_ACTUAL_LAYOUT_ACTION,
  GET_ACTUAL_LAYOUT_ACTION,
  UPLOAD_ACTUAL_LAYOUT_ACTION_RESULT,
  GET_ACTUAL_LAYOUT_ACTION_RESULT,
} from './constants';

const initState = {
  success: false,
  data: {
    reLoadPic: '',
    reLoadStabilePic: '',
  },
};

export default function (state = initState, action) {
  switch (action.type) {
    case UPLOAD_ACTUAL_LAYOUT_ACTION:
    case GET_ACTUAL_LAYOUT_ACTION:
      return { ...state, success: false };
    case UPLOAD_ACTUAL_LAYOUT_ACTION_RESULT:
      return { ...state, success: action.payload.success };
    case GET_ACTUAL_LAYOUT_ACTION_RESULT:
      return { ...state, success: action.payload.success, data: action.payload.data };
    default:
      return state;
  }
}
