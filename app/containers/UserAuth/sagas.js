import { call, put, select, takeLatest } from 'redux-saga/effects';
import { USER_AUTH, GET_USER_AUTH } from './constants';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import { shipUserAutResultAction, getShipUserAutResultAction, getShipUserAutAction } from './actions';
import { errorMessage } from '../ErrorHandler/actions';
import { selectUserId, selectUserIdCard } from './selectors';
import uploadImage from '../../common/uploadImage';

function* shipUserAut(action) {
  console.log(action);
  try {
    const userId = yield select(selectUserId);
    const cardId = yield select(selectUserIdCard);
    const {
      autPicBehind,
      autPicBridge,
      autPicFront,
      autPicPanorama,
    } = action.payload;

    const param = {
      id: userId,
      idCard: cardId,
    };

    if (autPicBehind && autPicBehind.path) {
      const response = yield call(uploadImage, autPicBehind, 'ship_back', userId);
      console.log('upload', response);
      param.autPicBehind = response.id;
    } else {
      param.autPicBehind = autPicBehind.substr(autPicBehind.lastIndexOf('/') + 1);
    }
    if (autPicBridge && autPicBridge.path) {
      const response = yield call(uploadImage, autPicBridge, 'ship_bridge', userId);
      console.log('upload', response);
      param.autPicBridge = response.id;
    } else {
      param.autPicBridge = autPicBridge.substr(autPicBridge.lastIndexOf('/') + 1);
    }
    if (autPicFront && autPicFront.path) {
      const response = yield call(uploadImage, autPicFront, 'ship_front', userId);
      console.log('upload', response);
      param.autPicFront = response.id;
    } else {
      param.autPicFront = autPicFront.substr(autPicFront.lastIndexOf('/') + 1);
    }
    if (autPicPanorama && autPicPanorama.path) {
      const response = yield call(uploadImage, autPicPanorama, 'ship_panorama', userId);
      console.log('upload', response);
      param.autPicPanorama = response.id;
    } else {
      param.autPicPanorama = autPicPanorama.substr(autPicPanorama.lastIndexOf('/') + 1);
    }

    const response = yield call(request, ApiFactory.shipUserAut(param));
    console.log('upload', response);
    yield put(shipUserAutResultAction(response, true));
    yield put(getShipUserAutAction());
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(shipUserAutResultAction(undefined, false));
  }
}

export function* shipUserAutSaga() {
  yield takeLatest(USER_AUTH, shipUserAut);
}

function* getShipUserAut(action) {
  console.log(action);
  try {
    const response = yield call(request, ApiFactory.getAutDetail());
    console.log(response);
    yield put(getShipUserAutResultAction(response, true));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(getShipUserAutResultAction(undefined, false));
  }
}

export function* getShipUserAutSaga() {
  yield takeLatest(GET_USER_AUTH, getShipUserAut);
}

// All sagas to be loaded
export default [
  shipUserAutSaga,
  getShipUserAutSaga,
];
