import { USER_AUTH_RESULT, GET_USER_AUTH_RESULT, GET_USER_AUTH, USER_AUTH } from './constants';

const initState = {
  success: false,
  data: {
    autPass: null,
    autPicBehind: '',
    autPicBridge: '',
    autPicFront: '',
    autPicPanorama: '',
  },
};

export default function (state = initState, action) {
  switch (action.type) {
    case GET_USER_AUTH:
    case USER_AUTH:
      return { ...state, success: false };
    case USER_AUTH_RESULT:
      return { ...state, success: action.payload.success };
    case GET_USER_AUTH_RESULT:
      return { ...state, success: action.payload.success, data: action.payload.data };
    default:
      return state;
  }
}
