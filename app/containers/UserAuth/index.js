import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, RefreshControl } from 'react-native';
import { connect } from 'react-redux';
import { Container, Content, Form, Item, Label } from 'native-base';
import { NavigationActions } from 'react-navigation';
import { createStructuredSelector } from 'reselect/es';
import HeaderButtons from 'react-navigation-header-buttons';
import { shipUserAutAction, getShipUserAutAction } from './actions';
import {
  makeSelectUserAuthSuccess,
  makeSelectShipName,
  makeSelectUserCardNo,
  makeSelectUserName,
  makeSelectShipBack,
  makeSelectShipBrige,
  makeSelectShipFront,
  makeSelectShipPanorama, makeSelectAuthState, makeSelectCanEdit,
} from './selectors';
import SelectImageView from '../../components/SelectImageView';
import { errorMessage } from '../ErrorHandler/actions';
import { AuthState } from './constants';
import { ROUTE_APPLICATION } from '../../RouteConstant';
import Svg from '../../components/Svg';
import screenHOC from '../screenHOC';

const styles = StyleSheet.create({
  imageStyle: {
    width: 144,
    height: 144,
  },
  imageTouch: {
    width: 144,
    height: 144,
    backgroundColor: '#E0E0E0',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    borderBottomColor: 'transparent',
  },
  label: {},
  text: {
    flex: 2,
  },
  item: {
    flex: 1,
    borderColor: 'transparent',
    marginTop: 10,
  },
  itemImage: {
    flex: 1,
    borderColor: 'transparent',
    paddingTop: 10,
  },
  container: {
    backgroundColor: '#FFFFFF',
    paddingLeft: 20,
    paddingTop: 10,
    paddingBottom: 10,
    paddingRight: 20,
  },
});

/**
 * Created by jianzhexu on 2018/4/4
 */
@screenHOC
class UserAuth extends React.PureComponent {
  static navigationOptions = ({ navigation }) => ({
    title: '填写个人信息',
    headerBackTitle: null,
    gesturesEnabled: false,
    headerStyle: {
      backgroundColor: '#DC001B',
    },
    headerTitleStyle: {
      fontSize: 19,
    },
    headerTintColor: '#ffffff',
    headerLeft: (
      <HeaderButtons color="white">
        <HeaderButtons.Item
          title=""
          buttonWrapperStyle={{ padding: 10 }}
          ButtonElement={<Svg icon="icon_back" size="20" color="white" />}
          onPress={() => {
            navigation.dispatch(NavigationActions.back());
          }}
        />
      </HeaderButtons>
    ),
    headerRight: (
      <HeaderButtons>
        <HeaderButtons.Item
          title="保存"
          buttonStyle={{ fontSize: 14, color: '#ffffff' }}
          onPress={() => {
            // navigation.navigate(ROUTE_APPLICATION);
            const handleSubmitFunc = navigation.getParam('submit', undefined);
            if (handleSubmitFunc) {
              requestAnimationFrame(() => {
                handleSubmitFunc();
              });
            }
          }}
        />
      </HeaderButtons>
    ),
  });

  static propTypes = {
    navigation: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    success: PropTypes.bool,
    userName: PropTypes.string,
    authState: PropTypes.number,
    canEdit: PropTypes.bool,
    cardNo: PropTypes.string,
    shipName: PropTypes.string,
    ship_bridge: PropTypes.string,
    ship_front: PropTypes.string,
    ship_back: PropTypes.string,
    ship_panorama: PropTypes.string,
  };

  static defaultProps = {
    success: undefined,
    userName: undefined,
    authState: undefined,
    canEdit: undefined,
    cardNo: undefined,
    shipName: undefined,
    ship_bridge: undefined,
    ship_front: undefined,
    ship_back: undefined,
    ship_panorama: undefined,
  };

  constructor(props) {
    super(props);
    this.state = {
      ship_bridge: null,
      ship_front: null,
      ship_back: null,
      ship_panorama: null,
      isInit: false,
      isSubmit: false,
      isRefreshing: false,
    };
    this.props.navigation.setParams({ submit: this.handleSubmit });
  }

  componentDidMount() {
    this.setState({
      isInit: true,
    }, () => {
      this.props.dispatch(getShipUserAutAction());
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.success && this.state.isSubmit) {
      // 提交认证
      // this.props.navigation.navigate(ROUTE_APPLICATION);
    }

    if (nextProps.success && this.state.isInit) {
      // 初始化结束
      this.setState({
        isInit: false,
        isRefreshing: false,
        ship_bridge: nextProps.ship_bridge,
        ship_front: nextProps.ship_front,
        ship_back: nextProps.ship_back,
        ship_panorama: nextProps.ship_panorama,
      });
      if (nextProps.authState === AuthState.Passed) {
        this.props.navigation.navigate(ROUTE_APPLICATION);
      }
    }
  }

  handleSubmit = () => {
    if (!this.props.canEdit) {
      this.props.navigation.goBack();
      return;
    }
    this.setState({
      isSubmit: true,
    }, () => {
      if (!this.state.ship_bridge) {
        this.props.dispatch(errorMessage('请选择驾驶台照片'));
        return;
      }
      if (!this.state.ship_back) {
        this.props.dispatch(errorMessage('请选择船后照片'));
        return;
      }
      if (!this.state.ship_front) {
        this.props.dispatch(errorMessage('请选择船前照片'));
        return;
      }
      if (!this.state.ship_panorama) {
        this.props.dispatch(errorMessage('请选择全景照片'));
        return;
      }
      this.props.dispatch(shipUserAutAction({
        autPicBehind: this.state.ship_back,
        autPicBridge: this.state.ship_bridge,
        autPicFront: this.state.ship_front,
        autPicPanorama: this.state.ship_panorama,
      }));
    });
  };

  renderState() {
    switch (this.props.authState) {
      case AuthState.Pending:
        return (
          <Item style={styles.item}>
            <Label>正在审核中，请耐心等待</Label>
          </Item>
        );
      case AuthState.Failed:
        return (
          <Item style={styles.item}>
            <Label>审核失败，请重新提交审核</Label>
          </Item>
        );
      case AuthState.Expired:
        return (
          <Item style={styles.item}>
            <Label>申请已过期，请重新提交审核</Label>
          </Item>
        );
      case AuthState.Passed:
        return (
          <Item style={styles.item}>
            <Label>审核已通过</Label>
          </Item>
        );
      default:
        return null;
    }
  }

  render() {
    return (
      <Container>
        <Content
          refreshControl={
            <RefreshControl
              refreshing={this.state.isRefreshing}
              onRefresh={() => {
                this.setState({
                  isRefreshing: true,
                  isInit: true,
                }, () => {
                  this.props.dispatch(getShipUserAutAction());
                });
              }}
            />
          }
        >
          <Form style={{
            backgroundColor: '#FFFFFF',
            paddingBottom: 20,
          }}
          >
            {this.renderState()}
            <Item style={styles.item}>
              <Label>用户名:</Label>
              <Label>{this.props.userName}</Label>
            </Item>
            <Item style={styles.item}>
              <Label>身份证:</Label>
              <Label>{this.props.cardNo}</Label>
            </Item>
            <Item style={styles.item}>
              <Label>船名:</Label>
              <Label>{this.props.shipName}</Label>
            </Item>
            <Item style={styles.itemImage}>
              <Label>驾驶台:</Label>
              <SelectImageView
                title="选择驾驶台照片"
                source={this.state.ship_bridge}
                readonly={!this.props.canEdit}
                onPickImages={(images) => {
                  const image = images[0];
                  this.setState({
                    ship_bridge: {
                      fileName: image.uri.split('/').reverse()[0],
                      fileSize: image.size,
                      path: image.uri,
                      uri: image.uri,
                    },
                  });
                }}
                onDelete={() => {
                  this.setState({
                    ship_bridge: undefined,
                  });
                }}
              />
            </Item>

            <Item style={styles.itemImage}>
              <Label>船前照:</Label>
              <SelectImageView
                title="选择船前照片"
                source={this.state.ship_front}
                readonly={!this.props.canEdit}
                onPickImages={(images) => {
                  const image = images[0];
                  this.setState({
                    ship_front: {
                      fileName: image.uri.split('/').reverse()[0],
                      fileSize: image.size,
                      path: image.uri,
                      uri: image.uri,
                    },
                  });
                }}
                onDelete={() => {
                  this.setState({
                    ship_front: undefined,
                  });
                }}
              />
            </Item>

            <Item style={styles.itemImage}>
              <Label>船后照:</Label>
              <SelectImageView
                title="选择船后照片"
                source={this.state.ship_back}
                readonly={!this.props.canEdit}
                onPickImages={(images) => {
                  const image = images[0];
                  this.setState({
                    ship_back: {
                      fileName: image.uri.split('/').reverse()[0],
                      fileSize: image.size,
                      path: image.uri,
                      uri: image.uri,
                    },
                  });
                }}
                onDelete={() => {
                  this.setState({
                    ship_back: undefined,
                  });
                }}
              />
            </Item>
            <Item style={styles.itemImage}>
              <Label>全景照:</Label>
              <SelectImageView
                title="选择全景照片"
                source={this.state.ship_panorama}
                readonly={!this.props.canEdit}
                onPickImages={(images) => {
                  const image = images[0];
                  this.setState({
                    ship_panorama: {
                      fileName: image.uri.split('/').reverse()[0],
                      fileSize: image.size,
                      path: image.uri,
                      uri: image.uri,
                    },
                  });
                }}
                onDelete={() => {
                  this.setState({
                    ship_panorama: undefined,
                  });
                }}
              />
            </Item>
          </Form>
        </Content>
      </Container>);
  }
}

const mapStateToProps = createStructuredSelector({
  success: makeSelectUserAuthSuccess(),
  userName: makeSelectUserName(),
  authState: makeSelectAuthState(),
  canEdit: makeSelectCanEdit(),
  cardNo: makeSelectUserCardNo(),
  shipName: makeSelectShipName(),
  ship_bridge: makeSelectShipBrige(),
  ship_front: makeSelectShipFront(),
  ship_back: makeSelectShipBack(),
  ship_panorama: makeSelectShipPanorama(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(UserAuth);
