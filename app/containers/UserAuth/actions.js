import { USER_AUTH, USER_AUTH_RESULT, GET_USER_AUTH, GET_USER_AUTH_RESULT } from './constants';

export function shipUserAutAction(data) {
  return {
    type: USER_AUTH,
    payload: data,
  };
}

export function shipUserAutResultAction(data, success) {
  return {
    type: USER_AUTH_RESULT,
    payload: {
      data,
      success,
    },
  };
}

export function getShipUserAutAction() {
  return {
    type: GET_USER_AUTH,
  };
}

export function getShipUserAutResultAction(data, success) {
  return {
    type: GET_USER_AUTH_RESULT,
    payload: {
      data,
      success,
    },
  };
}
