/*
 *
 * Login constants
 *
 */
export const USER_AUTH = 'app/UserAuth/USER_AUTH';
export const USER_AUTH_RESULT = 'app/UserAuth/USER_AUTH_RESULT';
export const GET_USER_AUTH = 'app/UserAuth/GET_USER_AUTH';
export const GET_USER_AUTH_RESULT = 'app/UserAuth/GET_USER_AUTH_RESULT';

export const AuthState = {
  Initial: 0, // 初始
  Pending: 10, // 待审核
  Failed: 20, // 未通过
  Expired: 25, // 过期
  Passed: 30, // 通过
};
