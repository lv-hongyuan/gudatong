import { createSelector } from 'reselect/es';
import { AuthState } from './constants';

const selectUserAuthDomain = () => state => state.userAuth;
const selectUserId = state => state.login.user.id;
const selectUserIdCard = state => state.login.user.idCard;
const selectUserDomain = () => state => state.login;

const makeSelectUserAuthSuccess = () => createSelector(
  selectUserAuthDomain(),
  (subState) => {
    console.debug(subState.success);
    return subState.success;
  },
);

const makeSelectUserName = () => createSelector(
  selectUserDomain(),
  subState => subState.user.userName,
);

const makeSelectUserCardNo = () => createSelector(
  selectUserDomain(),
  subState => subState.user.idCard,
);

const makeSelectShipName = () => createSelector(
  selectUserDomain(),
  subState => subState.user.shipName,
);

const makeSelectAuthState = () => createSelector(
  selectUserAuthDomain(),
  subState => subState.data && subState.data.autPass,
);

const makeSelectCanEdit = () => createSelector(
  selectUserAuthDomain(),
  subState => subState.data && subState.data.autPass !== AuthState.Passed,
);

const makeSelectShipBrige = () => createSelector(
  selectUserAuthDomain(),
  subState => subState.data && subState.data.autPicBridge,
);

const makeSelectShipBack = () => createSelector(
  selectUserAuthDomain(),
  subState => subState.data && subState.data.autPicBehind,
);

const makeSelectShipFront = () => createSelector(
  selectUserAuthDomain(),
  subState => subState.data && subState.data.autPicFront,
);

const makeSelectShipPanorama = () => createSelector(
  selectUserAuthDomain(),
  subState => subState.data && subState.data.autPicPanorama,
);

export {
  makeSelectUserAuthSuccess,
  selectUserIdCard,
  selectUserId,
  makeSelectUserName,
  makeSelectUserCardNo,
  makeSelectShipName,
  makeSelectShipBrige,
  makeSelectShipBack,
  makeSelectShipFront,
  makeSelectShipPanorama,
  makeSelectAuthState,
  makeSelectCanEdit,
};
