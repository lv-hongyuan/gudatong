import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  GET_DX_RECORDS,
  DELETE_DX_RECORD,
} from './constants';

export const getDxRecordsRoutine = createRoutine(
  GET_DX_RECORDS,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const getDxRecordsPromise = promisifyRoutine(getDxRecordsRoutine);

export const deleteDxRecordRoutine = createRoutine(
  DELETE_DX_RECORD,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const deleteDxRecordPromise = promisifyRoutine(deleteDxRecordRoutine);
