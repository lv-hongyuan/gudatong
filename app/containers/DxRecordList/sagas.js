import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import { getDxRecordsRoutine, deleteDxRecordRoutine } from './actions';
import { errorMessage } from '../ErrorHandler/actions';
import { getPopId } from '../ShipWork/sagas';

function* getDxRecords(action) {
  console.log(action);
  try {
    yield put(getDxRecordsRoutine.request());
    const popId = yield call(getPopId);
    const response = yield call(
      request,
      ApiFactory.findInvertedBoxRecords(popId),
    );
    console.log('getDxRecords', response.toString());
    yield put(getDxRecordsRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(getDxRecordsRoutine.failure(e));
  } finally {
    yield put(getDxRecordsRoutine.fulfill());
  }
}

export function* getDxRecordsSaga() {
  yield takeLatest(getDxRecordsRoutine.TRIGGER, getDxRecords);
}

function* deleteDxRecord(action) {
  console.log(action);
  try {
    yield put(deleteDxRecordRoutine.request());
    const id = action.payload;
    const response = yield call(
      request,
      ApiFactory.deleteInvertedBoxRecord(id),
    );
    console.log('deleteDxRecord', response.toString());
    yield put(deleteDxRecordRoutine.success(id));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(deleteDxRecordRoutine.failure(e));
  } finally {
    yield put(deleteDxRecordRoutine.fulfill());
  }
}

export function* deleteDxRecordSaga() {
  yield takeLatest(deleteDxRecordRoutine.TRIGGER, deleteDxRecord);
}

// All sagas to be loaded
export default [
  getDxRecordsSaga,
  deleteDxRecordSaga,
];
