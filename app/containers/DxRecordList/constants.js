/*
 *
 * DxRecordList constants
 *
 */
export const GET_DX_RECORDS = 'app/DxRecordList/GET_DX_RECORDS';

export const DELETE_DX_RECORD = 'app/DxRecordList/DELETE_DX_RECORD';
