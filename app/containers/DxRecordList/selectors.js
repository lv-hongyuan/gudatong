import { createSelector } from 'reselect/es';

const selectDxRecordsDomain = () => state => state.dxRecordList;
const selectShipStateDomain = () => state => state.shipWork.data;

const makeDangerousGoodsRecords = () => createSelector(
  selectDxRecordsDomain(),
  (subState) => {
    console.debug(subState.data);
    return subState.data;
  },
);

const makeSelectPortName = () => createSelector(
  selectShipStateDomain(),
  subState => subState.portName,
);

export {
  makeDangerousGoodsRecords,
  makeSelectPortName,
};
