import React from 'react';
import PropTypes from 'prop-types';
import { SwipeableFlatList, RefreshControl } from 'react-native';
import HeaderButtons from 'react-navigation-header-buttons';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { connect } from 'react-redux';
import { Container, View, Button, Icon } from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import { ROUTE_EDIT_DX_RECORD } from '../../RouteConstant';
import { makeSelectIsSail } from '../ShipWork/selectors';
import { getDxRecordsPromise, deleteDxRecordPromise } from './actions';
import myTheme from '../../Themes';
import { makeDangerousGoodsRecords, makeSelectPortName } from './selectors';
import EmptyView from '../../components/EmptyView';
import screenHOC from '../screenHOC';
import DxRecordItem from '../../components/DxRecordItem';

/**
 * 倒箱记录页面
 * Created by jianzhexu on 2018/3/29
 */
@screenHOC
class DxRecordList extends React.PureComponent {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.getParam('title', '倒箱记录'),
    headerRight: (
      <HeaderButtons>
        <HeaderButtons.Item
          title="创建"
          buttonStyle={{ fontSize: 14, color: '#ffffff' }}
          onPress={navigation.getParam('onBtnCreate')}
        />
      </HeaderButtons>
    ),
  });
  static propTypes = {
    navigation: PropTypes.object.isRequired,
    getDxRecords: PropTypes.func.isRequired,
    deleteDxRecord: PropTypes.func.isRequired,
    isSail: PropTypes.bool.isRequired,
    portName: PropTypes.string.isRequired,
    data: PropTypes.array.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
    };
    this.props.navigation.setParams({ onBtnCreate: this.onBtnCreate });
  }

  componentDidMount() {
    this.reLoadingPage();
  }

  onBtnCreate = () => {
    this.props.navigation.navigate(ROUTE_EDIT_DX_RECORD, {
      title: '创建倒箱记录',
      onGoBack: () => {
        this.reLoadingPage();
      },
      isEdit: false,
      isVoyage: this.props.isSail,
    });
  };

  reLoadingPage = () => {
    requestAnimationFrame(() => {
      this.props.getDxRecords()
        .then(() => {
          this.setState({ refreshing: false });
        })
        .catch(() => {
          this.setState({ refreshing: false });
        });
    });
  };

  itemPress = (data) => {
    this.props.navigation.navigate(ROUTE_EDIT_DX_RECORD, {
      title: '编辑倒箱记录',
      onGoBack: () => {
        this.reLoadingPage();
      },
      isEdit: true,
      data,
      isVoyage: this.props.isSail,
    });
  };

  deleteItem = (item) => {
    this.props.deleteDxRecord(item.id)
      .then(() => {
        this.reLoadingPage();
      })
      .catch(() => {
      });
  };

  renderItem = ({ item }) => (
    <DxRecordItem
      id={item.id}
      popId={item.popId}
      contNumber={item.xl}
      reason={item.reason}
      portName={this.props.portName}
      onDelete={this.deleteItem}
      onItemPress={this.itemPress}
    />
  );

  renderEmptyView = () => (<EmptyView />);

  renderQuickActions = ({ item }) => (
    <Button
      danger
      onPress={() => this.deleteItem(item)}
      style={{
        flex: 1, width: 80, borderRadius: 0, alignSelf: 'flex-end', justifyContent: 'center', elevation: 0,
      }}
    >
      <Icon active name="trash" />
    </Button>
  );

  render() {
    return (
      <Container theme={myTheme}>
        <SwipeableFlatList
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => {
                this.setState({ refreshing: true });
                this.reLoadingPage();
              }}
            />
          }
          contentInsetAdjustmentBehavior="scrollableAxes"
          style={{ flex: 1, backgroundColor: '#e0e0e0' }}
          keyExtractor={item => `${item.id}`}
          data={this.props.data}
          ListEmptyComponent={this.renderEmptyView}
          initialNumToRender={10}
          renderItem={this.renderItem}
          renderQuickActions={this.renderQuickActions}
          maxSwipeDistance={80}
          ItemSeparatorComponent={() => (<View style={{ height: 5 }} />)}
          ListFooterComponent={() => (<View style={{ height: 5 }} />)}
          ListHeaderComponent={() => (<View style={{ height: 5 }} />)}
        />
      </Container>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  data: makeDangerousGoodsRecords(),
  portName: makeSelectPortName(),
  isSail: makeSelectIsSail(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getDxRecords: getDxRecordsPromise,
      deleteDxRecord: deleteDxRecordPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(DxRecordList);
