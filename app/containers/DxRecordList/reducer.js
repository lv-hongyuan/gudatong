import {
  getDxRecordsRoutine,
  deleteDxRecordRoutine,
} from './actions';

const initState = {
  data: [],
};

export default function (state = initState, action) {
  switch (action.type) {
    case getDxRecordsRoutine.TRIGGER:
      return { ...state, loading: true };
    case getDxRecordsRoutine.SUCCESS:
      return { ...state, data: action.payload.dtoList };
    case getDxRecordsRoutine.FAILURE:
      return { ...state, error: action.payload };
    case getDxRecordsRoutine.FULFILL:
      return { ...state, loading: false };

    case deleteDxRecordRoutine.TRIGGER:
      return { ...state, loading: true };
    case deleteDxRecordRoutine.SUCCESS: {
      const id = action.payload;
      const list = state.data || [];
      const index = list.findIndex(item => item.id === id);
      if (index !== -1) {
        list.splice(index, 1);
      }
      return { ...state, data: list };
    }
    case deleteDxRecordRoutine.FAILURE:
      return { ...state, error: action.payload };
    case deleteDxRecordRoutine.FULFILL:
      return { ...state, loading: false };

    default:
      return state;
  }
}
