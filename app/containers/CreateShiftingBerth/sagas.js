import { call, put, takeLatest } from 'redux-saga/effects';
import { CREATE_SHIFTING_BERTH_ACTION, UPDATE_SHIFTING_BERTH_ACTION } from './constants';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import { createShiftingBerthResultAction, updateShiftingBerthResultAction } from './actions';
import { errorMessage } from '../ErrorHandler/actions';

function* createShiftingBerth(action) {
  console.log(action);
  try {
    const response = yield call(request, ApiFactory.createShiftRecord(action.payload));
    yield put(createShiftingBerthResultAction(response, true));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(createShiftingBerthResultAction(undefined, false));
  }
}

export function* createShiftingBerthSaga() {
  yield takeLatest(CREATE_SHIFTING_BERTH_ACTION, createShiftingBerth);
}

function* updateShiftingBerth(action) {
  console.log(action);
  try {
    const response = yield call(request, ApiFactory.updateShiftRecord(action.payload));
    yield put(updateShiftingBerthResultAction(response, true));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(updateShiftingBerthResultAction(undefined, false));
  }
}

export function* updateShiftingBerthSaga() {
  yield takeLatest(UPDATE_SHIFTING_BERTH_ACTION, updateShiftingBerth);
}

// All sagas to be loaded
export default [
  createShiftingBerthSaga,
  updateShiftingBerthSaga,
];
