import {
  CREATE_SHIFTING_BERTH_ACTION,
  CREATE_SHIFTING_BERTH_ACTION_RESULT,
  UPDATE_SHIFTING_BERTH_ACTION,
  UPDATE_SHIFTING_BERTH_ACTION_RESULT,
} from './constants';

export function createShiftingBerthAction(data) {
  return {
    type: CREATE_SHIFTING_BERTH_ACTION,
    payload: data,
  };
}

export function createShiftingBerthResultAction(data, success) {
  return {
    type: CREATE_SHIFTING_BERTH_ACTION_RESULT,
    payload: {
      data,
      success,
    },
  };
}

export function updateShiftingBerthAction(data) {
  return {
    type: UPDATE_SHIFTING_BERTH_ACTION,
    payload: data,
  };
}

export function updateShiftingBerthResultAction(success) {
  return {
    type: UPDATE_SHIFTING_BERTH_ACTION_RESULT,
    payload: {
      success,
    },
  };
}

