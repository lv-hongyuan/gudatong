import {
  CREATE_SHIFTING_BERTH_ACTION_RESULT,
  UPDATE_SHIFTING_BERTH_ACTION_RESULT,
  CREATE_SHIFTING_BERTH_ACTION,
  UPDATE_SHIFTING_BERTH_ACTION,
} from './constants';

const initState = {
  success: false,
};

export default function (state = initState, action) {
  switch (action.type) {
    case CREATE_SHIFTING_BERTH_ACTION:
    case UPDATE_SHIFTING_BERTH_ACTION:
      return { ...state, success: false };
    case UPDATE_SHIFTING_BERTH_ACTION_RESULT:
    case CREATE_SHIFTING_BERTH_ACTION_RESULT:
      return { ...state, success: action.payload.success };
    default:
      return state;
  }
}
