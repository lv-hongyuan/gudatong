import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { Container, Content, Item, Label, View, InputGroup, Text } from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import HeaderButtons from 'react-navigation-header-buttons';
import { makeSelectPopId, makeSelectPortName, makeSelectCreateShiftingBerthSuccess } from './selectors';
import { createShiftingBerthAction, updateShiftingBerthAction } from './actions';
import { errorMessage } from '../ErrorHandler/actions';
import commonStyles from '../../common/commonStyles';
import InputItem from '../../components/InputItem';
import DatePullSelector from '../../components/DatePullSelector';
import screenHOC from '../screenHOC';
import { MaskType } from '../../components/InputItem/TextInput';
import AlertView from '../../components/Alert';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    paddingLeft: 20,
    paddingTop: 10,
    paddingBottom: 10,
    paddingRight: 20,
  },
});

/**
 * 创建移泊记录
 * Created by jianzhexu on 2018/3/26
 */
@screenHOC
class CreateShiftingBerth extends React.PureComponent {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.getParam('title', '创建移泊记录'),
    headerRight: (
      <HeaderButtons>
        <HeaderButtons.Item
          title="保存"
          buttonStyle={{ fontSize: 14, color: '#ffffff' }}
          onPress={() => {
            const handleSubmitFunc = navigation.getParam('submit', undefined);
            if (handleSubmitFunc) {
              requestAnimationFrame(() => {
                handleSubmitFunc();
              });
            }
          }}
        />
      </HeaderButtons>
    ),
  });
  static propTypes = {
    navigation: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    success: PropTypes.object.isRequired,
    popId: PropTypes.number.isRequired,
    portName: PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      isInit: false,
      portName: '',
      isUpdating: false,
      tugNum: '',
      shiftTimeStart: 0,
      shiftTimeEnd: 0,
      id: 0,
      popId: 0,
    };
    this.props.navigation.setParams({ submit: this.handleSubmit });
  }

  componentWillMount() {
    this.setState({
      isInit: true,
    });
  }

  componentWillReceiveProps(nextProps) {
    const data = nextProps.navigation.getParam('data', undefined);
    if (data && this.state.isInit) {
      this.setState(() => ({
        tugNum: data.tugNum,
        isInit: false,
        shiftTimeStart: data.shiftTimeStart,
        shiftTimeEnd: data.shiftTimeEnd,
        portName: data.portName,
        id: data.id,
        popId: data.popId,
      }));
    }

    if (nextProps.success && this.state.isUpdating) {
      const goBackCallback = this.props.navigation.getParam('onGoBack', undefined);
      if (goBackCallback) {
        goBackCallback();
      }
      this.props.navigation.goBack();
      this.setState({
        isUpdating: false,
      });
    }
  }

  handleSubmit = () => {

    if (this.state.shiftTimeStart > this.state.shiftTimeEnd) {
      AlertView.show({ message: '结束时间不能早于开始时间' });
      return;
    }

    this.setState({
      isUpdating: true,
    }, () => {
      const type = this.props.navigation.getParam('type', undefined);

      if (type) {
        this.props.dispatch(updateShiftingBerthAction({
          popId: this.state.popId,
          tugNum: this.state.tugNum,
          shiftTimeStart: this.state.shiftTimeStart,
          shiftTimeEnd: this.state.shiftTimeEnd,
          id: this.state.id,
        }));
      } else {
        const param = {
          popId: this.props.popId,
          portName: this.props.portName,
          tugNum: this.state.tugNum,
        };
        if (this.state.shiftTimeStart <= 0 && this.state.shiftTimeEnd <= 0) {
          this.props.dispatch(errorMessage('请选择开始移泊时间或结束移泊时间'));
          return;
        }
        if (this.state.shiftTimeStart > 0) {
          param.shiftTimeStart = this.state.shiftTimeStart;
        }
        if (this.state.shiftTimeEnd > 0) {
          param.shiftTimeEnd = this.state.shiftTimeEnd;
        }
        this.props.dispatch(createShiftingBerthAction(param));
      }
    });
  };

  render() {
    return (
      <Container>
        <Content>
          <View style={styles.container}>
            <InputGroup style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.readOnlyInputLabel}>发生港口:</Label>
                <Label
                  style={commonStyles.text}
                >{this.state.portName ? this.state.portName : this.props.portName}
                </Label>
              </Item>
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>开始移泊:</Label>
                <DatePullSelector
                  value={this.state.shiftTimeStart || null}
                  onChangeValue={(value) => {
                    this.setState({ shiftTimeStart: value });
                  }}
                />
              </Item>
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>结束移泊:</Label>
                <DatePullSelector
                  value={this.state.shiftTimeEnd || null}
                  onChangeValue={(value) => {
                    this.setState({ shiftTimeEnd: value });
                  }}
                />
              </Item>
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="使用拖轮:"
                value={this.state.tugNum}
                editable={!this.state.readonly}
                onChangeText={(text) => {
                  this.setState({ tugNum: text });
                }}
                keyboardType="numeric"
                maskType={MaskType.INTEGER}
                rightItem={<Text style={commonStyles.tail}>艘</Text>}
              />
            </InputGroup>
          </View>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  success: makeSelectCreateShiftingBerthSuccess(),
  portName: makeSelectPortName(),
  popId: makeSelectPopId(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateShiftingBerth);
