import { createSelector } from 'reselect/es';

const selectCreateShiftingBerthDomain = () => state => state.createShiftingBerth;
const selectShipStateDomain = () => state => state.shipWork.data;

const makeSelectCreateShiftingBerthSuccess = () => createSelector(
  selectCreateShiftingBerthDomain(),
  (subState) => {
    console.debug(subState.success);
    return subState.success;
  },
);

const makeSelectPortName = () => createSelector(
  selectShipStateDomain(),
  subState => subState.portName,
);

const makeSelectPopId = () => createSelector(
  selectShipStateDomain(),
  (subState) => {
    console.debug(subState.success);
    return subState.popId;
  },
);

export {
  makeSelectCreateShiftingBerthSuccess,
  selectCreateShiftingBerthDomain,
  makeSelectPortName,
  makeSelectPopId,
};
