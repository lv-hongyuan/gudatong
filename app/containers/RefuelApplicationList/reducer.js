import {
  getRefuelApplicationRoutine,
  deleteRefuelApplicationRoutine,
} from './actions';

const initState = {
  data: [],
};

export default function (state = initState, action) {
  switch (action.type) {
    case getRefuelApplicationRoutine.TRIGGER:
      return { ...state, loading: true };
    case getRefuelApplicationRoutine.SUCCESS:
      return { ...state, data: action.payload.dtoList };
    case getRefuelApplicationRoutine.FAILURE:
      return { ...state, error: action.payload };
    case getRefuelApplicationRoutine.FULFILL:
      return { ...state, loading: false };

    case deleteRefuelApplicationRoutine.TRIGGER:
      return { ...state, loading: true };
    case deleteRefuelApplicationRoutine.SUCCESS: {
      const id = action.payload;
      const list = state.data || [];
      const index = list.findIndex(item => item.id === id);
      if (index !== -1) {
        list.splice(index, 1);
      }
      return { ...state, data: list };
    }
    case deleteRefuelApplicationRoutine.FAILURE:
      return { ...state, error: action.payload };
    case deleteRefuelApplicationRoutine.FULFILL:
      return { ...state, loading: false };

    default:
      return state;
  }
}
