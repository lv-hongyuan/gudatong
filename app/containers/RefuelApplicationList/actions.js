import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  GET_REFUEL_APPLICATION,
  DELETE_REFUEL_APPLICATION,
} from './constants';

export const getRefuelApplicationRoutine = createRoutine(
  GET_REFUEL_APPLICATION,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const getRefuelApplicationPromise = promisifyRoutine(getRefuelApplicationRoutine);

export const deleteRefuelApplicationRoutine = createRoutine(
  DELETE_REFUEL_APPLICATION,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const deleteRefuelApplicationPromise = promisifyRoutine(deleteRefuelApplicationRoutine);
