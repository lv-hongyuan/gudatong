import React from 'react';
import PropTypes from 'prop-types';
import { FlatList, RefreshControl } from 'react-native';
import { connect } from 'react-redux';
import { Container, View } from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import HeaderButtons from 'react-navigation-header-buttons';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { ROUTE_EDIT_REFUEL_APPLICATION } from '../../RouteConstant';
import { makeSelectIsSail } from '../ShipWork/selectors';
import { getRefuelApplicationPromise, deleteRefuelApplicationPromise } from './actions';
import { errorMessage } from '../ErrorHandler/actions';
import myTheme from '../../Themes';
import { makeRefuelApplicationList } from './selectors';
import EmptyView from '../../components/EmptyView';
import screenHOC from '../screenHOC';
import RefuelApplicationItem from '../../components/RefuelApplicationItem';
import { ReviewState, reviewTitleFromState } from '../../common/Constant';

/**
 * 加油申请列表页面
 * Created by jianzhexu on 2018/3/29
 */
@screenHOC
class RefuelApplicationList extends React.PureComponent {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.getParam('title', '加油申请'),
    headerRight: (
      <HeaderButtons>
        <HeaderButtons.Item
          title="创建"
          buttonStyle={{ fontSize: 14, color: '#ffffff' }}
          onPress={navigation.getParam('onBtnCreate')}
        />
      </HeaderButtons>
    ),
  });
  static propTypes = {
    navigation: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    data: PropTypes.array.isRequired,
    isSail: PropTypes.bool.isRequired,
    getRefuelApplication: PropTypes.func.isRequired,
    deleteRefuelApplication: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
    };
    this.props.navigation.setParams({ onBtnCreate: this.onBtnCreate.bind(this) });
  }

  componentDidMount() {
    this.reLoadingPage();
  }

  onBtnCreate = () => {
    this.props.navigation.navigate(ROUTE_EDIT_REFUEL_APPLICATION, {
      title: '创建加油申请',
      onGoBack: () => {
        this.reLoadingPage();
      },
      isEdit: false,
      isVoyage: this.props.isSail,
    });
  };

  reLoadingPage = () => {
    requestAnimationFrame(() => {
      this.props.getRefuelApplication()
        .then(() => {
          this.setState({ refreshing: false });
        })
        .catch(() => {
          this.setState({ refreshing: false });
        });
    });
  };

  itemPress = (data) => {
    if (data.state === ReviewState.Approved || data.state === ReviewState.Dismissed) {
      this.props.dispatch(errorMessage(`申请${reviewTitleFromState(data.state)}，无法编辑`));
      return;
    }
    this.props.navigation.navigate(ROUTE_EDIT_REFUEL_APPLICATION, {
      title: '编辑加油申请',
      onGoBack: () => {
        this.reLoadingPage();
      },
      isEdit: true,
      data,
      isVoyage: this.props.isSail,
    });
  };

  deleteItem = (data) => {
    if (data.state === ReviewState.Approved || data.state === ReviewState.Dismissed) {
      this.props.dispatch(errorMessage(`申请${reviewTitleFromState(data.state)}，无法删除`));
      return;
    }
    this.props.deleteRefuelApplication(data.id)
      .then(() => {
        this.reLoadingPage();
      })
      .catch(() => {
      });
  };

  renderItem = ({ item }) => (
    <RefuelApplicationItem
      id={item.id}
      shipId={item.shipId}
      shipName={item.shipName}
      portId={item.portId}
      portName={item.portName}
      etaTime={item.etaTime}
      oil180={item.oil180}
      oil120={item.oil120}
      oil0={item.oil0}
      state={item.state}
      onDelete={this.deleteItem}
      onItemPress={this.itemPress}
    />
  );

  renderEmptyView = () => (<EmptyView />);

  render() {
    return (
      <Container theme={myTheme}>
        <FlatList
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => {
                this.setState({ refreshing: true });
                this.reLoadingPage();
              }}
            />
          }
          contentInsetAdjustmentBehavior="scrollableAxes"
          style={{ flex: 1, backgroundColor: '#e0e0e0' }}
          keyExtractor={item => `${item.id}`}
          data={this.props.data}
          ListEmptyComponent={this.renderEmptyView}
          initialNumToRender={10}
          renderItem={this.renderItem}
          ItemSeparatorComponent={() => (<View style={{ height: 5 }} />)}
          ListFooterComponent={() => (<View style={{ height: 5 }} />)}
          ListHeaderComponent={() => (<View style={{ height: 5 }} />)}
        />
      </Container>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  data: makeRefuelApplicationList(),
  isSail: makeSelectIsSail(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getRefuelApplication: getRefuelApplicationPromise,
      deleteRefuelApplication: deleteRefuelApplicationPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(RefuelApplicationList);
