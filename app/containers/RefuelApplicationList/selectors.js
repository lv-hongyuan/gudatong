import { createSelector } from 'reselect/es';

const selectRefuelApplicationDomain = () => state => state.refuelApplication;

const makeRefuelApplicationList = () => createSelector(
  selectRefuelApplicationDomain(),
  (subState) => {
    console.debug(subState.success);
    return subState.data;
  },
);

export {
  makeRefuelApplicationList,
};
