/*
 *
 * RefuelApplicationList constants
 *
 */
export const GET_REFUEL_APPLICATION = 'app/RefuelApplicationList/GET_REFUEL_APPLICATION';

export const DELETE_REFUEL_APPLICATION = 'app/RefuelApplicationList/DELETE_REFUEL_APPLICATION';
