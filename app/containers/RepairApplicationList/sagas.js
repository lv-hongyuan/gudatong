import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import { getRepairApplicationRoutine, deleteRepairApplicationRoutine } from './actions';
import { errorMessage } from '../ErrorHandler/actions';

function* getRepairApplication(action) {
  console.log(action);
  try {
    yield put(getRepairApplicationRoutine.request());
    const response = yield call(
      request,
      ApiFactory.getShipRepairApplayList(),
    );
    console.log('getRepairApplication', response.toString());
    yield put(getRepairApplicationRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(getRepairApplicationRoutine.failure(e));
  } finally {
    yield put(getRepairApplicationRoutine.fulfill());
  }
}

export function* getRepairApplicationSaga() {
  yield takeLatest(getRepairApplicationRoutine.TRIGGER, getRepairApplication);
}

function* deleteRepairApplication(action) {
  console.log(action);
  try {
    yield put(deleteRepairApplicationRoutine.request());
    const id = action.payload;
    const response = yield call(
      request,
      ApiFactory.deleteShipRepairApplay(id),
    );
    console.log('getRepairApplication', response.toString());
    yield put(deleteRepairApplicationRoutine.success(id));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(deleteRepairApplicationRoutine.failure(e));
  } finally {
    yield put(deleteRepairApplicationRoutine.fulfill());
  }
}

export function* deleteRepairApplicationSaga() {
  yield takeLatest(deleteRepairApplicationRoutine.TRIGGER, deleteRepairApplication);
}

// All sagas to be loaded
export default [
  getRepairApplicationSaga,
  deleteRepairApplicationSaga,
];
