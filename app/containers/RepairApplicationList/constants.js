/*
 *
 * RepairApplicationList constants
 *
 */
export const GET_REPAIR_APPLICATION = 'app/RepairApplicationList/GET_REPAIR_APPLICATION';

export const DELETE_REPAIR_APPLICATION = 'app/RepairApplicationList/DELETE_REPAIR_APPLICATION';
