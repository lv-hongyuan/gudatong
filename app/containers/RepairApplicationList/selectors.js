import { createSelector } from 'reselect/es';

const selectRepairApplicationDomain = () => state => state.repairApplication;

const makeRefuelApplicationList = () => createSelector(
  selectRepairApplicationDomain(),
  (subState) => {
    console.debug(subState.success);
    return subState.data;
  },
);

export {
  makeRefuelApplicationList,
};
