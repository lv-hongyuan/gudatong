import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  GET_REPAIR_APPLICATION,
  DELETE_REPAIR_APPLICATION,
} from './constants';

export const getRepairApplicationRoutine = createRoutine(
  GET_REPAIR_APPLICATION,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const getRepairApplicationPromise = promisifyRoutine(getRepairApplicationRoutine);

export const deleteRepairApplicationRoutine = createRoutine(
  DELETE_REPAIR_APPLICATION,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const deleteRepairApplicationPromise = promisifyRoutine(deleteRepairApplicationRoutine);
