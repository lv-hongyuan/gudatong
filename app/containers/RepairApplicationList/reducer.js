import {
  getRepairApplicationRoutine,
  deleteRepairApplicationRoutine,
} from './actions';

const initState = {
  data: [],
};

export default function (state = initState, action) {
  switch (action.type) {
    case getRepairApplicationRoutine.TRIGGER:
      return { ...state, loading: true };
    case getRepairApplicationRoutine.SUCCESS:
      return { ...state, data: action.payload.dtoList };
    case getRepairApplicationRoutine.FAILURE:
      return { ...state, error: action.payload };
    case getRepairApplicationRoutine.FULFILL:
      return { ...state, loading: false };

    case deleteRepairApplicationRoutine.TRIGGER:
      return { ...state, loading: true };
    case deleteRepairApplicationRoutine.SUCCESS: {
      const id = action.payload;
      const list = state.data || [];
      const index = list.findIndex(item => item.id === id);
      if (index !== -1) {
        list.splice(index, 1);
      }
      return { ...state, data: list };
    }
    case deleteRepairApplicationRoutine.FAILURE:
      return { ...state, error: action.payload };
    case deleteRepairApplicationRoutine.FULFILL:
      return { ...state, loading: false };

    default:
      return state;
  }
}
