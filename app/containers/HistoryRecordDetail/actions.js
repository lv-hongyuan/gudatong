import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_HISTORY_RECORD_DETAIL } from './constants';

export const getHistoryRecordDetailRoutine = createRoutine(GET_HISTORY_RECORD_DETAIL,updates => updates,
    () => ({ globalLoading: true }));
export const getHistoryRecordDetailPromise = promisifyRoutine(getHistoryRecordDetailRoutine);