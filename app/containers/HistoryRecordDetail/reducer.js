import { getHistoryRecordDetailRoutine } from './actions';
import _ from "lodash";

const initState = {
  loading: false,
  data: {},
};

export default function (state = initState, action) {
  switch (action.type) {
    case getHistoryRecordDetailRoutine.TRIGGER:
      return { ...state, loading: true, };
    case getHistoryRecordDetailRoutine.SUCCESS:
      console.log('详情数据：',action.payload)
      return { ...state, data: action.payload.data, };
    case getHistoryRecordDetailRoutine.FAILURE:
      return {
        ...state, data: {}, error: action.payload,
      };
    case getHistoryRecordDetailRoutine.FULFILL:
      return { ...state, loading: false };

    default:
      return state;
  }
}
