import { createSelector } from 'reselect/es';

const historyRecordDetailDomain = () => state => state.historyRecordDetail;

const makeList = () => createSelector(historyRecordDetailDomain(), (subState) => {
  console.debug(subState);
  return subState.list;
})

export { makeList};