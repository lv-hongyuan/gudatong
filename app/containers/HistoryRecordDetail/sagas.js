import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../ErrorHandler/actions';
import { getHistoryRecordDetailRoutine } from './actions';

function* HistoryRecordDetail(action) {
    console.log(action);
    const { id } = (action.payload || {});

    try {
        yield put(getHistoryRecordDetailRoutine.request());
        const param = {id};
        console.log('param:', param)
        const response = yield call(request, ApiFactory.GetSailDynDetail(param));
        console.log('列表数据：', response);
        yield put(getHistoryRecordDetailRoutine.success({
            data: (response || {})
        }));
    } catch (e) {
        console.log(e.message);
        yield put(errorMessage(e.message));
        yield put(getHistoryRecordDetailRoutine.failure(e));
    } finally {
        yield put(getHistoryRecordDetailRoutine.fulfill());
    }
}

export function* getHistoryRecordDetailSaga() {
    yield takeLatest(getHistoryRecordDetailRoutine.TRIGGER, HistoryRecordDetail);
}

// All sagas to be loaded
export default [getHistoryRecordDetailSaga];
