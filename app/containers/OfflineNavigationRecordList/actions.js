import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  GET_OFFLINE_NAVIGATION_RECORD,
  DELETE_OFFLINE_NAVIGATION_RECORD,
  UPLOAD_OFFLINE_NAVIGATION_RECORD,
  UPLOAD_ALL_OFFLINE_NAVIGATION_RECORD,
} from './constants';

export const getOfflineNavigationRecordListRoutine = createRoutine(
  GET_OFFLINE_NAVIGATION_RECORD,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const getOfflineNavigationRecordListPromise = promisifyRoutine(getOfflineNavigationRecordListRoutine);

export const deleteOfflineNavigationRecordRoutine = createRoutine(
  DELETE_OFFLINE_NAVIGATION_RECORD,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const deleteOfflineNavigationRecordPromise = promisifyRoutine(deleteOfflineNavigationRecordRoutine);

export const uploadOfflineNavigationRecordRoutine = createRoutine(
  UPLOAD_OFFLINE_NAVIGATION_RECORD,
  updates => updates,
  () => ({ globalLoading: true }),
);

export const uploadAllOfflineNavigationRecordRoutine = createRoutine(
  UPLOAD_ALL_OFFLINE_NAVIGATION_RECORD,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const uploadAllOfflineNavigationRecordPromise = promisifyRoutine(uploadAllOfflineNavigationRecordRoutine);
