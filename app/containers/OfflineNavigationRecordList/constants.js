/*
 *
 * OfflineNavigationRecordList constants
 *
 */
export const GET_OFFLINE_NAVIGATION_RECORD = 'app/OfflineNavigationRecordList/GET_OFFLINE_NAVIGATION_RECORD';

export const DELETE_OFFLINE_NAVIGATION_RECORD = 'app/OfflineNavigationRecordList/DELETE_OFFLINE_NAVIGATION_RECORD';

export const UPLOAD_OFFLINE_NAVIGATION_RECORD = 'app/OfflineNavigationRecordList/UPLOAD_OFFLINE_NAVIGATION_RECORD';

export const UPLOAD_ALL_OFFLINE_NAVIGATION_RECORD = 'app/OfflineNavigationRecordList/UPLOAD_ALL_OFFLINE_NAVIGATION_RECORD';
