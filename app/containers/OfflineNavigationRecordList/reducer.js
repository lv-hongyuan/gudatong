import {
  getOfflineNavigationRecordListRoutine,
  deleteOfflineNavigationRecordRoutine,
} from './actions';

const initState = {
  data: [],
};

export default function (state = initState, action) {
  switch (action.type) {
    case getOfflineNavigationRecordListRoutine.TRIGGER:
      return { ...state, loading: true };
    case getOfflineNavigationRecordListRoutine.SUCCESS:
      return { ...state, data: action.payload };
    case getOfflineNavigationRecordListRoutine.FAILURE:
      return { ...state, error: action.payload };
    case getOfflineNavigationRecordListRoutine.FULFILL:
      return { ...state, loading: false };

    case deleteOfflineNavigationRecordRoutine.TRIGGER:
      return { ...state, loading: true };
    case deleteOfflineNavigationRecordRoutine.SUCCESS: {
      const id = action.payload;
      const list = state.data || [];
      const index = list.findIndex(item => item.id === id);
      if (index !== -1) {
        list.splice(index, 1);
      }
      return { ...state, data: list };
    }
    case deleteOfflineNavigationRecordRoutine.FAILURE:
      return { ...state, error: action.payload };
    case deleteOfflineNavigationRecordRoutine.FULFILL:
      return { ...state, loading: false };

    default:
      return state;
  }
}
