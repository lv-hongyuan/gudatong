import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import {
  getOfflineNavigationRecordListRoutine,
  uploadAllOfflineNavigationRecordRoutine,
} from './actions';
import { errorMessage } from '../ErrorHandler/actions';
import realm from '../../common/realm';
import { getPopId, getShipId } from '../ShipWork/sagas';

//获取未上传的离线数据列表
function* getRefuelApplication(action) {
  console.log(action);
  try {
    yield put(getOfflineNavigationRecordListRoutine.request());
    const offlineTasks = realm.objects('OfflineTask')
      .filtered('type = "NavigationRecord"')
      .sorted('date', true);
    console.log('getRefuelApplication', offlineTasks);
    yield put(getOfflineNavigationRecordListRoutine.success(offlineTasks));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(getOfflineNavigationRecordListRoutine.failure(e));
  } finally {
    yield put(getOfflineNavigationRecordListRoutine.fulfill());
  }
}

export function* getRefuelApplicationSaga() {
  yield takeLatest(getOfflineNavigationRecordListRoutine.TRIGGER, getRefuelApplication);
}

//上传所有离线数据
function* uploadAllOfflineNavigation(action) {
  try {
    yield put(uploadAllOfflineNavigationRecordRoutine.request());

    const NavigationTasks = realm.objects('OfflineTask')//从本地缓存获取要上传的离线船报 ：
      .filtered('type = "NavigationRecord"')
      .sorted('date', true);

    const ArrivePortTasks = realm.objects('OfflineTask') //从本地缓存获取要上传的离线确报：
      .filtered('type = "ArrivePortRecord"')
      .sorted('date', true);

    if (ArrivePortTasks.length === 0 && NavigationTasks.length === 0) {
      yield put(uploadAllOfflineNavigationRecordRoutine.success());  //如果没有离线数据则直接跳过上传
      return;
    }

    //获取线上最新航次信息
    const popId = yield call(getPopId);
    const shipId = yield call(getShipId);
    let param = { popId, shipId }
    const onLineNavigation = yield call(request, ApiFactory.getArrivePort(param));
    const { voyageId, isFirst, shipName } = onLineNavigation || {};

    let sailDyns = []; //要上传的航行船报
    let arrivals = []; //要上传的抵港确报

    for (var i = NavigationTasks.length - 1; i >= 0; i--) {
      let task = NavigationTasks[i];
      if (task) {
        const offlineShipName = JSON.parse(task.data).shipName
        if (offlineShipName != shipName) continue   //如果当前船报的船名与最新船名不一样则放弃此次上传
        const record = { popId, ...JSON.parse(task.data), voyageId };
        sailDyns.push(record)
      }
    }

    for (var j = ArrivePortTasks.length - 1; j >= 0; j--) {
      let task = ArrivePortTasks[j];
      if (task) {
        const offlineShipName = JSON.parse(task.data).shipName
        if (offlineShipName != shipName) continue   //如果当前船报的船名与最新船名不一样则放弃此次上传
        const record = { popId, shipId, isFirst, ...JSON.parse(task.data), voyageId };
        arrivals.push(record)
      }
    }

    const params = { sailDyns, arrivals }
    console.log('要上传的所有船报：', params);
    yield call(request, ApiFactory.createOffLineData(params));
    console.log('离线船报上传成功');

    //删除已上传完成的离线船报
    for (var i = NavigationTasks.length; i >= 0;i--) {
      const task = NavigationTasks[i];
      if(task){
        const offlineShipName = JSON.parse(task.data).shipName
        if (offlineShipName != shipName) continue   //如果当前船报的船名与最新船名不一样则放弃此次删除
        realm.write(() => {
          realm.delete(task);
        });
      }
      
    }

    //删除已上传完成的离线确报
    for (var j = ArrivePortTasks.length; j >= 0; j--) {
      const task = ArrivePortTasks[j];
      if(task){
        const offlineShipName = JSON.parse(task.data).shipName
        if (offlineShipName != shipName) continue   //如果当前确报的船名与最新船名不一样则放弃此次删除
        realm.write(() => {
          realm.delete(task);
        });
      }
      
    }
    console.log('剩余未上传离线船报数', NavigationTasks.length);
    console.log('剩余未上传离线确报数', ArrivePortTasks.length);
    yield put(uploadAllOfflineNavigationRecordRoutine.success());
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(uploadAllOfflineNavigationRecordRoutine.failure(e));
  } finally {
    yield put(uploadAllOfflineNavigationRecordRoutine.fulfill());
  }
}

export function* uploadAllOfflineNavigationSaga() {
  yield takeLatest(uploadAllOfflineNavigationRecordRoutine.TRIGGER, uploadAllOfflineNavigation);
}

// All sagas to be loaded
export default [
  getRefuelApplicationSaga,
  uploadAllOfflineNavigationSaga,
];
