import React from 'react';
import PropTypes from 'prop-types';
import {Platform, StyleSheet, TouchableOpacity,View ,DeviceEventEmitter} from 'react-native';
import JPushModule from 'jpush-react-native';
import { connect } from 'react-redux';
import { Container, Content, Grid, Label, Row } from 'native-base';
import Toast from 'teaset/components/Toast/Toast';
import HeaderButtons from 'react-navigation-header-buttons';
import Svg from '../../components/Svg';
import myTheme from '../../Themes';
import screenHOC from '../screenHOC';
import {
  ROUTE_BERTH_PLAN,
  ROUTE_REFUEL_APPLICATION,
  ROUTE_REPAIR_APPLICATION,
  ROUTE_VOYAGE_INSTRUCTION_LIST,
  ROUTE_SAVE_FEE_LIST,
  ROUTE_SAVE_FEE,
  ROUTE_LOADING_PLAN,
  ROUTE_IMPORTANT_BULLETIN_BOARD,
  ROUTE_BULLETIN_BOARD,
  ROUTE_HISTORY_RECORD,
  ROUTE_ARRIVEPORT_RECORD_LIST
} from '../../RouteConstant';
import { getUnReadMesPromise } from '../ShipWork/actions';
import { makeUnReadMes } from '../ShipWork/selectors';
import { createStructuredSelector } from 'reselect/es';
import { bindPromiseCreators } from 'redux-saga-routines/es';
const styles = StyleSheet.create({
  content: {},
  row: {
    height: 50,
    paddingLeft: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    borderBottomWidth: myTheme.borderWidth,
    borderBottomColor: myTheme.borderColor,
  },
  view: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  label: {
    flex: 1,
    paddingLeft: 20,
  },
  cornermark:{
    height:12,
    width:12,
    borderRadius: 6,
    backgroundColor:'#c00',
  }
});

@screenHOC
class Message extends React.PureComponent {
  static navigationOptions = {
    title: '信息工具',
    headerLeft: <HeaderButtons />,
  };

  static propTypes = {
    navigation: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      newsCornerMark: false,   //公告栏角标状态
      orderCornerMark: false,     //航次指令角标状态
      UnReadMesage: 0,   //未读的公告消息
      UnReadBoard: 0,     //未读的航次指令消息
    };
  }

  componentDidMount() {
    this.getNewsList()
    this.listener = DeviceEventEmitter.addListener('refreshCornerMarkState', () => {
      this.getNewsList()
    });
    this.listenerDesktop = DeviceEventEmitter.addListener('refreshDesktopMarkState',(data) =>{
      if(data.data == "voyageNotice"){
        this.setState({UnReadBoard:this.state.UnReadBoard+1})
      }else if(data.data == "notice"){
        this.setState({UnReadMesage:this.state.UnReadMesage+1})
      }
      this.showNotice()
    })
  }

  componentWillUnmount() {
    this.listener.remove();
    this.listenerDesktop.remove();
  }

  //设置ios桌面角标数量，设置信息工具界面公告栏/航次指令角标
  showNotice(){
    console.log(this.state.UnReadBoard,this.state.UnReadMesage);
    if(this.state.UnReadMesage > 0){
      this.setState({ newsCornerMark: true })
    }else{
      this.setState({ newsCornerMark: false })
    }
    if(this.state.UnReadBoard > 0){
      this.setState({ orderCornerMark: true })
    }else{
      this.setState({ orderCornerMark: false })
    }
    if (Platform.OS === 'ios') {
      let bages = this.state.UnReadMesage + this.state.UnReadBoard
      JPushModule.setBadge(bages, (success) => {
        console.log('设置通知', success);
      })
    }
  }

  //获取未读公告与航次指令数量 **
  getNewsList() {
    this.props.getUnReadMes()
      .then(() => {
        let data = this.props.unReadMes
        if (data.length > 0) {
          for (var i = 0; i < data.length; i++) {
            if (data[i].type == 10) {
              this.setState({ UnReadMesage:data[i].noticeNum})
            } else {
              this.setState({ UnReadBoard:data[i].noticeNum})
            }
          }
        }
        this.showNotice()
      })
      .catch(() => {
      });
  }

  onBackPressed = () => false;

  render() {
    return (
      <Container>
        <Content style={styles.content}>
          <Grid>
            <Row style={[styles.row, { marginTop: 10 }]}>
                <TouchableOpacity
                    style={styles.view}
                    onPress={() => {
                      this.props.navigation.push(ROUTE_BULLETIN_BOARD);
                    }}
                >
                  <Svg icon="file" size={40} />
                  <Label style={styles.label}>公告栏</Label>
                  {this.state.newsCornerMark && <View style={styles.cornermark}></View>}
                  <Svg icon="right_arrow" size={40} />
                </TouchableOpacity>
            </Row>
            <Row style={styles.row}>
              <TouchableOpacity
                style={styles.view}
                onPress={() => {
                  this.props.navigation.push(ROUTE_IMPORTANT_BULLETIN_BOARD);
                }}
              >
                <Svg icon="vol" size={40} />
                <Label style={styles.label}>航次指令</Label>
                {this.state.orderCornerMark && <View style={styles.cornermark}></View>}
                <Svg icon="right_arrow" size={40} />
              </TouchableOpacity>
            </Row>
            {/* <Row style={styles.row}>
              <TouchableOpacity
                style={styles.view}
                onPress={() => {
                  this.props.navigation.push(ROUTE_LOADING_PLAN);
                }}
              >
                <Svg icon="plan" size={40} />
                <Label style={styles.label}>预计装货载量</Label>
                <Svg icon="right_arrow" size={40} />
              </TouchableOpacity>
            </Row> */}
            {/* <Row style={styles.row}> */}
            {/* <TouchableOpacity */}
            {/* style={styles.view} */}
            {/* onPress={() => { */}
            {/* Toast.message('敬请期待') */}
            {/* }}> */}
            {/* <Svg icon='orientation' size={40}/> */}
            {/* <Label style={styles.label}>拖轮安排</Label> */}
            {/* <Svg icon='right_arrow' size={40}/> */}
            {/* </TouchableOpacity> */}
            {/* </Row> */}
            {/* <Row style={styles.row}>
              <TouchableOpacity
                style={styles.view}
                onPress={() => {
                  this.props.navigation.push(ROUTE_BERTH_PLAN);
                }}
              >
                <Svg icon="calender" size={40} />
                <Label style={styles.label}>预靠计划</Label>
                <Svg icon="right_arrow" size={40} />
              </TouchableOpacity>
            </Row> */}
            {/* <Row style={[styles.row, { marginTop: 10 }]}>
              <TouchableOpacity
                style={styles.view}
                onPress={() => {
                  this.props.navigation.push(ROUTE_REFUEL_APPLICATION);
                }}
              >
                <Svg icon="oil" size={40} />
                <Label style={styles.label}>加油申请</Label>
                <Svg icon="right_arrow" size={40} />
              </TouchableOpacity>
            </Row> */}
            {/* <Row style={styles.row}>
              <TouchableOpacity
                style={styles.view}
                onPress={() => {
                  this.props.navigation.push(ROUTE_REPAIR_APPLICATION);
                }}
              >
                <Svg icon="servicing" size={40} />
                <Label style={styles.label}>修船申请</Label>
                <Svg icon="right_arrow" size={40} />
              </TouchableOpacity>
            </Row> */}
            <Row style={styles.row}>
              <TouchableOpacity
                  style={styles.view}
                  onPress={() => {
                    this.props.navigation.push(ROUTE_SAVE_FEE_LIST);
                  }}
              >
                <Svg icon="plan" size={40} />
                <Label style={styles.label}>节拖奖励</Label>
                <Svg icon="right_arrow" size={40} />
              </TouchableOpacity>
            </Row>
            <Row style={styles.row}>
              <TouchableOpacity
                  style={styles.view}
                  onPress={() => {
                    this.props.navigation.push(ROUTE_HISTORY_RECORD);
                    // this.props.navigation.navigate(ROUTE_HISTORY_RECORD);
                  }}
              >
                <Svg icon="calender" size={40} />
                <Label style={styles.label}>历史船报</Label>
                <Svg icon="right_arrow" size={40} />
              </TouchableOpacity>
            </Row>
            <Row style={styles.row}>
              <TouchableOpacity
                  style={styles.view}
                  onPress={() => {
                    this.props.navigation.push(ROUTE_ARRIVEPORT_RECORD_LIST);
                  }}
              >
                <Svg icon="plan" size={40} color={'#27f'}/>
                <Label style={styles.label}>历史确报</Label>
                <Svg icon="right_arrow" size={40} />
              </TouchableOpacity>
            </Row>
            {/* <Row style={[styles.row, {marginTop: 10}]}> */}
            {/* <TouchableOpacity */}
            {/* style={styles.view} */}
            {/* onPress={() => { */}
            {/* Toast.message('敬请期待') */}
            {/* }}> */}
            {/* <Svg icon='calender' size={40}/> */}
            {/* <Label style={styles.label}>航次总结</Label> */}
            {/* <Svg icon='right_arrow' size={40}/> */}
            {/* </TouchableOpacity> */}
            {/* </Row> */}
          </Grid>
        </Content>
      </Container>
    );
  }
}

Message.propTypes = {
  unReadMes: PropTypes.array,
};
Message.defaultProps = {
  unReadMes: [],
};

const mapStateToProps = createStructuredSelector({
  unReadMes: makeUnReadMes(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getUnReadMes: getUnReadMesPromise,
    }, dispatch),
    dispatch,
  };
}
export default connect(mapStateToProps,mapDispatchToProps)(Message);
