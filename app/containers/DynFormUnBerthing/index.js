import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Keyboard, SafeAreaView, AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import { Container, Content, Form, InputGroup, Item, Label, Text, View, Button } from 'native-base';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import _ from 'lodash';
import { createStructuredSelector } from 'reselect/es';
import DatePullSelector from '../../components/DatePullSelector';
import InputItem from '../../components/InputItem';
import screenHOC from '../screenHOC';
import commonStyles from '../../common/commonStyles';
// import Selector from '../../components/Selector';
// import { weatherList, windDirectionList, windPowerList, windScaleList } from '../../common/Constant';
import { updateDynPromise, saveDynPromise, getLeaveReportPromise } from '../ShipWork/actions';
import { makeSelectState, makeSelectPopId, makeLeaveReport, makeLastStateTime } from '../ShipWork/selectors';
import AlertView from '../../components/Alert';
import { defaultFormat } from '../../utils/DateFormat';
import { MaskType, TextInput } from '../../components/InputItem/TextInput';
import myTheme from '../../Themes';
import AppStorage from "../../utils/AppStorage";
import StorageKeys from "../../common/StorageKeys";

const styles = StyleSheet.create({
  leaveTons: {
    borderWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
    padding: 5,
    margin: 5,
    // marginLeft: -5,
    // marginRight: -5,
    borderRadius: 5,
  },
  buttonContainer: {
    flexDirection: 'row',
    backgroundColor: '#E0E0E0',
    height: 50,
  },
  saveButton: {
    height: '100%',
    backgroundColor: '#FBB03B',
    borderRadius: 0,
    flex: 1,
    justifyContent: 'center',
  },
  completeButton: {
    height: '100%',
    backgroundColor: '#DC001B',
    borderRadius: 0,
    flex: 2,
    justifyContent: 'center',
  },
  bottomTip: {
    textAlign: 'center',
    padding: 8,
    fontSize: 12,
    color: '#969696',
    marginBottom: 60,
  },
});

/**
 *  离泊页面
 * Created by jianzhexu on 2018/3/26
 */
@screenHOC
class DynFormUnBerthing extends React.PureComponent {
  static navigationOptions = () => ({
    title: '离泊登记（开航报）',
  });
  static propTypes = {
    navigation: PropTypes.object.isRequired,
    saveDyn: PropTypes.func.isRequired,
    updateDyn: PropTypes.func.isRequired,
    getLeaveReport: PropTypes.func.isRequired,
    leaveReport: PropTypes.object,
    popId: PropTypes.number.isRequired,
    lastStateTime: PropTypes.number,
  };
  static defaultProps = {
    leaveReport: {},
    lastStateTime: undefined,
  };

  constructor(props) {
    super(props);
    const params = this.props.navigation.state.params
    this.state = {
      shipName: AppStorage.shipName,  //船名
      voyageCode: undefined,          //航次号
      line: undefined,                //航段
      atd: undefined,                 //实际离泊时间
      leaveInfo: undefined,           //离泊信息
      f20: undefined,
      e20: undefined,
      f40: undefined,
      e40: undefined,
      goodsTon: undefined,            //货重
      // TEU: undefined,                 //TEU
      draughtFrontB: undefined,       //艏吃水
      draughtAfterB: undefined,       //艉吃水
      gm: undefined,                  //GM
      bw: undefined,                  //压载水
      oil180B: '0',             //180#重油
      oil120B: '0',             //120#重油
      oil0B: '0',               //0#轻油
      mainEngineOil: '0',       //主机滑油
      auxiliaryEngineOil: '0',  //辅机滑油
      cylinderOil: '0',         //汽缸油
      waterB: '0',              //淡水
      oil180Add: '0',           //重油补给
      oil0Add: '0',             //轻油补给
      waterAdd: undefined,            //淡水补给
      nextArrivalPosition: undefined, //下港到港地
      nextEta: undefined,             //下港ETA
      notes: '检查集装箱绑扎系固正常',               //检查集装箱绑扎系固正常
      tugNum: '0',              //离泊拖轮数
      memo: undefined,                //备注
      isLastPort:params.mark ? params.mark : ''
    };
  }

  componentDidMount() {
    this.props.getLeaveReport()
      .then(() => { })
      .catch(() => { });
    console.log('124565432',this.state.isLastPort);
    
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.leaveReport !== this.props.leaveReport) {
      this.setState({
        ...nextProps.leaveReport,
        notes: nextProps.leaveReport.notes || "检查集装箱绑扎系固正常",
      });
    }
  }

  handleSave = () => {
    Keyboard.dismiss();

    const {
      shipName, voyageCode, line, atd, leaveInfo, f20, e20, f40, e40, goodsTon, draughtFrontB, draughtAfterB,
      gm, bw, oil180B, oil120B, oil0B, mainEngineOil, auxiliaryEngineOil, cylinderOil, waterB,
      oil180Add, oil0Add, waterAdd, nextArrivalPosition, nextEta, notes, tugNum, memo,
    } = this.state;

    this.props.saveDyn({
      popId: this.props.popId,
      state: 90,
      time: atd,
      leave: {
        shipName,
        voyageCode,
        line,
        atd,
        leaveInfo,
        f20,
        e20,
        f40,
        e40,
        goodsTon,
        draughtFrontB,
        draughtAfterB,
        gm,
        bw,
        oil180B,
        oil120B,
        oil0B,
        mainEngineOil,
        auxiliaryEngineOil,
        cylinderOil,
        waterB,
        oil180Add,
        oil0Add,
        waterAdd,
        nextArrivalPosition,
        nextEta,
        notes,
        tugNum,
        memo,
      },
    }).then(() => {
      this.props.navigation.goBack();
    }).catch(() => {

    });
  };

  handleSubmit = () => {
    Keyboard.dismiss();

    const {
      updateDyn, popId, navigation, leaveReport,
    } = this.props;
    const {
      shipName, voyageCode, line, atd, leaveInfo, f20, e20, f40, e40, goodsTon, draughtFrontB, draughtAfterB,
      gm, bw, oil180B, oil120B, oil0B, mainEngineOil, auxiliaryEngineOil, cylinderOil, waterB,
      oil180Add, oil0Add, waterAdd, nextArrivalPosition, nextEta, notes, tugNum, memo,
    } = this.state;

    if (!atd) {
      AlertView.show({ message: '请选择离泊完成时间' });
      return;
    }
    if (_.isEmpty(leaveInfo)) {
      AlertView.show({ message: '请输入离泊信息' });
      return;
    }
    // if (parseFloat(f20) === 0
    //   && parseFloat(e20) === 0
    //   && parseFloat(f40) === 0
    //   && parseFloat(e40) === 0) {
    //   AlertView.show({ message: '离港在船货量信息未填写' });
    //   return;
    // }
    if (_.isNil(f20)) {
      AlertView.show({ message: '请填写20F' });
      return;
    }
    if (_.isNil(e20)) {
      AlertView.show({ message: '请填写20E' });
      return;
    }
    if (_.isNil(f40)) {
      AlertView.show({ message: '请填写40F' });
      return;
    }
    if (_.isNil(e40)) {
      AlertView.show({ message: '请填写40E' });
      return;
    }
    // if (_.isNil(goodsTon) || parseFloat(goodsTon) === 0) {
    //   AlertView.show({ message: '请填写货物吨数' });
    //   return;
    // }
    // if (_.isNil(draughtFrontB) || parseFloat(draughtFrontB) === 0) {
    //   AlertView.show({ message: '请输入艏吃水' });
    //   return;
    // }
    // if (_.isNil(draughtAfterB) || parseFloat(draughtAfterB) === 0) {
    //   AlertView.show({ message: '请输入艉吃水' });
    //   return;
    // }
    // if (_.isNil(gm) || parseFloat(gm) === 0) {
    //   AlertView.show({ message: '请输入GM稳性' });
    //   return;
    // }
    // if (_.isNil(bw) || parseFloat(bw) === 0) {
    //   AlertView.show({ message: '请输入BW压载水' });
    //   return;
    // }
    // if (_.isNil(oil180B) || parseFloat(oil180B) === 0) {
    //   AlertView.show({ message: '请输入现存180#' });
    //   return;
    // }
    // if (_.isNil(oil120B) || parseFloat(oil120B) === 0) {
    //   AlertView.show({ message: '请输入现存120#' });
    //   return;
    // }
    // if (_.isNil(oil0B) || parseFloat(oil0B) === 0) {
    //   AlertView.show({ message: '请输入写现存0#' });
    //   return;
    // }
    // if (_.isNil(mainEngineOil) || parseFloat(mainEngineOil) === 0) {
    //   AlertView.show({ message: '请输入现存主机滑油' });
    //   return;
    // }
    // if (_.isNil(auxiliaryEngineOil) || parseFloat(auxiliaryEngineOil) === 0) {
    //   AlertView.show({ message: '请输入现存辅机滑油' });
    //   return;
    // }
    // if (_.isNil(cylinderOil) || parseFloat(cylinderOil) === 0) {
    //   AlertView.show({ message: '请输入现存汽缸油' });
    //   return;
    // }
    // if (_.isNil(waterB) || parseFloat(waterB) === 0) {
    //   AlertView.show({ message: '请输入现存淡水' });
    //   return;
    // }
    if (_.isEmpty(nextArrivalPosition) && this.state.isLastPort != 'isLastPort') {
      AlertView.show({ message: '请输入下港到港地' });
      return;
    }
    if (!nextEta && this.state.isLastPort != 'isLastPort') {
      AlertView.show({ message: '请选择下港ETA' });
      return;
    }
    if (this.state.nextEta && this.state.nextEta < this.state.atd && this.state.isLastPort != 'isLastPort') {
      AlertView.show({ message: '下港ETA时间不能小于离泊完成时间' });
      return;
    }
    // if (_.isNil(tugNum) || parseFloat(tugNum) === 0) {
    //   AlertView.show({ message: '请输入离泊拖轮数' });
    //   return;
    // }

    function doSubmit() {
      updateDyn({
        popId: popId,
        state: 90,
        time: atd,
        leave: {
          shipName,
          voyageCode,
          line,
          atd,
          leaveInfo,
          f20,
          e20,
          f40,
          e40,
          goodsTon,
          draughtFrontB,
          draughtAfterB,
          gm,
          bw,
          oil180B,
          oil120B,
          oil0B,
          mainEngineOil,
          auxiliaryEngineOil,
          cylinderOil,
          waterB,
          oil180Add,
          oil0Add,
          waterAdd,
          nextArrivalPosition,
          nextEta,
          notes,
          tugNum,
          memo,
        },
      }).then(() => {
        const goBackCallback = this.props.navigation.getParam('onGoBack', undefined);
        if (goBackCallback) {
          goBackCallback();
        }
        navigation.goBack();
      }).catch(() => {
      });
    }

    function confirmSubmit() {
      if(this.state.isLastPort == 'isLastPort'){
        doSubmit.bind(this)();
        return
      }
      if (leaveReport.eta && Math.abs(leaveReport.eta - nextEta) > 10800000) {
        AlertView.show({
          message: `下港ETA时间${defaultFormat(nextEta)}\n与预计时间${defaultFormat(leaveReport.eta)}的\n时间差超过3小时，确定提交吗？`,
          showCancel: true,
          confirmAction: () => {
            doSubmit.bind(this)();
          },
        });
      } else {
        AlertView.show({
          message: `确定下港ETA时间为${defaultFormat(nextEta)}吗？`,
          showCancel: true,
          confirmAction: () => {
            doSubmit.bind(this)();
          },
        });
      }
    }

    if (parseFloat(f20) === 0
      && parseFloat(e20) === 0
      && parseFloat(f40) === 0
      && parseFloat(e40) === 0
      && parseFloat(goodsTon) === 0) {
      AlertView.show({
        message: '离港在船货量信息未填写\n(货量信息都为0)\n确认要提交吗？',
        showCancel: true,
        confirmAction: () => {
          confirmSubmit.bind(this)();
        },
      });
    } else {
      confirmSubmit.bind(this)();
    }
  };

  makeTimeValueToShow() {
    let timeValueToShow = null;
    if (this.props.lastStateTime) {
      const suitableTime = this.props.lastStateTime + 1800000; // 增加半小时
      const currentTime = new Date().getTime(); // 当前时间
      if (suitableTime < currentTime) {
        timeValueToShow = suitableTime;
      }
    }
    return timeValueToShow;
  }

  render() {
    return (
      <Container>
        <Content contentInsetAdjustmentBehavior="scrollableAxes">
          <Form style={{ backgroundColor: '#FFFFFF' }}>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="船名:"
                value={this.state.shipName}
                placeholder=""
                editable={false}
              />
              <InputItem
                label="航次:"
                value={this.state.voyageCode}
                placeholder=""
                editable={false}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="航线:"
                value={this.state.line}
                placeholder=""
                editable={false}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={{ color: 'red' }}>*</Label>
                <Label style={commonStyles.inputLabel}>离泊完成时间:</Label>
                <DatePullSelector
                  defaultValue={this.makeTimeValueToShow()}
                  value={this.state.atd || null}
                  onChangeValue={(value) => {
                    this.setState({ atd: value });
                  }}
                />
              </Item>
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                isRequired
                label="离泊信息:"
                value={this.state.leaveInfo}
                onChangeText={(text) => {
                  this.setState({ leaveInfo: text.trim() });
                }}
              />
            </InputGroup>
            <View style={styles.leaveTons}>
              <Text style={commonStyles.inputLabel}>船舶离港在船总货量</Text>
              <InputGroup style={commonStyles.inputGroup}>
                <InputItem
                  isRequired
                  label="20F:"
                  placeholder='0'
                  keyboardType="numeric"
                  maskType={MaskType.INTEGER}
                  value={this.state.f20}
                  // placeholder='0'
                  onChangeText={(text) => {
                    this.setState({ f20: text });
                  }}
                />
                <InputItem
                  isRequired
                  label="20E:"
                  placeholder='0'
                  keyboardType="numeric"
                  maskType={MaskType.INTEGER}
                  value={this.state.e20}
                  // placeholder='0'
                  onChangeText={(text) => {
                    this.setState({ e20: text });
                  }}
                />
              </InputGroup>
              <InputGroup style={commonStyles.inputGroup}>
                <InputItem
                  isRequired
                  label="40F:"
                  placeholder='0'
                  keyboardType="numeric"
                  maskType={MaskType.INTEGER}
                  value={this.state.f40}
                  // placeholder='0'
                  onChangeText={(text) => {
                    this.setState({ f40: text });
                  }}
                />
                <InputItem
                  isRequired
                  label="40E:"
                  placeholder='0'
                  keyboardType="numeric"
                  maskType={MaskType.INTEGER}
                  value={this.state.e40}
                  // placeholder='0'
                  onChangeText={(text) => {
                    this.setState({ e40: text });
                  }}
                />
              </InputGroup>
              <InputGroup style={commonStyles.inputGroup}>
                <InputItem
                  isRequired
                  label="货重:"
                  placeholder='0'
                  keyboardType="numeric"
                  maskType={MaskType.FLOAT}
                  value={this.state.goodsTon}
                  onChangeText={(text) => {
                    this.setState({ goodsTon: text });
                  }}
                  rightItem={<Text style={commonStyles.tail}>吨</Text>}
                />
                <View style={commonStyles.fixItem} />
              </InputGroup>
              <InputGroup style={commonStyles.inputGroup}>
                <InputItem
                  label="TEU:"
                  value={(0 | parseFloat(this.state.f20)) + (0 | parseFloat(this.state.e20)) + ((0 | parseFloat(this.state.f40)) + (0 | parseFloat(this.state.e40))) * 2}
                  editable={false}
                />
              </InputGroup>
            </View>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                isRequired
                label="艏&nbsp;&nbsp;吃&nbsp;&nbsp;水:"
                value={this.state.draughtFrontB}
                onChangeText={(text) => {
                  this.setState({ draughtFrontB: text });
                }}
                placeholder='0'
                keyboardType="numeric"
                maskType={MaskType.FLOAT}
                rightItem={<Text style={commonStyles.tail}>米</Text>}
              />
              <InputItem
                isRequired
                label="艉&nbsp;&nbsp;吃&nbsp;&nbsp;水:"
                value={this.state.draughtAfterB}
                onChangeText={(text) => {
                  this.setState({ draughtAfterB: text });
                }}
                placeholder='0'
                keyboardType="numeric"
                maskType={MaskType.FLOAT}
                rightItem={<Text style={commonStyles.tail}>米</Text>}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                isRequired
                label="G&nbsp;M&nbsp;稳性:"
                value={this.state.gm}
                onChangeText={(text) => {
                  this.setState({ gm: text });
                }}
                placeholder='0'
                keyboardType="numeric"
                maskType={MaskType.FLOAT}
              />
              <InputItem
                isRequired
                label="BW压载水:"
                value={this.state.bw}
                onChangeText={(text) => {
                  this.setState({ bw: text });
                }}
                placeholder='0'
                keyboardType="numeric"
                maskType={MaskType.FLOAT}
                rightItem={<Text style={commonStyles.tail}>吨</Text>}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                isRequired
                label="180#重油:"
                value={this.state.oil180B}
                onChangeText={(text) => {
                  this.setState({ oil180B: text });
                }}
                placeholder='0'
                keyboardType="numeric"
                maskType={MaskType.FLOAT}
                rightItem={<Text style={commonStyles.tail}>吨</Text>}
              />
              <InputItem
                isRequired
                label="120#重油:"
                value={this.state.oil120B}
                onChangeText={(text) => {
                  this.setState({ oil120B: text });
                }}
                placeholder='0'
                keyboardType="numeric"
                maskType={MaskType.FLOAT}
                rightItem={<Text style={commonStyles.tail}>吨</Text>}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                isRequired
                label="0#轻油:"
                value={this.state.oil0B}
                onChangeText={(text) => {
                  this.setState({ oil0B: text });
                }}
                placeholder='0'
                keyboardType="numeric"
                maskType={MaskType.FLOAT}
                rightItem={<Text style={commonStyles.tail}>吨</Text>}
              />
              <InputItem
                isRequired
                label="主机滑油:"
                value={this.state.mainEngineOil}
                onChangeText={(text) => {
                  this.setState({ mainEngineOil: text });
                }}
                placeholder='0'
                keyboardType="numeric"
                maskType={MaskType.FLOAT}
                rightItem={<Text style={commonStyles.tail}>吨</Text>}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                isRequired
                label="辅机滑油:"
                value={this.state.auxiliaryEngineOil}
                onChangeText={(text) => {
                  this.setState({ auxiliaryEngineOil: text });
                }}
                placeholder='0'
                keyboardType="numeric"
                maskType={MaskType.FLOAT}
                rightItem={<Text style={commonStyles.tail}>吨</Text>}
              />
              <InputItem
                isRequired
                label="汽缸油:"
                value={this.state.cylinderOil}
                onChangeText={(text) => {
                  this.setState({ cylinderOil: text });
                }}
                placeholder='0'
                keyboardType="numeric"
                maskType={MaskType.FLOAT}
                rightItem={<Text style={commonStyles.tail}>吨</Text>}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                isRequired
                label="淡水:"
                value={this.state.waterB}
                onChangeText={(text) => {
                  this.setState({ waterB: text });
                }}
                placeholder='0'
                keyboardType="numeric"
                maskType={MaskType.FLOAT}
                rightItem={<Text style={commonStyles.tail}>吨</Text>}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="重油补给:"
                value={this.state.oil180Add}
                onChangeText={(text) => {
                  this.setState({ oil180Add: text });
                }}
                placeholder='0'
                keyboardType="numeric"
                maskType={MaskType.FLOAT}
                rightItem={<Text style={commonStyles.tail}>吨</Text>}
              />
              <InputItem
                label="轻油补给:"
                value={this.state.oil0Add}
                onChangeText={(text) => {
                  this.setState({ oil0Add: text });
                }}
                placeholder='0'
                keyboardType="numeric"
                maskType={MaskType.FLOAT}
                rightItem={<Text style={commonStyles.tail}>吨</Text>}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="淡水补给:"
                value={this.state.waterAdd}
                onChangeText={(text) => {
                  this.setState({ waterAdd: text });
                }}
                placeholder='0'
                keyboardType="numeric"
                maskType={MaskType.FLOAT}
                rightItem={<Text style={commonStyles.tail}>吨</Text>}
              />
              <View style={commonStyles.fixItem} />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                isRequired = {this.state.isLastPort == 'isLastPort' ? false : true}
                placeholder="下港到港地"
                value={this.state.nextArrivalPosition}
                onChangeText={(text) => {
                  this.setState({ nextArrivalPosition: text });
                }}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <Item style={[commonStyles.inputItem, { borderBottomWidth: myTheme.borderWidth }]}>
                {this.state.isLastPort == 'isLastPort' ? null : <Label style={{ color: 'red' }}>*</Label>}
                <Label style={[commonStyles.inputLabel, { color: 'red' }]}>下港ETA:</Label>
                <DatePullSelector
                  value={this.state.nextEta || null}
                  minValue={this.state.atd}
                  onChangeValue={(value) => {
                    this.setState({ nextEta: value });
                  }}
                />
              </Item>
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                // isRequired
                label="离泊拖轮数:"
                placeholder="0"
                value={this.state.tugNum}
                onChangeText={(text) => {
                  this.setState({ tugNum: text });
                }}
                keyboardType="numeric"
                maskType={MaskType.INTEGER}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                placeholder=""
                value={this.state.notes}
                onChangeText={(text) => {
                  this.setState({ notes: text });
                }}
              />

            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="备注:"
                placeholder="无"
                value={this.state.memo}
                onChangeText={(text) => {
                  this.setState({ memo: text });
                }}
              />
            </InputGroup>
          </Form>
          <Text style={styles.bottomTip}>———— 已经到底了 ————</Text>
        </Content>
        <SafeAreaView style={{
          position: 'absolute', bottom: 0, left: 0, right: 0, backgroundColor: '#ffffff',
        }}
        >
          <View style={styles.buttonContainer}>
            <Button onPress={this.handleSave} style={styles.saveButton}>
              <Text>暂存</Text>
            </Button>
            <Button onPress={this.handleSubmit} style={styles.completeButton}>
              <Text>提交</Text>
            </Button>
          </View>
        </SafeAreaView>
      </Container>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  popId: makeSelectPopId(),
  state: makeSelectState(),
  leaveReport: makeLeaveReport(),
  lastStateTime: makeLastStateTime(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getLeaveReport: getLeaveReportPromise,
      saveDyn: saveDynPromise,
      updateDyn: updateDynPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(DynFormUnBerthing);
