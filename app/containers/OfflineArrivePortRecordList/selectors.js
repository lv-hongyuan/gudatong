import { createSelector } from 'reselect/es';

const selectRefuelApplicationDomain = () => state => state.offlineArrivePortRecordList;

const makeRefuelApplicationList = () => createSelector(
  selectRefuelApplicationDomain(),
  (subState) => {
    console.debug(subState.success);
    return subState.data;
  },
);

export {
  makeRefuelApplicationList,
};
