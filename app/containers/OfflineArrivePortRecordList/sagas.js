import { call, put, takeLatest } from 'redux-saga/effects';
import {
  getOfflineArrivePortRecordListRoutine,
} from './actions';
import { errorMessage } from '../ErrorHandler/actions';
import realm from '../../common/realm';

function* getRefuelApplication(action) {
  console.log(action);
  try {
    yield put(getOfflineArrivePortRecordListRoutine.request());
    const offlineTasks = realm.objects('OfflineTask')
      .filtered('type = "ArrivePortRecord"')
      .sorted('date', true);
    console.log('getRefuelApplication', offlineTasks);
    yield put(getOfflineArrivePortRecordListRoutine.success(offlineTasks));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(getOfflineArrivePortRecordListRoutine.failure(e));
  } finally {
    yield put(getOfflineArrivePortRecordListRoutine.fulfill());
  }
}

export function* getRefuelApplicationSaga() {
  yield takeLatest(getOfflineArrivePortRecordListRoutine.TRIGGER, getRefuelApplication);
}

// All sagas to be loaded
export default [
  getRefuelApplicationSaga
];
