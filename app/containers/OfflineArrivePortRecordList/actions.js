import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  GET_OFFLINE_ARRIVEPORT_RECORD,
} from './constants';

export const getOfflineArrivePortRecordListRoutine = createRoutine(
  GET_OFFLINE_ARRIVEPORT_RECORD,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const getOfflineArrivePortRecordListPromise = promisifyRoutine(getOfflineArrivePortRecordListRoutine);

