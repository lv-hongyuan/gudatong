import {
  getOfflineArrivePortRecordListRoutine,
} from './actions';

const initState = {
  data: [],
};

export default function (state = initState, action) {
  switch (action.type) {
    case getOfflineArrivePortRecordListRoutine.TRIGGER:
      return { ...state, loading: true };
    case getOfflineArrivePortRecordListRoutine.SUCCESS:
      return { ...state, data: action.payload };
    case getOfflineArrivePortRecordListRoutine.FAILURE:
      return { ...state, error: action.payload };
    case getOfflineArrivePortRecordListRoutine.FULFILL:
      return { ...state, loading: false };
    default:
      return state;
  }
}
