import React from 'react';
import PropTypes from 'prop-types';
import { FlatList, RefreshControl, AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import StorageKeys from '../../common/StorageKeys';
import { Container, View } from 'native-base';
import HeaderButtons from 'react-navigation-header-buttons';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { createStructuredSelector } from 'reselect/es';
import ShortId from 'shortid';
import { ROUTE_ARRIVEPORT_RECORD ,ROUTE_OFFLINE_ARRIVEPORT_RECORD_DETAIL} from '../../RouteConstant';
import { makeSelectIsSail } from '../ShipWork/selectors';
import {
  getOfflineArrivePortRecordListPromise,
} from './actions';
import myTheme from '../../Themes';
import { makeRefuelApplicationList } from './selectors';
import EmptyView from '../../components/EmptyView';
import screenHOC from '../screenHOC';
import NavigationRecordItem from '../../components/NavigationRecordItem';

/**
 * 离线确报列表页面
 * Created by hongyuanwang on 2019/12/13
 */
@screenHOC
class OfflineArrivePOrtRecordList extends React.PureComponent {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.getParam('title', '离线确报'),
    headerRight: (
      <HeaderButtons>
        <HeaderButtons.Item
          title="创建"
          buttonStyle={{ fontSize: 14, color: '#ffffff' }}
          onPress={navigation.getParam('onBtnCreate')}
        />
      </HeaderButtons>
    ),
  });
  static propTypes = {
    navigation: PropTypes.object.isRequired,
    // data: PropTypes.array.isRequired,
    getOfflineArrivePortRecordList: PropTypes.func.isRequired,
    isSail: PropTypes.bool.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
    };
    this.shipName = ''
    this.props.navigation.setParams({ onBtnCreate: this.onBtnCreate.bind(this) });
  }

  componentDidMount() {
    this.reLoadingPage();
    AsyncStorage.getItem(StorageKeys.DYN)
      .then((str)=>{
        if(str){
          const dyn = JSON.parse(str);
          this.shipName = dyn.shipName
        }
      })
  }

  onBtnCreate = () => {
    this.props.navigation.navigate(ROUTE_ARRIVEPORT_RECORD, {
      title: '创建离线确报',
      onGoBack: () => {
        this.reLoadingPage();
      },
      isEdit: false,
      isVoyage: this.props.isSail,
    });
  };

  reLoadingPage = () => {
    requestAnimationFrame(() => {
      this.props.getOfflineArrivePortRecordList()
        .then(() => {
          this.setState({ refreshing: false });
        })
        .catch(() => {
          this.setState({ refreshing: false });
        });
    });
  };

  //点击查看详情
  itemPress = ({data}) => {
    const DATA = JSON.parse(data)
    this.props.navigation.navigate(
      ROUTE_OFFLINE_ARRIVEPORT_RECORD_DETAIL,{DATA},
    );
  };

  renderItem = ({ item }) => {
    let shipName =JSON.parse(item.data).shipName
    if(shipName == this.shipName){
      return <NavigationRecordItem
      data={item.data}
      date={item.date}
      onDelete={this.deleteItem}
      onItemPress={this.itemPress}
    />
    } else {return null}
  };

  renderEmptyView = () => (<EmptyView />);

  render() {
    return (
      <Container theme={myTheme}>
        <FlatList
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => {
                this.setState({ refreshing: true });
                this.reLoadingPage();
              }}
            />
          }
          // contentInsetAdjustmentBehavior="scrollableAxes"
          style={{ flex: 1, backgroundColor: '#e0e0e0' }}
          keyExtractor={() => ShortId.generate()}
          data={this.props.data}
          ListEmptyComponent={this.renderEmptyView}
          initialNumToRender={10}
          renderItem={this.renderItem}
          ItemSeparatorComponent={() => (<View style={{ height: 5 }} />)}
          ListFooterComponent={() => (<View style={{ height: 5 }} />)}
          ListHeaderComponent={() => (<View style={{ height: 5 }} />)}
        />
      </Container>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  data: makeRefuelApplicationList(),
  isSail: makeSelectIsSail(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getOfflineArrivePortRecordList: getOfflineArrivePortRecordListPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(OfflineArrivePOrtRecordList);
