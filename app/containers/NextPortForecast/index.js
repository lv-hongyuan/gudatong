import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { InputGroup, Container, Content, Item, Label, View } from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import HeaderButtons from 'react-navigation-header-buttons';
import _ from 'lodash';
import {
  makeSelectNextPortForecastSuccess,
  makeSelectShipName,
  makeSelectVoyageCode,
  makeSelectPortInfo, makeSelectHasNextPort,
} from './selectors';
import { getNextPortForecastAction, updateNextPortForecastAction } from './actions';
import commonStyles from '../../common/commonStyles';
import DatePullSelector from '../../components/DatePullSelector';
import EmptyView from '../../components/EmptyView';
import screenHOC from '../screenHOC';
import InputItem from '../../components/InputItem';
import AlertView from '../../components/Alert';

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    borderBottomColor: 'transparent',
  },
  label: {},
  text: {
    flex: 2,
  },
  container: {
    backgroundColor: '#FFFFFF',
    paddingLeft: 20,
    paddingTop: 10,
    paddingBottom: 10,
    paddingRight: 20,
  },
});

/**
 * 下港预报
 * Created by jianzhexu on 2018/3/26
 */
@screenHOC
class NextPortForecast extends React.PureComponent {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.getParam('title', '下港预报'),
    headerRight: (
      <HeaderButtons>
        <HeaderButtons.Item
          show={navigation.getParam('hasNextPort', false) ? 'always' : 'never'}
          title="保存"
          buttonStyle={{ fontSize: 14, color: '#ffffff' }}
          onPress={() => {
            const handleSubmitFunc = navigation.getParam('submit', undefined);
            if (handleSubmitFunc) {
              requestAnimationFrame(() => {
                handleSubmitFunc();
              });
            }
          }}
        />
      </HeaderButtons>
    ),
  });
  static propTypes = {
    navigation: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    hasNextPort: PropTypes.bool.isRequired,
    success: PropTypes.bool.isRequired,
    data: PropTypes.object.isRequired,
    shipName: PropTypes.string.isRequired,
    voyageCode: PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      data: props.data || {},
    };
    props.navigation.setParams({
      hasNextPort: props.hasNextPort,
      submit: this.handleSubmit,
    });
  }

  componentDidMount() {
    if (this.props.hasNextPort) {
      this.props.dispatch(getNextPortForecastAction());
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.data !== nextProps.data) {
      this.setState({ data: nextProps.data || {} });
    }
    if (nextProps.success && this.state.isUpdata) {
      this.setState({
        isUpdata: false,
      }, () => {
        this.props.navigation.goBack();
      });
    }
    if (this.props.hasNextPort !== nextProps.hasNextPort) {
      this.props.navigation.setParams({ hasNextPort: this.props.hasNextPort });
    }
  }

  setData(params) {
    this.setState({ data: { ...this.state.data, ...params } });
  }

  handleSubmit = () => {
    const { wharf, time, berth } = this.state.data;
    if (_.isEmpty(wharf)) {
      AlertView.show({ message: '请输入靠泊码头' });
      return;
    }
    if (_.isEmpty(berth)) {
      AlertView.show({ message: '请输入靠泊泊位' });
      return;
    }
    if (!time) {
      AlertView.show({ message: '请选择预计到港时间' });
      return;
    }
    this.props.dispatch(updateNextPortForecastAction({ wharf, time, berth }));
  };

  render() {
    return (
      <Container>
        <Content>
          {
            this.props.hasNextPort ? (
              <View style={styles.container}>
                <InputGroup style={commonStyles.inputGroup}>
                  <Item style={commonStyles.inputItem}>
                    <Label style={commonStyles.readOnlyInputLabel}>船名:</Label>
                    <Label style={commonStyles.text}>{this.props.shipName}</Label>
                  </Item>
                </InputGroup>
                <InputGroup style={commonStyles.inputGroup}>
                  <Item style={commonStyles.inputItem}>
                    <Label style={commonStyles.readOnlyInputLabel}>航次:</Label>
                    <Label style={commonStyles.text}>{this.props.voyageCode}</Label>
                  </Item>
                </InputGroup>
                <InputGroup style={commonStyles.inputGroup}>
                  <Item style={commonStyles.inputItem}>
                    <Label style={commonStyles.readOnlyInputLabel}>港口:</Label>
                    <Label style={commonStyles.text}>{this.state.data.nextPortName}</Label>
                  </Item>
                </InputGroup>
                <InputGroup style={commonStyles.inputGroup}>
                  <InputItem
                    label="码头:"
                    value={this.state.data.wharf}
                    onChangeText={(text) => {
                      this.setData({ wharf: text.trim() });
                    }}
                  />
                  <InputItem
                    label="泊位:"
                    value={this.state.data.berth}
                    onChangeText={(text) => {
                      this.setData({ berth: text.trim() });
                    }}
                  />
                </InputGroup>
                <InputGroup style={commonStyles.inputGroup}>
                  <Item style={commonStyles.inputItem}>
                    <Label style={commonStyles.inputLabel}>预计到港时间:</Label>
                    <DatePullSelector
                      value={this.state.data.time || null}
                      onChangeValue={(value) => {
                        this.setData({ time: value });
                      }}
                    />
                  </Item>
                </InputGroup>
              </View>
            ) : (
              <EmptyView />
            )
          }
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  shipName: makeSelectShipName(),
  voyageCode: makeSelectVoyageCode(),
  // portName: makeSelectPortName(),
  hasNextPort: makeSelectHasNextPort(),
  success: makeSelectNextPortForecastSuccess(),
  data: makeSelectPortInfo(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(NextPortForecast);
