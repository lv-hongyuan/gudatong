import {
  UPDATE_NEXT_PORT_FORECAST_ACTION_RESULT,
  GET_NEXT_PORT_FORECAST_ACTION,
  GET_NEXT_PORT_FORECAST_ACTION_RESULT,
  UPDATE_NEXT_PORT_FORECAST_ACTION,
} from './constants';

const initState = {
  success: false,
  time: '',
  data: {
    shipId: null,
    shipName: null,
    timeDate: null,
    nextPopId: null,
    nextPortName: null,
    time: null,
  },
};

export default function (state = initState, action) {
  switch (action.type) {
    case GET_NEXT_PORT_FORECAST_ACTION:
      return { ...state, success: false };
    case UPDATE_NEXT_PORT_FORECAST_ACTION:
      return { ...state, success: false };
    case UPDATE_NEXT_PORT_FORECAST_ACTION_RESULT:
      return { ...state, success: action.payload.success };
    case GET_NEXT_PORT_FORECAST_ACTION_RESULT:
      return {
        ...state,
        success: action.payload.success,
        data: action.payload.data,
      };
    default:
      return state;
  }
}
