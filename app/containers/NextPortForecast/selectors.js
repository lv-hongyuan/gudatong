import { createSelector } from 'reselect/es';

const selectNextPortForecastDomain = () => state => state.nextPortForecast;
const selectShipStateDomain = () => state => state.shipWork.data;

const makeSelectPortInfo = () => createSelector(
  selectNextPortForecastDomain(),
  subState => subState.data,
);

const makeSelectHasNextPort = () => createSelector(
  selectShipStateDomain(),
  subState => subState.nextPortId !== 0,
);

const makeSelectShipName = () => createSelector(
  selectShipStateDomain(),
  subState => subState.shipName,
);

const makeSelectVoyageCode = () => createSelector(
  selectShipStateDomain(),
  subState => subState.voyageCode,
);

// const makeSelectPortName = () => createSelector(
//     selectNextPortForecastDomain(),
//     (subState) => {
//         return subState.nextPortName;
//     }
// );

const makeSelectNextPortForecastSuccess = () => createSelector(
  selectNextPortForecastDomain(),
  (subState) => {
    console.debug(subState.success);
    return subState.success;
  },
);

export {
  makeSelectPortInfo,
  makeSelectNextPortForecastSuccess,
  makeSelectHasNextPort,
  makeSelectShipName,
  makeSelectVoyageCode,
  // makeSelectPortName
};
