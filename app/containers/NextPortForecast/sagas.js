import { call, put, select, takeLatest } from 'redux-saga/effects';
import { UPDATE_NEXT_PORT_FORECAST_ACTION, GET_NEXT_PORT_FORECAST_ACTION } from './constants';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import { updateNextPortForecastResultAction, getNextPortForecastResultAction } from './actions';
import { errorMessage } from '../ErrorHandler/actions';
import { getPopId } from '../ShipWork/sagas';

const nextPId = state => state.nextPortForecast.data.nextPopId;

function* updateNextPortForecastApply(action) {
  console.log(action);
  try {
    const nPid = yield select(nextPId);
    const { wharf, berth, time } = action.payload;
    const response = yield call(
      request,
      ApiFactory.nextPortEta({
        nextPopId: nPid,
        wharf,
        berth,
        time,
      }),
    );
    yield put(updateNextPortForecastResultAction(response, true));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(updateNextPortForecastResultAction(undefined, false));
  }
}

export function* updateNextPortForecastSaga() {
  yield takeLatest(UPDATE_NEXT_PORT_FORECAST_ACTION, updateNextPortForecastApply);
}

function* getNextPortForecastAction(action) {
  console.log(action);
  try {
    const popId = yield call(getPopId);
    const response = yield call(request, ApiFactory.nextPortEtaShow(popId));
    yield put(getNextPortForecastResultAction(response, true));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(getNextPortForecastResultAction(undefined, false));
  }
}

export function* getNextPortForecastActionSaga() {
  yield takeLatest(GET_NEXT_PORT_FORECAST_ACTION, getNextPortForecastAction);
}

// All sagas to be loaded
export default [
  updateNextPortForecastSaga,
  getNextPortForecastActionSaga,
];
