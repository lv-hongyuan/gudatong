import {
  UPDATE_NEXT_PORT_FORECAST_ACTION,
  UPDATE_NEXT_PORT_FORECAST_ACTION_RESULT,
  GET_NEXT_PORT_FORECAST_ACTION,
  GET_NEXT_PORT_FORECAST_ACTION_RESULT,
} from './constants';

export function updateNextPortForecastAction(data) {
  return {
    type: UPDATE_NEXT_PORT_FORECAST_ACTION,
    payload: data,
  };
}

export function updateNextPortForecastResultAction(data, success) {
  return {
    type: UPDATE_NEXT_PORT_FORECAST_ACTION_RESULT,
    payload: {
      data,
      success,
    },
  };
}

export function getNextPortForecastAction() {
  return {
    type: GET_NEXT_PORT_FORECAST_ACTION,
  };
}

export function getNextPortForecastResultAction(data, success) {
  return {
    type: GET_NEXT_PORT_FORECAST_ACTION_RESULT,
    payload: {
      data,
      success,
    },
  };
}
