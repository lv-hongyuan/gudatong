import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { Container, Content, View, Text } from 'native-base';
import _ from 'lodash';
import { createStructuredSelector } from 'reselect/es';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import FastImage from 'react-native-fast-image';
import { getHelpDetailPromise } from './actions';
import myTheme from '../../Themes';
import { makeHelpDetail, makeIsLoading } from './selectors';
import screenHOC from '../screenHOC';

const styles = StyleSheet.create({
  container: {
    width: '100%',
    backgroundColor: 'white',
    padding: 10,
  },
  label: {
    fontSize: 14,
    color: '#535353',
    paddingTop: 10,
  },
  text: {
    fontSize: 14,
    color: myTheme.inputColor,
    paddingTop: 10,
    flex: 1,
  },
  image: {
    marginTop: 10,
    width: '100%',
    borderWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
  },
});

/**
 * 帮助信息页面
 * Created by jianzhexu on 2018/3/29
 */
@screenHOC
class HelpDetail extends React.PureComponent {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.getParam('title', '帮助信息'),
  });
  static propTypes = {
    navigation: PropTypes.object.isRequired,
    getHelpDetailPromise: PropTypes.func.isRequired,
    data: PropTypes.object,
  };
  static defaultProps = {
    data: {},
  };

  constructor(props) {
    super(props);
    this.state = {
      imageRatio: null,
    };
  }

  componentWillMount() {
    this.reLoadingPage();
  }

  onLoadImage = (event) => {
    const { width, height } = event.nativeEvent;
    this.setState({ imageRatio: width / height });
  };

  reLoadingPage = () => {
    requestAnimationFrame(() => {
      this.props.getHelpDetailPromise(this.props.navigation.getParam('id'))
        .then(() => {
        })
        .catch(() => {
        });
    });
  };

  render() {
    const { wordMean, wordName, wordPic } = (this.props.data || {});

    return (
      <Container theme={myTheme}>
        <Content>
          <View style={styles.container}>
            <Text style={styles.label}>{wordName}</Text>
            <Text style={styles.text}>{wordMean}</Text>
            <FastImage
              style={[styles.image, { aspectRatio: this.state.imageRatio }]}
              source={{ uri: _.isEmpty(wordPic) ? 'empty' : wordPic }}
              onLoad={this.onLoadImage}
            />
          </View>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  data: makeHelpDetail(),
  isLoading: makeIsLoading(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getHelpDetailPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(HelpDetail);
