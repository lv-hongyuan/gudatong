import { getHelpDetailRoutine } from './actions';

const initState = {
  data: undefined,
  loading: false,
};

export default function (state = initState, action) {
  switch (action.type) {
    case getHelpDetailRoutine.TRIGGER:
      return { ...state, data: undefined, loading: true };
    case getHelpDetailRoutine.SUCCESS:
      return { ...state, data: action.payload };
    case getHelpDetailRoutine.FAILURE:
      return { ...state, error: action.payload };
    case getHelpDetailRoutine.FULFILL:
      return { ...state, loading: false };

    default:
      return state;
  }
}
