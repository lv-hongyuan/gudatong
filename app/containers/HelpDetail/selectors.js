import { createSelector } from 'reselect';

const selectHelpDetailDomain = () => state => state.helpDetail;

const makeHelpDetail = () => createSelector(
  selectHelpDetailDomain(),
  (subState) => {
    console.debug(subState.data);
    return subState.data;
  },
);

const makeIsLoading = () => createSelector(
  selectHelpDetailDomain(),
  (subState) => {
    console.debug(subState.loading);
    return subState.loading;
  },
);

export {
  makeHelpDetail,
  makeIsLoading,
};
