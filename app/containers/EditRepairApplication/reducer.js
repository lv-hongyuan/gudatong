import {
  createRepairApplicationRoutine,
  updateRepairApplicationRoutine,
} from './actions';

const initState = {
  parentData: [],
  childData: [],
};

export default function (state = initState, action) {
  switch (action.type) {
    // 创建加油申请
    case createRepairApplicationRoutine.TRIGGER:
      return { ...state, loading: true };
    case createRepairApplicationRoutine.SUCCESS:
      return state;
    case createRepairApplicationRoutine.FAILURE:
      return { ...state, error: action.payload };
    case createRepairApplicationRoutine.FULFILL:
      return { ...state, loading: false };

    // 编辑加油申请
    case updateRepairApplicationRoutine.TRIGGER:
      return { ...state, loading: true };
    case updateRepairApplicationRoutine.SUCCESS:
      return state;
    case updateRepairApplicationRoutine.FAILURE:
      return { ...state, error: action.payload };
    case updateRepairApplicationRoutine.FULFILL:
      return { ...state, loading: false };

    default:
      return state;
  }
}
