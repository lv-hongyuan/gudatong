/*
 *
 * EditRepairApplication constants
 *
 */
export const CREATE_REPAIR_APPLICATION = 'app/EditRepairApplication/CREATE_REPAIR_APPLICATION';

export const UPDATE_REPAIR_APPLICATION = 'app/EditRepairApplication/UPDATE_REPAIR_APPLICATION';
