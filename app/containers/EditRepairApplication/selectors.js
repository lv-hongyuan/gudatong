import { createSelector } from 'reselect/es';

const selectShipStateDomain = () => state => state.shipWork.data;

const makeSelectPopId = () => createSelector(
  selectShipStateDomain(),
  (subState) => {
    console.debug(subState.success);
    return subState.popId;
  },
);

const makeIsEdit = () => createSelector(
  (state, props) => props.navigation.state.params,
  (subState) => {
    console.debug(subState.isEdit);
    return subState.isEdit;
  },
);

const makeData = () => createSelector(
  (state, props) => props.navigation.state.params,
  (subState) => {
    console.debug(subState.data);
    return subState.data;
  },
);

const makeOnoBack = () => createSelector(
  (state, props) => props.navigation.state.params,
  (subState) => {
    console.debug(subState.onGoBack);
    return subState.onGoBack;
  },
);

const makeSelectPortName = () => createSelector(
  selectShipStateDomain(),
  subState => subState.portName,
);

export {
  makeSelectPopId,
  makeIsEdit,
  makeData,
  makeOnoBack,
  makeSelectPortName,
};
