import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import {
  createRepairApplicationRoutine,
  updateRepairApplicationRoutine,
} from './actions';
import { errorMessage } from '../ErrorHandler/actions';

function* createRepairApplication(action) {
  console.log(action);
  try {
    yield put(createRepairApplicationRoutine.request());
    const {
      popId,
      portId,
      portName,
      shipId,
      shipName,
      startTime,
      lengthTime,
      reason,
      isEnd,
      state,
    } = action.payload;

    const response = yield call(
      request,
      ApiFactory.createShipRepairApplay({
        popId,
        portId,
        portName,
        shipId,
        shipName,
        startTime,
        lengthTime,
        reason,
        isEnd,
        state,
      }),
    );
    console.log('createRepairApplication', response.toString());
    yield put(createRepairApplicationRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(createRepairApplicationRoutine.failure(e));
  } finally {
    yield put(createRepairApplicationRoutine.fulfill());
  }
}

export function* createRepairApplicationSaga() {
  yield takeLatest(createRepairApplicationRoutine.TRIGGER, createRepairApplication);
}

function* updateRepairApplication(action) {
  console.log(action);
  try {
    yield put(updateRepairApplicationRoutine.request());
    const {
      id,
      popId,
      portId,
      portName,
      shipId,
      shipName,
      startTime,
      lengthTime,
      reason,
      isEnd,
      state,
    } = action.payload;

    const response = yield call(
      request,
      ApiFactory.updateShipRepairApplay({
        id,
        popId,
        portId,
        portName,
        shipId,
        shipName,
        startTime,
        lengthTime,
        reason,
        isEnd,
        state,
      }),
    );
    console.log('updateRepairApplication', response.toString());
    yield put(updateRepairApplicationRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(updateRepairApplicationRoutine.failure(e));
  } finally {
    yield put(updateRepairApplicationRoutine.fulfill());
  }
}

export function* updateRepairApplicationSaga() {
  yield takeLatest(updateRepairApplicationRoutine.TRIGGER, updateRepairApplication);
}

// All sagas to be loaded
export default [
  createRepairApplicationSaga,
  updateRepairApplicationSaga,
];
