import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  CREATE_REPAIR_APPLICATION,
  UPDATE_REPAIR_APPLICATION,
} from './constants';

export const createRepairApplicationRoutine = createRoutine(
  CREATE_REPAIR_APPLICATION,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const createRepairApplicationPromise = promisifyRoutine(createRepairApplicationRoutine);

export const updateRepairApplicationRoutine = createRoutine(
  UPDATE_REPAIR_APPLICATION,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const updateRepairApplicationPromise = promisifyRoutine(updateRepairApplicationRoutine);
