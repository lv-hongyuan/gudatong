import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Container, Content, Item, Label, InputGroup, Text } from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import HeaderButtons from 'react-navigation-header-buttons';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import _ from 'lodash';
import {
  createRepairApplicationPromise,
  updateRepairApplicationPromise,
} from './actions';
import {
  makeData,
  makeOnoBack,
  makeIsEdit,
} from './selectors';
import { errorMessage } from '../ErrorHandler/actions';
import commonStyles from '../../common/commonStyles';
import InputItem from '../../components/InputItem';
import stringToNumber from '../../utils/stringToNumber';
import DatePullSelector from '../../components/DatePullSelector';
import screenHOC from '../screenHOC';
import { currentSuitableTime } from '../../utils/suitableDateTime';
import Switch from '../../components/Switch';
import { MaskType } from '../../components/InputItem/TextInput';
import AlertView from '../../components/Alert';


/**
 * 修船申请
 * Created by jianzhexu on 2018/3/23
 */
@screenHOC
class EditRepairApplication extends React.PureComponent {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.getParam('title', '创建修船申请'),
    headerRight: (
      <HeaderButtons>
        <HeaderButtons.Item
          title="保存"
          buttonStyle={{ fontSize: 14, color: '#ffffff' }}
          onPress={() => {

            AlertView.show({
              message: '确定创建修船申请?', showCancel: true, cancelAction: () => {
                console.log('点击了取消')
                return;
              }, confirmAction: () => {
                console.log('点击了确定')
                const handleSubmitFunc = navigation.getParam('submit', undefined);
                if (handleSubmitFunc) {
                  requestAnimationFrame(() => {
                    handleSubmitFunc();
                  });
                }
              }
            })

            // const handleSubmitFunc = navigation.getParam('submit', undefined);
            // if (handleSubmitFunc) {
            //   requestAnimationFrame(() => {
            //     handleSubmitFunc();
            //   });
            // }

            
          }}
        />
      </HeaderButtons>
    ),
  });
  static propTypes = {
    navigation: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    updateRepairApplication: PropTypes.func.isRequired,
    createRepairApplication: PropTypes.func.isRequired,
    onGoBack: PropTypes.func.isRequired,
    data: PropTypes.object.isRequired,
    isEdit: PropTypes.bool.isRequired,
  };

  constructor(props) {
    super(props);

    const data = props.data || {};
    this.state = {
      id: data.id,
      portId: data.portId,
      portName: data.portName,
      shipId: data.shipId,
      shipName: data.shipName,
      startTime: props.data ? (data.startTime || null) : currentSuitableTime(), // 预抵时间
      lengthTime: data.lengthTime,
      reason: data.reason,
      isEnd: data.isEnd,
      state: data.state,
    };
    this.props.navigation.setParams({ submit: this.handleSubmit });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.data && nextProps.data !== this.props.data) {
      const data = nextProps.data || {};
      this.setState({
        id: data.id,
        portId: data.portId,
        portName: data.portName,
        shipId: data.shipId,
        shipName: data.shipName,
        startTime: data.startTime, // 开始时间
        lengthTime: data.lengthTime,
        reason: data.reason,
        isEnd: data.isEnd,
        state: data.state,
      });
    }
  }

  handleSubmit = () => {

    const param = {
      id: this.state.id,
      portId: this.state.portId,
      portName: this.state.portName,
      shipId: this.state.shipId,
      shipName: this.state.shipName,
      startTime: this.state.startTime, // 开始时间
      lengthTime: stringToNumber(this.state.lengthTime),
      reason: this.state.reason,
      isEnd: this.state.isEnd,
      state: this.state.state,
    };
    if (_.isEmpty(param.portName)) {
      this.props.dispatch(errorMessage('请填写修船地点'));
      return;
    }
    if (!param.startTime) {
      this.props.dispatch(errorMessage('请填写修船开始时间'));
      return;
    }
    if (!param.lengthTime) {
      this.props.dispatch(errorMessage('请填写修船时长'));
      return;
    }
    // if (_.isEmpty(this.state.reason)) {
    //     this.props.dispatch(errorMessage('请填写修船原因'));
    //     return;
    // }
    if (this.props.isEdit) {
      this.props.updateRepairApplication(param).then(() => {
        if (this.props.onGoBack) this.props.onGoBack();
        this.props.navigation.goBack();
      }).catch(() => { });
    } else {
      this.props.createRepairApplication(param).then(() => {
        if (this.props.onGoBack) this.props.onGoBack();
        this.props.navigation.goBack();
      }).catch(() => { });
    }
  };

  render() {
    return (
      <Container style={{
        backgroundColor: '#FFFFFF',
      }}
      >
        <Content style={{
          padding: 10,
        }}
        >
          <InputGroup style={commonStyles.inputGroup}>
            <InputItem
              label="修船地点:"
              value={this.state.portName}
              onChangeText={(text) => {
                this.setState({
                  portName: text.trim(),
                });
              }}
            />
          </InputGroup>
          <InputGroup style={commonStyles.inputGroup}>
            <Item style={commonStyles.inputItem}>
              <Label style={commonStyles.inputLabel}>修船开始:</Label>
              <DatePullSelector
                value={this.state.startTime || null}
                onChangeValue={(value) => {
                  this.setState({ startTime: value });
                }}
              />
            </Item>
          </InputGroup>
          <InputGroup style={commonStyles.inputGroup}>
            <InputItem
              label="修船时间:"
              value={this.state.lengthTime || null}
              onChangeText={(text) => {
                this.setState({ lengthTime: text });
              }}
              keyboardType="numeric"
              maskType={MaskType.FLOAT}
              rightItem={<Text style={commonStyles.tail}>小时</Text>}
            />
          </InputGroup>
          <InputGroup style={commonStyles.inputGroup}>
            <InputItem
              label="修船原因:"
              value={this.state.reason || null}
              onChangeText={(text) => {
                this.setState({
                  reason: text,
                });
              }}
            />
          </InputGroup>
          <InputGroup style={commonStyles.inputGroup}>
            <Item style={[commonStyles.inputItem, { borderBottomWidth: 0 }]}>
              <Label style={commonStyles.inputLabel}>是否结束:</Label>
              <Switch
                value={this.state.isEnd === 1}
                onPress={() => {
                  this.setState({ isEnd: this.state.isEnd === 1 ? 0 : 1 });
                }}
                style={{
                  height: 30,
                  width: 60,
                }}
              />
            </Item>
          </InputGroup>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  isEdit: makeIsEdit(),
  data: makeData(),
  onGoBack: makeOnoBack(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      createRepairApplication: createRepairApplicationPromise,
      updateRepairApplication: updateRepairApplicationPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EditRepairApplication);
