import _ from 'lodash';
import {
  getVoyageInstructionListRoutine,
  readOneVoyageInstructionRoutine,
  readAllVoyageInstructionPromise,
} from './actions';
import { RefreshState } from '../../components/RefreshListView';

const initState = {
  list: [],
  isLoading: false,
  refreshState: RefreshState.Idle,
};

export default function (state = initState, action) {
  switch (action.type) {
    case getVoyageInstructionListRoutine.TRIGGER: {
      const { page } = action.payload;
      return {
        ...state,
        loading: true,
        refreshState: page > 1 ? RefreshState.FooterRefreshing : RefreshState.HeaderRefreshing,
      };
    }
    case getVoyageInstructionListRoutine.SUCCESS: {
      const { page, pageSize, list } = action.payload;
      return {
        ...state,
        list: page === 1 ? list : state.list.concat(list),
        refreshState: list.length < pageSize ? RefreshState.NoMoreData : RefreshState.Idle,
      };
    }
    case getVoyageInstructionListRoutine.FAILURE: {
      const { page, error } = action.payload;
      return {
        ...state, list: page === 1 ? [] : state.list, error, refreshState: RefreshState.Failure,
      };
    }
    case getVoyageInstructionListRoutine.FULFILL: {
      return { ...state, loading: false };
    }

    // 已读
    case readOneVoyageInstructionRoutine.TRIGGER:
      return { ...state, isLoading: true };
    case readOneVoyageInstructionRoutine.SUCCESS: {
      const newList = state.list.map((item) => {
        if (item.id === action.payload) {
          const newItem = _.cloneDeep(item);
          newItem.readState = 2;
          return newItem;
        }
        return item;
      });
      // 角标
      const badge = newList.reduce((pre, current) => {
        const unRead = current.readState === 0 || current.readState === 1;
        return pre + (unRead ? 1 : 0);
      }, 0);
      return { ...state, list: newList, badge };
    }
    case readOneVoyageInstructionRoutine.FAILURE:
      return { ...state, error: action.payload };
    case readOneVoyageInstructionRoutine.FULFILL:
      return { ...state, isLoading: false };

    // 全部已读
    case readAllVoyageInstructionPromise.TRIGGER:
      return { ...state, isLoading: true };
    case readAllVoyageInstructionPromise.SUCCESS: {
      const newList = state.list.map((item) => {
        const newItem = _.cloneDeep(item);
        newItem.readState = 2;
        return newItem;
      });
      // 角标
      const badge = newList.reduce((pre, current) => {
        const unRead = current.readState === 0 || current.readState === 1;
        return pre + (unRead ? 1 : 0);
      }, 0);
      return { ...state, list: newList, badge };
    }
    case readAllVoyageInstructionPromise.FAILURE:
      return { ...state, error: action.payload };
    case readAllVoyageInstructionPromise.FULFILL:
      return { ...state, isLoading: false };

    default:
      return state;
  }
}
