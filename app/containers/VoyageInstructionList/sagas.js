import { call, put, takeLatest } from 'redux-saga/effects';
import _ from 'lodash';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import {
  getVoyageInstructionListRoutine,
  readOneVoyageInstructionRoutine,
  readAllVoyageInstructionRoutine,
} from './actions';
import { errorMessage } from '../ErrorHandler/actions';

function* getVoyageInstructionList(action) {
  console.log(action);
  const { page, pageSize, ...rest } = (action.payload || {});
  try {
    yield put(getVoyageInstructionListRoutine.request());
    const param = {
      filter: {
        logic: 'and',
        filters: [],
      },
      page,
      pageSize,
    };
    Object.keys(rest).forEach((key) => {
      const value = rest[key];
      if (!_.isEmpty(value)) {
        param.filter.filters.push({
          field: key,
          operator: 'eq',
          value,
        });
      }
    });
    const response = yield call(request, ApiFactory.getNoticeList(param));
    console.log(response);
    const { totalElements, content } = (response.dtoList || {});
    yield put(getVoyageInstructionListRoutine.success({
      page, pageSize, list: (content || []), totalElements,
    }));
  } catch (error) {
    console.log(error.message);
    yield put(errorMessage(error.message));
    yield put(getVoyageInstructionListRoutine.failure({ page, pageSize, error }));
  } finally {
    yield put(getVoyageInstructionListRoutine.fulfill());
  }
}

export function* getVoyageInstructionListSaga() {
  yield takeLatest(getVoyageInstructionListRoutine.TRIGGER, getVoyageInstructionList);
}

function* readOneVoyageInstruction(action) {
  console.log(action);
  try {
    yield put(readOneVoyageInstructionRoutine.request());
    const id = action.payload;
    const response = yield call(
      request,
      ApiFactory.readOne(id),
    );
    console.log('readOne', response.toString());
    yield put(readOneVoyageInstructionRoutine.success(id));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(readOneVoyageInstructionRoutine.failure(e));
  } finally {
    yield put(readOneVoyageInstructionRoutine.fulfill());
  }
}

export function* readOneVoyageInstructionSaga() {
  yield takeLatest(readOneVoyageInstructionRoutine.TRIGGER, readOneVoyageInstruction);
}

function* readAllVoyageInstruction(action) {
  console.log(action);
  try {
    yield put(readAllVoyageInstructionRoutine.request());
    const response = yield call(
      request,
      ApiFactory.readAll(),
    );
    console.log('readAll', response.toString());
    yield put(readAllVoyageInstructionRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(readAllVoyageInstructionRoutine.failure(e));
  } finally {
    yield put(readAllVoyageInstructionRoutine.fulfill());
  }
}

export function* readAllVoyageInstructionSaga() {
  yield takeLatest(readAllVoyageInstructionRoutine.TRIGGER, readAllVoyageInstruction);
}

// All sagas to be loaded
export default [
  getVoyageInstructionListSaga,
  readOneVoyageInstructionSaga,
  readAllVoyageInstructionSaga,
];
