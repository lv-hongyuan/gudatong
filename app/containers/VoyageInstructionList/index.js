import React from 'react';
import PropTypes from 'prop-types';
import { SafeAreaView, TouchableOpacity, StyleSheet, Keyboard } from 'react-native';
import { NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import _ from 'lodash';
import { Container, View, Text, Label, Item, InputGroup, Button } from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import HeaderButtons from 'react-navigation-header-buttons';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import {
  getVoyageInstructionListPromise,
  readOneVoyageInstructionPromise,
  readAllVoyageInstructionPromise,
} from './actions';
import myTheme from '../../Themes';
import {
  makeVoyageInstructionList,
  makeSelectRefreshState,
  makeLoading,
} from './selectors';
import screenHOC from '../screenHOC';
import { defaultFormat } from '../../utils/DateFormat';
import RefreshListView from '../../components/RefreshListView';
import commonStyles from '../../common/commonStyles';
import InputItem from '../../components/InputItem';
import VoyageInstructionItem from '../../components/VoyageInstructionItem';
import Svg from '../../components/Svg';

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignContent: 'stretch',
  },
  searchInput: {
    borderWidth: 1,
    borderRadius: 4,
    borderColor: myTheme.borderColor,
    minHeight: 30,
    paddingLeft: 5,
    flexDirection: 'row',
  },
  searchItem: {
    flex: 0,
    width: '49.5%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 0,
    minHeight: 30,
    paddingLeft: 5,
  },
  cellContainer: {
    backgroundColor: 'white',
    marginTop: 8,
  },
  row: {
    paddingLeft: 30,
    paddingTop: 5,
    paddingBottom: 5,
    paddingRight: 30,
    flexDirection: 'row',
  },
  label: {
    // fontSize: 12,
    // color: '#535353',
    // margin: 2,
    width: 80,
    paddingLeft: 0,
    paddingRight: 0,
  },
  text: {
    color: myTheme.inputColor,
    marginLeft: 10,
    flex: 1,
  },
});

const firstPageNum = 1;
const DefaultPageSize = 10;

@screenHOC
class VoyageInstructionList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      shipName: '',
      voyageCode: '',
      inputShipName: '',
      inputVoyageCode: '',
      page: firstPageNum,
      pageSize: DefaultPageSize,
      displayMode: true,
    };
  }

  componentDidMount() {
    this.listView.beginRefresh();
    this.props.navigation.setParams({
      exitSearch: () => {
        this.changeDisplayMode(true);
      },
    });
  }

  onHeaderRefresh = () => {
    // 开始上拉翻页
    this.loadList(false);
  };

  onFooterRefresh = () => {
    // 开始下拉刷新
    this.loadList(true);
  };

  onBtnReadAll = () => {
    this.props.readAllVoyageInstruction()
      .then(() => {
      })
      .catch(() => {
      });
  };

  onReadMessage = (id) => {
    this.props.readOneVoyageInstruction(id)
      .then(() => {
      })
      .catch(() => {
      });
  };

  setSearchValue(callBack) {
    this.setState({
      shipName: this.state.inputShipName,
      voyageCode: this.state.inputVoyageCode,
    }, callBack);
  }

  setInputValue(callBack) {
    this.setState({
      inputShipName: this.state.shipName,
      inputVoyageCode: this.state.voyageCode,
    }, callBack);
  }

  changeDisplayMode = (displayMode) => {
    this.props.navigation.setParams({
      displayMode,
    });
    this.setState({ displayMode });
  };

  loadList(loadMore = false) {
    const {
      shipName,
      voyageCode,
      page,
      pageSize,
    } = this.state;
    this.props.getVoyageInstructionList({
      shipName,
      voyageCode,
      page: loadMore ? page : firstPageNum,
      pageSize,
    })
    // eslint-disable-next-line no-shadow
      .then(({ page }) => {
        this.setState({ page: page + 1 });
      })
      .catch(() => {});
  }

  renderFilter() {
    return (
      <View style={styles.container}>
        {this.state.displayMode ? (
          <SafeAreaView style={{ width: '100%', flexDirection: 'row' }}>
            <View style={{ padding: 10, flex: 1 }}>
              <TouchableOpacity
                activeOpacity={1}
                style={styles.searchInput}
                onPress={() => {
                  this.setInputValue();
                  this.changeDisplayMode(false);
                }}
              >
                {!_.isEmpty(this.state.shipName) && (
                  <View style={styles.searchItem}>
                    <Label style={commonStyles.inputLabel}>船名:</Label>
                    <Text style={commonStyles.text}>{this.state.shipName}</Text>
                  </View>
                )}
                {!_.isEmpty(this.state.voyageCode) && (
                  <View style={styles.searchItem}>
                    <Label style={commonStyles.inputLabel}>航次:</Label>
                    <Text style={commonStyles.text}>{this.state.voyageCode}</Text>
                  </View>
                )}
                {_.isEmpty(this.state.shipName) && _.isEmpty(this.state.voyageCode) && (
                  <Item
                    style={[commonStyles.inputItem, { borderBottomWidth: 0 }]}
                    onPress={() => {
                      this.setInputValue();
                      this.changeDisplayMode(false);
                    }}
                  >
                    <Text style={commonStyles.text}>点击输入要搜索的船名/航次</Text>
                  </Item>
                )}
              </TouchableOpacity>
            </View>
          </SafeAreaView>
        ) : (
          <SafeAreaView style={{ width: '100%' }}>
            <View style={{ padding: 10 }}>
              <InputGroup style={commonStyles.inputGroup}>
                <InputItem
                  autoFocus
                  label="船名:"
                  returnKeyType="search"
                  value={this.state.inputShipName}
                  clearButtonMode="while-editing"
                  onChangeText={(text) => {
                    this.setState({ inputShipName: text });
                  }}
                  onSubmitEditing={() => {
                    Keyboard.dismiss();
                    this.setSearchValue();
                    this.changeDisplayMode(true);
                    this.listView.beginRefresh();
                  }}
                  onFocus={() => {
                    this.changeDisplayMode(false);
                  }}
                />
              </InputGroup>
              <InputGroup style={commonStyles.inputGroup}>
                <InputItem
                  label="航次:"
                  returnKeyType="search"
                  value={this.state.inputVoyageCode}
                  clearButtonMode="while-editing"
                  onChangeText={(text) => {
                    this.setState({ inputVoyageCode: text });
                  }}
                  onSubmitEditing={() => {
                    Keyboard.dismiss();
                    this.setSearchValue();
                    this.changeDisplayMode(true);
                    this.listView.beginRefresh();
                  }}
                  onFocus={() => {
                    this.changeDisplayMode(false);
                  }}
                />
              </InputGroup>
            </View>
            <Button
              onPress={() => {
                Keyboard.dismiss();
                this.setSearchValue();
                this.changeDisplayMode(true);
                this.setState({
                  displayMode: true,
                }, () => {
                  this.listView.beginRefresh();
                });
              }}
              block
              style={{
                height: 45, margin: 10, justifyContent: 'center', backgroundColor: '#DC001B',
              }}
            >
              <Text style={{ color: '#ffffff' }}>搜索</Text>
            </Button>
          </SafeAreaView>
        )}
      </View>
    );
  }

  renderItem = ({ item }) => {
    const {
      sendTime, id, shipName, voyageCode, line,
    } = item || {};
    return (
      <VoyageInstructionItem
        date={defaultFormat(sendTime)}
        hasRead={item.readState === 2}
        onReadMessage={() => {
          this.onReadMessage(id);
        }}
      >
        <View style={styles.cellContainer}>
          <View style={styles.row}>
            <Text style={styles.label}>舰名：</Text>
            <Text style={styles.text}>{shipName || '-----------'}</Text>
          </View>
          <View style={styles.row}>
            <Text style={styles.label}>航次：</Text>
            <Text style={styles.text}>{voyageCode || '-----------'}</Text>
          </View>
          <View style={styles.row}>
            <Text style={styles.label}>执行航线：</Text>
            <Text style={styles.text}>{line || '-----------'}</Text>
          </View>
        </View>
      </VoyageInstructionItem>
    );
  };

  render() {
    return (
      <Container theme={myTheme} style={{ backgroundColor: '#E0E0E0' }}>
        {this.renderFilter()}
        <View style={{ flex: 1, width: '100%' }}>
          <RefreshListView
            contentInsetAdjustmentBehavior="scrollableAxes"
            ref={(ref) => { this.listView = ref; }}
            style={{ width: '100%' }}
            data={this.props.list || []}
            keyExtractor={item => `${item.id}`}
            renderItem={this.renderItem}
            refreshState={this.props.refreshState}
            onHeaderRefresh={this.onHeaderRefresh}
            onFooterRefresh={this.onFooterRefresh}
            ListHeaderComponent={() => (<View style={{ height: 5 }} />)}
            ItemSeparatorComponent={() => (<View style={{ height: 5 }} />)}
          />
          {!this.state.displayMode && (
            <View style={{
              position: 'absolute', width: '100%', height: '100%', backgroundColor: '#ffffff',
            }}
            />
          )}
        </View>
      </Container>
    );
  }
}

VoyageInstructionList.navigationOptions = ({ navigation }) => ({
  title: '航次指令',
  headerLeft: (
    <HeaderButtons>
      {navigation.getParam('displayMode', true) ? (
        <HeaderButtons.Item
          title=""
          buttonWrapperStyle={{ padding: 10 }}
          ButtonElement={<Svg icon="icon_back" size="20" color="white" />}
          onPress={() => {
            navigation.dispatch(NavigationActions.back());
          }}
        />
      ) : (
        <HeaderButtons.Item
          title="取消"
          buttonStyle={{ fontSize: 14, color: '#ffffff' }}
          onPress={() => {
            const exitSearch = navigation.getParam('exitSearch');
            exitSearch();
          }}
        />
      )}
    </HeaderButtons>
  ),
  headerRight: (
    <HeaderButtons>
      {navigation.getParam('displayMode', true) ? (
        <HeaderButtons.Item
          title="全部已读"
          buttonStyle={{ fontSize: 14, color: '#ffffff' }}
          onPress={navigation.getParam('onBtnReadAll')}
        />
      ) : null}
    </HeaderButtons>
  ),
});

VoyageInstructionList.propTypes = {
  navigation: PropTypes.object.isRequired,
  getVoyageInstructionList: PropTypes.func.isRequired,
  readOneVoyageInstruction: PropTypes.func.isRequired,
  readAllVoyageInstruction: PropTypes.func.isRequired,
  list: PropTypes.array,
  refreshState: PropTypes.number.isRequired,
};
VoyageInstructionList.defaultProps = {
  list: [],
};

const mapStateToProps = createStructuredSelector({
  list: makeVoyageInstructionList(),
  isLoading: makeLoading(),
  refreshState: makeSelectRefreshState(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getVoyageInstructionList: getVoyageInstructionListPromise,
      readOneVoyageInstruction: readOneVoyageInstructionPromise,
      readAllVoyageInstruction: readAllVoyageInstructionPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(VoyageInstructionList);
