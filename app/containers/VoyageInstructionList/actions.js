import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  GET_VOYAGE_INSTRUCTION_LIST,
  READ_ONE_VOYAGE_INSTRUCTION,
  READ_ALL_VOYAGE_INSTRUCTION,
} from './constants';

export const getVoyageInstructionListRoutine = createRoutine(
  GET_VOYAGE_INSTRUCTION_LIST,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const getVoyageInstructionListPromise = promisifyRoutine(getVoyageInstructionListRoutine);

export const readOneVoyageInstructionRoutine = createRoutine(
  READ_ONE_VOYAGE_INSTRUCTION,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const readOneVoyageInstructionPromise = promisifyRoutine(readOneVoyageInstructionRoutine);

export const readAllVoyageInstructionRoutine = createRoutine(
  READ_ALL_VOYAGE_INSTRUCTION,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const readAllVoyageInstructionPromise = promisifyRoutine(readAllVoyageInstructionRoutine);
