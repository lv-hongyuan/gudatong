import { createSelector } from 'reselect/es';

const selectVoyageInstructionListDomain = () => state => state.voyageInstructionList;

const makeVoyageInstructionList = () => createSelector(
  selectVoyageInstructionListDomain(),
  (subState) => {
    console.debug(subState.success);
    return subState.list;
  },
);

const makeSelectRefreshState = () => createSelector(selectVoyageInstructionListDomain(), (subState) => {
  console.debug(subState);
  return subState.refreshState;
});

const makeLoading = () => createSelector(selectVoyageInstructionListDomain(), (subState) => {
  console.debug(subState.isLoading);
  return subState.isLoading;
});

export {
  makeVoyageInstructionList,
  makeSelectRefreshState,
  makeLoading,
};
