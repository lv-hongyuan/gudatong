/*
 *
 * VoyageInstructionList constants
 *
 */
export const GET_VOYAGE_INSTRUCTION_LIST = 'app/VoyageInstructionList/GET_VOYAGE_INSTRUCTION_LIST';
export const READ_ONE_VOYAGE_INSTRUCTION = 'app/VoyageInstructionList/READ_ONE_VOYAGE_INSTRUCTION';
export const READ_ALL_VOYAGE_INSTRUCTION = 'app/VoyageInstructionList/READ_ALL_VOYAGE_INSTRUCTION';
