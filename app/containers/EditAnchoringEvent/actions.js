import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  CREATE_ANCHORAGE_EVENT,
  UPDATE_ANCHORAGE_EVENT,
  GET_ANCHORAGE_EVENT_TYPES,
} from './constants';

export const getAnchoringEventTypesRoutine = createRoutine(
  GET_ANCHORAGE_EVENT_TYPES,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const getAnchoringEventTypesPromise = promisifyRoutine(getAnchoringEventTypesRoutine);

export const createAnchoringEventRoutine = createRoutine(
  CREATE_ANCHORAGE_EVENT,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const createAnchoringEventPromise = promisifyRoutine(createAnchoringEventRoutine);

export const updateAnchoringEventRoutine = createRoutine(
  UPDATE_ANCHORAGE_EVENT,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const updateAnchoringEventPromise = promisifyRoutine(updateAnchoringEventRoutine);
