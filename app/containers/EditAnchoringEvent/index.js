import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { Container, Content, Item, Label, InputGroup, View } from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import HeaderButtons from 'react-navigation-header-buttons';
import _ from 'lodash';
import { ShipEventRange } from '../../common/Constant';
import AlertView from '../../components/Alert';
import {
  makeAnchoringEventTypes,
  makeIsEdit, makeRange,
} from './selectors';
import Selector from '../../components/Selector';
import {
  getAnchoringEventTypesPromise,
  createAnchoringEventPromise,
  updateAnchoringEventPromise,
} from './actions';
import commonStyles from '../../common/commonStyles';
import InputItem from '../../components/InputItem';
import DatePullSelector from '../../components/DatePullSelector';
import screenHOC from '../screenHOC';
import { makePortName, makeSelectPopId } from '../ShipWork/selectors';

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    borderBottomColor: 'transparent',
  },
  label: {},
  text: {
    flex: 2,
  },
  container: {
    backgroundColor: '#FFFFFF',
    paddingLeft: 20,
    paddingTop: 10,
    paddingBottom: 10,
    paddingRight: 20,
  },
  item: {
    height: 40,
    justifyContent: 'center',
  },
  itemContainer: {
    flexDirection: 'column',
    marginTop: 10,
    minHeight: 100,
    paddingBottom: 30,
    paddingLeft: 20,
    paddingRight: 20,
  },
});

/**
 * 创建/编辑锚泊和漂航记录
 * Created by jianzhexu on 2018/3/26
 */
@screenHOC
class EditAnchoringEvent extends React.PureComponent {
  static navigationOptions = ({ navigation }) => ({
    // title: navigation.getParam('title', '编辑锚泊/漂航记录'),
    title: navigation.getParam('title', '编辑港内停航原因'),
    headerRight: (
      <HeaderButtons>
        <HeaderButtons.Item
          title="保存"
          buttonStyle={{ fontSize: 14, color: '#ffffff' }}
          onPress={navigation.getParam('submit')}
        />
      </HeaderButtons>
    ),
  });

  constructor(props) {
    super(props);

    const data = props.navigation.getParam('data', {});
    this.state = {
      portName: data.portName,
      id: data.id,
      popId: data.popId,
      startTime: data.startTime, // 事发时间
      endTime: data.endTime, // 结束时间
      hidePlace: data.hidePlace, // 结束时间
      typeChildCode: data.typeChildCode, // 事件子类型
      typeChildText: data.typeChildText, // 事件子类型
      describe: data.describe, // 事件描述
    };
    this.props.navigation.setParams({ submit: this.handleSubmit });
  }

  componentDidMount() {
    this.props.navigation.setParams({ submit: this.handleSubmit });
    if (!this.props.isEdit) {
      this.props.getAnchoringEventTypes({ range: this.props.range })
        .then(() => {}).catch(() => {});
    }
  }

  onBtnType = () => {
    if (this.props.types.length > 0) {
      this.selector.showPicker();
    } else {
      this.props.getAnchoringEventTypes({ range: this.props.range })
        .then(() => {
          this.selector.showPicker();
        }).catch(() => {});
    }
  };

  onChangeType = (item) => {
    if (this.state.typeChildCode !== item.code) {
      this.setState({
        typeChildCode: item.code,
        typeChildText: item.text,
      });
    }
  };

  handleSubmit = () => {
    if (_.isNil(this.state.typeChildCode)) {
      AlertView.show({ message: '请选择事件类型' });
      return;
    }
    if (!this.state.hidePlace) {
      AlertView.show({ message: '请填写抛锚位置' });
      return;
    }
    if (this.state.typeChildText === '其他原因' && _.isEmpty(this.state.describe)) {
      AlertView.show({ message: '请填写备注' });
      return;
    }
    if (!this.state.startTime) {
      AlertView.show({ message: '请选择抛锚时间' });
      return;
    }
    if (!this.state.endTime) {
      // AlertView.show({ message: '请选择结束时间' });
      // return;
      this.setState({endTime:''})
    }
    if (this.state.endTime && this.state.endTime < this.state.startTime) {
      AlertView.show({ message: '结束时间不能早于开始时间' });
      return;
    }

    if (this.props.isEdit) {
      this.props.updateAnchoringEvent({
        id: this.state.id,
        popId: this.state.popId,
        portName: this.state.portName,
        startTime: this.state.startTime,
        endTime: this.state.endTime,
        hidePlace: this.state.hidePlace,
        typeChildCode: this.state.typeChildCode,
        typeChildText: this.state.typeChildText,
        describe: this.state.describe,
        range: this.props.range,
      }).then(() => {
        this.props.navigation.goBack();
        const goBackCallback = this.props.navigation.getParam('onGoBack');
        if (goBackCallback) goBackCallback();
      })
        .catch(() => {});
    } else {
      if (this.state.startTime <= 0 || this.state.typeChildText === '') {
        AlertView.show({ message: '必须填写事发时间，事件类型' });
        return;
      }

      this.props.createAnchoringEvent({
        popId: this.props.popId,
        portName: this.props.portName,
        startTime: this.state.startTime,
        endTime: this.state.endTime,
        hidePlace: this.state.hidePlace,
        typeChildCode: this.state.typeChildCode,
        typeChildText: this.state.typeChildText,
        describe: this.state.describe,
        range: this.props.range,
      }).then(() => {
        this.props.navigation.goBack();
        const goBackCallback = this.props.navigation.getParam('onGoBack');
        if (goBackCallback) goBackCallback();
      })
        .catch(() => {});
    }
  };

  render() {
    return (
      <Container>
        <Content>
          <View style={styles.container}>
            {/*<InputGroup style={commonStyles.inputGroup}>*/}
            {/*<Item style={commonStyles.inputItem}>*/}
            {/*<Label style={commonStyles.readOnlyInputLabel}>抛锚地点:</Label>*/}
            {/*<Label style={commonStyles.text}>{this.state.portName || this.props.portName}</Label>*/}
            {/*</Item>*/}
            {/*</InputGroup>*/}
            <InputGroup style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>事件类型:</Label>
                <Selector
                  value={this.state.typeChildText}
                  items={this.props.types || []}
                  disabled={this.props.isEdit}
                  ref={(ref) => {
                    this.selector = ref;
                  }}
                  onPress={this.onBtnType}
                  pickerTitle="请选择事件类型"
                  getItemValue={item => item.text}
                  getItemText={item => item.text}
                  onSelected={this.onChangeType}
                />
              </Item>
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="抛锚位置:"
                value={this.state.hidePlace}
                editable={!this.state.readonly}
                onChangeText={(text) => {
                  this.setState({ hidePlace: text });
                }}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="备&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;注:"
                value={this.state.describe}
                onChangeText={(text) => {
                  this.setState({
                    describe: text.trim(),
                  });
                }}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>开始时间:</Label>
                <DatePullSelector
                  value={this.state.startTime || null}
                  onChangeValue={(value) => {
                    this.setState({ startTime: value });
                  }}
                />
              </Item>
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>结束时间:</Label>
                <DatePullSelector
                  value={this.state.endTime || null}
                  minValue={this.state.startTime}
                  onChangeValue={(value) => {
                    this.setState({ endTime: value });
                  }}
                />
              </Item>
            </InputGroup>
          </View>
        </Content>
      </Container>
    );
  }
}

EditAnchoringEvent.propTypes = {
  navigation: PropTypes.object.isRequired,
  getAnchoringEventTypes: PropTypes.func.isRequired,
  createAnchoringEvent: PropTypes.func.isRequired,
  updateAnchoringEvent: PropTypes.func.isRequired,
  types: PropTypes.array,
  popId: PropTypes.number,
  portName: PropTypes.string,
  isEdit: PropTypes.bool,
  range: PropTypes.number,
};
EditAnchoringEvent.defaultProps = {
  types: [],
  popId: undefined,
  portName: undefined,
  isEdit: false,
  range: ShipEventRange.SHIP_ANCHORING,
};

const mapStateToProps = createStructuredSelector({
  types: makeAnchoringEventTypes(),
  portName: makePortName(),
  popId: makeSelectPopId(),
  isEdit: makeIsEdit(),
  range: makeRange(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getAnchoringEventTypes: getAnchoringEventTypesPromise,
      createAnchoringEvent: createAnchoringEventPromise,
      updateAnchoringEvent: updateAnchoringEventPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EditAnchoringEvent);
