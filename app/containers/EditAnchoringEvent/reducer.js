import {
  getAnchoringEventTypesRoutine,
  createAnchoringEventRoutine,
  updateAnchoringEventRoutine,
} from './actions';

const initState = {
  emergency: undefined,
  isLoading: false,
  loadingError: undefined,
  navBack: false,
  types: [],
  subTypes: [],
};

export default function (state = initState, action) {
  switch (action.type) {
    // 获取突发事件类型
    case getAnchoringEventTypesRoutine.TRIGGER:
      return { ...state, isLoading: true, loadingError: undefined };
    case getAnchoringEventTypesRoutine.SUCCESS:
      return { ...state, types: action.payload };
    case getAnchoringEventTypesRoutine.FAILURE:
      return { ...state, types: [], loadingError: action.payload };
    case getAnchoringEventTypesRoutine.FULFILL:
      return { ...state, isLoading: false };

    // 创建突发事件
    case createAnchoringEventRoutine.TRIGGER:
      return { ...state, isLoading: true, loadingError: undefined };
    case createAnchoringEventRoutine.SUCCESS:
      return { ...state };
    case createAnchoringEventRoutine.FAILURE:
      return { ...state };
    case createAnchoringEventRoutine.FULFILL:
      return { ...state, isLoading: false };

    // 更新突发事件
    case updateAnchoringEventRoutine.TRIGGER:
      return { ...state, isLoading: true, loadingError: undefined };
    case updateAnchoringEventRoutine.SUCCESS:
      return { ...state };
    case updateAnchoringEventRoutine.FAILURE:
      return { ...state };
    case updateAnchoringEventRoutine.FULFILL:
      return { ...state, isLoading: false };

    default:
      return state;
  }
}
