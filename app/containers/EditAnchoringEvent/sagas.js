import { call, put, takeLatest } from 'redux-saga/effects';
import { ShipEventRange } from '../../common/Constant';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import {
  getAnchoringEventTypesRoutine,
  createAnchoringEventRoutine,
  updateAnchoringEventRoutine,
} from './actions';
import { errorMessage } from '../ErrorHandler/actions';

function* getAnchoringEventTypes(action) {
  console.log(action);
  try {
    yield put(getAnchoringEventTypesRoutine.request());
    const response = yield call(
      request,
      ApiFactory.findShipEventTypes(action.payload.range),
    );
    console.log('getAnchoringEventTypes', response.toString());
    yield put(getAnchoringEventTypesRoutine.success(response.dtoList));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(getAnchoringEventTypesRoutine.failure(e));
  } finally {
    yield put(getAnchoringEventTypesRoutine.fulfill());
  }
}

export function* getAnchoringEventTypesSaga() {
  yield takeLatest(getAnchoringEventTypesRoutine.TRIGGER, getAnchoringEventTypes);
}

function* createAnchoringEvent(action) {
  console.log(action);
  try {
    yield put(createAnchoringEventRoutine.request());
    const response = yield call(
      request,
      ApiFactory.createShipEvent(action.payload),
    );
    console.log('createAnchoringEvent', response.toString());
    yield put(createAnchoringEventRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(createAnchoringEventRoutine.failure(e));
  } finally {
    yield put(createAnchoringEventRoutine.fulfill());
  }
}

export function* createAnchoringEventSaga() {
  yield takeLatest(createAnchoringEventRoutine.TRIGGER, createAnchoringEvent);
}

function* updateAnchoringEvent(action) {
  console.log(action);
  try {
    yield put(updateAnchoringEventRoutine.request());
    const response = yield call(
      request,
      ApiFactory.updateShipEvent(action.payload),
    );
    console.log('updateAnchoringEvent', response.toString());
    yield put(updateAnchoringEventRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(updateAnchoringEventRoutine.failure(e));
  } finally {
    yield put(updateAnchoringEventRoutine.fulfill());
  }
}

export function* updateAnchoringEventSaga() {
  yield takeLatest(updateAnchoringEventRoutine.TRIGGER, updateAnchoringEvent);
}

// All sagas to be loaded
export default [
  getAnchoringEventTypesSaga,
  createAnchoringEventSaga,
  updateAnchoringEventSaga,
];

