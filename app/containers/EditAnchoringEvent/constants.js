/*
 *
 * EditAnchoringEvent constants
 *
 */
export const GET_ANCHORAGE_EVENT_TYPES = 'app/EditAnchoringEvent/GET_ANCHORAGE_EVENT_TYPES';
export const CREATE_ANCHORAGE_EVENT = 'app/EditAnchoringEvent/CREATE_ANCHORAGE_EVENT';
export const UPDATE_ANCHORAGE_EVENT = 'app/EditAnchoringEvent/UPDATE_ANCHORAGE_EVENT';
