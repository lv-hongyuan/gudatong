import { createSelector } from 'reselect/es';

const selectEditAnchoringEventDomain = () => state => state.editAnchoringEvent;
const makeAnchoringEventTypes = () => createSelector(selectEditAnchoringEventDomain(), (subState) => {
  console.log(subState);
  return subState.types || [];
});

const makeIsEdit = () => createSelector(
  (state, props) => props.navigation.state.params,
  (subState) => {
    console.debug(subState.isEdit);
    return !!subState.isEdit;
  },
);

const makeRange = () => createSelector(
  (state, props) => props.navigation.state.params,
  (subState) => {
    console.debug(subState && subState.range);
    return subState && subState.range;
  },
);

export {
  makeAnchoringEventTypes,
  makeIsEdit,
  makeRange,
};
