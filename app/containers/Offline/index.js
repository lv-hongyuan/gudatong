import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, TouchableOpacity } from 'react-native';
import HeaderButtons from 'react-navigation-header-buttons';
import { connect } from 'react-redux';
import { Container, Content, Grid, Label, Row } from 'native-base';
import Svg from '../../components/Svg';
import myTheme from '../../Themes';
import screenHOC from '../screenHOC';
import {
  ROUTE_OFFLINE_NAVIGATION_RECORD_LIST,
  ROUTE_OFFLINE_ARRIVEPORT_RECORD_LIST,
  ROUTE_VOYAGE_EVENT_LIST,
  // ROUTE_ANCHORAGE,
} from '../../RouteConstant';

const styles = StyleSheet.create({
  content: {},
  row: {
    height: 50,
    paddingLeft: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    borderBottomWidth: myTheme.borderWidth,
    borderBottomColor: myTheme.borderColor,
  },
  view: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  label: {
    flex: 1,
    paddingLeft: 20,
  },
});

@screenHOC
class Offline extends React.PureComponent {
  static navigationOptions = {
    title: '离线工具',
    headerLeft: <HeaderButtons />,
  };

  static propTypes = {
    navigation: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  onBackPressed = () => false;

  pushToRecordList = () => {
    this.props.navigation.push(ROUTE_OFFLINE_NAVIGATION_RECORD_LIST);
  }
  pushToArrivePortRecordList = () =>{
    this.props.navigation.push(ROUTE_OFFLINE_ARRIVEPORT_RECORD_LIST);
  }
  pushToVoyageEventList = () => {
    this.props.navigation.push(ROUTE_VOYAGE_EVENT_LIST);
  }

  render() {
    return (
      <Container>
        <Content style={styles.content}>
          <Grid>
            <Row style={[styles.row, { marginTop: 10 }]}>
              <TouchableOpacity
                style={styles.view}
                onPress={this.pushToRecordList}
              >
                <Svg icon="file" size={40} />
                <Label style={styles.label}>更新航报</Label>
                <Svg icon="right_arrow" size={40} />
              </TouchableOpacity>
            </Row>
            <Row style={[styles.row, { marginTop: 10 }]}>
              <TouchableOpacity
                style={styles.view}
                onPress={this.pushToArrivePortRecordList}
              >
                <Svg icon="file" size={40} />
                <Label style={styles.label}>抵港确报</Label>
                <Svg icon="right_arrow" size={40} />
              </TouchableOpacity>
            </Row>
            {/* <Row style={[styles.row, { marginTop: 10 }]}> */}
            {/* <TouchableOpacity */}
            {/* style={styles.view} */}
            {/* onPress={() => { */}
            {/* this.props.navigation.push(ROUTE_ANCHORAGE); */}
            {/* }} */}
            {/* > */}
            {/* <Svg icon="file" size={40} /> */}
            {/* <Label style={styles.label}>锚泊漂航</Label> */}
            {/* <Svg icon="right_arrow" size={40} /> */}
            {/* </TouchableOpacity> */}
            {/* </Row> */}
            <Row style={[styles.row, { marginTop: 10 }]}>
              <TouchableOpacity
                style={styles.view}
                onPress={this.pushToVoyageEventList}
              >
                <Svg icon="file" size={40} />
                <Label style={styles.label}>停航原因</Label>
                <Svg icon="right_arrow" size={40} />
              </TouchableOpacity>
            </Row>
          </Grid>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = () => ({});

export default connect(mapStateToProps)(Offline);
