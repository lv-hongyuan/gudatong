import { getAnnouncementListRoutine, getPublishNoticeReadRoutine, getImportantMultipleRoutine, cleanImportantSelectArrRoutine, upDataImportantMultipleRoutine} from './actions';
import fixIdList from '../../utils/fixIdList';
import { RefreshState } from '../../components/RefreshListView';
import _ from "lodash";

const initState = {
  loading: false,
  list: [],
  refreshState: RefreshState.Idle,
  success: false,
  isLoading: false,
  selectArr:[]
};

export default function (state = initState, action) {
  switch (action.type) {
    case getAnnouncementListRoutine.TRIGGER:{
      const { page } = action.payload
      return {
        ...state,
        loading:true,
        refreshState: page > 1 ? RefreshState.FooterRefreshing : RefreshState.HeaderRefreshing
      }
    }
    case getAnnouncementListRoutine.SUCCESS:{
      const { page, pageSize, list } = action.payload;
      return {
        ...state, 
        list: page == 1 ? fixIdList(list) : fixIdList(state.list.concat(list)), 
        refreshState: list.length < pageSize ? RefreshState.NoMoreData : RefreshState.Idle,
      }
    }
    case getAnnouncementListRoutine.FAILURE:{
      const { page, error } = action.payload;
      return {
        ...state, 
        list: page == 1 ? [] : state.list, 
        error, 
        refreshState: RefreshState.Failure,
      };
    }
    case getAnnouncementListRoutine.FULFILL:
      return { ...state, loading: false };

    case getPublishNoticeReadRoutine.TRIGGER:
      return { ...state, loading: true };
    case getPublishNoticeReadRoutine.SUCCESS:
      const newList = state.list.map((item) => {
        if (item.id === action.payload) {
          const newItem = _.cloneDeep(item);
          newItem.state = 1;
          return newItem;
        }
        return item;
      });
      return { ...state, list: newList };

    case getPublishNoticeReadRoutine.FAILURE:
      return {
        ...state, list: [], error: action.payload
      };
    case getPublishNoticeReadRoutine.FULFILL:
      return { ...state, loading: false };
    case getImportantMultipleRoutine.SUCCESS:{
      return {
        ...state, 
        list: action.payload.list,
        selectArr:action.payload.selectArr
      }
    }

    case cleanImportantSelectArrRoutine.SUCCESS:{
      return {
        ...state,
        selectArr:action.payload.selectArr,
        list:action.payload.list
      }
    }

    case upDataImportantMultipleRoutine.TRIGGER:
      return {...state,loading: true}

    case upDataImportantMultipleRoutine.SUCCESS:
      return {...state}

    case upDataImportantMultipleRoutine.FAILURE:
      return{...state,error:action.payload}
    
    case upDataImportantMultipleRoutine.FULFILL:
      return {...state, loading:false}
    default:
      return state;
  }
}
