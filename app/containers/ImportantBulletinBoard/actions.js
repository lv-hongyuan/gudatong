import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_IMPORTANT_ANNOUNCEMENT_LIST, GET_IMPORTANT_PUBLISH_NOTICE_READ_ID, GET_IMPORTANT_MUITIPLE, CLEAN_IMPORTANT__SELECTARR, UPDATE_IMPORTANT__MULTIPLE } from './constants';

export const getAnnouncementListRoutine = createRoutine(GET_IMPORTANT_ANNOUNCEMENT_LIST);
export const getAnnouncementListPromise = promisifyRoutine(getAnnouncementListRoutine);

export const getPublishNoticeReadRoutine =createRoutine(GET_IMPORTANT_PUBLISH_NOTICE_READ_ID);
export const getPublishNoticeReadPromise = promisifyRoutine(getPublishNoticeReadRoutine)

export const getImportantMultipleRoutine =createRoutine(GET_IMPORTANT_MUITIPLE);
export const getImportantMultiplePromise = promisifyRoutine(getImportantMultipleRoutine);

export const cleanImportantSelectArrRoutine = createRoutine(CLEAN_IMPORTANT__SELECTARR);
export const cleanImportantSelectArrPromise = promisifyRoutine(cleanImportantSelectArrRoutine);

export const upDataImportantMultipleRoutine = createRoutine(UPDATE_IMPORTANT__MULTIPLE);
export const upDataImportantMultiplePromise = promisifyRoutine(upDataImportantMultipleRoutine);

