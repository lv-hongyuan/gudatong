import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../ErrorHandler/actions';
import {getAnnouncementListRoutine, getPublishNoticeReadRoutine, getImportantMultipleRoutine, cleanImportantSelectArrRoutine, upDataImportantMultipleRoutine } from './actions';

function* getNoticeList(action) {
  console.log(action);
  const { page, pageSize, ...rest} = (action.payload || {});
  try {
    yield put(getAnnouncementListRoutine.request());
    const param = {
      filter: {
        logic: 'and',
        filters: [],
      },
      page,
      pageSize,
    };
    Object.keys(rest).forEach((key) => {
      const value = rest[key];
      param.filter.filters.push({
        field: key,
        operator: 'eq',
        value,
      });
    });
    console.log('param:',param)
    const response = yield call(request, ApiFactory.getNoticeList(param));
    console.log('航次指令数据+++',response);
    if (response.dtoList) {
      for (var i = 0; i < response.dtoList.content.length; i++) {
        response.dtoList.content[i].select = false
      }
    }
    // yield put(getAnnouncementListRoutine.success(response.dtoList));
    yield put(getAnnouncementListRoutine.success({
      list:(response.dtoList ? response.dtoList.content : []),
      page,
      pageSize,
    }));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(getAnnouncementListRoutine.failure(e));
  } finally {
    yield put(getAnnouncementListRoutine.fulfill());
  }
}

export function* getAnnouncementListSaga() {
  yield takeLatest(getAnnouncementListRoutine.TRIGGER, getNoticeList);
}

function* getPublishNoticeRead(action) {
  console.log(action);
  try {
    yield put(getPublishNoticeReadRoutine.request());
    console.log('yidu//////',JSON.stringify(action.payload))
    const response = yield call(request, ApiFactory.getPublishNoticeRead(action.payload));
    yield put(getPublishNoticeReadRoutine.success(response.id));
  } catch (e) {
    console.log(e.message);
    console.log('报错了');
    yield put(errorMessage(e.message));
    yield put(getPublishNoticeReadRoutine.failure(e));
  } finally {
    yield put(getPublishNoticeReadRoutine.fulfill());
  }
}

export function* getPublishNoticeReadSaga() {
  yield takeLatest(getPublishNoticeReadRoutine.TRIGGER, getPublishNoticeRead);
}

//!点击左侧选择
function* getImportantMultiple(action) {
  const { item, list, selectArr } = (action.payload || {});
  try {
    let IDList = []
    let FistList = list.concat()
    for (var i = 0; i < FistList.length; i++) {
      IDList.push(FistList[i].id)
    }
    let cellindex = IDList.indexOf(item.id)
    let data = selectArr.concat()
    if (!item.select) {
      data.push(item.id)
    } else {
      let aa = data.indexOf(item.id)
      data.splice(aa, 1)
    }
    FistList[cellindex].select = !item.select
    yield put(getImportantMultipleRoutine.success({
      list: FistList,
      selectArr: data
    }));
  } catch (e) {
    console.log(e.message);
  }
}

export function* getImportantMultipleSage() {
  yield takeLatest(getImportantMultipleRoutine.TRIGGER, getImportantMultiple)
}

//!清空多选数据 / 全选数据  key为true时全选  false时清空
function* cleanImportantSelectArr(action) {
  const { list, key, selectArr } = (action.payload || {});
  let data = selectArr.concat()
  let FistList = list.concat()
  if (key) {
    for (var i = 0; i < FistList.length; i++) {
      if ((FistList[i].state == 1) || (FistList[i].select == true)) continue  //!如果数据未已读或者已经手动选中  跳过这此循环
      FistList[i].select = true
      data.push(FistList[i].id)
    }
  } else {
    for (var i = 0; i < FistList.length; i++) {
      if (FistList[i].select == false) continue  //!如果数据为选中跳过这次循环
      FistList[i].select = false
    }
    data = []
  }
  console.log(data);
  yield put(cleanImportantSelectArrRoutine.success({
    list:FistList,
    selectArr: data
  }));
}

export function* cleanImportantSelectSage() {
  yield takeLatest(cleanImportantSelectArrRoutine.TRIGGER, cleanImportantSelectArr)
}

//!多选数据标记为已读
function* upDataImportantMultiple(action) {
  const { ids } = (action.payload || {});
  console.log(ids);
  try {
    yield put(upDataImportantMultipleRoutine.request());
    const response = yield call(request, ApiFactory.upDataMultiple(ids));
    yield put(upDataImportantMultipleRoutine.success(response))
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(upDataImportantMultipleRoutine.failure(e))
  } finally {
    yield put(upDataImportantMultipleRoutine.fulfill())
  }
}
export function* upDataImportantMultipleSage() {
  yield takeLatest(upDataImportantMultipleRoutine.TRIGGER, upDataImportantMultiple)
}


// All sagas to be loaded
export default [getAnnouncementListSaga,getPublishNoticeReadSaga,getImportantMultipleSage, cleanImportantSelectSage, upDataImportantMultipleSage];
