/*
 *
 * BulletinBoard constants
 *
 */
export const GET_IMPORTANT_ANNOUNCEMENT_LIST = 'app/BulletinBoard/GET_IMPORTANT_ANNOUNCEMENT_LIST';
export const GET_IMPORTANT_PUBLISH_NOTICE_READ_ID = 'app/BulletinBoard/GET_IMPORTANT_PUBLISH_NOTICE_READ_ID';
export const GET_IMPORTANT_MUITIPLE = 'app/bulletinBoad/GET_IMPORTANT_MUITIPLE'; //!多选
export const CLEAN_IMPORTANT__SELECTARR = 'app/bulletinBorad/CLEAN_IMPORTANT__SELECTARR';  //!清空多选
export const UPDATE_IMPORTANT__MULTIPLE = 'app/bulletinBorad/UPDATE_IMPORTANT__MULTIPLE';  //!上传标记为已读的消息
