import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { Container, Content, Grid, Label, Row } from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import Svg from '../../components/Svg';
import {
  ROUTE_ANCHORAGE,
  ROUTE_BEFOREHAND_LAYOUT,
  ROUTE_SERVICE_FEE,
  ROUTE_SHIFTING_BERTH,
  ROUTE_TUGBOAT_APPLY_LIST,
  ROUTE_DX_RECORD_LIST,
  ROUTE_SPECIAL_BOX,
} from '../../RouteConstant';
import myTheme from '../../Themes';
import screenHOC from '../screenHOC';
import { ShipEventRange } from '../../common/Constant';

const styles = StyleSheet.create({
  content: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  column: {
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  row: {
    height: 50,
    paddingLeft: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    borderBottomWidth: myTheme.borderWidth,
    borderBottomColor: myTheme.borderColor,
  },
  view: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  label: {
    flex: 1,
    paddingLeft: 20,
  },
  text: {
    fontSize: 14,
    color: '#969696',
  },
});

/**
 * Created by jianzhexu on 2018/3/26
 */
@screenHOC
class MoreOperation extends React.PureComponent {
  static navigationOptions = {
    title: '更多',
  };
  static propTypes = {
    navigation: PropTypes.object.isRequired,
  };

  render() {
    return (
      <Container>
        <Content
          style={styles.content}
          contentInsetAdjustmentBehavior="scrollableAxes"
        >
          <Grid>
            <Row style={[styles.row, { borderTopWidth: myTheme.borderWidth, borderTopColor: myTheme.borderColor }]}>
              <TouchableOpacity
                style={styles.view}
                onPress={() => {
                  this.props.navigation.navigate(ROUTE_ANCHORAGE, {
                    range: ShipEventRange.SHIP_BERTHING
                  });
                }}
              >
                <Svg icon="anchorage" color="#ff0e28" size={40} />
                {/* <Label style={styles.label}>锚泊漂航</Label> */}
                <Label style={styles.label}>突发事件</Label>
                <Svg icon="right_arrow" size={30} />
              </TouchableOpacity>
            </Row>
            <Row style={styles.row}>
              <TouchableOpacity
                style={styles.view}
                onPress={() => {
                  this.props.navigation.navigate(ROUTE_SHIFTING_BERTH);
                }}
              >
                <Svg icon="shiftingBerth" color="#ff0e28" size={40} />
                <Label style={styles.label}>移泊记录</Label>
                <Svg icon="right_arrow" size={30} />
              </TouchableOpacity>
            </Row>
            <Row style={styles.row}>
              <TouchableOpacity
                style={styles.view}
                onPress={() => {
                  this.props.navigation.navigate(ROUTE_SPECIAL_BOX);
                }}
              >
                <Svg icon="specialTypeToteBin" color="#ff0e28" size={40} />
                <Label style={styles.label}>特殊箱</Label>
                <Svg icon="right_arrow" size={30} />
              </TouchableOpacity>
            </Row>
            <Row style={styles.row}>
              <TouchableOpacity
                style={styles.view}
                onPress={() => {
                  this.props.navigation.navigate(ROUTE_DX_RECORD_LIST);
                }}
              >
                <Svg icon="specialTypeToteBin" color="#ff0e28" size={40} />
                <Label style={styles.label}>倒箱记录</Label>
                <Svg icon="right_arrow" size={30} />
              </TouchableOpacity>
            </Row>
            <Row style={styles.row}>
              <TouchableOpacity
                style={styles.view}
                onPress={() => {
                  this.props.navigation.navigate(ROUTE_TUGBOAT_APPLY_LIST);
                }}
              >
                <Svg icon="tugboatApply" color="#ff0e28" size={40} />
                <Label style={styles.label}>拖轮申请</Label>
                <Svg icon="right_arrow" size={30} />
              </TouchableOpacity>
            </Row>
            <Row style={styles.row}>
              <TouchableOpacity
                style={styles.view}
                onPress={() => {
                  this.props.navigation.navigate(ROUTE_SERVICE_FEE);
                }}
              >
                <Svg icon="serviceFee" color="#ff0e28" size={40} />
                <Label style={styles.label}>劳务费申请</Label>
                <Svg icon="right_arrow" size={30} />
              </TouchableOpacity>
            </Row>
            <Row style={styles.row}>
              <TouchableOpacity
                style={styles.view}
                onPress={() => {
                  this.props.navigation.navigate(ROUTE_BEFOREHAND_LAYOUT);
                }}
              >
                {/* <Svg icon="beforehandLayout" color="#E2BDC5" size={40} /> */}
                <Svg icon="beforehandLayout" color="#ff0e28" size={40} />
                <Label style={styles.label}>预配图</Label>
                <Svg icon="right_arrow" size={30} />
              </TouchableOpacity>
            </Row>
          </Grid>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  // loginSuccess: makeSelectLoginSuccess(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(MoreOperation);
