/*
 *
 * EditRefrigeratorRecord constants
 *
 */
export const CREATE_REFRIGERATOR_RECORD = 'app/EditRefrigeratorRecord/CREATE_REFRIGERATOR_RECORD';

export const UPDATE_REFRIGERATOR_RECORD = 'app/EditRefrigeratorRecord/UPDATE_REFRIGERATOR_RECORD';
