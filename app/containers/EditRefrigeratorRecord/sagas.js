import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import {
  createRefrigeratorRecordRoutine,
  updateRefrigeratorRecordRoutine,
} from './actions';
import { errorMessage } from '../ErrorHandler/actions';
import { getPopId } from '../ShipWork/sagas';

function* createRefrigeratorRecord(action) {
  console.log(action);
  try {
    yield put(createRefrigeratorRecordRoutine.request());
    const popId = yield call(getPopId);
    const {
      contNumber,
      unloadingPort,
      shipLoadPosition,
      temperature,
      ventilationOpening,
      contType,
    } = (action.payload || {});
    const response = yield call(
      request,
      ApiFactory.createRefrigerator({
        popId,
        contNumber,
        unloadingPort,
        shipLoadPosition,
        temperature,
        ventilationOpening,
        contType,
      }),
    );
    console.log('createRefrigeratorRecord', response.toString());
    yield put(createRefrigeratorRecordRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(createRefrigeratorRecordRoutine.failure(e));
  } finally {
    yield put(createRefrigeratorRecordRoutine.fulfill());
  }
}

export function* createRefrigeratorRecordSaga() {
  yield takeLatest(createRefrigeratorRecordRoutine.TRIGGER, createRefrigeratorRecord);
}

function* updateRefrigeratorRecord(action) {
  console.log(action);
  try {
    yield put(updateRefrigeratorRecordRoutine.request());
    const popId = yield call(getPopId);
    const {
      id,
      contNumber,
      unloadingPort,
      shipLoadPosition,
      temperature,
      ventilationOpening,
      contType,
    } = (action.payload || {});
    const response = yield call(
      request,
      ApiFactory.updateRefrigerator({
        id,
        popId,
        contNumber,
        unloadingPort,
        shipLoadPosition,
        temperature,
        ventilationOpening,
        contType,
      }),
    );
    console.log('updateRefrigeratorRecord', response.toString());
    yield put(updateRefrigeratorRecordRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(updateRefrigeratorRecordRoutine.failure(e));
  } finally {
    yield put(updateRefrigeratorRecordRoutine.fulfill());
  }
}

export function* updateRefrigeratorRecordSaga() {
  yield takeLatest(updateRefrigeratorRecordRoutine.TRIGGER, updateRefrigeratorRecord);
}

// All sagas to be loaded
export default [
  createRefrigeratorRecordSaga,
  updateRefrigeratorRecordSaga,
];
