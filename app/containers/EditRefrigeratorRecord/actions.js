import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  CREATE_REFRIGERATOR_RECORD,
  UPDATE_REFRIGERATOR_RECORD,
} from './constants';

export const createRefrigeratorRecordRoutine = createRoutine(
  CREATE_REFRIGERATOR_RECORD,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const createRefrigeratorRecordPromise = promisifyRoutine(createRefrigeratorRecordRoutine);

export const updateRefrigeratorRecordRoutine = createRoutine(
  UPDATE_REFRIGERATOR_RECORD,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const updateRefrigeratorRecordPromise = promisifyRoutine(updateRefrigeratorRecordRoutine);
