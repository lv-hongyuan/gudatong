import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Container, Content, InputGroup, Item, Label, Text } from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import HeaderButtons from 'react-navigation-header-buttons';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import _ from 'lodash';
import { Keyboard } from 'react-native';
import {
  createRefrigeratorRecordPromise,
  updateRefrigeratorRecordPromise,
} from './actions';
import {
  makeSelectPopId,
  makeData,
  makeSelectPortName,
  makeOnoBack,
  makeIsEdit, makeSelectShipName,
} from './selectors';
import { errorMessage } from '../ErrorHandler/actions';
import commonStyles from '../../common/commonStyles';
import InputItem from '../../components/InputItem';
import screenHOC from '../screenHOC';
import { contTypeList } from '../../common/Constant';
import Selector from '../../components/Selector';
import { MaskType } from '../../components/InputItem/TextInput';
import stringToNumber from '../../utils/stringToNumber';
import { makePortList } from '../EditDxRecord/selectors';
import { getNextPortListPromise } from '../EditDxRecord/actions';

/**
 * 冷藏箱记录
 * Created by jianzhexu on 2018/3/23
 */
@screenHOC
class EditRefrigeratorRecordRecord extends React.PureComponent {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.getParam('title', '创建冷藏箱记录'),
    headerRight: (
      <HeaderButtons>
        <HeaderButtons.Item
          title="保存"
          buttonStyle={{ fontSize: 14, color: '#ffffff' }}
          onPress={() => {
            const handleSubmitFunc = navigation.getParam('submit', undefined);
            if (handleSubmitFunc) {
              requestAnimationFrame(() => {
                handleSubmitFunc();
              });
            }
          }}
        />
      </HeaderButtons>
    ),
  });
  static propTypes = {
    navigation: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    getNextPortList: PropTypes.func.isRequired,
    updateRefrigeratorRecord: PropTypes.func.isRequired,
    createRefrigeratorRecord: PropTypes.func.isRequired,
    onGoBack: PropTypes.func.isRequired,
    data: PropTypes.object.isRequired,
    isEdit: PropTypes.bool.isRequired,
    portList: PropTypes.array.isRequired,
  };

  constructor(props) {
    super(props);

    const data = props.data || {};
    this.state = {
      id: data.id,
      popId: data.popId,
      contNumber: data.contNumber,
      unloadingPort: data.unloadingPort,
      shipLoadPosition: data.shipLoadPosition,
      temperature: data.temperature,
      ventilationOpening: data.ventilationOpening,
      contType: data.contType,
    };
    this.props.navigation.setParams({ submit: this.handleSubmit });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.data && nextProps.data !== this.props.data) {
      const data = nextProps.data || {};
      this.setState({
        id: data.id,
        popId: data.popId,
        contNumber: data.contNumber,
        unloadingPort: data.unloadingPort,
        shipLoadPosition: data.shipLoadPosition,
        temperature: data.temperature,
        ventilationOpening: data.ventilationOpening,
        contType: data.contType,
      });
    }
  }

  onPressPort = () => {
    Keyboard.dismiss();
    this.props.getNextPortList()
      .then(() => {
        this.select.showPicker();
      })
      .catch(() => {});
  };

  handleSubmit = () => {
    const param = {
      id: this.state.id,
      popId: this.state.popId,
      contNumber: this.state.contNumber,
      unloadingPort: this.state.unloadingPort,
      shipLoadPosition: this.state.shipLoadPosition,
      temperature: stringToNumber(this.state.temperature),
      ventilationOpening: this.state.ventilationOpening,
      contType: this.state.contType,
    };
    if (_.isEmpty(param.contNumber)) {
      this.props.dispatch(errorMessage('请填写箱号'));
      return;
    }
    if (_.isEmpty(param.unloadingPort)) {
      this.props.dispatch(errorMessage('请填写卸货港'));
      return;
    }
    if (_.isEmpty(param.shipLoadPosition)) {
      this.props.dispatch(errorMessage('请填写装船位置'));
      return;
    }
    if (_.isNil(param.temperature)) {
      this.props.dispatch(errorMessage('请填写设置温度'));
      return;
    }
    if (_.isEmpty(param.ventilationOpening)) {
      this.props.dispatch(errorMessage('请填写通风开度'));
      return;
    }
    if (_.isEmpty(param.contType)) {
      this.props.dispatch(errorMessage('请选择箱型'));
      return;
    }
    if (this.props.isEdit) {
      this.props.updateRefrigeratorRecord(param).then(() => {
        if (this.props.onGoBack) this.props.onGoBack();
        this.props.navigation.goBack();
      }).catch(() => {});
    } else {
      this.props.createRefrigeratorRecord(param).then(() => {
        if (this.props.onGoBack) this.props.onGoBack();
        this.props.navigation.goBack();
      }).catch(() => {});
    }
  };

  render() {
    return (
      <Container style={{
        backgroundColor: '#FFFFFF',
      }}
      >
        <Content style={{
          padding: 10,
        }}
        >
          <InputGroup style={commonStyles.inputGroup}>
            <InputItem
              label="箱号:"
              value={this.state.contNumber}
              onChangeText={(text) => {
                this.setState({ contNumber: text });
              }}
              // keyboardType="numeric"
              // maskType={MaskType.INTEGER}
            />
          </InputGroup>
          <InputGroup style={commonStyles.inputGroup}>
            <Item style={commonStyles.inputItem}>
              <Label style={commonStyles.inputLabel}>卸货港:</Label>
              <Selector
                ref={(ref) => {
                  this.select = ref;
                }}
                onPress={this.onPressPort}
                value={this.state.unloadingPort}
                items={this.props.portList}
                pickerTitle="请选择卸货港"
                getItemValue={item => item.text}
                getItemText={item => item.text}
                // pickerType="popover"
                onSelected={(item) => {
                  this.setState({ unloadingPort: item.text });
                }}
              />
            </Item>
          </InputGroup>
          <InputGroup style={commonStyles.inputGroup}>
            <InputItem
              label="装船位置:"
              value={this.state.shipLoadPosition || null}
              onChangeText={(text) => {
                this.setState({ shipLoadPosition: text.trim() });
              }}
            />
          </InputGroup>
          <InputGroup style={commonStyles.inputGroup}>
            <InputItem
              label="设置温度:"
              value={this.state.temperature || null}
              onChangeText={(text) => {
                this.setState({ temperature: text });
              }}
              maskType={MaskType.FLOAT}
              canNegative
              rightItem={<Text style={commonStyles.tail}>°C</Text>}
            />
          </InputGroup>
          <InputGroup style={commonStyles.inputGroup}>
            <InputItem
              label="通风开度:"
              value={this.state.ventilationOpening || null}
              onChangeText={(text) => {
                this.setState({ ventilationOpening: text.trim() });
              }}
            />
          </InputGroup>
          <InputGroup style={commonStyles.inputGroup}>
            <Item style={commonStyles.inputItem}>
              <Label style={commonStyles.inputLabel}>箱型:</Label>
              <Selector
                value={this.state.contType}
                items={contTypeList}
                pickerTitle="请选择箱型"
                getItemValue={item => item}
                getItemText={item => item}
                // pickerType="popover"
                onSelected={(item) => {
                  this.setState({ contType: item });
                }}
              />
            </Item>
          </InputGroup>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  portName: makeSelectPortName(),
  shipName: makeSelectShipName(),
  popId: makeSelectPopId(),
  isEdit: makeIsEdit(),
  data: makeData(),
  onGoBack: makeOnoBack(),
  portList: makePortList(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      createRefrigeratorRecord: createRefrigeratorRecordPromise,
      updateRefrigeratorRecord: updateRefrigeratorRecordPromise,
      getNextPortList: getNextPortListPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EditRefrigeratorRecordRecord);
