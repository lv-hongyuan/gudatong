import {
  createRefrigeratorRecordRoutine,
  updateRefrigeratorRecordRoutine,
} from './actions';

const initState = {
  parentData: [],
  childData: [],
};

export default function (state = initState, action) {
  switch (action.type) {
    // 创建加油申请
    case createRefrigeratorRecordRoutine.TRIGGER:
      return { ...state, loading: true };
    case createRefrigeratorRecordRoutine.SUCCESS:
      return state;
    case createRefrigeratorRecordRoutine.FAILURE:
      return { ...state, error: action.payload };
    case createRefrigeratorRecordRoutine.FULFILL:
      return { ...state, loading: false };

    // 编辑加油申请
    case updateRefrigeratorRecordRoutine.TRIGGER:
      return { ...state, loading: true };
    case updateRefrigeratorRecordRoutine.SUCCESS:
      return state;
    case updateRefrigeratorRecordRoutine.FAILURE:
      return { ...state, error: action.payload };
    case updateRefrigeratorRecordRoutine.FULFILL:
      return { ...state, loading: false };

    default:
      return state;
  }
}
