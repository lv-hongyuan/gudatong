import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../ErrorHandler/actions';
import { getShipTugRewardByIdRoutine, getSaveShipTugRewardRoutine, getSubmitShipTugRewardRoutine, getConfirmSaveShipTugRewardRoutine, refreshDataRoutine, emptyDataRoutine } from './actions';

function* getShipTugRewardById(action) {
  console.log(action);
  try {
    yield put(getShipTugRewardByIdRoutine.request());
    // console.log('!!!!!',action.payload)
    const response = yield call(request, ApiFactory.getShipTugRewardById(action.payload));
    // console.log('请求数据成功+++',JSON.stringify(response));
    yield put(getShipTugRewardByIdRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(getShipTugRewardByIdRoutine.failure(e));
  } finally {
    yield put(getShipTugRewardByIdRoutine.fulfill());
  }
}

export function* getShipTugRewardByIdSaga() {
  yield takeLatest(getShipTugRewardByIdRoutine.TRIGGER, getShipTugRewardById);
}

function* getSaveShipTugReward(action) {
  console.log(action);
  try {
    yield put(getSaveShipTugRewardRoutine.request());
    console.log('暂存发送+++',action.payload)
    const response = yield call(request, ApiFactory.getSaveShipTugReward(action.payload));
    console.log('暂存成功+++',JSON.stringify(response));
    yield put(getSaveShipTugRewardRoutine.success(response));
  } catch (e) {
    console.log('11111111+',e.code);
    if (e.code == 802){

    } else{
      yield put(errorMessage(e.message));
    }
    yield put(getSaveShipTugRewardRoutine.failure(e));
  } finally {
    yield put(getSaveShipTugRewardRoutine.fulfill());
  }
}

export function* getSaveShipTugRewardSaga() {
  yield takeLatest(getSaveShipTugRewardRoutine.TRIGGER, getSaveShipTugReward);
}

function* getSubmitShipTugReward(action) {
  console.log(action);
  try {
    yield put(getSubmitShipTugRewardRoutine.request());
    console.log('提交发送+++',action.payload)
    const response = yield call(request, ApiFactory.getSubmitShipTugReward(action.payload));
    console.log('提交成功+++',JSON.stringify(response));
    yield put(getSubmitShipTugRewardRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(getSubmitShipTugRewardRoutine.failure(e));
  } finally {
    yield put(getSubmitShipTugRewardRoutine.fulfill());
  }
}

export function* getSubmitShipTugRewardSaga() {
  yield takeLatest(getSubmitShipTugRewardRoutine.TRIGGER, getSubmitShipTugReward);
}

function* getConfirmSaveShipTugReward(action) {
  console.log(action);
  try {
    yield put(getConfirmSaveShipTugRewardRoutine.request());
    console.log('暂存确认发送+++',action.payload)
    const response = yield call(request, ApiFactory.getConfirmSaveShipTugReward(action.payload));
    console.log('暂存确认成功+++',JSON.stringify(response));
    yield put(getConfirmSaveShipTugRewardRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(getConfirmSaveShipTugRewardRoutine.failure(e));
  } finally {
    yield put(getConfirmSaveShipTugRewardRoutine.fulfill());
  }
}

export function* getConfirmSaveShipTugRewardSaga() {
  yield takeLatest(getConfirmSaveShipTugRewardRoutine.TRIGGER, getConfirmSaveShipTugReward);
}

function* refreshData(action) {
  console.log(action);
  try {
    yield put(refreshDataRoutine.TRIGGER());
  } catch (e) {

  }
}

export function* refreshDataSaga() {
  // yield takeLatest(refreshDataRoutine.TRIGGER, refreshData);
}

function* emptyData(action) {
  console.log(action);
  try {
    yield put(emptyDataRoutine.TRIGGER());
  } catch (e) {

  }
}

export function* emptyDataSaga() {
  // yield takeLatest(refreshDataRoutine.TRIGGER, emptyData);
}

// All sagas to be loaded
export default [getShipTugRewardByIdSaga, getSaveShipTugRewardSaga, getSubmitShipTugRewardSaga, getConfirmSaveShipTugRewardSaga, refreshDataSaga, emptyDataSaga ];
