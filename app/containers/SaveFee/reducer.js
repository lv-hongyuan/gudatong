import {
    getShipTugRewardByIdRoutine,
    getSaveShipTugRewardRoutine,
    getSubmitShipTugRewardRoutine,
    getConfirmSaveShipTugRewardRoutine,
    refreshDataRoutine,
    emptyDataRoutine
} from './actions';
import fixIdList from '../../utils/fixIdList';
import {RefreshState} from '../../components/RefreshListView';

const initState = {
    // loading: false,
    list: [],
    refreshState: RefreshState.Idle,
    data: [],
};

export default function (state = initState, action) {
    switch (action.type) {
        case getShipTugRewardByIdRoutine.TRIGGER:
            return {...state, loading: true, refreshState: RefreshState.HeaderRefreshing};
        case getShipTugRewardByIdRoutine.SUCCESS:
            return {
                ...state,
                list: action.payload.detailList,
                data: action.payload,
                refreshState: RefreshState.NoMoreData
            };
        case getShipTugRewardByIdRoutine.FAILURE:
            return {
                ...state, list: [], error: action.payload, refreshState: RefreshState.Failure,
            };
        case getShipTugRewardByIdRoutine.FULFILL:
            return {...state, loading: false};

        case getSaveShipTugRewardRoutine.TRIGGER:
            return {...state, loading: true};
        case getSaveShipTugRewardRoutine.SUCCESS:
            return {...state};
        case getSaveShipTugRewardRoutine.FAILURE:
            return {...state, error: action.payload};
        case getSaveShipTugRewardRoutine.FULFILL:
            return {...state, loading: false};

        case getSubmitShipTugRewardRoutine.TRIGGER:
            return {...state, loading: true};
        case getSubmitShipTugRewardRoutine.SUCCESS:
            return {...state};
        case getSubmitShipTugRewardRoutine.FAILURE:
            return {...state, error: action.payload};
        case getSubmitShipTugRewardRoutine.FULFILL:
            return {...state, loading: false};

        case getConfirmSaveShipTugRewardRoutine.TRIGGER:
            return {...state, loading: true};
        case getConfirmSaveShipTugRewardRoutine.SUCCESS:
            return {...state};
        case getConfirmSaveShipTugRewardRoutine.FAILURE:
            return {...state, error: action.payload};
        case getConfirmSaveShipTugRewardRoutine.FULFILL:
            return {...state, loading: false};

        case refreshDataRoutine.TRIGGER:
            console.log('action.payload+++', action.payload);
            return {...state, list: action.payload.detailList, data: action.payload};

        case emptyDataRoutine.TRIGGER:
            return {list: []};

        default:
            return state;
    }
}
