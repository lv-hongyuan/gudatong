import { createSelector } from 'reselect/es';

const selectSaveFeeDomain = () => state => state.saveFee;

const makeList = () => createSelector(selectSaveFeeDomain(), (subState) => {
  console.debug(subState);
  return subState.list;
});

const makeData = () => createSelector(selectSaveFeeDomain(), (subState) => {
  console.debug(subState);
  return subState.data;
});

const makeRefreshState = () => createSelector(selectSaveFeeDomain(), (subState) => {
  console.debug(subState.refreshState);
  return subState.refreshState;
});

export { makeList, makeRefreshState, makeData };
