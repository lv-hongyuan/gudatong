/*
 *
 * SaveFee constants
 *
 */
export const GET_SHIP_TUG_REWARD_BY_ID = 'app/SaveFee/GET_SHIP_TUG_REWARD_BY_ID';
export const GET_SAVE_SHIP_TUG_REWARD = 'app/SaveFee/GET_SAVE_SHIP_TUG_REWARD';
export const GET_SUBMIT_SHIP_TUG_REWARD = 'app/SaveFee/GET_SUBMIT_SHIP_TUG_REWARD';
export const GET_CONFIRM_SAVE_SHIP_TUG_REWARD = 'app/SaveFee/GET_CONFIRM_SAVE_SHIP_TUG_REWARD';
export const REFRESH_DATA = 'app/SaveFee/REFRESH_DATA';
export const EMPTY_DATA = 'app/SaveFee/EMPTY_DATA';
