import React from 'react';
import PropTypes from 'prop-types';
import { SafeAreaView, Animated, DeviceEventEmitter,StatusBar } from 'react-native';
import { connect } from 'react-redux';
import {
    Container, InputGroup,
    Text,
    View,
} from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import {
    getShipTugRewardByIdPromise,
    getSaveShipTugRewardPromise,
    getSubmitShipTugRewardPromise,
    getConfirmSaveShipTugRewardPromise,
    refreshDataPromise,
    emptyDataPromise
} from './actions';
import { ROUTE_SAVE_FEE_ALGORITHM, ROUTE_SAVE_FEE_QUESTION } from '../../RouteConstant';
import { makeList, makeRefreshState, makeData } from './selectors';
import myTheme from '../../Themes';
import screenHOC, { Orientations } from '../listScreenHOC';
import SaveFeeItem from './components/SaveFeeItem';
import RefreshListView from '../../components/RefreshListView';
import HeaderButtons from "react-navigation-header-buttons";
import { createPilotApplyAction, updatePilotApplyAction } from "../CreatePilotApply/actions";
import AlertView from "../../components/Alert";
import Svg from "../../components/Svg";
import { NavigationActions } from "react-navigation";
import commonStyles from "../../common/commonStyles";
import SelectDate from "../SaveFeeList/components/SelectDate";
import SelectItem from './components/SelectItem';
import Orientation from 'react-native-orientation';


const styles = {
    container: {
        backgroundColor: 'white',
        flexDirection: 'row',
        // alignContent: 'stretch',
    },
    col: {
        backgroundColor: 'white',
        width: 80,
        height: 40,
        paddingLeft: 0,
        paddingRight: 0,
        paddingTop: 0,
        paddingBottom: 0,
        borderRightWidth: myTheme.borderWidth,
        borderBottomWidth: myTheme.borderWidth,
        borderColor: myTheme.borderColor,
        flexDirection: 'row',
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
    },
    touchCol: {
        backgroundColor: 'white',
        width: 80,
    },
    touchContent: {
        backgroundColor: 'white',
        paddingLeft: 0,
        paddingRight: 0,
        paddingTop: 0,
        paddingBottom: 0,
        borderRightWidth: myTheme.borderWidth,
        borderBottomWidth: myTheme.borderWidth,
        borderColor: myTheme.borderColor,
        flexDirection: 'row',
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
    },
    text: {
        color: myTheme.inputColor,
        textAlign: 'center',
        flex: 1,
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 10,
        paddingBottom: 10,
    },
    subText: {
        color: myTheme.inputColor,
        textAlign: 'center',
        flex: 1,
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 0,
        paddingBottom: 0,
    },
    footView: {
        height: 30,
        // backgroundColor: 'red',
        // width: 1000
        marginBottom: 0,
        flexDirection: 'row',
        borderTopWidth: myTheme.borderWidth,
        borderColor: myTheme.borderColor,
        backgroundColor: 'white',
    },
    textView: {
        marginTop: 0,
        marginBottom: 0,
        // backgroundColor: 'green',
        alignItems: 'center',
        borderWidth: myTheme.borderWidth,
        borderColor: myTheme.borderColor,
        justifyContent: 'center',
        alignContent: 'center',
        textAlign: 'center',
    },
    titleText: {
        padding: 0,
        textAlign: 'center',
        color: myTheme.inputColor,
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
    },
    moneyText: {
        width: 80,
        color: myTheme.inputColor,
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
    },
};

@screenHOC
class SaveFee extends React.PureComponent {
    constructor(props) {
        super(props);
        this.id = this.props.navigation.state.params.id,
            this.feemon = this.props.navigation.state.params.feemon
        this.restart = 0
        this.opState = 0
        this.state = {
            startTime: null,
            endTime: null,
            containerOffSet: new Animated.Value(0),
            recordW: 0,
            recordH: 0,
            getData: '',
            shipCaptain: null,
            payable: '',
            actualPayment: '',
            // opState: 0,
        };
    }


    componentDidMount() {
        this.listView.beginRefresh();
        this.props.navigation.setParams({ submit: this.handleSubmit });
        this.props.navigation.setParams({ saveReward: this.handleSaveReward });
        this.props.navigation.setParams({ reStart: this.restartRequest });
        this.listener = DeviceEventEmitter.addListener('setData', (param) => {
            console.log('$$$$$', param.data);
            this.setState({
                getData: param.data,
                payable: param.data.payable,
                actualPayment: param.data.actualPayment,
            })
            this.props.refreshDataPromise(param.data)
        });
        // this.setState({
        //   payable: this.state.payable,
        //   actualPayment: this.state.actualPayment,
        // })

    }



    onHeaderRefresh = () => {
        this.loadList(false);
    };

    onFooterRefresh = () => {
        this.loadList(true);
    };

    //根View的onLayout回调函数
    onLayout = (event) => {
        //获取根View的宽高，以及左上角的坐标值
        let { x, y, width, height } = event.nativeEvent.layout;
        this.setState({
            recordW: width,
            recordH: height,
        });
    }

    get translateX() {
        const x = 0;
        return this.state.containerOffSet.interpolate({
            inputRange: [-1, 0, x, x + 1],
            outputRange: [0, 0, 0, 1],
        });
    }

    supportedOrientations = Orientations.LANDSCAPE;

    loadList() {
        // console.log('@@@@@@@', this.props.navigation.state.params.id)
        this.props.getShipTugRewardByIdPromise({
            id: this.id,
            feemon: this.feemon,
            restart: this.restart
        }).then((data) => {
            console.log('console.data+++', this.props.data)
            if (data._backcode == '811') {
                AlertView.show({
                    message: data._backmes, confirmAction: () => {
                        this.props.navigation.goBack();
                    }
                })
                return
            } else if (data._backcode == '813') {
                AlertView.show({ message: data._backmes })
            }
            this.feemon = data.feemon
            this.id = data.id
            this.opState = data.opState
            this.props.navigation.setParams({ opState: data.opState });
            // this.setState({
            //     getData: this.props.data,
            //     payable: this.props.data.payable,
            //     actualPayment: this.props.data.actualPayment,
            //     shipCaptain: this.props.data.captain,
            //     opState:this.props.data.opState,
            // })
            this.setState({
                getData: data,
                payable: data.payable,
                actualPayment: data.actualPayment,
                shipCaptain: data.captain,
                // opState: data.opState,
            })
        })
            .catch((e) => {

            });
    }

    componentWillUnmount() {
        this.listener.remove();
        this.props.emptyDataPromise();
        Orientation.lockToPortrait();
    }

    //提交和暂存的弹出框
    showAlertViewWithSubmit(isSubmit) {
        if (isSubmit) {
            AlertView.show({
                message: '确定提交?', showCancel: true, confirmAction: () => {
                    // if (this.state.shipCaptain == '') {
                    //     AlertView.show({ message: '请填写船长' })
                    // } else {
                        this.props.getSubmitShipTugRewardPromise(this.state.getData)
                            .then((data) => {
                                // console.log('data===', data)
                                if (data._backcode == '200') {
                                    DeviceEventEmitter.emit('submitSuccess')
                                    this.props.navigation.dispatch(NavigationActions.back());
                                } else {
                                    AlertView.show({ message: data._backmes })
                                }
                            })
                            .catch(() => {
                            });
                    // }
                    }
            })
        } else {
            AlertView.show({
                message: '确定暂存?', showCancel: true, confirmAction: () => {
                    this.props.getSaveShipTugRewardPromise(this.state.getData)
                        .then((data) => {
                            // console.log('data===',data);
                            if (data._backcode == '200') {
                                DeviceEventEmitter.emit('submitSuccess')
                                this.props.navigation.dispatch(NavigationActions.back());
                            } else if (data._backcode == '812') {
                                AlertView.show({
                                    message: data._backmes,
                                    showCancel: true,
                                    confirmAction: () => {
                                        console.log('操作')
                                        this.props.getConfirmSaveShipTugRewardPromise(this.state.getData)
                                            .then(() => {
                                                DeviceEventEmitter.emit('submitSuccess')
                                                this.props.navigation.dispatch(NavigationActions.back());
                                            })
                                            .catch(() => {
                                            });
                                    }
                                })
                            } else {
                                AlertView.show({ message: data._backmes })
                            }

                        })
                        .catch((e) => {
                            //如果返回802点击取消就没有操作，点击确定就是确认暂存
                            // if (e.code == 802) {
                            //     AlertView.show({
                            //         message: e.message,
                            //         showCancel: true,
                            //         confirmAction: () => {
                            //             console.log('操作')
                            //             this.props.getConfirmSaveShipTugRewardPromise(this.state.getData)
                            //                 .then(() => {
                            //                     DeviceEventEmitter.emit('submitSuccess')
                            //                     this.props.navigation.dispatch(NavigationActions.back());
                            //                 })
                            //                 .catch(() => {
                            //                 });
                            //         }
                            //     })
                            // } else {
                            //     console.log('暂存成功啦+++')
                            //     DeviceEventEmitter.emit('submitSuccess')
                            //     this.props.navigation.dispatch(NavigationActions.back());
                            // }
                        })
                }
            })
        }
    }

    //重新生成
    restartRequest = () => {
        AlertView.show({
            message: '确定要重新生成吗?', showCancel: true, confirmAction: () => {
                this.restart = 1
                this.opState = 0
                if(this.props.list && this.props.list.length> 0){
                    this.listViewRef._flatListRef.scrollToIndex({ animated: false, index: 0, viewPosition: 0 })
                }
                this.listView.beginRefresh()
            }
        })
    }

    //提交
    handleSubmit = () => {
        if (!this.state.getData.detailList || this.state.getData.detailList.length == 0) {
            AlertView.show({ message: '没有需要提交的数据' });
            return
        }
        AlertView.show({
            message: '船长',
            component: () => (

                <SelectItem getData={this.state.getData} captainEndEditing={this.captainEndEditing} />
            ),
            showCancel: true,
            confirmAction: () => {
                if (this.state.shipCaptain == '') {
                    AlertView.show({ message: '请填写船长' })
                    return
                } 
                this.showAlertViewWithSubmit(true)
            },
        });

    };


    //暂存
    handleSaveReward = () => {
        if (!this.state.getData.detailList || this.state.getData.detailList.length == 0) {
            AlertView.show({ message: '没有需要暂存的数据' });
            return
        }
        AlertView.show({
            message: '船长',
            component: () => (
                <SelectItem getData={this.state.getData} captainEndEditing={this.captainEndEditing} />
            ),
            showCancel: true,
            confirmAction: () => {
                console.log('船长是谁??', this.state.shipCaptain)
                console.log('船长名字++', this.state.shipCaptain)
                console.log('船长data++', this.state.getData)
                this.showAlertViewWithSubmit(false)
            },
        });
    };

    // Alert框更改的时候调用
    captainEndEditing = (param) => {
        console.log('captainEndEditing+++', param, '---', this.state.getData);
        const data = this.state.getData;
        data.captain = param;
        this.setState({
            getData: data,
            shipCaptain: param
        })
    }
    shouldComponentUpdate(nextProps, nextState) {
        if (nextState.shipCaptain !== this.state.shipCaptain) {
            console.log('打印next.item');
            return true;
        }
        if (nextState.getData !== this.state.getData) {
            console.log('打印next.item');
            return true;
        }
        if (nextProps.refreshState !== this.props.refreshState) {
            console.log('thisP:', this.props.refreshState)
            console.log('nextP:', nextProps.refreshState)
            return true;
        }
        return false;
    }

    renderItem = ({ item, index }) => (
        <SaveFeeItem
            item={item}
            index={index}
            headerTranslateX={this.translateX}
            opState={this.opState}
        />
    );

    render() {
        console.log('||||||||', this.props.list)
        return (
            <Container theme={myTheme} style={{ backgroundColor: '#E0E0E0' }}>
                <StatusBar hidden={true}/>
                <SafeAreaView style={{
                    backgroundColor: '#E0E0E0',
                    flex: 1,
                }}
                >
                    <View
                        style={{
                            width: '100%',
                            flex: 1,
                        }}
                        onLayout={(event) => {
                            const { width } = event.nativeEvent.layout;
                            this.setState({ containerWidth: width });
                        }}
                    >
                        <Animated.ScrollView
                            onScroll={Animated.event([{
                                nativeEvent: { contentOffset: { x: this.state.containerOffSet } },
                            }], { useNativeDriver: true })}
                            scrollEventThrottle={1}
                            horizontal
                        >
                            <View style={{
                                flex: 1,
                                minWidth: '100%',
                            }}
                            >
                                <View style={styles.container}>
                                    <View style={[styles.col, { width: 100 }]}>
                                        <Text style={styles.text}>时间</Text>
                                    </View>
                                    <View style={styles.col}>
                                        <Text style={styles.text}>港口</Text>
                                    </View>
                                    <View style={styles.col}>
                                        <Text style={styles.text}>航次</Text>
                                    </View>
                                    <View style={[styles.col, {
                                        flexDirection: 'column',
                                        width: 240,
                                    }]}
                                    >
                                        <View style={[styles.col, {
                                            width: 240,
                                            flex: 1,
                                        }]}
                                        >
                                            <Text style={styles.subText}>航次拖轮使用情况</Text>
                                        </View>
                                        <View style={{
                                            flexDirection: 'row',
                                            flex: 1,
                                        }}
                                        >
                                            <View style={[styles.col, {
                                                width: 80,
                                                height: '100%',
                                                borderBottomWidth: 0,
                                            }]}
                                            >
                                                <Text style={styles.subText}>靠/离</Text>
                                            </View>
                                            <View style={[styles.col, {
                                                width: 80,
                                                height: '100%',
                                                borderBottomWidth: 0,
                                            }]}
                                            >
                                                <Text style={styles.subText}>节省数</Text>
                                            </View>
                                            <View style={[styles.col, {
                                                width: 80,
                                                height: '100%',
                                                borderBottomWidth: 0,
                                            }]}
                                            >
                                                <Text style={styles.subText}>金额(元)</Text>
                                            </View>
                                        </View>
                                    </View>
                                    {/*<View style={[styles.col, { width: 90 }]}>*/}
                                    {/*  <Text style={styles.text}>订舱重量</Text>*/}
                                    {/*</View>*/}
                                    <View style={[styles.col, {
                                        flexDirection: 'column',
                                        width: 260,
                                    }]}
                                    >
                                        <View style={[styles.col, {
                                            width: 260,
                                            flex: 1,
                                        }]}
                                        >
                                            <Text style={styles.subText}>航次引水使用情况</Text>
                                        </View>
                                        <View style={{
                                            flexDirection: 'row',
                                            flex: 1,
                                        }}
                                        >
                                            <View style={[styles.col, {
                                                width: 100,
                                                height: '100%',
                                                borderBottomWidth: 0,
                                            }]}
                                            >
                                                <Text style={styles.subText}>靠/离</Text>
                                            </View>
                                            <View style={[styles.col, {
                                                width: 80,
                                                height: '100%',
                                                borderBottomWidth: 0,
                                            }]}
                                            >
                                                <Text style={styles.subText}>节省数</Text>
                                            </View>
                                            <View style={[styles.col, {
                                                width: 80,
                                                height: '100%',
                                                borderBottomWidth: 0,
                                            }]}
                                            >
                                                <Text style={styles.subText}>金额(元)</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={[styles.col]}>
                                        <Text style={styles.text}>合计</Text>
                                    </View>
                                    <View style={[styles.col]}>
                                        <Text style={styles.text}>修正</Text>
                                    </View>
                                    <View style={[styles.col, { width: 200 }]}>
                                        <Text style={styles.text}>船上备注</Text>
                                    </View>
                                    <View style={[styles.col, { width: 200 }]}>
                                        <Text style={styles.text}>现场备注</Text>
                                    </View>
                                    <View style={[styles.col, { width: 200 }]}>
                                        <Text style={styles.text}>航运部备注</Text>
                                    </View>
                                    <Animated.View style={{
                                        position: 'absolute',
                                        top: 0,
                                        left: 0,
                                        bottom: 0,
                                        transform: [{ translateX: this.translateX }],
                                    }}
                                    >
                                        <View style={styles.container}>
                                            <View style={[styles.col, { width: 100 }]}>
                                                <Text style={styles.text}>时间</Text>
                                            </View>
                                            <View style={[styles.col]}>
                                                <Text style={styles.text}>港口</Text>
                                            </View>
                                            <View style={[styles.col]}>
                                                <Text style={styles.text}>航次</Text>
                                            </View>
                                        </View>
                                    </Animated.View>
                                </View>
                                <RefreshListView
                                    ref={(ref) => {
                                        this.listView = ref;
                                    }}
                                    listRef={(ref) => { this.listViewRef = ref; }}
                                    headerTranslateX={this.translateX}
                                    containerWidth={this.state.containerWidth}
                                    data={this.props.list || []}
                                    style={{ flex: 1 }}
                                    keyExtractor={(item, index) => `${index}`}
                                    renderItem={this.renderItem}
                                    refreshState={this.props.refreshState}
                                    onHeaderRefresh={this.onHeaderRefresh}
                                // onFooterRefresh={this.onFooterRefresh}
                                />
                            </View>
                        </Animated.ScrollView>
                        <View style={styles.footView}>
                            <View style={[styles.textView, { width: 100 }]}>
                                <Text style={styles.titleText}>总计</Text>
                            </View>
                            <View style={[styles.textView, { width: 80 }]}>
                                <Text style={styles.titleText}>应付</Text>
                            </View>
                            <View style={[styles.textView, { width: 160 }]}>
                                <Text style={styles.moneyText}>{this.state.payable}</Text>
                            </View>
                            <View style={[styles.textView, { width: 80 }]}>
                                <Text style={styles.titleText}>实付</Text>
                            </View>
                            <View style={[styles.textView, { width: 180 }]}>
                                <Text style={styles.moneyText}>{this.state.actualPayment}</Text>
                            </View>
                        </View>
                    </View>
                </SafeAreaView>
            </Container>
        );
    }
}

SaveFee.navigationOptions = ({ navigation }) => ({
    headerStyle: {
        backgroundColor: '#DC001B',
        height:40,
    },
    headerTitle: (
        <View style={{
            flex: 1,
            textAlign: 'center',
            justifyContent: 'center',
            marginTop: 0,
            marginBottom: 0,
            alignItems: 'center',
            alignContent: 'center',
        }}>
            <HeaderButtons>
                <HeaderButtons.Item
                    title="节拖奖励"
                    buttonStyle={{ fontSize: 17, fontWeight: '700', color: '#ffffff' }}
                />
                <HeaderButtons.Item
                    title=""
                    buttonWrapperStyle={{ padding: 5 }}
                    ButtonElement={<Svg icon="questionMark" size="20" color="white" style={{ padding: 5 }} />}
                    onPress={() => {
                        // navigation.push(ROUTE_SAVE_FEE_ALGORITHM);
                        navigation.push(ROUTE_SAVE_FEE_QUESTION);
                    }}
                />
            </HeaderButtons>
        </View>
    ),
    headerRight: (navigation.getParam('opState') == 0 || navigation.getParam('opState') == 10 || navigation.getParam('opState') == 70) ? (
        <HeaderButtons>
            <HeaderButtons.Item
                title="重新生成"
                buttonStyle={{ fontSize: 14, color: '#ffffff' }}
                onPress={() => {
                    const restartRequest = navigation.getParam('reStart', undefined);
                    if (restartRequest) {
                        // requestAnimationFrame(() => {
                        restartRequest();
                        // });
                    }
                }}
            />
            <HeaderButtons.Item
                title="提交"
                buttonStyle={{ fontSize: 14, color: '#ffffff' }}
                onPress={() => {
                    const handleSubmitFunc = navigation.getParam('submit', undefined);
                    if (handleSubmitFunc) {
                        requestAnimationFrame(() => {
                            handleSubmitFunc();
                        });
                    }
                }}
            />
            <HeaderButtons.Item
                title="暂存"
                buttonStyle={{ fontSize: 14, color: '#ffffff' }}
                onPress={() => {
                    const handleSubmitFunc = navigation.getParam('saveReward', undefined);
                    if (handleSubmitFunc) {
                        requestAnimationFrame(() => {
                            handleSubmitFunc();
                        });
                    }
                }}
            />
        </HeaderButtons>
    ) : null,
});

SaveFee.propTypes = {
    navigation: PropTypes.object.isRequired,
    getShipTugRewardByIdPromise: PropTypes.func.isRequired,
    refreshState: PropTypes.number.isRequired,
    list: PropTypes.array,
    data: PropTypes.array,
    getSaveShipTugRewardPromise: PropTypes.func.isRequired,
    getSubmitShipTugRewardPromise: PropTypes.func.isRequired,
    getConfirmSaveShipTugRewardPromise: PropTypes.func.isRequired,
};

SaveFee.defaultProps = {
    list: [],
    data: [],
};

const mapStateToProps = createStructuredSelector({
    refreshState: makeRefreshState(),
    list: makeList(),
    data: makeData(),
});

function mapDispatchToProps(dispatch) {
    return {
        ...bindPromiseCreators({
            getShipTugRewardByIdPromise,
            getSaveShipTugRewardPromise,
            getSubmitShipTugRewardPromise,
            getConfirmSaveShipTugRewardPromise,
            refreshDataPromise,
            emptyDataPromise,
        }, dispatch),
        dispatch,
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(SaveFee);
