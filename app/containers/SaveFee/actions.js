import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_SHIP_TUG_REWARD_BY_ID,
    GET_SAVE_SHIP_TUG_REWARD,
    GET_SUBMIT_SHIP_TUG_REWARD,
    GET_CONFIRM_SAVE_SHIP_TUG_REWARD,
    REFRESH_DATA,
    EMPTY_DATA,
    RELOAD_DATAl
} from './constants';

export const getShipTugRewardByIdRoutine = createRoutine(GET_SHIP_TUG_REWARD_BY_ID);
export const getShipTugRewardByIdPromise = promisifyRoutine(getShipTugRewardByIdRoutine);

export const getSaveShipTugRewardRoutine = createRoutine(GET_SAVE_SHIP_TUG_REWARD,
    updates => updates,
    () => ({ globalLoading: true })
    );
export const getSaveShipTugRewardPromise = promisifyRoutine(getSaveShipTugRewardRoutine);

export const getSubmitShipTugRewardRoutine = createRoutine(GET_SUBMIT_SHIP_TUG_REWARD,
    updates => updates,
    () => ({ globalLoading: true }),
    );
export const getSubmitShipTugRewardPromise = promisifyRoutine(getSubmitShipTugRewardRoutine);

export const getConfirmSaveShipTugRewardRoutine = createRoutine(GET_CONFIRM_SAVE_SHIP_TUG_REWARD,
    updates => updates,
    () => ({ globalLoading: true }),
);
export const getConfirmSaveShipTugRewardPromise = promisifyRoutine(getConfirmSaveShipTugRewardRoutine);

export const refreshDataRoutine = createRoutine(REFRESH_DATA);
export const refreshDataPromise = promisifyRoutine(refreshDataRoutine)

export const emptyDataRoutine = createRoutine(EMPTY_DATA);
export const emptyDataPromise = promisifyRoutine(emptyDataRoutine)
