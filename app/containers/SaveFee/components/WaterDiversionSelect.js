import React from 'react';
import PropTypes from 'prop-types';
import {DeviceEventEmitter, StyleSheet} from 'react-native';
import {InputGroup} from 'native-base';
import commonStyles from "../../../common/commonStyles";
import DatePullSelector from "../../../components/DatePullSelector";
import {DATE_WHEEL_TYPE} from "../../../components/DateTimeWheel";
import {defaultFormat} from "../../../utils/DateFormatYearMonth";
import InputItem from '../../../components/RewardInputItem';
import {WaterDiversionFromState, WaterDiversionState} from "../../../common/Constant";
import Selector from "../../../components/Selector";


class WaterDiversionSelect extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {waterDiversion: ''};
    }

    componentDidMount() {
        this.setState({
            waterDiversion: this.props.waterDiversion,
        })
    }

    componentWillReceiveProps(nextProps){
        this.setState({waterDiversion:nextProps.waterDiversion})
    }

    //返回页面的回调方法
    waterDiversionWay(id, item, way) {
        console.log('aaaaa++', item, '++id', id, '--', way)
        DeviceEventEmitter.emit('selectItem', {id: id, item: item, way: way})
        // this.props.waterDiversionWay(id,item)
    }

    render() {
        return (
            <Selector
                valueStyle={{flex:1,alignSelf: 'center', textAlign: 'center', padding: 0, color: '#969696'}}
                style={{flex:1 ,borderWidth: 0, height: '100%', paddingLeft: 0, backgroundColor: 'transparent',}}
                value={this.state.waterDiversion}
                items={Object.values(WaterDiversionState)}
                getItemValue={item => item}
                getItemText={item => WaterDiversionFromState(item)}
                onSelected={(item) => {
                    this.setState({waterDiversion: item});
                    this.waterDiversionWay(this.props.id, item, this.props.way);
                }}
                icon={''}
            />
        );
    }
}

WaterDiversionSelect.propTypes = {
    waterDiversionWay: PropTypes.func,
    waterDiversion: PropTypes.string.isRequired,
    id: PropTypes.number,
    way: PropTypes.string.isRequired,
};
WaterDiversionSelect.defaultProps = {
    captainEndEditing: undefined,
};

export default WaterDiversionSelect;
