import React from 'react';
import PropTypes from 'prop-types';
import {StyleSheet} from 'react-native';
import {InputGroup} from 'native-base';
import commonStyles from "../../../common/commonStyles";
import InputItem from '../../../components/RewardInputItem';
import myTheme from "../../../Themes";

const styles = StyleSheet.create({});

class SelectItem extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {shipCaptain: ''};
    }

    //返回页面的回调方法
    captainEndEditing(event) {
        console.log('dasdasdas++', event)
        this.props.captainEndEditing(event)
    }

    render() {
        return (
            <InputGroup style={[commonStyles.inputGroup, {
                marginLeft: 10,
                marginRight: 10,
                width: 160,
                height: 30,
                borderBottomWidth: 1,
                borderColor: myTheme.borderColor,
            }]}>
                <InputItem
                    style={{marginRight: 13}}
                    placeholder={'请输入船长'}
                    maxLength={100}
                    value={this.props.getData.captain}
                    onChangeText={(text) => {
                        this.captainEndEditing(text)
                    }}
                    onBlur={(event)=>{
                        // this.captainEndEditing(event.nativeEvent.text)
                    }}
                />
            </InputGroup>
        );
    }
}

SelectItem.propTypes = {
    captainEndEditing: PropTypes.func,
    getData: PropTypes.object.isRequired,
};
SelectItem.defaultProps = {
    captainEndEditing: undefined,
};

export default SelectItem;
