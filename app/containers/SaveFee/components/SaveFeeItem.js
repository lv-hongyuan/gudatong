/* eslint-disable camelcase */
import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Animated, DeviceEventEmitter } from 'react-native';
import { InputGroup, Text, View } from 'native-base';
import myTheme from '../../../Themes';
import commonStyles from "../../../common/commonStyles";
import InputItem from "../../../components/RewardInputItem";
import HeaderButtons from "react-navigation-header-buttons";
import Selector from '../../../components/Selector';
import { WaterDiversionState, WaterDiversionFromState } from '../../../common/Constant';
import { MaskType } from "../../../components/RewardInputItem/TextInput";
import DiversionSelect from './WaterDiversionSelect';
import { cloneDeep } from 'lodash'
import { connect } from "react-redux";
import { makeData, makeList, makeRefreshState } from "../selectors";
import { createStructuredSelector } from "reselect";

const styles = StyleSheet.create({
    container: {
        alignContent: 'stretch',
        backgroundColor: 'white',
        flexDirection: 'row',
    },
    col: {
        width: 80,
        // paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 5,
        paddingBottom: 5,
        borderRightWidth: myTheme.borderWidth,
        borderBottomWidth: myTheme.borderWidth,
        borderColor: myTheme.borderColor,
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
        textAlign: 'center'
    },
    textCol: {
        width: 80,
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 5,
        paddingBottom: 5,
        borderRightWidth: myTheme.borderWidth,
        borderBottomWidth: myTheme.borderWidth,
        borderColor: myTheme.borderColor,
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
        textAlign: 'center'
    },
    textView: {
        width: 80,
        paddingLeft: 0,
        paddingRight: 0,
        paddingTop: 0,
        paddingBottom: 0,
        borderRightWidth: myTheme.borderWidth,
        // borderBottomWidth: myTheme.borderWidth,
        borderColor: myTheme.borderColor,
    },
    text: {
        color: myTheme.inputColor,
    },
});

class SaveFeeItem extends React.PureComponent {

    constructor(props) {
        super(props);

        this.state = {
            objectData: '',
            voyTargetExist: '',
            TugShip: '',
            revise: this.props.item.revise,
            berthTugShip: this.props.item.berthTugShip,
            leaveTugShip: this.props.item.leaveTugShip,
            berthDivShip : this.props.item.berthDivShip,
            leaveDivShip :this.props.item.leaveDivShip,
            shipNote:this.props.item.shipNote,

        };
    }

    shouldComponentUpdate(nextProps, nextState) {
        // return JSON.stringify(this.props.item) !== JSON.stringify(nextProps.item);
        // console.log(
        //     'nextProps.item', nextProps.item.saveTugNum,
        //     'this.props.item.saveTugNum++', this.props.item.saveTugNum,
        //     'nextProps.item', nextProps.item.saveTugMoney,
        //     'this.props.item.saveTugMoney++', this.props.item.saveTugMoney,
        //     'nextProps.item', nextProps.item.saveDivNum,
        //     'this.props.item.saveDivNum++', this.props.item.saveDivNum,
        //     'nextProps.item', nextProps.item.saveDivMoney,
        //     'this.props.item.saveDivMoney++', this.props.item.saveDivMoney,
        //     'nextProps.item', nextProps.item.amount,
        //     'this.props.item.amount++', this.props.item.amount,
        //     'refreshState', this.props.refreshState,
        //     'nextProps.item', nextProps.item.berthDivShip,
        // );
        if (this.props.item.saveTugNum !== nextProps.item.saveTugNum) {
            console.log('打印next.item');
            return true;
        }
        if (this.props.item.saveTugMoney !== nextProps.item.saveTugMoney) {
            return true;
        }
        if (this.props.item.saveDivNum !== nextProps.item.saveDivNum) {
            return true;
        }
        if (this.props.item.saveDivMoney !== nextProps.item.saveDivMoney) {
            return true;
        }
        if (this.props.item.amount !== nextProps.item.amount) {
            return true;
        }
        if (this.props.item.berthDivShip !== nextProps.item.berthDivShip) {
            return true;
        }
        if (this.props.item.leaveDivShip !== nextProps.item.leaveDivShip) {
            return true;
        }
        if (this.state.revise !== nextState.revise) {
            return true;
        }
        if (this.state.berthTugShip !== nextState.berthTugShip) {
            return true;
        }
        if (this.state.leaveTugShip !== nextState.leaveTugShip) {
            return true;
        }
        if (this.props.opState !== nextProps.opState) {
            return true
        }
        if (this.state.shipNote !== nextState.shipNote) {
            return true;
        }
        
        return false;
        // (this.props.item !== nextProps.item);
    }

    componentWillReceiveProps(nextProps){
        // console.log('@@@@',nextProps)
        this.setState({
            revise: nextProps.item.revise,
            berthTugShip: nextProps.item.berthTugShip,
            leaveTugShip: nextProps.item.leaveTugShip,
            berthDivShip: nextProps.item.berthDivShip,
            leaveDivShip: nextProps.item.leaveDivShip,
            shipNote:nextProps.item.shipNote,

        })
    }

    componentDidMount() {
        //voyTargetExist 奖励方式0后续的节省数，金额合计都不用计算
        this.setState({
            // objectData: this.props.data,
            voyTargetExist: this.props.data.detailList[0].voyTargetExist
        })
        this.listener = DeviceEventEmitter.addListener('selectItem', (param) => {
            console.log('getListener+++', param.item, '++', param.way)
            this.waterDiversion(param.id, param.item, param.way);
        })
    }

    componentWillUnmount() {
        this.listener.remove();
    }

    // TextInput编辑结束把对应的字段替换掉然后发一个通知到主页面
    //靠/离用拖轮数计算方式
    tugShipEndEditing = (index, text, way) => {
        if (text == '') {
            text = 0;
        }
        console.log('tugShipEndEditing+++', index, '---', text)
        console.log('data++', this.props.data.detailList);
        const dataLength = this.props.data.detailList;
        const data = cloneDeep(this.props.data);
        for (let i = 0; i < dataLength.length; i++) {
            //判断到当前index这条
            if (index == i) {
                console.log('靠用', this.state.voyTargetExist)
                //奖励0的方式节省数，金额，合计，计算并数据并赋值
                if (this.state.voyTargetExist == 0) {
                    //节省代码创建当前的nowData
                    const nowData = data.detailList[i]
                    //0：靠 1：离
                    if (way == 0) {
                        nowData.berthTugShip = text;
                    } else {
                        nowData.leaveTugShip = text;
                    }
                    console.log('当前的数据data++', nowData)
                    //拖轮节省数=配备拖轮数-靠用拖轮数-离用拖轮数 如果小于0取0
                    const targetNum = parseFloat(nowData.targetTugNum) - parseFloat(nowData.berthTugShip) - parseFloat(nowData.leaveTugShip)
                    if (targetNum < 0) {
                        nowData.saveTugNum = 0
                    } else {
                        nowData.saveTugNum = parseFloat(nowData.targetTugNum) - parseFloat(nowData.berthTugShip) - parseFloat(nowData.leaveTugShip)
                    }
                    //拖轮金额=拖轮节省数*节省拖轮奖励（元/次）
                    nowData.saveTugMoney = (parseFloat(nowData.saveTugNum)) * (parseFloat(nowData.targetTugMoney))
                    //合计=拖轮金额+引水金额
                    nowData.amount = parseFloat(nowData.saveTugMoney) + parseFloat(nowData.saveDivMoney)
                    //应付=所有合计加一起
                    let allPayable = 0;
                    for (let k = 0; k < dataLength.length; k++) {
                        allPayable = parseFloat(data.detailList[k].amount) + parseFloat(allPayable)
                    }
                    data.payable = allPayable;
                    //实付=应付+修正总和
                    let allActualPayment = 0;
                    for (let j = 0; j < dataLength.length; j++) {
                        allActualPayment = parseFloat(data.detailList[j].revise) + parseFloat(allActualPayment)
                    }
                    const actualPayment = allActualPayment + allPayable
                    data.actualPayment = actualPayment;

                    console.log('$$$', data)
                    this.setState({
                        //objectData: data,
                    })
                    console.log('objectData+++', this.props.data)
                    DeviceEventEmitter.emit('setData', { data: data })
                } else {
                    //节省代码创建当前的nowData
                    const nowData = data.detailList[i]
                    //0：靠 1：离
                    if (way == 0) {
                        nowData.berthTugShip = text;
                    } else {
                        nowData.leaveTugShip = text;
                    }
                    this.setState({
                        //objectData: data,
                    })
                    console.log('objectData+++', this.props.data)
                    DeviceEventEmitter.emit('setData', { data: data })
                }
            }
        }
    }

    //修正
    reviseEndEditing = (index, text) => {
        if (text == '') {
            text = 0;
        }
        const dataLength = this.props.data.detailList;
        const data = cloneDeep(this.props.data);
        for (let i = 0; i < dataLength.length; i++) {
            //判断到当前index这条
            if (index == i) {
                data.detailList[i].revise = text;
                //应付=所有合计加一起
                let allPayable = 0;
                for (let k = 0; k < dataLength.length; k++) {
                    allPayable = parseFloat(data.detailList[k].amount) + parseFloat(allPayable)
                }
                data.payable = allPayable;
                console.log('allPayable', allPayable)
                //实付=应付+修正总和
                let allActualPayment = 0;
                for (let j = 0; j < dataLength.length; j++) {
                    allActualPayment = parseFloat(data.detailList[j].revise) + parseFloat(allActualPayment)
                    console.log('data.detailList[j].revise', data.detailList[j].revise)
                }
                console.log('allActualPayment', allActualPayment)
                let actualPayment = 0;
                actualPayment = parseFloat(allActualPayment) + parseFloat(allPayable)
                //实付总和
                data.actualPayment = actualPayment;
                this.setState({
                    // objectData: data,
                    revise: text,
                })
                console.log('%%%%!', data);
                DeviceEventEmitter.emit('setData', { data: data })
            }
        }
    }

    //引水靠离点击方法
    waterDiversion(index, item, way) {
        console.log('waterDiversion1+++', index, '---', item, '--', way)
        console.log('data++', this.props.data);
        const dataLength = this.props.data.detailList;
        const data = cloneDeep(this.props.data);
        console.log('data.saveDivNum', data.saveDivNum);
        for (let i = 0; i < dataLength.length; i++) {
            //判断到当前index这条
            if (index == i) {
                console.log('靠用', this.state.voyTargetExist)
                //奖励0的方式节省数，金额，合计，计算并数据并赋值
                if (this.state.voyTargetExist == 0) {
                    //节省代码创建当前的nowData
                    const nowData = data.detailList[i]
                    //0：靠 1：离
                    if (way == 'berth') {
                        //节省次数=总次数2-item(靠泊次数0或者1)-离泊次数  自行:0 使用引水:1
                        //靠泊情况得到离泊的次数然后再减去靠泊的次数
                        if (nowData.leaveDivShip == '船舶自行') {
                            nowData.saveDivNum = 2 - parseFloat(item) - 0
                            //离泊是0表示船舶自行else引水
                            if (item == 0) {
                                nowData.berthDivShip = '船舶自行'
                            } else {
                                nowData.berthDivShip = '使用引水'
                            }
                            console.log('靠泊节省数++', nowData.saveDivNum)
                        } else {
                            nowData.saveDivNum = 2 - parseFloat(item) - 1
                            if (item == 0) {
                                nowData.berthDivShip = '船舶自行'
                            } else {
                                nowData.berthDivShip = '使用引水'
                            }
                            console.log('离泊节省数++', nowData.saveDivNum)
                        }
                    } else {
                        if (nowData.berthDivShip == '船舶自行') {
                            nowData.saveDivNum = 2 - parseFloat(item) - 0
                            if (item == 0) {
                                nowData.leaveDivShip = '船舶自行'
                            } else {
                                nowData.leaveDivShip = '使用引水'
                            }
                        } else {
                            nowData.saveDivNum = 2 - parseFloat(item) - 1
                            if (item == 0) {
                                nowData.leaveDivShip = '船舶自行'
                            } else {
                                nowData.leaveDivShip = '使用引水'
                            }
                        }
                    }

                    //引水奖励金额=(总共2次-靠泊次数-离泊次数)*奖励金额/2  因为奖励金额是每2次的
                    nowData.saveDivMoney = (nowData.saveDivNum) * parseFloat(nowData.targetDivMoney) / 2
                    //合计=拖轮金额+引水金额
                    nowData.amount = parseFloat(nowData.saveTugMoney) + parseFloat(nowData.saveDivMoney)
                    //应付=所有合计加一起
                    let allPayable = 0;
                    for (let k = 0; k < dataLength.length; k++) {
                        allPayable = parseFloat(data.detailList[k].amount) + parseFloat(allPayable)
                    }
                    data.payable = allPayable;
                    console.log('allPayable', allPayable)
                    //实付=应付+修正总和
                    let allActualPayment = 0;
                    for (let j = 0; j < dataLength.length; j++) {
                        allActualPayment = parseFloat(data.detailList[j].revise) + parseFloat(allActualPayment)
                        console.log('data.detailList[j].revise', data.detailList[j].revise)
                    }
                    console.log('allActualPayment', allActualPayment)
                    let actualPayment = 0;
                    actualPayment = parseFloat(allActualPayment) + parseFloat(allPayable)
                    //实付总和
                    data.actualPayment = actualPayment;
                    console.log('$$$++', data.actualPayment)
                    this.setState({
                        //objectData: data,
                    })
                    console.log('objectData+++', data)
                    DeviceEventEmitter.emit('setData', { data: data })
                } else {
                    //节省代码创建当前的nowData
                    const nowData = data.detailList[i]
                    //0：靠 1：离
                    if (way == 'berth') {
                        //节省次数=总次数2-item(靠泊次数0或者1)-离泊次数  自行:0 使用引水:1
                        //靠泊情况得到离泊的次数然后再减去靠泊的次数
                        if (nowData.leaveDivShip == '船舶自行') {
                            nowData.saveDivNum = 2 - parseFloat(item) - 0
                            //离泊是0表示船舶自行else引水
                            if (item == 0) {
                                nowData.berthDivShip = '船舶自行'
                            } else {
                                nowData.berthDivShip = '使用引水'
                            }
                        } else {
                            nowData.saveDivNum = 2 - parseFloat(item) - 1
                            if (item == 0) {
                                nowData.berthDivShip = '船舶自行'
                            } else {
                                nowData.berthDivShip = '使用引水'
                            }
                            console.log('saveDivNum++', nowData.saveDivNum)
                        }
                    } else {
                        if (nowData.berthDivShip == '船舶自行') {
                            nowData.saveDivNum = 2 - parseFloat(item) - 0
                            if (item == 0) {
                                nowData.leaveDivShip = '船舶自行'
                            } else {
                                nowData.leaveDivShip = '使用引水'
                            }
                        } else {
                            nowData.saveDivNum = 2 - parseFloat(item) - 1
                            if (item == 0) {
                                nowData.leaveDivShip = '船舶自行'
                            } else {
                                nowData.leaveDivShip = '使用引水'
                            }
                        }
                    }
                    this.setState({
                        //objectData: data,
                    })
                    console.log('objectData111+++', data)
                    DeviceEventEmitter.emit('setData', { data: data })
                }
            }
        }
    }

    //船上备注修改
    shipNoteEndEditing = (index, text) => {
        const dataLength = this.props.data.detailList;
        const data = cloneDeep(this.props.data);
        for (let i = 0; i < dataLength.length; i++) {
            //判断到当前index这条
            if (index == i) {
                data.detailList[i].shipNote = text;
                this.setState({
                    //objectData: data,
                })
                DeviceEventEmitter.emit('setData', { data: data })
            }
        }
    }

    //{}里边相当于结构object index是在props里边取
    render() {
        // console.log('item++++', this.props.index)
        const {
            id, atb, atd, portName, berthVoy, leaveVoy, saveTugNum, saveTugMoney, saveDivNum, saveDivMoney, amount, portNote, hybNote
        } = this.props.item || {};
        const { index, opState } = this.props || {};
        const { revise, berthTugShip, leaveTugShip, berthDivShip, leaveDivShip,shipNote  } = this.state;
        // const { revise, berthTugShip, leaveTugShip} = this.props.item;
        
        let editableInput = (opState == 0 || opState == 10 || opState == 70) ? true : false;
       
        return (
            <View style={styles.container}>
                <View style={[styles.textView, { flexDirection: 'column', width: 100, }]}>
                    <View style={[styles.textCol, { width: 100, flex: 1, }]}>
                        <Text style={styles.text}>{atb}</Text>
                    </View>
                    <View style={[styles.textCol, { width: 100, flex: 1, }]}>
                        <Text style={styles.text}>{atd}</Text>
                    </View>
                </View>

                <View style={styles.col}>
                    <Text style={styles.text}>{portName}</Text>
                </View>

                <View style={[styles.textView, { flexDirection: 'column', }]}>
                    <View style={[styles.textCol, { flex: 1, }]}>
                        <Text style={styles.text}>{berthVoy}</Text>
                    </View>
                    <View style={[styles.textCol, { flex: 1, }]}>
                        <Text style={styles.text}>{leaveVoy}</Text>
                    </View>
                </View>

                <View style={[styles.textView, { flexDirection: 'column', }]}>
                    <View style={[styles.textCol, { flex: 1 }]}>
                        {editableInput ? <InputItem
                            keyboardType="numeric"
                            maskType={MaskType.INTEGER}
                            value={berthTugShip}
                            // selectTextOnFocus={true}
                            onChangeText={(text) => {
                                this.tugShipEndEditing(index, text, '0')
                            }}
                            placeholder={''}
                            onFocus={() => {
                                console.log('~~~',berthTugShip,this.state.berthTugShip)
                                if (berthTugShip == 0 || berthTugShip == '0') {
                                    this.setState({ berthTugShip: '' })
                                }
                                
                            }}
                            onBlur={(event) => {
                                if ( event.nativeEvent.text == '') {
                                    this.setState({ berthTugShip: '0' })
                                }else{
                                    // this.setState({ berthTugShip: event.nativeEvent.text })
                                }
                                
                            }}
                        /> : <Text style={styles.text}>{berthTugShip}</Text>}
                    </View>
                    <View style={[styles.textCol, { flex: 1, }]}>
                        {editableInput ? <InputItem
                            keyboardType="numeric"
                            maskType={MaskType.INTEGER}
                            // selectTextOnFocus={true}
                            value={leaveTugShip}
                            onChangeText={(text) => {
                                this.tugShipEndEditing(index, text, '1')
                            }}
                            placeholder=''
                            onFocus={() => {
                                if (leaveTugShip == 0 || leaveTugShip == '0') {
                                    this.setState({ leaveTugShip: '' })
                                }
                               
                            }}
                            onBlur={(event) => {
                                if (event.nativeEvent.text === '') {
                                    this.setState({ leaveTugShip: '0' })
                                }else{
                                    // this.setState({ leaveTugShip: event.nativeEvent.text })
                                }
                                  
                            }}
                        /> : <Text style={styles.text}>{leaveTugShip}</Text>}
                    </View>
                </View>

                <View style={styles.col}>
                    <Text style={styles.text}>{saveTugNum}</Text>
                </View>

                <View style={[styles.col]}>
                    <Text style={styles.text}>{saveTugMoney}</Text>
                </View>
                <View style={[styles.textView, { flexDirection: 'column', width: 100, }]}>
                    <View style={[styles.textCol, {
                        flex: 1,
                        width: 100,
                        justifyContent: 'center',
                        alignItems: 'center',
                        textAlign: 'center',
                    }]}>
                        {editableInput ? <DiversionSelect
                            waterDiversion={berthDivShip}
                            id={index}
                            way={'berth'}
                        /> : <Text style={[styles.text,{textAlign:'left'}]}>{berthDivShip}</Text>}
                    </View>
                    <View style={[styles.textCol, {
                        flex: 1,
                        width: 100,
                        justifyContent: 'center',
                        alignItems: 'center',
                        textAlign: 'center'
                    }]}>
                        {editableInput ? <DiversionSelect
                            waterDiversion={leaveDivShip}
                            id={index}
                            way={'leave'}
                        /> : <Text style={[styles.text,{textAlign:'left'}]}>{leaveDivShip}</Text>}
                    </View>
                </View>

                <View style={[styles.col]}>
                    <Text style={styles.text}>{saveDivNum}</Text>
                </View>

                <View style={[styles.col]}>
                    <Text style={styles.text}>{saveDivMoney}</Text>
                </View>

                <View style={[styles.col]}>
                    <Text style={styles.text}>{amount}</Text>
                </View>

                <View style={[styles.col]}>
                    <InputGroup style={[commonStyles.inputGroup, { flex: 1 }]}>
                        {editableInput ? <InputItem
                            maskType={MaskType.POSITIVE_FLOAT}
                            value={revise}
                            // selectTextOnFocus={true}
                            // clearTextOnFocus={true}
                            onChangeText={(text) => {
                                this.reviseEndEditing(index, text)
                            }}
                            placeholder=''
                            onBlur={(event) => {
                                if (revise === '') {
                                    this.setState({ revise: '0' })
                                }else{
                                    // this.setState({ revise: event.nativeEvent.text })
                                }
                            }}
                            onFocus={() => {
                                if (revise == 0 || revise == '0') {
                                    this.setState({ revise: '' })
                                }
                                if(this.state.revise == '0'){
                                    this.setState({ revise: '' })
                                }
                            }}
                        /> : <Text style={styles.text}>{revise}</Text>}
                    </InputGroup>
                </View>

                <View style={[styles.col, { width: 200 }]}>
                    <InputGroup style={[commonStyles.inputGroup, { flex: 1 }]}>
                        {editableInput ? <InputItem
                            value={shipNote}
                            maxLength={48}
                            onChangeText={(text) => {
                                // this.setState({ inputShipNote: text });
                                this.shipNoteEndEditing(index, text)
                            }}
                            onBlur={(event) => {
                                // this.shipNoteEndEditing(index,event.nativeEvent.text)
                            }}
                            placeholder=''
                        /> : <Text style={styles.text}>{shipNote}</Text>}
                    </InputGroup>
                </View>

                <View style={[styles.col, { width: 200, paddingLeft: 10, }]}>
                    <Text style={styles.text} numberOfLines={4}>{portNote}</Text>
                </View>
                <View style={[styles.col, { width: 200, paddingLeft: 10, }]}>
                    <Text style={styles.text} numberOfLines={4}>{hybNote}</Text>
                </View>
                <Animated.View style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    bottom: 0,
                    transform: [{ translateX: this.props.headerTranslateX || 0}],
                }}
                >
                    <View style={[styles.container, { flex: 1 }]}>
                        <View style={[styles.textView, { flexDirection: 'column', width: 100, }]}>
                            <View style={[styles.textCol, { flex: 1, width: 100, }]}>
                                <Text style={styles.text}>{atb}</Text>
                            </View>
                            <View style={[styles.textCol, { flex: 1, width: 100, }]}>
                                <Text style={styles.text}>{atd}</Text>
                            </View>
                        </View>
                        <View style={styles.col}>
                            <Text style={styles.text}>{portName}</Text>
                        </View>
                        <View style={[styles.textView, { flexDirection: 'column', }]}>
                            <View style={[styles.textCol, { flex: 1, }]}>
                                <Text style={styles.text}>{berthVoy}</Text>
                            </View>
                            <View style={[styles.textCol, { flex: 1, }]}>
                                <Text style={styles.text}>{leaveVoy}</Text>
                            </View>
                        </View>
                    </View>
                </Animated.View>
            </View>
        );
    }
}

SaveFeeItem.propTypes = {
    item: PropTypes.object.isRequired,
    headerTranslateX: PropTypes.object.isRequired,
    data: PropTypes.object.isRequired,
    index: PropTypes.number,
    opState: PropTypes.number,
};

const mapStateToProps = createStructuredSelector({
    data: makeData(),
});

export default connect(mapStateToProps)(SaveFeeItem);
