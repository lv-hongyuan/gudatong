import { call, put, takeLatest } from 'redux-saga/effects';
import { UPLOAD_BEFOREHAND_LAYOUT_ACTION, GET_BEFOREHAND_LAYOUT_ACTION } from './constants';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import { uploadBeforehandResultAction, getBeforehandResultAction } from './actions';
import { errorMessage } from '../ErrorHandler/actions';
import uploadImage from '../../common/uploadImage';
import { getPopId } from '../ShipWork/sagas';

function* uploadBeforehandLayout(action) {
  console.log(action);
  try {
    const { stowagePic, stabilityTablePic } = action.payload;
    const popId = yield call(getPopId);
    const param = { stowagePic, stabilityTablePic, popId };

    if (stowagePic) {
      if (stowagePic.path) {
        const response = yield call(uploadImage, stowagePic, 'stowagePic', popId);
        param.stowagePic = response.id;
      } else {
        param.stowagePic = stowagePic.substr(stowagePic.lastIndexOf('/') + 1);
      }
    }
    if (stabilityTablePic) {
      if (stabilityTablePic.path) {
        const response = yield call(uploadImage, stabilityTablePic, 'stabilityTablePic', popId);
        param.stabilityTablePic = response.id;
      } else {
        param.stabilityTablePic = stabilityTablePic.substr(stabilityTablePic.lastIndexOf('/') + 1);
      }
    }
    const response = yield call(request, ApiFactory.upLoadPlanCargo(param));
    console.log(response);
    yield put(uploadBeforehandResultAction(response, true));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(uploadBeforehandResultAction(undefined, false));
  }
}

export function* uploadBeforehandLayoutSaga() {
  yield takeLatest(UPLOAD_BEFOREHAND_LAYOUT_ACTION, uploadBeforehandLayout);
}

function* upLoadPlanShow(action) {
  console.log(action);
  try {
    const popId = yield call(getPopId);
    const response = yield call(request, ApiFactory.upLoadPlanShow(popId));
    console.log(response);
    yield put(getBeforehandResultAction(response, true));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(getBeforehandResultAction(undefined, false));
  }
}

export function* upLoadPlanShowSaga() {
  yield takeLatest(GET_BEFOREHAND_LAYOUT_ACTION, upLoadPlanShow);
}

// All sagas to be loaded
export default [
  uploadBeforehandLayoutSaga,
  upLoadPlanShowSaga,
];
