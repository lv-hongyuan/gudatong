import { createSelector } from 'reselect/es';

const selectBeforehandLayoutDomain = () => state => state.beforehandLayout;
const selectShipStateDomain = () => state => state.shipWork.data;

const makeSelectUploadBeforehandLayoutSuccess = () => createSelector(
  selectBeforehandLayoutDomain(),
  subState => subState.success,
);

const makeSelectShipName = () => createSelector(
  selectShipStateDomain(),
  subState => subState.shipName,
);

const makeSelectVoyageCode = () => createSelector(
  selectShipStateDomain(),
  subState => subState.voyageCode,
);

const makeSelectPortName = () => createSelector(
  selectShipStateDomain(),
  subState => subState.portName,
);

const makeSelectStowagePic = () => createSelector(
  selectBeforehandLayoutDomain(),
  subState => subState.data && subState.data.stowagePic,
);
const makeSelectStabilityTablePic = () => createSelector(
  selectBeforehandLayoutDomain(),
  subState => subState.data && subState.data.stabilityTablePic,
);

export {
  makeSelectUploadBeforehandLayoutSuccess,
  selectBeforehandLayoutDomain,
  makeSelectShipName,
  makeSelectVoyageCode,
  makeSelectPortName,
  makeSelectStowagePic,
  makeSelectStabilityTablePic,
};
