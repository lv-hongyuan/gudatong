import {
  UPLOAD_BEFOREHAND_LAYOUT_ACTION,
  UPLOAD_BEFOREHAND_LAYOUT_ACTION_RESULT,
  GET_BEFOREHAND_LAYOUT_ACTION,
  GET_BEFOREHAND_LAYOUT_ACTION_RESULT,
} from './constants';

export function uploadBeforehandAction(data) {
  return {
    type: UPLOAD_BEFOREHAND_LAYOUT_ACTION,
    payload: data,
  };
}

export function uploadBeforehandResultAction(data, success) {
  return {
    type: UPLOAD_BEFOREHAND_LAYOUT_ACTION_RESULT,
    payload: {
      data,
      success,
    },
  };
}

export function getBeforehandAction() {
  return {
    type: GET_BEFOREHAND_LAYOUT_ACTION,
  };
}

export function getBeforehandResultAction(data, success) {
  return {
    type: GET_BEFOREHAND_LAYOUT_ACTION_RESULT,
    payload: {
      data,
      success,
    },
  };
}
