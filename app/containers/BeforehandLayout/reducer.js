import {
  UPLOAD_BEFOREHAND_LAYOUT_ACTION_RESULT,
  GET_BEFOREHAND_LAYOUT_ACTION_RESULT,
  GET_BEFOREHAND_LAYOUT_ACTION,
  UPLOAD_BEFOREHAND_LAYOUT_ACTION,
} from './constants';

const initState = {
  success: false,
  data: {
    stowagePic: '',
    stabilityTablePic: '',
  },
};

export default function (state = initState, action) {
  switch (action.type) {
    case GET_BEFOREHAND_LAYOUT_ACTION:
    case UPLOAD_BEFOREHAND_LAYOUT_ACTION:
      return { ...state, success: false };
    case UPLOAD_BEFOREHAND_LAYOUT_ACTION_RESULT:
      return { ...state, success: action.payload.success };
    case GET_BEFOREHAND_LAYOUT_ACTION_RESULT:
      return { ...state, success: action.payload.success, data: action.payload.data };
    default:
      return state;
  }
}
