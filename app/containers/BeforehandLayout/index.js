import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { Container, Content, Form, Item, Label } from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import HeaderButtons from 'react-navigation-header-buttons';
import { uploadBeforehandAction, getBeforehandAction } from './actions';
import {
  makeSelectPortName,
  makeSelectShipName, makeSelectStabilityTablePic, makeSelectStowagePic,
  makeSelectUploadBeforehandLayoutSuccess,
  makeSelectVoyageCode,
} from './selectors';
import SelectImageView from '../../components/SelectImageView';
import commonStyles from '../../common/commonStyles';
import screenHOC from '../screenHOC';

const styles = StyleSheet.create({
  item: {
    borderBottomColor: 'transparent',
    marginBottom: 10,
  },
  imageStyle: {
    width: 144,
    height: 144,
  },
  imageTouch: {
    width: 144,
    height: 144,
  },
});

/**
 * 预配图上传
 * Created by jianzhexu on 2018/3/26
 */
@screenHOC
class BeforehandLayout extends React.PureComponent {
  static navigationOptions = ({ navigation }) => ({
    title: '预配图上传',
    headerRight: (
      <HeaderButtons>
        <HeaderButtons.Item
          title="保存"
          buttonStyle={{ fontSize: 14, color: '#ffffff' }}
          onPress={() => {
            const handleSubmitFunc = navigation.getParam('submit', undefined);
            if (handleSubmitFunc) {
              requestAnimationFrame(() => {
                handleSubmitFunc();
              });
            }
          }}
        />
      </HeaderButtons>
    ),
  });
  static propTypes = {
    navigation: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    isSuccess: PropTypes.bool.isRequired,
    shipName: PropTypes.string.isRequired,
    portName: PropTypes.string.isRequired,
    voyageCode: PropTypes.string.isRequired,
    stowagePic: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
    stabilityTablePic: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  };
  static defaultProps = {
    stowagePic: undefined,
    stabilityTablePic: undefined,
  };

  constructor(props) {
    super(props);
    this.state = {
      stowagePic: null,
      stabilityTablePic: null,
      isUploading: false,
      isInit: true,
    };
    this.props.navigation.setParams({ submit: this.handleSubmit });
  }

  componentDidMount() {
    this.props.dispatch(getBeforehandAction());
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isSuccess && this.state.isUploading) {
      const goBackCallback = this.props.navigation.getParam('onGoBack', undefined);
      if (goBackCallback) {
        goBackCallback();
      }
      this.props.navigation.goBack();
      this.setState({
        isUploading: false,
      });
    }

    if (nextProps.isSuccess && this.state.isInit) {
      this.setState({
        stowagePic: nextProps.stowagePic,
        stabilityTablePic: nextProps.stabilityTablePic,
        isInit: false,
      });
    }
  }

  handleSubmit = () => {
    this.setState({
      isUploading: true,
    }, () => {
      this.props.dispatch(uploadBeforehandAction({
        stowagePic: this.state.stowagePic,
        stabilityTablePic: this.state.stabilityTablePic,
      }));
    });
  };

  render() {
    const { shipName, voyageCode, portName } = this.props;
    return (
      <Container>
        <Content style={{
          backgroundColor: '#FFFFFF',
          padding: 10,
        }}
        >
          <Form>
            <Item style={styles.item}>
              <Label style={commonStyles.inputLabel}>船&nbsp;&nbsp;&nbsp;&nbsp;名:</Label>
              <Label style={commonStyles.contentLabel}>{shipName}</Label>
            </Item>
            <Item style={styles.item}>
              <Label style={commonStyles.inputLabel}>航&nbsp;&nbsp;&nbsp;&nbsp;次:</Label>
              <Label style={commonStyles.contentLabel}>{voyageCode}</Label>
            </Item>
            <Item style={styles.item}>
              <Label style={commonStyles.inputLabel}>港&nbsp;&nbsp;&nbsp;&nbsp;口:</Label>
              <Label style={commonStyles.contentLabel}>{portName}</Label>
            </Item>
            <Item style={styles.item}>
              <Label style={commonStyles.inputLabel}>配载图:</Label>
              <SelectImageView
                title="选择配载图照片"
                source={this.state.stowagePic}
                onPickImages={(images) => {
                  const image = images[0];
                  this.setState({
                    stowagePic: {
                      fileName: image.uri.split('/').reverse()[0],
                      fileSize: image.size,
                      path: image.uri,
                      uri: image.uri,
                    },
                  });
                }}
                onDelete={() => {
                  this.setState({
                    stowagePic: undefined,
                  });
                }}
              />
            </Item>
            <Item style={styles.item}>
              <Label style={commonStyles.inputLabel}>稳性表:</Label>
              <SelectImageView
                title="选择稳性表照片"
                source={this.state.stabilityTablePic}
                onPickImages={(images) => {
                  const image = images[0];
                  this.setState({
                    stabilityTablePic: {
                      fileName: image.uri.split('/').reverse()[0],
                      fileSize: image.size,
                      path: image.uri,
                      uri: image.uri,
                    },
                  });
                }}
                onDelete={() => {
                  this.setState({
                    stabilityTablePic: undefined,
                  });
                }}
              />
            </Item>
          </Form>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  portName: makeSelectPortName(),
  voyageCode: makeSelectVoyageCode(),
  shipName: makeSelectShipName(),
  isSuccess: makeSelectUploadBeforehandLayoutSuccess(),
  stowagePic: makeSelectStowagePic(),
  stabilityTablePic: makeSelectStabilityTablePic(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(BeforehandLayout);
