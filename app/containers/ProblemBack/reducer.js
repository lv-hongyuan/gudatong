import {
  CREATE_PROBLEM_BACK_ACTION_RESULT,
  CREATE_PROBLEM_BACK_ACTION,
} from './constants';

const initState = {
  success: false,
  data: {},
};

export default function (state = initState, action) {
  switch (action.type) {
    case CREATE_PROBLEM_BACK_ACTION:
      return { ...state, success: false };
    case CREATE_PROBLEM_BACK_ACTION_RESULT:
      return { ...state, success: action.payload.success };
    default:
      return state;
  }
}
