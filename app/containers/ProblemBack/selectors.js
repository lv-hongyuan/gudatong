import { createSelector } from 'reselect/es';

const selectProblemBackDomain = () => state => state.problemBack;
const makeSelectCreateProblemBackSuccess = () => createSelector(
  selectProblemBackDomain(),
  (subState) => {
    console.debug(subState.success);
    return subState.success;
  },
);

const makeSelectData = () => createSelector(
  selectProblemBackDomain(),
  subState => subState.data,
);
export {
  makeSelectCreateProblemBackSuccess,
  selectProblemBackDomain,
  makeSelectData,
};
