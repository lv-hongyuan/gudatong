import { call, put, takeLatest } from 'redux-saga/effects';
import {
  CREATE_PROBLEM_BACK_ACTION,
} from './constants';
import request from '../../utils/request';
import uploadImage from '../../common/uploadImage';
import ApiFactory from '../../common/Api';
import {
  createProblemBackActionResultAction,
} from './actions';
import { errorMessage } from '../ErrorHandler/actions';

function* createProblemBack(action) {
  console.log(action);
  try {
    const { data } = action.payload;
    const { backPic } = data;
    if (backPic) {
      const response = yield call(uploadImage, backPic, 'businessType', 0);
      data.backPic = response.id;
    }
    const response = yield call(
      request,
      ApiFactory.createFeedBack(action.payload.data),
    );
    yield put(createProblemBackActionResultAction(response, undefined));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(createProblemBackActionResultAction(undefined, e));
  }
}

export function* createProblemBackSaga() {
  yield takeLatest(CREATE_PROBLEM_BACK_ACTION, createProblemBack);
}

// All sagas to be loaded
export default [createProblemBackSaga];
