import {
  CREATE_PROBLEM_BACK_ACTION,
  CREATE_PROBLEM_BACK_ACTION_RESULT,
} from './constants';

export function createProblemBackAction(data) {
  return {
    type: CREATE_PROBLEM_BACK_ACTION,
    payload: { data },
  };
}

export function createProblemBackActionResultAction(data, success) {
  return {
    type: CREATE_PROBLEM_BACK_ACTION_RESULT,
    payload: {
      data,
      success,
    },
  };
}

