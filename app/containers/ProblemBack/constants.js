/*
 *
 * ProblemBack constants
 *
 */
export const CREATE_PROBLEM_BACK_ACTION = 'app/ProblemBack/CREATE_PROBLEM_BACK_ACTION';
export const CREATE_PROBLEM_BACK_ACTION_RESULT = 'app/ProblemBack/CREATE_PROBLEM_BACK_ACTION_RESULT';

export const backType = [
  '数据显示问题',
  '图片加载问题',
  '信息录入问题',
  '操作问题',
  '其它问题',
];
