import { ERROR_MESSAGE, RESET_MESSAGE, SUCCESS_MESSAGE, WARN_MESSAGE } from './constants';

const initState = {
  msg: '',
  type: ERROR_MESSAGE,
  show: false,
};

export default function (state = initState, action) {
  switch (action.type) {
    case ERROR_MESSAGE:
      return {
        ...state, type: action.type, msg: action.payload, show: true,
      };
    case RESET_MESSAGE:
      return {
        ...state, type: ERROR_MESSAGE, msg: '', show: false,
      };
    case SUCCESS_MESSAGE:
      return {
        ...state, type: action.type, msg: action.payload, show: true,
      };
    case WARN_MESSAGE:
      return {
        ...state, type: action.type, msg: action.payload, show: true,
      };
    default:
      return state;
  }
}
