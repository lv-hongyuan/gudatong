import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { connect } from 'react-redux';
import LoadingComponent from '../../components/LoadingCmponent';
import AlertView from '../../components/Alert';
import { resetMessage } from './actions';

class ErrorHandler extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {

  }

  renderChildren() {
    return React.Children.map(this.props.children, child => React.cloneElement(child, { ...this.props }));
  }

  render() {
    return (
      <View {...this.props}>
        {this.renderChildren()}
        {this.props.error_Show &&
        <AlertView
          show={this.props.error_Show}
          message={this.props.error_Msg}
          confirmAction={() => {
            this.props.dispatch(resetMessage());
          }}
        />
        }
        <LoadingComponent visible={this.props.isLoading} />
      </View>
    );
  }
}

ErrorHandler.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  error_Msg: state.error.msg,
  error_Type: state.error.type,
  error_Show: state.error.show,
  isLoading: state.loading.isLoading,
});

export default connect(mapStateToProps)(ErrorHandler);
