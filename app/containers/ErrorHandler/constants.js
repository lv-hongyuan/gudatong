export const ERROR_MESSAGE = 'app/App/ERROR_MESSAGE';
export const SUCCESS_MESSAGE = 'app/App/SUCCESS_MESSAGE';
export const WARN_MESSAGE = 'app/App/WARN_MESSAGE';
export const RESET_MESSAGE = 'app/App/RESET_MESSAGE';
