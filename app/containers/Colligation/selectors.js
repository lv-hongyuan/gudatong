import { createSelector } from 'reselect/es';

const selectColligationDomain = () => state => state.colligation;
const selectShipStateDomain = () => state => state.shipWork.data;

const makeSelectShipName = () => createSelector(
  selectShipStateDomain(),
  subState => subState.shipName,
);

const makeSelectVoyageCode = () => createSelector(
  selectShipStateDomain(),
  subState => subState.voyageCode,
);

const makeSelectPortName = () => createSelector(
  selectShipStateDomain(),
  subState => subState.portName,
);

const makeSelectUploadColligationImageSuccess = () => createSelector(
  selectColligationDomain(),
  (subState) => {
    console.debug(subState.success);
    return subState.success;
  },
);

const makeSelectColligationImages = () => createSelector(
  selectColligationDomain(),
  subState => subState.data,
);

export {
  makeSelectUploadColligationImageSuccess,
  selectColligationDomain,
  makeSelectShipName,
  makeSelectVoyageCode,
  makeSelectColligationImages,
  makeSelectPortName,
};
