import {
  UPLOAD_COLLIGATION_IMAGES_ACTION_RESULT,
  GET_COLLIGATION_IMAGES_ACTION_RESULT,
  GET_COLLIGATION_IMAGES_ACTION,
  UPLOAD_COLLIGATION_IMAGES_ACTION,
} from './constants';

const initState = {
  success: false,
  data: [],
};

export default function (state = initState, action) {
  switch (action.type) {
    case GET_COLLIGATION_IMAGES_ACTION:
    case UPLOAD_COLLIGATION_IMAGES_ACTION:
      return { ...state, success: false };
    case UPLOAD_COLLIGATION_IMAGES_ACTION_RESULT:
      return { ...state, success: action.payload.success };
    case GET_COLLIGATION_IMAGES_ACTION_RESULT:
      if (action.payload.data) {
        return {
          ...state,
          success: action.payload.success,
          data: action.payload.data.bindPic ? action.payload.data.bindPic.slice(0) : [],
        };
      }
      return { ...state, success: action.payload.success };

    default:
      return state;
  }
}
