import { call, put, takeLatest } from 'redux-saga/effects';
import { GET_COLLIGATION_IMAGES_ACTION, UPLOAD_COLLIGATION_IMAGES_ACTION } from './constants';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import { uploadColligationImageResultAction, getColligationImageResultAction } from './actions';
import { errorMessage } from '../ErrorHandler/actions';
import uploadImage from '../../common/uploadImage';
import { getPopId } from '../ShipWork/sagas';

function* uploadColligationImage(action) {
  console.log(action);
  try {
    const popId = yield call(getPopId);
    const resultImages = [];

    const currentData = action.payload || [];

    for (let i = 0; i < currentData.length; i += 1) {
      const item = currentData[i];
      if (item.path) {
        const response = yield call(uploadImage, item, 'bind_situation');
        console.log(response);
        resultImages.push(response.id);
      } else {
        resultImages.push(item.substr(item.lastIndexOf('/') + 1));
      }
    }

    const response1 = yield call(request, ApiFactory.bindSituation({
      popId,
      bindPic: resultImages,
    }));
    console.log(response1);
    yield put(uploadColligationImageResultAction(undefined, true));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(uploadColligationImageResultAction(undefined, false));
  }
}

export function* uploadColligationImageSaga() {
  yield takeLatest(UPLOAD_COLLIGATION_IMAGES_ACTION, uploadColligationImage);
}

function* getColligationImage(action) {
  console.log(action);
  try {
    const popId = yield call(getPopId);
    const response = yield call(request, ApiFactory.bindShow(popId));
    console.log(response);
    yield put(getColligationImageResultAction(response, true));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(getColligationImageResultAction(undefined, false));
  }
}

export function* getColligationImageSaga() {
  yield takeLatest(GET_COLLIGATION_IMAGES_ACTION, getColligationImage);
}

// All sagas to be loaded
export default [
  getColligationImageSaga,
  uploadColligationImageSaga,
];
