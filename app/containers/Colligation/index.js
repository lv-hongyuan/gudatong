import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Modal } from 'react-native';
import { connect } from 'react-redux';
import { Container, Content, Item, Label, View } from 'native-base';
import ImageViewer from 'react-native-image-zoom-viewer';
import { createStructuredSelector } from 'reselect/es';
import HeaderButtons from 'react-navigation-header-buttons';
import {
  makeSelectPortName,
  makeSelectShipName,
  makeSelectVoyageCode,
  makeSelectUploadColligationImageSuccess,
  makeSelectColligationImages,
} from './selectors';
import { getColligationImageAction, uploadColligationImageAction } from './actions';
import SelectImageView from '../../components/SelectImageView';
import screenHOC from '../screenHOC';
import myTheme from '../../Themes';

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    borderBottomColor: 'transparent',
  },
  label: {},
  text: {
    flex: 2,
  },
  container: {
    backgroundColor: '#FFFFFF',
    marginTop: 10,
    paddingLeft: 20,
    paddingTop: 10,
    paddingBottom: 10,
    paddingRight: 20,
  },
});

/**
 *
 * 绑扎情况
 * Created by jianzhexu on 2018/3/26
 */
@screenHOC
class Colligation extends React.PureComponent {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.getParam('title', '绑扎情况'),
    headerRight: (
      <HeaderButtons>
        <HeaderButtons.Item
          title="保存"
          buttonStyle={{ fontSize: 14, color: '#ffffff' }}
          onPress={() => {
            const handleSubmitFunc = navigation.getParam('submit', undefined);
            if (handleSubmitFunc) handleSubmitFunc();
          }}
        />
      </HeaderButtons>
    ),
  });
  static propTypes = {
    navigation: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    success: PropTypes.bool.isRequired,
    data: PropTypes.array.isRequired,
    portName: PropTypes.string.isRequired,
    shipName: PropTypes.string.isRequired,
    voyageCode: PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      data: [],
      isUploading: false,
      isInit: true,
      visible: false,
      currentIndex: 0,
    };

    this.props.navigation.setParams({ submit: this.handleSubmit });
  }

  componentDidMount() {
    this.props.dispatch(getColligationImageAction());
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.success && this.state.isUploading) {
      this.setState({ isUploading: false }, () => {
        const goBackCallback = this.props.navigation.getParam('onGoBack', undefined);
        if (goBackCallback) {
          goBackCallback();
        }
        this.props.navigation.goBack();
      });
    }

    if (nextProps.success && this.state.isInit) {
      this.setState({
        data: nextProps.data.slice(0),
        isInit: false,
      });
    }
  }

  handleSubmit = () => {
    this.props.dispatch(uploadColligationImageAction(this.state.data));
    this.setState({
      isUploading: true,
    });
  };

  render() {
    return (
      <Container theme={myTheme}>
        <Content contentInsetAdjustmentBehavior="scrollableAxes">
          <View style={styles.container}>
            <Item style={styles.row}>
              <Label style={styles.label}>港口:</Label>
              <Label style={styles.text}>{this.props.portName} </Label>
            </Item>
            <Item style={styles.row}>
              <Label style={styles.label}>船名:</Label>
              <Label style={styles.text}>{this.props.shipName} </Label>
            </Item>
            <Item style={styles.row}>
              <Label style={styles.label}>航次:</Label>
              <Label style={styles.text}>{this.props.voyageCode}</Label>
            </Item>
          </View>

          <View style={styles.container}>
            <Item style={styles.row}>
              <Label style={styles.text}>点击上传绑扎情况图片</Label>
            </Item>
            <View style={{
              flexDirection: 'row',
              flexWrap: 'wrap',
              width: '100%',
              // marginTop: 10,
              justifyContent: 'flex-start',
              alignItems: 'center',
            }}
            >
              {this.state.data.map((item, index) => (
                <View
                  key={`${item.uri || item}`}
                  style={{
                    width: '33%',
                    aspectRatio: 1,
                    justifyContent: 'center',
                    marginTop: 10,
                    padding: 5,
                    alignItems: 'center',
                  }}
                >
                  <SelectImageView
                    source={item.uri ? item : { uri: item }}
                    style={{
                      width: '100%',
                      height: '100%',
                    }}
                    onPress={() => {
                      this.setState({
                        visible: true,
                        currentIndex: index,
                      });
                    }}
                    onDelete={() => {
                      const self = this;
                      const data = self.state.data.slice(0);
                      data.splice(index, 1);
                      self.setState({
                        data,
                      });
                    }}
                  />
                </View>
              ))}
              <View style={{
                width: '33%',
                aspectRatio: 1,
                justifyContent: 'center',
                padding: 5,
                marginTop: 10,
                alignItems: 'center',
              }}
              >
                <SelectImageView
                  style={{
                    width: '100%',
                    height: '100%',
                  }}
                  options={{
                    imageCount: 9,
                  }}
                  onPickImages={(images) => {
                    let data = this.state.data.slice(0);
                    data = data.concat(images.map(image => ({
                      fileName: image.uri.split('/').reverse()[0],
                      fileSize: image.size,
                      path: image.uri,
                      uri: image.uri,
                    })));
                    this.setState({ data });
                  }}
                />
              </View>
            </View>
          </View>
        </Content>
        <Modal visible={this.state.visible} transparent>
          <ImageViewer
            index={this.state.currentIndex}
            imageUrls={this.state.data.map(item => ({
              url: item.uri ? item.uri : item,
            }))}
            onClick={() => {
              this.setState({ visible: false });
            }}
            onSwipeDown={() => {
              this.setState({ visible: false });
            }}
          />
        </Modal>
      </Container>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  success: makeSelectUploadColligationImageSuccess(),
  portName: makeSelectPortName(),
  shipName: makeSelectShipName(),
  voyageCode: makeSelectVoyageCode(),
  data: makeSelectColligationImages(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Colligation);
