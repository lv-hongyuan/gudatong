import {
  UPLOAD_COLLIGATION_IMAGES_ACTION,
  UPLOAD_COLLIGATION_IMAGES_ACTION_RESULT,
  GET_COLLIGATION_IMAGES_ACTION,
  GET_COLLIGATION_IMAGES_ACTION_RESULT,
} from './constants';

export function uploadColligationImageAction(data) {
  return {
    type: UPLOAD_COLLIGATION_IMAGES_ACTION,
    payload: data,
  };
}

export function uploadColligationImageResultAction(data, success) {
  return {
    type: UPLOAD_COLLIGATION_IMAGES_ACTION_RESULT,
    payload: {
      data,
      success,
    },
  };
}

export function getColligationImageAction() {
  return {
    type: GET_COLLIGATION_IMAGES_ACTION,
  };
}

export function getColligationImageResultAction(data, success) {
  return {
    type: GET_COLLIGATION_IMAGES_ACTION_RESULT,
    payload: {
      data,
      success,
    },
  };
}
