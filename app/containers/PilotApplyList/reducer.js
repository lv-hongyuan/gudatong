import {
  GET_PILOT_APPLY_LIST_ACTION_RESULT,
  GET_PILOT_APPLY_LIST_ACTION,
  DELETE_PILOT_APPLY_ACTION_RESULT,
  DELETE_PILOT_APPLY_ACTION,
} from './constants';

const initState = {
  success: false,
  data: [],
};

export default function (state = initState, action) {
  switch (action.type) {
    case DELETE_PILOT_APPLY_ACTION:
    case GET_PILOT_APPLY_LIST_ACTION:
      return { ...state, success: false };
    case GET_PILOT_APPLY_LIST_ACTION_RESULT:
      return { ...state, success: action.payload.success, data: action.payload.data.dtoList };
    case DELETE_PILOT_APPLY_ACTION_RESULT:
      return { ...state, success: action.payload.success };
    default:
      return state;
  }
}
