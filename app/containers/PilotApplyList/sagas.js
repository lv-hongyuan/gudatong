import { call, put, takeLatest } from 'redux-saga/effects';
import {
  GET_PILOT_APPLY_LIST_ACTION,
  DELETE_PILOT_APPLY_ACTION,
} from './constants';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import {
  getPilotApplyListResultAction,
  deletePilotApplyResultAction,
} from './actions';
import { errorMessage } from '../ErrorHandler/actions';
import { getPopId } from '../ShipWork/sagas';

function* getPilotApplyList(action) {
  console.log(action);
  try {
    const popId = yield call(getPopId);
    const response = yield call(request, ApiFactory.findApplyDivs(popId));
    console.log(response);
    yield put(getPilotApplyListResultAction(response, true));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(getPilotApplyListResultAction(undefined, false));
  }
}

export function* getPilotApplyListSaga() {
  yield takeLatest(GET_PILOT_APPLY_LIST_ACTION, getPilotApplyList);
}

function* deletePilotApply(action) {
  console.log(action);
  try {
    const response = yield call(request, ApiFactory.deleteApplyDiv(action.payload));
    yield put(deletePilotApplyResultAction(response, true));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(deletePilotApplyResultAction(undefined, false));
  }
}

export function* deletePilotApplySaga() {
  yield takeLatest(DELETE_PILOT_APPLY_ACTION, deletePilotApply);
}

// All sagas to be loaded
export default [
  getPilotApplyListSaga,
  deletePilotApplySaga,
];
