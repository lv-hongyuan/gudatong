import {
  GET_PILOT_APPLY_LIST_ACTION_RESULT,
  GET_PILOT_APPLY_LIST_ACTION,
  DELETE_PILOT_APPLY_ACTION,
  DELETE_PILOT_APPLY_ACTION_RESULT,
} from './constants';

export function getPilotApplyListAction(data) {
  return {
    type: GET_PILOT_APPLY_LIST_ACTION,
    payload: data,
  };
}

export function refreshPilotApplyListAction(data) {
  return {
    type: GET_PILOT_APPLY_LIST_ACTION,
    payload: data,
    noLoading: true,
  };
}

export function getPilotApplyListResultAction(data, success) {
  return {
    type: GET_PILOT_APPLY_LIST_ACTION_RESULT,
    payload: {
      data,
      success,
    },
  };
}

export function deletePilotApplyAction(data) {
  return {
    type: DELETE_PILOT_APPLY_ACTION,
    payload: data,
  };
}

export function deletePilotApplyResultAction(success) {
  return {
    type: DELETE_PILOT_APPLY_ACTION_RESULT,
    payload: {
      success,
    },
  };
}
