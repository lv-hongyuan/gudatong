import { createSelector } from 'reselect/es';

const selectPilotApplyListDomain = () => state => state.pilotApplyList;

const makeSelectPilotApplyListSuccess = () => createSelector(
  selectPilotApplyListDomain(),
  (subState) => {
    console.debug(subState.success);
    return subState.success;
  },
);

const makeSelectPilotApplyList = () => createSelector(
  selectPilotApplyListDomain(),
  (subState) => {
    console.debug(subState.success);
    return subState.data;
  },
);

export {
  makeSelectPilotApplyListSuccess,
  selectPilotApplyListDomain,
  makeSelectPilotApplyList,
};
