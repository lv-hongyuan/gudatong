import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, TouchableOpacity, StatusBar, Platform, Clipboard } from 'react-native';
import { connect } from 'react-redux';
import { Container, View, Text, } from 'native-base';
import _ from 'lodash';
import screenHOC from '../screenHOC';
import Svg from "../../components/Svg";
import { NavigationActions } from "react-navigation";
import myTheme from '../../Themes';
import { iphoneX } from '../../utils/iphoneX-helper';
import AlertView from '../../components/Alert';
const IS_ANDROID = Platform.OS === 'ios' ? false : true;
const styles = StyleSheet.create({
  Line: {
    height: 30,
    justifyContent: 'center',
    paddingLeft: 5,
  },
  bottomTip: {
    textAlign: 'center',
    padding: 8,
    color: '#969696',
  },
});

@screenHOC
class OfflineArrivePortRecordDetail extends React.PureComponent {
  constructor(props) {
    super(props);
    this.data = this.props.navigation.state.params.DATA
    console.log(this.data);
    this.state = {
      shipName: '',
      voyageCode: '',                  //航次号
      line: '',                        //航线
      arrivalPosition: '暂无数据',             //抵港位置
      arrivePortTime: this.data.nextPortEta ? this.data.nextPortEta : '暂无数据',              //抵港时间
      toNextPortDistance: '0',          //剩余航程
      foreDraft: '0',                   //艏吃水
      midshipDraft: '0',                //舯吃水
      aftDraft: '0',                    //艉吃水
      reason: ''                         //修改原因
    };
  }
  copyDetailStr() {
    let detailstr =
      `船名：${this.state.shipName}，
航次：${this.state.voyageCode}，
航线：${this.state.line}，
抵港位置：${this.state.arrivalPosition}，
抵港时间：${this.state.arrivePortTime}，
剩余航程：${this.state.toNextPortDistance} 海里，
抵港艏吃水：${this.state.foreDraft} 米，
抵港舯吃水：${this.state.midshipDraft} 米，
抵港艉吃水：${this.state.aftDraft} 米，
修改原因：${this.state.reason}`
    console.log(detailstr);
    Clipboard.setString(detailstr);
  }

  formatDate(now) {
    var year = now.getFullYear() ? now.getFullYear() : '0000';  //取得4位数的年份
    var month = now.getMonth() + 1 ? now.getMonth() + 1 : '00';  //取得日期中的月份，其中0表示1月，11表示12月
    var date = now.getDate() ? now.getDate() : '00';      //返回日期月份中的天数（1到31）
    var hour = now.getHours() ? now.getHours() : '00';     //返回日期中的小时数（0到23）
    var minute = now.getMinutes() ? now.getMinutes() : '00'; //返回日期中的分钟数（0到59）
    return year + "-" + month + "-" + date + " " + hour + ":" + minute
  }

  getdata() {
    var d=new Date(this.state.arrivePortTime);
    const data = this.data
    this.setState({
      shipName: data.shipName ? data.shipName : '',
      voyageCode: data.shipName ? data.voyageCode : '',                  //航次号
      line: data.lineName ? data.lineName : '',                      //航线
      arrivalPosition: data.exactLocation ? data.exactLocation : '暂无数据',             //抵港位置
      arrivePortTime: this.formatDate(d) ? this.formatDate(d) : '暂无数据',                //抵港时间
      toNextPortDistance: data.toNextPortDistance ? data.toNextPortDistance : '0',           //剩余航程
      foreDraft: data.draughtFrontB ? data.draughtFrontB : '0',                //艏吃水
      midshipDraft: data.draughtMiddleB ? data.draughtMiddleB : '0',             //舯吃水
      aftDraft: data.draughtAfterB ? data.draughtAfterB : '0',               //艉吃水
      reason: data.reason ? data.reason : '',                   //修改原因
    })
  }
  componentDidMount() {
    this.getdata()
  }
  render() {
    return (
      <Container theme={myTheme} style={{ backgroundColor: '#fff' }}>
        <StatusBar
          hidden={false}
          barStyle={'light-content'}
          style={{ backgroundColor: '#DC001B' }}
        />
        <View style={{
          backgroundColor: '#DC001B',
          height: iphoneX ? 88 : IS_ANDROID ? 56 : 64,
          flexDirection: 'row',
          alignItems: 'center',
          paddingHorizontal: 10,
          paddingTop: iphoneX ? 44 : IS_ANDROID ? 0 : 20,
          marginTop: 0,
          justifyContent: 'space-between'
        }}>
          <TouchableOpacity
            onPress={() => { this.props.navigation.dispatch(NavigationActions.back()) }}
            style={{
              width: 40,
              height: 40,
              justifyContent: 'center'
            }}>
            <Svg icon="icon_back" size="20" color="white" />
          </TouchableOpacity>
          <View>
            <Text style={{ color: '#fff', fontSize: 18, fontWeight: '700' }}>{this.data.shipName}</Text>
          </View>
          <TouchableOpacity
            onPress={() => {
              this.copyDetailStr()
              AlertView.show({
                message: '复制成功，可以直接粘贴'
              });
            }}
            style={{
              height: 30,
              justifyContent: 'center'
            }}>
            <Text style={{ color: '#fff', fontSize: 14 }}>复制</Text>
          </TouchableOpacity>
        </View>
        <View style={{ flex: 1, paddingHorizontal: 10, paddingTop: 10 }}>

          <View style={styles.Line}>
            <Text style={{ color: myTheme.inputColor }}>船名：{this.state.shipName}</Text>
          </View>
          <View style={styles.Line}>
            <Text style={{ color: myTheme.inputColor }}>航次：{this.state.voyageCode}</Text>
          </View>
          <View style={styles.Line}>
            <Text style={{ color: myTheme.inputColor }}>航线：{this.state.line}</Text>
          </View>

          <View style={styles.Line}>
            <Text style={{ color: myTheme.inputColor }}>抵港位置：{this.state.arrivalPosition}</Text>
          </View>

          <View style={styles.Line}>
            <Text style={{ color: myTheme.inputColor }}>抵港时间：{this.state.arrivePortTime}</Text>
          </View>

          <View style={styles.Line}>
            <Text style={{ color: myTheme.inputColor }}>剩余航程：{this.state.toNextPortDistance} 海里</Text>
          </View>

          <View style={styles.Line}>
            <Text style={{ color: myTheme.inputColor }}>抵港艏吃水：{this.state.foreDraft} 米</Text>
          </View>

          <View style={styles.Line}>
            <Text style={{ color: myTheme.inputColor }}>抵港舯吃水：{this.state.midshipDraft} 米</Text>
          </View>

          <View style={styles.Line}>
            <Text style={{ color: myTheme.inputColor }}>抵港艉吃水：{this.state.aftDraft} 米</Text>
          </View>

          <View style={styles.Line}>
            <Text style={{ color: myTheme.inputColor }}>修改原因：{this.state.reason}</Text>
          </View>
        </View>
        <Text style={styles.bottomTip}>———— 已经到底了 ————</Text>
      </Container>
    );
  }
}
OfflineArrivePortRecordDetail.navigationOptions = () => ({
  headerLeft: (
    null
  ),
  headerStyle: {
    height: 0,
    marginTop: iphoneX ? -44 : -30,
  },
});
OfflineArrivePortRecordDetail.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default connect()(OfflineArrivePortRecordDetail);
