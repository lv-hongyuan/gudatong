import { createSelector } from 'reselect/es';

const selectLoadingPlanDomain = () => state => state.loadingPlan;

const makeLoadingPlan = () => createSelector(
  selectLoadingPlanDomain(),
  (subState) => {
    console.debug(subState.success);
    return subState.data;
  },
);

export {
  makeLoadingPlan,
};
