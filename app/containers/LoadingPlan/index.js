import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { Container, Content, View, Label, Form, Text } from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { getLoadingPlanPromise } from './actions';
import myTheme from '../../Themes';
import { makeLoadingPlan } from './selectors';
import screenHOC from '../screenHOC';
import commonStyles from '../../common/commonStyles';

const styles = StyleSheet.create({
  row: {
    flex: 1,
    paddingLeft: 15,
    paddingTop: 5,
    paddingBottom: 5,
    paddingRight: 30,
    flexDirection: 'row',
  },
  label: {
    fontSize: 14,
    color: '#535353',
  },
  textName: {
    fontSize: 14,
    color: myTheme.inputColor,
    marginLeft: 10,
    flex: 1,
  },
  text: {
    fontSize: 14,
    color: myTheme.inputColor,
    marginLeft: 10,
    flex: 1,
    textAlign: 'right',
  },
  container: {
    width: '100%',
    backgroundColor: 'white',
    padding: 10,
  },
});

/**
 * 预计装货载量页面
 */
@screenHOC
class LoadingPlan extends React.PureComponent {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.getParam('title', '预计装货载量'),
  });
  static propTypes = {
    getLoadingPlanPromise: PropTypes.func.isRequired,
    data: PropTypes.object,
  };
  static defaultProps = {
    data: {},
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() {
    this.reLoadingPage();
  }

  reLoadingPage = () => {
    requestAnimationFrame(() => {
      this.props.getLoadingPlanPromise()
        .then(() => {
        })
        .catch(() => {
        });
    });
  };

  render() {
    const {
      zwcm, portName,
      voyageCode, eload20gp, fload20gp, eload40gp, fload40gp, loadzl,
      voyageCodeLeave, eunload20gp, funload20gp, eunload40gp, funload40gp, unloadzl,
    } = this.props.data;

    return (
      <Container theme={myTheme}>
        <Content>
          <Form style={{
            backgroundColor: '#FFFFFF',
            padding: 10,
          }}
          >
            <View style={styles.row}>
              <Label style={styles.label}>船名:</Label>
              <Label style={styles.textName}>{zwcm || '--------'}</Label>
            </View>
            <View style={styles.row}>
              <Label style={styles.label}>港口:</Label>
              <Label style={styles.textName}>{portName || '--------'}</Label>
            </View>
          </Form>

          <Form style={{
            marginTop: 20,
            backgroundColor: '#FFFFFF',
            marginBottom: 20,
            paddingBottom: 10,
            padding: 10,
          }}
          >
            <Label style={{
              paddingLeft: 15,
              paddingBottom: 5,
              paddingTop: 5,
              fontSize: 14,
              color: 'red',
            }}
            >装载货量：
            </Label>
            <View style={[styles.row, { borderBottomWidth: myTheme.borderWidth }, { borderColor: '#c9c9c9' }]}>
              <Label style={styles.label}>航次:</Label>
              <Label style={styles.textName}>{voyageCode || '--------'}</Label>
            </View>
            <View style={styles.row}>
              <Label style={styles.label}>20E:</Label>
              <Label style={styles.text}>{eload20gp || 0}</Label>
              <Text style={commonStyles.tail}>箱</Text>
              <Label style={styles.label}>20F:</Label>
              <Label style={styles.text}>{fload20gp || 0}</Label>
              <Text style={commonStyles.tail}>箱</Text>
            </View>
            <View style={[styles.row, { borderBottomWidth: myTheme.borderWidth }, { borderColor: '#c9c9c9' }]}>
              <Label style={styles.label}>40E:</Label>
              <Label style={styles.text}>{eload40gp || 0}</Label>
              <Text style={commonStyles.tail}>箱</Text>
              <Label style={styles.label}>40F:</Label>
              <Label style={styles.text}>{fload40gp || 0}</Label>
              <Text style={commonStyles.tail}>箱</Text>
            </View>
            <View style={styles.row}>
              <Label style={styles.label}>装箱总重:</Label>
              <Label style={styles.text}>{loadzl || 0}</Label>
              <Text style={commonStyles.tail}>吨</Text>
            </View>
          </Form>

          <Form style={{
            backgroundColor: '#FFFFFF',
            marginBottom: 20,
            paddingBottom: 10,
            padding: 10,
          }}
          >
            <Label style={{
              paddingLeft: 15,
              paddingBottom: 5,
              paddingTop: 5,
              fontSize: 14,
              color: 'red',
            }}
            >卸载货量：
            </Label>
            <View style={[styles.row, { borderBottomWidth: myTheme.borderWidth }, { borderColor: '#c9c9c9' }]}>
              <Label style={styles.label}>航次:</Label>
              <Label style={styles.textName}>{voyageCodeLeave || '--------'}</Label>
            </View>
            <View style={styles.row}>
              <Label style={styles.label}>20E:</Label>
              <Label style={styles.text}>{eunload20gp || 0}</Label>
              <Text style={commonStyles.tail}>箱</Text>
              <Label style={styles.label}>20F:</Label>
              <Label style={styles.text}>{funload20gp || 0}</Label>
              <Text style={commonStyles.tail}>箱</Text>
            </View>
            <View style={[styles.row, { borderBottomWidth: myTheme.borderWidth }, { borderColor: '#c9c9c9' }]}>
              <Label style={styles.label}>40E:</Label>
              <Label style={styles.text}>{eunload40gp || 0}</Label>
              <Text style={commonStyles.tail}>箱</Text>
              <Label style={styles.label}>40F:</Label>
              <Label style={styles.text}>{funload40gp || 0}</Label>
              <Text style={commonStyles.tail}>箱</Text>
            </View>
            <View style={styles.row}>
              <Label style={styles.label}>卸箱总重:</Label>
              <Label style={styles.text}>{unloadzl || 0}</Label>
              <Text style={commonStyles.tail}>吨</Text>
            </View>
          </Form>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  data: makeLoadingPlan(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getLoadingPlanPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(LoadingPlan);
