import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_LOADING_PLAN } from './constants';

export const getLoadingPlanRoutine = createRoutine(
  GET_LOADING_PLAN,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const getLoadingPlanPromise = promisifyRoutine(getLoadingPlanRoutine);
