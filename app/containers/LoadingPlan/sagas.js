import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import { getLoadingPlanRoutine } from './actions';
import { errorMessage } from '../ErrorHandler/actions';
import { getPopId } from '../ShipWork/sagas';

function* getLoadingPlan(action) {
  console.log(action);
  try {
    yield put(getLoadingPlanRoutine.request());
    const popId = yield call(getPopId);
    const response = yield call(request, ApiFactory.planLoad(popId));
    console.log('planLoad', response.toString());
    yield put(getLoadingPlanRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(getLoadingPlanRoutine.failure(e));
  } finally {
    yield put(getLoadingPlanRoutine.fulfill());
  }
}

export function* getLoadingPlanSaga() {
  yield takeLatest(getLoadingPlanRoutine.TRIGGER, getLoadingPlan);
}

// All sagas to be loaded
export default [
  getLoadingPlanSaga,
];
