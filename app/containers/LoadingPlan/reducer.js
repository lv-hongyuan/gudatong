import { getLoadingPlanRoutine } from './actions';

const initState = {
  data: undefined,
};

export default function (state = initState, action) {
  switch (action.type) {
    case getLoadingPlanRoutine.TRIGGER:
      return { ...state, loading: true };
    case getLoadingPlanRoutine.SUCCESS:
      return { ...state, data: action.payload };
    case getLoadingPlanRoutine.FAILURE:
      return { ...state, error: action.payload };
    case getLoadingPlanRoutine.FULFILL:
      return { ...state, loading: false };

    default:
      return state;
  }
}
