import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, TouchableOpacity, StatusBar, Platform, Clipboard } from 'react-native';
// import HeaderButtons from "react-navigation-header-buttons";
import { connect } from 'react-redux';
import { Container, Content, View, Text, } from 'native-base';
import _ from 'lodash';
import screenHOC from '../screenHOC';
import Svg from "../../components/Svg";
import { NavigationActions } from "react-navigation";
import myTheme from '../../Themes';
import { iphoneX } from '../../utils/iphoneX-helper';
// import Toast from 'react-native-easy-toast';
import AlertView from '../../components/Alert';
const IS_ANDROID = Platform.OS === 'ios' ? false : true;
const styles = StyleSheet.create({
  item: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  NO1Line: {
    height: 30,
    justifyContent: 'center',
    width: '47%',
    paddingLeft: 5,
  },
  lat: {
    height: 30,
    width: '47%',
    alignItems: 'center',
    paddingLeft: 5,
  },
  bottomTip: {
    textAlign: 'center',
    padding: 8,
    color: '#969696',
  },
});

@screenHOC
class OfflineNavigationRecordDetail extends React.PureComponent {
  constructor(props) {
    super(props);
    this.data = this.props.navigation.state.params.DATA
    console.log(this.data);
    this.state = {
      shipName: '',
      voyageCode: '',          //航次号
      line: '',                //航段
      longD: '',
      longF: '',
      latD: '',
      latF: '',
      windPower: '',           //风力
      windDirection: '',       //风向
      mainEngineSpeed: '',     //主机转速
      aveSpeed: '',            //平均航速
      wave: '',                //海浪
      course: '0',              //航向
      storageShip180: '0',      //180#重油
      storageShip120: '0',      //120#重油
      storageShip0: '0',        //0#轻油
      mainEngineOil: '0',       //主机滑油
      auxiliaryEngineOil: '0',  //辅机滑油
      cylinderOil: '0',         //汽缸油
      storageWater: '0',        //淡水
      maxDraught: '0',          //抵港最大吃水
      toNextPortDistance: '0',  //剩余航程
      nextPortEta: this.data.nextPortEta ? this.data.nextPortEta : '',         //ETA
      memo: '',                 //备注
      notes: '检查集装箱绑扎系固正常',               //检查集装箱绑扎系固正常（默认，可编辑）
    };
  }

  formatDate(now) {
    var year = now.getFullYear() ? now.getFullYear() : '0000';  //取得4位数的年份
    var month = now.getMonth() + 1 ? now.getMonth() + 1 : '00';  //取得日期中的月份，其中0表示1月，11表示12月
    var date = now.getDate() ? now.getDate() : '00';      //返回日期月份中的天数（1到31）
    var hour = now.getHours() ? now.getHours() : '00';     //返回日期中的小时数（0到23）
    var minute = now.getMinutes() ? now.getMinutes() : '00'; //返回日期中的分钟数（0到59）
    return year + "-" + month + "-" + date + " " + hour + ":" + minute
  }

  getData() {
    var d = new Date(this.state.nextPortEta);
    const dataobj = this.data
    switch (dataobj.windPower) {
      case 0:
        WindPower = '无风/0.<1KT';
        break;
      case 1:
        WindPower = '软风/1.1-3KTS';
        break;
      case 2:
        WindPower = '轻风/2.4-6KTS';
        break;
      case 3:
        WindPower = '微风/3.7-10KTS';
        break;
      case 4:
        WindPower = '和风/4.11-16KTS';
        break;
      case 5:
        WindPower = '清劲风/5.17-21KTS';
        break;
      case 6:
        WindPower = '强风/6.22-27KTS';
        break;
      case 7:
        WindPower = '疾风/7.28-33KTS';
        break;
      case 8:
        WindPower = '大风/8.34-40KTS';
        break;
      case 9:
        WindPower = '烈风/9.41-47KTS';
        break;
      case 10:
        WindPower = '狂风/10.48-55KTS';
        break;
      case 11:
        WindPower = '暴风/11.56-63KT';
        break;
      case 12:
        WindPower = '飓风/12.>=64KTS';
        break;
      default:
        break;
    }
    switch (dataobj.wave) {
      case 0:
        Wave = '0级/无浪';
        break;
      case 1:
        Wave = '1级/微浪';
        break;
      case 2:
        Wave = '2级/小浪';
        break;
      case 3:
        Wave = '3级/轻浪';
        break;
      case 4:
        Wave = '4级/中浪';
        break;
      case 5:
        Wave = '5级/大浪';
        break;
      case 6:
        Wave = '6级/巨浪';
        break;
      case 7:
        Wave = '7级/狂浪';
        break;
      case 8:
        Wave = '8级/狂涛';
        break;
      case 9:
        Wave = '9级/怒涛';
        break;
      default:
        break
    }
    this.setState({
      shipName: dataobj.shipName,
      voyageCode: dataobj.voyageCode,
      line: dataobj.line,
      longD: dataobj.longD,
      longF: dataobj.longF,
      latD: dataobj.latD,
      latF: dataobj.latF,
      windPower: WindPower,
      windDirection: dataobj.windDirection,
      mainEngineSpeed: dataobj.mainEngineSpeed ? dataobj.mainEngineSpeed : 0,//主机转速
      aveSpeed: dataobj.aveSpeed ? dataobj.aveSpeed : 0,//平均航速
      wave: Wave,
      course: dataobj.course ? dataobj.course : 0,//航向
      storageShip180: dataobj.storageShip180 ? dataobj.storageShip180 : 0,//180#重油
      storageShip120: dataobj.storageShip120 ? dataobj.storageShip120 : 0,//120#重油
      storageShip0: dataobj.storageShip0 ? dataobj.storageShip0 : 0,//0#重油
      mainEngineOil: dataobj.mainEngineOil ? dataobj.mainEngineOil : 0,//主机滑油
      auxiliaryEngineOil: dataobj.auxiliaryEngineOil ? dataobj.auxiliaryEngineOil : 0,//辅机滑油
      cylinderOil: dataobj.cylinderOil ? dataobj.cylinderOil : 0,//汽缸油
      storageWater: dataobj.storageWater ? dataobj.storageWater : 0,//淡水
      maxDraught: dataobj.maxDraught ? dataobj.maxDraught : 0,//抵港最大吃水
      toNextPortDistance: dataobj.toNextPortDistance ? dataobj.toNextPortDistance : 0,//剩余航程
      nextPortEta: this.formatDate(d) ? this.formatDate(d) : '暂无数据', 
      memo: dataobj.memo ? dataobj.memo : '',
      notes: dataobj.notes ? dataobj.notes : '',
    })
  }

componentDidMount() {
  this.getData()
}
copyDetailStr() {
  let detailstr =
    `船名:${this.state.shipName}，
航次：${this.state.voyageCode}，
航线：${this.state.line}，
纬度：N ${this.state.latD}° ${this.state.latF}'，
经度：E ${this.state.longD}° ${this.state.longF}'，
风向：${this.state.windDirection}，
风力：${this.state.windPower}，
航向：${this.state.course}，
海浪：${this.state.wave}，
主机转速：${this.state.mainEngineSpeed} 转，
平均航速：${this.state.aveSpeed} 节，
180#重油：${this.state.storageShip180} 吨，
120#重油：${this.state.storageShip120} 吨，
0#轻油：${this.state.storageShip0} 吨，
主机滑油：${this.state.mainEngineOil} 吨，
辅机滑油：${this.state.auxiliaryEngineOil} 吨，
汽缸油：${this.state.cylinderOil} 吨，
淡水：${this.state.storageWater} 吨，
抵港最大吃水：${this.state.maxDraught} 米，
剩余航程：${this.state.toNextPortDistance} 海里，
ETA：${this.state.nextPortEta}，
备注： ${this.state.memo ? this.state.memo : ''}，
${this.state.notes ? this.state.notes : ''}`
  console.log(detailstr);
  Clipboard.setString(detailstr);
}
render() {
  return (
    <Container theme={myTheme} style={{ backgroundColor: '#fff' }}>
      <StatusBar
        hidden={false}
        barStyle={'light-content'}
        style={{ backgroundColor: '#DC001B' }}
      />
      <View style={{
        backgroundColor: '#DC001B',
        height: iphoneX ? 88 : IS_ANDROID ? 56 : 64,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 10,
        paddingTop: iphoneX ? 44 : IS_ANDROID ? 0 : 20,
        marginTop: 0,
        justifyContent: 'space-between'
      }}>
        <TouchableOpacity
          onPress={() => { this.props.navigation.dispatch(NavigationActions.back()) }}
          style={{
            width: 40,
            height: 40,
            justifyContent: 'center'
          }}>
          <Svg icon="icon_back" size="20" color="white" />
        </TouchableOpacity>
        <View>
          <Text style={{ color: '#fff', fontSize: 18, fontWeight: '700' }}>{this.data.shipName}</Text>
        </View>
        <TouchableOpacity
          onPress={() => {
            this.copyDetailStr()
            AlertView.show({
              message: '复制成功，可以直接粘贴'
            });
          }}
          style={{
            height: 30,
            justifyContent: 'center'
          }}>
          <Text style={{ color: '#fff', fontSize: 14 }}>复制</Text>
        </TouchableOpacity>
      </View>
      <View style={{ flex: 1, paddingHorizontal: 10, paddingTop: 10 }}>

        <View style={styles.item}>
          <View style={styles.NO1Line}>
            <Text style={{ color: myTheme.inputColor }}>船名：{this.state.shipName}</Text>
          </View>
          <View style={styles.NO1Line}>
            <Text style={{ color: myTheme.inputColor }}>航次：{this.state.voyageCode}</Text>
          </View>
        </View>

        <View style={[styles.NO1Line, { width: '100%' }]}>
          <Text style={{ color: myTheme.inputColor }}>航线：{this.state.line}</Text>
        </View>

        <View style={styles.item}>
          <View style={[{ flexDirection: 'row' }, styles.lat]}>
            <Text style={{ marginRight: 5, color: myTheme.inputColor }}>纬度：N {this.state.latD}&deg;</Text>
            <Text style={{ color: myTheme.inputColor }}>{this.state.latF}'</Text>
          </View>
          <View style={[{ flexDirection: 'row' }, styles.lat]}>
            <Text style={{ marginRight: 5, color: myTheme.inputColor }}>经度：E {this.state.longD}&deg;</Text>
            <Text style={{ color: myTheme.inputColor }}>{this.state.longF}'</Text>
          </View>
        </View>

        <View style={styles.item}>
          <View style={styles.NO1Line}>
            <Text style={{ color: myTheme.inputColor }}>风向：{this.state.windDirection}</Text>
          </View>
          <View style={styles.NO1Line}>
            <Text style={{ color: myTheme.inputColor }}>风力：{this.state.windPower}</Text>
          </View>
        </View>

        <View style={styles.item}>
          <View style={styles.NO1Line}>
            <Text style={{ color: myTheme.inputColor }}>航向：{this.state.course}</Text>
          </View>
          <View style={styles.NO1Line}>
            <Text style={{ color: myTheme.inputColor }}>海浪：{this.state.wave}</Text>
          </View>
        </View>

        <View style={styles.item}>
          <View style={styles.NO1Line}>
            <Text style={{ color: myTheme.inputColor }}>主机转速：{this.state.mainEngineSpeed}  转</Text>
          </View>
          <View style={styles.NO1Line}>
            <Text style={{ color: myTheme.inputColor }}>平均航速：{this.state.aveSpeed}  节</Text>
          </View>
        </View>

        <View style={styles.item}>
          <View style={styles.NO1Line}>
            <Text style={{ color: myTheme.inputColor }}>180#重油：{this.state.storageShip180}  吨</Text>
          </View>
          <View style={styles.NO1Line}>
            <Text style={{ color: myTheme.inputColor }}>120#重油：{this.state.storageShip120}  吨</Text>
          </View>
        </View>

        <View style={styles.item}>
          <View style={styles.NO1Line}>
            <Text style={{ color: myTheme.inputColor }}>0#轻油：{this.state.storageShip0}  吨</Text>
          </View>
          <View style={styles.NO1Line}>
            <Text style={{ color: myTheme.inputColor }}>主机滑油：{this.state.mainEngineOil}  吨</Text>
          </View>
        </View>

        <View style={styles.item}>
          <View style={styles.NO1Line}>
            <Text style={{ color: myTheme.inputColor }}>辅机滑油：{this.state.auxiliaryEngineOil}  吨</Text>
          </View>
          <View style={styles.NO1Line}>
            <Text style={{ color: myTheme.inputColor }}>汽缸油：{this.state.cylinderOil}  吨</Text>
          </View>
        </View>

        <View style={styles.item}>
          <View style={styles.NO1Line}>
            <Text style={{ color: myTheme.inputColor }}>淡水：{this.state.storageWater}  吨</Text>
          </View>
          <View style={styles.NO1Line}>
            <Text style={{ color: myTheme.inputColor }}>抵港最大吃水：{this.state.maxDraught}  米</Text>
          </View>
        </View>

        <View style={[styles.NO1Line, { width: '100%' }]}>
          <Text style={{ color: myTheme.inputColor }}>剩余航程：{this.state.toNextPortDistance}  海里</Text>
        </View>

        <View style={[styles.NO1Line, { width: '100%' }]}>
          <Text style={{ color: myTheme.inputColor }}>ETA：{this.state.nextPortEta}</Text>
        </View>

        <View style={[styles.NO1Line, { width: '100%' }]}>
          <Text style={{ color: myTheme.inputColor }}>备注：{this.state.memo}</Text>
        </View>
        <View style={[styles.NO1Line, { width: '100%' }]}>
          <Text style={{ color: myTheme.inputColor }}>{this.state.notes}</Text>
        </View>
      </View>
      <Text style={styles.bottomTip}>———— 已经到底了 ————</Text>
    </Container>
  );
}
}
OfflineNavigationRecordDetail.navigationOptions = ({ navigation }) => ({
  headerLeft: (
    null
  ),
  headerStyle: {
    height: 0,
    marginTop: iphoneX ? -44 : -30,
  },
});
OfflineNavigationRecordDetail.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default connect()(OfflineNavigationRecordDetail);
