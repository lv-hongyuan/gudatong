import React from 'react';
import PropTypes from 'prop-types';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { Col, Container, Content, Grid, Label, Row, Thumbnail } from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import HeaderButtons from 'react-navigation-header-buttons';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import DeviceInfo from 'react-native-device-info';
// import Toast from 'teaset/components/Toast/Toast';
import Svg from '../../components/Svg';
import { makeLoading } from './selectors';
import { checkUpdate } from '../../common/checkUpdate';
import AlertView from '../../components/Alert';
import myTheme from '../../Themes';
import screenHOC from '../screenHOC';
import {
  ROUTE_CERTIFICATE_MANAGEMENT,
  ROUTE_LOGIN,
  ROUTE_PROBLEM_BACK,
  ROUTE_HELP,
} from '../../RouteConstant';
import { logoutPromiseCreator } from './actions';
import { makeUserInfo } from '../AppInit/selectors';
import NavigatorService from '../../NavigatorService';

const styles = StyleSheet.create({
  userAvatarContainer: {
    backgroundColor: '#DC001B',
    height: 150,
    flexDirection: 'column',
    justifyContent: 'flex-start',
  },
  row: {
    height: 50,
    paddingLeft: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    borderBottomWidth: myTheme.borderWidth,
    borderBottomColor: myTheme.borderColor,
  },
  view: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  label: {
    flex: 1,
    paddingLeft: 20,
  },
  text: {
    fontSize: 14,
    color: '#969696',
  },
});

@screenHOC
class Documents extends React.PureComponent {
  static navigationOptions = {
    title: '资料管理',
    headerStyle: {
      backgroundColor: '#DC001B',
      borderBottomWidth: 0,
      shadowOpacity: 0,
      elevation: 0,
    },
    headerLeft: <HeaderButtons />,
  };
  static propTypes = {
    navigation: PropTypes.object.isRequired,
    logout: PropTypes.func.isRequired,
    userInfo: PropTypes.object.isRequired,
  };

  onBackPressed = () => false;

  confirmLogout() {
    AlertView.show({
      title: '确定要退出登录吗？',
      showCancel: true,
      confirmAction: () => {
        this.props.logout()
          .then(() => {
            NavigatorService.navigate(ROUTE_LOGIN);
          })
          .catch(() => {
          });
      },
    });
  }

  render() {
    return (
      <Container>
        <Content>
          <View style={styles.userAvatarContainer}>
            <Grid style={{
              flex: 1,
            }}
            >
              <Row style={{
                height: '100%',
                justifyContent: 'center',
                alignItems: 'center',
              }}
              >
                {/*<Col style={{*/}
                {/*  height: '100%',*/}
                {/*  flex: 1.5,*/}
                {/*  justifyContent: 'center',*/}
                {/*}}*/}
                {/*>*/}
                {/*  <View style={{*/}
                {/*    justifyContent: 'center',*/}
                {/*    alignItems: 'flex-start',*/}
                {/*    paddingLeft: 20,*/}
                {/*  }}*/}
                {/*  >*/}
                {/*    <Thumbnail*/}
                {/*      large*/}
                {/*      source={{ uri: 'https://upload.wikimedia.org/wikipedia/commons/d/de/Bananavarieties.jpg' }}*/}

                {/*    />*/}
                {/*  </View>*/}
                {/*</Col>*/}
                <Col style={{
                  flex: 2,
                  marginLeft: 15,
                }}
                >
                  <View style={{
                    flexDirection: 'column',
                    height: '100%',
                    justifyContent: 'center',
                    alignItems: 'flex-start',
                  }}
                  >
                    <Text style={{
                      fontSize: 20,
                      fontWeight: '700',
                      color: '#FFFFFF',
                    }}
                    >{this.props.userInfo.userName}
                    </Text>
                    <View style={{
                      marginTop: 5,
                      flexDirection: 'row',
                    }}
                    >
                      <Label style={{
                        color: '#FFFFFF',
                        fontSize: 18,
                      }}
                      >账号:
                      </Label>
                      <Text style={{
                        color: '#FFFFFF',
                        fontSize: 18,
                      }}
                      >{this.props.userInfo.phoneNum}
                      </Text>
                    </View>
                  </View>
                </Col>
              </Row>
            </Grid>
          </View>
          <Grid>
            <Row style={styles.row}>
              <TouchableOpacity
                style={styles.view}
                onPress={() => {
                  this.props.navigation.push(ROUTE_CERTIFICATE_MANAGEMENT);
                }}
              >
                <Svg icon="certificate" size={40} />
                <Label style={styles.label}>证书有效期</Label>
                <Svg icon="right_arrow" size={30} />
              </TouchableOpacity>
            </Row>
            {/* <Row style={[styles.row, { */}
            {/* marginTop: 20 */}
            {/* }]}> */}
            {/* <TouchableOpacity */}
            {/* style={styles.view} */}
            {/* onPress={() => { */}
            {/* Toast.message('敬请期待') */}
            {/* }}> */}
            {/* <Svg icon='about' size={40}/> */}
            {/* <Label style={styles.label}>关于我们</Label> */}
            {/* <Svg icon='right_arrow' size={30}/> */}
            {/* </TouchableOpacity> */}
            {/* </Row> */}
            <Row style={styles.row}>
              <TouchableOpacity
                style={styles.view}
                onPress={() => {
                  checkUpdate(true);
                }}
              >
                <Svg style={{marginLeft: 5}} icon="update" size={28} />
                <Label style={[styles.label, {marginLeft: 7}]}>检查更新</Label>
                <Label style={styles.text}>{DeviceInfo.getVersion()}</Label>
                <Svg icon="right_arrow" size={30} />
              </TouchableOpacity>
            </Row>
            <Row style={styles.row}>
              <TouchableOpacity
                style={styles.view}
                onPress={() => {
                  this.props.navigation.push(ROUTE_PROBLEM_BACK);
                }}
              >
                <Svg icon="feedback" size={40} />
                <Label style={styles.label}>意见反馈</Label>
                <Svg icon="right_arrow" size={30} />
              </TouchableOpacity>
            </Row>
            <Row style={styles.row}>
              <TouchableOpacity
                style={styles.view}
                onPress={() => {
                  this.props.navigation.navigate(ROUTE_HELP);
                }}
              >
                <Svg style={{marginLeft: 5}} icon="getHelp" size={28} />
                <Label style={[styles.label, {marginLeft: 7}]}>获取帮助</Label>
                <Svg icon="right_arrow" size={30} />
              </TouchableOpacity>
            </Row>
            <Row style={[styles.row, { marginTop: 20 }]}>
              <TouchableOpacity
                style={styles.view}
                onPress={() => {
                  this.confirmLogout();
                }}
              >
                <Svg style={{marginLeft: 10}} icon="cancel" size={28} />
                <Label style={[styles.label, {marginLeft: 3}]}>注销</Label>
                <Svg icon="right_arrow" size={30} />
              </TouchableOpacity>
            </Row>
          </Grid>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  isLoading: makeLoading(),
  userInfo: makeUserInfo(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      logout: logoutPromiseCreator,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Documents);
