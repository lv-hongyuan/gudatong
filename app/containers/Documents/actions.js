import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { LOCAL_LOGOUT_ACTION, LOGOUT_ACTION } from './constants';

export const localLogoutRoutine = createRoutine(
  LOCAL_LOGOUT_ACTION,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const logoutRoutine = createRoutine(
  LOGOUT_ACTION,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const logoutPromiseCreator = promisifyRoutine(logoutRoutine);

