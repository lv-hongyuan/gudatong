import { logoutRoutine } from './actions';

const initState = {
  loading: false,
};

export default function (state = initState, action) {
  switch (action.type) {
    // 登出
    case logoutRoutine.TRIGGER:
      return { ...state, loading: true };
    case logoutRoutine.SUCCESS:
      return state;
    case logoutRoutine.FAILURE:
      return { ...state, error: action.payload };
    case logoutRoutine.FULFILL:
      return { ...state, loading: false };

    default:
      return state;
  }
}
