import { createSelector } from 'reselect/es';

const selectDocumentsDomain = () => state => state.documents;

const makeLoading = () =>
  createSelector(selectDocumentsDomain(), (subState) => {
    console.debug(subState.loading);
    return subState.loading;
  });

export {
  makeLoading,
};
