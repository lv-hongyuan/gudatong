import React from 'react';
import PropTypes from 'prop-types';
import { Keyboard, StyleSheet, SafeAreaView, AsyncStorage } from 'react-native';
import { Form, InputGroup, Item, Label, Container, Content, View, Text, Button } from 'native-base';
import _ from 'lodash';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { createStructuredSelector } from 'reselect/es';
import { connect } from 'react-redux';
import commonStyles from '../../common/commonStyles';
import InputItem from '../../components/InputItem/index';
import DatePullSelector from '../../components/DatePullSelector/index';
import SelectImageView from '../../components/SelectImageView/index';
import { updateDynPromise, saveDynPromise, getBerthReportPromise } from '../ShipWork/actions';
import { makeBerthReport, makeLastStateTime, makeSelectPopId, makeSelectState } from '../ShipWork/selectors';
// import { getCurrentLocation } from '../../common/BaiduLocation/LocationManager'
import screenHOC from '../screenHOC';
import {
  weatherList,
  windDirectionList,
  // windPowerList,
  windScaleList,
} from '../../common/Constant';
import Selector from '../../components/Selector';
import AlertView from '../../components/Alert';
import { MaskType } from '../../components/InputItem/TextInput';
import AppStorage from "../../utils/AppStorage";
import StorageKeys from "../../common/StorageKeys";

/**
 * Created by ocean on 2018/4/25
 */

const styles = StyleSheet.create({
  buttonContainer: {
    flexDirection: 'row',
    backgroundColor: '#E0E0E0',
    height: 50,
  },
  saveButton: {
    height: '100%',
    backgroundColor: '#FBB03B',
    borderRadius: 0,
    flex: 1,
    justifyContent: 'center',
  },
  completeButton: {
    height: '100%',
    backgroundColor: '#DC001B',
    borderRadius: 0,
    flex: 2,
    justifyContent: 'center',
  },
  bottomTip: {
    textAlign: 'center',
    padding: 8,
    fontSize: 12,
    color: '#969696',
    marginBottom: 60,
  },
});

@screenHOC
class LandForm extends React.Component {
  static navigationOptions = () => ({
    title: '靠泊登记（靠泊报）',
  });

  constructor(props) {
    super(props);

    this.state = {
      shipName: AppStorage.shipName,  // 船名
      voyageCode: undefined,          // 航次号
      line: undefined,                // 航段
      atb: undefined,                 // 第一桩上缆
      berthPosition: undefined,       // 靠泊位置
      oil180B: '0',             // 180#重油
      oil120B: '0',             // 120#重油
      oil0B: '0',               // 0#轻油
      mainEngineOil: '0',       // 主机滑油
      auxiliaryEngineOil: '0',  // 辅机滑油
      cylinderOil: '0',         // 汽缸油
      waterB: '0',              // 淡水
      tugNum: '0',              // 靠泊拖轮数
      memo: undefined,                // 备注
    };
  }

  componentDidMount() {
    this.props.getBerthReport()
      .then(() => {})
      .catch(() => {});
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.berthReport !== this.props.berthReport) {
      this.setState({
        ...nextProps.berthReport,
      });
    }
  }

  handleSave = () => {
    Keyboard.dismiss();

    const {
      shipName, voyageCode, line, atb, berthPosition, oil180B, oil120B, oil0B, mainEngineOil,
      auxiliaryEngineOil, cylinderOil, waterB, tugNum, memo,
    } = this.state;

    const param = {
      popId: this.props.popId,
      state: this.props.state,
      time: atb,
      berth: {
        shipName,
        voyageCode,
        line,
        atb,
        berthPosition,
        oil180B,
        oil120B,
        oil0B,
        mainEngineOil,
        auxiliaryEngineOil,
        cylinderOil,
        waterB,
        tugNum,
        memo,
      },
    };
    this.props.saveDyn(param)
      .then(() => {
        this.props.navigation.goBack();
      }).catch(() => {});
  };

  handleSubmit = () => {
    Keyboard.dismiss();

    const {
      shipName, voyageCode, line, atb, berthPosition, oil180B, oil120B, oil0B, mainEngineOil,
      auxiliaryEngineOil, cylinderOil, waterB, tugNum, memo,
    } = this.state;

    if (!atb) {
      AlertView.show({ message: '请选择第一缆上桩时间' });
      return;
    }
    if (_.isNil(berthPosition)) {
      AlertView.show({ message: '请输入靠泊位置' });
      return;
    }
    // if (_.isNil(oil180B) || parseFloat(oil180B) === 0) {
    //   AlertView.show({ message: '请输入现存180#' });
    //   return;
    // }
    // if (_.isNil(oil120B) || parseFloat(oil120B) === 0) {
    //   AlertView.show({ message: '请输入现存120#' });
    //   return;
    // }
    // if (_.isNil(oil0B) || parseFloat(oil0B) === 0) {
    //   AlertView.show({ message: '请输入写现存0#' });
    //   return;
    // }
    // if (_.isNil(mainEngineOil) || parseFloat(mainEngineOil) === 0) {
    //   AlertView.show({ message: '请输入现存主机滑油' });
    //   return;
    // }
    // if (_.isNil(auxiliaryEngineOil) || parseFloat(auxiliaryEngineOil) === 0) {
    //   AlertView.show({ message: '请输入现存辅机滑油' });
    //   return;
    // }
    // if (_.isNil(cylinderOil) || parseFloat(cylinderOil) === 0) {
    //   AlertView.show({ message: '请输入现存汽缸油' });
    //   return;
    // }
    // if (_.isNil(waterB) || parseFloat(waterB) === 0) {
    //   AlertView.show({ message: '请输入现存淡水' });
    //   return;
    // }

    const param = {
      popId: this.props.popId,
      state: this.props.state,
      time: atb,
      berth: {
        shipName,
        voyageCode,
        line,
        atb,
        berthPosition,
        oil180B,
        oil120B,
        oil0B,
        mainEngineOil,
        auxiliaryEngineOil,
        cylinderOil,
        waterB,
        tugNum,
        memo,
      },
    };
    // getCurrentLocation(5000).then(({latitude, longitude}) => {
    //   param.berth.latD = Math.abs(parseInt(latitude))
    //   param.berth.latF = Math.abs((latitude - parseInt(latitude)) * 60)
    //   param.berth.latN = latitude > 0 ? 1 : 0
    //   param.berth.lonD = Math.abs(parseInt(longitude))
    //   param.berth.lonF = Math.abs((longitude - parseInt(longitude)) * 60)
    //   param.berth.lonN = longitude > 0 ? 1 : 0
    //
    //   this.props.updateDynPromise(param)
    //     .then(() => {
    //       this.props.navigation.goBack()
    //     }).catch(() => {})
    // }).catch(() => {
    this.props.updateDyn(param)
      .then(() => {
        const goBackCallback = this.props.navigation.getParam('onGoBack', undefined);
        if (goBackCallback) {
          goBackCallback();
        }
        this.props.navigation.goBack();
      }).catch(() => {});
    // })
  };

  makeTimeValueToShow() {
    let timeValueToShow = null;
    if (this.props.lastStateTime) {
      const suitableTime = this.props.lastStateTime + 1800000; // 增加半小时
      const currentTime = new Date().getTime(); // 当前时间
      if (suitableTime < currentTime) {
        timeValueToShow = suitableTime;
      }
    }
    return timeValueToShow;
  }

  render() {
    return (
      <Container>
        <Content contentInsetAdjustmentBehavior="scrollableAxes">
          <Form style={{ backgroundColor: '#FFFFFF' }}>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                  label="船名:"
                  value={this.state.shipName}
                  placeholder=""
                  editable={false}
              />
              <InputItem
                  label="航次:"
                  value={this.state.voyageCode}
                  placeholder=""
                  editable={false}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                  label="航线:"
                  value={this.state.line}
                  placeholder=""
                  editable={false}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={{ color: 'red' }}>*</Label>
                <Label style={commonStyles.inputLabel}>第一缆上桩:</Label>
                <DatePullSelector
                  defaultValue={this.makeTimeValueToShow()}
                  value={this.state.atb || null}
                  onChangeValue={(value) => {
                    this.setState({ atb: value });
                  }}
                />
              </Item>
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                  isRequired
                  label="靠泊位置:"
                  value={this.state.berthPosition}
                  onChangeText={(text) => {
                    this.setState({ berthPosition: text });
                  }}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                  isRequired
                  label="180#重油:"
                  value={this.state.oil180B}
                  onChangeText={(text) => {
                    this.setState({ oil180B: text });
                  }}
                  placeholder='0'
                  keyboardType="numeric"
                  maskType={MaskType.FLOAT}
                  rightItem={<Text style={commonStyles.tail}>吨</Text>}
              />
              <InputItem
                  isRequired
                  label="120#重油:"
                  value={this.state.oil120B}
                  onChangeText={(text) => {
                    this.setState({ oil120B: text });
                  }}
                  placeholder='0'
                  keyboardType="numeric"
                  maskType={MaskType.FLOAT}
                  rightItem={<Text style={commonStyles.tail}>吨</Text>}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                  isRequired
                  label="0#轻油:"
                  value={this.state.oil0B}
                  onChangeText={(text) => {
                    this.setState({ oil0B: text });
                  }}
                  placeholder='0'
                  keyboardType="numeric"
                  maskType={MaskType.FLOAT}
                  rightItem={<Text style={commonStyles.tail}>吨</Text>}
              />
              <InputItem
                  isRequired
                  label="主机滑油:"
                  value={this.state.mainEngineOil}
                  onChangeText={(text) => {
                    this.setState({ mainEngineOil: text });
                  }}
                  placeholder='0'
                  keyboardType="numeric"
                  maskType={MaskType.FLOAT}
                  rightItem={<Text style={commonStyles.tail}>吨</Text>}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                  isRequired
                  label="辅机滑油:"
                  value={this.state.auxiliaryEngineOil}
                  onChangeText={(text) => {
                    this.setState({ auxiliaryEngineOil: text });
                  }}
                  placeholder='0'
                  keyboardType="numeric"
                  maskType={MaskType.FLOAT}
                  rightItem={<Text style={commonStyles.tail}>吨</Text>}
              />
              <InputItem
                  isRequired
                  label="汽缸油:"
                  value={this.state.cylinderOil}
                  onChangeText={(text) => {
                    this.setState({ cylinderOil: text });
                  }}
                  placeholder='0'
                  keyboardType="numeric"
                  maskType={MaskType.FLOAT}
                  rightItem={<Text style={commonStyles.tail}>吨</Text>}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                  isRequired
                  label="淡水:"
                  value={this.state.waterB}
                  onChangeText={(text) => {
                    this.setState({ waterB: text });
                  }}
                  placeholder='0'
                  keyboardType="numeric"
                  maskType={MaskType.FLOAT}
                  rightItem={<Text style={commonStyles.tail}>吨</Text>}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                isRequired
                label="靠泊拖轮数:"
                placeholder="0"
                value={this.state.tugNum}
                onChangeText={(text) => {
                  this.setState({ tugNum: text });
                }}
                keyboardType="numeric"
                maskType={MaskType.INTEGER}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                  label="备注:"
                  placeholder="无"
                  value={this.state.memo}
                  onChangeText={(text) => {
                    this.setState({ memo: text });
                  }}
              />
            </InputGroup>
            {/*<InputGroup style={[commonStyles.inputGroup, {*/}
            {/*  height: 100,*/}
            {/*  justifyContent: 'flex-start',*/}
            {/*  marginBottom: 20,*/}
            {/*}]}*/}
            {/*>*/}
            {/*  <Item style={[commonStyles.inputItem, {*/}
            {/*    justifyContent: 'flex-start',*/}
            {/*    height: 100,*/}
            {/*    borderBottomWidth: 0,*/}
            {/*  }]}*/}
            {/*  >*/}
            {/*    <Label style={commonStyles.inputLabel}>靠泊照片:</Label>*/}
            {/*    <SelectImageView*/}
            {/*      source={this.state.berthPic}*/}
            {/*      onPickImages={(images) => {*/}
            {/*        const image = images[0];*/}
            {/*        this.setState({*/}
            {/*          berthPic: {*/}
            {/*            fileName: image.uri.split('/').reverse()[0],*/}
            {/*            fileSize: image.size,*/}
            {/*            path: image.uri,*/}
            {/*            uri: image.uri,*/}
            {/*          },*/}
            {/*        });*/}
            {/*      }}*/}
            {/*      onDelete={() => {*/}
            {/*        this.setState({ berthPic: null });*/}
            {/*      }}*/}
            {/*    />*/}
            {/*  </Item>*/}
            {/*</InputGroup>*/}
          </Form>
          <Text style={styles.bottomTip}>———— 已经到底了 ————</Text>
        </Content>
        <SafeAreaView style={{
          position: 'absolute', bottom: 0, left: 0, right: 0, backgroundColor: '#ffffff',
        }}
        >
          <View style={styles.buttonContainer}>
            <Button onPress={this.handleSave} style={styles.saveButton}>
              <Text>暂存</Text>
            </Button>
            <Button onPress={this.handleSubmit} style={styles.completeButton}>
              <Text>提交</Text>
            </Button>
          </View>
        </SafeAreaView>
      </Container>
    );
  }
}

LandForm.propTypes = {
  popId: PropTypes.number,
  state: PropTypes.number,
  berthReport: PropTypes.object,
  getBerthReport: PropTypes.func.isRequired,
  saveDyn: PropTypes.func.isRequired,
  updateDyn: PropTypes.func.isRequired,
  navigation: PropTypes.object.isRequired,
  lastStateTime: PropTypes.number,
  isConnected: PropTypes.bool.isRequired,
};

LandForm.defaultProps = {
  popId: undefined,
  state: undefined,
  berthReport: {},
  lastStateTime: undefined,
};

const mapStateToProps = createStructuredSelector({
  popId: makeSelectPopId(),
  state: makeSelectState(),
  berthReport: makeBerthReport(),
  lastStateTime: makeLastStateTime(),
  isConnected: state => state.network.isConnected,
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getBerthReport: getBerthReportPromise,
      saveDyn: saveDynPromise,
      updateDyn: updateDynPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(LandForm);
