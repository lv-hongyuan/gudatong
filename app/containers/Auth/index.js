import React from 'react';
import PropTypes from 'prop-types';
import { Container, View } from 'native-base';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect/es';
import { AsyncStorage, DeviceEventEmitter } from 'react-native';
import StorageKeys from '../../common/StorageKeys';
import { ROUTE_APPLICATION, ROUTE_AUTH, ROUTE_OFFLINE } from '../../RouteConstant';
import myTheme, { deviceHeight } from '../../Themes';
import Svg from '../../components/Svg';
import screenHOC from '../screenHOC';
import { READY_TO_HANDLE_LAUNCH_NOTIFICATION } from '../../components/NotificationCenter/constants';
import { makeUserInfo } from '../AppInit/selectors';

@screenHOC
class Auth extends React.Component {
  componentDidMount() {
    if (this.props.user && this.props.user.autPass === 30) {
      AsyncStorage.getItem(StorageKeys.IS_SAIL)
        .then((isSail) => {
          // 航行状态时切换
          if (isSail === '0' && !this.props.isConnected) {
            this.props.navigation.navigate(ROUTE_OFFLINE);
          } else {
            this.props.navigation.navigate(ROUTE_APPLICATION);
          }
        })
        .catch(() => {
          this.props.navigation.navigate(ROUTE_APPLICATION);
        });
    } else {
      this.props.navigation.navigate(ROUTE_AUTH);
    }
    DeviceEventEmitter.emit(READY_TO_HANDLE_LAUNCH_NOTIFICATION);
  }

  render() {
    return (
      <Container>
        <View style={{
          backgroundColor: myTheme.primaryColor,
          height: deviceHeight,
          flexDirection: 'column',
          justifyContent: 'flex-end',
          alignItems: 'center',
        }}
        >

          <View style={{
            flex: 1,
            width: 10,
          }}
          />
          <Svg
            icon="onLand_logo"
            width={160}
            height={120}
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              alignContent: 'center',
              marginBottom: 140,
            }}
            color="white"
          />

        </View>
      </Container>
    );
  }
}

Auth.navigationOptions = {
  header: null,
};

Auth.propTypes = {
  navigation: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  isConnected: PropTypes.bool.isRequired,
};

const mapStateToProps = createStructuredSelector({
  user: makeUserInfo(),
  isConnected: state => state.network.isConnected,
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Auth);
