import React from 'react';
import PropTypes from 'prop-types';
import {Keyboard, StyleSheet, SafeAreaView, AsyncStorage} from 'react-native';
import { InputGroup, Item, Label, Container, Content, Form, Text, View, Button } from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import { connect } from 'react-redux';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import _ from 'lodash';
import commonStyles from '../../common/commonStyles';
import InputItem from '../../components/InputItem/index';
import DatePullSelector from '../../components/DatePullSelector/index';
import { updateDynPromise, saveDynPromise, getReachReportPromise } from '../ShipWork/actions';
import { makeLastStateTime, makeReachReport, makeSelectPopId, makeSelectState } from '../ShipWork/selectors';
import screenHOC from '../screenHOC';
import CoordinateInput from '../../components/CoordinateInput';
import {
  // surgeLevelList,
  waveList,
  weatherList,
  windDirectionList,
  // windPowerList,
  windScaleList,
} from '../../common/Constant';
import Selector from '../../components/Selector';
import Switch from '../../components/Switch';
import AlertView from '../../components/Alert';
import { MaskType } from '../../components/InputItem/TextInput';
import AppStorage from "../../utils/AppStorage";
import StorageKeys from "../../common/StorageKeys";

/**
 * Created by ocean on 2018/4/25
 */

const styles = StyleSheet.create({
  buttonContainer: {
    flexDirection: 'row',
    backgroundColor: '#E0E0E0',
    height: 50,
  },
  saveButton: {
    height: '100%',
    backgroundColor: '#FBB03B',
    borderRadius: 0,
    flex: 1,
    justifyContent: 'center',
  },
  completeButton: {
    height: '100%',
    backgroundColor: '#DC001B',
    borderRadius: 0,
    flex: 2,
    justifyContent: 'center',
  },
  bottomTip: {
    textAlign: 'center',
    padding: 8,
    fontSize: 12,
    color: '#969696',
    marginBottom: 60,
  },
});

@screenHOC
class DynFormAnchor extends React.Component {
  static navigationOptions = () => ({
    title: '下锚登记（抵港报）',
  });

  constructor(props) {
    super(props);

    this.state = {
      isOn: false,
      shipName: AppStorage.shipName,  // 船名
      voyageCode: undefined,          // 航次号
      line: undefined,                // 航段
      time: undefined,                // 抛锚时间
      anchorPosition: undefined,      // 锚位
      latD: undefined,                // 经度
      latF: undefined,
      latN: 1,
      lonD: undefined,                // 经度
      lonF: undefined,
      lonN: 1,
      oil180B: '0',             // 180#重油
      oil120B: '0',             // 120#重油
      oil0B: '0',               // 0#轻油
      mainEngineOil: '0',       // 主机滑油
      auxiliaryEngineOil: '0',  // 辅机滑油
      cylinderOil: '0',         // 汽缸油
      waterB: '0',              // 淡水
      notes: '锚泊等待进港动态',               // 锚泊等待进港动态
      planWeighAnchor: undefined,     // 计划起锚时间
      planAnchorBerth: undefined,     // 计划靠泊泊位
      memo: undefined,                // 备注
    };
  }

  componentDidMount() {
    this.props.getReachReport()
      .then(() => {})
      .catch(() => {});
    // if (!this.props.isConnected) {
    //   AsyncStorage.getItem(StorageKeys.DYN)
    //       .then((str) => {
    //         if (str) {
    //           const dyn = JSON.parse(str);
    //           this.setState({
    //             line: this.state.line || dyn.lineName,
    //             voyageCode: this.state.voyageCode || dyn.voyageCode,
    //           });
    //         }
    //       })
    //       .catch(() => {});
    // }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.reachReport !== this.props.reachReport) {
      // const data = {};
      // Object.keys(nextProps.reachReport).forEach((key) => {
      //   let value;
      //   switch (key) {
      //     case 'shipName':
      //       value = nextProps.reachReport[key] || AppStorage.shipName;
      //       break;
      //     case 'voyageCode':
      //       value = nextProps.reachReport[key] || this.state.voyageCode;
      //       break;
      //     case 'line':
      //       value = nextProps.reachReport[key] || this.state.line;
      //       break;
      //     default:
      //       value = nextProps.reachReport[key];
      //       break;
      //   }
      //   data[key] = value === 0 ? null : value;
      // });
      this.setState({
        ...nextProps.reachReport,
        notes: nextProps.reachReport.notes || "锚泊等待进港动态",
      });
    }
  }

  handleSave = () => {
    Keyboard.dismiss();
    const {
      shipName, voyageCode, line, time, anchorPosition, latD, latF, latN, lonD, lonF, lonN,
      oil180B, oil120B, oil0B, mainEngineOil, auxiliaryEngineOil, cylinderOil, waterB, notes,
      planWeighAnchor, planAnchorBerth, memo,
    } = this.state;
    const param = {
      popId: this.props.popId,
      state: this.props.state,
      time: time,
      arrival: {
        shipName,
        voyageCode,
        line,
        time, // 抛锚时间
        anchorPosition,
        latD,
        latF,
        latN,
        lonD,
        lonF,
        lonN,
        oil180B,
        oil120B,
        oil0B,
        mainEngineOil,
        auxiliaryEngineOil,
        cylinderOil,
        waterB,
        notes,
        planWeighAnchor,
        planAnchorBerth,
        memo,
      },
    };
    this.props.saveDyn(param)
      .then(() => {
        this.props.navigation.goBack();
      })
      .catch(() => {});
  };

  handleSubmit = () => {
    Keyboard.dismiss();

    if (this.state.isOn) {
      const {
        shipName, voyageCode, line, time, anchorPosition, latD, latF, latN, lonD, lonF, lonN,
        oil180B, oil120B, oil0B, mainEngineOil, auxiliaryEngineOil, cylinderOil, waterB, notes,
        planWeighAnchor, planAnchorBerth, memo,
      } = this.state;

      if (!time) {
        AlertView.show({ message: '请选择抛锚时间' });
        return;
      }
      if (_.isNil(anchorPosition)) {
        AlertView.show({ message: '请输入锚位' });
        return;
      }
      if (_.isNil(lonD) || lonD == '') {
        AlertView.show({ message: '请输入经度' });
        return;
      }
      if (_.isNil(latD) || latD == '') {
        AlertView.show({ message: '请输入纬度' });
        return;
      }
      if((latD + latF / 60 > 90) || (latF >= 60)){
        AlertView.show({ message: '纬度输入有误！' });
        return;
      }
      if((lonD + lonF / 60 > 180) || (lonF >= 60)){
        AlertView.show({ message: '经度输入有误！'});
        return;
      }
      if (_.isNil(planWeighAnchor) || parseFloat(planWeighAnchor) == 0) {
        AlertView.show({ message: '请选择计划起锚时间' });
        return;
      }
      if (this.state.planWeighAnchor && this.state.planWeighAnchor < this.state.time) {
        AlertView.show({ message: '计划起锚时间不能早于抛锚时间' });
        return;
      }
      if (_.isNil(planAnchorBerth)) {
        AlertView.show({ message: '请输入计划靠泊泊位' });
        return;
      }    
      function doSubmit() {
        const param = {
          popId: this.props.popId,
          state: this.props.state,
          time: time,
          arrival: {
            shipName,
            voyageCode,
            line,
            time, // 抛锚时间
            anchorPosition,
            latD,
            latF,
            latN,
            lonD,
            lonF,
            lonN,
            oil180B,
            oil120B,
            oil0B,
            mainEngineOil,
            auxiliaryEngineOil,
            cylinderOil,
            waterB,
            notes,
            planWeighAnchor,
            planAnchorBerth,
            memo,
          },
        };
        this.props.updateDyn(param)
          .then(() => {
            const goBackCallback = this.props.navigation.getParam('onGoBack', undefined);
            if (goBackCallback) {
              goBackCallback();
            }
            this.props.navigation.goBack();
          })
          .catch(() => {});
      }
      AlertView.show({
        message: '确定提交吗？',
        showCancel: true,
        confirmAction: () => {
          doSubmit.bind(this)();
        },
      });
    } else {
      AlertView.show({
        message: '确定不下锚吗？',
        showCancel: true,
        confirmAction: () => {
          this.props.updateDyn({
            popId: this.props.popId,
            state: this.props.state,
            time: 0,
          })
            .then(() => {
              const goBackCallback = this.props.navigation.getParam('onGoBack', undefined);
              if (goBackCallback) {
                goBackCallback();
              }
              this.props.navigation.goBack();
            })
            .catch(() => {});
        },
      });
    }
  };

  makeTimeValueToShow() {
    let timeValueToShow = null;
    if (this.props.lastStateTime) {
      const suitableTime = this.props.lastStateTime + 1800000; // 增加半小时
      const currentTime = new Date().getTime(); // 当前时间
      if (suitableTime < currentTime) {
        timeValueToShow = suitableTime;
      }
    }
    return timeValueToShow;
  }

  render() {
    return (
      <Container>
        <Content contentInsetAdjustmentBehavior="scrollableAxes">
          <View style={{ backgroundColor: '#FFFFFF' }}>
            <InputGroup style={commonStyles.inputGroup}>
              <Item style={[commonStyles.inputItem, { borderBottomWidth: 0 }]}>
                <Label style={commonStyles.inputLabel}>是否下锚</Label>
                <Switch
                  onPress={() => {
                    this.setState({ isOn: !this.state.isOn });
                  }}
                  value={this.state.isOn}
                />
              </Item>
            </InputGroup>
          </View>
          {this.state.isOn && (
            <Form style={{ backgroundColor: '#FFFFFF' }}>
              <InputGroup style={commonStyles.inputGroup}>
                <InputItem
                    label="船名:"
                    value={this.state.shipName}
                    placeholder=""
                    editable={false}
                />
                <InputItem
                    label="航次:"
                    value={this.state.voyageCode}
                    placeholder=""
                    editable={false}
                />
              </InputGroup>
              <InputGroup style={commonStyles.inputGroup}>
                <InputItem
                    label="航线:"
                    value={this.state.line}
                    placeholder=""
                    editable={false}
                />
              </InputGroup>
              <InputGroup style={commonStyles.inputGroup}>
                <Item style={commonStyles.inputItem}>
                  <Label style={{ color: 'red' }}>*</Label>
                  <Label style={commonStyles.inputLabel}>抛锚时间:</Label>
                  <DatePullSelector
                      defaultValue={this.makeTimeValueToShow()}
                      value={this.state.time || null}
                      onChangeValue={(value) => {
                        this.setState({ time: value });
                      }}
                  />
                </Item>
              </InputGroup>
              <InputGroup style={commonStyles.inputGroup}>
                <InputItem
                    isRequired
                    label="锚位:"
                    value={this.state.anchorPosition}
                    onChangeText={(text) => {
                      this.setState({ anchorPosition: text });
                    }}
                />
              </InputGroup>
              <InputGroup style={commonStyles.inputGroup}>
                <Item style={commonStyles.inputItem}>
                  <Label style={{ color: 'red' }}>*</Label>
                  <Label style={commonStyles.inputLabel}>纬度:</Label>
                  <CoordinateInput
                      type="la"
                      degrees={this.state.latD}
                      points={this.state.latF}
                      onChangeValue={(degrees, points) => {
                        this.setState({ latD: Math.abs(degrees), latF: Math.abs(points), latN: degrees > 0 ? 1 : 0 });
                      }}
                  />
                </Item>
                <Item style={commonStyles.inputItem}>
                  <Label style={{ color: 'red' }}>*</Label>
                  <Label style={commonStyles.inputLabel}>经度:</Label>
                  <CoordinateInput
                      type="lo"
                      degrees={this.state.lonD}
                      points={this.state.lonF}
                      onChangeValue={(degrees, points) => {
                        this.setState({ lonD: Math.abs(degrees), lonF: Math.abs(points), lonN: degrees > 0 ? 1 : 0 });
                      }}
                  />
                </Item>
              </InputGroup>
              <InputGroup style={commonStyles.inputGroup}>
                <InputItem
                    isRequired
                    label="180#重油:"
                    value={this.state.oil180B}
                    onChangeText={(text) => {
                      this.setState({ oil180B: text });
                    }}
                    placeholder='0'
                    keyboardType="numeric"
                    maskType={MaskType.FLOAT}
                    rightItem={<Text style={commonStyles.tail}>吨</Text>}
                />
                <InputItem
                    isRequired
                    label="120#重油:"
                    value={this.state.oil120B}
                    onChangeText={(text) => {
                      this.setState({ oil120B: text });
                    }}
                    placeholder='0'
                    keyboardType="numeric"
                    maskType={MaskType.FLOAT}
                    rightItem={<Text style={commonStyles.tail}>吨</Text>}
                />
              </InputGroup>
              <InputGroup style={commonStyles.inputGroup}>
                <InputItem
                    isRequired
                    label="0#轻油:"
                    value={this.state.oil0B}
                    onChangeText={(text) => {
                      this.setState({ oil0B: text });
                    }}
                    placeholder='0'
                    keyboardType="numeric"
                    maskType={MaskType.FLOAT}
                    rightItem={<Text style={commonStyles.tail}>吨</Text>}
                />
                <InputItem
                    isRequired
                    label="主机滑油:"
                    value={this.state.mainEngineOil}
                    onChangeText={(text) => {
                      this.setState({ mainEngineOil: text });
                    }}
                    placeholder='0'
                    keyboardType="numeric"
                    maskType={MaskType.FLOAT}
                    rightItem={<Text style={commonStyles.tail}>吨</Text>}
                />
              </InputGroup>
              <InputGroup style={commonStyles.inputGroup}>
                <InputItem
                    isRequired
                    label="辅机滑油:"
                    value={this.state.auxiliaryEngineOil}
                    onChangeText={(text) => {
                      this.setState({ auxiliaryEngineOil: text });
                    }}
                    placeholder='0'
                    keyboardType="numeric"
                    maskType={MaskType.FLOAT}
                    rightItem={<Text style={commonStyles.tail}>吨</Text>}
                />
                <InputItem
                    isRequired
                    label="汽缸油:"
                    value={this.state.cylinderOil}
                    onChangeText={(text) => {
                      this.setState({ cylinderOil: text });
                    }}
                    placeholder='0'
                    keyboardType="numeric"
                    maskType={MaskType.FLOAT}
                    rightItem={<Text style={commonStyles.tail}>吨</Text>}
                />
              </InputGroup>
              <InputGroup style={commonStyles.inputGroup}>
                <InputItem
                    isRequired
                    label="淡水:"
                    value={this.state.waterB}
                    onChangeText={(text) => {
                      this.setState({ waterB: text });
                    }}
                    placeholder='0'
                    keyboardType="numeric"
                    maskType={MaskType.FLOAT}
                    rightItem={<Text style={commonStyles.tail}>吨</Text>}
                />
              </InputGroup>
              <InputGroup style={commonStyles.inputGroup}>
                <Item style={commonStyles.inputItem}>
                  <Label style={{ color: 'red' }}>*</Label>
                  <Label style={commonStyles.inputLabel}>计划起锚时间:</Label>
                  <DatePullSelector
                      defaultValue={this.makeTimeValueToShow()}
                      value={this.state.planWeighAnchor || null}
                      minValue={this.state.time}
                      onChangeValue={(value) => {
                        this.setState({ planWeighAnchor: value });
                      }}
                  />
                </Item>
              </InputGroup>
              <InputGroup style={commonStyles.inputGroup}>
                <InputItem
                    isRequired
                    label="计划靠泊泊位:"
                    value={this.state.planAnchorBerth}
                    onChangeText={(text) => {
                      this.setState({ planAnchorBerth: text });
                    }}
                />
              </InputGroup>
              <InputGroup style={commonStyles.inputGroup}>
                <InputItem
                    // placeholder="锚泊等待进港动态"
                    placeholder = ""
                    value={this.state.notes}
                    onChangeText={(text) => {
                      this.setState({ notes: text });
                    }}
                />
              </InputGroup>
              <InputGroup style={commonStyles.inputGroup}>
                <InputItem
                  label="备注:"
                  placeholder="无"
                  value={this.state.memo}
                  onChangeText={(text) => {
                    this.setState({ memo: text });
                  }}
                />
              </InputGroup>
            </Form>
          )}
          {this.state.isOn && (
            <Text style={styles.bottomTip}>———— 已经到底了 ————</Text>
          )}
        </Content>
        <SafeAreaView style={{
          position: 'absolute', bottom: 0, left: 0, right: 0, backgroundColor: '#ffffff',
        }}
        >
          <View style={styles.buttonContainer}>
            <Button onPress={this.handleSave} style={styles.saveButton}>
              <Text>暂存</Text>
            </Button>
            <Button onPress={this.handleSubmit} style={styles.completeButton}>
              <Text>提交</Text>
            </Button>
          </View>
        </SafeAreaView>
      </Container>
    );
  }
}

DynFormAnchor.propTypes = {
  popId: PropTypes.number,
  state: PropTypes.number,
  reachReport: PropTypes.object,
  getReachReport: PropTypes.func.isRequired,
  saveDyn: PropTypes.func.isRequired,
  updateDyn: PropTypes.func.isRequired,
  navigation: PropTypes.object.isRequired,
  lastStateTime: PropTypes.number,
  isConnected: PropTypes.bool.isRequired,
};

DynFormAnchor.defaultProps = {
  popId: undefined,
  state: undefined,
  reachReport: {},
  lastStateTime: undefined,
};

const mapStateToProps = createStructuredSelector({
  popId: makeSelectPopId(),
  state: makeSelectState(),
  reachReport: makeReachReport(),
  lastStateTime: makeLastStateTime(),
  isConnected: state => state.network.isConnected,
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getReachReport: getReachReportPromise,
      saveDyn: saveDynPromise,
      updateDyn: updateDynPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(DynFormAnchor);
