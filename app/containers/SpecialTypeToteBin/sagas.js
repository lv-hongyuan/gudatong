import { call, put, takeLatest } from 'redux-saga/effects';
import { GET_SPECIAL_TYPE_TOTE_BIN_LIST_ACTION, DELETE_SPECIAL_TYPE_TOTE_BIN_ACTION } from './constants';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import { getSpecialTypeToteBinListResultAction, deleteSpecialTypeToteBinResultAction } from './actions';
import { errorMessage } from '../ErrorHandler/actions';
import { getPopId } from '../ShipWork/sagas';

function* getSpecialTypeToteBinList(action) {
  console.log(action);
  try {
    const popId = yield call(getPopId);
    const response = yield call(request, ApiFactory.findSpecialCont(popId));
    yield put(getSpecialTypeToteBinListResultAction(response, true));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(getSpecialTypeToteBinListResultAction(undefined, false));
  }
}

export function* getSpecialTypeToteBinListSaga() {
  yield takeLatest(GET_SPECIAL_TYPE_TOTE_BIN_LIST_ACTION, getSpecialTypeToteBinList);
}

function* deleteSpecialTypeToteBin(action) {
  console.log(action);
  try {
    const response = yield call(request, ApiFactory.deleteSpecialCont(action.payload));
    yield put(deleteSpecialTypeToteBinResultAction(response, true));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(deleteSpecialTypeToteBinResultAction(undefined, false));
  }
}

export function* deleteSpecialTypeToteBinSaga() {
  yield takeLatest(DELETE_SPECIAL_TYPE_TOTE_BIN_ACTION, deleteSpecialTypeToteBin);
}

// All sagas to be loaded
export default [
  getSpecialTypeToteBinListSaga,
  deleteSpecialTypeToteBinSaga,
];
