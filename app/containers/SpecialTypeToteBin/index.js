import React from 'react';
import PropTypes from 'prop-types';
import { FlatList, RefreshControl } from 'react-native';
import { connect } from 'react-redux';
import { Container, View } from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import HeaderButtons from 'react-navigation-header-buttons';
import SpecialTypeToteBinItem from '../../components/SpecialTypeToteBinItem';
import {
  ROUTE_CREATE_SPECIAL_TYPE_TOTE_BIN,
} from '../../RouteConstant';
import myTheme from '../../Themes';
import { makeSelectGetSpecialTypeToteBinListSuccess, makeSelectSpecialTypeToteBinList } from './selectors';
import {
  deleteSpecialTypeToteBinAction, getSpecialTypeToteBinListAction,
  refreshSpecialTypeToteBinListAction,
} from './actions';
import EmptyView from '../../components/EmptyView';
import screenHOC from '../screenHOC';

/**
 * 特种箱记录列表
 * Created by jianzhexu on 2018/3/26
 */
@screenHOC
class SpecialTypeToteBin extends React.PureComponent {
  static navigationOptions = ({ navigation }) => ({
    title: '特种箱记录',
    headerRight: (
      <HeaderButtons>
        <HeaderButtons.Item
          title="创建"
          buttonStyle={{ fontSize: 14, color: '#ffffff' }}
          onPress={() => {
            const reload = navigation.getParam('reload', undefined);
            navigation.navigate(ROUTE_CREATE_SPECIAL_TYPE_TOTE_BIN, {
              title: '创建特种箱',
              onGoBack: () => {
                if (reload) {
                  reload();
                }
              },
            });
          }}
        />
      </HeaderButtons>
    ),
  });
  static propTypes = {
    navigation: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    success: PropTypes.bool.isRequired,
    data: PropTypes.array.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      isDelete: false,
      refreshing: false,
    };
    this.props.navigation.setParams({ reload: this.reLoadingPage });
  }

  componentWillMount() {
    this.reLoadingPage();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.success && this.state.isDelete) {
      this.setState({ isDelete: false }, () => {
        this.reLoadingPage();
      });
    }
    if (this.state.refreshing) {
      this.setState({ refreshing: false });
    }
  }

  reLoadingPage = () => {
    if (this.state.refreshing) {
      requestAnimationFrame(() => {
        this.props.dispatch(refreshSpecialTypeToteBinListAction());
      });
    } else {
      requestAnimationFrame(() => {
        this.props.dispatch(getSpecialTypeToteBinListAction());
      });
    }
  };

  itemPress = (data) => {
    this.props.navigation.navigate(ROUTE_CREATE_SPECIAL_TYPE_TOTE_BIN, {
      title: '编辑特种箱记录',
      onGoBack: () => {
        this.reLoadingPage();
      },
      type: 1,
      data,
    });
  };

  deleteItem = (id) => {
    this.setState({ isDelete: true }, () => {
      this.props.dispatch(deleteSpecialTypeToteBinAction(id));
    });
  };

  renderItem = ({ item }) => (<SpecialTypeToteBinItem
    hight={item.height}
    id={item.id}
    portName={item.portName}
    type={item.type}
    bay={item.bay}
    contNo={item.contNo}
    longBehind={item.longBehind}
    longFront={item.longFront}
    popId={item.popId}
    voyageId={item.voyageId}
    height={item.height}
    weight={item.weight}
    wideLeft={item.wideLeft}
    wideRight={item.wideRight}
    contType={item.contType}
    onItemPress={this.itemPress}
    onDelete={this.deleteItem}
  />);

  renderEmptyView = () => (<EmptyView />);

  render() {
    return (
      <Container theme={myTheme}>
        <FlatList
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => {
                this.setState({ refreshing: true }, () => {
                  this.reLoadingPage();
                });
              }}
            />
          }
          contentInsetAdjustmentBehavior="scrollableAxes"
          style={{ flex: 1, backgroundColor: '#e0e0e0' }}
          keyExtractor={item => `${item.id}`}
          data={this.props.data}
          ListEmptyComponent={this.renderEmptyView}
          initialNumToRender={10}
          renderItem={this.renderItem}
          ItemSeparatorComponent={() => (<View style={{ height: 5 }} />)}
          ListFooterComponent={() => (<View style={{ height: 5 }} />)}
          ListHeaderComponent={() => (<View style={{ height: 5 }} />)}
        />
      </Container>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  success: makeSelectGetSpecialTypeToteBinListSuccess(),
  data: makeSelectSpecialTypeToteBinList(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SpecialTypeToteBin);
