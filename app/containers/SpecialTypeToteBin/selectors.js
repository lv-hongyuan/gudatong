import { createSelector } from 'reselect/es';

const selectSpecialTypeToteBinDomain = () => state => state.specialTypeToteBin;

const makeSelectGetSpecialTypeToteBinListSuccess = () => createSelector(
  selectSpecialTypeToteBinDomain(),
  (subState) => {
    console.debug(subState.success);
    return subState.success;
  },
);

const makeSelectSpecialTypeToteBinList = () => createSelector(
  selectSpecialTypeToteBinDomain(),
  (subState) => {
    console.debug(subState.success);
    return subState.data;
  },
);

export {
  makeSelectGetSpecialTypeToteBinListSuccess,
  selectSpecialTypeToteBinDomain,
  makeSelectSpecialTypeToteBinList,
};
