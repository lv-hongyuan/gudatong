import {
  GET_SPECIAL_TYPE_TOTE_BIN_LIST_ACTION_RESULT,
  DELETE_SPECIAL_TYPE_TOTE_BIN_ACTION_RESULT,
  DELETE_SPECIAL_TYPE_TOTE_BIN_ACTION,
  GET_SPECIAL_TYPE_TOTE_BIN_LIST_ACTION,
} from './constants';

const initState = {
  success: false,
};

export default function (state = initState, action) {
  switch (action.type) {
    case DELETE_SPECIAL_TYPE_TOTE_BIN_ACTION:
    case GET_SPECIAL_TYPE_TOTE_BIN_LIST_ACTION:
      return { ...state, success: false };
    case GET_SPECIAL_TYPE_TOTE_BIN_LIST_ACTION_RESULT:
      return {
        ...state,
        success: action.payload.success,
        data: action.payload.data ? action.payload.data.dtoList : [],
      };
    case DELETE_SPECIAL_TYPE_TOTE_BIN_ACTION_RESULT:
      return { ...state, success: action.payload.success };
    default:
      return state;
  }
}
