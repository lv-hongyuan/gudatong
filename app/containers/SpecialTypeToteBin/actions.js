import {
  GET_SPECIAL_TYPE_TOTE_BIN_LIST_ACTION,
  GET_SPECIAL_TYPE_TOTE_BIN_LIST_ACTION_RESULT,
  DELETE_SPECIAL_TYPE_TOTE_BIN_ACTION,
  DELETE_SPECIAL_TYPE_TOTE_BIN_ACTION_RESULT,
} from './constants';

export function getSpecialTypeToteBinListAction(data) {
  return {
    type: GET_SPECIAL_TYPE_TOTE_BIN_LIST_ACTION,
    payload: data,
  };
}

export function refreshSpecialTypeToteBinListAction(data) {
  return {
    type: GET_SPECIAL_TYPE_TOTE_BIN_LIST_ACTION,
    payload: data,
  };
}

export function getSpecialTypeToteBinListResultAction(data, success) {
  return {
    type: GET_SPECIAL_TYPE_TOTE_BIN_LIST_ACTION_RESULT,
    payload: {
      data,
      success,
    },
  };
}

export function deleteSpecialTypeToteBinAction(data) {
  return {
    type: DELETE_SPECIAL_TYPE_TOTE_BIN_ACTION,
    payload: data,
  };
}

export function deleteSpecialTypeToteBinResultAction(data, success) {
  return {
    type: DELETE_SPECIAL_TYPE_TOTE_BIN_ACTION_RESULT,
    payload: {
      data,
      success,
    },
  };
}
