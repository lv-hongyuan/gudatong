/*
 *
 * EditDangerousGoodsRecord constants
 *
 */
export const CREATE_DANGEROUS_GOODS_RECORD = 'app/EditDangerousGoodsRecord/CREATE_DANGEROUS_GOODS_RECORD';

export const UPDATE_DANGEROUS_GOODS_RECORD = 'app/EditDangerousGoodsRecord/UPDATE_DANGEROUS_GOODS_RECORD';
