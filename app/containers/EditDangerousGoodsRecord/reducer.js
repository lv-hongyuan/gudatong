import {
  createDangerousGoodsRecordRoutine,
  updateDangerousGoodsRecordRoutine,
} from './actions';

const initState = {
  parentData: [],
  childData: [],
};

export default function (state = initState, action) {
  switch (action.type) {
    // 创建加油申请
    case createDangerousGoodsRecordRoutine.TRIGGER:
      return { ...state, loading: true };
    case createDangerousGoodsRecordRoutine.SUCCESS:
      return state;
    case createDangerousGoodsRecordRoutine.FAILURE:
      return { ...state, error: action.payload };
    case createDangerousGoodsRecordRoutine.FULFILL:
      return { ...state, loading: false };

    // 编辑加油申请
    case updateDangerousGoodsRecordRoutine.TRIGGER:
      return { ...state, loading: true };
    case updateDangerousGoodsRecordRoutine.SUCCESS:
      return state;
    case updateDangerousGoodsRecordRoutine.FAILURE:
      return { ...state, error: action.payload };
    case updateDangerousGoodsRecordRoutine.FULFILL:
      return { ...state, loading: false };

    default:
      return state;
  }
}
