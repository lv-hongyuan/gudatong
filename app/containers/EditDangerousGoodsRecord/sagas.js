import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import {
  createDangerousGoodsRecordRoutine,
  updateDangerousGoodsRecordRoutine,
} from './actions';
import { errorMessage } from '../ErrorHandler/actions';
import { getPopId } from '../ShipWork/sagas';

function* createDangerousGoodsRecord(action) {
  console.log(action);
  try {
    yield put(createDangerousGoodsRecordRoutine.request());
    const popId = yield call(getPopId);
    const {
      contNumber,
      unloadingPort,
      shipLoadPosition,
      imoUnNo,
      contType,
    } = action.payload;
    const response = yield call(
      request,
      ApiFactory.createDangerousGoods({
        popId,
        contNumber,
        unloadingPort,
        shipLoadPosition,
        imoUnNo,
        contType,
      }),
    );
    console.log('createDangerousGoodsRecord', response.toString());
    yield put(createDangerousGoodsRecordRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(createDangerousGoodsRecordRoutine.failure(e));
  } finally {
    yield put(createDangerousGoodsRecordRoutine.fulfill());
  }
}

export function* createDangerousGoodsRecordSaga() {
  yield takeLatest(createDangerousGoodsRecordRoutine.TRIGGER, createDangerousGoodsRecord);
}

function* updateDangerousGoodsRecord(action) {
  console.log(action);
  try {
    yield put(updateDangerousGoodsRecordRoutine.request());
    const popId = yield call(getPopId);
    const {
      id,
      contNumber,
      unloadingPort,
      shipLoadPosition,
      imoUnNo,
      contType,
    } = action.payload;
    const response = yield call(
      request,
      ApiFactory.updateDangerousGoods({
        id,
        popId,
        contNumber,
        unloadingPort,
        shipLoadPosition,
        imoUnNo,
        contType,
      }),
    );
    console.log('updateDangerousGoodsRecord', response.toString());
    yield put(updateDangerousGoodsRecordRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(updateDangerousGoodsRecordRoutine.failure(e));
  } finally {
    yield put(updateDangerousGoodsRecordRoutine.fulfill());
  }
}

export function* updateDangerousGoodsRecordSaga() {
  yield takeLatest(updateDangerousGoodsRecordRoutine.TRIGGER, updateDangerousGoodsRecord);
}

// All sagas to be loaded
export default [
  createDangerousGoodsRecordSaga,
  updateDangerousGoodsRecordSaga,
];
