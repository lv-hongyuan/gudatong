import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  CREATE_DANGEROUS_GOODS_RECORD,
  UPDATE_DANGEROUS_GOODS_RECORD,
} from './constants';

export const createDangerousGoodsRecordRoutine = createRoutine(
  CREATE_DANGEROUS_GOODS_RECORD,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const createDangerousGoodsRecordPromise = promisifyRoutine(createDangerousGoodsRecordRoutine);

export const updateDangerousGoodsRecordRoutine = createRoutine(
  UPDATE_DANGEROUS_GOODS_RECORD,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const updateDangerousGoodsRecordPromise = promisifyRoutine(updateDangerousGoodsRecordRoutine);
