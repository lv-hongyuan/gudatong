import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Container, Content, InputGroup, Item, Label } from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import HeaderButtons from 'react-navigation-header-buttons';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import _ from 'lodash';
import { Keyboard } from 'react-native';
import {
  createDangerousGoodsRecordPromise,
  updateDangerousGoodsRecordPromise,
} from './actions';
import {
  makeSelectPopId,
  makeData,
  makeOnoBack,
  makeIsEdit,
} from './selectors';
import { errorMessage } from '../ErrorHandler/actions';
import commonStyles from '../../common/commonStyles';
import InputItem from '../../components/InputItem';
import screenHOC from '../screenHOC';
import { contTypeList } from '../../common/Constant';
import Selector from '../../components/Selector';
import validContNumber from '../../utils/validContNumber';
import { getNextPortListPromise } from '../EditDxRecord/actions';
import { makePortList } from '../EditDxRecord/selectors';

/**
 * 危险品记录
 * Created by jianzhexu on 2018/3/23
 */
@screenHOC
class EditDangerousGoodsRecord extends React.PureComponent {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.getParam('title', '创建危险品记录'),
    headerRight: (
      <HeaderButtons>
        <HeaderButtons.Item
          title="保存"
          buttonStyle={{ fontSize: 14, color: '#ffffff' }}
          onPress={() => {
            const handleSubmitFunc = navigation.getParam('submit', undefined);
            if (handleSubmitFunc) {
              requestAnimationFrame(() => {
                handleSubmitFunc();
              });
            }
          }}
        />
      </HeaderButtons>
    ),
  });
  static propTypes = {
    navigation: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    getNextPortList: PropTypes.func.isRequired,
    updateDangerousGoodsRecord: PropTypes.func.isRequired,
    createDangerousGoodsRecord: PropTypes.func.isRequired,
    onGoBack: PropTypes.func.isRequired,
    data: PropTypes.object.isRequired,
    isEdit: PropTypes.bool.isRequired,
    portList: PropTypes.array.isRequired,
  };

  constructor(props) {
    super(props);

    const data = props.data || {};
    this.state = {
      id: data.id,
      popId: data.popId,
      contNumber: data.contNumber,
      unloadingPort: data.unloadingPort,
      shipLoadPosition: data.shipLoadPosition,
      imoUnNo: data.imoUnNo,
      contType: data.contType,
    };
    this.props.navigation.setParams({ submit: this.handleSubmit });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.data && nextProps.data !== this.props.data) {
      const data = nextProps.data || {};
      this.setState({
        id: data.id,
        popId: data.popId,
        contNumber: data.contNumber,
        unloadingPort: data.unloadingPort,
        shipLoadPosition: data.shipLoadPosition,
        imoUnNo: data.imoUnNo,
        contType: data.contType,
      });
    }
  }

  onPressPort = () => {
    Keyboard.dismiss();
    this.props.getNextPortList()
      .then(() => {
        this.select.showPicker();
      })
      .catch(() => {});
  };

  handleSubmit = () => {
    if (_.isEmpty(this.state.contNumber)) {
      this.props.dispatch(errorMessage('请填写箱号'));
      return;
    }
    if (!validContNumber(this.state.contNumber)) {
      this.props.dispatch(errorMessage('请填写正确的箱号'));
      return;
    }
    if (_.isEmpty(this.state.unloadingPort)) {
      this.props.dispatch(errorMessage('请填写卸货港'));
      return;
    }
    if (_.isEmpty(this.state.shipLoadPosition)) {
      this.props.dispatch(errorMessage('请填写装船位置'));
      return;
    }
    if (_.isEmpty(this.state.imoUnNo)) {
      this.props.dispatch(errorMessage('请填写IMO/UN NO. '));
      return;
    }
    if (_.isEmpty(this.state.contType)) {
      this.props.dispatch(errorMessage('请选择箱型'));
      return;
    }
    if (this.props.isEdit) {
      this.props.updateDangerousGoodsRecord({
        id: this.state.id,
        popId: this.state.popId,
        contNumber: this.state.contNumber,
        unloadingPort: this.state.unloadingPort,
        shipLoadPosition: this.state.shipLoadPosition,
        imoUnNo: this.state.imoUnNo,
        contType: this.state.contType,
      }).then(() => {
        if (this.props.onGoBack) this.props.onGoBack();
        this.props.navigation.goBack();
      }).catch(() => {});
    } else {
      this.props.createDangerousGoodsRecord({
        popId: this.state.popId,
        contNumber: this.state.contNumber,
        unloadingPort: this.state.unloadingPort,
        shipLoadPosition: this.state.shipLoadPosition,
        imoUnNo: this.state.imoUnNo,
        contType: this.state.contType,
      }).then(() => {
        if (this.props.onGoBack) this.props.onGoBack();
        this.props.navigation.goBack();
      }).catch(() => {});
    }
  };

  render() {
    return (
      <Container style={{
        backgroundColor: '#FFFFFF',
      }}
      >
        <Content style={{
          padding: 10,
        }}
        >
          <InputGroup style={commonStyles.inputGroup}>
            <InputItem
              label="箱号:"
              value={this.state.contNumber}
              maxLength={11}
              placeholder=""
              onChangeText={(text) => {
                this.setState({
                  contNumber: text,
                });
              }}
            />
          </InputGroup>
          <InputGroup style={commonStyles.inputGroup}>
            <Item style={commonStyles.inputItem}>
              <Label style={commonStyles.inputLabel}>卸货港:</Label>
              <Selector
                ref={(ref) => {
                  this.select = ref;
                }}
                onPress={this.onPressPort}
                value={this.state.unloadingPort}
                items={this.props.portList}
                pickerTitle="请选择卸货港"
                getItemValue={item => item.text}
                getItemText={item => item.text}
                // pickerType="popover"
                onSelected={(item) => {
                  this.setState({ unloadingPort: item.text });
                }}
              />
            </Item>
          </InputGroup>
          <InputGroup style={commonStyles.inputGroup}>
            <InputItem
              label="装船位置:"
              value={this.state.shipLoadPosition || null}
              onChangeText={(text) => {
                this.setState({
                  shipLoadPosition: text,
                });
              }}
            />
          </InputGroup>
          <InputGroup style={commonStyles.inputGroup}>
            <InputItem
              label="IMO/UN NO.:"
              value={this.state.imoUnNo || null}
              onChangeText={(text) => {
                this.setState({
                  imoUnNo: text,
                });
              }}
            />
          </InputGroup>
          <InputGroup style={commonStyles.inputGroup}>
            <Item style={commonStyles.inputItem}>
              <Label style={commonStyles.inputLabel}>箱型:</Label>
              <Selector
                value={this.state.contType}
                items={contTypeList}
                pickerTitle="请选择箱型"
                getItemValue={item => item}
                getItemText={item => item}
                // pickerType="popover"
                onSelected={(item) => {
                  this.setState({ contType: item });
                }}
              />
            </Item>
          </InputGroup>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  popId: makeSelectPopId(),
  isEdit: makeIsEdit(),
  data: makeData(),
  onGoBack: makeOnoBack(),
  portList: makePortList(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      createDangerousGoodsRecord: createDangerousGoodsRecordPromise,
      updateDangerousGoodsRecord: updateDangerousGoodsRecordPromise,
      getNextPortList: getNextPortListPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EditDangerousGoodsRecord);
