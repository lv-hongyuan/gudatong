import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_SHIP_TUG_REWARD_TARGET } from './constants';

export const getShipTugRewardTargetRoutine = createRoutine(GET_SHIP_TUG_REWARD_TARGET);
export const getShipTugRewardTargetPromise = promisifyRoutine(getShipTugRewardTargetRoutine);