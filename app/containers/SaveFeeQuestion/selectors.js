import { createSelector } from 'reselect/es';
const selectGetShipTugRewardTargetDomain = () => state => state.saveFeeQuestion;

const makeList = () => createSelector(selectGetShipTugRewardTargetDomain(), (subState) => {
  return subState.list;
});
  
  const makeRefreshState = () => createSelector(selectGetShipTugRewardTargetDomain(), (subState) => {
    console.debug(subState.refreshState);
    return subState.refreshState;
  });

  const makeData = () => createSelector(selectGetShipTugRewardTargetDomain(), (subState) => {
    console.debug(subState);
    return subState.data;
  });
  
  export { makeList, makeRefreshState,makeData };