import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../ErrorHandler/actions';
import { getShipTugRewardTargetRoutine } from './actions';

function* getShipTugRewardTarget(action) {
  console.log(action);
  try {
    yield put(getShipTugRewardTargetRoutine.request());
    console.log('!!!!!', action.payload)
    const response = yield call(request, ApiFactory.getShipRewardTarget());
    console.log('+++question', JSON.stringify(response));
    yield put(getShipTugRewardTargetRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(getShipTugRewardTargetRoutine.failure(e));
  } finally {
    yield put(getShipTugRewardTargetRoutine.fulfill());
  }
}

export function* getEditionTargetSaga() {
  yield takeLatest(getShipTugRewardTargetRoutine.TRIGGER, getShipTugRewardTarget);
}

export default [getEditionTargetSaga];
