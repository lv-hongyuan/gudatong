import { getShipTugRewardTargetRoutine } from './actions';
import { RefreshState } from '../../components/RefreshListView';
import fixIdList from '../../utils/fixIdList';

const initState = {
  list: [],
  refreshState: RefreshState.Idle,
  data: {},
};

export default function (state = initState, action) {
  switch (action.type) {
    case getShipTugRewardTargetRoutine.TRIGGER: {
      return {...state, loading: true, refreshState: RefreshState.HeaderRefreshing};
    }
    case getShipTugRewardTargetRoutine.SUCCESS: {
      return {
        ...state,
        list: action.payload.targetList?action.payload.targetList:[],
        data: action.payload,
        refreshState: RefreshState.NoMoreData
    };
    }
    case getShipTugRewardTargetRoutine.FAILURE: {
      const { page, error } = action.payload;
      return {
        ...state, list: page === 1 ? [] : state.list, error, refreshState: RefreshState.Failure,
      };
    }
    case getShipTugRewardTargetRoutine.FULFILL: {
      return { ...state, loading: false };
    }

    default:
      return state;
  }
}

