import React from 'react';
import PropTypes from 'prop-types';
import { SafeAreaView, Animated, DeviceEventEmitter } from 'react-native';
import { connect } from 'react-redux';
import {
    Container, InputGroup,
    Text,
    View,
} from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import myTheme from '../../Themes';
import screenHOC, { Orientations } from '../listScreenHOC';
import RefreshListView from '../../components/RefreshListView';
import HeaderButtons from "react-navigation-header-buttons";
import { createPilotApplyAction, updatePilotApplyAction } from "../CreatePilotApply/actions";
import { NavigationActions } from "react-navigation";
import commonStyles from "../../common/commonStyles";
import SelectDate from "../SaveFeeList/components/SelectDate";
import SaveFeeQuestionItem from './components/SaveFeeQuestionItem';
import { makeList, makeRefreshState, makeData } from './selectors';
import { getShipTugRewardTargetPromise } from './actions';
import Svg from '../../components/Svg';
import Orientation from 'react-native-orientation';



const styles = {
    container: {
        backgroundColor: 'white',
        flexDirection: 'row',
        // alignContent: 'stretch',
    },
    col: {
        backgroundColor: 'white',
        width: 80,
        height: 40,
        paddingLeft: 0,
        paddingRight: 0,
        paddingTop: 0,
        paddingBottom: 0,
        borderRightWidth: myTheme.borderWidth,
        borderBottomWidth: myTheme.borderWidth,
        borderColor: myTheme.borderColor,
        flexDirection: 'row',
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
    },
    touchCol: {
        backgroundColor: 'white',
        width: 80,
    },
    touchContent: {
        backgroundColor: 'white',
        paddingLeft: 0,
        paddingRight: 0,
        paddingTop: 0,
        paddingBottom: 0,
        borderRightWidth: myTheme.borderWidth,
        borderBottomWidth: myTheme.borderWidth,
        borderColor: myTheme.borderColor,
        flexDirection: 'row',
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
    },
    text: {
        color: myTheme.inputColor,
        textAlign: 'center',
        flex: 1,
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 5,
        paddingBottom: 5,
    },
    subText: {
        color: myTheme.inputColor,
        textAlign: 'center',
        flex: 1,
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 0,
        paddingBottom: 0,
    },
    footView: {
        height: 30,
        // backgroundColor: 'red',
        width: 1000,
        marginBottom: 0,
        flexDirection: 'row',
        borderTopWidth: myTheme.borderWidth,
        borderColor: myTheme.borderColor,
        backgroundColor: 'white',
    },
    textView: {
        marginTop: 0,
        marginBottom: 0,
        // backgroundColor: 'green',
        alignItems: 'center',
        borderWidth: myTheme.borderWidth,
        borderColor: myTheme.borderColor,
        justifyContent: 'center',
        alignContent: 'center',
        textAlign: 'center',
    },
    titleText: {
        padding: 0,
        textAlign: 'center',
        color: myTheme.inputColor,
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
    },
    moneyText: {
        width: 80,
        color: myTheme.inputColor,
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
    },
};

const firstPageNum = 1;
const DefaultPageSize = 10;

@screenHOC
class SaveFeeQuestion extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            containerOffSet: new Animated.Value(0),
            recordW: 0,
            recordH: 0,
        };
    }

    componentDidMount() {
        this.listView.beginRefresh();
    }

    onHeaderRefresh = () => {
        this.loadList(false);
    };

    onFooterRefresh = () => {
        this.loadList(true);
    };

    //根View的onLayout回调函数
    onLayout = (event) => {
        //获取根View的宽高，以及左上角的坐标值
        let { x, y, width, height } = event.nativeEvent.layout;
        this.setState({
            recordW: width,
            recordH: height,
        });
    }

    get translateX() {
        const x = 0;
        return this.state.containerOffSet.interpolate({
            inputRange: [-1, 0, x, x + 1],
            outputRange: [0, 0, 0, 1],
        });
    }

    supportedOrientations = Orientations.LANDSCAPE;

    loadList() {
        this.props.getShipTugRewardTargetPromise()
            .then((data)=>{
                this.props.navigation.setParams({navTitle: this.props.data.shipName + '节拖奖励指标'});
            })
    }

    componentWillUnmount() {
        
    }

    renderItem = ({ item, index }) => (
        <SaveFeeQuestionItem
            item={item}
            index={index}
            headerTranslateX={this.translateX}
        />
    );

    render() {
        console.log('data===', this.props.data)
        let data = this.props.data;
        return (
            <Container theme={myTheme} style={{ backgroundColor: '#E0E0E0' }}>
                <SafeAreaView style={{
                    backgroundColor: '#E0E0E0',
                    flex: 1,
                }}
                >
                    <View
                        style={{
                            width: '100%',
                            flex: 1,
                        }}
                        onLayout={(event) => {
                            const { width } = event.nativeEvent.layout;
                            this.setState({ containerWidth: width });
                        }}
                    >
                        <Animated.ScrollView
                            onScroll={Animated.event([{
                                nativeEvent: { contentOffset: { x: this.state.containerOffSet } },
                            }], { useNativeDriver: true })}
                            scrollEventThrottle={1}
                            horizontal
                        >
                            <View style={{
                                flex: 1,
                                minWidth: '100%',
                            }}
                            >

                                {data.type == 0 ? <View style={{ width: '100%',height:'100%'}}>
                                    <Text style={[styles.text,{fontSize:17,height:50,fontWeight:'700'}]}>{data.targetText}</Text>
                                </View> : null}
                                {data.type == 10 ? <View style={styles.container}>
                                    <View style={[styles.col, { width: '25%' }]}>
                                        <Text style={styles.text}>{'港口'}</Text>
                                    </View>
                                    <View style={[styles.col, { width: '25%' }]}>
                                        <Text style={styles.text}>{'配备拖轮数'}</Text>
                                    </View>
                                    <View style={[styles.col, { width: '25%' }]}>
                                        <Text style={styles.text}>{'节省拖轮奖励(元/条)'}</Text>
                                    </View>
                                    <View style={[styles.col, { width: '25%' }]}>
                                        <Text style={styles.text}>{'节省靠离引水(元/次)'}</Text>
                                    </View>
                                </View> : null}

                                <RefreshListView
                                    ref={(ref) => {
                                        this.listView = ref;
                                    }}
                                    headerTranslateX={this.translateX}
                                    containerWidth={this.state.containerWidth}
                                    data={this.props.list || []}
                                    style={{ flex: 1 }}
                                    keyExtractor={item => `${item.portId}`}
                                    renderItem={this.renderItem}
                                    refreshState={this.props.refreshState}
                                    onHeaderRefresh={this.onHeaderRefresh}
                                // onFooterRefresh={this.onFooterRefresh}
                                />

                            </View>
                        </Animated.ScrollView>
                    </View>
                </SafeAreaView>
            </Container>
        )
    }
}

SaveFeeQuestion.navigationOptions = ({ navigation }) => ({
    title: navigation.getParam('navTitle', undefined),
    headerRight: <HeaderButtons>
    </HeaderButtons>
});

const mapStateToProps = createStructuredSelector({
    refreshState: makeRefreshState(),
    list: makeList(),
    data: makeData(),
});

function mapDispatchToProps(dispatch) {
    return {
        ...bindPromiseCreators({
            getShipTugRewardTargetPromise,
        }, dispatch),
        dispatch,
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(SaveFeeQuestion);
