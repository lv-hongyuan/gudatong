import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Animated, DeviceEventEmitter } from 'react-native';
import { InputGroup, Text, View } from 'native-base';
import myTheme from '../../../Themes';
import commonStyles from "../../../common/commonStyles";
import HeaderButtons from "react-navigation-header-buttons";
import Selector from '../../../components/Selector';
import { cloneDeep } from 'lodash'
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { makeList, makeRefreshState,makeData} from "../selectors";


const styles = StyleSheet.create({
    container: {
        alignContent: 'stretch',
        backgroundColor: 'white',
        flexDirection: 'row',
    },
    col: {
        width: 80,
        // paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 5,
        paddingBottom: 5,
        borderRightWidth: myTheme.borderWidth,
        borderBottomWidth: myTheme.borderWidth,
        borderColor: myTheme.borderColor,
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
        textAlign: 'center'
    },
    textCol: {
        width: 80,
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 5,
        paddingBottom: 5,
        borderRightWidth: myTheme.borderWidth,
        borderBottomWidth: myTheme.borderWidth,
        borderColor: myTheme.borderColor,
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
        textAlign: 'center'
    },
    text: {
        color: myTheme.inputColor,
    },
});

class SaveFeeQuestionItem extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {

        };
    }

    componentDidMount() {
        console.log('###===', this.props.item)
        console.log('$$$===', this.props.data)
    }

    componentWillUnmount() {

    }

    render() {
        console.log('item===', this.props.index)
        const {
            id, port,tugNum,divMoney,tugMoney
        } = this.props.item || {};
        const { index } = this.props || {};
        return (
            <View style={styles.container}>
                <View style={[styles.textCol, { width: '25%', flex: 1, }]}>
                    <Text style={styles.text}>{port}</Text>
                </View>
                <View style={[styles.col, { width: '25%' }]}>
                    <Text style={styles.text}>{tugNum}</Text>
                </View>
                <View style={[styles.col, { width: '25%' }]}>
                    <Text style={styles.text}>{tugMoney}</Text>
                </View>
                <View style={[styles.col, { width: '25%' }]}>
                    <Text style={styles.text}>{divMoney}</Text>
                </View>
            </View>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    data: makeData(),
});

export default connect(mapStateToProps)(SaveFeeQuestionItem);