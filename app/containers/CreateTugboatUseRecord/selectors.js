import { createSelector } from 'reselect/es';

const selectCreateTugboatUseRecordDomain = () => state => state.createTugboatUseRecord;
const selectShipStateDomain = () => state => state.shipWork.data;
const makeSelectCreateTugboatUseRecordSuccess = () => createSelector(
  selectCreateTugboatUseRecordDomain(),
  (subState) => {
    console.debug(subState.success);
    return subState.success;
  },
);

const makeSelectPortName = () => createSelector(
  selectShipStateDomain(),
  subState => subState.portName,
);

const makeSelectPopId = () => createSelector(
  selectShipStateDomain(),
  (subState) => {
    console.debug(subState.success);
    return subState.popId;
  },
);

export {
  makeSelectCreateTugboatUseRecordSuccess,
  selectCreateTugboatUseRecordDomain,
  makeSelectPortName,
  makeSelectPopId,
};
