import {
  CREATE_TUGBOAT_USE_RECORD_ACTION_RESULT,
  UPDATE_TUGBOAT_USE_ACTION_RESULT,
  CREATE_TUGBOAT_USE_RECORD_ACTION,
  UPDATE_TUGBOAT_USE_ACTION,
} from './constants';

const initState = {
  success: false,
};

export default function (state = initState, action) {
  switch (action.type) {
    case CREATE_TUGBOAT_USE_RECORD_ACTION:
    case UPDATE_TUGBOAT_USE_ACTION:
      return { ...state, success: false };
    case CREATE_TUGBOAT_USE_RECORD_ACTION_RESULT:
    case UPDATE_TUGBOAT_USE_ACTION_RESULT:
      return { ...state, success: action.payload.success };
    default:
      return state;
  }
}
