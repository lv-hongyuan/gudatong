import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { Container, Content, Item, Label, View, InputGroup } from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import HeaderButtons from 'react-navigation-header-buttons';
import { createTugboatUseRecordAction, updateTugboatUseRecordAction } from './actions';
import { getTugboatUseWayName } from '../../common/Constant';
import { errorMessage } from '../ErrorHandler/actions';
import { makeSelectPortName, makeSelectPopId, makeSelectCreateTugboatUseRecordSuccess } from './selectors';
import CommonDataPickerDialog from '../../components/CommonDataPickerDialog';
import commonStyles from '../../common/commonStyles';
import SelectButton from '../../components/SelectButton';
import stringToNumber from '../../utils/stringToNumber';
import InputItem from '../../components/InputItem';
import DatePullSelector from '../../components/DatePullSelector';
import screenHOC from '../screenHOC';
import { currentSuitableTime } from '../../utils/suitableDateTime';
import { MaskType } from '../../components/InputItem/TextInput';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    paddingLeft: 20,
    paddingTop: 10,
    paddingBottom: 10,
    paddingRight: 20,
  },
});

/**
 * 创建拖轮使用记录
 * Created by jianzhexu on 2018/3/26
 */
@screenHOC
class CreateTugboatUseRecord extends React.PureComponent {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.getParam('title', '创建拖轮使用记录'),
    headerRight: (
      <HeaderButtons>
        <HeaderButtons.Item
          title="保存"
          buttonStyle={{ fontSize: 14, color: '#ffffff' }}
          onPress={() => {
            const handleSubmitFunc = navigation.getParam('submit', undefined);
            if (handleSubmitFunc) {
              requestAnimationFrame(() => {
                handleSubmitFunc();
              });
            }
          }}
        />
      </HeaderButtons>
    ),
  });
  static propTypes = {
    navigation: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    popId: PropTypes.number.isRequired,
    portName: PropTypes.string.isRequired,
    success: PropTypes.bool.isRequired,
  };

  constructor(props) {
    super(props);

    const data = this.props.navigation.getParam('data', undefined);
    this.state = {
      showUseWay: false,
      isInit: false,
      tugName: '',
      tugPower: 0,
      startTime: data ? (data.startTime || null) : currentSuitableTime(),
      endTime: data ? data.endTime || null : null,
      useWay: 10,
      isUpdating: false,
      portName: '',
      id: '',
      popId: '',
    };
    this.props.navigation.setParams({ submit: this.handleSubmit });
  }

  componentDidMount() {
    this.setState({
      isInit: true,
    });
  }

  componentWillReceiveProps(nextProps) {
    const data = nextProps.navigation.getParam('data', undefined);
    if (data && this.state.isInit) {
      this.setState(() => ({
        useWay: data.useWay,
        isInit: false,
        tugName: data.tugName,
        tugPower: data.tugPower,
        startTime: data.startTime || null,
        endTime: data.endTime || null,
        portName: data.portName,
        id: data.id,
        popId: data.popId,
      }));
    }

    if (nextProps.success && this.state.isUpdating) {
      const goBackCallback = this.props.navigation.getParam('onGoBack', undefined);
      if (goBackCallback) {
        goBackCallback();
      }
      this.props.navigation.goBack();
      this.setState({
        isUpdating: false,
      });
    }
  }

  handleSubmit = () => {
    this.setState({
      isUpdating: true,
    }, () => {
      const type = this.props.navigation.getParam('type', undefined);

      const param = {
        id: this.state.id,
        popId: this.state.popId,
        useWay: this.state.useWay,
        tugName: this.state.tugName,
        tugPower: stringToNumber(this.state.tugPower),
        startTime: this.state.startTime,
        portName: this.state.portName,
        endTime: this.state.endTime,
      };
      if (!param.startTime) {
        this.props.dispatch(errorMessage('请选择开始时间'));
        return;
      }
      if (!param.startTime) {
        this.props.dispatch(errorMessage('请选择开始时间'));
        return;
      }
      if (!param.endTime) {
        this.props.dispatch(errorMessage('请选择结束时间'));
        return;
      }
      if (param.endTime < param.startTime) {
        this.props.dispatch(errorMessage('结束时间不能早于开始时间'));
        return;
      }

      if (type) {
        this.props.dispatch(updateTugboatUseRecordAction(param));
      } else {
        param.popId = this.props.popId;
        param.portName = this.props.portName;
        if (this.state.startTime > 0) {
          param.startTime = this.state.startTime;
        }
        if (this.state.endTime > 0) {
          param.endTime = this.state.endTime;
        }
        this.props.dispatch(createTugboatUseRecordAction(param));
      }
    });
  };

  renderUseWay = () => (
    <CommonDataPickerDialog
      title="选择使用途径"
      data={[{ name: '靠泊', value: 10 }, { name: '离泊', value: 20 }, { name: '移泊', value: 30 }]}
      value={this.state.useWay}
      onItemPress={(value) => {
        this.setState({
          useWay: value.value,
          showUseWay: false,
        });
      }}
      onClose={() => {
        this.setState({
          showUseWay: false,
        });
      }}
      visible={this.state.showUseWay}
    />);

  render() {
    return (
      <Container>
        <Content>
          {this.renderUseWay()}
          <View style={styles.container}>
            <InputGroup style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.readOnlyInputLabel}>发生港口:</Label>
                <Label
                  style={commonStyles.text}
                >{this.state.portName ? this.state.portName : this.props.portName}
                </Label>
              </Item>
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>使用途径:</Label>
                <SelectButton
                  value={getTugboatUseWayName(this.state.useWay)}
                  onPress={() => {
                    this.setState({
                      showUseWay: true,
                    });
                  }}
                />
              </Item>
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="拖轮名称:"
                value={this.state.tugName}
                editable={!this.state.readonly}
                onChangeText={(text) => {
                  this.setState({ tugName: text.trim() });
                }}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="拖轮功率:"
                value={this.state.tugPower}
                editable={!this.state.readonly}
                onChangeText={(text) => {
                  this.setState({ tugPower: text });
                }}
                keyboardType="numeric"
                maskType={MaskType.FLOAT}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>开始时间:</Label>
                <DatePullSelector
                  value={this.state.startTime || null}
                  onChangeValue={(value) => {
                    this.setState({ startTime: value });
                  }}
                />
              </Item>
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>结束时间:</Label>
                <DatePullSelector
                  value={this.state.endTime || null}
                  minValue={this.state.startTime}
                  onChangeValue={(value) => {
                    this.setState({ endTime: value });
                  }}
                />
              </Item>
            </InputGroup>
          </View>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  success: makeSelectCreateTugboatUseRecordSuccess(),
  portName: makeSelectPortName(),
  popId: makeSelectPopId(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateTugboatUseRecord);
