import { call, put, takeLatest } from 'redux-saga/effects';
import { CREATE_TUGBOAT_USE_RECORD_ACTION, UPDATE_TUGBOAT_USE_ACTION } from './constants';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import { createTugboatUseRecordResultAction, updateTugboatUseRecordResultAction } from './actions';
import { errorMessage } from '../ErrorHandler/actions';

function* createTugboatUseRecord(action) {
  console.log(action);
  try {
    const response = yield call(request, ApiFactory.createTugUse(action.payload));
    yield put(createTugboatUseRecordResultAction(response, true));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(createTugboatUseRecordResultAction(undefined, false));
  }
}

export function* createTugboatUseRecordSaga() {
  yield takeLatest(CREATE_TUGBOAT_USE_RECORD_ACTION, createTugboatUseRecord);
}

function* updateTugboatUseRecord(action) {
  console.log(action);
  try {
    const response = yield call(request, ApiFactory.updateTugUse(action.payload));
    yield put(updateTugboatUseRecordResultAction(response, true));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(updateTugboatUseRecordResultAction(undefined, false));
  }
}

export function* updateTugboatUseRecordSaga() {
  yield takeLatest(UPDATE_TUGBOAT_USE_ACTION, updateTugboatUseRecord);
}

// All sagas to be loaded
export default [
  createTugboatUseRecordSaga,
  updateTugboatUseRecordSaga,
];
