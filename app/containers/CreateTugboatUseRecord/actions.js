import {
  CREATE_TUGBOAT_USE_RECORD_ACTION,
  CREATE_TUGBOAT_USE_RECORD_ACTION_RESULT,
  UPDATE_TUGBOAT_USE_ACTION,
  UPDATE_TUGBOAT_USE_ACTION_RESULT,
} from './constants';

export function createTugboatUseRecordAction(data) {
  return {
    type: CREATE_TUGBOAT_USE_RECORD_ACTION,
    payload: data,
  };
}

export function createTugboatUseRecordResultAction(data, success) {
  return {
    type: CREATE_TUGBOAT_USE_RECORD_ACTION_RESULT,
    payload: {
      data,
      success,
    },
  };
}

export function updateTugboatUseRecordAction(data) {
  return {
    type: UPDATE_TUGBOAT_USE_ACTION,
    payload: data,
  };
}

export function updateTugboatUseRecordResultAction(success) {
  return {
    type: UPDATE_TUGBOAT_USE_ACTION_RESULT,
    payload: {
      success,
    },
  };
}
