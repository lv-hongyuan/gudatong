import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import { getPopId } from '../ShipWork/sagas';
import { getBerthPlanRoutine } from './actions';
import { errorMessage } from '../ErrorHandler/actions';

function* getBerthPlan(action) {
  console.log(action);
  try {
    yield put(getBerthPlanRoutine.request());
    const popId = yield call(getPopId);
    const response = yield call(request, ApiFactory.getBerthPlan(popId));
    console.log('getNoticeList', response.toString());
    yield put(getBerthPlanRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(getBerthPlanRoutine.failure(e));
  } finally {
    yield put(getBerthPlanRoutine.fulfill());
  }
}

export function* getBerthPlanSaga() {
  yield takeLatest(getBerthPlanRoutine.TRIGGER, getBerthPlan);
}

// All sagas to be loaded
export default [
  getBerthPlanSaga,
];
