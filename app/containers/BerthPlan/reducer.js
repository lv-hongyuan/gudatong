import { getBerthPlanRoutine } from './actions';

const initState = {
  data: undefined,
};

export default function (state = initState, action) {
  switch (action.type) {
    case getBerthPlanRoutine.TRIGGER:
      return { ...state, loading: true };
    case getBerthPlanRoutine.SUCCESS:
      return { ...state, data: action.payload };
    case getBerthPlanRoutine.FAILURE:
      return { ...state, error: action.payload };
    case getBerthPlanRoutine.FULFILL:
      return { ...state, loading: false };

    default:
      return state;
  }
}
