import { createSelector } from 'reselect/es';

const selectBerthPlanDomain = () => state => state.berthPlan;

const makeBerthPlan = () => createSelector(
  selectBerthPlanDomain(),
  (subState) => {
    console.debug(subState.success);
    return subState.data;
  },
);

export {
  makeBerthPlan,
};
