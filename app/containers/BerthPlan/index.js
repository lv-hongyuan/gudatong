import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { Container, Content, View, Label } from 'native-base';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { createStructuredSelector } from 'reselect/es';
import { getBerthPlanPromise } from './actions';
import myTheme from '../../Themes';
import { makeBerthPlan } from './selectors';
import screenHOC from '../screenHOC';
import { defaultFormat } from '../../utils/DateFormat';

const styles = StyleSheet.create({
    row: {
        flex: 1,
        paddingLeft: 15,
        paddingTop: 5,
        paddingBottom: 5,
        paddingRight: 30,
        flexDirection: 'row',
    },
    label: {
        fontSize: 14,
        color: '#535353',
    },
    text: {
        fontSize: 14,
        color: myTheme.inputColor,
        marginLeft: 10,
        flex: 1,
    },
    container: {
        width: '100%',
        backgroundColor: 'white',
        padding: 10,
    },
});

/**
 * 预靠计划页面
 * Created by jianzhexu on 2018/3/29
 */
@screenHOC
class BerthPlan extends React.PureComponent {
    static navigationOptions = ({ navigation }) => ({
        title: navigation.getParam('title', '预靠计划'),
    });
    static propTypes = {
        data: PropTypes.object,
        getBerthPlan: PropTypes.func.isRequired,
    };
    static defaultProps = {
        data: {},
    };

    componentWillMount() {
        this.reLoadingPage();
    }

    reLoadingPage = () => {
        requestAnimationFrame(() => {
            this.props.getBerthPlan()
                .then(() => {
                })
                .catch(() => {
                });
        });
    };

    render() {
        const {
            portName, directBerth, etbPort, etdPort, estWaitBerthTime, estWaitGoodsTime,
        } = (this.props.data || {});

        return (
            <Container theme={myTheme}>
                <Content>
                    <View style={styles.container}>
                        <View style={styles.row}>
                            <Label style={styles.label}>港口名称:</Label>
                            <Label style={styles.text}>{portName}</Label>
                        </View>
                        <View style={styles.row}>
                            <Label style={styles.label}>预计靠泊时间:</Label>
                            <Label style={styles.text}>{etbPort > 0 ? defaultFormat(etbPort) : ''}</Label>
                        </View>
                        <View style={styles.row}>
                            <Label style={styles.label}>预计离泊时间:</Label>
                            <Label style={styles.text}>{etdPort > 0 ? defaultFormat(etdPort) : ''}</Label>
                        </View>
                        <View style={styles.row}>
                            <Label style={styles.label}>是否直靠:</Label>
                            <Label style={styles.text}>{directBerth === 1 ? '直靠' : '非直靠'}</Label>
                        </View>
                        {
                            directBerth !== 1 && (
                                <View style={styles.row}>
                                    <Label style={styles.label}>预计影响时长:</Label>
                                    <Label style={styles.text}>{estWaitBerthTime > 0 ? estWaitBerthTime : ''}</Label>
                                </View>
                            )
                        }
                        {/*{*/}
                        {/*  directBerth !== 1 && (*/}
                        {/*    <View style={styles.row}>*/}
                        {/*      <Label style={styles.label}>预计等泊时间:</Label>*/}
                        {/*      <Label style={styles.text}>{estWaitBerthTime > 0 ? estWaitBerthTime : ''}</Label>*/}
                        {/*    </View>*/}
                        {/*  )*/}
                        {/*}*/}
                        {/*{*/}
                        {/*  directBerth !== 1 && (*/}
                        {/*    <View style={styles.row}>*/}
                        {/*      <Label style={styles.label}>预计等货时间:</Label>*/}
                        {/*      <Label style={styles.text}>{estWaitGoodsTime > 0 ? estWaitGoodsTime : ''}</Label>*/}
                        {/*    </View>*/}
                        {/*  )*/}
                        {/*}*/}
                    </View>
                </Content>
            </Container>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    data: makeBerthPlan(),
});

function mapDispatchToProps(dispatch) {
    return {
        ...bindPromiseCreators({
            getBerthPlan: getBerthPlanPromise,
        }, dispatch),
        dispatch,
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(BerthPlan);
