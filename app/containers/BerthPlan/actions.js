import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  GET_BERTH_PLAN,
} from './constants';

export const getBerthPlanRoutine = createRoutine(
  GET_BERTH_PLAN,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const getBerthPlanPromise = promisifyRoutine(getBerthPlanRoutine);
