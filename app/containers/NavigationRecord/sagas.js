import { call, put, select, takeLatest } from 'redux-saga/effects';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import {
  getNavigationRecordListRoutine,
  createNavigationRecordRoutine,
} from './actions';
import { errorMessage } from '../ErrorHandler/actions';
import realm from '../../common/realm';
import { getPopId, getShipId } from '../ShipWork/sagas';

const _isConnected = state => state.network.isConnected;

function* getNavigationRecordList(action) {
  console.log(action);
  try {
    yield put(getNavigationRecordListRoutine.request());
    const isConnected = yield select(_isConnected);
    if (isConnected) {
      // 获取popId，shipId
      const shipId = yield call(getShipId);
      const popId = yield call(getPopId);
      const param = {
        shipId,
        popId,
      };
      const response = yield call(request, ApiFactory.getLastSailDyn(param));
      yield put(getNavigationRecordListRoutine.success(response));
    } else {
      // 无网络时从本地存储中获取数据源
      const offlineTasks = realm.objects('OfflineTask')
        .filtered('type = "OfflineNavigationRecord"')
        .sorted('date', true);
      if (offlineTasks.length > 0) {
        const task = offlineTasks[0];
        yield put(getNavigationRecordListRoutine.success(JSON.parse(task.data)));
      } else {
        yield put(getNavigationRecordListRoutine.success({}));
      }
    }
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(getNavigationRecordListRoutine.failure(e));
  } finally {
    yield put(getNavigationRecordListRoutine.fulfill());
  }
}

export function* getNavigationRecordListSaga() {
  yield takeLatest(getNavigationRecordListRoutine.TRIGGER, getNavigationRecordList);
}

function* createNavigationRecordList(action) {
  console.log(action);
  try {
    yield put(createNavigationRecordRoutine.request());
    const isConnected = yield select(_isConnected);
    if (isConnected) {
      const popId = yield call(getPopId);
      const response = yield call(request, ApiFactory.createSailDyn({ popId, ...action.payload }));
      console.log(response);
      const offlineTasks = realm.objects('OfflineTask')   //获取当前用于自动导入的数据源
        .filtered('type = "OfflineNavigationRecord"')
        .sorted('date', true);
      if(offlineTasks.length > 0){    //如果当前数据源数量大于0，清空数据源，
        while (offlineTasks.length > 0) {
          const task = offlineTasks[0];
          realm.write(() => {
            realm.delete(task);
          });
        }
      }
      //写入最新的数据
      realm.write(() => {
        realm.create('OfflineTask', {
          type: 'OfflineNavigationRecord',
          data: JSON.stringify(action.payload),
          date: new Date().getTime(),
        });
      });
      yield put(createNavigationRecordRoutine.success(response));
    } else {
      //离线模式下将填写的数据存储至本地，用于恢复网络后上传至服务器
      realm.write(() => {
        // Create a OfflineTask object
        realm.create('OfflineTask', {
          type: 'NavigationRecord',
          data: JSON.stringify(action.payload),
          date: new Date().getTime(),
        });
      });
      //获取当前用于自动导入的数据源。如果当前数据源数量大于0，清空数据源，
      const offlineTasks = realm.objects('OfflineTask')
        .filtered('type = "OfflineNavigationRecord"')
        .sorted('date', true);
      if(offlineTasks.length > 0){
        while (offlineTasks.length > 0) {
          const task = offlineTasks[0];
          realm.write(() => {
            realm.delete(task);
          });
        }
      }
      //清空数据源后写入最新的数据（只保留最新数据）
      realm.write(() => {
        realm.create('OfflineTask', {
          type: 'OfflineNavigationRecord',
          data: JSON.stringify(action.payload),
          date: new Date().getTime(),
        });
      });
      yield put(createNavigationRecordRoutine.success({}));
    }
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(createNavigationRecordRoutine.failure(e));
  } finally {
    yield put(createNavigationRecordRoutine.fulfill());
  }
}

export function* createNavigationRecordListSaga() {
  yield takeLatest(createNavigationRecordRoutine.TRIGGER, createNavigationRecordList);
}

// All sagas to be loaded
export default [
  getNavigationRecordListSaga,
  createNavigationRecordListSaga,
];
