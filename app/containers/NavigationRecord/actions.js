import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  GET_NAVIGATION_RECORD_ACTION,
  CREATE_NAVIGATION_RECORD_ACTION,
} from './constants';

export const getNavigationRecordListRoutine = createRoutine(
  GET_NAVIGATION_RECORD_ACTION,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const getNavigationRecordListPromise = promisifyRoutine(getNavigationRecordListRoutine);

export const createNavigationRecordRoutine = createRoutine(
  CREATE_NAVIGATION_RECORD_ACTION,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const createNavigationRecordPromise = promisifyRoutine(createNavigationRecordRoutine);
