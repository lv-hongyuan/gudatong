import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Keyboard, AsyncStorage,TouchableOpacity ,Clipboard} from 'react-native';
import { connect } from 'react-redux';
import {
  Container,
  Content,
  InputGroup,
  Item,
  Label,
  View,
  Text,
} from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import _ from 'lodash';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import StorageKeys from '../../common/StorageKeys';
import myTheme from '../../Themes';
import AppStorage from '../../utils/AppStorage';
import { makeFirstStateTime } from '../ShipWork/selectors';
import { createNavigationRecordPromise, getNavigationRecordListPromise } from './actions';
import { makeSelectNavigationRecord } from './selectors';
import InputItem from '../../components/InputItem';
import { defaultFormat } from '../../utils/DateFormat';
import DatePullSelector from '../../components/DatePullSelector';
import commonStyles from '../../common/commonStyles';
import screenHOC from '../screenHOC';
import Selector from '../../components/Selector';
import CoordinateInput from '../../components/CoordinateInput';
import {
  positionReportTypeList,
  // surgeLevelList,
  waveList,
  weatherList,
  windDirectionList,
  windPowerList,
  windScaleList,
} from '../../common/Constant';
import AlertView from '../../components/Alert';
import { MaskType, TextInput } from '../../components/InputItem/TextInput';
import stringToNumber from '../../utils/stringToNumber';
import { ROUTE_HISTORY_RECORD } from '../../RouteConstant';
import HeaderButtons from 'react-navigation-header-buttons';
import { NavigationActions } from 'react-navigation';
const styles = StyleSheet.create({
  checkBox: {
    borderRadius: 1,
    height: 14,
    width: 14,
    left: 0,
  },
  bottomTip: {
    textAlign: 'center',
    padding: 8,
    fontSize: 12,
    color: '#969696',
  },
});

/**
 * 船报页面
 * Created by jianzhexu on 2018/3/23
 */
@screenHOC
class NavigationRecord extends React.PureComponent {
  static navigationOptions = ({ navigation }) => ({
    title: '更新船报',
    headerRight: (
      <HeaderButtons>
        {navigation.state.params.isConnected ? 
          <HeaderButtons.Item
          title="历史船报"
          buttonStyle={{ fontSize: 14, color: '#ffffff' }}
          onPress={() => {
            navigation.navigate(ROUTE_HISTORY_RECORD);
          }}
        /> : <HeaderButtons.Item
          title="复制"
          buttonStyle={{ fontSize: 14, color: '#ffffff' }}
          onPress={() => {
            const copysubmit = navigation.getParam('copySubmit');
            copysubmit()
          }}
        />
        }
      </HeaderButtons>
    ),
  });

  constructor(props) {
    super(props);
    // 已航时间
    // let totalTime;
    // if (props.firstStateTime) {
    //   totalTime = ((new Date().getTime() - props.firstStateTime) / 3600000).toFixed(1);
    // }

    this.state = {
      voyageId: undefined,            //航次ID
      shipName: undefined,  //船名
      voyageCode: undefined,          //航次号
      line: undefined,                //航段
      // positionReportType: undefined,  //船报类型
      longD: undefined,
      longF: undefined,
      latD: undefined,
      latF: undefined,
      windPower: undefined,           //风力
      windPowerValue: '',
      waveValue:'',
      windDirection: undefined,       //风向
      mainEngineSpeed: undefined,     //主机转速
      aveSpeed: undefined,            //平均航速
      wave: undefined,                //海浪
      course: '0',              //航向
      storageShip180: '0',      //180#重油
      storageShip120: '0',      //120#重油
      storageShip0: '0',        //0#轻油
      mainEngineOil: '0',       //主机滑油
      auxiliaryEngineOil: '0',  //辅机滑油
      cylinderOil: '0',         //汽缸油
      storageWater: '0',        //淡水
      maxDraught: '0',          //抵港最大吃水
      toNextPortDistance: '0',  //剩余航程
      nextPortEta: undefined,         //下港ETA
      memo: undefined,                 //备注
      notes: '检查集装箱绑扎系固正常',               //检查集装箱绑扎系固正常（默认，可编辑）
    };
    this.props.navigation.setParams({ submit: this.handleSubmit });
  }

  formatDate(now) { 
    var year=now.getFullYear() ? now.getFullYear() : '0000';  //取得4位数的年份
    var month=now.getMonth()+1 ? now.getMonth()+1 : '00';  //取得日期中的月份，其中0表示1月，11表示12月
    var date=now.getDate() ? now.getDate() : '00';      //返回日期月份中的天数（1到31）
    var hour=now.getHours() ? now.getHours() : '00';     //返回日期中的小时数（0到23）
    var minute=now.getMinutes() ? now.getMinutes() : '00'; //返回日期中的分钟数（0到59）
    return year+"-"+month+"-"+date+" "+hour+":"+minute 
  } 
    
  //一键复制
  copySubmitFunction (){
    var dnextPortEta=new Date(this.state.nextPortEta);
    let detailstr =
            `船名:${this.state.shipName}，
航次：${this.state.voyageCode}，
航线：${this.state.line}，
纬度：N ${this.state.latD ? this.state.latD : '0'}° ${this.state.latF ? this.state.latF : '0'}'，
经度：E ${this.state.longD ? this.state.longD : '0'}° ${this.state.longF ? this.state.longF : '0'}'，
风向：${this.state.windDirection ? this.state.windDirection : ''}，
风力：${this.state.windPowerValue}，
航向：${this.state.course}，
海浪：${this.state.waveValue}，
主机转速：${this.state.mainEngineSpeed ? this.state.mainEngineSpeed : '0'} 转，
平均航速：${this.state.aveSpeed ? this.state.aveSpeed : '0'} 节，
180#重油：${this.state.storageShip180} 吨，
120#重油：${this.state.storageShip120} 吨，
0#轻油：${this.state.storageShip0} 吨，
主机滑油：${this.state.mainEngineOil} 吨，
辅机滑油：${this.state.auxiliaryEngineOil} 吨，
汽缸油：${this.state.cylinderOil} 吨，
淡水：${this.state.storageWater} 吨，
抵港最大吃水：${this.state.maxDraught ? this.state.maxDraught : '0'} 米，
剩余航程：${this.state.toNextPortDistance ? this.state.toNextPortDistance : '0'} 海里，
ETA：${this.formatDate(dnextPortEta)}，
备注： ${this.state.memo ? this.state.memo : ''}，
${this.state.notes ? this.state.notes : ''}`
        console.log(detailstr);
        Clipboard.setString(detailstr);
  }

  componentDidMount() {
    this.props.navigation.setParams({
      copySubmit: () => {
        this.copySubmitFunction();
        AlertView.show({
          message: '复制成功，可以直接粘贴'
        });
      }
    });
    this.props.getNavigationRecordList()
      .then((data) => {
        this.setState({
          windPower:data.windPower,
          wave:data.wave
        })
      })
      .catch(() => { });
    if (!this.props.isConnected) {
      AsyncStorage.getItem(StorageKeys.DYN)
        .then((str) => {
          if (str) {
            const dyn = JSON.parse(str);
            console.log(dyn);
            if(dyn.shipName != this.state.shipName){
              this.setState({
                longD: undefined,
                longF: undefined,
                latD: undefined,
                latF: undefined,
                windPower: undefined,           //风力
                windPowerValue: '',
                waveValue:'',
                windDirection: undefined,       //风向
                mainEngineSpeed: undefined,     //主机转速
                aveSpeed: undefined,            //平均航速
                wave: undefined,                //海浪
                course: '0',              //航向
                storageShip180: '0',      //180#重油
                storageShip120: '0',      //120#重油
                storageShip0: '0',        //0#轻油
                mainEngineOil: '0',       //主机滑油
                auxiliaryEngineOil: '0',  //辅机滑油
                cylinderOil: '0',         //汽缸油
                storageWater: '0',        //淡水
                maxDraught: '0',          //抵港最大吃水
                toNextPortDistance: '0',  //剩余航程
                nextPortEta: undefined,         //下港ETA
                memo: undefined,                 //备注
                notes: '检查集装箱绑扎系固正常',               //检查集装箱绑扎系固正常（默认，可编辑）
              })
            }
            this.setState({
              shipName: dyn.shipName || this.state.shipName,
              line: dyn.lineName || this.state.line,
              voyageCode: dyn.voyageCode || this.state.voyageCode,
            });
          }
        })
        .catch(() => { });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.data !== this.props.data) {
      const data = {};
      Object.keys(nextProps.data).forEach((key) => {
        let value;
        switch (key) {
          case 'shipName':
            value = nextProps.data[key] || this.state.shipName;
            break;
          case 'voyageCode':
            value = nextProps.data[key] || this.state.voyageCode;
            break;
          case 'line':
            value = nextProps.data[key] || this.state.line;
            break;
          default:
            value = nextProps.data[key];
            break;
        }
        data[key] = value === 0 ? null : value;
      });
      // 重置船报类型，每次都需要重新选择
      // data.positionReportType = undefined;
      this.setState({
        ...data,
        notes: data.notes || "检查集装箱绑扎系固正常",
      });
    }
  }

  handleSubmit = () => {
    // this.props.navigation.navigate(ROUTE_HISTORY_RECORD);
    // return
    Keyboard.dismiss();

    const { createNavigationRecord, navigation } = this.props;
    const {
      voyageId, shipName, voyageCode, line, longD, longF, latD, latF, windPower, windDirection,
      mainEngineSpeed, aveSpeed, wave, course, storageShip180, storageShip120, storageShip0, mainEngineOil, auxiliaryEngineOil,
      cylinderOil, storageWater, maxDraught, toNextPortDistance, nextPortEta, memo, notes
    } = this.state;

    // if (!positionReportType) {
    //   AlertView.show({ message: '请选择船报类型' });
    //   return;
    // }
    if (_.isNil(longD) || longD == '') {
      AlertView.show({ message: '请填写经度' });
      return;
    }
    if (_.isNil(latD) || latD == '') {
      AlertView.show({ message: '请填写纬度' });
      return;
    }
    if((stringToNumber(latD) + stringToNumber(latF) / 60 > 90) || (stringToNumber(latF) >= 60)){
      AlertView.show({ message: '纬度输入有误！' });
      return;
    }
    if((stringToNumber(longD) + stringToNumber(longF) / 60 > 180) || (stringToNumber(longF) >= 60)){
      AlertView.show({ message: '经度输入有误！'});
      return;
    }
    if (!windDirection) {
      AlertView.show({ message: '请选择风向' });
      return;
    }
    if (windPower == null) {
      AlertView.show({ message: '请选择风力' });
      return;
    }
    if (!wave && wave!= 0) {
      AlertView.show({ message: '请选择浪级' });
      return;
    }
    if (_.isNil(mainEngineSpeed) || parseFloat(mainEngineSpeed) === 0) {
      AlertView.show({ message: '请填写主机转速' });
      return;
    }
    if (_.isNil(aveSpeed) || parseFloat(aveSpeed) === 0) {
      AlertView.show({ message: '请填写平均航速' });
      return;
    }
    if (!nextPortEta) {
      AlertView.show({ message: '请选择下港ETA' });
      return;
    }

    function doSubmit() {
      const param = {
        opTime: new Date().getTime(),
        voyageId,
        voyageCode,
        shipName,
        line,
        // positionReportType,
        longD: stringToNumber(longD),
        longF: stringToNumber(longF),
        latD: stringToNumber(latD),
        latF: stringToNumber(latF),
        windPower,
        windDirection,
        mainEngineSpeed,
        aveSpeed,
        wave,
        course:course,
        storageShip180: stringToNumber(storageShip180),
        storageShip120: stringToNumber(storageShip120),
        storageShip0: stringToNumber(storageShip0),
        mainEngineOil: stringToNumber(mainEngineOil),
        auxiliaryEngineOil: stringToNumber(auxiliaryEngineOil),
        cylinderOil: stringToNumber(cylinderOil),
        storageWater: stringToNumber(storageWater),
        maxDraught: stringToNumber(maxDraught),
        toNextPortDistance: stringToNumber(toNextPortDistance),
        nextPortEta,
        memo,
        notes,
      };
      createNavigationRecord(param)
        .then(() => {
          const goBackCallback = navigation.getParam('onGoBack', undefined);
          if (goBackCallback) {
            goBackCallback();
          }
            if (this.props.isConnected) {
              navigation.navigate(ROUTE_HISTORY_RECORD);
            } else {
              AlertView.show({
                message: '航行船报已保存至本地，将在网络打开时上传至服务器。',
                showCancel: false,
                confirmAction: () => {
                  navigation.dispatch(NavigationActions.back());
                },
              });
            }
        })
        .catch(() => { });
    }

    // 检查ETA时间差
    if (this.props.data.nextPortEta && Math.abs(this.props.data.nextPortEta - nextPortEta) > 10800000) {
      AlertView.show({
        message: '下港ETA的时间修改超过3小时，确定提交吗？',
        showCancel: true,
        confirmAction: () => {
          doSubmit.bind(this)();
        },
      });
    } else {
      AlertView.show({
        message: '确定提交吗？',
        showCancel: true,
        confirmAction: () => {
          doSubmit.bind(this)();
        },
      });
      // doSubmit.bind(this)();
    }
  };

  render() {
    return (
      <Container>
        <Content theme={myTheme} contentInsetAdjustmentBehavior="scrollableAxes">
          <View style={{
            padding: 10,
            backgroundColor: '#FFFFFF',
          }}
          >
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="船名:"
                value={this.state.shipName}
                placeholder=""
                editable={false}
              />
              <InputItem
                label="航次:"
                value={this.state.voyageCode}
                placeholder=""
                editable={false}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="航线:"
                value={this.state.line}
                placeholder=""
                editable={false}
              />
            </InputGroup>
            {/* <InputGroup style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={{ color: 'red' }}>*</Label>
                <Label style={commonStyles.inputLabel}>船位报:</Label>
                <Selector
                  value={this.state.positionReportType}
                  items={positionReportTypeList}
                  pickerTitle="请选择船报类型"
                  getItemValue={item => item.id}
                  getItemText={item => item.value}
                  // pickerType="popover"
                  onSelected={(item) => {
                    this.setState({ positionReportType: item.id });
                  }}
                />
              </Item>
            </InputGroup> */}
            <InputGroup style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={{ color: 'red' }}>*</Label>
                <Label style={commonStyles.inputLabel}>纬度:</Label>
                <CoordinateInput
                  type="la"
                  degrees={this.state.latD}
                  points={this.state.latF}
                  onChangeValue={(degrees, points) => {
                    this.setState({ latD: degrees, latF: points });
                  }}
                />
              </Item>
              <Item style={commonStyles.inputItem}>
                <Label style={{ color: 'red' }}>*</Label>
                <Label style={commonStyles.inputLabel}>经度:</Label>
                <CoordinateInput
                  type="lo"
                  degrees={this.state.longD}
                  points={this.state.longF}
                  onChangeValue={(degrees, points) => {
                    this.setState({ longD: degrees, longF: points });
                  }}
                />
              </Item>
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={{ color: 'red' }}>*</Label>
                <Label style={commonStyles.inputLabel}>风向:</Label>
                <Selector
                  value={this.state.windDirection}
                  items={windDirectionList}
                  pickerTitle="请选择风向"
                  getItemValue={item => item}
                  getItemText={item => item}
                  pickerType="modal"
                  onSelected={(item) => {
                    this.setState({ windDirection: item });
                  }}
                />
              </Item>
              <Item style={commonStyles.inputItem}>
                <Label style={{ color: 'red' }}>*</Label>
                <Label style={commonStyles.inputLabel}>风力:</Label>
                <Selector
                  value={this.state.windPower}
                  items={windPowerList}
                  pickerTitle="请选择风力"
                  getItemValue={item => item.id}
                  getItemText={item => item.value}
                  pickerType="modal"
                  onSelected={(item) => {
                    this.setState({ 
                      windPower: item.id ,
                      windPowerValue:item.value
                    });
                  }}
                />
              </Item>
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                isRequired
                label="航向:"
                onChangeText={(text) => {
                  // const newText = text.replace(/[^\w\.\/]/ig, '');

                  // console.log('newText===>>>', newText)
                  this.setState({ course: text })
                }}
                placeholder='0'
                value={this.state.course}
                maskType={MaskType.ALPHANUM}
                keyboardType="numeric"
              // maskType={MaskType.FLOAT}
              />

              <Item style={commonStyles.inputItem}>
                <Label style={{ color: 'red' }}>*</Label>
                <Label style={commonStyles.inputLabel}>海浪:</Label>
                <Selector
                  value={this.state.wave}
                  items={waveList}
                  pickerTitle="请选择浪级"
                  getItemValue={item => item.id}
                  getItemText={item => item.value}
                  pickerType="modal"
                  onSelected={(item) => {
                    console.log('浪级:', item.id)
                    this.setState({ 
                      wave: item.id ,
                      waveValue:item.value
                    });
                  }}
                />
              </Item>
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                isRequired
                label="主机转速:"
                value={this.state.mainEngineSpeed}
                onChangeText={(text) => {
                  this.setState({ mainEngineSpeed: text });
                }}
                placeholder='0'
                keyboardType="numeric"
                maskType={MaskType.INTEGER}
                rightItem={<Text style={commonStyles.tail}>转</Text>}
              />
              <InputItem
                isRequired
                label="平均航速:"
                value={this.state.aveSpeed}
                onChangeText={(text) => {
                  this.setState({ aveSpeed: text });
                }}
                placeholder='0'
                keyboardType="numeric"
                maskType={MaskType.FLOAT}
                rightItem={<Text style={commonStyles.tail}>节</Text>}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                isRequired
                label="180#重油:"
                value={this.state.storageShip180}
                onChangeText={(text) => {
                  this.setState({ storageShip180: text });
                }}
                placeholder='0'
                keyboardType="numeric"
                maskType={MaskType.FLOAT}
                rightItem={<Text style={commonStyles.tail}>吨</Text>}
              />
              <InputItem
                isRequired
                label="120#重油:"
                value={this.state.storageShip120}
                onChangeText={(text) => {
                  this.setState({ storageShip120: text });
                }}
                placeholder='0'
                keyboardType="numeric"
                maskType={MaskType.FLOAT}
                rightItem={<Text style={commonStyles.tail}>吨</Text>}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                isRequired
                label="0#轻油:"
                value={this.state.storageShip0}
                onChangeText={(text) => {
                  this.setState({ storageShip0: text });
                }}
                placeholder='0'
                keyboardType="numeric"
                maskType={MaskType.FLOAT}
                rightItem={<Text style={commonStyles.tail}>吨</Text>}
              />
              <InputItem
                isRequired
                label="主机滑油:"
                value={this.state.mainEngineOil}
                onChangeText={(text) => {
                  this.setState({ mainEngineOil: text });
                }}
                placeholder='0'
                keyboardType="numeric"
                maskType={MaskType.FLOAT}
                rightItem={<Text style={commonStyles.tail}>吨</Text>}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                isRequired
                label="辅机滑油:"
                value={this.state.auxiliaryEngineOil}
                onChangeText={(text) => {
                  this.setState({ auxiliaryEngineOil: text });
                }}
                placeholder='0'
                keyboardType="numeric"
                maskType={MaskType.FLOAT}
                rightItem={<Text style={commonStyles.tail}>吨</Text>}
              />
              <InputItem
                isRequired
                label="汽缸油:"
                value={this.state.cylinderOil}
                onChangeText={(text) => {
                  this.setState({ cylinderOil: text });
                }}
                placeholder='0'
                keyboardType="numeric"
                maskType={MaskType.FLOAT}
                rightItem={<Text style={commonStyles.tail}>吨</Text>}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                isRequired
                label="淡水:"
                value={this.state.storageWater}
                onChangeText={(text) => {
                  this.setState({ storageWater: text });
                }}
                placeholder='0'
                keyboardType="numeric"
                maskType={MaskType.FLOAT}
                rightItem={<Text style={commonStyles.tail}>吨</Text>}
              />
              <InputItem
                isRequired
                label="抵港最大吃水:"
                value={this.state.maxDraught}
                onChangeText={(text) => {
                  this.setState({ maxDraught: text });
                }}
                placeholder='0'
                keyboardType="numeric"
                maskType={MaskType.FLOAT}
                rightItem={<Text style={commonStyles.tail}>米</Text>}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                isRequired
                label="剩余航程:"
                value={this.state.toNextPortDistance}
                onChangeText={(text) => {
                  this.setState({ toNextPortDistance: text });
                }}
                placeholder='0'
                keyboardType="numeric"
                maskType={MaskType.FLOAT}
                rightItem={<Text style={commonStyles.tail}>海里</Text>}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <Item style={[commonStyles.inputItem, { borderBottomWidth: myTheme.borderWidth }]}>
                <Label style={{ color: 'red' }}>*</Label>
                <Label style={[commonStyles.inputLabel, { color: 'red' }]}>ETA:</Label>
                <DatePullSelector
                  value={this.state.nextPortEta || null}
                  onChangeValue={(value) => {
                    this.setState({ nextPortEta: value });
                  }}
                />
              </Item>
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="备注:"
                value={this.state.memo}
                placeholder={'无'}
                editable={true}
                onChangeText={(text) => {
                  this.setState({ memo: text });
                }}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                // label="总体备注:"
                value={this.state.notes}
                placeholder=''
                onChangeText={(text) => {
                  this.setState({ notes: text });
                }}
              />
            </InputGroup>
            <TouchableOpacity
              style={{
                height:45,
                backgroundColor:'rgb(217,10,37)',
                width:'100%',
                borderRadius:5,
                justifyContent:'center',
                alignItems:'center',
                marginTop:10
              }}
              onPress={() => {
                const handleSubmitFunc = this.props.navigation.getParam('submit', undefined);
                if (handleSubmitFunc) {
                  requestAnimationFrame(() => {
                    handleSubmitFunc();
                  });
                }
              }}
            >
              <Text style={{color:'#fff',fontSize:15}}>提交</Text>
            </TouchableOpacity>
          </View>
          <Text style={styles.bottomTip}>———— 已经到底了 ————</Text>
        </Content>
      </Container>
    );
  }
}

NavigationRecord.propTypes = {
  navigation: PropTypes.object.isRequired,
  createNavigationRecord: PropTypes.func.isRequired,
  getNavigationRecordList: PropTypes.func.isRequired,
  data: PropTypes.object,
  firstStateTime: PropTypes.number,
  isConnected: PropTypes.bool.isRequired,
};
NavigationRecord.defaultProps = {
  data: {},
  firstStateTime: undefined,
};

const mapStateToProps = createStructuredSelector({
  data: makeSelectNavigationRecord(),
  firstStateTime: makeFirstStateTime(),
  isConnected: state => state.network.isConnected,
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getNavigationRecordList: getNavigationRecordListPromise,
      createNavigationRecord: createNavigationRecordPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(NavigationRecord);
