import { createSelector } from 'reselect/es';

const selectNavigationRecordDomain = () => state => state.navigationRecord;

const makeSelectNavigationRecord = () => createSelector(
  selectNavigationRecordDomain(),
  (subState) => {
    console.debug(subState.data);
    return subState.data;
  },
);

export {
  selectNavigationRecordDomain,
  makeSelectNavigationRecord,
};
