/*
 *
 * NavigationRecord constants
 *
 */
export const GET_NAVIGATION_RECORD_ACTION = 'app/SailingReport/GET_SAILING_REPORT_ACTION';

export const CREATE_NAVIGATION_RECORD_ACTION = 'app/SailingReport/CREATE_SAILING_REPORT_ACTION';
