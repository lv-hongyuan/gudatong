import { getNavigationRecordListRoutine, createNavigationRecordRoutine } from './actions';

const initState = {
  success: false,
  data: {},
};

export default function (state = initState, action) {
  switch (action.type) {
    // 发送验证码
    case getNavigationRecordListRoutine.TRIGGER:
      return { ...state, loading: true };
    case getNavigationRecordListRoutine.SUCCESS:
      return { ...state, data: action.payload };
    case getNavigationRecordListRoutine.FAILURE:
      return { ...state, error: action.payload };
    case getNavigationRecordListRoutine.FULFILL:
      return { ...state, loading: false };

    // 登录
    case createNavigationRecordRoutine.TRIGGER:
      return { ...state, loading: true };
    case createNavigationRecordRoutine.SUCCESS:
      return { ...state };
    case createNavigationRecordRoutine.FAILURE:
      return { ...state, error: action.payload };
    case createNavigationRecordRoutine.FULFILL:
      return { ...state, loading: false };

    default:
      return state;
  }
}
