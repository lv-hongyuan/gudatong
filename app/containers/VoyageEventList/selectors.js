import { createSelector } from 'reselect/es';

const selectVoyageEventDomain = () => state => state.voyageEvent;

const makeSelectVoyageEventList = () => createSelector(
  selectVoyageEventDomain(),
  (subState) => {
    console.debug(subState.data);
    return subState.data;
  },
);

export {
  makeSelectVoyageEventList,
};
