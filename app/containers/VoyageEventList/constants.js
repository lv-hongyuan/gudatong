/*
 *
 * VoyageEventList constants
 *
 */
export const GET_VOYAGE_EVENT_LIST_ACTION = 'app/VoyageEventList/GET_VOYAGE_EVENT_LIST_ACTION';

export const DELETE_VOYAGE_EVENT_ACTION = 'app/VoyageEventList/DELETE_VOYAGE_EVENT_ACTION';

export const UPLOAD_ALL_OFFLINE_VOYAGE_EVENT_ACTION = 'app/VoyageEventList/UPLOAD_ALL_OFFLINE_VOYAGE_EVENT_ACTION';
