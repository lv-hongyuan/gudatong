import { getVoyageListRoutine, deleteVoyageEventRoutine } from './actions';

const initState = {
  data: [],
};

export default function (state = initState, action) {
  switch (action.type) {
    case getVoyageListRoutine.TRIGGER:
      return { ...state, loading: true };
    case getVoyageListRoutine.SUCCESS:
      return { ...state, data: action.payload || [] };
    case getVoyageListRoutine.FAILURE:
      return { ...state, error: action.payload };
    case getVoyageListRoutine.FULFILL:
      return { ...state, loading: false };

    case deleteVoyageEventRoutine.TRIGGER:
      return { ...state, loading: true };
    case deleteVoyageEventRoutine.SUCCESS:
      return { ...state };
    case deleteVoyageEventRoutine.FAILURE: {
      const eventItem = action.payload;
      let newList = [];
      if (eventItem) {
        newList = state.list.filter((item) => {
          if (item.id !== undefined && eventItem.id !== undefined
            && item.id !== null && eventItem.id !== null) {
            return item.id !== eventItem.id;
          }
          if (item.dbId !== undefined && eventItem.dbId !== undefined
            && item.dbId !== null && eventItem.dbId !== null) {
            return item.dbId !== eventItem.dbId;
          }
          return true;
        });
      } else {
        newList = state.list;
      }
      return { ...state, list: newList, isLoading: false };
    }
    case deleteVoyageEventRoutine.FULFILL:
      return { ...state, loading: false };

    default:
      return state;
  }
}
