import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  GET_VOYAGE_EVENT_LIST_ACTION,
  DELETE_VOYAGE_EVENT_ACTION,
  UPLOAD_ALL_OFFLINE_VOYAGE_EVENT_ACTION,
} from './constants';

export const getVoyageListRoutine = createRoutine(
  GET_VOYAGE_EVENT_LIST_ACTION,
  updates => updates,
  () => ({ globalLoading: false }),
);
export const getVoyageListPromise = promisifyRoutine(getVoyageListRoutine);

export const deleteVoyageEventRoutine = createRoutine(
  DELETE_VOYAGE_EVENT_ACTION,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const deleteVoyageEventPromise = promisifyRoutine(deleteVoyageEventRoutine);

export const uploadAllVoyageEventRoutine = createRoutine(
  UPLOAD_ALL_OFFLINE_VOYAGE_EVENT_ACTION,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const uploadAllVoyageEventPromise = promisifyRoutine(uploadAllVoyageEventRoutine);
