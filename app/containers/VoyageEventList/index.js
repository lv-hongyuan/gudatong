import React from 'react';
import PropTypes from 'prop-types';
import { FlatList, RefreshControl } from 'react-native';
import { connect } from 'react-redux';
import { Container, View } from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import HeaderButtons from 'react-navigation-header-buttons';
import { ROUTE_VOYAGE_EVENT } from '../../RouteConstant';
import {
  getVoyageListPromise,
  deleteVoyageEventPromise,
} from './actions';
import { errorMessage } from '../ErrorHandler/actions';
import myTheme from '../../Themes';
import { makeSelectVoyageEventList } from './selectors';
import EmptyView from '../../components/EmptyView';
import screenHOC from '../screenHOC';
import VoyageEventItem from '../../components/VoyageEventItem';

/**
 * 停航原因列表页面
 * Created by jianzhexu on 2018/3/29
 */
@screenHOC
class VoyageEventList extends React.PureComponent {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.getParam('title', '途中停航原因'),
    headerRight: (
      <HeaderButtons>
        <HeaderButtons.Item
          title="创建"
          buttonStyle={{ fontSize: 14, color: '#ffffff' }}
          onPress={navigation.getParam('onBtnCreate')}
        />
      </HeaderButtons>
    ),
  });
  static propTypes = {
    navigation: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    getVoyageList: PropTypes.func.isRequired,
    deleteVoyageEvent: PropTypes.func.isRequired,
    data: PropTypes.array.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
    };
    this.props.navigation.setParams({ onBtnCreate: this.onBtnCreate.bind(this) });
  }

  componentDidMount() {
    this.reLoadingPage();
  }

  onBtnCreate = () => {
    this.props.navigation.navigate(ROUTE_VOYAGE_EVENT, {
      title: '创建途中停航原因',
      onGoBack: () => {
        this.reLoadingPage();
      },
      isEdit: false,
    });
  };

  reLoadingPage = () => {
    requestAnimationFrame(() => {
      this.props.getVoyageList()
        .finally(() => {
          this.setState({ refreshing: false });
        });
    });
  };

  itemPress = (data) => {
    //事件结束后,不可编辑
    // if (data.endTime > 0) {
    //   this.props.dispatch(errorMessage('事件已结束，无法编辑'));
    //   return;
    // }
    this.props.navigation.navigate(ROUTE_VOYAGE_EVENT, {
      title: '编辑途中停航原因',
      onGoBack: () => {
        this.reLoadingPage();
      },
      isEdit: true,
      data,
    });
  };

  deleteItem = (data) => {
    if (data.endTime > 0) {
      this.props.dispatch(errorMessage('事件已结束，无法删除'));
      return;
    }
    this.props.deleteVoyageEvent(data)
      .then(() => {
        this.reLoadingPage();
      })
      .catch(() => {
      });
  };

  renderItem = ({ item }) => (
    <VoyageEventItem
      describe={item.describe}
      typeChildCode={item.typeChildCode}
      typeChildText={item.typeChildText}
      startTime={item.startTime}
      endTime={item.endTime}
      hidePlace={item.hidePlace}
      portName={item.portName}
      longD={item.longD}
      longF={item.longF}
      latD={item.latD}
      latF={item.latF}
      onDelete={this.deleteItem}
      onItemPress={this.itemPress}
      popId={item.popId}
      dbId={item.dbId}
      id={item.id}
    />
  );

  renderEmptyView = () => (<EmptyView />);

  render() {
    return (
      <Container theme={myTheme}>
        <FlatList
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => {
                this.setState({ refreshing: true }, () => {
                  this.reLoadingPage();
                });
              }}
            />
          }
          contentInsetAdjustmentBehavior="scrollableAxes"
          style={{ flex: 1, backgroundColor: '#e0e0e0' }}
          keyExtractor={item => `${item.id || item.dbId}`}
          data={this.props.data}
          ListEmptyComponent={this.renderEmptyView}
          initialNumToRender={10}
          renderItem={this.renderItem}
          ItemSeparatorComponent={() => (<View style={{ height: 5 }} />)}
          ListFooterComponent={() => (<View style={{ height: 5 }} />)}
          ListHeaderComponent={() => (<View style={{ height: 5 }} />)}
        />
      </Container>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  data: makeSelectVoyageEventList(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getVoyageList: getVoyageListPromise,
      deleteVoyageEvent: deleteVoyageEventPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(VoyageEventList);
