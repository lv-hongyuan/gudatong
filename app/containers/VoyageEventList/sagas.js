import { call, put, select, takeLatest } from 'redux-saga/effects';
import { ShipEventRange } from '../../common/Constant';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import {
  getVoyageListRoutine,
  deleteVoyageEventRoutine,
  uploadAllVoyageEventRoutine,
} from './actions';
import { errorMessage } from '../ErrorHandler/actions';
import { getPopId, getVoyageCode } from '../ShipWork/sagas';
import realm from '../../common/realm';

const _isConnected = state => state.network.isConnected;

function* getVoyageEventList(action) {
  console.log(action);
  try {
    yield put(getVoyageListRoutine.request());
    const isConnected = yield select(_isConnected);
    if (isConnected) {
      const popId = yield call(getPopId);
      const response = yield call(request, ApiFactory.findShipEvents(popId, ShipEventRange.SHIP_SAILING));
      console.log('getVoyageEventList', response.toString());
      yield put(getVoyageListRoutine.success(response.dtoList));
    } else {
      const OfflineVoyageEvents = realm.objects('OfflineVoyageEvent')
        .sorted('date', true)
        .map((item) => {
          const event = JSON.parse(item.data);
          event.dbId = item.id;
          return event;
        });
      console.log('getOfflineVoyageEventList', OfflineVoyageEvents);
      yield put(getVoyageListRoutine.success(OfflineVoyageEvents));
    }
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(getVoyageListRoutine.failure(e));
  } finally {
    yield put(getVoyageListRoutine.fulfill());
  }
}

export function* getVoyageEventListSaga() {
  yield takeLatest(getVoyageListRoutine.TRIGGER, getVoyageEventList);
}

function* deleteVoyageEvent(action) {
  console.log(action);
  try {
    yield put(deleteVoyageEventRoutine.request());
    const isConnected = yield select(_isConnected);
    const offLineTask = Number.isInteger(action.payload.dbId) && realm.objects('OfflineVoyageEvent').filtered(`id = ${action.payload.dbId}`)[0];
    if (isConnected) {
      const response = yield call(request, ApiFactory.deleteShipEvent(action.payload.id));
      console.log('deleteVoyageEvent', response.toString());
    }
    if (offLineTask) {
      realm.write(() => {
        realm.delete(offLineTask);
      });
    }
    yield put(deleteVoyageEventRoutine.success(action.payload));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(deleteVoyageEventRoutine.failure(e));
  } finally {
    yield put(deleteVoyageEventRoutine.fulfill());
  }
}

export function* deleteVoyageEventSaga() {
  yield takeLatest(deleteVoyageEventRoutine.TRIGGER, deleteVoyageEvent);
}

function* uploadAllOfflineVoyageEvent(action) {
  console.log(action);
  try {
    yield put(uploadAllVoyageEventRoutine.request());
    const offlineTasks = realm.objects('OfflineVoyageEvent')
      .sorted('date', true);

    if (offlineTasks.length === 0) {
      yield put(uploadAllVoyageEventRoutine.success());
      return;
    }

    // 获取popId，shipId
    const popId = yield call(getPopId);
    const voyageCode = yield call(getVoyageCode);

    // 上传离线数据
    while (offlineTasks.length > 0) {
      const task = offlineTasks[0];
      const record = {
        ...JSON.parse(task.data),
        ...{
          popId,
          voyageCode,
        },
      };
      yield call(request, ApiFactory.createShipEvent(record));
      realm.write(() => {
        realm.delete(task);
      });
    }
    yield put(uploadAllVoyageEventRoutine.success());
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(uploadAllVoyageEventRoutine.failure(e));
  } finally {
    yield put(uploadAllVoyageEventRoutine.fulfill());
  }
}

export function* uploadAllOfflineVoyageEventSaga() {
  yield takeLatest(uploadAllVoyageEventRoutine.TRIGGER, uploadAllOfflineVoyageEvent);
}

// All sagas to be loaded
export default [
  getVoyageEventListSaga,
  deleteVoyageEventSaga,
  uploadAllOfflineVoyageEventSaga,
];
