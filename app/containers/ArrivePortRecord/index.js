import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Keyboard, AsyncStorage, TouchableOpacity, NetInfo ,Clipboard} from 'react-native';
import { connect } from 'react-redux';
import { Container, Content, InputGroup, Item, Label, View, Text, } from 'native-base';
import _ from 'lodash';
import StorageKeys from '../../common/StorageKeys';
import myTheme from '../../Themes';
import AppStorage from '../../utils/AppStorage';
import { createStructuredSelector } from 'reselect/es';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { makeFirstStateTime } from '../ShipWork/selectors';
import { getArrivePortRecordPromise, createArrivePortRecordPromise } from './actions';
import stringToNumber from '../../utils/stringToNumber';
import { makeArrivePortRecord } from './selectors';
import InputItem from '../../components/InputItem';
import commonStyles from '../../common/commonStyles';
import screenHOC from '../screenHOC';
import { ROUTE_ARRIVEPORT_RECORD_LIST } from '../../RouteConstant';
import HeaderButtons from 'react-navigation-header-buttons';
import { MaskType, TextInput } from '../../components/InputItem/TextInput'
import DatePullSelector from '../../components/DatePullSelector';
import AlertView from '../../components/Alert';
import { NavigationActions } from 'react-navigation';
import { MapView, Location } from 'react-native-baidumap-sdk';

/**
 * 抵港确报页面
 * Created by hongyuanwang on 2019/12/04
 */
@screenHOC
class ArrivePortRecord extends React.PureComponent {
  static navigationOptions = ({ navigation }) => ({
    title: '抵港确报',
    headerRight: (
      <HeaderButtons>
        {navigation.state.params.isConnected ?
          <HeaderButtons.Item
            title="历史确报"
            buttonStyle={{ fontSize: 14, color: '#ffffff' }}
            onPress={() => {
              navigation.navigate(ROUTE_ARRIVEPORT_RECORD_LIST);
            }}
          /> : <HeaderButtons.Item
            title="复制"
            buttonStyle={{ fontSize: 14, color: '#ffffff' }}
            onPress={() => {
              const copysubmit = navigation.getParam('copySubmit');
              copysubmit()
            }}
          />
        }
      </HeaderButtons>
    ),
  });
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      voyageId: undefined,             //航次ID
      shipName: undefined,   //船名
      voyageCode: undefined,           //航次号
      lineName: undefined,                 //航线
      exactLocation: '',             //抵港位置
      nextPortEta: '',              //抵港时间
      draughtFrontB: '',                   //艏吃水
      draughtMiddleB: '',                //舯吃水
      draughtAfterB: '',                    //艉吃水
      toNextPortDistance: '',          //剩余航程
      reason: '',//修改原因
      isFirst: undefined
    };
    this.props.navigation.setParams({ submit: this.handleSubmit });
  }

  componentDidMount() {
    this.props.navigation.setParams({
      copySubmit: () => {
        this.copySubmitFunction();
        AlertView.show({
          message: '复制成功，可以直接粘贴'
        });
      }
    });
    this.props.getArrivePortRecord()
      .then(() => { })
      .catch(() => { });
    if (!this.props.isConnected) {
      AsyncStorage.getItem(StorageKeys.DYN)
        .then((str) => {
          if (str) {
            const dyn = JSON.parse(str);
            console.log(dyn.shipName,this.state.shipName,dyn);
            if(dyn.shipName != this.state.shipName){
              this.setState({
                exactLocation: '',             //抵港位置
                nextPortEta: '',              //抵港时间
                draughtFrontB: '',                   //艏吃水
                draughtMiddleB: '',                //舯吃水
                draughtAfterB: '',                    //艉吃水
                toNextPortDistance: '',          //剩余航程
                reason: '',//修改原因
              })
            }
            this.setState({
              data: dyn,
              shipName: dyn.shipName || this.state.shipName,
              lineName: dyn.lineName || this.state.lineName,
              voyageCode: dyn.voyageCode || this.state.voyageCode,
              isFirst: dyn.isFirst || this.state.isFirst
            });
          }
        })
        .catch(() => { });
    }

    //!监听当前位置信息
    this.addLocation = Location.addLocationListener(location =>
      this.setState({ location })
    )
    Location.start()
  }

  componentWillUnmount() {
    //!移除位置监听
    this.addLocation && this.addLocation.remove()
    Location.stop()
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.data !== this.props.data) {
      const data = {};
      Object.keys(nextProps.data).forEach((key) => {
        let value;
        switch (key) {
          case 'shipName':
            value = nextProps.data[key] ||this.state.shipName;
            break;
          case 'voyageCode':
            value = nextProps.data[key] || this.state.voyageCode;
            break;
          case 'lineName':
            value = nextProps.data[key] || this.state.lineName;
            break;
          case 'isFirst':
            value = nextProps.data[key] || this.state.isFirst;
            break;
          default:
            value = nextProps.data[key];
            break;
        }
        data[key] = value === 0 ? null : value;
      });
      this.setState({
        ...data
      });
    }
  }

  formatDate(now) { 
    var year=now.getFullYear() ? now.getFullYear() : '0000';  //取得4位数的年份
    var month=now.getMonth()+1 ? now.getMonth()+1 : '00';  //取得日期中的月份，其中0表示1月，11表示12月
    var date=now.getDate() ? now.getDate() : '00';      //返回日期月份中的天数（1到31）
    var hour=now.getHours() ? now.getHours() : '00';     //返回日期中的小时数（0到23）
    var minute=now.getMinutes() ? now.getMinutes() : '00'; //返回日期中的分钟数（0到59）
    return year+"-"+month+"-"+date+" "+hour+":"+minute
  } 
    
  //一键复制
  copySubmitFunction (){
    var d=new Date(this.state.nextPortEta);
    let detailstr =
    `船名：${this.state.shipName}，
航次：${this.state.voyageCode}，
航线：${this.state.lineName}，
抵港位置：${this.state.exactLocation ? this.state.exactLocation : ''}，
抵港时间：${this.formatDate(d)}，
剩余航程：${this.state.toNextPortDistance ? this.state.toNextPortDistance : '0'} 海里，
抵港艏吃水：${this.state.draughtFrontB ? this.state.draughtFrontB : '0'} 米，
抵港舯吃水：${this.state.draughtMiddleB ? this.state.draughtMiddleB : '0'} 米，
抵港艉吃水：${this.state.draughtAfterB ? this.state.draughtAfterB : '0'} 米，
修改原因：${this.state.reason ? this.state.reason : ''}`
console.log(detailstr);
Clipboard.setString(detailstr);
  }

  //提交按钮
  handleSubmit = () => {
    console.log(this.state.location,'1234');
    
    Keyboard.dismiss();
    const { createArrivePortRecord, navigation } = this.props;
    const {
      voyageId,
      shipName,
      voyageCode,
      lineName,
      exactLocation,    //抵港位置
      nextPortEta,     //抵港时间
      draughtFrontB,          //艏吃水
      draughtMiddleB,       //舯吃水
      draughtAfterB,           //艉吃水
      toNextPortDistance,  //剩余航程
      reason,             //修改原因
      isFirst,
    } = this.state;
    if (exactLocation && exactLocation.length > 30) {
      AlertView.show({ message: '确认抵港位置字数超出最大限制' });
      return;
    }
    if (_.isNil(exactLocation) || exactLocation.replace(/\s+/g,"") == '') {
      AlertView.show({ message: '请填写确认抵港位置' });
      return;
    }
    if (this.state.isFirst == 1) {
      if (_.isNil(reason) || reason.replace(/\s+/g,"") == '') {
        AlertView.show({ message: '请填写修改原因' });
        return;
      }

    }
    if (!nextPortEta) {
      AlertView.show({ message: '请选择抵港时间' });
      return;
    }
    function doSubmit() {
      const param = {
        opTime: new Date().getTime(),
        voyageId,
        voyageCode,
        shipName,
        lineName,
        exactLocation,
        reason,
        isFirst,
        nextPortEta,
        draughtFrontB: stringToNumber(draughtFrontB),
        draughtMiddleB: stringToNumber(draughtMiddleB),
        draughtAfterB: stringToNumber(draughtAfterB),
        toNextPortDistance: stringToNumber(toNextPortDistance),
      };
      createArrivePortRecord(param)
        .then(() => {
          const goBackCallback = navigation.getParam('onGoBack', undefined);
          if (goBackCallback) {
            goBackCallback();
          }
          if (this.props.isConnected) {
            if(this.state.isFirst == 0 || this.state.isFirst == undefined){
              this.setState({isFirst:1})
            }
            navigation.navigate(ROUTE_ARRIVEPORT_RECORD_LIST);
          } else {
            if (this.state.isFirst == 0) {   //离线时修改本地存储的isFirst状态
              let data2 = {}
              data2 = this.state.data
              data2.isFirst = 1
              console.log(data2);
              AsyncStorage.setItem(StorageKeys.DYN, JSON.stringify(data2))
            }
            AlertView.show({
              message: '抵港确报已保存至本地，将在网络打开时上传至服务器。',
              showCancel: false,
              confirmAction: () => {
                navigation.dispatch(NavigationActions.back());
              },
            });
          }
        })
        .catch(() => { });
    }
    //获取当前时间戳
    let timeNow =  Date.parse(new Date());
    console.log(timeNow,nextPortEta);
    // 检查ETA时间差
    if (nextPortEta && nextPortEta < timeNow ) {
      AlertView.show({
        message: '抵港时间不能小于当前时间。',
        showCancel: false,
        confirmAction: () => {
          // doSubmit.bind(this)();
        },
      });
    } else if(nextPortEta && Math.abs(nextPortEta - timeNow) > 108000000){
      AlertView.show({
        message: '预计到港30小时前不可录入抵港确报!',
        showCancel: false,
        confirmAction: () => {},
      });
    } else{
      AlertView.show({
        message: '确定提交吗？',
        showCancel: true,
        confirmAction: () => {
          doSubmit.bind(this)();
        },
      });
      // doSubmit.bind(this)();
    }
  };

  render() {
    return (
      <Container>
        <Content theme={myTheme} >
          <View style={{
            padding: 10,
            backgroundColor: '#FFFFFF',
          }}
          >
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="船名:"
                value={this.state.shipName}
                placeholder=""
                editable={false}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="航次:"
                value={this.state.voyageCode}
                placeholder=""
                editable={false}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="航线:"
                value={this.state.lineName}
                placeholder=""
                editable={false}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="确认抵港位置:"
                value={this.state.exactLocation}
                onChangeText={(text) => {
                  this.setState({ exactLocation: text });
                }}
                isRequired
                maxLength={30}      //输入字符数量限制
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <Item style={[commonStyles.inputItem, { borderBottomWidth: myTheme.borderWidth }]}>
                <Label style={{ color: 'red' }}>*</Label>
                <Label style={commonStyles.inputLabel}>确认抵港时间:</Label>
                <DatePullSelector
                  value={this.state.nextPortEta || null}
                  onChangeValue={(value) => {
                    this.setState({ nextPortEta: value });
                  }}
                />
              </Item>
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                isRequired
                label="剩余航程:"
                value={this.state.toNextPortDistance}
                onChangeText={(text) => {
                  this.setState({ toNextPortDistance: text });
                }}
                placeholder='0'
                keyboardType="numeric"
                maskType={MaskType.INTEGER}   //输入值类型
                rightItem={<Text style={commonStyles.tail}>海里</Text>}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                isRequired                //是否显示 *
                label="抵港艏吃水:"
                value={this.state.draughtFrontB}
                onChangeText={(text) => {
                  this.setState({ draughtFrontB: text });
                }}
                placeholder='0' //默认值
                keyboardType="numeric"      //键盘类型
                maskType={MaskType.FLOAT}   //输入值类型
                decimalNumber={1}           //小数点后位数限制
                rightItem={<Text style={commonStyles.tail}>米</Text>}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                isRequired                //是否显示 *
                label="抵港舯吃水:"
                value={this.state.draughtMiddleB}
                onChangeText={(text) => {
                  this.setState({ draughtMiddleB: text });
                }}
                placeholder='0'
                keyboardType="numeric"
                maskType={MaskType.FLOAT}   //输入值类型
                decimalNumber={1}           //小数点后位数限制
                rightItem={<Text style={commonStyles.tail}>米</Text>}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                isRequired                //是否显示 *
                label="抵港艉吃水:"
                value={this.state.draughtAfterB}
                onChangeText={(text) => {
                  this.setState({ draughtAfterB: text });
                }}
                placeholder='0'
                keyboardType="numeric"
                maskType={MaskType.FLOAT}   //输入值类型
                decimalNumber={1}           //小数点后位数限制
                rightItem={<Text style={commonStyles.tail}>米</Text>}
              />
            </InputGroup>
            {this.state.isFirst == 1 ?
              <InputGroup style={commonStyles.inputGroup}>
                <InputItem
                  label="修改原因:"
                  value={this.state.reason}
                  onChangeText={(text) => {
                    this.setState({ reason: text });
                  }}
                  isRequired
                />
              </InputGroup> : null
            }

            <TouchableOpacity
              style={{
                height: 45,
                backgroundColor: 'rgb(217,10,37)',
                width: '100%',
                borderRadius: 5,
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: 10
              }}
              onPress={() => {
                const handleSubmitFunc = this.props.navigation.getParam('submit', undefined);
                if (handleSubmitFunc) {
                  requestAnimationFrame(() => {
                    handleSubmitFunc();
                  });
                }
              }}
            >
              <Text style={{ color: '#fff', fontSize: 15 }}>提交</Text>
            </TouchableOpacity>
          </View>
        </Content>
      </Container>
    );
  }
}

ArrivePortRecord.propTypes = {
  navigation: PropTypes.object.isRequired,
  getArrivePortRecord: PropTypes.func.isRequired,
  data: PropTypes.object,
  firstStateTime: PropTypes.number,
  isConnected: PropTypes.bool.isRequired,
  createArrivePortRecord: PropTypes.func.isRequired,
};
ArrivePortRecord.defaultProps = {
  data: {},
  firstStateTime: undefined,
};

const mapStateToProps = createStructuredSelector({
  data: makeArrivePortRecord(),
  firstStateTime: makeFirstStateTime(),
  isConnected: state => state.network.isConnected,
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getArrivePortRecord: getArrivePortRecordPromise,
      createArrivePortRecord: createArrivePortRecordPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ArrivePortRecord);
