import { createSelector } from 'reselect/es';

const selectArrivePortRecordDomain = () => state => state.arrivePortRecord;

const makeArrivePortRecord = () => createSelector(
  selectArrivePortRecordDomain(),
  (subState) => {
    console.debug(subState.data);
    return subState.data;
  },
);

export {
  selectArrivePortRecordDomain,
  makeArrivePortRecord,
};
