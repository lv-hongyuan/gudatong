import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  GET_ARRIVEPORT_RECORD_ACTION,
  CREATE_ARRIVEPORT_RECORD_ACTION
} from './constants';

export const getArrivePortRecordRoutine = createRoutine(
  GET_ARRIVEPORT_RECORD_ACTION,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const getArrivePortRecordPromise = promisifyRoutine(getArrivePortRecordRoutine);

export const createArrivePortRecordRoutine = createRoutine(
  CREATE_ARRIVEPORT_RECORD_ACTION,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const createArrivePortRecordPromise = promisifyRoutine(createArrivePortRecordRoutine);