import { getArrivePortRecordRoutine ,createArrivePortRecordRoutine} from './actions';

const initState = {
  success: false,
  data: {},
};

export default function (state = initState, action) {
  switch (action.type) {
    case getArrivePortRecordRoutine.TRIGGER:
      return { ...state, loading: true };
    case getArrivePortRecordRoutine.SUCCESS:
      console.log(action.payload);
      return { ...state, data: action.payload };
    case getArrivePortRecordRoutine.FAILURE:
      return { ...state, error: action.payload };
    case getArrivePortRecordRoutine.FULFILL:
      return { ...state, loading: false };

    //提交
    case createArrivePortRecordRoutine.TRIGGER:
      return { ...state, loading: true };
    case createArrivePortRecordRoutine.SUCCESS:
      return { ...state };
    case createArrivePortRecordRoutine.FAILURE:
      return { ...state, error: action.payload };
    case createArrivePortRecordRoutine.FULFILL:
      return { ...state, loading: false };
    default:
      return state;
  }
}
