import { call, put, select, takeLatest } from 'redux-saga/effects';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import {
  getArrivePortRecordRoutine,
  createArrivePortRecordRoutine
} from './actions';
import { errorMessage } from '../ErrorHandler/actions';
import realm from '../../common/realm';
import {getPopId,getShipId } from '../ShipWork/sagas';

const _isConnected = state => state.network.isConnected;

function* getArrivePortRecord(action) {
  console.log(action);
  try {
    yield put(getArrivePortRecordRoutine.request());
    const isConnected = yield select(_isConnected);
    if (isConnected) {
      // 获取popId，shipId
      const shipId = yield call(getShipId);
      const popId = yield call(getPopId);
      const param = {
        shipId,
        popId,
      };
      console.log(param);
      const response = yield call(request, ApiFactory.getArrivePort(param));
      yield put(getArrivePortRecordRoutine.success(response));
    } else {
      // 无网络时从本地存储中获取数据源
      const offlineTasks = realm.objects('OfflineTask')
        .filtered('type = "OfflineArrivePortRecord"')
        .sorted('date', true);
      if (offlineTasks.length > 0) {
        const task = offlineTasks[0];
        console.log('1111111:',JSON.parse(task.data));
        yield put(getArrivePortRecordRoutine.success(JSON.parse(task.data)));
      } else {
        yield put(getArrivePortRecordRoutine.success({}));
      }
    } 
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(getArrivePortRecordRoutine.failure(e));
  } finally {
    yield put(getArrivePortRecordRoutine.fulfill());
  }
}

export function* getArrivePortRecordSaga() {
  yield takeLatest(getArrivePortRecordRoutine.TRIGGER, getArrivePortRecord);
}

function* createArrivePortRecordList(action) {
  console.log(action);
  let dataArr = action.payload
  try {
    yield put(createArrivePortRecordRoutine.request());
    const isConnected = yield select(_isConnected);
    if (isConnected) {
      const popId = yield call(getPopId);
      const shipId = yield call(getShipId);
      const param = {
        shipId,
        popId,
        voyageId:dataArr.voyageId,
        voyageCode:dataArr.voyageCode,
        shipName:dataArr.shipName,
        lineName:dataArr.lineName,
        exactLocation:dataArr.exactLocation,
        toNextPortDistance:dataArr.toNextPortDistance,
        nextPortEta:dataArr.nextPortEta,
        opTime:dataArr.opTime,
        draughtFrontB:dataArr.draughtFrontB,
        draughtMiddleB:dataArr.draughtMiddleB,
        draughtAfterB:dataArr.draughtAfterB,
        reason:dataArr.reason,
        isFirst:dataArr.isFirst
      };
      console.log(param);
      const response = yield call(request, ApiFactory.submitArrivalConfirmOperation(param));
      console.log(response);
      const offlineTasks = realm.objects('OfflineTask') //获取当前用于自动导入的数据源
        .filtered('type = "OfflineArrivePortRecord"')
        .sorted('date', true);
      if(offlineTasks.length > 0){//如果当前数据源数量大于0，清空数据源，
        while (offlineTasks.length > 0) {
          const task = offlineTasks[0];
          realm.write(() => {
            realm.delete(task);
          });
        }
      }
      //写入最新的数据
      realm.write(() => {
        realm.create('OfflineTask', {
          type: 'OfflineArrivePortRecord',
          data: JSON.stringify(action.payload),
          date: new Date().getTime(),
        });
      });
      yield put(createArrivePortRecordRoutine.success(response));
    } else {
      //离线模式下将填写的数据存储至本地，用于恢复网络后上传至服务器
      realm.write(() => {
        realm.create('OfflineTask', {
          type: 'ArrivePortRecord',
          data: JSON.stringify(action.payload),
          date: new Date().getTime(),
        });
      });
      //获取当前用于自动导入的数据源。如果当前数据源数量大于0，清空数据源，
      const offlineTasks = realm.objects('OfflineTask') 
        .filtered('type = "OfflineArrivePortRecord"')
        .sorted('date', true);
      if(offlineTasks.length > 0){
        while (offlineTasks.length > 0) {
          const task = offlineTasks[0];
          realm.write(() => {
            realm.delete(task);
          });
        }
      }
      //清空数据源后写入最新的数据（只保留最新数据）
      realm.write(() => {
        realm.create('OfflineTask', {
          type: 'OfflineArrivePortRecord',
          data: JSON.stringify(action.payload),
          date: new Date().getTime(),
        });
      });
      yield put(createArrivePortRecordRoutine.success({}));
    }
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(createArrivePortRecordRoutine.failure(e));
  } finally {
    yield put(createArrivePortRecordRoutine.fulfill());
  }
}

export function* createArrivePortRecordListSaga() {
  yield takeLatest(createArrivePortRecordRoutine.TRIGGER, createArrivePortRecordList);
}
export default [
  getArrivePortRecordSaga,
  createArrivePortRecordListSaga
];
