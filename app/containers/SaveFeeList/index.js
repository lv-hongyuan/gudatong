import React from 'react';
import PropTypes from 'prop-types';
import {SafeAreaView, Animated, Keyboard, DeviceEventEmitter} from 'react-native';
import {connect} from 'react-redux';
import {
    Button,
    Container, InputGroup, Item, Label,
    Text,
    View,
} from 'native-base';
import {createStructuredSelector} from 'reselect/es';
import {bindPromiseCreators} from 'redux-saga-routines/es';
import {getShipTugRewardListPromiseCreator, checkShipTugRewardRepeatPromiseCreator} from './actions';
import {
    ROUTE_SAVE_FEE
} from '../../RouteConstant';
import {makeList, makeRefreshState, makeIsLoading} from './selectors';
import myTheme from '../../Themes';
import screenHOC from '../listScreenHOC';
import ShipTimeItem from './components/ShipTimeItem';
import RefreshListView from '../../components/RefreshListView';
import HeaderButtons from "react-navigation-header-buttons";
import commonStyles from "../../common/commonStyles";
import InputItem from "../../components/InputItem";
import Selector from "../../components/Selector";
import {ReviewState, reviewTitleFromState, getLineType} from "../../common/Constant";
import Svg from "../../components/Svg";
import {NavigationActions} from "react-navigation";
import AlertView from '../../components/Alert';
import DatePullSelector from '../../components/DatePullSelector';
import {DATE_WHEEL_TYPE} from "../../components/DateTimeWheel";
import SelectDate from './components/SelectDate';

const styles = {
    container: {
        backgroundColor: 'white',
        flexDirection: 'row',
        alignContent: 'stretch',
    },
    col: {
        backgroundColor: 'white',
        width: 80,
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 10,
        paddingBottom: 10,
        borderRightWidth: myTheme.borderWidth,
        borderBottomWidth: myTheme.borderWidth,
        borderColor: myTheme.borderColor,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    touchCol: {
        backgroundColor: 'white',
        width: 80,
    },
    touchContent: {
        flex: 1,
        backgroundColor: 'white',
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 10,
        paddingBottom: 10,
        borderRightWidth: myTheme.borderWidth,
        borderBottomWidth: myTheme.borderWidth,
        borderColor: myTheme.borderColor,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    text: {
        color: myTheme.inputColor,
        textAlign: 'center',
        flex: 1,
    },
};


const firstPageNum = 1;
const DefaultPageSize = 10;

@screenHOC
class SaveFeeList extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            containerOffSet: new Animated.Value(0),
            containerWidth: undefined,
            month: '',
            businessWeek: '',
            lineType: '',
            currentTime: undefined,
            page: firstPageNum,
            pageSize: DefaultPageSize,
        };
    }

    componentDidMount() {
        this.listView.beginRefresh();
        this.props.navigation.setParams({
            setNewDate: this.setInputNewDate
        });
        this.listener = DeviceEventEmitter.addListener('submitSuccess', () => {
            this.listView.beginRefresh();
        });
    }

    componentWillUnmount() {
        this.listener.remove();
    }

    onHeaderRefresh = () => {
        this.loadList(false);
    };

    onFooterRefresh = () => {
        this.loadList(true);
    };

    //component里边封装的控件，由于alert不会重复触发只能在自定义控件内部的state赋值
    setInputNewDate = () => {
        AlertView.show({
            message: '创建月份',
            component: () => (
                <SelectDate onSelectDate={this.onSelectDate}/>
            ),
            showCancel: true,
            cancelAction: () => {
                this.setState({currentTime: null})
            },
            confirmAction: () => {
                if (this.state.currentTime != null) {
                    this.props.navigation.navigate(
                        ROUTE_SAVE_FEE,
                        {
                            id: '',
                            feemon: this.state.currentTime,
                        },
                    );
                    this.setState({currentTime: null})
                } else {
                    AlertView.show({message: '请选择时间'});
                }

            },
        });
    };

    onSelectDate = (date) => {
        console.log('------', date)
        this.setState({
            currentTime: date,
        })
    }

    onSelect = (item) => {
        console.log('+++点我了', JSON.stringify(item));
        this.props.navigation.navigate(
            ROUTE_SAVE_FEE,
            {
                id: item.id,
                feemon: '',
                captain: item.captain,
            },
        );
    };

    get translateX() {
        const x = 0;
        return this.state.containerOffSet.interpolate({
            inputRange: [-1, 0, x, x + 1],
            outputRange: [0, 0, 0, 1],
        });
    }

    loadList(loadMore) {
        const {
            page,
            pageSize,
        } = this.state;
        this.props.getShipTugRewardList({
            page: loadMore ? page : firstPageNum,
            pageSize,
            loadMore,
        })
        // eslint-disable-next-line no-shadow
            .then(({page, showAudit}) => {
                console.log('sssss', showAudit);
                // DeviceEventEmitter.emit('addOilApplyShowAudit',{ audit:showAudit });
                this.setState({page: page + 1});
            })
            .catch(() => {
            });
    }

    renderItem = ({item}) => (
        <ShipTimeItem
            item={item}
            headerTranslateX={this.translateX}
            onSelect={this.onSelect}
        />
    );

    render() {
        return (
            <Container theme={myTheme} style={{backgroundColor: '#E0E0E0'}}>
                <SafeAreaView style={{backgroundColor: '#E0E0E0', flex: 1}}>
                    <View
                        style={{width: '100%', flex: 1}}
                        onLayout={(event) => {
                            const {width} = event.nativeEvent.layout;
                            this.setState({containerWidth: width});
                        }}
                    >
                        <Animated.ScrollView
                            onScroll={Animated.event([{
                                nativeEvent: {contentOffset: {x: this.state.containerOffSet}},
                            }], {useNativeDriver: true})}
                            scrollEventThrottle={1}
                            horizontal
                        >
                            <View style={{flex: 1, minWidth: '100%'}}>
                                <View style={styles.container}>
                                    <View style={[styles.col, {width:90}]}>
                                        <Text style={styles.text}>月份</Text>
                                    </View>
                                    {/*<View style={[styles.col, { width: 80 }]}>*/}
                                    {/*  <Text style={styles.text}>航次</Text>*/}
                                    {/*</View>*/}
                                    <View style={[styles.col, {flex:1}]}>
                                        <Text style={styles.text}>审核状态</Text>
                                    </View>
                                    {/* <View style={{
                                        width: 70,
                                        backgroundColor: 'white',
                                        paddingLeft: 2,
                                        paddingRight: 2,
                                        paddingTop: 10,
                                        paddingBottom: 10,
                                        borderRightWidth: myTheme.borderWidth,
                                        borderBottomWidth: myTheme.borderWidth,
                                        borderColor: myTheme.borderColor,
                                        flexDirection: 'row',
                                        justifyContent: 'space-between'
                                    }}>
                                        <Text style={styles.text}>有效状态</Text>
                                    </View> */}
                                    <View style={[styles.col, {flex: 1}]}>
                                        <Text style={styles.text}>提交时间</Text>
                                    </View>
                                    
                                    <Animated.View style={{
                                        position: 'absolute',
                                        top: 0,
                                        left: 0,
                                        bottom: 0,
                                        transform: [{translateX: this.translateX}],
                                    }}
                                    >
                                        <View style={styles.container}>
                                            <View style={[styles.col, {width: 90}]}>
                                                <Text style={styles.text}>月份</Text>
                                            </View>
                                            {/*<View style={[styles.col, { width: 80 }]}>*/}
                                            {/*  <Text style={styles.text}>航次</Text>*/}
                                            {/*</View>*/}
                                        </View>
                                    </Animated.View>
                                </View>
                                <RefreshListView
                                    ref={(ref) => {
                                        this.listView = ref;
                                    }}
                                    headerTranslateX={this.translateX}
                                    containerWidth={this.state.containerWidth}
                                    data={this.props.list || []}
                                    style={{flex: 1}}
                                    keyExtractor={item => `${item.id}`}
                                    renderItem={this.renderItem}
                                    refreshState={this.props.refreshState}
                                    onHeaderRefresh={this.onHeaderRefresh}
                                    onFooterRefresh={this.onFooterRefresh}
                                />
                            </View>
                        </Animated.ScrollView>
                    </View>
                </SafeAreaView>
            </Container>
        );
    }
}


SaveFeeList.navigationOptions = ({navigation}) => ({
    title: '节拖奖励',
    headerRight: <HeaderButtons>
        {navigation.getParam('showFilter', false) ? (
            null) : (<HeaderButtons.Item
                title="新建"
                buttonStyle={{
                    fontSize: 14,
                    color: '#ffffff',
                }}
                onPress={navigation.getParam('setNewDate')}
            />
        )}
    </HeaderButtons>
});

SaveFeeList.propTypes = {
    navigation: PropTypes.object.isRequired,
    getShipTugRewardList: PropTypes.func.isRequired,
    list: PropTypes.array,
    refreshState: PropTypes.number.isRequired,
    checkShipTugRewardRepeat: PropTypes.func.isRequired,
};
SaveFeeList.defaultProps = {
    list: [],
};

const mapStateToProps = createStructuredSelector({
    refreshState: makeRefreshState(),
    list: makeList(),
    isLoading: makeIsLoading(),
});

function mapDispatchToProps(dispatch) {
    return {
        ...bindPromiseCreators({
            getShipTugRewardList: getShipTugRewardListPromiseCreator,
            checkShipTugRewardRepeat: checkShipTugRewardRepeatPromiseCreator,
        }, dispatch),
        dispatch,
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(SaveFeeList);
