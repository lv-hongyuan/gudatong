import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../ErrorHandler/actions';
import { getShipTugRewardListRoutine, checkShipTugRewardRepeatRoutine } from './actions';

function* getShipTugRewardList(action) {
  console.log('节拖奖励请求开始++',action);
  const {
    page, pageSize, loadMore, ...rest
  } = (action.payload || {});
  try {
    yield put(getShipTugRewardListRoutine.request());
    const param = {
      filter: {
        logic: 'and',
        filters: [],
      },
      page,
      pageSize,
    };
    Object.keys(rest).map((key) => {
      const value = rest[key];
      if (value !== null && value !== undefined && value !== '') {
        param.filter.filters.push({
          field: key,
          operator: 'eq',
          value,
        });
      }
    });
    console.log('param+++',JSON.stringify(param))
    const response = yield call(request, ApiFactory.getShipTugRewardList(param));
    console.log('+++节拖奖励',JSON.stringify(response));
    const showAudit = response.showAudit;
    const { totalElements, content } = (response.dtoList || {});
    yield put(getShipTugRewardListRoutine.success({
      loadMore, page, pageSize, list: (content || []), totalElements,showAudit,
    }));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(getShipTugRewardListRoutine.failure(e));
  } finally {
    yield put(getShipTugRewardListRoutine.fulfill());
  }
}

export function* getEditionListSaga() {
  yield takeLatest(getShipTugRewardListRoutine.TRIGGER, getShipTugRewardList);
}

function* checkShipTugRewardRepeat(action) {
  console.log(action);
  try {
    yield put(checkShipTugRewardRepeatRoutine.request());
    const response = yield call(request, ApiFactory.checkShipTugRewardRepeat(action.payload));
    const {detailList, payable, actualPayment, id }= response;
    console.log('新建!!!',JSON.stringify(response));
    yield put(checkShipTugRewardRepeatRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(checkShipTugRewardRepeatRoutine.failure(e));
  } finally {
    yield put(checkShipTugRewardRepeatRoutine.fulfill());
  }
}

export function* checkShipTugRewardRepeatDeductionSaga() {
  yield takeLatest(checkShipTugRewardRepeatRoutine.TRIGGER, checkShipTugRewardRepeat);
}

// All sagas to be loaded
export default [getEditionListSaga, checkShipTugRewardRepeatDeductionSaga];
