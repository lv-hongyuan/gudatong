/*
 *
 * ShipTime constants
 *
 */
export const GET_SHIP_TUG_REWARD_LIST = 'app/SaveFeeList/GET_SHIP_TUG_REWARD_LIST';
export const CHECK_SHIP_TUG_REWARD_REPEAT = 'app/SaveFeeList/CHECK_SHIP_TUG_REWARD_REPEAT';
