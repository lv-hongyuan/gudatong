import { createSelector } from 'reselect/es';

const selectGetShipTugRewardListDomain = () => state => state.saveFeeList;

const makeList = () => createSelector(selectGetShipTugRewardListDomain(), (subState) => {
  return subState.list;
});

const makeRefreshState = () => createSelector(selectGetShipTugRewardListDomain(), (subState) => {
  return subState.refreshState;
});

const makeIsLoading = () => createSelector(selectGetShipTugRewardListDomain(), (subState) => {
  console.debug(subState.isLoading);
  return subState.isLoading;
});

export { makeList, makeRefreshState, makeIsLoading };
