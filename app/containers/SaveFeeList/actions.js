import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_SHIP_TUG_REWARD_LIST, CHECK_SHIP_TUG_REWARD_REPEAT } from './constants';

export const getShipTugRewardListRoutine = createRoutine(GET_SHIP_TUG_REWARD_LIST);
export const getShipTugRewardListPromiseCreator = promisifyRoutine(getShipTugRewardListRoutine);

export const checkShipTugRewardRepeatRoutine = createRoutine(CHECK_SHIP_TUG_REWARD_REPEAT);
export const checkShipTugRewardRepeatPromiseCreator = promisifyRoutine(checkShipTugRewardRepeatRoutine);
