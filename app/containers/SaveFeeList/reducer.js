import { getShipTugRewardListRoutine, checkShipTugRewardRepeatRoutine } from './actions';
import { RefreshState } from '../../components/RefreshListView';
import fixIdList from '../../utils/fixIdList';

const initState = {
  loading: false,
  list: [],
  refreshState: RefreshState.Idle,
  loadingError: null,
  isLoading: false,
};

export default function (state = initState, action) {
  switch (action.type) {
    case getShipTugRewardListRoutine.TRIGGER: {
      const { loadMore } = action.payload;
      return {
        ...state,
        loading: true,
        refreshState: loadMore ? RefreshState.FooterRefreshing : RefreshState.HeaderRefreshing,
      };
    }
    case getShipTugRewardListRoutine.SUCCESS: {
      const { page, pageSize, list, showAudit} = action.payload;
      return {
        showAudit,
        ...state,
        list: page === 1 ? list : state.list.concat(list),
        refreshState: list.length < pageSize ? RefreshState.NoMoreData : RefreshState.Idle,
      };
    }
    case getShipTugRewardListRoutine.FAILURE: {
      const { page, error } = action.payload;
      return {
        ...state, list: page === 1 ? [] : state.list, error, refreshState: RefreshState.Failure,
      };
    }
    case getShipTugRewardListRoutine.FULFILL: {
      return { ...state, loading: false };
    }

    case checkShipTugRewardRepeatRoutine.TRIGGER:
      return { ...state, loading: true};
    case checkShipTugRewardRepeatRoutine.SUCCESS:
      console.log('新建++',action.payload)
        const {payable, actualPayment, detailList, id} = action.payload;
        const RewardDetails=action.payload
        console.log('action++',action.payload)
        // const RewardDetails=null;
      return { ...state, RewardDetails , actualPayment:actualPayment};
    case checkShipTugRewardRepeatRoutine.FAILURE:
      return {
        ...state, error: action.payload
      };
    case checkShipTugRewardRepeatRoutine.FULFILL:
      return { ...state, loading: false };

    default:
      return state;
  }
}
