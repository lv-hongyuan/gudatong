import React from 'react';
import PropTypes from 'prop-types';
import {  StyleSheet } from 'react-native';
import {InputGroup} from 'native-base';
import commonStyles from "../../../common/commonStyles";
import DatePullSelector from "../../../components/DatePullSelector";
import {DATE_WHEEL_TYPE} from "../../../components/DateTimeWheel";
import {defaultFormat} from "../../../utils/DateFormatYearMonth";

const styles = StyleSheet.create({
});

class SelectDate extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {currentTime:''};
  }

    componentDidMount() {
      console.log('换新数据');
      this.setState({
          currentTime:''
      })
    }

    componentWillUnmount(){

    }

  makeTimeShowValue() {
    const nowTime = new Date().getTime(); // 当前时间
    console.log('当前时间++',nowTime)
    return nowTime;
  }
    //返回页面的回调方法
    onSelectDate() {
      console.log('dasdasdas')
        var date = this.state.currentTime
        // const timeValue = toDate(date, 'yyyy-MM').getTime();
        console.log('返回的时间+++',defaultFormat(date))
        this.props.onSelectDate(defaultFormat(date))
    }

  render() {
    // const {
    //   currentTime
    // } = (this.props.currentTime || {});
    return (
        <InputGroup style={[commonStyles.inputGroup, {marginLeft: 10, marginRight: 10,marginTop: 5,width: 120,height: 30 }]}>
          <DatePullSelector
              defaultValue={this.makeTimeShowValue()}
              type={DATE_WHEEL_TYPE.DATE_YM}
              onChangeValue={(value) => {
                console.log('value+++',value);
                this.setState({ currentTime: value });
                this.onSelectDate();
              }}
              value={this.state.currentTime || null}
              // onSubmit={()=>{console.log('1111')
              //     this.onSelectDate}}
          />
        </InputGroup>
    );
  }
}

SelectDate.propTypes = {
  onSelectDate: PropTypes.func,
  currentTime: PropTypes.number,
};
SelectDate.defaultProps = {
  onSelectDate: undefined,
};

export default SelectDate;
