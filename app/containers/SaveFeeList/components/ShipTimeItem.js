import React from 'react';
import PropTypes from 'prop-types';
import { TouchableHighlight, StyleSheet, Animated } from 'react-native';
import { Text, View } from 'native-base';
import myTheme from '../../../Themes';
// import { defaultFormat } from '../../../../../utils/DateFormat';
import { reviewStateFromState } from '../../../common/Constant';

const styles = StyleSheet.create({
  container: {
    alignContent: 'stretch',
    backgroundColor: 'white',
    flexDirection: 'row',
  },
  col: {
    width: 80,
    // paddingLeft: 10,
    // paddingRight: 10,
    paddingTop: 5,
    paddingBottom: 5,
    borderRightWidth: myTheme.borderWidth,
    borderBottomWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
  },
  text: {
    color: myTheme.inputColor,
    textAlign: 'center',
  },
});

class ShipTimeItem extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  onPressEvent = () => {
    if (this.props.onSelect) {
      this.props.onSelect(this.props.item);
    }
  };

  render() {
    const {
      feemon, opState, submitTime,opType
    } = (this.props.item || {});
    
    return (
      <TouchableHighlight onPress={this.onPressEvent}>
        <View style={styles.container}>
          <View style={[styles.col, { width:90 }]}>
            <Text style={styles.text}>{feemon}</Text>
          </View>
          {/*<View style={[styles.col, { width: 80 }]}>*/}
          {/*  <Text style={styles.text}>{voyageCode}</Text>*/}
          {/*</View>*/}
          <View style={[styles.col, { flex:1 }]}>
            <Text style={styles.text}>{reviewStateFromState(opState)}</Text>
          </View>
          {/* <View style={[styles.col, { width: 70 }]}>
            <Text style={styles.text}>{opType == 0 ? '有效' : '无效'}</Text>
          </View> */}
          <View style={[styles.col, { flex:1 }]}>
            <Text style={styles.text}>{submitTime}</Text>
          </View>
          
          <Animated.View style={{
            position: 'absolute',
            top: 0,
            left: 0,
            bottom: 0,
            transform: [{ translateX: this.props.headerTranslateX || 0 }],
          }}
          >
            <View style={[styles.container, { flex: 1 }]}>
              <View style={[styles.col, { width: 90 }]}>
                <Text style={styles.text}>{feemon}</Text>
              </View>
              {/*<View style={[styles.col, { width: 80 }]}>*/}
              {/*  <Text style={styles.text}>{voyageCode + '/' + 'yy'}</Text>*/}
              {/*</View>*/}
            </View>
          </Animated.View>
        </View>
      </TouchableHighlight>
    );
  }
}

ShipTimeItem.propTypes = {
  item: PropTypes.object.isRequired,
  onSelect: PropTypes.func,
  headerTranslateX: PropTypes.object.isRequired,
};
ShipTimeItem.defaultProps = {
  onSelect: undefined,
};

export default ShipTimeItem;
