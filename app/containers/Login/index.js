import React from 'react';
import PropTypes from 'prop-types';
import { Platform, StyleSheet, InteractionManager, PixelRatio } from 'react-native';
import { createStructuredSelector } from 'reselect/es';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { connect } from 'react-redux';
import {
  Button,
  Container,
  Content,
  Form,
  Input,
  Item,
  Text,
} from 'native-base';
import ApiConstants, { EnableServerChange } from '../../common/ApiConstants';
import { sendVerCodePromiseCreator, loginPromiseCreator } from './actions';
import { ROUTE_APPLICATION, ROUTE_USER_AUTH } from '../../RouteConstant';
import {
  makeSelectAutPass,
  makeSelectUser,
} from './selectors';
import myTheme from '../../Themes';
import Svg from '../../components/Svg';
import screenHOC from '../screenHOC';
const platform = Platform.OS;
const styles = StyleSheet.create({
  input: {
    paddingLeft: 20,
    paddingRight: 20,
    fontSize: platform === 'ios' ? 15 : 15 / PixelRatio.getFontScale(),
    height: '100%',
    lineHeight: 18,
    color: '#565656',
    paddingVertical: 0,
  },
  verButton: {
    height: 40,
    backgroundColor: '#EE7E0A',
  },
  timeText: {
    color: 'white',
    width: 100,
    textAlign: 'center',
  },
});

@screenHOC
class Login extends React.PureComponent {
  static navigationOptions = {
    header: null,
  };

  static propTypes = {
    navigation: PropTypes.object.isRequired,
    login: PropTypes.func.isRequired,
    sendVerCode: PropTypes.func.isRequired,
    autPass: PropTypes.number.isRequired,
    // isLoading: PropTypes.bool.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      phoneNumber: '',
      verCode: '',
      isWaitCode: false,
      timerNum: 60,
      verCodeText: '获取验证码',
      host: ApiConstants.host,
    };
  }

  componentWillUnmount() {
    if (this.timer) clearInterval(this.timer);
    this.timer = undefined;
  }

  onBackPressed = () => false;

  onBtnLoginButton = () => {
    requestAnimationFrame(() => {
      this.props.login({
        phoneNumber: this.state.phoneNumber,
        verCode: this.state.verCode,
      }).then(() => {
        if (this.props.autPass === 30) {
          this.props.navigation.navigate(ROUTE_APPLICATION);
        } else {
          // 如果审核失败，要提示信息
          this.props.navigation.navigate(ROUTE_USER_AUTH);
        }
      }).catch((e) => {
        console.log(e);
      });
    });
  };

  onBtnVerButton = () => {
    this.props.sendVerCode({
      userId: this.state.userId,
      password: this.state.password,
      phoneNumber: this.state.phoneNumber,
    }).then(() => {
      this.startCountDown();
    }).catch((e) => {
      console.log(e);
    });
  };

  startCountDown() {
    this.setState({
      isWaitCode: true,
      verCodeText: '60 秒',
    }, () => {
      if (this.timer) clearInterval(this.timer);
      this.timer = setInterval(() => {
        InteractionManager.runAfterInteractions(() => {
          const currentNum = this.state.timerNum - 1;
          if (currentNum <= 0) {
            this.setState({
              timerNum: 60,
              isWaitCode: false,
              verCodeText: '重新发送',
            });
            if (this.timer) clearInterval(this.timer);
            this.timer = undefined;
          } else {
            this.setState({
              timerNum: currentNum,
              verCodeText: `${currentNum} 秒`,
            });
          }
        });
      }, 1000);
    });
  }

  render() {
    return (
      <Container
        style={{
          backgroundColor: myTheme.primaryColor,
          flexDirection: 'column',
          justifyContent: 'center',
          marginTop: 'auto',
          height: '100%',
          marginBottom: 'auto',
        }}
      >
        <Content theme={myTheme} contentContainerStyle={{ flex: 1, minHeight: '100%', justifyContent: 'center' }}>
          <Svg
            icon="onLand_logo"
            width={220}
            height={180}
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              alignContent: 'center',
            }}
            color="white"
          />
          <Form style={{
            marginLeft: 50,
            marginRight: 50,
          }}
          >
            {EnableServerChange && (
              <Item rounded style={[styles.item, { marginBottom: 0 }]}>
                <Input
                  returnKeyType="next"
                  placeholderTextColor="#848484"
                  placeholder="现场服务器"
                  style={styles.input}
                  value={this.state.host}
                  autoCapitalize="none"
                  onChangeText={(text) => {
                    this.setState({ host: text });
                    ApiConstants.host = text;
                  }}
                />
              </Item>
            )}
            <Item
              rounded
              style={{
                marginBottom: 20,
                justifyContent: 'center',
                backgroundColor: '#fff',
                borderTopWidth: 0,
                borderRightWidth: 0,
                height: 40,
              }}
            >
              <Input
                returnKeyType="next"
                keyboardType="numeric"
                multiline={false}
                onChangeText={(text) => {
                  this.setState({
                    phoneNumber: text,
                  });
                }}
                value={this.state.phoneNumber}
                placeholderTextColor="#848484"
                placeholder="手机号"
                style={styles.input}
              />
            </Item>
            <Item
              rounded
              style={{
                marginBottom: 20,
                justifyContent: 'center',
                backgroundColor: '#fff',
                borderTopWidth: 0,
                borderRightWidth: 0,
                height: 40,
              }}
            >
              <Input
                keyboardType="numeric"
                returnKeyType="go"
                onChangeText={(text) => {
                  this.setState({
                    verCode: text,
                  });
                }}
                value={this.state.verCode}
                placeholderTextColor="#848484"
                placeholder="验证码"
                style={styles.input}
              />
              <Button
                rounded
                disabled={this.state.isWaitCode}
                onPress={this.onBtnVerButton}
                style={styles.verButton}
              >
                <Text style={styles.timeText}>
                  {this.state.verCodeText}
                </Text>
              </Button>
            </Item>
          </Form>
          <Button
            style={{
              marginLeft: 50,
              marginRight: 50,
              height: 40,
              backgroundColor: 'white',
            }}
            block
            rounded
            disabled={this.props.isLoading}
            onPress={this.onBtnLoginButton}
          >
            <Text style={{ color: '#DF001B' }}>登录</Text>
          </Button>

        </Content>
      </Container>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  user: makeSelectUser(),
  autPass: makeSelectAutPass(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      sendVerCode: sendVerCodePromiseCreator,
      login: loginPromiseCreator,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
