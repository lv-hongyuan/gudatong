import { createSelector } from 'reselect/es';

const selectLoginDomain = () => state => state.login;

const makeSelectUser = () => createSelector(
  selectLoginDomain(),
  subState => subState.user,
);

const makeSelectAutPass = () => createSelector(
  selectLoginDomain(),
  (subState) => {
    if (subState.user) {
      return (subState.user || {}).autPass;
    }
    return 0;
  },
);

export {
  makeSelectUser,
  makeSelectAutPass,
};
