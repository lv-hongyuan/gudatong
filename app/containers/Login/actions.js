import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { SEND_VER_CODE, LOGIN } from './constants';

export const sendVerCodeRoutine = createRoutine(
  SEND_VER_CODE,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const sendVerCodePromiseCreator = promisifyRoutine(sendVerCodeRoutine);

export const loginRoutine = createRoutine(
  LOGIN,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const loginPromiseCreator = promisifyRoutine(loginRoutine);
