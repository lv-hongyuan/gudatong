import { call, put, takeLatest, select } from 'redux-saga/effects';
import { Platform } from 'react-native';
import request from '../../utils/request';
import ApiFactory, { header } from '../../common/Api';
import { loginRoutine, sendVerCodeRoutine } from './actions';
import { errorMessage } from '../ErrorHandler/actions';
import { saveUserInfo } from '../AppInit/sagas';
import uploadAppInfo from '../../common/uploadAppInfo';

const uuidv1 = require('uuid/v1');
const registrationId = state => state.notificationCenter.registrationId;

function* login(action) {
  console.log(action);
  try {
    yield put(loginRoutine.request());
    const { phoneNumber, verCode } = action.payload;
    const uniqueDeviceCode = uuidv1();
    console.log('登录进来啦+++',`${uniqueDeviceCode} uniqueDeviceCode`);
    const deviceToken = yield select(registrationId);
    const response = yield call(request, ApiFactory.login({
      phoneNum: phoneNumber,
      password: verCode,
      appKey: Platform.OS, // Android or iOS
      deviceToken: deviceToken, // UUID , push message
    }));
    console.log('login', response);
    const apiToken = response.tokenId;
    yield call(saveUserInfo, { payload: response });
    header.content.token = apiToken;
    // 上传app版本信息
    yield call(uploadAppInfo, response.userId);
    yield put(loginRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield call(saveUserInfo, { payload: undefined });
    yield put(loginRoutine.failure(e));
  } finally {
    yield put(loginRoutine.fulfill());
  }
}

export function* loginSaga() {
  yield takeLatest(loginRoutine.TRIGGER, login);
}

function* sendVerificationCode(action) {
  console.log(action);
  try {
    yield put(sendVerCodeRoutine.request());
    const { phoneNumber } = action.payload;
    const response = yield call(request, ApiFactory.sendVerificationCode({
      phoneNum: phoneNumber,
    }));
    console.log(response);
    yield put(sendVerCodeRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(sendVerCodeRoutine.failure(e));
  } finally {
    yield put(sendVerCodeRoutine.fulfill());
  }
}

export function* sendVerificationCodeSaga() {
  yield takeLatest(sendVerCodeRoutine.TRIGGER, sendVerificationCode);
}

// All sagas to be loaded
export default [
  loginSaga,
  sendVerificationCodeSaga,
];
