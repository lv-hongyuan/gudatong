import React from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet, View, Dimensions, DeviceEventEmitter
} from 'react-native';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect/es';
import {
  Container, Content, Form, Text,
} from 'native-base';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { getPublishNoticeReadPromise } from './actions';
import myTheme from '../../Themes';
import screenHOC from '../screenHOC';
import {defaultFormat} from "../../utils/DateFormat";
// import {ROUTE_ASSESSMENT_CLASS} from "../../RouteConstant";
import {makeList, makeRefreshState, makeIsLoading } from "../BulletinContent/selectors";
let deviceWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
  form: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 8,
    paddingTop: 20,
    flex: 1,
  },
  contentView: {
    width: deviceWidth-40,
    textAlign:'center',
  },
  contentText: {
    fontSize:18,
    color: '#535353',
    lineHeight:22,
  },
  khText: {
    fontSize:18,
    color: '#EB7C3B',
    lineHeight:22,
  },
  dateView: {
    marginTop:7,
    // width: deviceWidth-30,
  },
  publisherText: {
    textAlign:'right',
    paddingRight: 20,
    fontSize:16,
    color: '#535353',
    width: deviceWidth-20,
  },
  dateText: {
    textAlign:'right',
    paddingRight: 20,
    fontSize:16,
    color: '#535353',
    width: deviceWidth-20,
  }
});

@screenHOC
class SaveFeeAlgorithm extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
  }


  loadData(id){
    this.props.getPublishNoticeRead({
      id: id,
      isPush:1,
    }).catch(() => {
    });
  }

  stringChange(detailContent){
      if (detailContent != null && detailContent.indexOf('\n') != -1){
          //字符串拆分从\n开始
          let headString = detailContent.split('\n')[0] + '\n';
          let footString = detailContent.split('\n')[1].trim();
          // let str = headString + '\n' + '&emsp;&emsp;' + footString;
          this.setState({headContent:headString,footContent:footString,isContent:true})
      }
  }

  render() {
    return (
     <View></View>
    )
  }
}

SaveFeeAlgorithm.navigationOptions = ({ navigation }) => ({
  title: '计算规则',
});

SaveFeeAlgorithm.propTypes = {
  getPublishNoticeRead: PropTypes.func.isRequired,
  workDetail: PropTypes.object,
};
SaveFeeAlgorithm.defaultProps = {
  workDetail: {},
};
const mapStateToProps = createStructuredSelector({
  isLoading: makeIsLoading(),
  refreshState: makeRefreshState(),
  workDetail: makeList(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getPublishNoticeRead :getPublishNoticeReadPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SaveFeeAlgorithm);
