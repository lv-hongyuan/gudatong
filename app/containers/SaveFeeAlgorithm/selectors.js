import { createSelector } from 'reselect/es';

const selectSaveFeeAlgorithmDomain = () => state => state.saveFeeAlgorithm;

const makeIsLoading = () => createSelector(selectSaveFeeAlgorithmDomain(), (subState) => {
  console.debug(subState.isLoading);
  return subState.isLoading;
});

const makeRefreshState = () => createSelector(selectSaveFeeAlgorithmDomain(), (subState) => {
  console.debug(subState.refreshState);
  return subState.refreshState;
});

const makeList = () => createSelector(selectSaveFeeAlgorithmDomain(), (subState) => {
  console.debug(subState);
  return subState.workDetail;
});

export { makeIsLoading, makeRefreshState, makeList };
