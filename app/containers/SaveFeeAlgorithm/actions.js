import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_PUBLICH_NOTICE_READ } from './constants';

export const getPublishNoticeReadRoutine =createRoutine(GET_PUBLICH_NOTICE_READ);
export const getPublishNoticeReadPromise = promisifyRoutine(getPublishNoticeReadRoutine)
