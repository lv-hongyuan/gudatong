import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  Container,
  Content,
  Grid,
  Label,
  Row,
  View,
  Button,
  Text,
} from 'native-base';
import {
  StyleSheet,
  PixelRatio,
  RefreshControl,
  Keyboard,
  TouchableOpacity,
} from 'react-native';
import { Overlay } from 'teaset';
import { createStructuredSelector } from 'reselect/es';
import HeaderButtons from 'react-navigation-header-buttons';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import TextIconWithBadge from '../../components/TextIconWithBadge';
import {
  ROUTE_MORE_OPERATION,
  ROUTE_NAVIGATION_RECORD,
  ROUTE_DYN_FORM_UN_BERTHING,
  ROUTE_ACTUAL_BURDENING_LAYOUT,
  ROUTE_COLLIGATION,
  ROUTE_DYN_FORM_LAND,
  ROUTE_DYN_FORM_ANCHOR,
  ROUTE_VOYAGE_EVENT_LIST, ROUTE_ANCHORAGE,
  ROUTE_ARRIVEPORT_RECORD,
} from '../../RouteConstant';
import ShipWorkTitleComponent from '../../components/ShipWorkTitleComponent';
import {
  getShipDynShowPromise,
  updateDynPromise,
  changeDynTimePromise,
  toPortPromise,
  returnShipStatePromise,
} from './actions';
import { getOperationName, ShipOperation, shipState } from '../../common/Constant';
import {
  makeSelectIsSail,
  makeSelectLineName,
  makeSelectVoyageCode,
  makeSelectNavigateState,
  makeSelectTimeLineData,
  makeSelectPopId,
  makeSelectState,
  makeSelectPortId,
  makeSelectHasStowagePic,
  makeSelectHasBindPic,
  makeSelectHasReLoadPic,
  makeCanSummaryInput,
  makeLastStateTime,
  makePortName,
  makeShipName,
} from './selectors';
import TimeLineCell from './component/TimeLineCell';
import DatePullPicker from '../../components/DatePullSelector/DatePullPicker';
import EmptyView from '../../components/EmptyView';
import AlertView from '../../components/Alert';
import PilotForm from './component/PilotForm';
import screenHOC from '../screenHOC';
import { defaultFormat, toDate } from '../../utils/DateFormat';
import SummaryAnchorForm from './component/SummaryAnchorForm';
import Ionicons from 'react-native-vector-icons/MaterialIcons';
import Selector from '../../components/Selector';
const styles = StyleSheet.create({
  tip: {
    backgroundColor: '#FFFFFF',
    padding: 10,
    color: 'red',
    textAlign: 'center',
  },
  opeRow: {
    backgroundColor: '#FFFFFF',
    marginTop: 8,
    height: 90,
    // paddingLeft: 10,
    alignItems: 'center',
  },
  opeIconContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    // paddingRight: 10,
    // flex: 4,
    alignItems: 'center',
  },
  mainButton: {
    height: '100%',
    backgroundColor: '#ff171a',
    width: '100%',
    alignSelf: 'flex-end',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 0,
    paddingRight: 0,
    paddingTop: 0,
    paddingBottom: 0,
  },
  arriveport: {
    backgroundColor: '#ff171a',
    position: 'absolute',
    right: 0,
    height: '100%'
  },
  arrivalButton: {
    height: 90,
    width: '33%',
    backgroundColor: '#f4901e',
  },
  timeLineRow: {
    backgroundColor: '#FFFFFF',
    marginTop: 8,
    minHeight: 40,
    paddingBottom: 10,
    flexDirection: 'column',
  },
  timeLineRowTitle: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 45,
    paddingLeft: 20,
    borderBottomWidth: 1 / PixelRatio.getPixelSizeForLayoutSize(1),
    borderBottomColor: '#e0e0e0',
  },
  timeLineRowTitleContainer: {
    width: 5,
    height: 14,
    backgroundColor: '#1e80ff',
    marginRight: 10,
  },
});

@screenHOC
class ShipWork extends React.PureComponent {
  static navigationOptions = ({ navigation }) => ({
    headerTitle: `${navigation.getParam('title', '')}航次`,
    tabBarLabel: '航行动态',
    headerLeft: <HeaderButtons />,
  });

  static propTypes = {
    navigation: PropTypes.object.isRequired,
    naviState: PropTypes.string,
    canSummaryInput: PropTypes.bool.isRequired,
    isSail: PropTypes.bool,
    lastStateTime: PropTypes.number,
    timeLineData: PropTypes.array,
    voyageCode: PropTypes.string,
    shipName : PropTypes.string,
    lineName: PropTypes.array,
    getShipDynShow: PropTypes.func.isRequired,
    updateDyn: PropTypes.func.isRequired,
    toPort: PropTypes.func.isRequired,
    changeDynTime: PropTypes.func.isRequired,
    popId: PropTypes.number,
    state: PropTypes.number,
    hasReLoadPic: PropTypes.bool,
    hasBindPic: PropTypes.bool,
    portName: PropTypes.string,
    isConnected: PropTypes.bool.isRequired,
  };

  static defaultProps = {
    voyageCode: '', // 航次号
    lineName: [], // 航线名称
    isSail: true,
    timeLineData: [],
    naviState: {},
    state: null,
    popId: '',
    hasReLoadPic: true,
    hasBindPic: true,
    lastStateTime: null,
    portName: null,
    shipName: ''
  };

  constructor(props) {
    super(props);
    this.state = {
      isRefreshing: false,
      shipStateList:[],
    };
  }
  componentDidMount() {
    requestAnimationFrame(() => {
      this.props.getShipDynShow()
        .then(() => {
          this.setState({
            isRefreshing: false,
          });
        })
        .then(()=>{
          this.getShipStateList()
        })
        .catch(() => {
          this.setState({
            isRefreshing: false,
          });
        });
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.voyageCode !== this.props.voyageCode) {
      this.props.navigation.setParams({
        title: `${nextProps.shipName}  ${nextProps.voyageCode}` || '',
      });
    }
  }

  onBackPressed = () => false;

  onChangeLastTime = (data) => {
    const { state, description } = data;
    const timeValue = toDate(description, 'yyyy-MM-dd hh:mm').getTime();
    this.showDateSelector({
      value: timeValue,
      callBack: (time) => {
        const stateTitle = getOperationName(state);
        const message = stateTitle
          ? `确定修改${stateTitle}时间为${defaultFormat(time)}吗？`
          : `确定修改时间为${defaultFormat(time)}吗？`;
        AlertView.show({
          message,
          showCancel: true,
          confirmAction: () => {
            const { changeDynTime, popId } = this.props;
            changeDynTime({
              popId: data.popId || popId,
              time,
              dynamic: data.title,
              state: data.state,
            }).then(() => {
            }).catch(() => {
            });
          },
        });
      },
    });
  };

  didFocus() {
    this.props.navigation.setParams({
      title: `${this.props.shipName}  ${this.props.voyageCode}` || '',
    });
  }

  updateDyn(time) {
    const { updateDyn, popId, state } = this.props;
    updateDyn({
      popId,
      time,
      state,
    }).then(() => {
      this.getShipStateList()
    }).catch(() => {
    });
  }

  pullView: Overlay.PullView;


   //! 引水上船
  showPilotForm() {
    if (this.pullView) this.pullView.close();
    Keyboard.dismiss();
    const title = getOperationName(this.props.state);
    const overlayView = (
      <Overlay.PullView
        side="bottom"
        modal={false}
        ref={(ref) => {
          this.pullView = ref;
        }}
      >
        <PilotForm
          value={this.makeTimeShowValue()}
          title={title}
          onCancel={() => {
            if (this.pullView) this.pullView.close();
          }}
          onSubmit={(time) => {
            if (this.pullView) this.pullView.close();
            const message = time > 0
              ? `确定于 ${defaultFormat(time)} ${title}吗？`
              : `确定不${title}吗？`;
            AlertView.show({
              message,
              showCancel: true,
              confirmAction: () => {
                this.updateDyn(time);
              },
            });
          }}
        />
      </Overlay.PullView>
    );
    Overlay.show(overlayView);
  }

  showSummaryAnchorForm() {
    if (this.pullView) this.pullView.close();
    Keyboard.dismiss();
    const title = '下锚';
    const overlayView = (
      <Overlay.PullView
        side="bottom"
        modal={false}
        ref={(ref) => {
          this.pullView = ref;
        }}
      >
        <SummaryAnchorForm
          title={title}
          value={this.makeTimeShowValue()}
          onCancel={() => {
            if (this.pullView) this.pullView.close();
          }}
          onSubmit={(time) => {
            if (this.pullView) this.pullView.close();
            const message = time > 0
              ? `确定于 ${defaultFormat(time)} ${title}吗？`
              : `确定不${title}吗？`;
            AlertView.show({
              message,
              showCancel: true,
              confirmAction: () => {
                this.updateDyn(time);
              },
            });
          }}
        />
      </Overlay.PullView>
    );
    Overlay.show(overlayView);
  }

  //! 干线下锚
  showAnchorForm() {
    this.props.navigation.navigate(ROUTE_DYN_FORM_ANCHOR, {
      onGoBack: () => {
        this.reLoadingPage();
      },
    });
  }

   //! 点击靠泊
  showLandForm = () => {
    this.props.navigation.navigate(ROUTE_DYN_FORM_LAND, {
      onGoBack: () => {
        this.reLoadingPage();
      },
    });
  };


   //! 点击解最后一缆车
  showUnBerthForm = () => {
    const {state,hasReLoadPic,portName,lineName,navigation} = this.props;
    const isLastPort = portName === lineName[lineName.length -1 ].name
    const isxiaXianPort = state > 55 && isLastPort
    if(isxiaXianPort && !hasReLoadPic ){
      //! 下线港口+未上传图片
      AlertView.show({
        message: '当前港口是否为下线港口？',
        showCancel: true,
        cancelTitle:'否',
        confirmTitle:'是',
        mark_type:'question',
        confirmAction: () => {
          this.props.navigation.navigate(ROUTE_DYN_FORM_UN_BERTHING, {
            mark:'isLastPort',
            onGoBack: () => {
              this.reLoadingPage();
            },
          });
        },
        cancelAction: () =>{
          this.props.navigation.navigate(ROUTE_ACTUAL_BURDENING_LAYOUT, {
            onGoBack: () => {
              this.reLoadingPage();
            },
          });
        }
      });

    } else if(!hasReLoadPic && !isxiaXianPort){
      //! 非下线港口+未上传实载图
      this.props.navigation.navigate(ROUTE_ACTUAL_BURDENING_LAYOUT, {
        onGoBack: () => {
          this.reLoadingPage();
        },
      });
    } else {
      //! 实载图已上传，跳转至离泊登记界面
      this.props.navigation.navigate(ROUTE_DYN_FORM_UN_BERTHING, {
        mark: isxiaXianPort ? 'isLastPort' : 'notLastPort',
        onGoBack: () => {
          this.reLoadingPage();
        },
      });
    }
  };

  showDateSelector = (options) => {
    if (this.pullView) this.pullView.close();
    Keyboard.dismiss();

    const { value, callBack } = options || {};

    const overlayView = (
      <Overlay.PullView
        side="bottom"
        modal={false}
        ref={(ref) => {
          this.pullView = ref;
        }}
      >
        <DatePullPicker
          value={value}
          onCancel={() => {
            if (this.pullView) this.pullView.close();
          }}
          onSubmit={(time) => {
            if (this.pullView) this.pullView.close();
            if (callBack) callBack(time);
          }}
        />
      </Overlay.PullView>
    );
    Overlay.show(overlayView);
  };

  mainOperationButtonPress = (clickstate) => {
    const {
      navigation, state, isSail, hasBindPic,
    } = this.props;

    if (isSail) {
      //! 航行状态
      if (clickstate == 'updata') {
        //! 点击录入航行船报
        navigation.navigate(ROUTE_NAVIGATION_RECORD, {
          isConnected:this.props.isConnected,
          onGoBack: () => {
            this.reLoadingPage();
          },
        });
      } else if (clickstate == 'makesure') {
         //! 点击录入抵港船报
        navigation.navigate(ROUTE_ARRIVEPORT_RECORD, {
          isConnected:this.props.isConnected,
          onGoBack: () => {
            this.reLoadingPage();
          },
        });
      }
    } else if (!this.props.canSummaryInput && state === ShipOperation.LIBO) {
       //! 解最后一缆
      this.showUnBerthForm();
    } else if (!this.props.canSummaryInput && state === ShipOperation.KAOBO) {
       //! 点击靠泊
      this.showLandForm();
    } else if (state === ShipOperation.XIAMAO) {
       //! 点击下锚
      if (this.props.canSummaryInput) {
        //! 如果是支线
        this.showSummaryAnchorForm();
      } else {
        //! 干线
        this.showAnchorForm();
      }
    } else if (state === ShipOperation.YINSHUI1 || state === ShipOperation.YINSHUI2) {
      //! 引水上船
      this.showPilotForm();
    } else if (!this.props.canSummaryInput && state === ShipOperation.BANGZAJS) {
      this.bandFinish()
    } else {
      this.showDateSelector({
        value: this.makeTimeShowValue(),
        callBack: (time) => {
          const title = getOperationName(state);
          const timeStr = defaultFormat(time);
          AlertView.show({
            message: `确定于 ${timeStr} ${title}吗？`,
            showCancel: true,
            confirmAction: () => {
              this.updateDyn(time);
            },
          });
        },
      });
    }
  };

  //! 绑扎完成
  bandFinish(){
    const {state,hasBindPic,portName,lineName} = this.props;
    //! 判断当前港口是否为当前航线的最后一个港口
    let isLastPort = portName === lineName[lineName.length -1 ].name
    //! 判断当前港口为下线港口（state大于55且为航线中最后一个港口）
    const isxiaXianPort = state > 55 && isLastPort
    if(isxiaXianPort && !hasBindPic ){
      //! 如果当前港口为下线港口且 未 上传绑扎图，弹出选项框，点击是弹出时间选择框，选择否跳转至图片上传界面
      AlertView.show({
        message: '当前港口是否为下线港口？',
        showCancel: true,
        cancelTitle:'否',
        confirmTitle:'是',
        mark_type:'question',
        confirmAction: () => {
          this.showDateSelector({
            value: this.makeTimeShowValue(),
            callBack: (time) => { 
              const title = getOperationName(state);
              const timeStr = defaultFormat(time);
              AlertView.show({
                message: `确定于 ${timeStr} ${title}吗？`,
                showCancel: true,
                confirmAction: () => {
                  this.updateDyn(time);
                },
              });
            },
          });
        },
        cancelAction: () =>{
          this.props.navigation.navigate(ROUTE_COLLIGATION, {
            onGoBack: () => {
              this.reLoadingPage();
            },
          });
        }
      });
    }else if( !isxiaXianPort && !hasBindPic){
      //! 如果当前港口不是下线港口且未上传绑扎图，跳转至图片上传界面
      this.props.navigation.navigate(ROUTE_COLLIGATION, {
        onGoBack: () => {
          this.reLoadingPage();
        },
      });
    } else {
      //! 如果当前港口已上传绑扎图，弹出时间选择框，
      this.showDateSelector({
        value: this.makeTimeShowValue(),
        callBack: (time) => {
          const title = getOperationName(state);
          const timeStr = defaultFormat(time);
          AlertView.show({
            message: `确定于 ${timeStr} ${title}吗？`,
            showCancel: true,
            confirmAction: () => {
              this.updateDyn(time);
            },
          });
        },
      });
      }
  }

  /**
   * 生成阶段时间选选择事的显示时间
   * 逻辑为:
   * || 到达锚地 =>下锚时间
   * || 起锚时间 => 引水上船 => 第一缆上桩 => 靠泊完成 => 开始作业
   * || 完成作业 =>  绑扎完成 => 开始离泊 => 解最后一缆 => 引水下船 => 定速航行
   * 这些小阶段的时间间隔默认为半小时（大于当前时间时，使用当前时间）
   * 其他阶段使用当前时间
   *
   * @returns 计算好的时间或者{null}
   */
  makeTimeShowValue() {
    const { lastStateTime, state } = this.props;
    switch (state) {
      case ShipOperation.XIAMAO:// 下锚时间
      case ShipOperation.YINSHUI1:// 引水上船
      case ShipOperation.KAOBO:// 开始靠泊
      case ShipOperation.KAOBOWC:// 靠泊完成
      case ShipOperation.BANGZAJS:// 绑扎结束
      case ShipOperation.KAISHIJIELAN:// 开始离泊
      case ShipOperation.LIBO: // 离泊完成
      case ShipOperation.YINSHUI2:// 引水下船
      case ShipOperation.ZHENGHANG:// 定速正航
        {
          if (!lastStateTime) return null;
          const suitableTime = lastStateTime + 1800000; // 增加半小时
          const currentTime = new Date().getTime(); // 当前时间
          if (suitableTime > currentTime) return null; // 如果超过当前时间，返回null
          return suitableTime;
        }
      default:
        return null;
    }
  }

  reLoadingPage = () => {
    requestAnimationFrame(() => {
      this.props.getShipDynShow()
        .then(() => {
          this.setState({
            isRefreshing: false,
          });
        })
        .then(()=>{
          this.getShipStateList()
        })
        .catch(() => {
          this.setState({
            isRefreshing: false,
          });
        });
    });
  };

  //! 获取回退列表
  getShipStateList(){
    console.log(this.props);
    
    let list = []
    let anchor = false
    const state = this.props.state
    for(var j = 0;j < this.props.timeLineData.length;j++){
      //!判断是否下锚
      if(this.props.timeLineData[j].title === '下锚时间'){
        anchor = true
      } 
    }
    for(var i = 0;i < shipState.length;i++){
      if(shipState[i].id < state){
        //! 如果没有下锚 回退时不显示起锚选项
        if(shipState[i].id == 30 && !anchor) continue
        list.push(shipState[i])
      }
    }
    this.setState({
      shipStateList:list
    })
  }

  renderOperationSection = () => {
    const {
      navigation, state, isSail, hasReLoadPic, hasBindPic,
    } = this.props;
    const isNavigating = isSail;
    return (
      <Row style={styles.opeRow}>
        <View style={styles.opeIconContainer}>
          <TextIconWithBadge
            style={{ width: isNavigating ? '25%' : '22%', height: '100%' }}
            text={isNavigating ? '途中停航' : '绑扎图上传'}
            textSize={isNavigating ? 14 : 12}
            showBadge={!isNavigating && !hasBindPic}
            iconSize={45}
            badgeText="!"
            icon={isNavigating ? 'burstOutEvent' : 'colligation'}
            color="#dc001b"
            onClick={() => {
              if (isNavigating) {
                navigation.navigate(ROUTE_VOYAGE_EVENT_LIST);
              } else {
                navigation.navigate(ROUTE_COLLIGATION, {
                  onGoBack: () => {
                    this.reLoadingPage();
                  },
                });
              }
            }}
          />
          <TextIconWithBadge
            style={{ width: isNavigating ? '25%' : '22%', height: '100%' }}
            text={isNavigating ? '港内停航' : '实载图上传'}
            textSize={isNavigating ? 14 : 12}
            showBadge={!isNavigating && !hasReLoadPic}
            badgeText="!"
            onClick={() => {
              if (isNavigating) {
                navigation.navigate(ROUTE_ANCHORAGE);
              } else {
                navigation.navigate(
                  ROUTE_ACTUAL_BURDENING_LAYOUT,
                  {
                    onGoBack: () => {
                      this.reLoadingPage();
                    },
                  },
                );
              }
            }}
            icon={isNavigating ? 'pilotApply' : 'actualBurdeningLayout'}
            color="#dc001b"
            iconSize={45}
          />
          <TextIconWithBadge
            style={{ width: isNavigating ? '25%' : '22%', height: '100%' }}
            text={isNavigating ? '抵港确报' : '更多信息'}
            textSize={14}
            showBadge={false}
            badgeText=''
            onClick={() => {
              if (isNavigating) {
                this.mainOperationButtonPress('makesure');
              } else {
                requestAnimationFrame(() => {
                  this.props.navigation.navigate(ROUTE_MORE_OPERATION);
                });
              }
            }}
            icon={isNavigating ? 'time' : 'more'}
            color="#dc001b"
            iconSize={45}
          />
          {isNavigating ?
            <TextIconWithBadge
              style={{ width: '25%', height: '100%' }}
              text={'航行船报'}
              textSize={14}
              showBadge={false}
              badgeText=''
              onClick={() => { this.mainOperationButtonPress('updata'); }}
              icon={'anchorage'}
              color="#dc001b"
              iconSize={45}
            /> :
            <View
              style={{
                flex: 1,
              }}
            >
              <Button
                full
                style={styles.mainButton}
                onPress={() => {
                  this.mainOperationButtonPress('updata');
                }}
              >
                <Text
                  style={{
                    fontSize: 19,
                    paddingLeft: 2,
                    paddingRight: 2,
                    paddingTop: 2,
                    paddingBottom: 2,
                    textAlign: 'center',
                  }}
                >{getOperationName(state)}</Text>
              </Button>
            </View>
          }
        </View>
      </Row>
    );
  };

  //! 点击回退按钮
  onBtnType() {
    this.selector.showPicker();
  };

  //!回退事件
  onChangeState = (item)=>{
    const {popId,returnShipState} = this.props;
    AlertView.show({
      message: `确认回退到${item.value}吗？`,
      mark_type:'question',
      showCancel: true,
      confirmAction: () => {
        console.log(item,popId);
        returnShipState({timeStateShip:item.id, id:popId,})
          .then(()=>{
            this.reLoadingPage();
          })
          .catch((e)=>{
            console.log(e);
            
          })
      },
    });
  }
  renderTimeLineSection = () => {
    const { isSail, timeLineData } = this.props;
    const isNavigating = isSail;
    return (
      <Row style={styles.timeLineRow}>
        <Row style={styles.timeLineRowTitle}>
          <TouchableOpacity 
            style={{
              width:30,height:30,marginRight:5,overflow:'hidden',
            }}
            onPress={()=>{
              this.onBtnType()
            }}
            activeOpacity={1}
          >
          <Ionicons 
              name={'settings-backup-restore'}
              size={30}
              style={{ color: 'rgb(217,10,37)' }}
            />
          <View style={{
            position:'absolute',backgroundColor:'#fff',
            left:9,top:9,width:11
          }}>
            <Text style={{fontSize:11,color:'rgb(217,10,37)',lineHeight:12}}>退</Text>
          </View>
          
          <Selector
            items={this.state.shipStateList}
            ref={(ref) => {
              this.selector = ref;
            }}
            onPress={this.onBtnType}
            pickerTitle="请选择回退操作"
            getItemValue={item => item.value}
            getItemText={item => item.value}
            onSelected={this.onChangeState}
          />
          
          </TouchableOpacity>
          <Label style={{ color: '#535353', fontSize: 14 }}>{isNavigating ? '航行动态' : '港口动态'}</Label>
          {
            isSail ? <Button
              full
              style={styles.arriveport}
              onPress={() => {
                AlertView.show({
                  message: '确认到港吗？',
                  showCancel: true,
                  confirmAction: () => {
                    const { toPort, popId } = this.props;
                    toPort({ popId }).then(() => {
                      this.reLoadingPage();
                    }).catch(() => {
                    });
                  },
                });
              }}
            >
              <Text style={{
                fontSize: 14,
                textAlign: 'center',
                // color: '#ff171a',
              }}
              >点此到港</Text>
            </Button> : null
          }
        </Row>
        <View style={{ paddingTop: 20 }}>
          {
            timeLineData.map((data, index) => (
              <TimeLineCell
                canEdit={!isNavigating && data.state !== 0 && index >= 1}
                onChangeTime={() => {
                  this.onChangeLastTime(data);
                }}
                key={`${data.id}`}
                time={data.description}
                title={data.title}
                isCurrent={index === 0}
                showTopLine={index > 0}
                showBottomLine={index < timeLineData.length - 1}
              />
            ))
          }
        </View>
      </Row>
    );
  };

  render() {
    const {
      voyageCode, lineName, naviState, isSail, portName,
    } = this.props;
    const { isRefreshing } = this.state;
    return (
      <Container>
        <Content
          refreshControl={
            <RefreshControl
              refreshing={isRefreshing}
              onRefresh={() => {
                this.setState({
                  isRefreshing: true,
                });
                this.reLoadingPage();
              }}
            />
          }
        >
          {
            voyageCode ? (
              <Grid>
                <Text style={styles.tip}>操作前请核对港口和航次</Text>
                <ShipWorkTitleComponent
                  portName={portName}
                  runningState={lineName}
                  state={naviState}
                  isSail={isSail}
                />
                {this.renderOperationSection()}
                {this.renderTimeLineSection()}
              </Grid>
            ) : (
                <View style={{ width: '100%', height: '100%', alignItems: 'center' }}>
                  <EmptyView />
                </View>
              )
          }
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  voyageCode: makeSelectVoyageCode(), // 航次号
  lineName: makeSelectLineName(), // 航线名称
  isSail: makeSelectIsSail(), // 是否航行中 0航行 1港口
  state: makeSelectState(),
  timeLineData: makeSelectTimeLineData(),
  naviState: makeSelectNavigateState(),
  popId: makeSelectPopId(),
  portId: makeSelectPortId(),
  hasStowagePic: makeSelectHasStowagePic(),
  hasBindPic: makeSelectHasBindPic(),
  hasReLoadPic: makeSelectHasReLoadPic(),
  canSummaryInput: makeCanSummaryInput(),
  lastStateTime: makeLastStateTime(),
  portName: makePortName(),
  shipName: makeShipName(),
  isConnected: state => state.network.isConnected,
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getShipDynShow: getShipDynShowPromise,
      updateDyn: updateDynPromise,
      changeDynTime: changeDynTimePromise,
      returnShipState: returnShipStatePromise,
      toPort: toPortPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ShipWork);
