import { createSelector } from 'reselect/es';
import _ from 'lodash';
import ShortId from 'shortid';
import { LineType } from '../../common/Constant';
import { defaultFormat } from '../../utils/DateFormat';

const selectShipWorkDomain = () => state => state.shipWork;

const makeSelectVoyageCode = () => createSelector(
  selectShipWorkDomain(),
  subState => (subState.data || {}).voyageCode,
);
const makeShipName = () =>createSelector(
  selectShipWorkDomain(),
  subState => (subState.data || {}).shipName,
);
const makeSelectLineName = () => createSelector(
  selectShipWorkDomain(),
  (subState) => {
    if (subState.data.lineName) {
      const data = subState.data.lineName.split('-');
      const currentIndex = Math.min(data.indexOf(subState.data.portName), data.length - 1);//当前港口在所有港口中的位置
      const result = [];
      const dataLength = data.length;  //港口数量
      const isSail = `${(subState.data || {}).isSail}` === '0';
      const portIndexes = [];  //要显示的港口
      if (currentIndex === 0) {
        portIndexes.push(0);
        portIndexes.push(1);
        if (dataLength > 2) {
          portIndexes.push(2);
        }
      } else if (currentIndex === dataLength - 1) {
        if (dataLength >= 3) {
          portIndexes.push(currentIndex - 2)
          portIndexes.push(currentIndex - 1)
          portIndexes.push(currentIndex);
        }else{
          portIndexes.push(0);
          portIndexes.push(currentIndex);
        }
      } else if (currentIndex > 0 && currentIndex < dataLength - 1) {
        portIndexes.push(currentIndex - 1);
        portIndexes.push(currentIndex);
        portIndexes.push(currentIndex + 1);
      } else {
        portIndexes.push(0);
        portIndexes.push(dataLength - 1);
      }
      portIndexes.forEach((portIndex) => {
        const info = {
          id: ShortId.generate(),
          name: data[portIndex],
          showLeft: portIndex !== portIndexes[0],
          showRight: portIndex !== portIndexes[portIndexes.length - 1],
        };

        if (portIndex < currentIndex) {
          result.push({
            isShip: false,
            arrive: true,
            ...info,
          });
        } else if (portIndex === currentIndex) {
          if (isSail) {
            result.push({
              isShip: false,
              arrive: true,
              ...info,
            });
            result.push({
              isShip: true,
              arrive: true,
              ...info,
              name: '航行中',
              showLeft: true,
              showRight: true,
              id: ShortId.generate(),
            });
          } else {
            result.push({
              isShip: false,
              arrive: true,
              ...info,
            });
          }
        } else {
          result.push({
            isShip: false,
            arrive: false,
            ...info,
          });
        }
      });
      return result;
    }
    return [];
  },
);

const makeSelectIsSail = () => createSelector(
  selectShipWorkDomain(),
  subState => `${(subState.data || {}).isSail}` === '0',
);

const makeSelectNavigateState = () => createSelector(
  selectShipWorkDomain(),
  subState => (subState.data || {}).dynMemo,
);

const makeSelectTimeLines = () => createSelector(
  selectShipWorkDomain(),
  subState => (subState.data || {}).timeLines || [],
);

const makeSelectTimeLineData = () => createSelector(
  makeSelectTimeLines(),
  timeLines => timeLines.map(item => ({
    title: item.dynamic,
    description: item.time ? defaultFormat(item.time) : '',
    state: item.state,
    popId: item.popId,
    id: ShortId.generate(),
  })).reverse(),
);

const makeSelectState = () => createSelector(
  selectShipWorkDomain(),
  subState => subState.data && subState.data.state,
);

const makeUnReadMes = () => createSelector(
  selectShipWorkDomain(),
  subState => subState.unReadMes || [],
);

const makeSelectPortId = () => createSelector(
  selectShipWorkDomain(),
  subState => subState.data && subState.data.portId,
);

const makeSelectPopId = () => createSelector(
  selectShipWorkDomain(),
  subState => subState.data && subState.data.popId,
);

const makeSelectHasStowagePic = () => createSelector(
  selectShipWorkDomain(),
  subState => `${(subState.data || {}).stowagePic}` === '1',
);

const makeSelectHasBindPic = () => createSelector(
  selectShipWorkDomain(),
  subState => `${(subState.data || {}).bindPic}` === '1',
);

const makeSelectHasReLoadPic = () => createSelector(
  selectShipWorkDomain(),
  subState => `${(subState.data || {}).reLoadPic}` === '1',
);

const makeReachReport = () => createSelector(
  selectShipWorkDomain(),
  subState => subState.reachReport,
);

const makeBerthReport = () => createSelector(
  selectShipWorkDomain(),
  subState => subState.berthReport,
);

const makeLeaveReport = () => createSelector(
  selectShipWorkDomain(),
  subState => subState.leaveReport,
);

const makeCanSummaryInput = () => createSelector(
  selectShipWorkDomain(),
  subState => (subState.data || {}).lineType === LineType.ZHI_XIAN,
);

const makePortName = () => createSelector(
  selectShipWorkDomain(),
  subState => (subState.data || {}).portName,
);

const makeLastStateTime = () => createSelector(
  makeSelectTimeLines(),
  timeLines => timeLines.reduce((pre, { time }) => {
    if (_.isNil(pre)) return time;
    if (_.isNil(time)) return pre;
    return Math.max(pre, time);
  }, undefined),
);

const makeFirstStateTime = () => createSelector(
  makeSelectTimeLines(),
  timeLines => timeLines.reduce((pre, { time }) => {
    if (_.isNil(pre)) return time;
    if (_.isNil(time)) return pre;
    return Math.min(pre, time);
  }, undefined),
);

export {
  makeSelectVoyageCode,
  makeSelectLineName,
  makeSelectIsSail,
  makeSelectState,
  makeSelectNavigateState,
  makeSelectTimeLineData,
  makeSelectPopId,
  makeSelectPortId,
  makeSelectHasStowagePic,
  makeSelectHasBindPic,
  makeSelectHasReLoadPic,
  makeReachReport,
  makeBerthReport,
  makeLeaveReport,
  makeCanSummaryInput,
  makeLastStateTime,
  makeFirstStateTime,
  makePortName,
  makeShipName,
  makeUnReadMes
};
