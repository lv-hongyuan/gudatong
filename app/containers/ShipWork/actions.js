import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  SHIP_DYN_SHOW,
  INPUT_DYN,
  CHANGE_DYN_TIME,
  TO_PORT_ACTION,
  SAVE_DYN,
  GET_REACH_REPORT,
  GET_BERTH_REPORT,
  GET_LEAVE_REPORT,
  GET_UNREADMES,
  RETURN_SHIP_STATE
} from './constants';

export const getShipDynShowRoutine = createRoutine(
  SHIP_DYN_SHOW,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const getShipDynShowPromise = promisifyRoutine(getShipDynShowRoutine);

export const updateDynRoutine = createRoutine(
  INPUT_DYN,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const updateDynPromise = promisifyRoutine(updateDynRoutine);

export const saveDynRoutine = createRoutine(
  SAVE_DYN,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const saveDynPromise = promisifyRoutine(saveDynRoutine);

export const changeDynTimeRoutine = createRoutine(
  CHANGE_DYN_TIME,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const changeDynTimePromise = promisifyRoutine(changeDynTimeRoutine);

export const toPortRoutine = createRoutine(
  TO_PORT_ACTION,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const toPortPromise = promisifyRoutine(toPortRoutine);

export const getReachReportRoutine = createRoutine(
  GET_REACH_REPORT,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const getReachReportPromise = promisifyRoutine(getReachReportRoutine);

//请求未读消息数量
export const getUnReadMesRoutine = createRoutine(
  GET_UNREADMES,
  updates => updates,
  () => ({ globalLoading: false }),
);
export const getUnReadMesPromise = promisifyRoutine(getUnReadMesRoutine);


export const getBerthReportRoutine = createRoutine(
  GET_BERTH_REPORT,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const getBerthReportPromise = promisifyRoutine(getBerthReportRoutine);

export const getLeaveReportRoutine = createRoutine(
  GET_LEAVE_REPORT,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const getLeaveReportPromise = promisifyRoutine(getLeaveReportRoutine);

export const returnShipStateRoutine = createRoutine(
  RETURN_SHIP_STATE,
  updates => updates,
  () => ({ globalLoading: true }),
)
export const returnShipStatePromise = promisifyRoutine(returnShipStateRoutine)
