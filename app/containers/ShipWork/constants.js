/*
 *
 * ShipWork constants
 *
 */
export const SHIP_DYN_SHOW = 'app/ShipWork/SHIP_DYN_SHOW';
export const GET_REACH_REPORT = 'app/ShipWork/GET_REACH_REPORT';
export const GET_BERTH_REPORT = 'app/ShipWork/GET_BERTH_REPORT';
export const GET_LEAVE_REPORT = 'app/ShipWork/GET_LEAVE_REPORT';
export const INPUT_DYN = 'app/ShipWork/INPUT_DYN';
export const SAVE_DYN = 'app/ShipWork/SAVE_DYN';
export const CHANGE_DYN_TIME = 'app/ShipWork/CHANGE_DYN_TIME';
export const TO_PORT_ACTION = 'app/ShipWork/TO_PORT_ACTION';
export const GET_UNREADMES = 'app/ShipWork/GET_UNREADMES';
export const RETURN_SHIP_STATE = 'app/shipWork/RETURN_SHIP_STATE';
