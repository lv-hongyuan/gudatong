import { call, put, select, takeLatest } from 'redux-saga/effects';
import _ from 'lodash';
import { AsyncStorage } from 'react-native';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import {
  getShipDynShowRoutine,
  getReachReportRoutine,
  getBerthReportRoutine,
  getLeaveReportRoutine,
  updateDynRoutine,
  saveDynRoutine,
  changeDynTimeRoutine,
  toPortRoutine,
  getUnReadMesRoutine,
  returnShipStateRoutine,
} from './actions';
import { errorMessage } from '../ErrorHandler/actions';
import uploadImage from '../../common/uploadImage';
import StorageKeys from '../../common/StorageKeys';

export function* getLatestDyn() {
  try {
    const response = yield call(request, ApiFactory.shipDynShow());
    if (!_.isNil(response.isSail)) {
      yield call(AsyncStorage.setItem, StorageKeys.IS_SAIL, `${response.isSail}`);
    }
    yield put(getShipDynShowRoutine.success(response));
    return response;
  } catch (e) {
    throw e;
  }
}

export function* getDyn() {
  try {
    let dyn = yield select(state => state.shipWork.data);
    if (!dyn || !dyn.popId) {
      dyn = yield call(getLatestDyn);
    }
    return dyn;
  } catch (e) {
    throw e;
  }
}

export function* getPopId() {
  try {
    const response = yield call(getDyn);
    return response.popId;
  } catch (e) {
    throw e;
  }
}

export function* getShipId() {
  try {
    const response = yield call(getDyn);
    return response.shipId;
  } catch (e) {
    throw e;
  }
}

export function* getVoyageId() {
  try {
    const response = yield call(getDyn);
    return response.voyageId;
  } catch (e) {
    throw e;
  }
}

export function* getVoyageCode() {
  try {
    const response = yield call(getDyn);
    return response.voyageCode;
  } catch (e) {
    throw e;
  }
}

function* shipDynShow(action) {
  console.log(action);
  try {
    yield put(getShipDynShowRoutine.request());
    const response = yield call(request, ApiFactory.shipDynShow());
    console.log('shipDynShow', response);
    if (!_.isNil(response.isSail)) {
      yield call(AsyncStorage.setItem, StorageKeys.IS_SAIL, `${response.isSail}`);
      yield call(AsyncStorage.setItem, StorageKeys.DYN, JSON.stringify(response));
    }
    yield put(getShipDynShowRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(getShipDynShowRoutine.failure(e));
  } finally {
    yield put(getShipDynShowRoutine.fulfill());
  }
}

export function* shipDynShowSaga() {
  yield takeLatest(getShipDynShowRoutine.TRIGGER, shipDynShow);
}
function* getUnReadMes(action){
  try{
    yield put(getUnReadMesRoutine.request());
    const response = yield call(request, ApiFactory.getNotReadNoticeNum());
    console.log('未读消息：',response);
    yield put(getUnReadMesRoutine.success({
      list:(response.dtoList ? response.dtoList : [])
    }));
  }catch(e){
    yield put(errorMessage(e.message));
    yield put(getUnReadMesRoutine.failure(e));
  } finally {
    yield put(getUnReadMesRoutine.fulfill());
  }
}

export function* getUnReadMesSaga(){
  yield takeLatest(getUnReadMesRoutine.TRIGGER, getUnReadMes)
}

function* getReachReport(action) {
  console.log(action);
  try {
    yield put(getReachReportRoutine.request());
    const popId = yield call(getPopId);
    const response = yield call(request, ApiFactory.getReachReport(popId));
    console.log('getReachReport', response.toString());
    if (!_.isNil(response.isSail)) {
      yield call(AsyncStorage.setItem, StorageKeys.IS_SAIL, `${response.isSail}`);
    }
    yield put(getReachReportRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(getReachReportRoutine.failure(e));
  } finally {
    yield put(getReachReportRoutine.fulfill());
  }
}

export function* getReachReportSaga() {
  yield takeLatest(getReachReportRoutine.TRIGGER, getReachReport);
}

function* getBerthReport(action) {
  console.log(action);
  try {
    yield put(getBerthReportRoutine.request());
    const popId = yield call(getPopId);
    const response = yield call(request, ApiFactory.getBerthReport(popId));
    console.log('getBerthReport', response.toString());
    if (!_.isNil(response.isSail)) {
      yield call(AsyncStorage.setItem, StorageKeys.IS_SAIL, `${response.isSail}`);
    }
    yield put(getBerthReportRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(getBerthReportRoutine.failure(e));
  } finally {
    yield put(getBerthReportRoutine.fulfill());
  }
}

export function* getBerthReportSaga() {
  yield takeLatest(getBerthReportRoutine.TRIGGER, getBerthReport);
}

function* getLeaveReport(action) {
  console.log(action);
  try {
    yield put(getLeaveReportRoutine.request());
    const popId = yield call(getPopId);
    const response = yield call(request, ApiFactory.getLeaveReport(popId));
    console.log('getLeaveReport', response.toString());
    yield put(getLeaveReportRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(getLeaveReportRoutine.failure(e));
  } finally {
    yield put(getLeaveReportRoutine.fulfill());
  }
}

export function* getLeaveReportSaga() {
  yield takeLatest(getLeaveReportRoutine.TRIGGER, getLeaveReport);
}

function* saveDyn(action) {
  console.log(action);
  try {
    yield put(saveDynRoutine.request());
    const param = action.payload;
    if (!!param.berth && !!param.berth.berthPic) {
      const { berthPic } = param.berth;
      if (berthPic.path) {
        const businessId = yield call(getPopId);
        const response = yield call(uploadImage, berthPic, 'berthPic', businessId);
        param.berth.berthPic = response.id;
      } else {
        param.berth.berthPic = berthPic.substr(berthPic.lastIndexOf('/') + 1);
      }
    }
    const response = yield call(request, ApiFactory.saveReport(param));
    console.log('saveDyn', response);
    if (!_.isNil(response.isSail)) {
      yield call(AsyncStorage.setItem, StorageKeys.IS_SAIL, `${response.isSail}`);
    }
    yield put(saveDynRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(saveDynRoutine.failure(e));
  } finally {
    yield put(saveDynRoutine.fulfill());
  }
}

export function* saveDynSaga() {
  yield takeLatest(saveDynRoutine.TRIGGER, saveDyn);
}

function* inputDyn(action) {
  console.log(action);
  try {
    yield put(updateDynRoutine.request());
    const param = action.payload;
    if (!!param.berth && !!param.berth.berthPic) {
      const { berthPic } = param.berth;
      if (berthPic.path) {
        const businessId = yield call(getPopId);
        const response = yield call(uploadImage, berthPic, 'berthPic', businessId);
        param.berth.berthPic = response.id;
      } else {
        param.berth.berthPic = berthPic.substr(berthPic.lastIndexOf('/') + 1);
      }
    }
    const response = yield call(request, ApiFactory.inputDyn(param));
    console.log('inputDyn', response.toString());
    if (!_.isNil(response.isSail)) {
      yield call(AsyncStorage.setItem, StorageKeys.IS_SAIL, `${response.isSail}`);
    }
    yield put(updateDynRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(updateDynRoutine.failure(e));
  } finally {
    yield put(updateDynRoutine.fulfill());
  }
}

export function* inputDynSaga() {
  yield takeLatest(updateDynRoutine.TRIGGER, inputDyn);
}

function* changeDynTime(action) {
  console.log(action);
  try {
    yield put(changeDynTimeRoutine.request());
    const param = action.payload;
    const response = yield call(request, ApiFactory.updateTime(param));
    console.log('changeDynTime', response);
    const dynResponse = yield call(request, ApiFactory.shipDynShow());
    console.log('shipDynShow', dynResponse);
    yield put(getShipDynShowRoutine.success(dynResponse));
    yield put(changeDynTimeRoutine.success());
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(changeDynTimeRoutine.failure(e));
  } finally {
    yield put(changeDynTimeRoutine.fulfill());
  }
}

export function* changeDynTimeSaga() {
  yield takeLatest(changeDynTimeRoutine.TRIGGER, changeDynTime);
}

function* toPort(action) {
  console.log(action);
  try {
    yield put(toPortRoutine.request());
    const response = yield call(request, ApiFactory.toPort(action.payload));
    console.log('toPort', response.toString());
    if (!_.isNil(response.isSail)) {
      yield call(AsyncStorage.setItem, StorageKeys.IS_SAIL, `${response.isSail}`);
    }
    yield put(toPortRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(toPortRoutine.failure(e));
  } finally {
    yield put(toPortRoutine.fulfill());
  }
}

export function* toPortSaga() {
  yield takeLatest(toPortRoutine.TRIGGER, toPort);
}

//! 点击回退
function* returnShipState(action){
  console.log('action:',action);
  try {
    yield put(returnShipStateRoutine.request());
    const response = yield call(request, ApiFactory.returnShipState(action.payload));
    yield put(returnShipStateRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(returnShipStateRoutine.failure(e));
  } finally {
    yield put(returnShipStateRoutine.fulfill());
  }

}

export function* returnShipStateSage() {
  yield takeLatest(returnShipStateRoutine.TRIGGER, returnShipState);
}
// All sagas to be loaded
export default [
  shipDynShowSaga,
  getReachReportSaga,
  getBerthReportSaga,
  getLeaveReportSaga,
  inputDynSaga,
  saveDynSaga,
  changeDynTimeSaga,
  toPortSaga,
  getUnReadMesSaga,
  returnShipStateSage,
];
