import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, PixelRatio } from 'react-native';
import { Button, View, Text } from 'native-base';
import Svg from '../../../components/Svg';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    width: '100%',
    minHeight: 40,
  },
  timeContainer: {
    alignItems: 'flex-end',
    paddingRight: 10,
    paddingBottom: 20,
  },
  time: {
    color: '#969696',
    fontSize: 12,
  },
  dataContainer: {
    alignItems: 'flex-start',
    paddingLeft: 10,
    paddingBottom: 20,
  },
  title: {
    color: '#969696',
  },
  detail: {
    color: '#969696',
  },
  topLine: {
    backgroundColor: '#969696',
    width: 1 / PixelRatio.getPixelSizeForLayoutSize(1),
    height: 10,
  },
  bottomLine: {
    backgroundColor: '#969696',
    width: 1 / PixelRatio.getPixelSizeForLayoutSize(1),
    flex: 1,
  },
  circle: {
    width: 10,
    height: 10,
    borderRadius: 6,
    backgroundColor: '#969696',
    top: 5,
    position: 'absolute',
  },
  editButton: {
    width: 25,
    height: 25,
    padding: 0,
    borderRadius: 15,
    backgroundColor: '#ffffff',
    top: -5,
    left: -7.5,
    position: 'absolute',
  },
  left: {
    width: 100,
    alignItems: 'flex-end',
  },
  lineContainer: {
    width: 10,
    alignItems: 'center',
  },
  right: {
    flex: 2,
  },
});

/**
 * Created by ocean on 2018/4/24
 */
class TimeLineCell extends React.Component {
  renderCircle() {
    if (this.props.canEdit) {
      return (
        <Button
          rounded
          style={styles.editButton}
          onPress={() => {
            if (this.props.onChangeTime) this.props.onChangeTime();
          }}
        >
          <Svg icon="edit_normal" size={25} color="#dc001b" />
        </Button>
      );
    }
    return (
      <View
        style={[
          styles.circle,
          { backgroundColor: this.props.isCurrent ? '#dc001b' : '#969696' },
        ]}
      />
    );
  }

  render() {
    const timeArray = this.props.time.split(' ');

    return (
      <View style={styles.container}>
        <View style={styles.left}>
          {
            this.props.time ? (
              <View style={styles.timeContainer}>
                <Text style={styles.time}>{timeArray[0]}</Text>
                <Text style={styles.time}>{timeArray[1]}</Text>
              </View>
            ) : null
          }
        </View>
        <View style={styles.lineContainer}>
          <View style={[
            styles.topLine,
            { backgroundColor: this.props.showTopLine ? '#969696' : 'transparent' },
          ]}
          />
          <View style={[
            styles.bottomLine,
            { backgroundColor: this.props.showBottomLine ? '#969696' : 'transparent' },
          ]}
          />
          {this.renderCircle()}
        </View>
        <View style={styles.right}>
          <View style={styles.dataContainer}>
            <Text
              style={[styles.title, { color: this.props.isCurrent ? '#dc001b' : '#969696' }]}
            >{this.props.title}
            </Text>
            <Text
              style={[styles.detail, { color: this.props.isCurrent ? '#dc001b' : '#969696' }]}
            >{this.props.detail}
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

TimeLineCell.propTypes = {
  title: PropTypes.string,
  detail: PropTypes.string,
  time: PropTypes.string,
  isCurrent: PropTypes.bool,
  showBottomLine: PropTypes.bool,
  showTopLine: PropTypes.bool,
  canEdit: PropTypes.bool,
  onChangeTime: PropTypes.func,
};

TimeLineCell.defaultProps = {
  title: undefined,
  detail: undefined,
  time: undefined,
  showTopLine: true,
  showBottomLine: true,
  isCurrent: false,
  canEdit: true,
  onChangeTime: undefined,
};

export default TimeLineCell;
