import React from 'react';
import PropTypes from 'prop-types';
import { PixelRatio, StyleSheet, View } from 'react-native';
import { InputGroup, Item, Label, Button, Text } from 'native-base';
import commonStyles from '../../../common/commonStyles';
import Switch from '../../../components/Switch';
import DateTimeWheel from '../../../components/DateTimeWheel';
import { iphoneX } from '../../../utils/iphoneX-helper';

/**
 * Created by ocean on 2018/5/9
 */

const styles = StyleSheet.create({
  titleContainer: {
    paddingLeft: 15,
    paddingRight: 15,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    borderBottomWidth: 1 / PixelRatio.getPixelSizeForLayoutSize(1),
    borderBottomColor: '#e0e0e0',
  },
  title: {
    color: '#dc001b',
  },
  buttonContainer: {
    flexDirection: 'row',
    backgroundColor: '#E0E0E0',
    left: 0,
    right: 0,
    marginBottom: iphoneX ? 34 : 0,
  },
  cancelButton: {
    backgroundColor: '#efefef',
    borderRadius: 0,
    flex: 1,
    justifyContent: 'center',
  },
  completeButton: {
    backgroundColor: '#DC001B',
    borderRadius: 0,
    flex: 1,
    justifyContent: 'center',
  },
  overlay: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
  },
});

class SummaryAnchorForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isOn: false,
    };
  }

  onCancel = () => {
    if (this.props.onCancel) this.props.onCancel();
  };

  onSubmit = () => {
    const date = this.state.isOn ? this.datePicker.currentValue : 0;
    if (this.props.onSubmit) this.props.onSubmit(date);
  };

  datePicker: DateTimeWheel;

  render() {
    return (
      <View style={{ backgroundColor: '#fff' }}>
        <View style={styles.titleContainer}>
          <Text style={styles.title}>{this.props.title}</Text>
        </View>
        <InputGroup style={commonStyles.inputGroup}>
          <Item style={[commonStyles.inputItem, { borderBottomWidth: 0 }]}>
            <Label style={commonStyles.inputLabel}>是否下锚</Label>
            <Switch
              onPress={() => {
                this.setState({ isOn: !this.state.isOn });
              }}
              value={this.state.isOn}
            />
          </Item>
        </InputGroup>
        <View style={{ opacity: this.state.isOn ? 1 : 0.3 }}>
          <DateTimeWheel
            ref={(ref) => {
              this.datePicker = ref;
            }}
            value={this.props.value}
          />
          {!this.state.isOn && (
            <View style={styles.overlay} />
          )}
        </View>
        <View style={styles.buttonContainer}>
          <Button
            onPress={this.onCancel}
            style={styles.cancelButton}
          >
            <Text style={{ color: '#535353' }}>取消</Text>
          </Button>
          <Button
            onPress={this.onSubmit}
            style={styles.completeButton}
          >
            <Text>确定</Text>
          </Button>
        </View>
      </View>
    );
  }
}

SummaryAnchorForm.propTypes = {
  onCancel: PropTypes.func,
  onSubmit: PropTypes.func,
  title: PropTypes.string,
  value: PropTypes.number,
};

SummaryAnchorForm.defaultProps = {
  onCancel: undefined,
  onSubmit: undefined,
  title: '下锚时间',
  value: undefined,
};

export default SummaryAnchorForm;
