import {
  getShipDynShowRoutine,
  getReachReportRoutine,
  getBerthReportRoutine,
  getLeaveReportRoutine,
  updateDynRoutine,
  saveDynRoutine,
  changeDynTimeRoutine,
  toPortRoutine,
  getUnReadMesRoutine,
  returnShipStateRoutine,
} from './actions';
import { ShipOperation } from '../../common/Constant';

const initState = {
  data: {
    voyageCode: '',
    lineName: '',
    isSail: 0,
    timeLines: [],
    popId: 0,
    dynMemo: '',
    portName: '',
    lineType: '',
  },
  reachReport: {},
  berthReport: {},
  leaveReport: {},
  unReadMes:[]
};

export default function (state = initState, action) {
  switch (action.type) {
    // 获取首页信息
    case getShipDynShowRoutine.TRIGGER:
      return { ...state, loading: true };
    case getShipDynShowRoutine.SUCCESS: {
      const finish = action.payload.nextPortId === 0 && action.payload.state === ShipOperation.WANCHENG;
      return { ...state, data: finish ? initState.data : action.payload };
    }
    case getShipDynShowRoutine.FAILURE:
      return { ...state, ...(action.payload.code === 201 ? { data: initState.data } : {}) };
    case getShipDynShowRoutine.FULFILL:
      return { ...state, loading: false };

    // 获取抵港报信息
    case getReachReportRoutine.TRIGGER:
      return { ...state, loading: true };
    case getReachReportRoutine.SUCCESS:
      return { ...state, reachReport: action.payload };
    case getReachReportRoutine.FAILURE:
      return { ...state };
    case getReachReportRoutine.FULFILL:
      return { ...state, loading: false };

    // 获取靠泊报信息
    case getBerthReportRoutine.TRIGGER:
      return { ...state, loading: true };
    case getBerthReportRoutine.SUCCESS:
      return { ...state, berthReport: action.payload };
    case getBerthReportRoutine.FAILURE:
      return { ...state };
    case getBerthReportRoutine.FULFILL:
      return { ...state, loading: false };

    // 获取开航报信息
    case getLeaveReportRoutine.TRIGGER:
      return { ...state, loading: true };
    case getLeaveReportRoutine.SUCCESS:
      return { ...state, leaveReport: action.payload };
    case getLeaveReportRoutine.FAILURE:
      return { ...state };
    case getLeaveReportRoutine.FULFILL:
      return { ...state, loading: false };

    // 上传阶段数据
    case updateDynRoutine.TRIGGER:
      return { ...state, loading: true };
    case updateDynRoutine.SUCCESS: {
      const finish = action.payload.nextPortId === 0 && action.payload.state === ShipOperation.WANCHENG;
      return { ...state, data: finish ? initState.data : action.payload };
    }
    case updateDynRoutine.FAILURE:
      return { ...state, ...(action.payload.code === 201 ? { data: initState.data } : {}) };
    case updateDynRoutine.FULFILL:
      return { ...state, loading: false };

    // 暂存阶段数据
    case saveDynRoutine.TRIGGER:
      return { ...state, loading: true };
    case saveDynRoutine.SUCCESS: {
      return { ...state };
    }
    case saveDynRoutine.FAILURE:
      return { ...state, ...(action.payload.code === 201 ? { data: initState.data } : {}) };
    case saveDynRoutine.FULFILL:
      return { ...state, loading: false };

    // 修改阶段时间
    case changeDynTimeRoutine.TRIGGER:
      return { ...state, loading: true };
    case changeDynTimeRoutine.SUCCESS:
      return state;
    case changeDynTimeRoutine.FAILURE:
      return state;
    case changeDynTimeRoutine.FULFILL:
      return { ...state, loading: false };

    // 到港
    case toPortRoutine.TRIGGER:
      return { ...state, loading: true };
    case toPortRoutine.SUCCESS:
      return { ...state, data: action.payload };
    case toPortRoutine.FAILURE:
      return { ...state, ...(action.payload.code === 201 ? { data: initState.data } : {}) };
    case toPortRoutine.FULFILL:
      return { ...state, loading: false }; 
    
    //获取未读消息数量
    case getUnReadMesRoutine.TRIGGER:
      return { ...state, loading: false };
    case getUnReadMesRoutine.SUCCESS:
      return { ...state, unReadMes: action.payload.list };
    case getUnReadMesRoutine.FAILURE:
      return { ...state};
    case getUnReadMesRoutine.FULFILL:
      return { ...state, loading: false }; 

    //状态回退
    case returnShipStateRoutine.TRIGGER:
      return { ...state, loading: false };
    case returnShipStateRoutine.SUCCESS:
      return { ...state, };
    case getUnReadMesRoutine.FAILURE:
      return { ...state};
    case returnShipStateRoutine.FULFILL:
      return { ...state, loading: false }; 

    default:
      return state;
  }
}
