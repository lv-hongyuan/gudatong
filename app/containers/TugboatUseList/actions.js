import {
  GET_TUGBOAT_USE_LIST_ACTION,
  GET_TUGBOAT_USE_LIST_ACTION_RESULT,
  DELETE_TUGBOAT_USE_ACTION,
  DELETE_TUGBOAT_USE_ACTION_RESULT,
} from './constants';

export function getTugboatUseListAction() {
  return {
    type: GET_TUGBOAT_USE_LIST_ACTION,
  };
}

export function getTugboatUseListResultAction(data, success) {
  return {
    type: GET_TUGBOAT_USE_LIST_ACTION_RESULT,
    payload: {
      data,
      success,
    },
  };
}

export function deleteTugboatUseAction(data) {
  return {
    type: DELETE_TUGBOAT_USE_ACTION,
    payload: data,
  };
}

export function refreshTugboatUseListAction() {
  return {
    type: GET_TUGBOAT_USE_LIST_ACTION,
    noLoading: true,
  };
}

export function deleteTugboatUseResultAction(success) {
  return {
    type: DELETE_TUGBOAT_USE_ACTION_RESULT,
    payload: {
      success,
    },
  };
}
