import { createSelector } from 'reselect/es';

const selectTugboatUseListDomain = () => state => state.tugboatUseList;

const makeSelectGetTugboatUseListSuccess = () => createSelector(
  selectTugboatUseListDomain(),
  (subState) => {
    console.debug(subState.success);
    return subState.success;
  },
);

const makeSelectTugboatUseList = () => createSelector(
  selectTugboatUseListDomain(),
  (subState) => {
    console.debug(subState.success);
    return subState.data;
  },
);

export {
  makeSelectGetTugboatUseListSuccess,
  selectTugboatUseListDomain,
  makeSelectTugboatUseList,
};
