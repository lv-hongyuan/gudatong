import { call, put, takeLatest } from 'redux-saga/effects';
import { GET_TUGBOAT_USE_LIST_ACTION, DELETE_TUGBOAT_USE_ACTION } from './constants';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import { getTugboatUseListResultAction, deleteTugboatUseResultAction } from './actions';
import { errorMessage } from '../ErrorHandler/actions';
import { getPopId } from '../ShipWork/sagas';

function* getTugboatUseList(action) {
  console.log(action);
  try {
    const popId = yield call(getPopId);
    const response = yield call(request, ApiFactory.findTugUses(popId));
    console.log(response);
    yield put(getTugboatUseListResultAction(response, true));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(getTugboatUseListResultAction(undefined, false));
  }
}

export function* getTugboatUseListSaga() {
  yield takeLatest(GET_TUGBOAT_USE_LIST_ACTION, getTugboatUseList);
}

function* deleteTugboatUse(action) {
  console.log(action);
  try {
    const response = yield call(request, ApiFactory.deleteTugUses(action.payload));
    yield put(deleteTugboatUseResultAction(response, true));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(deleteTugboatUseResultAction(undefined, false));
  }
}

export function* deleteTugboatUseSaga() {
  yield takeLatest(DELETE_TUGBOAT_USE_ACTION, deleteTugboatUse);
}

// All sagas to be loaded
export default [
  getTugboatUseListSaga,
  deleteTugboatUseSaga,
];
