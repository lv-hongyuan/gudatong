import {
  GET_TUGBOAT_USE_LIST_ACTION_RESULT,
  DELETE_TUGBOAT_USE_ACTION_RESULT,
  DELETE_TUGBOAT_USE_ACTION,
  GET_TUGBOAT_USE_LIST_ACTION,
} from './constants';

const initState = {
  success: false,
  data: [],
};

export default function (state = initState, action) {
  switch (action.type) {
    case DELETE_TUGBOAT_USE_ACTION:
    case GET_TUGBOAT_USE_LIST_ACTION:
      return { ...state, success: false };
    case GET_TUGBOAT_USE_LIST_ACTION_RESULT:
      return {
        ...state,
        success: action.payload.success,
        data: action.payload.data ? action.payload.data.dtoList : [],
      };
    case DELETE_TUGBOAT_USE_ACTION_RESULT:
      return { ...state, success: action.payload.success };
    default:
      return state;
  }
}
