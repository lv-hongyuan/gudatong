import React from 'react';
import { BackHandler, View, StyleSheet } from 'react-native';
import hoistNonReactStatic from 'hoist-non-react-statics';
import Orientation from 'react-native-orientation';
import NavigatorService from '../NavigatorService';
import Loading from '../components/Loading';

const styles = StyleSheet.create({
  screen: {
    flex: 1,
  },
});

export const Orientations = {
  PORTRAIT: 'portrait',
  LANDSCAPE: 'landscape',
  ALL: 'all',
};

function getDisplayName(WrappedComponent) {
  return WrappedComponent.displayName || WrappedComponent.name || 'Component';
}

/*
 * 配合react-navigation使用
 * 定义了屏幕显示的生命周期，以及Android的返回按钮事件
 */
function screenHOC(WrappedComponent: React.Component) {
  class ScreenComponent extends React.PureComponent {
    componentDidMount() {
      if (super.componentDidMount) super.componentDidMount();
      this.subs = [
        this.props.navigation.addListener('willFocus', this.willFocus),
        this.props.navigation.addListener('didFocus', this.didFocus),
        this.props.navigation.addListener('willBlur', this.willBlur),
        this.props.navigation.addListener('didBlur', this.didBlur),
      ];
    }

    componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.onBackPressed);
      if (super.componentWillUnmount) super.componentWillUnmount();
      this.subs.forEach(listener => listener.remove());
    }

    // android点击返回按钮
    onBackPressed = () => {
      if (this.wrapped.onBackPressed) {
        return this.wrapped.onBackPressed();
      }
      NavigatorService.back();
      return true;
    };

    // 屏幕将要显示
    willFocus = () => {
      console.debug('willFocus', ScreenComponent.displayName);
      switch (this.wrapped.supportedOrientations) {
        case Orientations.PORTRAIT:
          Orientation.lockToPortrait();
          console.log('lockToPortrait')
          break;
        case Orientations.LANDSCAPE:
          Orientation.lockToLandscape();
          console.log('lockToLandscape')
          break;
        case Orientations.ALL:
          Orientation.unlockAllOrientations();
          console.log('unlockAllOrientations')
          break;
        default:
          Orientation.lockToPortrait();
          console.log('lockToPortrait')
          break;
      }
      // if (this.wrapped.willFocus) this.wrapped.willFocus();
    };

    // 屏幕显示
    didFocus = () => {
      console.debug('didFocus', ScreenComponent.displayName);
      BackHandler.addEventListener('hardwareBackPress', this.onBackPressed);
      if (this.wrapped.didFocus) this.wrapped.didFocus();
    };

    // 屏幕将要隐藏
    willBlur = () => {
      console.debug('willBlur', ScreenComponent.displayName);
      BackHandler.removeEventListener('hardwareBackPress', this.onBackPressed);
      if (this.wrapped.willBlur) this.wrapped.willBlur();
      // Orientation.lockToPortrait();
    };

    // 屏幕隐藏
    didBlur = () => {
      console.debug('didBlur', ScreenComponent.displayName);
      if (this.wrapped.didBlur) this.wrapped.didBlur();
    };

    render() {
      const { ref, ...rest } = this.props;
      const selfRef = (o) => {
        if (ref) { ref(o); }
        this.wrapped = o;
      };
      return (
          <View style={styles.screen}>
            <WrappedComponent
                ref={selfRef}
                {...rest}
            />
            {!!(this.props.isLoading) && <Loading />}
          </View>
      );
    }
  }

  hoistNonReactStatic(ScreenComponent, WrappedComponent);

  ScreenComponent.displayName = `Screen(${getDisplayName(WrappedComponent)})`;

  return ScreenComponent;
}

export default screenHOC;
