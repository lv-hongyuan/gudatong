import React from 'react';
import PropTypes from 'prop-types';
import { FlatList, RefreshControl } from 'react-native';
import { connect } from 'react-redux';
import { Container, View } from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import HeaderButtons from 'react-navigation-header-buttons';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { ROUTE_EDIT_DANGEROUS_GOODS_RECORD } from '../../RouteConstant';
import { makeSelectIsSail } from '../ShipWork/selectors';
import { getDangerousGoodsRecordsPromise, deleteDangerousGoodsRecordPromise } from './actions';
import myTheme from '../../Themes';
import { makeDangerousGoodsRecords } from './selectors';
import EmptyView from '../../components/EmptyView';
import screenHOC from '../screenHOC';
import DangerousGoodsItem from '../../components/DangerousGoodsItem';

/**
 * 危险品记录页面
 * Created by jianzhexu on 2018/3/29
 */
@screenHOC
class DangerousGoodsRecords extends React.PureComponent {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.getParam('title', '危险品记录'),
    headerRight: (
      <HeaderButtons>
        <HeaderButtons.Item
          title="创建"
          buttonStyle={{ fontSize: 14, color: '#ffffff' }}
          onPress={navigation.getParam('onBtnCreate')}
        />
      </HeaderButtons>
    ),
  });
  static propTypes = {
    navigation: PropTypes.object.isRequired,
    getDangerousGoodsRecords: PropTypes.func.isRequired,
    deleteDangerousGoodsRecord: PropTypes.func.isRequired,
    data: PropTypes.array.isRequired,
    isSail: PropTypes.bool.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
    };
    this.props.navigation.setParams({ onBtnCreate: this.onBtnCreate.bind(this) });
  }

  componentDidMount() {
    this.reLoadingPage();
  }

  onBtnCreate = () => {
    this.props.navigation.navigate(ROUTE_EDIT_DANGEROUS_GOODS_RECORD, {
      title: '创建危险品记录',
      onGoBack: () => {
        this.reLoadingPage();
      },
      isEdit: false,
      isVoyage: this.props.isSail,
    });
  };

  reLoadingPage = () => {
    requestAnimationFrame(() => {
      this.props.getDangerousGoodsRecords()
        .then(() => {
          this.setState({ refreshing: false });
        })
        .catch(() => {
          this.setState({ refreshing: false });
        });
    });
  };

  itemPress = (data) => {
    this.props.navigation.navigate(ROUTE_EDIT_DANGEROUS_GOODS_RECORD, {
      title: '编辑危险品记录',
      onGoBack: () => {
        this.reLoadingPage();
      },
      isEdit: true,
      data,
      isVoyage: this.props.isSail,
    });
  };

  deleteItem = (id) => {
    this.props.deleteDangerousGoodsRecord(id)
      .then(() => {
        this.reLoadingPage();
      })
      .catch(() => {
      });
  };

  renderItem = ({ item }) => (
    <DangerousGoodsItem
      id={item.id}
      popId={item.popId}
      contNumber={item.contNumber}
      unloadingPort={item.unloadingPort}
      shipLoadPosition={item.shipLoadPosition}
      imoUnNo={item.imoUnNo}
      contType={item.contType}
      onDelete={this.deleteItem}
      onItemPress={this.itemPress}
    />
  );

  renderEmptyView = () => (<EmptyView />);

  render() {
    return (
      <Container theme={myTheme}>
        <FlatList
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => {
                this.setState({ refreshing: true });
                this.reLoadingPage();
              }}
            />
          }
          contentInsetAdjustmentBehavior="scrollableAxes"
          style={{ flex: 1, backgroundColor: '#e0e0e0' }}
          keyExtractor={item => `${item.id}`}
          data={this.props.data}
          ListEmptyComponent={this.renderEmptyView}
          initialNumToRender={10}
          renderItem={this.renderItem}
          ItemSeparatorComponent={() => (<View style={{ height: 5 }} />)}
          ListFooterComponent={() => (<View style={{ height: 5 }} />)}
          ListHeaderComponent={() => (<View style={{ height: 5 }} />)}
        />
      </Container>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  data: makeDangerousGoodsRecords(),
  isSail: makeSelectIsSail(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getDangerousGoodsRecords: getDangerousGoodsRecordsPromise,
      deleteDangerousGoodsRecord: deleteDangerousGoodsRecordPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(DangerousGoodsRecords);
