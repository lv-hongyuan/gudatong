import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  GET_DANGEROUS_GOODS_RECORDS,
  DELETE_DANGEROUS_GOODS_RECORD,
} from './constants';

export const getDangerousGoodsRecordsRoutine = createRoutine(
  GET_DANGEROUS_GOODS_RECORDS,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const getDangerousGoodsRecordsPromise = promisifyRoutine(getDangerousGoodsRecordsRoutine);

export const deleteDangerousGoodsRecordRoutine = createRoutine(
  DELETE_DANGEROUS_GOODS_RECORD,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const deleteDangerousGoodsRecordPromise = promisifyRoutine(deleteDangerousGoodsRecordRoutine);
