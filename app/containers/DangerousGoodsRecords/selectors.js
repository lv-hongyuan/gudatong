import { createSelector } from 'reselect/es';

const selectDangerousGoodsRecordsDomain = () => state => state.dangerousGoodsRecords;

const makeDangerousGoodsRecords = () => createSelector(
  selectDangerousGoodsRecordsDomain(),
  (subState) => {
    console.debug(subState.data);
    return subState.data;
  },
);

export {
  makeDangerousGoodsRecords,
};
