/*
 *
 * DangerousGoodsRecords constants
 *
 */
export const GET_DANGEROUS_GOODS_RECORDS = 'app/DangerousGoodsRecords/GET_DANGEROUS_GOODS_RECORDS';

export const DELETE_DANGEROUS_GOODS_RECORD = 'app/DangerousGoodsRecords/DELETE_DANGEROUS_GOODS_RECORD';
