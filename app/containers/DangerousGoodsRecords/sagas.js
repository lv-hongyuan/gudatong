import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import { getDangerousGoodsRecordsRoutine, deleteDangerousGoodsRecordRoutine } from './actions';
import { errorMessage } from '../ErrorHandler/actions';
import { getPopId } from '../ShipWork/sagas';

function* getDangerousGoodsRecords(action) {
  console.log(action);
  try {
    yield put(getDangerousGoodsRecordsRoutine.request());
    const popId = yield call(getPopId);
    const response = yield call(
      request,
      ApiFactory.getDangerousGoodsByPop(popId),
    );
    console.log('getDangerousGoodsRecords', response.toString());
    yield put(getDangerousGoodsRecordsRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(getDangerousGoodsRecordsRoutine.failure(e));
  } finally {
    yield put(getDangerousGoodsRecordsRoutine.fulfill());
  }
}

export function* getDangerousGoodsRecordsSaga() {
  yield takeLatest(getDangerousGoodsRecordsRoutine.TRIGGER, getDangerousGoodsRecords);
}

function* deleteDangerousGoodsRecord(action) {
  console.log(action);
  try {
    yield put(deleteDangerousGoodsRecordRoutine.request());
    const id = action.payload;
    const response = yield call(
      request,
      ApiFactory.deleteDangerousGoods(id),
    );
    console.log('deleteDangerousGoodsRecord', response.toString());
    yield put(deleteDangerousGoodsRecordRoutine.success(id));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(deleteDangerousGoodsRecordRoutine.failure(e));
  } finally {
    yield put(deleteDangerousGoodsRecordRoutine.fulfill());
  }
}

export function* deleteDangerousGoodsRecordSaga() {
  yield takeLatest(deleteDangerousGoodsRecordRoutine.TRIGGER, deleteDangerousGoodsRecord);
}

// All sagas to be loaded
export default [
  getDangerousGoodsRecordsSaga,
  deleteDangerousGoodsRecordSaga,
];
