import {
  getDangerousGoodsRecordsRoutine,
  deleteDangerousGoodsRecordRoutine,
} from './actions';

const initState = {
  data: [],
};

export default function (state = initState, action) {
  switch (action.type) {
    case getDangerousGoodsRecordsRoutine.TRIGGER:
      return { ...state, loading: true };
    case getDangerousGoodsRecordsRoutine.SUCCESS:
      return { ...state, data: action.payload.dtoList };
    case getDangerousGoodsRecordsRoutine.FAILURE:
      return { ...state, error: action.payload };
    case getDangerousGoodsRecordsRoutine.FULFILL:
      return { ...state, loading: false };

    case deleteDangerousGoodsRecordRoutine.TRIGGER:
      return { ...state, loading: true };
    case deleteDangerousGoodsRecordRoutine.SUCCESS: {
      const id = action.payload;
      const list = state.data || [];
      const index = list.findIndex(item => item.id === id);
      if (index !== -1) {
        list.splice(index, 1);
      }
      return { ...state, data: list };
    }
    case deleteDangerousGoodsRecordRoutine.FAILURE:
      return { ...state, error: action.payload };
    case deleteDangerousGoodsRecordRoutine.FULFILL:
      return { ...state, loading: false };

    default:
      return state;
  }
}
