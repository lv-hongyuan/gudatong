import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_ARRIVEPORT_RECORD_LIST } from './constants';

export const getArrivePortRecordListRoutine = createRoutine(GET_ARRIVEPORT_RECORD_LIST);
export const getArrivePortRecordListPromise = promisifyRoutine(getArrivePortRecordListRoutine);