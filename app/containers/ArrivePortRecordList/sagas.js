import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../ErrorHandler/actions';
import { getArrivePortRecordListRoutine } from './actions';

function* ArrivePortRecordList(action) {
    console.log('请求开始++',action);
    const { page, pageSize } = (action.payload || {});

    try {
        yield put(getArrivePortRecordListRoutine.request());
        const param = {
            filter: {
                logic: 'and',
                filters: [],
            },
            page,
            pageSize,
        };
        console.log('param:', param)
        const response = yield call(request, ApiFactory.getArrivalConfirmOperationList(param));
        console.log('列表数据：', response);
        yield put(getArrivePortRecordListRoutine.success({
            page, 
            pageSize,
            list:(response.dtoList ? response.dtoList.content : []),
        }));
    } catch (e) {
        console.log(e.message);
        yield put(errorMessage(e.message));
        yield put(getArrivePortRecordListRoutine.failure({ page, pageSize, e }));
    } finally {
        yield put(getArrivePortRecordListRoutine.fulfill());
    }
}

export function* getArrivePortRecordListSaga() {
    yield takeLatest(getArrivePortRecordListRoutine.TRIGGER, ArrivePortRecordList);
}

// All sagas to be loaded
export default [getArrivePortRecordListSaga];
