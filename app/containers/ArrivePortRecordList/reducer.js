import { getArrivePortRecordListRoutine } from './actions';
import fixIdList from '../../utils/fixIdList';
import { RefreshState } from '../../components/RefreshListView';
import _ from "lodash";

const initState = {
  loading: false,
  list: [],
  refreshState: RefreshState.Idle,
};

export default function (state = initState, action) {
  switch (action.type) {
    case getArrivePortRecordListRoutine.TRIGGER:{
      console.log('11111',action.payload)
      const { page } = action.payload;
      return { 
        ...state, 
        loading: true, 
        refreshState: page > 1 ? RefreshState.FooterRefreshing : RefreshState.HeaderRefreshing,
      }
    }
    case getArrivePortRecordListRoutine.SUCCESS:{
      const { page, pageSize, list } = action.payload;
      return { 
        ...state, 
        // list: fixIdList(action.payload.list),
        list: page == 1 ? list : state.list.concat(list),
        refreshState: list.length < pageSize ? RefreshState.NoMoreData : RefreshState.Idle
      }
    }
    case getArrivePortRecordListRoutine.FAILURE:{
      const { page, error } = action.payload;
      return {
        ...state, 
        list: page === 1 ? [] : state.list,
        error, 
        refreshState: RefreshState.Failure,
      };
    }
    case getArrivePortRecordListRoutine.FULFILL:{
      return { ...state, loading: false };
    }
    default:
      return state;
  }
}
