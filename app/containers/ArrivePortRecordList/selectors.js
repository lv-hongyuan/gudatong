import { createSelector } from 'reselect/es';

const arrivePortRecordList = () => state => state.arrivePortRecordList;

const makeList = () => createSelector(arrivePortRecordList(), (subState) => {
  console.debug(subState);
  return subState.list;
});

const makeRefreshState = () => createSelector(arrivePortRecordList(), (subState) => {
  console.debug(subState.refreshState);
  return subState.refreshState;
});


export { makeList, makeRefreshState};