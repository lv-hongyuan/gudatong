import React from 'react';
import PropTypes from 'prop-types';
import { Text, StyleSheet,SafeAreaView } from 'react-native';
import { createStructuredSelector } from 'reselect/es';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { makeList, makeRefreshState } from './selectors';
import { getArrivePortRecordListPromise } from './actions';
import { connect } from 'react-redux';
import ArrivePortRecordItem from './components/ArrivePortItem';
import RefreshListView from '../../components/RefreshListView';
import { Container, View, } from 'native-base';
import myTheme from '../../Themes';
import screenHOC from '../screenHOC';
import _ from "lodash";
import HeaderButtons from "react-navigation-header-buttons";
import Svg from "../../components/Svg";
import { NavigationActions } from "react-navigation";
import { ROUTE_ARRIVEPORT_RECORD_DETAIL} from '../../RouteConstant';
const styles = StyleSheet.create({
    title: {
        height: 35,
        width: '100%',
        backgroundColor: '#fff',
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 0.5,
        borderBottomColor: '#eee',
    },
    titletext: {
        borderRightColor: '#eee',
        borderRightWidth: 1,
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    }
})

const firstPageNum = 1;
const DefaultPageSize = 10;

@screenHOC
class ArrivePortRecordList extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            pageSize: DefaultPageSize,  //展示行数
            page: firstPageNum,
        };
    }

    componentDidMount() {
        this.listView.beginRefresh();
    }

    loadList(loadMore = false) {
        const { page, pageSize } = this.state;
        this.props.getArrivePortRecordListPromise({ 
            page: loadMore ? page : 1, 
            pageSize ,
        })
            .then(({ page }) => {
                this.setState({ page: page + 1 });
            })
            .catch(() => {
            });
    }

    // 上拉加载
    onHeaderRefresh = () => {
        this.loadList(false);
    };

    // 下拉刷新
    onFooterRefresh = () => {
        this.loadList(true)
    }

    renderItem = ({ item }) => {
        return (
            <ArrivePortRecordItem
                item={item}
                onSelect={this.onSelect}
            />
        )
    };

    onSelect = (item) => {
        console.log(item);
        let num = item.opTimeStr.replace(/[^0-9]/ig,"");
        this.props.navigation.navigate(
            ROUTE_ARRIVEPORT_RECORD_DETAIL,
            {
                id: item.id,
                title:num
            },
        );
    };

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={styles.title}>
                    <View style={[styles.titletext, { width: '20%' }]}><Text style={{color: myTheme.inputColor,}}>航次</Text></View>
                    <View style={[styles.titletext, { width: '40%' }]}><Text style={{color: myTheme.inputColor,}}>航线</Text></View>
                    <View style={[styles.titletext, { width: '40%', borderRightWidth: 0 }]}><Text style={{color: myTheme.inputColor}}>提交时间</Text></View>
                </View>
                <RefreshListView
                    ref={(ref) => { this.listView = ref; }}
                    data={this.props.list || []}
                    keyExtractor={item => `${item.id}`}
                    renderItem={this.renderItem}
                    refreshState={this.props.refreshState}
                    onHeaderRefresh={this.onHeaderRefresh}
                    onFooterRefresh={this.onFooterRefresh}
                />
            </SafeAreaView>
        );
    }
}


ArrivePortRecordList.navigationOptions = ({ navigation }) => ({
    title: '历史确报',
    headerLeft: (
        <HeaderButtons>
            <HeaderButtons.Item
                title=""
                buttonWrapperStyle={{ padding: 10 }}
                ButtonElement={<Svg icon="icon_back" size="20" color="white" />}
                onPress={() => {
                    navigation.dispatch(NavigationActions.back());
                }}
            />
        </HeaderButtons>
    ),
});

ArrivePortRecordList.propTypes = {
    navigation: PropTypes.object.isRequired,
    getArrivePortRecordListPromise: PropTypes.func.isRequired,
    list: PropTypes.array,
    refreshState: PropTypes.number.isRequired,
};
ArrivePortRecordList.defaultProps = {
    list: [],
};

const mapStateToProps = createStructuredSelector({
    refreshState: makeRefreshState(),
    list: makeList(),
});

function mapDispatchToProps(dispatch) {
    return {
        ...bindPromiseCreators({
            getArrivePortRecordListPromise,
        }, dispatch),
        dispatch,
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ArrivePortRecordList);
