import {
  createTugboatApplyRoutine,
  updateTugboatApplyRoutine,
} from './actions';

const initState = {
  data: undefined,
};

export default function (state = initState, action) {
  switch (action.type) {
    case createTugboatApplyRoutine.TRIGGER:
      return { ...state, loading: true };
    case createTugboatApplyRoutine.SUCCESS:
      return { ...state, data: action.payload };
    case createTugboatApplyRoutine.FAILURE:
      return { ...state, error: action.payload };
    case createTugboatApplyRoutine.FULFILL:
      return { ...state, loading: false };

    case updateTugboatApplyRoutine.TRIGGER:
      return { ...state, loading: true };
    case updateTugboatApplyRoutine.SUCCESS:
      return { ...state, data: action.payload };
    case updateTugboatApplyRoutine.FAILURE:
      return { ...state, error: action.payload };
    case updateTugboatApplyRoutine.FULFILL:
      return { ...state, loading: false };

    default:
      return state;
  }
}
