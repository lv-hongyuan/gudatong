import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  CREATE_TUGBOAT_APPLY_ACTION,
  UPDATE_TUGBOAT_APPLY_ACTION,
} from './constants';

export const createTugboatApplyRoutine = createRoutine(
  CREATE_TUGBOAT_APPLY_ACTION,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const createTugboatApplyPromise = promisifyRoutine(createTugboatApplyRoutine);

export const updateTugboatApplyRoutine = createRoutine(
  UPDATE_TUGBOAT_APPLY_ACTION,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const updateTugboatApplyPromise = promisifyRoutine(updateTugboatApplyRoutine);

export const UseWays = [
  { name: '靠泊', value: 10 },
  { name: '离泊', value: 20 },
  { name: '移泊', value: 30 },
];

export const ApplyTypes = [
  { name: '大风影响', value: 1 },
  { name: '港口强制', value: 2 },
  { name: '潮水因素', value: 3 },
  { name: '码头条件', value: 4 },
];
