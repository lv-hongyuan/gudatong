/*
 *
 * Login constants
 *
 */
export const CREATE_TUGBOAT_APPLY_ACTION = 'app/CreateTugboatApply/CREATE_TUGBOAT_APPLY_ACTION';

export const UPDATE_TUGBOAT_APPLY_ACTION = 'app/CreateTugboatApply/UPDATE_TUGBOAT_APPLY_ACTION';

export const OpType = {
  APPLY: '申请',
  USE: '使用',
};
