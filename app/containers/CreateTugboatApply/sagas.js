import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import {
  createTugboatApplyRoutine,
  updateTugboatApplyRoutine,
} from './actions';
import { errorMessage } from '../ErrorHandler/actions';

function* createTugboatApply(action) {
  console.log(action);
  try {
    yield put(createTugboatApplyRoutine.request());
    const response = yield call(request, ApiFactory.createApplyTug(action.payload));
    console.log('createTugboatApply', response.toString());
    yield put(createTugboatApplyRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(createTugboatApplyRoutine.failure(e));
  } finally {
    yield put(createTugboatApplyRoutine.fulfill());
  }
}

export function* createTugboatApplySaga() {
  yield takeLatest(createTugboatApplyRoutine.TRIGGER, createTugboatApply);
}

function* updateTugboatApply(action) {
  console.log(action);
  try {
    yield put(updateTugboatApplyRoutine.request());
    const response = yield call(request, ApiFactory.updateApplyTug(action.payload));
    console.log('updateTugboatApply', response.toString());
    yield put(updateTugboatApplyRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e.message));
    yield put(updateTugboatApplyRoutine.failure(e));
  } finally {
    yield put(updateTugboatApplyRoutine.fulfill());
  }
}

export function* updateTugboatApplySaga() {
  yield takeLatest(updateTugboatApplyRoutine.TRIGGER, updateTugboatApply);
}

// All sagas to be loaded
export default [
  createTugboatApplySaga,
  updateTugboatApplySaga,
];
