import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, SafeAreaView } from 'react-native';
import { connect } from 'react-redux';
import { Container, Content, Item, Label, InputGroup, View, Button, Text } from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import HeaderButtons from 'react-navigation-header-buttons';
import AlertView from '../../components/Alert';
import Selector from '../../components/Selector';
import { OpType } from './constants';
import { makeSelectPortName, makeSelectPopId } from './selectors';
import { UseWays, createTugboatApplyPromise, updateTugboatApplyPromise, ApplyTypes } from './actions';
import commonStyles from '../../common/commonStyles';
import InputItem from '../../components/InputItem';
import screenHOC from '../screenHOC';
import { MaskType } from '../../components/InputItem/TextInput';

const styles = StyleSheet.create({
  buttonContainer: {
    flexDirection: 'row',
    backgroundColor: '#E0E0E0',
    height: 50,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    borderBottomColor: 'transparent',
  },
  label: {},
  text: {
    flex: 2,
  },
  container: {
    backgroundColor: '#FFFFFF',
    paddingLeft: 20,
    paddingTop: 10,
    paddingBottom: 10,
    paddingRight: 20,
  },
  applyButton: {
    height: '100%',
    backgroundColor: '#FBB03B',
    borderRadius: 0,
    flex: 1,
    justifyContent: 'center',
  },
  useButton: {
    height: '100%',
    backgroundColor: '#DC001B',
    borderRadius: 0,
    flex: 1,
    justifyContent: 'center',
  },
});

/**
 * 创建编辑拖轮申请
 * Created by jianzhexu on 2018/4/3
 */
@screenHOC
class CreateTugboatApply extends React.PureComponent {
  static navigationOptions = ({ navigation }) => {
    const type = navigation.getParam('type', undefined);
    return {
      title: navigation.getParam('title', '编辑拖轮申请'),
      headerRight: (
        <HeaderButtons>
          {type ? (
            <HeaderButtons.Item
              title="保存"
              buttonStyle={{ fontSize: 14, color: '#ffffff' }}
              onPress={() => {
                const handleSubmitFunc = navigation.getParam('submit', undefined);
                if (handleSubmitFunc) {
                  requestAnimationFrame(() => {
                    handleSubmitFunc(type);
                  });
                }
              }}
            />
          ) : null}
        </HeaderButtons>
      ),
    };
  };

  static propTypes = {
    navigation: PropTypes.object.isRequired,
    createTugboatApplyPromise: PropTypes.func.isRequired,
    updateTugboatApplyPromise: PropTypes.func.isRequired,
    popId: PropTypes.number.isRequired,
    portName: PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);

    const data = props.navigation.getParam('data', {});
    this.state = {
      applyType: 1,
      tugNum: undefined,
      useWay: 10,
      applyReasons: undefined,
      portName: undefined,
      id: undefined,
      popId: undefined,
      ...data,
    };
  }

  componentDidMount() {
    this.props.navigation.setParams({ submit: this.handleSubmit });
  }

  handleSubmit = (opType = OpType.APPLY) => {
    const type = this.props.navigation.getParam('type', undefined);
    const { popId } = this.props;
    const {
      useWay, tugNum, applyType, applyReasons, id,
    } = this.state;
    if (!tugNum || tugNum === '0' || tugNum === '') {
      AlertView.show({ message: '请填写拖轮数量' });
      return;
    }
    if (type) {
      this.props.updateTugboatApplyPromise({
        popId: this.state.popId,
        useWay,
        tugNum,
        applyType,
        applyReasons,
        id,
        opType,
      }).then(() => {
        const goBackCallback = this.props.navigation.getParam('onGoBack', undefined);
        if (goBackCallback) goBackCallback();
        this.props.navigation.goBack();
      }).catch(() => {});
    } else {
      this.props.createTugboatApplyPromise({
        popId,
        useWay,
        tugNum,
        applyType,
        applyReasons,
        opType,
      }).then(() => {
        const goBackCallback = this.props.navigation.getParam('onGoBack', undefined);
        if (goBackCallback) goBackCallback();
        this.props.navigation.goBack();
      }).catch(() => {});
    }
  };

  render() {
    const type = this.props.navigation.getParam('type', undefined);
    return (
      <Container>
        <Content>
          <View style={styles.container}>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="发生港口:"
                value={this.state.portName ? this.state.portName : this.props.portName}
                placeholder=""
                editable={false}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>使用途径:</Label>
                <Selector
                  value={this.state.useWay}
                  items={UseWays}
                  getItemText={item => item.name}
                  getItemValue={item => item.value}
                  onSelected={(item) => {
                    this.setState({
                      useWay: item.value,
                    });
                  }}
                />
              </Item>
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="拖轮数量:"
                value={this.state.tugNum}
                editable={!this.state.readonly}
                onChangeText={(text) => {
                  this.setState({ tugNum: text });
                }}
                placeholder="0"
                keyboardType="numeric"
                maskType={MaskType.INTEGER}
                rightItem={<Text style={commonStyles.tail}>艘</Text>}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>原因类型:</Label>
                <Selector
                  value={this.state.applyType}
                  items={ApplyTypes}
                  getItemText={item => item.name}
                  getItemValue={item => item.value}
                  onSelected={(item) => {
                    this.setState({
                      applyType: item.value,
                    });
                  }}
                />
              </Item>
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="具体原因:"
                value={this.state.applyReasons}
                editable={!this.state.readonly}
                onChangeText={(text) => {
                  this.setState({ applyReasons: text.trim() });
                }}
              />
            </InputGroup>
          </View>
        </Content>
        {type ? null : (
          <SafeAreaView style={{
            position: 'absolute',
            bottom: 0,
            left: 0,
            right: 0,
            backgroundColor: '#ffffff',
          }}
          >
            <View style={styles.buttonContainer}>
              <Button
                onPress={() => {
                  this.handleSubmit(OpType.APPLY);
                }}
                style={styles.applyButton}
              >
                <Text>拖轮申请</Text>
              </Button>
              <Button
                onPress={() => {
                  this.handleSubmit(OpType.USE);
                }}
                style={styles.useButton}
              >
                <Text>拖轮使用</Text>
              </Button>
            </View>
          </SafeAreaView>
        )}
      </Container>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  portName: makeSelectPortName(),
  popId: makeSelectPopId(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      createTugboatApplyPromise,
      updateTugboatApplyPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateTugboatApply);
