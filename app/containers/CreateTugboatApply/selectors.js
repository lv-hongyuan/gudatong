import { createSelector } from 'reselect/es';

const selectShipStateDomain = () => state => state.shipWork.data;

const makeSelectPortName = () => createSelector(
  selectShipStateDomain(),
  subState => subState.portName,
);

const makeSelectPopId = () => createSelector(
  selectShipStateDomain(),
  (subState) => {
    console.debug(subState.success);
    return subState.popId;
  },
);
export {
  makeSelectPortName,
  makeSelectPopId,
};
