import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../ErrorHandler/actions';
import { getArrivePortRecordDetailRoutine } from './actions';

function* ArrivePortRecordDetail(action) {
    console.log(action);
    const { id } = (action.payload || {});
    try {
        yield put(getArrivePortRecordDetailRoutine.request());
        const param = {id};
        console.log('param:', param)
        const response = yield call(request, ApiFactory.getArrivalConfirmOperationDetail(param));
        console.log('列表数据：', response);
        yield put(getArrivePortRecordDetailRoutine.success({
            data: (response || {})
        }));
    } catch (e) {
        console.log(e.message);
        yield put(errorMessage(e.message));
        yield put(getArrivePortRecordDetailRoutine.failure(e));
    } finally {
        yield put(getArrivePortRecordDetailRoutine.fulfill());
    }
}

export function* getArrivePortRecordDetailSaga() {
    yield takeLatest(getArrivePortRecordDetailRoutine.TRIGGER, ArrivePortRecordDetail);
}

// All sagas to be loaded
export default [getArrivePortRecordDetailSaga];
