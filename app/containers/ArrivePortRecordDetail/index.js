import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, TouchableOpacity, StatusBar, Platform, Clipboard } from 'react-native';
import { connect } from 'react-redux';
import { Container, View, Text, } from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import _ from 'lodash';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { getArrivePortRecordDetailPromise } from './actions';
import { makeList } from './selectors';
import screenHOC from '../screenHOC';
import Svg from "../../components/Svg";
import { NavigationActions } from "react-navigation";
import myTheme from '../../Themes';
import { iphoneX } from '../../utils/iphoneX-helper';
import AlertView from '../../components/Alert';
const IS_ANDROID = Platform.OS === 'ios' ? false : true;
const styles = StyleSheet.create({
    Line: {
        height: 30,
        justifyContent: 'center',
        paddingLeft: 5,
    },
    bottomTip: {
        textAlign: 'center',
        padding: 8,
        color: '#969696',
    },
});

@screenHOC
class ArrivePortRecordDetai extends React.PureComponent {
    constructor(props) {
        super(props);
        this.parmas = this.props.navigation.state.params
        this.state = {
            shipName: '',
            voyageCode: '',                  //航次号
            line: '',                        //航线
            arrivalPosition: '暂无数据',             //抵港位置
            arrivePortTime: '暂无数据',              //抵港时间
            toNextPortDistance: '0',          //剩余航程
            foreDraft: '0',                   //艏吃水
            midshipDraft: '0',                //舯吃水
            aftDraft: '0',                    //艉吃水
            reason:''                         //修改原因
        };
    }

    getData() {
        this.props.getArrivePortRecordDetailPromise({ id: this.parmas.id })
            .then((data) => {
                let dataobj = data.data
                console.log(dataobj);
                this.setState({
                    shipName: dataobj.shipName,
                    voyageCode: dataobj.voyageCode,
                    line: dataobj.lineName,
                    arrivalPosition: dataobj.exactLocation,             //抵港位置
                    arrivePortTime: dataobj.nextPortEtaStr,              //抵港时间
                    foreDraft: dataobj.draughtFrontB,                   //抵港艏吃水
                    midshipDraft: dataobj.draughtMiddleB,                //抵港舯吃水
                    aftDraft: dataobj.draughtAfterB,                    //抵港艉吃水
                    toNextPortDistance: dataobj.toNextPortDistance,          //剩余航程
                    reason:dataobj.reason
                })
            })
            .catch(() => {
            });
    }
    componentDidMount() {
        this.getData()
    }
    copyDetailStr() {
        let detailstr =
            `船名：${this.state.shipName}，
航次：${this.state.voyageCode}，
航线：${this.state.line}，
抵港位置：${this.state.arrivalPosition}，
抵港时间：${this.state.arrivePortTime}，
剩余航程：${this.state.toNextPortDistance} 海里，
抵港艏吃水：${this.state.foreDraft} 米，
抵港舯吃水：${this.state.midshipDraft} 米，
抵港艉吃水：${this.state.aftDraft} 米，
修改原因：${this.state.reason}`
        console.log(detailstr);
        Clipboard.setString(detailstr);
    }
    render() {

        return (
            <Container theme={myTheme} style={{ backgroundColor: '#fff' }}>
                <StatusBar
                    hidden={false}
                    barStyle={'light-content'}
                    style={{ backgroundColor: '#DC001B' }}
                />
                <View style={{
                    backgroundColor: '#DC001B',
                    height: iphoneX ? 88 : IS_ANDROID ? 56 : 64,
                    flexDirection: 'row',
                    alignItems: 'center',
                    paddingHorizontal: 10,
                    paddingTop: iphoneX ? 44 : IS_ANDROID ? 0 : 20,
                    marginTop: 0,
                    justifyContent: 'space-between'
                }}>
                    <TouchableOpacity
                        onPress={() => { this.props.navigation.dispatch(NavigationActions.back()) }}
                        style={{
                            width: 40,
                            height: 40,
                            justifyContent: 'center'
                        }}>
                        <Svg icon="icon_back" size="20" color="white" />
                    </TouchableOpacity>
                    <View>
                        <Text style={{ color: '#fff', fontSize: 18, fontWeight: '700' }}>{this.parmas.title}</Text>
                    </View>
                    <TouchableOpacity
                        onPress={() => {
                            this.copyDetailStr()
                            AlertView.show({
                                message: '复制成功，可以直接粘贴'
                            });
                        }}
                        style={{
                            height: 30,
                            justifyContent: 'center'
                        }}>
                        <Text style={{ color: '#fff', fontSize: 14 }}>复制</Text>
                    </TouchableOpacity>
                </View>
                <View style={{ flex: 1, paddingHorizontal: 10, paddingTop: 10 }}>

                    <View style={styles.Line}>
                        <Text style={{ color: myTheme.inputColor }}>船名：{this.state.shipName}</Text>
                    </View>
                    <View style={styles.Line}>
                        <Text style={{ color: myTheme.inputColor }}>航次：{this.state.voyageCode}</Text>
                    </View>
                    <View style={styles.Line}>
                        <Text style={{ color: myTheme.inputColor }}>航线：{this.state.line}</Text>
                    </View>

                    <View style={styles.Line}>
                        <Text style={{ color: myTheme.inputColor }}>抵港位置：{this.state.arrivalPosition}</Text>
                    </View>

                    <View style={styles.Line}>
                        <Text style={{ color: myTheme.inputColor }}>抵港时间：{this.state.arrivePortTime}</Text>
                    </View>

                    <View style={styles.Line}>
                        <Text style={{ color: myTheme.inputColor }}>剩余航程：{this.state.toNextPortDistance} 海里</Text>
                    </View>

                    <View style={styles.Line}>
                        <Text style={{ color: myTheme.inputColor }}>抵港艏吃水：{this.state.foreDraft} 米</Text>
                    </View>

                    <View style={styles.Line}>
                        <Text style={{ color: myTheme.inputColor }}>抵港舯吃水：{this.state.midshipDraft} 米</Text>
                    </View>

                    <View style={styles.Line}>
                        <Text style={{ color: myTheme.inputColor }}>抵港艉吃水：{this.state.aftDraft} 米</Text>
                    </View>

                    <View style={styles.Line}>
                        <Text style={{ color: myTheme.inputColor }}>修改原因：{this.state.reason}</Text>
                    </View>
                </View>
                <Text style={styles.bottomTip}>———— 已经到底了 ————</Text>
            </Container>
        );
    }
}
ArrivePortRecordDetai.navigationOptions = () => ({
    headerLeft: (
        null
    ),
    headerStyle: {
        height: 0,
        marginTop: iphoneX ? -44 : -30,
    },
});
ArrivePortRecordDetai.propTypes = {
    navigation: PropTypes.object.isRequired,
    getArrivePortRecordDetailPromise: PropTypes.func.isRequired,
    data: PropTypes.object,
};
ArrivePortRecordDetai.defaultProps = {
    data: {},
};

const mapStateToProps = createStructuredSelector({
    data: makeList(),
});

function mapDispatchToProps(dispatch) {
    return {
        ...bindPromiseCreators({
            getArrivePortRecordDetailPromise
        }, dispatch),
        dispatch,
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ArrivePortRecordDetai);
