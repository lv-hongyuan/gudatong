import { getArrivePortRecordDetailRoutine } from './actions';
import _ from "lodash";

const initState = {
  loading: false,
  data: {},
};

export default function (state = initState, action) {
  switch (action.type) {
    case getArrivePortRecordDetailRoutine.TRIGGER:
      return { ...state, loading: true, };
    case getArrivePortRecordDetailRoutine.SUCCESS:
      console.log('详情数据：',action.payload)
      return { ...state, data: action.payload.data, };
    case getArrivePortRecordDetailRoutine.FAILURE:
      return {
        ...state, data: {}, error: action.payload,
      };
    case getArrivePortRecordDetailRoutine.FULFILL:
      return { ...state, loading: false };

    default:
      return state;
  }
}
