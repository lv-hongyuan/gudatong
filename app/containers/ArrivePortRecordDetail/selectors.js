import { createSelector } from 'reselect/es';

const ArrivePortRecordDetail = () => state => state.arrivePortRecordDetail;

const makeList = () => createSelector(ArrivePortRecordDetail(), (subState) => {
  console.debug(subState);
  return subState.list;
})

export { makeList};