import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_ARRIVEPORT_RECORD_DETAIL } from './constants';

export const getArrivePortRecordDetailRoutine = createRoutine(GET_ARRIVEPORT_RECORD_DETAIL,updates => updates,
    () => ({ globalLoading: true }));
export const getArrivePortRecordDetailPromise = promisifyRoutine(getArrivePortRecordDetailRoutine);