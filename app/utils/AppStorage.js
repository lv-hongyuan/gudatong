import store from '../Store';

const makeUserInfo = () => store.getState().app.user;

const AppStorage = {
  get token() {
    return (makeUserInfo() || {}).token;
  },
  get userId() {
    return (makeUserInfo() || {}).userId;
  },
  get sysUser() {
    return (makeUserInfo() || {}).sysUser;
  },
  get userInfo() {
    return makeUserInfo();
  },
  get registrationId() {
    return (makeUserInfo() || {}).deviceKey;
  },
  get shipId() {
    return (makeUserInfo() || {}).shipId;
  },
  get shipName() {
    return (makeUserInfo() || {}).shipName;
  },
};

export default AppStorage;
