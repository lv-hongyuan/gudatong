
// 网络请求与报错检查

import { DeviceEventEmitter } from 'react-native';
import { APP_LOGOUT } from '../common/Constant';
import RNFS from 'react-native-fs';
import { formatDate,longToDate } from '../utils/DateFormat';

/**
 * Parses the JSON returned by a network request
 *
 * @param  {object} response A response from a network request
 *
 * @return {object}          The parsed JSON from the request
 */
function parseJSON(response) {
  //获取接口名
  let interfaceName="";
  if(response.url && response.url.length > 0){
    let OffLineApiUrl = response.url
    if(/([^\*\/\(\:]+)$/.test(OffLineApiUrl)){interfaceName=RegExp.$1;}
  }
  return response.text().then((text) => {
    let errorMessage = ''
    if(interfaceName == 'submitArrivalConfirmOperation'){
      errorMessage = '离线确报上传失败,请尝试关闭、开启网络重新上传'
    } else if(interfaceName == 'createSailDyn'){
      errorMessage = '离线船报上传失败,请尝试关闭、开启网络重新上传'
    }else{
      errorMessage = '数据解析错误'
    }
    console.log('接口名：',interfaceName);
    if (text && text.length > 0) {
      const json = JSON.parse(text);
      return Promise.resolve(json);
    }
    throw new Error(errorMessage);
  });
}

function processResult(responseJson) {
  if (Array.isArray(responseJson)) {
    return new Promise((resolve => resolve(responseJson)));
  }

  // eslint-disable-next-line no-underscore-dangle
  const code = `${responseJson._backcode}`;
  switch (code) {
    case '200':
      // upNetError()
    case '811':
    case '812':
    case '813':
      return Promise.resolve(responseJson);
    case '303':
    case '304':
    case '901':
      DeviceEventEmitter.emit(APP_LOGOUT);
      break;
    default:
      break;
  }

  // eslint-disable-next-line no-underscore-dangle
  const error = new Error(responseJson._backmes || '未知错误');
  error.code = parseInt(code, 10);
  error.response = responseJson;
  throw error;
}

/**
 * Checks if a network request came back fine, and throws an error if not
 *
 * @param  {object} response   A response from a network request
 *
 * @return {object|undefined} Returns either the response, or throws an error
 */
function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }

  // const error = new Error(response.statusText);
  const error = new Error(`未知错误 ${response.status}`);
  error.response = response;
  return parseJSON(response)
    .then((responseJson) => {
      error.message = responseJson.error || `未知错误 ${response.status}`;
      error.code = responseJson.status;
      throw error;
    })
    .catch(() => {
      throw error;
    });
}
//查看当前本地错误日志
function upNetError(){
  const path = RNFS.MainBundlePath + '/NetErrorMessage.txt';
  return RNFS.readFile(path)
      .then((result) => {
        // console.log('当前本地错误日志：',result);
      })
      .catch((err) => {
        console.log(err.message);
      });
}

function saveNetError(error){
  if(error.message == 'Network request failed'){
    error.message = '网络连接失败';
  }
  const timeStamp = new Date().getTime()
  const NewTime = formatDate(longToDate(timeStamp),'yyyy-MM-dd hh:mm:ss')
  const path = RNFS.MainBundlePath + '/NetErrorMessage.txt';
  return RNFS.appendFile(
    path, `
    ${NewTime}:      ${error.message}`,
    'utf8')
    .then(() => {
      console.log('错误日志保存至本地成功');
    })
    .catch((err) => {
      console.log(err.message);
    });
}

/**
 * Requests a URL, returning a promise
 *
 * @return {object} The api call
 * @param api
 */
export default function request(api) {
  return api
    .then(checkStatus)
    .then(parseJSON)
    .then(processResult)
    .catch(error=>{
      // saveNetError(error)
      if(error.message == 'Network request failed'){
        error.message = '网络连接失败';
      }
      throw(error);
    })
}
