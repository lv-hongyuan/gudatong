import shortId from 'shortid';

export default function fixIdList(list) {
  if (Array.isArray(list)) {
    return list.map((item) => {
      if (item.id === null || item.id === undefined) {
        return { ...item, id: shortId.generate() };
      }
      return { ...item };
    });
  }
  return list;
}
