export default function stringToNumber(string) {
  if (typeof string === 'number') {
    return string;
  }
  if (typeof string === 'string') {
    if (string.length === 0) {
      return null;
    }
    const arr = string.match(/(-)?(\d+)?(\.)?(\d+)?/g);
    if (!arr || arr.length === 0) {
      return null;
    }
    if (arr[0].length === 0) {
      return null;
    }
    return parseFloat(arr[0]);
  }
  return null;
}
