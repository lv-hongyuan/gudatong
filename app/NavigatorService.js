import { NavigationActions } from 'react-navigation';

let topNavigator;

const cacheFunctions = [];

function addCacheCall(func, ...params) {
  cacheFunctions.push({
    func,
    params,
  });
}

function setTopLevelNavigator(navigatorRef) {
  topNavigator = navigatorRef;
  if (cacheFunctions.length > 0) {
    cacheFunctions.forEach((cache) => {
      cache.func(...cache.params);
    });
  }
  cacheFunctions.splice(0, cacheFunctions.length);
}

function navigate(routeName, params) {
  if (topNavigator) {
    topNavigator.dispatch(NavigationActions.navigate({
      routeName,
      params,
    }));
  } else {
    addCacheCall(navigate, routeName, params);
  }
}

function back(routeName, params) {
  if (topNavigator) {
    topNavigator.dispatch(NavigationActions.back({
      routeName,
      params,
    }));
  } else {
    addCacheCall(back, routeName, params);
  }
}

function dispatch(action) {
  if (topNavigator) {
    topNavigator.dispatch(action);
  } else {
    addCacheCall(dispatch, action);
  }
}

// add other navigation functions that you need and export them

export default {
  navigate,
  back,
  dispatch,
  setTopLevelNavigator,
};
