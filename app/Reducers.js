import { combineReducers } from 'redux';
import { reducer as network } from 'react-native-offline';
import { NavigationActions } from 'react-navigation';
import { AppNavigator } from './AppNavigator';
import loginReducer from './containers/Login/reducer';
import userAuthReducer from './containers/UserAuth/reducer';
import shipWorkReducer from './containers/ShipWork/reducer';
import actualBurdeningLayoutReducer from './containers/ActualBurdeningLayout/reducer';
import beforehandLayoutReducer from './containers/BeforehandLayout/reducer';
import anchorageReducer from './containers/Anchorage/reducer';
import colligationReducer from './containers/Colligation/reducer';
import createPilotApplyReducer from './containers/CreatePilotApply/reducer';
import editAnchoringEventReducer from './containers/EditAnchoringEvent/reducer';
import createShiftingBerthReducer from './containers/CreateShiftingBerth/reducer';
import createSpecialTypeToteBinReducer from './containers/CreateSpecialTypeToteBin/reducer';
import createTugboatApplyReducer from './containers/CreateTugboatApply/reducer';
import createTugboatUseRecordReducer from './containers/CreateTugboatUseRecord/reducer';
import navigationRecordReducer from './containers/NavigationRecord/reducer';
import arrivePortRecordReducer from './containers/ArrivePortRecord/reducer';
import arrivePortRecordListReducer from './containers/ArrivePortRecordList/reducer';
import arrivePortRecordDetailReducer from './containers/ArrivePortRecordDetail/reducer';
import pilotApplyListReducer from './containers/PilotApplyList/reducer';
import serviceFeeReducer from './containers/ServiceFee/reducer';
import nextPortForecastReducer from './containers/NextPortForecast/reducer';
import shiftingBerthReducer from './containers/ShiftingBerth/reducer';
import specialTypeToteBinReducer from './containers/SpecialTypeToteBin/reducer';
import tugboatApplyListReducer from './containers/TugboatApplyList/reducer';
import tugboatUseListReducer from './containers/TugboatUseList/reducer';
import errReducer from './containers/ErrorHandler/reducer';
import documentsReducer from './containers/Documents/reducer';
import refuelApplicationListReducer from './containers/RefuelApplicationList/reducer';
import editRefuelApplicationReducer from './containers/EditRefuelApplication/reducer';
import repairApplicationListReducer from './containers/RepairApplicationList/reducer';
import editRepairApplicationReducer from './containers/EditRepairApplication/reducer';
import certificateManagementReducer from './containers/CertificateManagement/reducer';
import bulletinBoardReducer from './containers/BulletinBoard/reducer';
import bulletinContentReducer from './containers/BulletinContent/reducer';
import importantBulletinBoardReducer from './containers/ImportantBulletinBoard/reducer';
import saveFeeListReducer from './containers/SaveFeeList/reducer';
import saveFeeReducer from './containers/SaveFee/reducer';
import saveFeeAlgorithmReducer from './containers/SaveFeeAlgorithm/reducer';
import saveFeeQuestionReducer from './containers/SaveFeeQuestion/reducer';
import voyageInstructionListReducer from './containers/VoyageInstructionList/reducer';
import voyageEventListReducer from './containers/VoyageEventList/reducer';
import createVoyageEventReducer from './containers/CreateVoyageEvent/reducer';
import berthPlanReducer from './containers/BerthPlan/reducer';
import refrigeratorListReducer from './containers/RefrigeratorList/reducer';
import editRefrigeratorRecordReducer from './containers/EditRefrigeratorRecord/reducer';
import dangerousGoodsRecordsReducer from './containers/DangerousGoodsRecords/reducer';
import editDangerousGoodsRecordReducer from './containers/EditDangerousGoodsRecord/reducer';
import appInitReducer from './containers/AppInit/reducer';
import offlineNavigationRecordListReducer from './containers/OfflineNavigationRecordList/reducer';
import offlineArrivePortRecordListReducer from './containers/OfflineArrivePortRecordList/reducer';
import dxRecordListReducer from './containers/DxRecordList/reducer';
import editDxRecordReducer from './containers/EditDxRecord/reducer';
import problemBackReducer from './containers/ProblemBack/reducer';
import loadingPlanReducer from './containers/LoadingPlan/reducer';
import helpReducer from './containers/Help/reducer';
import helpDetailReducer from './containers/HelpDetail/reducer';
import specialBoxReducer from './containers/SpecialBox/reducer';
import notificationCenterReducer from './components/NotificationCenter/reducer';
import historyRecordReducer from './containers/HistoryRecord/reducer';
import historyRecordDetailReducer from './containers/HistoryRecordDetail/reducer';

const initState = AppNavigator.router.getStateForAction(NavigationActions.init());
const navReducer = (state = initState, action) => {
  const newState = AppNavigator.router.getStateForAction(action, state);
  return newState || state;
};
const loading = (state = { isLoading: false }, action) => {
  if (action.type.indexOf('Navigation') !== -1
    || action.type.indexOf('redux') !== -1
    || action.type.indexOf('MESSAGE') !== -1
    || action.type.indexOf('@@network-connectivity/') !== -1
  ) {
    return state;
  }

  if (action.meta && action.meta.globalLoading) {
    if (action.type.indexOf('/TRIGGER') !== -1) {
      return { ...state, isLoading: true };
    }
    if (action.type.indexOf('/FULFILL') !== -1) {
      return { ...state, isLoading: false };
    }
  }

  if (action.type.indexOf('/TRIGGER') !== -1
    || action.type.indexOf('/REQUEST') !== -1
    || action.type.indexOf('/SUCCESS') !== -1
    || action.type.indexOf('/FAILURE') !== -1
    || action.type.indexOf('/FULFILL') !== -1
  ) {
    return state;
  }

  if (action.noLoading) {
    return state;
  }
  if (action.type.indexOf('RESULT') === -1) {
    return { ...state, isLoading: true };
  }
  return { ...state, isLoading: false };
};

const AppReducer = combineReducers({
  nav: navReducer,
  loading,
  app: appInitReducer,
  notificationCenter: notificationCenterReducer,
  anchorage: anchorageReducer,
  documents: documentsReducer,
  error: errReducer,
  tugboatUseList: tugboatUseListReducer,
  tugboatApplyList: tugboatApplyListReducer,
  specialTypeToteBin: specialTypeToteBinReducer,
  shiftingBerth: shiftingBerthReducer,
  serviceFee: serviceFeeReducer,
  pilotApplyList: pilotApplyListReducer,
  nextPortForecast: nextPortForecastReducer,
  navigationRecord: navigationRecordReducer,
  arrivePortRecord: arrivePortRecordReducer,
  arrivePortRecordList: arrivePortRecordListReducer,
  arrivePortRecordDetail: arrivePortRecordDetailReducer,
  historyRecord:historyRecordReducer,
  historyRecordDetail:historyRecordDetailReducer,
  createTugboatUseRecord: createTugboatUseRecordReducer,
  createTugboatApply: createTugboatApplyReducer,
  createSpecialTypeToteBin: createSpecialTypeToteBinReducer,
  createShiftingBerth: createShiftingBerthReducer,
  createPilotApply: createPilotApplyReducer,
  editAnchoringEvent: editAnchoringEventReducer,
  colligation: colligationReducer,
  beforehandLayout: beforehandLayoutReducer,
  actualBurdeningLayout: actualBurdeningLayoutReducer,
  userAuth: userAuthReducer,
  login: loginReducer,
  shipWork: shipWorkReducer,
  refuelApplication: refuelApplicationListReducer,
  editRefuelApplication: editRefuelApplicationReducer,
  repairApplication: repairApplicationListReducer,
  editRepairApplication: editRepairApplicationReducer,
  certificateManagement: certificateManagementReducer,
  bulletinBoard: bulletinBoardReducer,
  bulletinContent: bulletinContentReducer,
  importantBulletinBoard: importantBulletinBoardReducer,
  saveFeeList: saveFeeListReducer,
  saveFee: saveFeeReducer,
  saveFeeAlgorithm: saveFeeAlgorithmReducer,
  saveFeeQuestion: saveFeeQuestionReducer,
  voyageInstructionList: voyageInstructionListReducer,
  voyageEvent: voyageEventListReducer,
  createVoyageEvent: createVoyageEventReducer,
  berthPlan: berthPlanReducer,
  refrigeratorList: refrigeratorListReducer,
  editRefrigeratorRecord: editRefrigeratorRecordReducer,
  dangerousGoodsRecords: dangerousGoodsRecordsReducer,
  editDangerousGoodsRecord: editDangerousGoodsRecordReducer,
  offlineNavigationRecordList: offlineNavigationRecordListReducer,
  offlineArrivePortRecordList: offlineArrivePortRecordListReducer,
  dxRecordList: dxRecordListReducer,
  editDxRecord: editDxRecordReducer,
  problemBack: problemBackReducer,
  loadingPlan: loadingPlanReducer,
  help: helpReducer,
  helpDetail: helpDetailReducer,
  specialBox: specialBoxReducer,
  network,
});

export default AppReducer;
