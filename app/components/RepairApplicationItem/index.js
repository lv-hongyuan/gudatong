import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { Button, Icon, Label, SwipeRow, View } from 'native-base';
import myTheme from '../../Themes';
import { reviewTitleFromState } from '../../common/Constant';
import commonStyles from '../../common/commonStyles';
import { defaultFormat } from '../../utils/DateFormat';

const styles = StyleSheet.create({
  row: {
    flex: 1,
    paddingLeft: 15,
    paddingTop: 5,
    paddingBottom: 5,
    paddingRight: 30,
    flexDirection: 'row',
  },
  label: {
    fontSize: 14,
    color: '#535353',
  },
  text: {
    fontSize: 14,
    color: myTheme.inputColor,
    marginLeft: 10,
    flex: 1,
  },
  container: {
    width: '100%',
    backgroundColor: 'white',
    padding: 10,
  },
});

/**
 * Created by jianzhexu on 2018/3/29
 */
class RepairApplicationItem extends React.PureComponent {
  static propTypes = {
    portId: PropTypes.number.isRequired,
    portName: PropTypes.string.isRequired,
    shipId: PropTypes.number.isRequired,
    shipName: PropTypes.string.isRequired,
    startTime: PropTypes.number.isRequired,
    lengthTime: PropTypes.number.isRequired,
    reason: PropTypes.string.isRequired,
    isEnd: PropTypes.number.isRequired,
    state: PropTypes.number.isRequired,
    id: PropTypes.number.isRequired,
    onDelete: PropTypes.func.isRequired,
    onItemPress: PropTypes.func.isRequired,
  };

  render() {
    const {
      portId, portName, shipId, shipName, startTime, lengthTime, reason, isEnd, state, id,
    } = this.props;

    return (
      <SwipeRow
        style={commonStyles.swipeRow}
        rightOpenValue={-75}
        body={
          <TouchableOpacity
            style={styles.container}
            onPress={() => {
              this.props.onItemPress({
                portId, portName, shipId, shipName, startTime, lengthTime, reason, isEnd, state, id,
              });
            }}
          >
            <View style={styles.row}>
              <Label style={styles.label}>修船地点:</Label>
              <Label style={styles.text}>{portName}</Label>
            </View>
            <View style={styles.row}>
              <Label style={styles.label}>修船开始:</Label>
              <Label style={styles.text}>{startTime > 0 ? defaultFormat(startTime) : ''}</Label>
            </View>
            <View style={styles.row}>
              <Label style={styles.label}>修船时间:</Label>
              <Label style={styles.text}>{`${lengthTime || 0}小时`}</Label>
            </View>
            <View style={styles.row}>
              <Label style={styles.label}>修船原因:</Label>
              <Label style={styles.text}>{reason}</Label>
            </View>
            <View style={styles.row}>
              <Label style={styles.label}>是否结束:</Label>
              <Label
                style={[styles.text, { color: isEnd === 1 ? myTheme.inputColor : 'red' }]}
              >
                {isEnd === 1 ? '已结束' : '未结束'}
              </Label>
            </View>
            <View style={styles.row}>
              <Label style={styles.label}>审核状态:</Label>
              <Label style={styles.text}>{reviewTitleFromState(state)}</Label>
            </View>
          </TouchableOpacity>
        }
        right={
          <Button
            danger
            onPress={() => this.props.onDelete({
              portId, portName, shipId, shipName, startTime, lengthTime, reason, isEnd, state, id,
            })}
          >
            <Icon active name="trash" />
          </Button>
        }
      />
    );
  }
}

RepairApplicationItem.propTypes = {};

export default RepairApplicationItem;
