import React, { Component } from 'react';
import { View, Text, Animated, Easing } from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/Ionicons';
import MJRefresh from 'react-native-mjrefresh';
import { SkypeIndicator } from 'react-native-indicators';

const AnimatedIcon = Animated.createAnimatedComponent(Icon);

class CustomRefreshControl extends Component {
  state = {
    text: '下拉刷新',
    rotate: new Animated.Value(0),
    refreshing: false,
  };
  onReleaseToRefresh = () => {
    this.setState({
      text: '释放刷新',
    });
    Animated.timing(this.state.rotate, {
      toValue: 1,
      duration: 197,
      useNativeDriver: true,
      easing: Easing.linear(),
    }).start();
  };
  onRefresh = () => {
    const { onRefresh } = this.props;
    this.setState({
      refreshing: true,
      text: '正在刷新',
    });
    if (onRefresh) onRefresh();
  };
  onPulling = (e) => {
    // console.log(e.nativeEvent)
    if (e.nativeEvent.percent === 0) {
      this.setState({
        text: '下拉刷新',
        refreshing: false,
      });
      Animated.timing(this.state.rotate, {
        toValue: 0,
        duration: 197,
        useNativeDriver: true,
        easing: Easing.linear(),
      }).start();
    }
  };
  onRefreshIdle = () => {

  };
  finishRefresh = () => {
    if (this.mjRefresh) this.mjRefresh.finishRefresh();
  };
  beginRefresh = () => {
    if (this.mjRefresh) this.mjRefresh.beginRefresh();
  };

  render() {
    return (
      <MJRefresh
        onRefreshIdle={this.onRefreshIdle}
        ref={(ref) => { this.mjRefresh = ref; }}
        onReleaseToRefresh={this.onReleaseToRefresh}
        onRefresh={this.onRefresh}
        onPulling={this.onPulling}
      >
        <Animated.View style={{
          transform: [{ translateX: this.props.headerTranslateX || 0 }],
          width: this.props.containerWidth || '100%',
        }}
        >
          <View style={{
            height: 40,
            paddingTop: 5,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
          }}
          >
            {/* <Image style={{ */}
            {/* position:'absolute', */}
            {/* width, */}
            {/* left:0,right:0,bottom:0,height:width*1436/1024, */}
            {/* }} */}
            {/* source={require('./93K58PICGPs_1024.jpg')} */}
            {/* /> */}
            {this.state.refreshing ? <SkypeIndicator style={{ flex: 0 }} size={24} color="#ED1727" /> :
            <AnimatedIcon
              style={{
                  transform: [{
                    rotate: this.state.rotate.interpolate({
                      inputRange: [0, 1],
                      outputRange: ['180deg', '0deg'],
                    }),
                  }],
                }}
              name="md-arrow-up"
              color="#ED1727"
              size={24}
            />}
            <Text style={{ marginLeft: 15 }}>{this.state.text}</Text>
          </View>
        </Animated.View>
      </MJRefresh>
    );
  }
}

CustomRefreshControl.propTypes = {
  headerTranslateX: PropTypes.number,
  containerWidth: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  onRefresh: PropTypes.func,
};

CustomRefreshControl.defaultProps = {
  onRefresh: undefined,
  containerWidth: undefined,
  headerTranslateX: undefined,
};

export default CustomRefreshControl;
