import React, { Component } from 'react';
import {
  Text,
  Animated,
  Easing,
} from 'react-native';
import PropTypes from 'prop-types';
import { SmartRefreshControl, AnyHeader } from 'react-native-smartrefreshlayout';
import Icon from 'react-native-vector-icons/Ionicons';
import { SkypeIndicator } from 'react-native-indicators';

const AnimatedIcon = Animated.createAnimatedComponent(Icon);

class CustomRefreshControl extends Component {
  state = {
    text: '下拉刷新',
    rotate: new Animated.Value(0),
    refreshing: false,
    autoRefresh: false,
  };
  onPullDownToRefresh = () => {
    this.setState({
      text: '下拉刷新',
      refreshing: false,
    });
    Animated.timing(this.state.rotate, {
      toValue: 0,
      duration: 197,
      useNativeDriver: true,
      easing: Easing.linear(),
    }).start();
  };
  onReleased = () => {
    this.setState({
      refreshing: true,
      text: '正在刷新',
    });
  };
  onReleaseToRefresh = () => {
    this.setState({
      text: '释放刷新',
    });
    Animated.timing(this.state.rotate, {
      toValue: 1,
      duration: 197,
      useNativeDriver: true,
      easing: Easing.linear(),
    }).start();
  };
  onRefresh = () => {
    const { onRefresh } = this.props;
    if (onRefresh) onRefresh();
  };
  finishRefresh = (params) => {
    this.setState({
      autoRefresh: false,
    }, () => {
      if (this.refreshRef) this.refreshRef.finishRefresh(params);
    });
  };
  beginRefresh = () => {
    this.setState({
      autoRefresh: true,
    });
  };

  render() {
    return (
      <SmartRefreshControl
        style={{ flex: 1 }}
        ref={(ref) => { this.refreshRef = ref; }}
        autoRefresh={{ refresh: this.state.autoRefresh }}
        onRefresh={this.onRefresh}
        onPullDownToRefresh={this.onPullDownToRefresh}
        onHeaderReleased={this.onReleased}
        onReleaseToRefresh={this.onReleaseToRefresh}
        headerHeight={40}
        HeaderComponent={
          <Animated.View style={{
            transform: [{ translateX: this.props.headerTranslateX || 0 }],
            width: this.props.containerWidth || '100%',
          }}
          >
            <AnyHeader style={{
              height: 40,
              paddingTop: 5,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
            }}
            >
              {this.state.refreshing ? (
                <SkypeIndicator style={{ flex: 0 }} size={24} color="#ED1727" />
              ) : (
                <AnimatedIcon
                  style={{
                    transform: [{
                      rotate: this.state.rotate.interpolate({
                        inputRange: [0, 1],
                        outputRange: ['180deg', '0deg'],
                      }),
                    }],
                  }}
                  name="md-arrow-up"
                  color="#ED1727"
                  size={24}
                />
              )}
              <Text style={{ marginLeft: 15 }}>{this.state.text}</Text>
            </AnyHeader>
          </Animated.View>
        }
      >
        {this.props.children}
      </SmartRefreshControl>
    );
  }
}

CustomRefreshControl.defaultProps = {
  children: null,
  onRefresh: undefined,
  containerWidth: undefined,
  headerTranslateX: undefined,
};

CustomRefreshControl.propTypes = {
  headerTranslateX: PropTypes.number,
  containerWidth: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  onRefresh: PropTypes.func,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default CustomRefreshControl;
