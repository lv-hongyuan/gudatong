import React from 'react';
import PropTypes from 'prop-types';
import ActionSheet from 'react-native-actionsheet';
import ImagePicker from 'react-native-syan-image-picker';

// import ImageResizer from 'react-native-image-resizer';

/**
 * Created by ocean on 2018/4/25
 */
class CustomImagePicker extends React.PureComponent {
  onPressCamera() {
    ImagePicker.openCamera({
      isCrop: false,
      showCropCircle: false,
      showCropFrame: false,
    }, (e, images) => {
      if (e) {
        console.log(e);
      } else if (this.props.onPickImages) {
        this.props.onPickImages(images);
      }
    });
  }

  onPressPhoto() {
    ImagePicker.asyncShowImagePicker({
      imageCount: 1,
      isRecordSelected: false,
      ...this.props.options,
    }).then((images) => {
      if (this.props.onPickImages) this.props.onPickImages(images);
    }).catch((e) => {
      console.log(e);
    });
  }

  onPress = (index) => {
    if (index === 0) {
      this.onPressCamera();
    } else if (index === 1) {
      this.onPressPhoto();
    }
  };

  show() {
    this.ActionSheet.show();
  }

  render() {
    return (
      <ActionSheet
        ref={(ref) => { this.ActionSheet = ref; }}
        title={this.props.title}
        options={['拍摄照片', '从相册选择', '取消']}
        cancelButtonIndex={2}
        onPress={this.onPress}
      />
    );
  }
}

CustomImagePicker.propTypes = {
  onPickImages: PropTypes.func,
  options: PropTypes.object,
  title: PropTypes.string,
};

CustomImagePicker.defaultProps = {
  onPickImages: undefined,
  options: {},
  title: '选择图片',
};

export default CustomImagePicker;
