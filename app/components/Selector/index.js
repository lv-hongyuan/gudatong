import React from 'react';
import PropTypes from 'prop-types';
import Overlay from 'teaset/components/Overlay/Overlay';
import Select from 'teaset/components/Select/Select';
import Theme from 'teaset/themes/Theme';
import { Keyboard, Text, TouchableOpacity, View, StyleSheet, ScrollView } from 'react-native';
import Svg from '../Svg';

import myTheme from '../../Themes';

/**
 * Created by ocean on 2018/4/2
 */

Theme.set({ fitIPhoneX: true, poppMinHeight: 10, poppMinWidth: 60 });

const maxItemsHeight = 400;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    width: 260,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden',
  },
  title: {
    margin: 10,
    paddingLeft: 10,
    paddingRight: 10,
    color: '#ED1727',
    textAlign: 'center',
  },
  item: {
    padding: 10,
    marginBottom: 10,
    borderWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
    borderRadius: 10,
    alignItems: 'center',
  },
  text: {},
  selected: {
    color: 'red',
    borderColor: 'red',
  },
});

class Selector extends React.Component {
  showPicker = () => {
    Keyboard.dismiss();
    if (this.props.pickerType === 'modal') {
      this.showModalPicker();
    } else {
      this.select.showPicker();
    }
  };

  showModalPicker = () => {
    const {
      pickerTitle, items, getItemText, getItemValue, value, onSelected,
    } = this.props;
    const overlayView = (
      <Overlay.PopView
        ref={(ref) => {
          this.popView = ref;
        }}
        style={{ alignItems: 'center', justifyContent: 'center' }}
      >
        <View style={styles.container}>
          {pickerTitle ? <Text style={styles.title}>{pickerTitle}</Text> : null}
          <ScrollView
            ref={(ref) => {
              this.scrollView = ref;
            }}
            onContentSizeChange={(contentWidth, contentHeight) => {
              if (contentHeight > maxItemsHeight) {
                console.log(contentWidth, contentHeight);
                const selectedIndex = items.findIndex(item => getItemValue(item) === value);
                if (selectedIndex !== -1) {
                  const offSet = ((contentHeight / items.length) * (selectedIndex + 1)) - maxItemsHeight;
                  if (offSet > 0) {
                    if (this.scrollView) this.scrollView.scrollTo({ x: 0, y: offSet, animated: false });
                  }
                }
              }
            }}
            style={{ maxHeight: maxItemsHeight, width: '100%' }}
            bounces={false}
          >
            {items.length > 0 && (
              <View style={{ padding: 10, paddingBottom: 0, width: '100%' }}>
                {items.map((item) => {
                  const selected = value === getItemValue(item);
                  return (
                    <TouchableOpacity
                      key={`${getItemValue(item)}`}
                      onPress={() => {
                        if (this.popView) this.popView.close();
                        console.log('Select item is :',item)
                        if (onSelected) onSelected(item);
                      }}
                    >
                      <View style={[styles.item, selected ? styles.selected : undefined]}>
                        <Text style={[styles.text, selected ? styles.selected : undefined]}>
                          {getItemText(item)}
                        </Text>
                      </View>
                    </TouchableOpacity>
                  );
                })}
              </View>
            )}
          </ScrollView>
        </View>
      </Overlay.PopView>
    );
    return Overlay.show(overlayView);
  };

  render() {
    return (
      <Select
        style={{
          flex: 1, borderWidth: 0, height: '100%', paddingLeft: 5, backgroundColor: 'transparent',
        }}
        ref={(ref) => {
          this.select = ref;
        }}
        onPress={() => {
          Keyboard.dismiss();
          this.showPicker();
        }}
        valueStyle={{
          flex: 1, textAlign: 'left', paddingLeft: 0, color: '#969696',
        }}
        items={this.customItems}
        getItemValue={item => item.value}
        getItemText={item => item.text}
        icon={(
          <Svg
            icon="selectAfter"
            size={10}
            style={{
              position: 'absolute',
              bottom: 0,
              right: 0,
            }}
          />
        )}
        placeholder={this.props.placeholder}
        placeholderTextColor={this.props.placeholderTextColor}
        {...this.props}
      />
    );
  }
}

Selector.propTypes = {
  pickerTitle: PropTypes.string,
  pickerType: PropTypes.string,
  datavalue: PropTypes.any,
  items: PropTypes.array,
  getItemValue: PropTypes.func,
  getItemText: PropTypes.func,
  onSelected: PropTypes.func,
  placeholder: PropTypes.string,
  placeholderTextColor: PropTypes.string,
  disabled: PropTypes.bool,
};

Selector.defaultProps = {
  pickerTitle: undefined,
  pickerType: 'modal',
  value: undefined,
  items: [],
  getItemValue: item => item.value,
  getItemText: item => item.text,
  onSelected: undefined,
  placeholder: '请选择...',
  placeholderTextColor: '#969696',
  disabled: false,
};

export default Selector;
