import React from 'react';
import PropTypes from 'prop-types';
import { Spinner, View } from 'native-base';
import Overlay from 'teaset/components/Overlay/Overlay';

/**
 * Created by jianzhexu on 2018/4/3
 */
class LoadingComponent extends React.PureComponent {
  static navigationOptions = {};

  static propTypes = {
    visible: PropTypes.bool.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.visible !== this.props.visible) {
      if (nextProps.visible) {
        this.overlay = Overlay.show(this.renderLoadingView());
      // } else if (this.popOverView) {
      //   this.popOverView.close();
      } else {
        Overlay.hide(this.overlay);
      }
    }
  }

  renderLoadingView() {
    return (
      <Overlay.PopView
        modal
        style={{ alignItems: 'center', justifyContent: 'center' }}
        ref={(ref) => {
          this.popOverView = ref;
        }}
      >
        <View style={{
          backgroundColor: '#fff',
          minWidth: 160,
          minHeight: 120,
          borderRadius: 15,
          justifyContent: 'center',
          alignItems: 'center',
        }}
        >
          <Spinner
            color="red"
            style={{
              width: 60,
              height: 60,
            }}
          />
        </View>
      </Overlay.PopView>
    );
  }

  render() {
    return (
      <View />
    );
  }
}

export default LoadingComponent;
