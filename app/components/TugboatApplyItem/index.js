import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { Button, Icon, Label, SwipeRow, View } from 'native-base';
import { getTugboatApplyType, getTugboatUseWayName, ReviewState } from '../../common/Constant';
import { OpType } from '../../containers/CreateTugboatApply/constants';
import myTheme from '../../Themes';
import commonStyles from '../../common/commonStyles';
import Svg from '../../components/Svg';

const styles = StyleSheet.create({
  row: {
    flex: 1,
    paddingLeft: 30,
    paddingTop: 5,
    paddingBottom: 5,
    paddingRight: 30,
    flexDirection: 'row',
  },
  label: {
    fontSize: 14,
    color: '#535353',
  },
  disabledLabel: {
    fontSize: 14,
    color: myTheme.inputColor,
  },
  text: {
    fontSize: 14,
    color: myTheme.inputColor,
    marginLeft: 20,
    flex: 1,
  },
  container: {
    width: '100%',
    backgroundColor: 'white',
    padding: 10,
  },
  doneImage: {
    position: 'absolute',
    top: 0,
    right: 40,
  },
});

/**
 * Created by jianzhexu on 2018/4/3
 */
class TugboatApplyItem extends React.PureComponent {
  static propTypes = {
    portName: PropTypes.string.isRequired, // 港口名称
    tugNum: PropTypes.number.isRequired, // 拖轮数量
    useWay: PropTypes.number.isRequired, // 使用途径 10靠泊 20离泊 30移泊
    applyType: PropTypes.number.isRequired, // 申请原因类型 1大风影响、2港口强制、3潮水因素、4码头条件
    applyReasons: PropTypes.string.isRequired, // 申请原因
    state: PropTypes.number.isRequired, // 申请结果 0未审批 10批准 20驳回
    rejectReasons: PropTypes.string.isRequired, // 驳回原因
    onDelete: PropTypes.func.isRequired,
    id: PropTypes.number.isRequired,
    popId: PropTypes.number.isRequired,
    onItemPress: PropTypes.func.isRequired,
    opType: PropTypes.string.isRequired,
  };

  render() {
    const {
      portName, tugNum, useWay, applyType, applyReasons, state, rejectReasons, id, popId, opType,
    } = this.props;

    let icon = 'op_type_use_under_reviewed';
    if (opType === OpType.APPLY) {
      switch (state) {
        case ReviewState.UnderReviewed:
          icon = 'op_type_use_under_reviewed';
          break;
        case ReviewState.Approved:
          icon = 'op_type_use_approved';
          break;
        case ReviewState.Dismissed:
          icon = 'op_type_use_dismissed';
          break;
        default:
          break;
      }
    } else {
      icon = 'op_type_use_record';
    }

    return (
      <SwipeRow
        style={commonStyles.swipeRow}
        rightOpenValue={-75}
        body={
          <TouchableOpacity
            style={styles.container}
            onPress={() => {
              this.props.onItemPress({
                portName, tugNum, useWay, applyType, applyReasons, state, rejectReasons, id, popId, opType,
              });
            }}
          >
            <View style={styles.row}>
              <Label style={styles.label}>发生港口:</Label>
              <Label style={styles.text}>{portName}</Label>
            </View>
            <View style={styles.row}>
              <Label style={styles.label}>使用途径:</Label>
              <Label style={styles.text}>{getTugboatUseWayName(useWay)}</Label>
            </View>
            <View style={styles.row}>
              <Label style={styles.label}>拖轮数量:</Label>
              <Label style={styles.text}>{tugNum || 0}艘</Label>
            </View>
            <View style={styles.row}>
              <Label style={styles.label}>申请原因类型:</Label>
              <Label style={styles.text}>{getTugboatApplyType(applyType)}</Label>
            </View>
            <View style={styles.row}>
              <Label style={styles.label}>申请原因:</Label>
              <Label style={styles.text}>{applyReasons}</Label>
            </View>
            {opType !== '使用' ? (
              <View>
                {/* <View style={styles.row}> */}
                {/* <Label style={styles.label}>审核情况:</Label> */}
                {/* <Label style={styles.text}>{reviewTitleFromState(state)}</Label> */}
                {/* </View> */}
                <View style={styles.row}>
                  <Label style={styles.label}>审核描述:</Label>
                  <Label style={styles.text}>{rejectReasons}</Label>
                </View>
              </View>
            ) : null}
            <Svg
              icon={icon}
              size={60}
              style={{ position: 'absolute', top: 0, right: 0 }}
            />
          </TouchableOpacity>
        }
        right={
          <Button
            danger
            onPress={() => this.props.onDelete({
              portName, tugNum, useWay, applyType, applyReasons, state, rejectReasons, id, popId, opType,
            })}
          >
            <Icon active name="trash" />
          </Button>
        }
      />
    );
  }
}

export default TugboatApplyItem;
