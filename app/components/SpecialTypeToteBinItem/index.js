import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { Button, Icon, Label, SwipeRow, View } from 'native-base';
import myTheme from '../../Themes';
import commonStyles from '../../common/commonStyles';

const styles = StyleSheet.create({
  row: {
    flex: 1,
    paddingLeft: 10,
    paddingTop: 5,
    paddingBottom: 5,
    paddingRight: 0,
    flexDirection: 'row',
  },
  item: {
    flex: 1,
    flexDirection: 'row',
  },
  label: {
    fontSize: 14,
    color: '#535353',
  },
  text: {
    fontSize: 14,
    color: myTheme.inputColor,
    marginLeft: 20,
    flex: 1,
  },
  container: {
    width: '100%',
    backgroundColor: 'white',
    padding: 10,
    // flexDirection: 'row',
  },
});

/**
 * Created by jianzhexu on 2018/3/29
 */
class SpecialTypeToteBinItem extends React.PureComponent {
  static propTypes = {
    id: PropTypes.number.isRequired,
    popId: PropTypes.number.isRequired,
    portName: PropTypes.string.isRequired,
    voyageId: PropTypes.number.isRequired,
    contNo: PropTypes.string.isRequired,
    bay: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    wideLeft: PropTypes.number.isRequired,
    wideRight: PropTypes.number.isRequired,
    longFront: PropTypes.number.isRequired,
    longBehind: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
    weight: PropTypes.number.isRequired,
    contType: PropTypes.string.isRequired,
    onDelete: PropTypes.func,
    onItemPress: PropTypes.func,
  };
  static defaultProps = {
    onDelete: undefined,
    onItemPress: undefined,
  };

  render() {
    const {
      id,
      popId,
      portName,
      voyageId,
      contNo,
      bay,
      type,
      wideLeft,
      wideRight,
      longFront,
      longBehind,
      height,
      weight,
      contType,
    } = this.props;

    return (
      <SwipeRow
        style={commonStyles.swipeRow}
        rightOpenValue={-75}
        body={
          <TouchableOpacity
            style={styles.container}
            onPress={() => {
              this.props.onItemPress({
                id,
                popId,
                portName,
                voyageId,
                contNo,
                bay,
                type,
                wideLeft,
                wideRight,
                longFront,
                longBehind,
                height,
                weight,
                contType,
              });
            }}
          >
            <View style={styles.row}>
              <View style={styles.item}>
                <Label style={styles.label}>箱号:</Label>
                <Label style={styles.text}>{this.props.contNo}</Label>
              </View>
              <View style={styles.item}>
                <Label style={styles.label}>卸货港:</Label>
                <Label style={styles.text}>{this.props.portName}</Label>
              </View>
            </View>
            <View style={styles.row}>
              <View style={styles.item}>
                <Label style={styles.label}>贝位:</Label>
                <Label style={styles.text}>{this.props.bay}</Label>
              </View>
              <View style={styles.item}>
                <Label style={styles.label}>超宽左:</Label>
                <Label style={styles.text}>{this.props.wideLeft}</Label>
              </View>
            </View>
            <View style={styles.row}>
              <View style={styles.item}>
                <Label style={styles.label}>类型:</Label>
                <Label style={styles.text}>{this.props.type}</Label>
              </View>
              <View style={styles.item}>
                <Label style={styles.label}>超宽右:</Label>
                <Label style={styles.text}>{this.props.wideRight}</Label>
              </View>
            </View>
            <View style={styles.row}>
              <View style={styles.item}>
                <Label style={styles.label}>重量:</Label>
                <Label style={styles.text}>{this.props.weight}</Label>
              </View>
              <View style={styles.item}>
                <Label style={styles.label}>超长前:</Label>
                <Label style={styles.text}>{this.props.longFront}</Label>
              </View>
            </View>
            <View style={styles.row}>
              <View style={styles.item}>
                <Label style={styles.label}>超高:</Label>
                <Label style={styles.text}>{this.props.height}</Label>
              </View>
              <View style={styles.item}>
                <Label style={styles.label}>超长后:</Label>
                <Label style={styles.text}>{this.props.longBehind}</Label>
              </View>
            </View>
            <View style={styles.row}>
              <View style={styles.item}>
                <Label style={styles.label}>箱型:</Label>
                <Label style={styles.text}>{contType}</Label>
              </View>
            </View>
          </TouchableOpacity>
        }
        right={
          <Button danger onPress={() => this.props.onDelete(this.props.id)}>
            <Icon active name="trash" />
          </Button>
        }
      />
    );
  }
}

SpecialTypeToteBinItem.propTypes = {};

export default SpecialTypeToteBinItem;
