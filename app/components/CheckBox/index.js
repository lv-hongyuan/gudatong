import React from 'react';
import PropTypes from 'prop-types';
import { View, TouchableWithoutFeedback, ViewPropTypes } from 'react-native';
import Svg from '../Svg';

/**
 * Created by ocean on 2018/4/28
 */
class CheckBox extends React.PureComponent {
  render() {
    return (
      <TouchableWithoutFeedback
        style={this.props.style}
        onPress={this.props.onPress}
      >
        <View>
          {
            this.props.checked ? (
              <Svg icon="check-on" size={15} />
            ) : (
              <View style={{
                width: 15, height: 15, borderColor: '#969696', borderWidth: 2,
              }}
              />
            )
          }
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

CheckBox.propTypes = {
  checked: PropTypes.bool,
  onPress: PropTypes.func.isRequired,
  style: ViewPropTypes.style,
};
CheckBox.defaultProps = {
  checked: false,
  style: undefined,
};

export default CheckBox;
