import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { Button, Icon, Label, SwipeRow, View } from 'native-base';
import myTheme from '../../Themes';
import { reviewTitleFromState } from '../../common/Constant';
import commonStyles from '../../common/commonStyles';
import { defaultFormat } from '../../utils/DateFormat';

const styles = StyleSheet.create({
  row: {
    flex: 1,
    paddingLeft: 15,
    paddingTop: 5,
    paddingBottom: 5,
    paddingRight: 30,
    flexDirection: 'row',
  },
  label: {
    fontSize: 14,
    color: '#535353',
  },
  text: {
    fontSize: 14,
    color: myTheme.inputColor,
    marginLeft: 10,
    flex: 1,
  },
  container: {
    width: '100%',
    backgroundColor: 'white',
    padding: 10,
  },
});

/**
 * Created by jianzhexu on 2018/3/29
 */
class RefuelApplicationItem extends React.PureComponent {
  static propTypes = {
    portId: PropTypes.number,
    portName: PropTypes.string,
    shipId: PropTypes.number,
    shipName: PropTypes.string,
    etaTime: PropTypes.number,
    oil180: PropTypes.number,
    oil120: PropTypes.number,
    oil0: PropTypes.number,
    state: PropTypes.number,
    id: PropTypes.number.isRequired,
    onDelete: PropTypes.func,
    onItemPress: PropTypes.func,
  };
  static defaultProps = {
    portId: null,
    portName: '',
    shipId: '',
    shipName: '',
    etaTime: null,
    oil180: null,
    oil120: null,
    oil0: null,
    state: null,
    onDelete: undefined,
    onItemPress: undefined,
  };

  render() {
    const {
      portId, portName, shipId, shipName, etaTime, oil180, oil120, oil0, id, state,
    } = this.props;

    return (
      <SwipeRow
        style={commonStyles.swipeRow}
        rightOpenValue={-75}
        body={
          <TouchableOpacity
            style={styles.container}
            onPress={() => {
              this.props.onItemPress({
                portId, portName, shipId, shipName, etaTime, oil180, oil120, oil0, id, state,
              });
            }}
          >
            <View style={styles.row}>
              <Label style={styles.label}>港口:</Label>
              <Label style={styles.text}>{portName}</Label>
            </View>
            <View style={styles.row}>
              <Label style={styles.label}>船名:</Label>
              <Label style={styles.text}>{shipName}</Label>
            </View>
            <View style={styles.row}>
              <Label style={styles.label}>预抵时间:</Label>
              <Label style={styles.text}>{etaTime > 0 ? defaultFormat(etaTime) : ''}</Label>
            </View>
            <View style={styles.row}>
              <Label style={styles.label}>180:</Label>
              <Label style={styles.text}>{oil180 > 0 ? oil180 : ''}</Label>
            </View>
            <View style={styles.row}>
              <Label style={styles.label}>120:</Label>
              <Label style={styles.text}>{oil120 > 0 ? oil120 : ''}</Label>
            </View>
            <View style={styles.row}>
              <Label style={styles.label}>0:</Label>
              <Label style={styles.text}>{oil0 > 0 ? oil0 : ''}</Label>
            </View>
            <View style={styles.row}>
              <Label style={styles.label}>审核状态:</Label>
              <Label style={styles.text}>{reviewTitleFromState(state)}</Label>
            </View>
          </TouchableOpacity>
        }
        right={
          <Button
            danger
            onPress={() => this.props.onDelete({
              portId, portName, shipId, shipName, etaTime, oil180, oil120, oil0, id, state,
            })}
          >
            <Icon active name="trash" />
          </Button>
        }
      />
    );
  }
}

RefuelApplicationItem.propTypes = {};

export default RefuelApplicationItem;
