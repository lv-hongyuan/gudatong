import React from 'react';
import PropTypes from 'prop-types';
import { Label, Badge, Text, Button, View } from 'native-base';
import { ViewPropTypes } from 'react-native';
import Svg from '../Svg';

class TextIconWithBadge extends React.PureComponent {
  render() {
    return (
      <Button
        transparent
        onPress={() => {
          console.log('iconClick');
          this.props.onClick();
        }}
        style={[{
          minWidth: 60,
          minHeight: 60,
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
        }, this.props.style]}
      >
        <View style={{
          height: 60,
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
        }}
        >
          <View>
            <Svg color={this.props.color} size={this.props.iconSize} icon={this.props.icon} />
            {this.props.showBadge &&
            <Badge
              style={{
                minWidth: 15,
                height: 15,
                paddingLeft: 0,
                paddingRight: 0,
                paddingTop: 0,
                paddingBottom: 0,
                position: 'absolute',
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                top: -3,
                right: -3,
              }}
            >
              <Text style={{
                fontSize: 13,
                lineHeight: 15,
              }}
              >{this.props.badgeText}
              </Text>
            </Badge>
            }
          </View>
          <Label
            numberOfLines={1}
            style={{
              width: '100%',
              textAlign: 'center',
              color: '#535353',
              marginTop: 10,
              fontSize: this.props.textSize ? this.props.textSize : 14,
            }}
          >{this.props.text}
          </Label>
        </View>
      </Button>);
  }
}

TextIconWithBadge.propTypes = {
  text: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  showBadge: PropTypes.bool.isRequired,
  color: PropTypes.string.isRequired,
  badgeText: PropTypes.string,
  iconSize: PropTypes.number.isRequired,
  textSize: PropTypes.number,
  icon: PropTypes.string.isRequired,
  style: ViewPropTypes.style,
};
TextIconWithBadge.defaultProps = {
  badgeText: undefined,
  textSize: undefined,
  style: {},
};

export default TextIconWithBadge;
