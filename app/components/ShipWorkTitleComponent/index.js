import React from 'react';
import { PixelRatio, StyleSheet,TouchableOpacity,Text,View } from 'react-native';
import PropTypes from 'prop-types';
import { Col, Label, Row } from 'native-base';
import _ from 'lodash';
import * as Animatable from 'react-native-animatable';
import ShortId from 'shortid';
import HorizontalTimeLine from '../HorizontalTimeLine';
import Ionicons from 'react-native-vector-icons/FontAwesome';

const zoomFlash = {
  0: {
    opacity: 1,
    scale: 1.3,
  },
  0.5: {
    opacity: 0.9,
    scale: 1,
  },
  1: {
    opacity: 1,
    scale: 1.3,
  },
};

Animatable.initializeRegistryWithDefinitions({ zoomFlash });

const styles = StyleSheet.create({
  col: {
    flexDirection: 'row',
    height: 45,
    paddingLeft: 10,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
});

/**
 * Created by jianzhexu on 2018/4/3
 */
class ShipWorkTitleComponent extends React.PureComponent {
  static propTypes = {
    state: PropTypes.string,
    portName: PropTypes.string,
    runningState: PropTypes.array,
    isSail: PropTypes.bool.isRequired,
  };
  static defaultProps = {
    portName: '',
    state: '',
    runningState: [],
  };

  renderState() {
    const { state, portName } = this.props;
    if (!_.isEmpty(portName) && !_.isEmpty(state)) {
      const portNameStr = `【${portName}】`;
      const sepArray = state.split(portNameStr);
      if (sepArray.length > 1) {
        return sepArray.reduce((array, current, index) => {
          array.push(<Label key={ShortId.generate()} style={{ fontSize: 14, color: '#535353' }}>{current}</Label>);
          if (index < sepArray.length - 1) {
            const portComponent = (
              <Animatable.Text
                key={ShortId.generate()}
                animation="zoomFlash"
                duration={1000}
                iterationCount="infinite"
                easing="linear"
                style={{ fontSize: 14, color: '#1e80ff' }}
              >{portNameStr}
              </Animatable.Text>
            );
            array.push(portComponent);
          }
          return array;
        }, [<Label key={ShortId.generate()} style={{ fontSize: 14, color: '#535353' }}>当前状态:</Label>]);
      }
    }
    return <Label key={ShortId.generate()} style={{ fontSize: 14, color: '#535353' }}>当前状态:{state}</Label>;
  }

  render() {
    const {
      runningState, isSail,
    } = this.props;
    return (
      <Row style={{
        backgroundColor: '#FFFFFF',
        flexDirection: 'column',
        marginTop: 8,
      }}
      >
        <Row style={{
          borderBottomWidth: 1 / PixelRatio.getPixelSizeForLayoutSize(1),
          borderBottomColor: '#e0e0e0',
        }}
        >
          <Col style={styles.col}>
            {this.renderState()}
          </Col>
        </Row>
        <Row style={{
          height: 100,
          flexDirection:'row'
        }}
        >
          
          <HorizontalTimeLine data={runningState} isSail={isSail} />
          {/* <TouchableOpacity 
            style={{
              width:34,backgroundColor:'#ff0e28',height:100,
              flexDirection:'row',alignItems:'center',justifyContent:'center'
              // padding:3,
            }}> 
            <View style={{width:1,height:90,backgroundColor:'#ddd',marginRight:2}}/>
            <View style={{width:1,height:90,backgroundColor:'#ddd',marginRight:4}}/>
            <Ionicons 
              name={'chevron-circle-left'}
              size={30}
              style={{ color: 'white' }}
            />
          </TouchableOpacity> */}
        </Row>
      </Row>
    );
  }
}

export default ShipWorkTitleComponent;
