import React from 'react';
import { TextInput as Input } from 'react-native';
import PropTypes from 'prop-types';

const DEBUG = false;
const log = (...args) => {
  if (DEBUG) console.log(args);
};

export const MaskType = {
  NONE: 'NONE',
  INTEGER: 'INTEGER',
  FLOAT: 'FLOAT',
  ALPHANUM: 'ALPHANUM'
};

function stringValue(value) {
  const type = typeof value;
  switch (type) {
    case 'string':
      return value;
    case 'number':
      return value.toString();
    default:
      return '';
  }
}

export class TextInput extends React.Component {
  // 正整数限制
  static integerFilter(value, integerNumber = 10) {
    if (!value) {
      return value;
    }

    const array = (`${value}`).match(RegExp(`(\\d{0,${integerNumber}})?`));
    const result = array.find(str => str && str.length > 0);
    if (!result) return '';

    const intValue = parseFloat(result);

    return Number.isNaN(intValue) ? '' : (`${intValue}`);
  }

  static propTypes = {
    maskType: PropTypes.string,
    value: PropTypes.string,
    integerNumber: PropTypes.number,
    decimalNumber: PropTypes.number,
    canNegative: PropTypes.bool,
    editable: PropTypes.bool,
    onChangeText: PropTypes.func,
  };

  static defaultProps = {
    maskType: MaskType.NONE,
    integerNumber: 10,
    decimalNumber: 10,
    canNegative: false,
    editable: true,
    value: undefined,
    onChangeText: undefined,
  };

  // 正负小数限制
  static floatFilter(value, integerNumber = 10, decimalNumber = 10, canNegative = false) {
    if (!value) {
      return value;
    }

    // eslint-disable-next-line max-len
    const reg = RegExp(`${canNegative ? '(-)?' : ''}(?:((\\d{0,${integerNumber}})?(\\.)(\\d{0,${decimalNumber}})?|((\\d{0,${integerNumber}})?(\\.)?)))`);
    const array = (`${value}`).match(reg);
    const result = array.find(str => str && str.length > 0);
    if (!result) return '';

    const valueArray = result.split('.');
    const negativeStr = valueArray[0][0] === '-' ? '-' : '';
    const intStr = valueArray[0][0] === '-' ? valueArray[0].slice(1, valueArray[0].length) : valueArray[0];
    const intValue = parseFloat(intStr);

    if (valueArray.length > 1) {
      return `${negativeStr + (Number.isNaN(intValue) ? '0' : intValue)}.${valueArray[1]}`;
    }
    return negativeStr + (Number.isNaN(intValue) ? '' : intValue);
  }

  static AlphanumFilter(value) {

    const newText = value.replace(/[^\w\.\/]/ig, '');
    console.log('===>>>',newText);
    return newText;
  }

  constructor(props) {
    super(props);

    this.state = {
      value: stringValue(props.value),
    };
  }

  // componentWillReceiveProps (nextProps) {
  //   log('componentWillReceiveProps')
  //   if (nextProps.value !== this.props.value) {
  //     this.setNewValue(stringValue(nextProps.value))
  //   }
  // }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.value !== prevState.prePropValue) {
      const newValue = stringValue(nextProps.value);
      log('getDerivedStateFromProps');
      return {
        value: newValue,
        prePropValue: nextProps.value,
      };
    }
    return null;
  }

  onChangeText = (value) => {
    log(value);

    this.setNewValue(value, () => {
      if (this.props.onChangeText) this.props.onChangeText(this.state.value);
    });
  };

  setNewValue(value, callBack = () => { }) {
    const newValue = this.filteredValue(value);
    this.setState({ value: newValue }, callBack);
  }

  // 正负小数限制
  filteredValue(value) {
    switch (this.props.maskType) {
      case MaskType.FLOAT:
        return TextInput.floatFilter(value, this.props.integerNumber, this.props.decimalNumber, this.props.canNegative);
      case MaskType.INTEGER:
        return TextInput.integerFilter(value, this.props.integerNumber);
      case MaskType.ALPHANUM:
        return TextInput.AlphanumFilter(value);
      default:
        return value;
    }
  }

  render() {
    const props = {
      ...this.props,
      value: this.state.value,
      onChangeText: this.onChangeText,
      underlineColorAndroid: 'transparent',
    };
    return <Input {...props} />;
  }
}

