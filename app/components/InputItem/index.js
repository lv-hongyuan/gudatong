import React from 'react';
import PropTypes from 'prop-types';
import { Platform,ViewPropTypes, StyleSheet ,PixelRatio,AppState} from 'react-native';
import { Item, Label } from 'native-base';
import { TextInput, MaskType } from './TextInput';
import commonStyles from '../../common/commonStyles';
import { inputAccessoryViewID } from '../InputAccessory';
const platform = Platform.OS;
/**
 * Created by ocean on 2018/4/12
 */

class InputItem extends React.Component {
  static stringValue(value) {
    const type = typeof value;
    switch (type) {
      case 'string':
        return value;
      case 'number':
        return value.toString();
      default:
        return '';
    }
  }

  constructor(props) {
    super(props);

    this.state = {
      value: InputItem.stringValue(props.value),
      isValid: true,
      appState: AppState.currentState,
      x:PixelRatio.getFontScale()
    };
  }

  //监听手机前后台状态实时更新安卓端字体大小
  componentDidMount(){
    AppState.addEventListener('change',this.handleAppStateChange)
  }
  componentWillUnmount(){
    AppState.removeEventListener('change',this.handleAppStateChange)
  }

  handleAppStateChange = (nextAppState) => {
    if(this.state.appState.match(/inactive|background/) && nextAppState === 'active'){
      this.setState({x:PixelRatio.getFontScale()})
    }
    this.setState({appState:nextAppState})
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.value !== nextProps.value) {
      this.setState({ value: InputItem.stringValue(nextProps.value) });
    }
  }

  onChangeText = (formatted) => {
    if (this.props.onChangeText) this.props.onChangeText(formatted);
  };

  onEndEditing = () => {
    this.validateInput();
  };

  onSubmitEditing = () => {
    this.validateInput();
  };

  validateInput = () => {};

  render() {
    const labelStyle =
      StyleSheet.flatten([
        this.props.editable ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel,
        this.props.labelStyle,
      ]);
    return (
      <Item error={!this.state.isValid} style={commonStyles.inputItem}>
        {this.props.isRequired && <Label style={{ color: 'red' }}>*</Label>}
        <Label style={labelStyle}>{this.props.label}
        </Label>
        <TextInput
          value={this.state.value}
          ref={(ref) => {
            this.input = ref;
          }}
          maskType={this.props.maskType}
          integerNumber={this.props.integerNumber}
          decimalNumber={this.props.decimalNumber}
          canNegative={this.props.canNegative}
          underlineColorAndroid="transparent"
          inputAccessoryViewID={inputAccessoryViewID}
          maxLength={this.props.maxLength}
          onChangeText={this.onChangeText}
          onEndEditing={this.onEndEditing}
          onSubmitEditing={this.onSubmitEditing}
          keyboardType={this.props.keyboardType}
          placeholder={this.props.placeholder}
          placeholderTextColor="#969696"
          style={StyleSheet.flatten([commonStyles.input, this.props.style,{fontSize:platform === 'ios' ? 14 : 14 / this.state.x}])}
          editable={this.props.editable}
        />
        {this.props.rightItem}
      </Item>
    );
  }
}

InputItem.propTypes = {
  value: PropTypes.any,
  label: PropTypes.string,
  rightItem: PropTypes.any,
  placeholder: PropTypes.string,
  onChangeText: PropTypes.func,
  maxLength: PropTypes.number,
  keyboardType: PropTypes.string,
  editable: PropTypes.bool,
  maskType: PropTypes.string,
  integerNumber: PropTypes.number,
  decimalNumber: PropTypes.number,
  canNegative: PropTypes.bool,
  isRequired: PropTypes.bool,
  style: ViewPropTypes.style,
  labelStyle: ViewPropTypes.style,
};

InputItem.defaultProps = {
  value: undefined,
  label: undefined,
  rightItem: undefined,
  keyboardType: 'default',
  placeholder: '请填写...',
  editable: true,
  maskType: MaskType.NONE,
  integerNumber: 10,
  decimalNumber: 10,
  canNegative: false,
  onChangeText: undefined,
  maxLength: undefined,
  isRequired: false,
  style: undefined,
  labelStyle: undefined,
};

export default InputItem;
