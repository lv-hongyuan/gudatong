import React from 'react';
import PropTypes from 'prop-types';
import { Text, View } from 'native-base';
import Svg from '../Svg';

const styles = {
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    flex: 1,
    alignItems: 'center',
    zIndex: 0,
  },
  leftLine: {
    flexDirection: 'column',
    justifyContent: 'center',
    flex: 1,
    // marginRight: -5,
    zIndex: 0,
  },
  iconContainer: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 1,
  },
  rightLine: {
    flexDirection: 'column',
    justifyContent: 'center',
    flex: 1,
    // marginLeft: -5,
    zIndex: 0,
  },
  text: {
    height: 20,
  },
};

/**
 * Created by jianzhexu on 2018/3/22
 */
class TimeLineComponent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      didLayout: false,
      iconCenterX: 0,
    };
  }

  onIconLayout = (event) => {
    // 获取根View的宽高，以及左上角的坐标值
    const { x, y, width } = event.nativeEvent.layout;
    this.setState({
      didLayout: true,
      iconCenterX: x + (width / 2),
      iconCenterY: y + 15,
    });
  };

  render() {
    let flex = 0;
    let justifyContent = 'center';
    if (this.props.showLeft) {
      flex += 0.5;
    }
    if (this.props.showRight) {
      flex += 0.5;
    }
    if (this.props.showLeft && !this.props.showRight) {
      justifyContent = 'flex-end';
    }
    if (!this.props.showLeft && this.props.showRight) {
      justifyContent = 'flex-start';
    }

    return (
      <View style={{
        flex, justifyContent, flexDirection: 'row', alignItems: 'center',
      }}
      >
        <View style={{ ...styles.iconContainer }} onLayout={this.onIconLayout}>
          <Svg icon={this.props.icon} size={30} color={this.props.color} />
          <Text style={{ ...styles.text, color: this.props.textColor }}>{this.props.text}</Text>
        </View>
        {
          this.state.didLayout && this.props.showLeft ? (
            <View style={{
              position: 'absolute',
              left: 0,
              width: this.state.iconCenterX,
              height: 2,
              borderWidth: 1,
              borderColor: this.props.leftColor,
              borderStyle: this.props.leftLineStyle,
              top: this.state.iconCenterY - 1,
            }}
            />
          ) : null
        }
        {
          this.state.didLayout && this.props.showRight ? (
            <View style={{
              position: 'absolute',
              left: this.state.iconCenterX,
              right: 0,
              height: 2,
              borderWidth: 1,
              borderColor: this.props.rightColor,
              borderStyle: this.props.rightLineStyle,
              top: this.state.iconCenterY - 1,
            }}
            />
          ) : null
        }
      </View>
    );
  }
}

TimeLineComponent.defaultPropTypes = {
  leftFlex: 1,
  rightFlex: 1,
};

TimeLineComponent.propTypes = {
  showLeft: PropTypes.bool.isRequired,
  leftColor: PropTypes.string,
  rightColor: PropTypes.string,
  leftLineStyle: PropTypes.string,
  rightLineStyle: PropTypes.string,
  color: PropTypes.string,
  textColor: PropTypes.string,
  showRight: PropTypes.bool.isRequired,
  text: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
};

TimeLineComponent.defaultProps = {
  leftColor: '#ff0e28',
  rightColor: '#ff0e28',
  leftLineStyle: 'dotted',
  rightLineStyle: 'dotted',
  color: undefined,
  textColor: '#9c9c9e',
};

export default TimeLineComponent;
