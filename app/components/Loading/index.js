import React from 'react';
import {
  StyleSheet, View, ActivityIndicator, ViewPropTypes,
} from 'react-native';
import PropTypes from 'prop-types';

/**
 * Created by ocean on 2018/4/16
 */

const styles = StyleSheet.create({
  background: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.3)',
  },
  loadingContainer: {
    backgroundColor: 'white',
    borderRadius: 4,
    justifyContent: 'center',
    alignItems: 'center',
    width: 100,
    padding: 20,
  },
  loadingText: {
    marginTop: 10,
  },
});

class Loading extends React.PureComponent {
  render() {
    return (
      <View style={StyleSheet.flatten([styles.background, this.props.backgroundStyle])}>
        <View style={styles.loadingContainer}>
          <ActivityIndicator size="large" color={this.props.indicatorColor} />
          {/* <Text style={styles.loadingText}>载入中...</Text> */}
        </View>
      </View>
    );
  }
}

Loading.propTypes = {
  backgroundStyle: ViewPropTypes.style,
  indicatorColor: PropTypes.string,
};

Loading.defaultProps = {
  backgroundStyle: undefined,
  indicatorColor: '#DC001B',
};

export default Loading;
