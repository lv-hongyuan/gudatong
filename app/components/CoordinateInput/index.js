import React from 'react';
import PropTypes from 'prop-types';
import { Platform , View, TouchableOpacity, StyleSheet, Text ,PixelRatio} from 'react-native';
import _ from 'lodash';
import { inputAccessoryViewID } from '../InputAccessory';
import { MaskType, TextInput } from '../InputItem/TextInput';

/**
 * Created by ocean on 2018/4/12
 */
const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: 50,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  sign: {
    width: 30,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    flex: 1,
    // fontSize: Platform.OS === 'ios' ? 14 : 14 / PixelRatio.getFontScale(),
    height: 50,
    color: '#969696',
    maxWidth: 60,
    textAlign: 'right',
    marginRight: 3,
  },
  text: {
    fontSize: 14,
    color: '#969696',
    maxWidth: 60,
  },
});

class CoordinateInput extends React.Component {
  static stringValue(value) {
    const type = typeof value;
    switch (type) {
      case 'string':
        return Math.abs(parseFloat(value)).toString();
      case 'number':
        return Math.abs(value).toString();
      default:
        return '';
    }
  }

  constructor(props) {
    super(props);

    this.state = {
      degrees: CoordinateInput.stringValue(this.props.degrees),
      points: CoordinateInput.stringValue(this.props.points),
      sign: _.isNumber(this.props.degrees) ? this.props.degrees >= 0 : true,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.degrees !== nextProps.degrees || this.props.points !== nextProps.points) {
      this.setState({
        degrees: CoordinateInput.stringValue(nextProps.degrees),
        points: CoordinateInput.stringValue(nextProps.points),
        sign: _.isNumber(nextProps.degrees) ? nextProps.degrees >= 0 : true,
      });
    }
  }

  onChangeDegrees = (formatted) => {
    const degreesNum = this.state.sign ? parseFloat(formatted) : -parseFloat(formatted);
    const pointsNum = this.state.sign ? parseFloat(this.state.points) : -parseFloat(this.state.points);
    const degrees = Number.isNaN(degreesNum) ? 0 : degreesNum;
    const points = Number.isNaN(pointsNum) ? 0 : pointsNum;
    if (this.props.onChangeValue) this.props.onChangeValue(degrees, points);
  };

  onChangePoints = (formatted) => {
    const degreesNum = this.state.sign ? parseFloat(this.state.degrees) : -parseFloat(this.state.degrees);
    const pointsNum = this.state.sign ? parseFloat(formatted) : -parseFloat(formatted);
    const degrees = Number.isNaN(degreesNum) ? 0 : degreesNum;
    const points = Number.isNaN(pointsNum) ? 0 : pointsNum;
    if (this.props.onChangeValue) this.props.onChangeValue(degrees, points);
  };

  onChangeSign = () => {
    const degreesNum = this.state.sign ? parseFloat(this.state.degrees) : -parseFloat(this.state.degrees);
    const pointsNum = this.state.sign ? parseFloat(this.state.points) : -parseFloat(this.state.points);
    const degrees = Number.isNaN(degreesNum) ? 0 : degreesNum;
    const points = Number.isNaN(pointsNum) ? 0 : pointsNum;
    if (this.props.onChangeValue) this.props.onChangeValue(degrees, points);
  };

  // onEndEditing = () => {
  //   this.validateInput()
  // }
  //
  // onSubmitEditing = () => {
  //   this.validateInput()
  // }

  render() {
    return (
      <View style={styles.container}>
        {/* <TouchableOpacity
          style={styles.sign}
          onPress={() => {
            this.setState({ sign: !this.state.sign }, () => {
              this.onChangeSign();
            });
          }}
        > */}
        <View style={styles.sign}>
          {
            this.props.type === 'lo'
              // ? <Text>{this.state.sign ? 'E' : 'W'}</Text>
              // : <Text>{this.state.sign ? 'N' : 'S'}</Text>
              ? <Text style={{color:'#535353'}}>{'E'}</Text>
              : <Text style={{color:'#535353'}}>{'N'}</Text>
          }
        </View>
        {/* </TouchableOpacity> */}
        <TextInput
          allowFontScaling={false}
          value={this.state.degrees}
          maskType={MaskType.INTEGER}
          integerNumber={3}
          ref={(ref) => {
            this.input = ref;
          }}
          underlineColorAndroid="transparent"
          inputAccessoryViewID={inputAccessoryViewID}
          maxLength={6}
          onChangeText={this.onChangeDegrees}
          // onEndEditing={this.onEndEditing}
          // onSubmitEditing={this.onSubmitEditing}
          keyboardType="numeric"
          placeholder=""
          placeholderTextColor="#969696"
          style={styles.input}
          editable={this.props.editable}
        />
        <Text style={[styles.text, { width: undefined }]}>°</Text>
        <TextInput
          allowFontScaling={false}
          value={this.state.points}
          maskType={MaskType.FLOAT}
          integerNumber={3}
          ref={(ref) => {
            this.input = ref;
          }}
          underlineColorAndroid="transparent"
          inputAccessoryViewID={inputAccessoryViewID}
          maxLength={6}
          onChangeText={this.onChangePoints}
          // onEndEditing={this.onEndEditing}
          // onSubmitEditing={this.onSubmitEditing}
          keyboardType="numeric"
          placeholder=""
          placeholderTextColor="#969696"
          style={styles.input}
          editable={this.props.editable}
        />
        <Text style={[styles.text, { width: undefined }]}>{'\''}</Text>
      </View>
    );
  }
}

CoordinateInput.propTypes = {
  degrees: PropTypes.number,
  points: PropTypes.number,
  onChangeValue: PropTypes.func,
  editable: PropTypes.bool,
  type: PropTypes.string.isRequired,
};

CoordinateInput.defaultProps = {
  degrees: undefined,
  points: undefined,
  editable: true,
  onChangeValue: undefined,
};

export default CoordinateInput;
