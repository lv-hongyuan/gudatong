import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { Button, Icon, Label, SwipeRow, View } from 'native-base';
import myTheme from '../../Themes';
import commonStyles from '../../common/commonStyles';

const styles = StyleSheet.create({
  row: {
    flex: 1,
    paddingLeft: 15,
    paddingTop: 5,
    paddingBottom: 5,
    paddingRight: 30,
    flexDirection: 'row',
  },
  label: {
    fontSize: 14,
    color: '#535353',
  },
  text: {
    fontSize: 14,
    color: myTheme.inputColor,
    marginLeft: 10,
    flex: 1,
  },
  container: {
    width: '100%',
    backgroundColor: 'white',
    padding: 10,
  },
});

/**
 * Created by jianzhexu on 2018/3/29
 */
class RefrigeratorItem extends React.PureComponent {
  static propTypes = {
    id: PropTypes.number,
    popId: PropTypes.number,
    contNumber: PropTypes.string,
    unloadingPort: PropTypes.string,
    shipLoadPosition: PropTypes.string,
    temperature: PropTypes.string,
    ventilationOpening: PropTypes.string,
    contType: PropTypes.string,
    onDelete: PropTypes.func,
    onItemPress: PropTypes.func,
  };
  static defaultProps = {
    id: null,
    popId: null,
    contNumber: null,
    unloadingPort: null,
    shipLoadPosition: null,
    temperature: null,
    ventilationOpening: null,
    contType: null,
    onDelete: undefined,
    onItemPress: undefined,
  };

  render() {
    const {
      id, popId, contNumber, unloadingPort, shipLoadPosition, temperature, ventilationOpening, contType,
    } = this.props;

    return (
      <SwipeRow
        style={commonStyles.swipeRow}
        rightOpenValue={-75}
        body={
          <TouchableOpacity
            style={styles.container}
            onPress={() => {
              if (this.props.onItemPress) {
                this.props.onItemPress({
                  id, popId, contNumber, unloadingPort, shipLoadPosition, temperature, ventilationOpening, contType,
                });
              }
            }}
          >
            <View style={styles.row}>
              <Label style={styles.label}>箱号:</Label>
              <Label style={styles.text}>{contNumber}</Label>
            </View>
            <View style={styles.row}>
              <Label style={styles.label}>卸货港:</Label>
              <Label style={styles.text}>{unloadingPort}</Label>
            </View>
            <View style={styles.row}>
              <Label style={styles.label}>装船位置:</Label>
              <Label style={styles.text}>{shipLoadPosition}</Label>
            </View>
            <View style={styles.row}>
              <Label style={styles.label}>设置温度:</Label>
              <Label style={styles.text}>{temperature}</Label>
              <Label style={[commonStyles.text, { flex: 0 }]}>°C</Label>
            </View>
            <View style={styles.row}>
              <Label style={styles.label}>通风开度:</Label>
              <Label style={styles.text}>{ventilationOpening}</Label>
            </View>
            <View style={styles.row}>
              <Label style={styles.label}>箱型:</Label>
              <Label style={styles.text}>{contType}</Label>
            </View>
          </TouchableOpacity>
        }
        right={
          <Button
            danger
            onPress={() => {
              if (this.props.onDelete) this.props.onDelete(this.props.id);
            }}
          >
            <Icon active name="trash" />
          </Button>
        }
      />
    );
  }
}

RefrigeratorItem.propTypes = {};

export default RefrigeratorItem;
