import React from 'react';
import { StyleSheet, TouchableOpacity, View, Keyboard } from 'react-native';
import { Text } from 'native-base';
import PropTypes from 'prop-types';
import myTheme from '../../Themes';
import Svg from '../Svg/index';

const styles = StyleSheet.create({
  button: {
    flex: 1,
    marginTop: 0,
    flexDirection: 'row',
    marginBottom: 0,
    padding: 0,
    height: '100%',
    alignItems: 'center',
  },
  text: {
    flex: 1,
    flexDirection: 'row',
    fontSize: myTheme.inputFontSize,
    color: myTheme.inputColor,
    textAlign: 'left',
    padding: 5,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 20,
  },
  placeholder: {
    flex: 1,
    flexDirection: 'row',
    fontSize: myTheme.inputFontSize,
    color: myTheme.inputColor,
    textAlign: 'left',
    padding: 5,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 20,
  },
  afterIcon: {
    position: 'absolute',
    bottom: 0,
    right: 0,
  },
});

class SelectButton extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  renderText = () => {
    if (this.props.value && this.props.value !== '') {
      return (
        <Text style={[styles.text, this.props.textStyle]}>
          {this.props.value}
        </Text>
      );
    }
    return (
      <Text style={[styles.placeholder, this.props.textStyle]}>
        {this.props.placeholder || ''}
      </Text>
    );
  };

  back = () => {
    Keyboard.dismiss();
    if (this.props.onPress) this.props.onPress();
  };

  render() {
    return this.props.readOnly ? (
      <View style={styles.button}>
        {this.renderText()}
        <Svg
          icon="selectAfter"
          size={10}
          style={{
            position: 'absolute',
            bottom: 0,
            right: 0,
          }}
        />
      </View>
    ) : (
      <TouchableOpacity
        style={styles.button}
        onPress={this.back}
      >
        {
          this.renderText()
        }
        <Svg
          icon="selectAfter"
          size={10}
          color="#969696"
          style={{
            position: 'absolute',
            bottom: 0,
            right: 0,
          }}
        />
      </TouchableOpacity>
    );
  }
}

SelectButton.propTypes = {
  value: PropTypes.string,
  placeholder: PropTypes.string,
  onPress: PropTypes.func,
  readOnly: PropTypes.bool,
  textStyle: Text.propTypes.style,
};

SelectButton.defaultProps = {
  value: undefined,
  placeholder: '',
  onPress: undefined,
  readOnly: false,
  textStyle: undefined,
};

export default SelectButton;
