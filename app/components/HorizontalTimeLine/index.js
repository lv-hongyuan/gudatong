import React from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity } from 'react-native';
import TimeLineComponent from '../TimeLineComponent';

/**
 * 2018/3/22 created by jianzhexu
 */
class HorizontalTimeLine extends React.Component {
  static propTypes = {
    data: PropTypes.array.isRequired,
  };

  renderState = () => {
    if (this.props.data && this.props.data.length > 0) {
      return this.props.data.map((item, index) => (
        <TimeLineComponent
          showLeft={item.showLeft}
          key={item.id}
          showRight={item.showRight}
          leftColor={item.arrive ? '#ff0e28' : '#9c9c9e'}
          rightColor={this.props.data[index + 1 >= this.props.data.length ? 0 : index + 1].arrive ? '#ff0e28' : '#9c9c9e'}
          textColor="#ff0e28"
          leftLineStyle='solid'
          rightLineStyle='solid'
          text={item.name}
          icon={item.isShip ? 'running' : (item.arrive ? 'complete' : 'idle')}
          flex={1}
        />));
    }
    return null;
  };

  render() {
    return (
      <TouchableOpacity style={{
        flexDirection: 'row',
        padding: 20,
        alignItems: 'center',
        flex:1,
        justifyContent: 'space-between',
      }}
      >
        {this.renderState()}
      </TouchableOpacity>
    );
  }
}

export default HorizontalTimeLine;
