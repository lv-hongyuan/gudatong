import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, TouchableOpacity } from 'react-native';
import _ from 'lodash';
import { Button, Icon, Label, SwipeRow, View } from 'native-base';
import myTheme from '../../Themes';
import commonStyles from '../../common/commonStyles';
import { defaultFormat } from '../../utils/DateFormat';

const styles = StyleSheet.create({
  row: {
    flex: 1,
    paddingLeft: 15,
    paddingTop: 5,
    paddingBottom: 5,
    paddingRight: 30,
    flexDirection: 'row',
  },
  label: {
    fontSize: 14,
    color: '#535353',
  },
  text: {
    fontSize: 14,
    color: myTheme.inputColor,
    marginLeft: 10,
    flex: 1,
  },
  container: {
    width: '100%',
    backgroundColor: 'white',
    padding: 10,
  },
});

/**
 * Created by jianzhexu on 2018/3/29
 */
class VoyageEventItem extends React.PureComponent {
  static propTypes = {
    longD: PropTypes.number,
    longF: PropTypes.number,
    latD: PropTypes.number,
    latF: PropTypes.number,
    describe: PropTypes.string,
    typeChildCode: PropTypes.number,
    typeChildText: PropTypes.string,
    startTime: PropTypes.number, // 事发时间
    endTime: PropTypes.number, // 事发时间
    hidePlace: PropTypes.string, // 事发时间
    id: PropTypes.number,
    dbId: PropTypes.number,
    popId: PropTypes.number,
    onDelete: PropTypes.func,
    onItemPress: PropTypes.func,
  };
  static defaultProps = {
    longD: undefined,
    longF: undefined,
    latD: undefined,
    latF: undefined,
    popId: undefined,
    id: undefined, // id
    dbId: undefined, // dbId
    startTime: undefined, // 事发时间
    endTime: undefined, // 结束时间
    hidePlace: undefined, // 抛锚位置
    typeChildCode: undefined, // 事件子类型
    typeChildText: undefined, // 事件子类型
    describe: undefined, // 事件描述
    onDelete: undefined,
    onItemPress: undefined,
  };

  render() {
    const {
      longD,
      longF,
      latD,
      latF,
      typeChildText, describe, hidePlace,
      typeChildCode, startTime, endTime, id, popId, dbId,
    } = this.props;

    const la = _.isNumber(latD) ? `${(latD >= 0 ? 'N' : 'S') + Math.abs(latD)}°${Math.abs(latF)}'` : '---';
    const lo = _.isNumber(longD) ? `${(longD >= 0 ? 'E' : 'W') + Math.abs(longD)}°${Math.abs(longF)}'` : '---';

    return (
      <SwipeRow
        style={commonStyles.swipeRow}
        rightOpenValue={-75}
        body={
          <TouchableOpacity
            style={styles.container}
            onPress={() => {
              this.props.onItemPress({
                longD,
                longF,
                latD,
                latF,
                typeChildText,
                describe,
                typeChildCode,
                startTime,
                endTime,
                hidePlace,
                id,
                popId,
                dbId,
              });
            }}
          >
            <View style={styles.row}>
              <Label style={styles.label}>纬度:</Label>
              <Label style={styles.text}>{la}</Label>
              <Label style={styles.label}>经度:</Label>
              <Label style={styles.text}>{lo}</Label>
            </View>
            <View style={styles.row}>
              <Label style={styles.label}>事件类型:</Label>
              <Label style={styles.text}>{typeChildText}</Label>
            </View>
            <View style={styles.row}>
              <Label style={styles.label}>抛锚位置:</Label>
              <Label style={styles.text}>{hidePlace}</Label>
            </View>
            <View style={styles.row}>
              <Label style={styles.label}>事件描述:</Label>
              <Label style={styles.text}>{describe}</Label>
            </View>
            <View style={styles.row}>
              <Label style={styles.label}>开始时间:</Label>
              <Label style={styles.text}>{startTime > 0 ? defaultFormat(startTime) : ''}</Label>
            </View>
            <View style={styles.row}>
              <Label style={styles.label}>结束时间:</Label>
              <Label style={styles.text}>{endTime > 0 ? defaultFormat(endTime) : ''}</Label>
            </View>
          </TouchableOpacity>
        }
        right={
          <Button
            danger
            onPress={() => this.props.onDelete({
              longD,
              longF,
              latD,
              latF,
              typeChildText,
              describe,
              typeChildCode,
              startTime,
              endTime,
              hidePlace,
              id,
              popId,
              dbId,
            })}
          >
            <Icon active name="trash" />
          </Button>
        }
      />
    );
  }
}

VoyageEventItem.propTypes = {};

export default VoyageEventItem;
