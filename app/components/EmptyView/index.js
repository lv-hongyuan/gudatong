/* eslint-disable global-require */
import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, StyleSheet, Image } from 'react-native';

/**
 * Created by ocean on 2018/4/26
 */

const styles = StyleSheet.create({
  image: {
    marginTop: 50,
    width: 300,
    height: 300,
  },
  message: {
    marginTop: 20,
    width: '80%',
    textAlign: 'center',
    color: '#808080',
  },
});

class EmptyView extends React.PureComponent {
  render() {
    return (
      <View style={{
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
      }}
      >
        <Image
          style={styles.image}
          resizeMode="contain"
          source={require('../../assets/emptyData.png')}
        />
        <Text style={styles.message}>{this.props.message}</Text>
      </View>
    );
  }
}

EmptyView.propTypes = {
  message: PropTypes.string,
};

EmptyView.defaultProps = {
  message: '当前暂无数据',
};

export default EmptyView;
