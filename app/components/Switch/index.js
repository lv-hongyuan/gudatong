import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { View, Text, StyleSheet, TouchableHighlight, Keyboard, ViewPropTypes } from 'react-native';
import myTheme from '../../Themes';

const styles = StyleSheet.create({
  defaultStyles: {
    width: 60,
    height: 30,
    borderRadius: 3,
  },
  container: {
    overflow: 'hidden',
    borderWidth: myTheme.borderWidth,
    borderRadius: 3,
    width: '100%',
    height: '100%',
    flexDirection: 'row',
  },
  segText: {
    color: '#ffffff',
    textAlign: 'center',
    lineHeight: 28,
    flex: 1,
  },
  segItem: {
    borderWidth: myTheme.borderWidth,
    borderRadius: 3,
    backgroundColor: '#ffffff',
    flex: 1,
  },
});

const onColor = '#DC001B';
const offColor = '#969696';

class Switch extends PureComponent {
  renderView() {
    const tintColor = this.props.value ? onColor : offColor;
    return (
      <View style={[styles.container, { backgroundColor: tintColor, borderColor: tintColor }]}>
        {this.props.value ? (
          <Text style={styles.segText}>是</Text>
        ) : (
          <View style={[styles.segItem, { borderColor: tintColor }]} />
        )}
        {this.props.value ? (
          <View style={[styles.segItem, { borderColor: tintColor }]} />
        ) : (
          <Text style={styles.segText}>否</Text>
        )}
      </View>
    );
  }

  render() {
    return this.props.readOnly ? (
      <View style={[styles.defaultStyles, this.props.style]}>
        {this.renderView()}
      </View>
    ) : (
      <TouchableHighlight
        onPress={() => {
          Keyboard.dismiss();
          this.props.onPress();
        }}
        style={[styles.defaultStyles, this.props.style]}
      >
        {this.renderView()}
      </TouchableHighlight>
    );
  }
}

Switch.propTypes = {
  onPress: PropTypes.func.isRequired,
  style: ViewPropTypes.style,
  value: PropTypes.bool,
  readOnly: PropTypes.bool,
};
Switch.defaultProps = {
  style: undefined,
  value: false,
  readOnly: false,
};

export default Switch;
