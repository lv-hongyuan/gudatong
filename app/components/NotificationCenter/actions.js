import { createRoutine } from 'redux-saga-routines';
import { SET_REGISTRATION_ID } from './constants';

export const sendRegistrationIdRoutine = createRoutine(SET_REGISTRATION_ID);
