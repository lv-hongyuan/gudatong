
//  通知中心  网络监听 ** 推送监听 ** 桌面角标控制

import React from 'react';
import PropTypes from 'prop-types';
import { Platform, NetInfo, DeviceEventEmitter, AppState } from 'react-native';
import JPushModule from 'jpush-react-native';
import { NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect/es';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { bindActionCreators } from 'redux';
import { ROUTE_LOGIN, ROUTE_VOYAGE_INSTRUCTION_LIST, ROUTE_BULLETIN_CONTENT } from '../../RouteConstant';
import { makeIsLogin } from '../../containers/AppInit/selectors';
import { bindRegistrationIDPromiseCreator } from '../../containers/AppInit/actions';
import { sendRegistrationIdRoutine } from './actions';
import AppStorage from '../../utils/AppStorage';
import { READY_TO_HANDLE_LAUNCH_NOTIFICATION } from './constants';
import AlertView from '../../components/Alert';
import NavigatorService from '../../NavigatorService';

const receiveCustomMsgEvent = 'receivePushMsg';
const receiveNotificationEvent = 'receiveNotification';
const openNotificationEvent = 'openNotification';

class NotificationCenter extends React.Component {
  constructor(props) {
    super(props);
    this.showAlertId = ''
    this.state = {
      resolvedLaunchAppNotification: false,
      isConnect: false,
    };
  }

  componentDidMount() {
    // 初始化推送
    if (Platform.OS === 'android') {
      JPushModule.initPush();
      JPushModule.notifyJSDidLoad((resultCode) => {
        console.log(resultCode);
      });
    } else {
      JPushModule.setupPush();
    }

    // 添加监听
    this.addListener();

    // 监听网络
    NetInfo.isConnected.addEventListener('connectionChange', this.netChange);

    JPushModule.getRegistrationID(this.onGetRegistrationId);
  }

  componentWillReceiveProps(nextProps) {
    // 登录后
    if (nextProps.isLogin && !this.props.isLogin) {
      // 获取RegistrationId
      JPushModule.getRegistrationID(this.onGetRegistrationId);
    }
  }

  componentWillUnmount() {
    this.removeListener();
    NetInfo.removeEventListener('connectionChange', this.netChange);
  }

  // setAlias (alias) {
  //   JPushModule.setAlias(alias, map => {
  //     if (map.errorCode === 0) {
  //       console.log('set alias succeed')
  //     } else {
  //       console.log('set alias failed, errorCode: ' + map.errorCode)
  //     }
  //   })
  // }

  // 收到自定义消息
  onReceiveCustomMsgListener = (map) => {
    this.setState({
      // pushMsg: map.message,
    });
    console.log(`extras: ${map.extras}`);
  };

  // static clearBadge () {
  //   if (Platform.OS === 'ios') {
  //     JPushModule.setBadge(0, () => {
  //     })
  //   }
  // }
  //
  // static setBadge (num) {
  //   if (Platform.OS === 'ios') {
  //     JPushModule.setBadge(num, () => {
  //     })
  //   }
  // }

  // 收到通知
  onReceiveNotification = (map) => {
    console.log('onReceiveNotification: ', map);
    const self = this;
    try {
      const extra = Platform.OS === 'ios' ? map.extras : JSON.parse(map.extras);
      const content = Platform.OS === 'ios' ? map.aps.alert : map.alertContent;
      const messageId = Platform.OS === 'ios' ? map._j_msgid : map.id;
      if (AppState.currentState === 'active' && content && content.length > 0) {
        console.log(extra);
        if(extra.type == 'sail'){
          AlertView.show({
            message: content,
            showCancel: false,
            confirmAction: () => {
              self.reduceBadge(messageId);
            },
          });
        }else if(extra.url) {
          DeviceEventEmitter.emit('refreshDesktopMarkState',{data:extra.type})
          AlertView.show({
            message: content,
            showCancel: true,
            confirmTitle: '查看',
            cancelAction: () =>{AlertView.closeAll()},
            confirmAction: () => {
              AlertView.closeAll();
              self.jumpSecondActivity(extra);
              if(Platform.OS === 'android') JPushModule.clearNotificationById(messageId);
            },
          });
        } else {
          AlertView.show({
            message: content,
            showCancel: true,
            confirmAction: () => {
              self.reduceBadge(messageId);
            },
          });
        }
      }
    } catch (error) {
      console.log(error);
    }
  };

  // 点击通知
  onReceiveOpenNotification = (map) => {
    console.log('Opening notification!', map);
    console.log(`map.extra: ${map.extras}`);
    try {
      const extra = Platform.OS === 'ios' ? map.extras : JSON.parse(map.extras);
      const content = Platform.OS === 'ios' ? map.aps.alert : map.alertContent;
      const messageId = Platform.OS === 'ios' ? map._j_msgid : map.id;
      if(extra.type == 'sail'){
        AlertView.show({
          message: content,
          showCancel: false,
        });
      }else if (extra.url) {
        AlertView.closeAll();
        this.jumpSecondActivity(extra);
        if(Platform.OS === 'android') JPushModule.clearNotificationById(messageId);
      }
    } catch (error) {
      console.log(error);
    }
  };

  // 获取RegistrationId
  onGetRegistrationId = (registrationId) => {
    console.log(`Device register succeed, registrationId ${registrationId}`);
    this.props.sendRegistrationId(registrationId);
    this.bindRegistrationID(registrationId);
  };

  resolveLaunchAppNotification = () => {
    if (this.state.resolvedLaunchAppNotification) {
      return;
    }
    this.setState({ resolvedLaunchAppNotification: true });
    if (Platform.OS === 'ios') {
      JPushModule.getLaunchAppNotification((notification) => {
        if (notification === undefined) {
          // application was launch by tap app's icon
        } else if (notification.aps === undefined) {
          // application was launch by tap local notification
        } else {
          // application was launch by tap remote notification
          this.onReceiveOpenNotification(notification);
        }
      });
    }
  };
  //notice:公告 voyageNotice:航次指令 点击推送跳到详情页
  jumpSecondActivity = (map) => {
    if (!map.url) {
      return;
    }
    DeviceEventEmitter.emit('refreshMessageState',{id:map.id,isPush:1,title:map.title})
    let action;
    switch (map.type) {
      // case 'ship':
      //   action = NavigationActions.navigate({
      //     routeName: ROUTE_VOYAGE_INSTRUCTION_LIST,
      //     params: {
      //       // path: map.url,
      //     },
      //   });
      //   break;
      case 'notice':
        action = NavigationActions.navigate({
          routeName: ROUTE_BULLETIN_CONTENT,
          params: {
            id:map.id,
            isPush: 1,
            title:map.title
          },
        });
        break;
      case 'voyageNotice':
        action = NavigationActions.navigate({
          routeName: ROUTE_BULLETIN_CONTENT,
          params: {
            id:map.id,
            isPush: 1,
            title:map.title
          },
        });
        break;

      default:
        return;
    }
    setTimeout(() => {
      if (this.props.isLogin) {
        NavigatorService.dispatch(action);
      } else {
        NavigatorService.dispatch(NavigationActions.navigate({
          routeName: ROUTE_LOGIN,
          params: {
            nextAction: action,
          },
        }));
      }
    }, 100);
  };

  //清除桌面角标
  reduceBadge = (id) => {
    if (Platform.OS === 'ios') {
      JPushModule.getBadge((bage) => {
        JPushModule.setBadge(Math.max(0, bage - 1), () => {
        });
      });
    } else {
      if (id) JPushModule.clearNotificationById(id);
    }
  };

  // 网络监控
  netChange = (isConnect) => {
    if (isConnect && !this.state.isConnect) {
      JPushModule.getRegistrationID(this.onGetRegistrationId);
    }
    this.setState({ isConnect });
  };

  // 绑定RegistrationID
  bindRegistrationID(registrationId) {
    if (!this.props.isLogin) {
      return;
    }
    if (!registrationId || registrationId === '' || AppStorage.registrationId === registrationId) {
      return;
    }
    this.props.bindRegistrationID(registrationId).catch(() => {
    });
  }

  addListener() {
    // 自定义消息，不弹出的消息
    JPushModule.addReceiveCustomMsgListener(this.onReceiveCustomMsgListener);

    // 收到通知
    JPushModule.addReceiveNotificationListener(this.onReceiveNotification);

    // 点击通知
    JPushModule.addReceiveOpenNotificationListener(this.onReceiveOpenNotification);

    // app启动完成，可以跳转画面
    this.deEmitter = DeviceEventEmitter.addListener(
      READY_TO_HANDLE_LAUNCH_NOTIFICATION,
      this.resolveLaunchAppNotification,
    );
  }

  removeListener() {
    // 收到自定义消息
    JPushModule.removeReceiveCustomMsgListener(receiveCustomMsgEvent);
    // 收到通知
    JPushModule.removeReceiveNotificationListener(receiveNotificationEvent);
    // 点击通知
    JPushModule.removeReceiveOpenNotificationListener(openNotificationEvent);

    // app启动完成，可以跳转画面
    this.deEmitter.remove();
  }

  render() {
    return null;
  }
}

NotificationCenter.propTypes = {
  sendRegistrationId: PropTypes.func.isRequired,
  bindRegistrationID: PropTypes.func.isRequired,
  isLogin: PropTypes.bool,
};

NotificationCenter.defaultProps = {
  isLogin: false,
};

const mapStateToProps = createStructuredSelector({
  isLogin: makeIsLogin(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      bindRegistrationID: bindRegistrationIDPromiseCreator,
    }, dispatch),
    ...bindActionCreators({
      sendRegistrationId: sendRegistrationIdRoutine,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(NotificationCenter);
