/*
 *
 * NotificationCenter constants
 *
 */
export const SET_REGISTRATION_ID = 'app/NotificationCenter/SET_REGISTRATION_ID';
export const READY_TO_HANDLE_LAUNCH_NOTIFICATION = 'app/NotificationCenter/READY_TO_HANDLE_LAUNCH_NOTIFICATION';
