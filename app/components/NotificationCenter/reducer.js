import { sendRegistrationIdRoutine } from './actions';

const initState = {
  registrationId: null,
};

export default function (state = initState, action) {
  switch (action.type) {
    case sendRegistrationIdRoutine.TRIGGER:
      return { ...state, registrationId: action.payload };

    default:
      return state;
  }
}
