import React from 'react';
import PropTypes from 'prop-types';
import { ViewPropTypes } from 'react-native';
import SvgUri from 'react-native-svg-uri';
import svgs from '../../assets/svgs';

class Svg extends React.PureComponent {
  render() {
    const {
      icon,
      color,
      size,
      width,
      height,
      style,
    } = this.props;
    const svgXmlData = svgs[icon];

    if (!svgXmlData) {
      const errMsg = `没有"${this.props.icon}"这个icon，添加Icon并 npm run build-js`;
      throw new Error(errMsg);
    }
    return (
      <SvgUri
        width={width || size}
        height={height || size}
        svgXmlData={svgXmlData}
        fill={color}
        style={style}
      />
    );
  }
}

Svg.propTypes = {
  icon: PropTypes.string.isRequired,
  color: PropTypes.string,
  size: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  style: ViewPropTypes.style,
};
Svg.defaultProps = {
  color: undefined,
  size: undefined,
  width: undefined,
  height: undefined,
  style: undefined,
};

export default Svg;
