import React from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity } from 'react-native';
import { View, Text } from 'native-base';
import Svg from '../Svg';

class TextIcon extends React.PureComponent {
  render() {
    return (
      <TouchableOpacity
        onPress={() => {
          console.log('iconClick');
          this.props.onClick();
        }}
      >
        <View style={{
          marginStart: 5,
          marginEnd: 5,
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
        }}
        >
          <Svg icon={this.props.icon} color={this.props.color} size={this.props.size} />
          <Text style={{
            marginTop: 5,
            color: '#535353',
          }}
          >{this.props.text}
          </Text>
        </View>
      </TouchableOpacity>);
  }
}

TextIcon.propTypes = {
  text: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  color: PropTypes.string.isRequired,
  size: PropTypes.number.isRequired,
};

export default TextIcon;
