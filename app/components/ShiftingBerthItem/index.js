import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { Button, Icon, Label, SwipeRow, View } from 'native-base';
import myTheme from '../../Themes';
import commonStyles from '../../common/commonStyles';
import { defaultFormat } from '../../utils/DateFormat';

const styles = StyleSheet.create({
  row: {
    flex: 1,
    paddingLeft: 30,
    paddingTop: 5,
    paddingBottom: 5,
    paddingRight: 30,
    flexDirection: 'row',
  },
  label: {
    fontSize: 14,
    color: '#535353',
  },
  text: {
    fontSize: 14,
    color: myTheme.inputColor,
    marginLeft: 20,
    flex: 1,
  },
  container: {
    width: '100%',
    backgroundColor: 'white',
    padding: 10,
  },
});

/**
 * 移泊记录
 * Created by jianzhexu on 2018/3/29
 */
class ShiftingBerthItem extends React.PureComponent {
  static propTypes = {
    portName: PropTypes.string.isRequired, // 港口名称
    tugNum: PropTypes.number.isRequired, // 拖轮数量
    shiftTimeStart: PropTypes.number.isRequired,
    shiftTimeEnd: PropTypes.number.isRequired,
    onDelete: PropTypes.func.isRequired,
    id: PropTypes.number.isRequired,
    popId: PropTypes.number.isRequired,
    onItemPress: PropTypes.func.isRequired,
  };

  render() {
    const {
      portName, tugNum, id, popId, shiftTimeEnd, shiftTimeStart,
    } = this.props;
    return (
      <SwipeRow
        style={commonStyles.swipeRow}
        rightOpenValue={-75}
        body={
          <TouchableOpacity
            style={styles.container}
            onPress={() => {
              this.props.onItemPress({
                portName, tugNum, id, popId, shiftTimeEnd, shiftTimeStart,
              });
            }}
          >
            <View style={styles.row}>
              <Label style={styles.label}>发生港口:</Label>
              <Label style={styles.text}>{portName}</Label>
            </View>
            <View style={styles.row}>
              <Label style={styles.label}>开始移泊:</Label>
              <Label style={styles.text}>{shiftTimeStart > 0 ? defaultFormat(shiftTimeStart) : ''}</Label>
            </View>
            <View style={styles.row}>
              <Label style={styles.label}>结束移泊:</Label>
              <Label style={styles.text}>{shiftTimeEnd > 0 ? defaultFormat(shiftTimeEnd) : ''}</Label>
            </View>
            <View style={styles.row}>
              <Label style={styles.label}>使用拖轮:</Label>
              <Label style={styles.text}>{`${tugNum || 0}艘`}</Label>
            </View>
          </TouchableOpacity>
        }
        right={
          <Button danger onPress={() => this.props.onDelete(this.props.id)}>
            <Icon active name="trash" />
          </Button>
        }
      />
    );
  }
}

ShiftingBerthItem.propTypes = {};

export default ShiftingBerthItem;
