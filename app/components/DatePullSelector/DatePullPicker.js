import React from 'react';
import PropTypes from 'prop-types';
import { View, Button, Text } from 'native-base';
import { PixelRatio, StyleSheet } from 'react-native';
import { iphoneX } from '../../utils/iphoneX-helper';
import DateTimeWheel, { DATE_WHEEL_TYPE } from '../DateTimeWheel';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    width: '100%',
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: iphoneX ? 34 : 0,
  },
  titleContainer: {
    paddingLeft: 15,
    paddingRight: 15,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    borderBottomWidth: 1 / PixelRatio.getPixelSizeForLayoutSize(1),
    borderBottomColor: '#e0e0e0',
  },
  title: {
    color: '#dc001b',
  },
  pickerContainer: {
    height: 200,
    width: '100%',
  },
  buttonContainer: {
    flexDirection: 'row',
    backgroundColor: '#E0E0E0',
    left: 0,
    right: 0,
  },
  cancelButton: {
    backgroundColor: '#efefef',
    borderRadius: 0,
    flex: 1,
    justifyContent: 'center',
  },
  completeButton: {
    backgroundColor: '#DC001B',
    borderRadius: 0,
    flex: 1,
    justifyContent: 'center',
  },
});

/**
 * Created by ocean on 2018/4/26
 */

class DatePullPicker extends React.Component {
  onCancel = () => {
    if (this.props.onCancel) this.props.onCancel();
  };

  onSubmit = () => {
    if (this.props.onSubmit) this.props.onSubmit(this.picker.currentValue);
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.titleContainer}>
          <Text style={styles.title}>{this.props.title}</Text>
        </View>
        <View style={styles.pickerContainer}>
          <DateTimeWheel
            ref={(ref) => {
              this.picker = ref;
            }}
            type={this.props.type}
            value={this.props.value || this.props.defaultValue}
            minValue={this.props.minValue}
            maxValue={this.props.maxValue}
          />
        </View>
        <View style={styles.buttonContainer}>
          <Button onPress={this.onCancel} style={styles.cancelButton}>
            <Text style={{ color: '#535353' }}>取消</Text>
          </Button>
          <Button
            onPress={this.onSubmit}
            style={styles.completeButton}
          >
            <Text>确定</Text>
          </Button>
        </View>
      </View>
    );
  }
}

DatePullPicker.propTypes = {
  title: PropTypes.string,

  value: PropTypes.number,
  defaultValue: PropTypes.number,
  minValue: PropTypes.number,
  maxValue: PropTypes.number,

  onCancel: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,

  type: PropTypes.string,
};

DatePullPicker.defaultProps = {
  title: '请选择时间',
  value: undefined,
  defaultValue: undefined,
  minValue: undefined,
  maxValue: undefined,
  type: DATE_WHEEL_TYPE.DATE_TIME,
};

export default DatePullPicker;
