import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { Label, SwipeRow, View } from 'native-base';
import myTheme from '../../Themes';
import { defaultFormat } from '../../utils/DateFormat';
import commonStyles from '../../common/commonStyles';

const styles = StyleSheet.create({
  row: {
    flex: 1,
    paddingLeft: 15,
    paddingTop: 5,
    paddingBottom: 5,
    paddingRight: 30,
    flexDirection: 'row',
  },
  label: {
    fontSize: 14,
    color: '#535353',
  },
  text: {
    fontSize: 14,
    color: myTheme.inputColor,
    marginLeft: 10,
    flex: 1,
  },
  container: {
    width: '100%',
    backgroundColor: 'white',
    padding: 10,
  },
});

/**
 * Created by jianzhexu on 2018/3/29
 */
class NavigationRecordItem extends React.PureComponent {
  static propTypes = {
    data: PropTypes.object,
    date: PropTypes.number,
    onItemPress: PropTypes.func,
  };
  static defaultProps = {
    data: undefined,
    date: undefined,
    onItemPress: undefined,
  };

  render() {
    const { data, date } = this.props;

    return (
      <SwipeRow
        style={commonStyles.swipeRow}
        // rightOpenValue={-75}
        body={
          <TouchableOpacity
            style={styles.container}
            onPress={() => {
              this.props.onItemPress({
                data, date,
              });
            }}
          >
            <View style={styles.row}>
              <Label style={styles.label}>日期:</Label>
              <Label style={styles.text}>{defaultFormat(date)}</Label>
            </View>
          </TouchableOpacity>
        }
        // right={
        //     <Button danger style={{marginTop: 5}} onPress={() => this.props.onDelete(this.props.id)}>
        //         <Icon active name="trash"/>
        //     </Button>
        // }
      />
    );
  }
}

NavigationRecordItem.propTypes = {};

export default NavigationRecordItem;
