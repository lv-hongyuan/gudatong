import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { Button, Icon, Label, SwipeRow, View } from 'native-base';
import myTheme from '../../Themes';
import { reviewTitleFromState } from '../../common/Constant';
import commonStyles from '../../common/commonStyles';

const styles = StyleSheet.create({
  row: {
    flex: 1,
    paddingLeft: 30,
    paddingTop: 5,
    paddingBottom: 5,
    paddingRight: 30,
    flexDirection: 'row',
  },
  label: {
    fontSize: 14,
    color: '#535353',
  },
  text: {
    fontSize: 14,
    color: myTheme.inputColor,
    marginLeft: 20,
    flex: 1,
  },
  container: {
    width: '100%',
    backgroundColor: 'white',
    padding: 10,
  },
});

/**
 * Created by jianzhexu on 2018/4/3
 */
class PilotApplyItem extends React.PureComponent {
  static propTypes = {
    portName: PropTypes.string.isRequired, // 港口名称
    useWay: PropTypes.number.isRequired, // 使用途径 10靠泊 20离泊
    applyType: PropTypes.number.isRequired, // 1非强制 2强制
    applyReasons: PropTypes.string.isRequired, // 申请原因
    state: PropTypes.number.isRequired, // 申请结果 0未审批 10批准 20驳回
    rejectReasons: PropTypes.string.isRequired, // 驳回原因
    onDelete: PropTypes.func.isRequired,
    id: PropTypes.number.isRequired,
    popId: PropTypes.number.isRequired,
    onItemPress: PropTypes.func.isRequired,
  };

  render() {
    const {
      portName, useWay, applyType, applyReasons, state, rejectReasons, id, popId,
    } = this.props;

    return (
      <SwipeRow
        style={commonStyles.swipeRow}
        rightOpenValue={-75}
        body={
          <TouchableOpacity
            style={styles.container}
            onPress={() => {
              this.props.onItemPress({
                portName, useWay, applyType, applyReasons, state, rejectReasons, id, popId,
              });
            }}
          >
            <View style={styles.row}>
              <Label style={styles.label}>发生港口:</Label>
              <Label style={styles.text}>{portName}</Label>
            </View>
            <View style={styles.row}>
              <Label style={styles.label}>使用途径:</Label>
              <Label style={styles.text}>{useWay === 10 ? '靠泊' : '离泊'}</Label>
            </View>
            <View style={styles.row}>
              <Label style={styles.label}>是否强制:</Label>
              <Label style={styles.text}>{applyType === 1 ? '非强制' : '强制'}</Label>
            </View>
            <View style={styles.row}>
              <Label style={styles.label}>申请原因:</Label>
              <Label style={styles.text}>{applyReasons}</Label>
            </View>
            <View style={styles.row}>
              <Label style={styles.label}>审核情况:</Label>
              <Label style={styles.text}>{reviewTitleFromState(state)}</Label>
            </View>
            <View style={styles.row}>
              <Label style={styles.label}>审核描述:</Label>
              <Label style={styles.text}>{rejectReasons}</Label>
            </View>
          </TouchableOpacity>
        }
        right={
          <Button
            danger
            onPress={() => this.props.onDelete({
              portName, useWay, applyType, applyReasons, state, rejectReasons, id, popId,
            })}
          >
            <Icon active name="trash" />
          </Button>
        }
      />
    );
  }
}

export default PilotApplyItem;
