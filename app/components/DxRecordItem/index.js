import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { Label, View } from 'native-base';
import commonStyles from '../../common/commonStyles';

const styles = StyleSheet.create({
  row: {
    flex: 1,
    paddingLeft: 15,
    paddingTop: 5,
    paddingBottom: 5,
    paddingRight: 30,
    flexDirection: 'row',
  },
  container: {
    width: '100%',
    backgroundColor: '#ffffff',
    padding: 10,
  },
  badgeContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignContent: 'flex-start',
    paddingLeft: 10,
    paddingRight: 10,
  },
  badge: {
    margin: 2,
  },
});

/**
 * Created by jianzhexu on 2018/3/29
 */
class DxRecordItem extends React.PureComponent {
  static propTypes = {
    id: PropTypes.number,
    popId: PropTypes.number,
    contNumber: PropTypes.string,
    reason: PropTypes.string,
    onItemPress: PropTypes.func.isRequired,
    portName: PropTypes.string.isRequired,
  };
  static defaultProps = {
    id: null,
    popId: null,
    contNumber: null,
    reason: null,
  };

  render() {
    const {
      id, popId, contNumber, reason,
    } = this.props;
    return (
      <View style={{ backgroundColor: '#ffffff' }}>
        <TouchableOpacity
          style={styles.container}
          onPress={() => {
            this.props.onItemPress({
              id,
              popId,
              contNumber,
              reason,
            });
          }}
        >
          <View style={styles.row}>
            <Label style={commonStyles.inputLabel}>当前港口:{this.props.portName}</Label>
          </View>
          <View style={styles.row}>
            <Label style={commonStyles.inputLabel}>倒箱数量:</Label>
            <Label style={commonStyles.text}>{contNumber}</Label>
          </View>
          <View style={styles.row}>
            <Label style={commonStyles.inputLabel}>倒箱原因:</Label>
            <Label style={commonStyles.text}>{reason}</Label>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

DxRecordItem.propTypes = {};

export default DxRecordItem;
