import React from 'react';
import PropTypes from 'prop-types';
import Overlay from 'teaset/components/Overlay/Overlay';
import { Label, List, ListItem, Text, View } from 'native-base';

/**
 * Created by jianzhexu on 2018/4/12
 */

class CommonDataPickerDialog extends React.Component {
  static propTypes = {
    onClose: PropTypes.func,
    title: PropTypes.string,
    visible: PropTypes.bool,
    data: PropTypes.array,
    onItemPress: PropTypes.func,
    value: PropTypes.number,
  };

  static defaultProps = {
    onClose: undefined,
    title: undefined,
    visible: false,
    onItemPress: undefined,
    value: undefined,
    data: [],
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  closeTimePicker = () => {
    if (this.pullViewDialog) Overlay.hide(this.pullViewDialog);
    this.pullViewDialog = undefined;
  };

  render() {
    this.pullView = (
      <Overlay.PullView
        onCloseRequest={() => {
          this.props.onClose();
          this.closeTimePicker();
        }}
        side="bottom"
        modal
      >
        <View style={{
          height: 50,
          borderBottomColor: '#e4e4e4',
          borderBottomWidth: 1,
          justifyContent: 'center',
          paddingStart: 20,
        }}
        >
          <Label style={{
            color: '#ff0727',
            fontWeight: '400',
            fontSize: 18,
          }}
          >{this.props.title}
          </Label>
        </View>
        <List
          renderRow={item => (
            <ListItem
              button
              selected={item.value === this.props.value}
              onPress={() => {
                if (this.props.onItemPress) this.props.onItemPress(item);
              }}
            >
              <Text style={item.value === this.props.value ? {
                color: '#ff0727',
              } : {}}
              >{item.name}
              </Text>
            </ListItem>)}
          dataArray={this.props.data}
          style={{
            minHeight: 200,
            paddingBottom: 30,
          }}
        />
      </Overlay.PullView>);
    if (!this.pullViewDialog && this.props.visible) {
      this.pullViewDialog = Overlay.show(this.pullView);
    }
    if (this.pullViewDialog && !this.props.visible) {
      this.closeTimePicker();
    }
    return (
      <View />
    );
  }
}

export default CommonDataPickerDialog;
