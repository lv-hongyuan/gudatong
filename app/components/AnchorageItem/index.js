import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { Button, Icon, Label, SwipeRow, View } from 'native-base';
import myTheme from '../../Themes';
import commonStyles from '../../common/commonStyles';
import { defaultFormat } from '../../utils/DateFormat';

const styles = StyleSheet.create({
  row: {
    flex: 1,
    paddingLeft: 30,
    paddingTop: 5,
    paddingBottom: 5,
    paddingRight: 30,
    flexDirection: 'row',
  },
  label: {
    fontSize: 14,
    color: '#535353',
  },
  text: {
    fontSize: 14,
    color: myTheme.inputColor,
    marginLeft: 20,
    flex: 1,
  },
  container: {
    width: '100%',
    backgroundColor: 'white',
    padding: 10,
  },
});

/**
 * Created by jianzhexu on 2018/3/26
 */
class AnchorageItem extends React.PureComponent {
  static propTypes = {
    portName: PropTypes.string.isRequired,
    typeChildText: PropTypes.string.isRequired,
    describe: PropTypes.string.isRequired,
    typeChildCode: PropTypes.number.isRequired,
    startTime: PropTypes.number.isRequired, // 抛锚时间
    endTime: PropTypes.number.isRequired, // 抛锚时间
    hidePlace: PropTypes.string.isRequired, // 抛锚时间
    id: PropTypes.number.isRequired,
    popId: PropTypes.number.isRequired,
    onDelete: PropTypes.func.isRequired,
    onItemPress: PropTypes.func.isRequired,
  };

  render() {
    const {
      portName, typeChildText, describe, hidePlace,
      typeChildCode, startTime, endTime, id, popId,
    } = this.props;

    return (
      <SwipeRow
        style={commonStyles.swipeRow}
        rightOpenValue={-75}
        body={
          <TouchableOpacity
            style={styles.container}
            onPress={() => {
              this.props.onItemPress({
                portName,
                typeChildText,
                describe,
                typeChildCode,
                startTime,
                endTime,
                hidePlace,
                id,
                popId,
              });
            }}
          >
            {/*<View style={styles.row}>*/}
            {/*<Label style={styles.label}>抛锚地点:</Label>*/}
            {/*<Label style={styles.text}>{portName}</Label>*/}
            {/*</View>*/}
            <View style={styles.row}>
              <Label style={styles.label}>类&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;型:</Label>
              <Label style={styles.text}>{typeChildText}</Label>
            </View>
            <View style={styles.row}>
              <Label style={styles.label}>抛锚位置:</Label>
              <Label style={styles.text}>{hidePlace}</Label>
            </View>
            <View style={styles.row}>
              <Label style={styles.label}>备&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;注:</Label>
              <Label style={styles.text}>{describe}</Label>
            </View>
            <View style={styles.row}>
              <Label style={styles.label}>开始时间:</Label>
              <Label style={styles.text}>{startTime > 0 ? defaultFormat(startTime) : ''}</Label>
            </View>
            <View style={styles.row}>
              <Label style={styles.label}>结束时间:</Label>
              <Label style={styles.text}>{endTime > 0 ? defaultFormat(endTime) : ''}</Label>
            </View>
          </TouchableOpacity>
        }
        right={
          <Button
            danger
            onPress={() => this.props.onDelete({
              portName,
              typeChildText,
              describe,
              typeChildCode,
              startTime,
              endTime,
              hidePlace,
              id,
              popId,
            })}
          >
            <Icon active name="trash" />
          </Button>
        }
      />
    );
  }
}

export default AnchorageItem;
