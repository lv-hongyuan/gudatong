import React from 'react';
import { DeviceEventEmitter } from 'react-native';
import { connect } from 'react-redux';
import {
  createStackNavigator,
  createSwitchNavigator,
  createBottomTabNavigator,
  NavigationActions,
} from 'react-navigation';
// 让安卓实现push动画
import { StackViewStyleInterpolator } from 'react-navigation-stack';
import HeaderButtons from 'react-navigation-header-buttons';
import Auth from './containers/Auth';
import Login from './containers/Login';
import UserAuth from './containers/UserAuth';
import ShipWork from './containers/ShipWork';
import DynFormLand from './containers/DynFormLand';
import DynFormAnchor from './containers/DynFormAnchor';
import DynFormUnBerthing from './containers/DynFormUnBerthing';
import Message from './containers/Message';
import Documents from './containers/Documents';
import MoreOperation from './containers/MoreOperation';
import NavigationRecord from './containers/NavigationRecord';
import ArrivePortRecord from './containers/ArrivePortRecord';
import ArrivePortRecordList from './containers/ArrivePortRecordList';
import ArrivePortRecordDetail from './containers/ArrivePortRecordDetail';
import HistoryRecord from './containers/HistoryRecord';
import HistoryRecordDetail from './containers/HistoryRecordDetail';
import BeforehandLayout from './containers/BeforehandLayout';
import Anchorage from './containers/Anchorage';
import EditAnchoringEvent from './containers/EditAnchoringEvent';
import ServiceFee from './containers/ServiceFee';
import Colligation from './containers/Colligation';
import CreateShiftingBerth from './containers/CreateShiftingBerth';
import CreateTugboatUseRecord from './containers/CreateTugboatUseRecord';
import NextPortForecast from './containers/NextPortForecast';
import ShiftingBerth from './containers/ShiftingBerth';
import TugboatUseList from './containers/TugboatUseList';
import SpecialTypeToteBin from './containers/SpecialTypeToteBin';
import CreateSpecialTypeToteBin from './containers/CreateSpecialTypeToteBin';
import ActualBurdeningLayout from './containers/ActualBurdeningLayout';
import TugboatApplyList from './containers/TugboatApplyList';
import CreateTugboatApply from './containers/CreateTugboatApply';
import CreatePilotApply from './containers/CreatePilotApply';
import PilotApplyList from './containers/PilotApplyList';
import RefuelApplicationList from './containers/RefuelApplicationList';
import EditRefuelApplication from './containers/EditRefuelApplication';
import RepairApplicationList from './containers/RepairApplicationList';
import EditRepairApplication from './containers/EditRepairApplication';
import CertificateManagement from './containers/CertificateManagement';
import BulletinBoard from './containers/BulletinBoard';
import BulletinContent from './containers/BulletinContent';
import SaveFeeList from './containers/SaveFeeList'
import SaveFee from './containers/SaveFee'
import SaveFeeAlgorithm from './containers/SaveFeeAlgorithm'
import SaveFeeQuestion from './containers/SaveFeeQuestion'
import ImportantBulletinBoard from './containers/ImportantBulletinBoard';
import VoyageInstructionList from './containers/VoyageInstructionList';
import VoyageEventList from './containers/VoyageEventList';
import CreateVoyageEvent from './containers/CreateVoyageEvent';
import BerthPlan from './containers/BerthPlan';
import RefrigeratorList from './containers/RefrigeratorList';
import EditRefrigeratorRecord from './containers/EditRefrigeratorRecord';
import DangerousGoodsRecords from './containers/DangerousGoodsRecords';
import EditDangerousGoodsRecord from './containers/EditDangerousGoodsRecord';
import Offline from './containers/Offline';
import OfflineNavigationRecordList from './containers/OfflineNavigationRecordList';
import OfflineArrivePortRecordList from './containers/OfflineArrivePortRecordList';
import OfflineNavigationRecordDetail from './containers/OfflineNavigationRecordDetail';
import OfflineArrivePortRecordDetail from './containers/OfflineArrivePortRecordDetail';
import DxRecordList from './containers/DxRecordList';
import EditDxRecord from './containers/EditDxRecord';
import ProblemBack from './containers/ProblemBack';
import LoadingPlan from './containers/LoadingPlan';
import Help from './containers/Help';
import HelpDetail from './containers/HelpDetail';
import SpecialBox from './containers/SpecialBox';
import NavigatorService from './NavigatorService';
import {
  ROUTE_AUTH,
  ROUTE_AUTH_LOADING,
  ROUTE_LOGIN,
  ROUTE_SHIP_WORK,
  ROUTE_DYN_FORM_LAND,
  ROUTE_DYN_FORM_ANCHOR,
  ROUTE_MESSAGE,
  ROUTE_DOCUMENTS,
  ROUTE_APPLICATION,
  ROUTE_MORE_OPERATION,
  ROUTE_DYN_FORM_UN_BERTHING,
  ROUTE_NAVIGATION_RECORD,
  ROUTE_ARRIVEPORT_RECORD,
  ROUTE_ARRIVEPORT_RECORD_LIST,
  ROUTE_ARRIVEPORT_RECORD_DETAIL,
  ROUTE_BEFOREHAND_LAYOUT,
  ROUTE_ANCHORAGE,
  ROUTE_EDIT_ANCHORAGE_EVENT,
  ROUTE_SERVICE_FEE,
  ROUTE_COLLIGATION,
  ROUTE_CREATE_SHIFTING_BERTH,
  ROUTE_CREATE_TUGBOAT_USE_RECORD,
  ROUTE_NEXT_PORT_FORECAST,
  ROUTE_SHIFTING_BERTH,
  ROUTE_SPECIAL_TYPE_TOTE_BIN,
  ROUTE_TUGBOAT_USE_LIST,
  ROUTE_CREATE_SPECIAL_TYPE_TOTE_BIN,
  ROUTE_ACTUAL_BURDENING_LAYOUT,
  ROUTE_TUGBOAT_APPLY_LIST,
  ROUTE_CREATE_TUGBOAT_APPLY,
  ROUTE_PILOT_APPLY_LIST,
  ROUTE_CREATE_PILOT_APPLY,
  ROUTE_USER_AUTH,
  ROUTE_REFUEL_APPLICATION,
  ROUTE_EDIT_REFUEL_APPLICATION,
  ROUTE_REPAIR_APPLICATION,
  ROUTE_EDIT_REPAIR_APPLICATION,
  ROUTE_CERTIFICATE_MANAGEMENT,
  ROUTE_BULLETIN_BOARD,
  ROUTE_BULLETIN_CONTENT,
  ROUTE_IMPORTANT_BULLETIN_BOARD,
  ROUTE_SAVE_FEE_LIST,
  ROUTE_SAVE_FEE,
  ROUTE_SAVE_FEE_ALGORITHM,
  ROUTE_VOYAGE_INSTRUCTION_LIST,
  ROUTE_VOYAGE_EVENT,
  ROUTE_VOYAGE_EVENT_LIST,
  ROUTE_BERTH_PLAN,
  ROUTE_REFRIGERATOR_LIST,
  ROUTE_EDIT_REFRIGERATOR_RECORD,
  ROUTE_DANGEROUS_GOODS_RECORDS,
  ROUTE_EDIT_DANGEROUS_GOODS_RECORD,
  ROUTE_OFFLINE,
  ROUTE_OFFLINE_HOME,
  ROUTE_OFFLINE_NAVIGATION_RECORD_LIST,
  ROUTE_OFFLINE_ARRIVEPORT_RECORD_LIST,
  ROUTE_DX_RECORD_LIST,
  ROUTE_EDIT_DX_RECORD,
  ROUTE_PROBLEM_BACK,
  ROUTE_LOADING_PLAN,
  ROUTE_HELP,
  ROUTE_HELP_DETAIL,
  ROUTE_SPECIAL_BOX,
  ROUTE_SAVE_FEE_QUESTION,
  ROUTE_HISTORY_RECORD,
  ROUTE_HISTORY_RECORD_DETAIL,
  ROUTE_OFFLINE_ARRIVEPORT_RECORD_DETAIL,
  ROUTE_OFFLINE_NAVIGATION_RECORD_DETAIL,
} from './RouteConstant';
import Svg from './components/Svg';
import { APP_LOGOUT, APP_NEED_UPDATE } from './common/Constant';
import { localLogoutRoutine } from './containers/Documents/actions';
import { checkUpdate } from './common/checkUpdate';

const defaultStackNavigationOptions = ({ navigation }) => ({
  headerTitleAllowFontScaling:false,//禁用字体跟随系统字体大小变化
  headerBackTitle: null,
  headerStyle: {
    backgroundColor: '#DC001B',
  },
  headerTintColor: '#ffffff',
  headerTitleStyle: {
    fontSize: 18,
    textAlign: 'center',
    flex: 1,
  },
  headerLeft: (
    <HeaderButtons color="white">
      <HeaderButtons.Item
        title=""
        buttonWrapperStyle={{ padding: 10 }}
        ButtonElement={<Svg icon="icon_back" size="20" color="white" />}
        onPress={() => {
          navigation.dispatch(NavigationActions.back());
        }}
      />
    </HeaderButtons>
  ),
  headerRight: (
    <HeaderButtons />
  ),
});

const ShipWorkNavigator = createStackNavigator({
  [ROUTE_SHIP_WORK]: { screen: ShipWork },
  [ROUTE_MORE_OPERATION]: { screen: MoreOperation },
  [ROUTE_DYN_FORM_LAND]: { screen: DynFormLand },
  [ROUTE_DYN_FORM_ANCHOR]: { screen: DynFormAnchor },
  [ROUTE_DYN_FORM_UN_BERTHING]: { screen: DynFormUnBerthing },
  [ROUTE_NAVIGATION_RECORD]: { screen: NavigationRecord },
  [ROUTE_ARRIVEPORT_RECORD]:{screen: ArrivePortRecord},
  [ROUTE_ARRIVEPORT_RECORD_LIST]:{screen:ArrivePortRecordList},
  [ROUTE_ARRIVEPORT_RECORD_DETAIL]:{screen:ArrivePortRecordDetail},
  [ROUTE_HISTORY_RECORD]: {screen: HistoryRecord},
  [ROUTE_HISTORY_RECORD_DETAIL]:{screen:HistoryRecordDetail},
  [ROUTE_BEFOREHAND_LAYOUT]: { screen: BeforehandLayout },
  [ROUTE_ANCHORAGE]: { screen: Anchorage },
  [ROUTE_EDIT_ANCHORAGE_EVENT]: { screen: EditAnchoringEvent },
  [ROUTE_SERVICE_FEE]: { screen: ServiceFee },
  [ROUTE_COLLIGATION]: { screen: Colligation },
  [ROUTE_CREATE_SHIFTING_BERTH]: { screen: CreateShiftingBerth },
  [ROUTE_CREATE_TUGBOAT_USE_RECORD]: { screen: CreateTugboatUseRecord },
  [ROUTE_NEXT_PORT_FORECAST]: { screen: NextPortForecast },
  [ROUTE_SHIFTING_BERTH]: { screen: ShiftingBerth },
  [ROUTE_TUGBOAT_USE_LIST]: { screen: TugboatUseList },
  [ROUTE_SPECIAL_TYPE_TOTE_BIN]: { screen: SpecialTypeToteBin },
  [ROUTE_CREATE_SPECIAL_TYPE_TOTE_BIN]: { screen: CreateSpecialTypeToteBin },
  [ROUTE_ACTUAL_BURDENING_LAYOUT]: { screen: ActualBurdeningLayout },
  [ROUTE_TUGBOAT_APPLY_LIST]: { screen: TugboatApplyList },
  [ROUTE_CREATE_TUGBOAT_APPLY]: { screen: CreateTugboatApply },
  [ROUTE_PILOT_APPLY_LIST]: { screen: PilotApplyList },
  [ROUTE_CREATE_PILOT_APPLY]: { screen: CreatePilotApply },
  [ROUTE_VOYAGE_EVENT_LIST]: { screen: VoyageEventList },
  [ROUTE_VOYAGE_EVENT]: { screen: CreateVoyageEvent },
  [ROUTE_REFRIGERATOR_LIST]: { screen: RefrigeratorList },
  [ROUTE_EDIT_REFRIGERATOR_RECORD]: { screen: EditRefrigeratorRecord },
  [ROUTE_DANGEROUS_GOODS_RECORDS]: { screen: DangerousGoodsRecords },
  [ROUTE_EDIT_DANGEROUS_GOODS_RECORD]: { screen: EditDangerousGoodsRecord },
  [ROUTE_DX_RECORD_LIST]: { screen: DxRecordList },
  [ROUTE_EDIT_DX_RECORD]: { screen: EditDxRecord },
  [ROUTE_SPECIAL_BOX]: { screen: SpecialBox },
  [ROUTE_BULLETIN_CONTENT]: { screen: BulletinContent },
}, {
  initialRouteName: ROUTE_SHIP_WORK,
  navigationOptions: defaultStackNavigationOptions,
  transitionConfig: () => ({
    screenInterpolator: StackViewStyleInterpolator.forHorizontal,
  }),
});

const MessageNavigator = createStackNavigator({
  [ROUTE_MESSAGE]: { screen: Message },
  [ROUTE_BULLETIN_BOARD]: { screen: BulletinBoard },
  [ROUTE_BULLETIN_CONTENT]: { screen: BulletinContent },
  [ROUTE_SAVE_FEE_LIST]: {screen:SaveFeeList},
  [ROUTE_SAVE_FEE]: {screen:SaveFee},
  [ROUTE_SAVE_FEE_ALGORITHM]: {screen:SaveFeeAlgorithm},
  [ROUTE_SAVE_FEE_QUESTION]: {screen:SaveFeeQuestion},
  [ROUTE_IMPORTANT_BULLETIN_BOARD]: { screen: ImportantBulletinBoard },
  [ROUTE_VOYAGE_INSTRUCTION_LIST]: { screen: VoyageInstructionList },
  [ROUTE_REFUEL_APPLICATION]: { screen: RefuelApplicationList },
  [ROUTE_EDIT_REFUEL_APPLICATION]: { screen: EditRefuelApplication },
  [ROUTE_REPAIR_APPLICATION]: { screen: RepairApplicationList },
  [ROUTE_EDIT_REPAIR_APPLICATION]: { screen: EditRepairApplication },
  [ROUTE_BERTH_PLAN]: { screen: BerthPlan },
  [ROUTE_LOADING_PLAN]: { screen: LoadingPlan },
  [ROUTE_HISTORY_RECORD]: {screen: HistoryRecord},
  [ROUTE_ARRIVEPORT_RECORD_LIST]:{screen:ArrivePortRecordList},
  [ROUTE_ARRIVEPORT_RECORD_DETAIL]:{screen:ArrivePortRecordDetail},
  [ROUTE_HISTORY_RECORD_DETAIL]:{screen:HistoryRecordDetail},
}, {
  initialRouteName: ROUTE_MESSAGE,
  navigationOptions: defaultStackNavigationOptions,
  transitionConfig: () => ({
    screenInterpolator: StackViewStyleInterpolator.forHorizontal,
  }),
});

const DocumentsNavigator = createStackNavigator({
  [ROUTE_DOCUMENTS]: { screen: Documents },
  [ROUTE_CERTIFICATE_MANAGEMENT]: { screen: CertificateManagement },
  [ROUTE_PROBLEM_BACK]: { screen: ProblemBack },
  [ROUTE_HELP]: { screen: Help },
  [ROUTE_HELP_DETAIL]: { screen: HelpDetail },
}, {
  initialRouteName: ROUTE_DOCUMENTS,
  navigationOptions: defaultStackNavigationOptions,
  transitionConfig: () => ({
    screenInterpolator: StackViewStyleInterpolator.forHorizontal,
  }),
});

const OfflineNavigator = createStackNavigator({
  [ROUTE_OFFLINE_HOME]: { screen: Offline },
  [ROUTE_OFFLINE_NAVIGATION_RECORD_LIST]: { screen: OfflineNavigationRecordList },
  [ROUTE_OFFLINE_ARRIVEPORT_RECORD_LIST]: { screen: OfflineArrivePortRecordList },
  [ROUTE_NAVIGATION_RECORD]: { screen: NavigationRecord },
  [ROUTE_ARRIVEPORT_RECORD]:{screen: ArrivePortRecord},
  [ROUTE_VOYAGE_EVENT_LIST]: { screen: VoyageEventList },
  [ROUTE_VOYAGE_EVENT]: { screen: CreateVoyageEvent },
  [ROUTE_OFFLINE_ARRIVEPORT_RECORD_DETAIL]: {screen: OfflineArrivePortRecordDetail},
  [ROUTE_OFFLINE_NAVIGATION_RECORD_DETAIL]: {screen: OfflineNavigationRecordDetail},
}, {
  initialRouteName: ROUTE_OFFLINE_HOME,
  navigationOptions: defaultStackNavigationOptions,
  transitionConfig: () => ({
    screenInterpolator: StackViewStyleInterpolator.forHorizontal,
  }),
});

const AuthNavigator = createStackNavigator({
  [ROUTE_LOGIN]: { screen: Login },
  [ROUTE_USER_AUTH]: { screen: UserAuth },
}, {
  initialRouteName: ROUTE_LOGIN,
  mode: 'modal',
  // transitionConfig: () => ({
  //     screenInterpolator: StackViewStyleInterpolator.forHorizontal,
  // }),
});

const HomeNavigator = createBottomTabNavigator({
  ShipWork: {
    screen: ShipWorkNavigator,
    navigationOptions: ({ navigation }) => ({
      tabBarVisible: navigation.state.index === 0,
      tabBarLabel: '航行动态',
      // eslint-disable-next-line react/prop-types
      tabBarIcon: ({ tintColor }) => (
        <Svg icon="shipWork" size="24" color={tintColor} />
      ),
    }),
  },
  Message: {
    screen: MessageNavigator,
    navigationOptions: ({ navigation }) => ({
      tabBarVisible: navigation.state.index === 0,
      tabBarLabel: '信息工具',
      // eslint-disable-next-line react/prop-types
      tabBarIcon: ({ tintColor }) => (
        <Svg icon="message" size="24" color={tintColor} />
      ),
    }),
  },
  Documents: {
    screen: DocumentsNavigator,
    navigationOptions: ({ navigation }) => ({
      tabBarVisible: navigation.state.index === 0,
      tabBarLabel: '资料管理',
      // eslint-disable-next-line react/prop-types
      tabBarIcon: ({ tintColor }) => (
        <Svg icon="doc" size="24" color={tintColor} />
      ),
    }),
  },
}, {
  tabBarOptions: {
    allowFontScaling:false,
    activeTintColor: '#ff171a',
    inactiveTintColor: '#808080',
    style: {
      borderTopColor: '#c9c9c9',
    },
  },
  swipeEnabled: false,
  animationEnabled: false,
  backBehavior: 'none',
});

export const AppNavigator = createSwitchNavigator({
  [ROUTE_AUTH_LOADING]: Auth,
  [ROUTE_AUTH]: AuthNavigator,
  [ROUTE_APPLICATION]: HomeNavigator,
  [ROUTE_OFFLINE]: OfflineNavigator,
}, {
  initialRouteName: ROUTE_AUTH_LOADING,
});

class AppWithNavigationState extends React.Component {
  componentDidMount() {
    const self = this;
    this.logoutEmitter = DeviceEventEmitter.addListener(APP_LOGOUT, () => {
      self.props.dispatch(localLogoutRoutine());
      NavigatorService.navigate(ROUTE_LOGIN);
    });
    this.updateEmitter = DeviceEventEmitter.addListener(APP_NEED_UPDATE, () => {
      checkUpdate(true);
    });
  }

  componentWillUnmount() {
    this.logoutEmitter.remove();
    this.updateEmitter.remove();
  }

  render() {
    return <AppNavigator ref={ref => NavigatorService.setTopLevelNavigator(ref)} />;
  }
}

const mapStateToProps = state => ({
  nav: state.nav,
});

export default connect(mapStateToProps)(AppWithNavigationState);
