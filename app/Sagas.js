import { routinePromiseWatcherSaga } from 'redux-saga-routines';
import { networkEventsListenerSaga } from 'react-native-offline';
import { fork } from 'redux-saga/effects';
import Login from './containers/Login/sagas';
import UserAuth from './containers/UserAuth/sagas';
import ShipWork from './containers/ShipWork/sagas';
import ActualBurdeningLayout from './containers/ActualBurdeningLayout/sagas';
import Anchorage from './containers/Anchorage/sagas';
import BeforehandLayout from './containers/BeforehandLayout/sagas';
import Colligation from './containers/Colligation/sagas';
import CreateAnchorage from './containers/EditAnchoringEvent/sagas';
import CreatePilotApply from './containers/CreatePilotApply/sagas';
import CreateShiftingBerth from './containers/CreateShiftingBerth/sagas';
import CreateSpecialTypeToteBin from './containers/CreateSpecialTypeToteBin/sagas';
import CreateTugboatApply from './containers/CreateTugboatApply/sagas';
import CreateTugboatUseRecord from './containers/CreateTugboatUseRecord/sagas';
import NavigationRecord from './containers/NavigationRecord/sagas';
import ArrivePortRecord from './containers/ArrivePortRecord/sagas';
import ArrivePortRecordList from './containers/ArrivePortRecordList/sagas';
import ArrivePortRecordDetail from './containers/ArrivePortRecordDetail/sagas';
import HistoryRecord from './containers/HistoryRecord/sagas';
import HistoryRecordDetail from './containers/HistoryRecordDetail/sagas';
import NextPortForecast from './containers/NextPortForecast/sagas';
import PilotApplyList from './containers/PilotApplyList/sagas';
import ServiceFee from './containers/ServiceFee/sagas';
import ShiftingBerth from './containers/ShiftingBerth/sagas';
import SpecialTypeToteBin from './containers/SpecialTypeToteBin/sagas';
import TugboatApplyList from './containers/TugboatApplyList/sagas';
import TugboatUseList from './containers/TugboatUseList/sagas';
import Documents from './containers/Documents/sagas';
import RefuelApplicationList from './containers/RefuelApplicationList/sagas';
import EditRefuelApplication from './containers/EditRefuelApplication/sagas';
import RepairApplicationList from './containers/RepairApplicationList/sagas';
import EditRepairApplication from './containers/EditRepairApplication/sagas';
import CertificateManagement from './containers/CertificateManagement/sagas';
import BulletinBoard from './containers/BulletinBoard/sagas';
import BulletinContent from './containers/BulletinContent/sagas';
import ImportantBulletinBoard from './containers/ImportantBulletinBoard/sagas';
import SaveFeeList from './containers/SaveFeeList/sagas';
import SaveFee from './containers/SaveFee/sagas';
import SaveFeeAlgorithm from './containers/SaveFeeAlgorithm/sagas';
import SaveFeeQuestion from './containers/SaveFeeQuestion/sagas';
import VoyageInstructionList from './containers/VoyageInstructionList/sagas';
import VoyageEventList from './containers/VoyageEventList/sagas';
import CreateVoyageEvent from './containers/CreateVoyageEvent/sagas';
import BerthPlan from './containers/BerthPlan/sagas';
import RefrigeratorList from './containers/RefrigeratorList/sagas';
import EditRefrigeratorRecord from './containers/EditRefrigeratorRecord/sagas';
import DangerousGoodsRecords from './containers/DangerousGoodsRecords/sagas';
import EditDangerousGoodsRecord from './containers/EditDangerousGoodsRecord/sagas';
import OfflineNavigationRecordList from './containers/OfflineNavigationRecordList/sagas';
import OfflineArrivePortRecordList from './containers/OfflineArrivePortRecordList/sagas';
import AppInit from './containers/AppInit/sagas';
import DxRecordList from './containers/DxRecordList/sagas';
import EditDxRecord from './containers/EditDxRecord/sagas';
import ProblemBack from './containers/ProblemBack/sagas';
import LoadingPlan from './containers/LoadingPlan/sagas';
import Help from './containers/Help/sagas';
import HelpDetail from './containers/HelpDetail/sagas';
import SpecialBox from './containers/SpecialBox/sagas';
import ApiConstants from './common/ApiConstants';

// function* networkSaga() {
//   yield fork(networkEventsListenerSaga, {
//     withExtraHeadRequest: false,
//     pingServerUrl: 'http://www.baidu.com',
//     timeout: 3000,
//     checkConnectionInterval: 20000,
//   });
// }

// function* networkSaga() {
//   yield fork(networkEventsListenerSaga, {
//     withExtraHeadRequest: false,
//     pingServerUrl: 'http://www.baidu.com',
//     timeout: 3000,
//     checkConnectionInterval: 20000,
//   });
// }
function* networkSaga() {
  yield fork(networkEventsListenerSaga, {
    withExtraHeadRequest: false,
    pingServerUrl: ApiConstants.api +'shipDynShow',
    timeout: 3000,
    checkConnectionInterval: 20000,
  });
}

export default function injectSagas(store) {
  const allSagas = [
    ...AppInit,
    ...Login,
    ...UserAuth,
    ...ShipWork,
    ...NavigationRecord,
    ...ArrivePortRecord,
    ...ArrivePortRecordList,
    ...ArrivePortRecordDetail,
    ...HistoryRecord,
    ...HistoryRecordDetail,
    ...ServiceFee,
    ...TugboatApplyList,
    ...TugboatUseList,
    ...SpecialTypeToteBin,
    ...ShiftingBerth,
    ...CreatePilotApply,
    ...PilotApplyList,
    ...NextPortForecast,
    ...CreateShiftingBerth,
    ...CreateSpecialTypeToteBin,
    ...CreateTugboatApply,
    ...CreateTugboatUseRecord,
    ...ActualBurdeningLayout,
    ...Anchorage,
    ...CreateAnchorage,
    ...BeforehandLayout,
    ...Colligation,
    ...Documents,
    ...RefuelApplicationList,
    ...EditRefuelApplication,
    ...RepairApplicationList,
    ...EditRepairApplication,
    ...CertificateManagement,
    ...BulletinBoard,
    ...BulletinContent,
    ...SaveFeeList,
    ...SaveFee,
    ...SaveFeeAlgorithm,
    ...SaveFeeQuestion,
    ...ImportantBulletinBoard,
    ...VoyageInstructionList,
    ...VoyageEventList,
    ...CreateVoyageEvent,
    ...BerthPlan,
    ...RefrigeratorList,
    ...EditRefrigeratorRecord,
    ...DangerousGoodsRecords,
    ...EditDangerousGoodsRecord,
    ...OfflineNavigationRecordList,
    ...OfflineArrivePortRecordList,
    ...DxRecordList,
    ...EditDxRecord,
    ...ProblemBack,
    ...LoadingPlan,
    ...Help,
    ...HelpDetail,
    ...SpecialBox,
    routinePromiseWatcherSaga,
    networkSaga,
  ];

  allSagas.map(store.runSaga);
}
