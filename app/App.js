import React from 'react';
import { AppRegistry, AppState, StatusBar ,Text ,TextInput} from 'react-native';
import { Provider } from 'react-redux';
import { StyleProvider } from 'native-base';
import Orientation from 'react-native-orientation';
// 引入引导页组件
import SplashScreen from 'rn-splash-screen';
import ErrorHandler from './containers/ErrorHandler';
import { APP_NAME } from './common/Constant';
import store from './Store';
import theme from './Themes';
import getTheme from '../native-base-theme/components';
import AppWithNavigationState from './AppNavigator';
import { checkUpdate } from './common/checkUpdate';
import AppInit from './containers/AppInit';
import NotificationCenter from './components/NotificationCenter';
import InputAccessory from './components/InputAccessory';
import RNFS from 'react-native-fs'

// 去掉黄色提示
console.disableYellowBox = true;
console.warn('YellowBox is disabled.');
//禁用字体大小跟随系统
Text.defaultProps={...(Text.defaultProps||{}),allowFontScaling:false};
TextInput.defaultProps={...(TextInput.defaultProps||{}),allowFontScaling:false};
class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      store,
      appState: AppState.currentState,
    };
  }

  static checkAppUpdate() {
    checkUpdate();
  }


  //创建本地错误日志txt文件
  writeFile() {
    const path = RNFS.MainBundlePath + '/NetErrorMessage.txt';
    if(RNFS.exists(path)) {
      console.log('错误日志文件已存在');
      return
    }
    RNFS.writeFile(path, '错误日志', 'utf8')
      .then((success) => {
        console.log('path', path);
      })
      .catch((err) => {
        console.log(err.message);
      });
}

  componentDidMount() {
    // this locks the view to Portrait Mode
    Orientation.lockToPortrait();

    setTimeout(() => {
      SplashScreen.hide();
    }, 0);


    App.checkAppUpdate();
    // this.writeFile()
  }

  render() {
    return (
      <StyleProvider style={getTheme(theme)}>
        <Provider store={store}>
          <AppInit>
            <StatusBar backgroundColor="#DC001B" barStyle="light-content" />
            <NotificationCenter dispatch={store.dispatch} />
            <ErrorHandler style={{ flex: 1 }}>
              <AppWithNavigationState />
              <InputAccessory />
            </ErrorHandler>
          </AppInit>
        </Provider>
      </StyleProvider>
    );
  }
}

AppRegistry.registerComponent(APP_NAME, () => App);
