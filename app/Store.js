/**
 * Create the store with asynchronously loaded reducers
 */

import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import AppReducers from './Reducers';
import allSaga from './Sagas';

const sagaMiddleware = createSagaMiddleware();

function configureStore() {
  const store = createStore(
    AppReducers,
    applyMiddleware(sagaMiddleware),
  );
  // Extensions
  store.runSaga = sagaMiddleware.run;
  allSaga(store);
  return store;
}

const store = configureStore();
export default store;
