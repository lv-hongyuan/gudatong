# 船上 APP

## 安装步骤

- `yarn`
- `react-native link`
- `react-native run-ios` OR `react-native run-android`

## 打包命令

#### ios 打包

```
bundle exec fastlane ios distribution version:[version] build:[build] des:[des]
```

#### Android 打包

```
bundle exec fastlane android distribution version:[version] build:[build] des:[des]
```

- `version`：选填项，app 版本号，不填写不做变更
- `build`：选填项，app build 号，不填写默认增加 1
- `des`：选填项，上传 pgyer 平台的  更新描述信息

## 使用到的第三方库

- [`react-native 0.57.1`](https://github.com/facebook/react-native) ：0.57 版本修复了 iOS 版本的中文输入问题，在 android 上实现了 `overflow` 属性，android 上有时会出现透视问题，需要注意

### 导航

- [`react-navigation`](https://github.com/react-navigation/react-navigation)：v2 版本不推荐使用 redux 注入，所以 app 内没有使用
- [`react-navigation-header-buttons`](https://github.com/vonovak/react-navigation-header-buttons)：导航栏按钮
- [`react-navigation-stack`](https://github.com/react-navigation/react-navigation-stack)： 主要为了使用左右 push/pop 的导航动画

### 本地存储

- [`realm`](https://realm.io/docs/javascript/latest/)：数据库

### 数据流

- [`redux-saga`](https://github.com/redux-saga/redux-saga)
- [`redux-saga-routines`](https://github.com/afitiskin/redux-saga-routines)：promise 化 action 调用

###  推送

- [`jcore-react-native`](https://github.com/jpush/jcore-react-native)
- [`jpush-react-native`](https://github.com/jpush/jpush-react-native)

### UI

- [`native-base`](https://github.com/GeekyAnts/NativeBase)
- [`react-native-fast-image`](https://github.com/DylanVann/react-native-fast-image)：android 依赖 Glide 框架， 与图片选择框架有依赖冲突，在 Android 的 gradle 文件中配置解决冲突
- [`react-native-image-zoom-viewer`](https://github.com/ascoders/react-native-image-viewer)

  ### 下拉刷新

  - [`react-native-mjrefresh`](https://github.com/react-native-studio/react-native-MJRefresh)
  - [`react-native-smartrefreshlayout`](https://github.com/react-native-studio/react-native-SmartRefreshLayout)

  ### svg 图标

  - [`react-native-svg 6.5.2`](https://github.com/react-native-community/react-native-svg)：v7 版本在 Android 上有崩溃问题，所以使用 v6 版本
  - [`react-native-svg-uri`](https://github.com/vault-development/react-native-svg-uri)

  ### 图片选择

  - [`react-native-syan-image-picker`](https://github.com/syanbo/react-native-syan-image-picker)：android 依赖 Glide 框架， 与图片选择框架有依赖冲突，在 Android 的 gradle 文件中配置解决冲突

  ### 转轮选择器

  - [`react-native-wheel-picker`](https://github.com/lesliesam/react-native-wheel-picker)：用来实现日期时间选择器，Android 版本存在数据项数目为 0 时崩溃的问题，使用时注意规避

  ### 启动画面

  - [`rn-splash-screen`](https://github.com/mehcode/rn-splash-screen)

## 文件结构

```
Rn-ShipWorkApp
├─.babelrc
├─.buckconfig
├─.eslintrc.json
├─.flowconfig
├─.watchmanconfig
├─Gemfile
├─Gemfile.lock
├─README.md
├─app.json
├─index.js
├─package.json
├─yarn.lock
├─native-base-theme
├─internal
|    ├─getSvg.js
|    ├─svgs
├─fastlane
|    ├─Appfile
|    ├─Fastfile
|    ├─Pluginfile
|    ├─README.md
|    ├─developmentExportOptions.plist
|    ├─enterpriseExportOptions.plist
|    └report.xml
├─dist
├─app
|  ├─App.js
|  ├─AppNavigator.js
|  ├─NavigatorService.js
|  ├─Reducers.js
|  ├─RouteConstant.js
|  ├─Sagas.js
|  ├─Store.js
|  ├─Themes.js
|  ├─utils
|  ├─containers
|  ├─components
|  ├─common
|  ├─assets
|  |   └svgs.js
```

- `internal` 文件夹：包含项目中用到的 svg 图标文件，项目运行前，使用`npm build`命令将所有的 svg 文件打包到 assets 文件夹中的`svgs.js`文件中，方便使用
- `app` 文件夹：项目的代码文件夹
  - `App.js`
  - `AppNavigator.js`
  - `NavigatorService.js`
  - `Reducers.js`
  - `RouteConstant.js`
  - `Sagas.js`
  - `Store.js`
  - `Themes.js`
  - `utils`
  - `containers`
  - `components`
  - `common`
  - `assets`
- `dist` 文件夹：打包后的 app 安装文件（android 为 apk 文件，iOS 为 ipa 文件）在此文件夹中
- `fastlane` 文件夹：app 打包的配置文件在此文件夹中，具体配置参照[fastlane](https://docs.fastlane.tools/)。
- `Gemfile`：fastlane 使用的 ruby 依赖文件
- `Gemfile.lock`：fastlane 使用的 ruby 依赖锁文件
- `.eslintrc.json`：代码格式化工具 eslint 配置文件，使用 `airbnb` 的规则
