package com.wsights.ship;

import android.support.multidex.MultiDexApplication;

import com.facebook.react.ReactApplication;
import com.reactlibrary.RNSyanImagePickerPackage;
import com.lmy.smartrefreshlayout.SmartRefreshLayoutPackage;
import io.realm.react.RealmReactPackage;

import cn.qiuxiang.react.baidumap.BaiduMapPackage;

import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.rnfs.RNFSPackage;
import com.horcrux.svg.SvgPackage;

import fr.bamlab.rnimageresizer.ImageResizerPackage;

import com.mehcode.reactnative.splashscreen.SplashScreenPackage;
import com.tencent.bugly.crashreport.CrashReport;
import com.zyu.ReactNativeWheelPickerPackage;
import com.dylanvann.fastimage.FastImageViewPackage;
import com.github.yamill.orientation.OrientationPackage;
import com.wsights.ship.autoupdate.UpgradePackage;

import cn.jpush.reactnativejpush.JPushPackage;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends MultiDexApplication implements ReactApplication {

    // 设置为 true 将不会弹出 toast
    private boolean SHUTDOWN_TOAST = true;
    // 设置为 true 将不会打印 log
    private boolean SHUTDOWN_LOG = true;

    private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
        @Override
        public boolean getUseDeveloperSupport() {
            return BuildConfig.DEBUG;
        }

        @Override
        protected List<ReactPackage> getPackages() {
            return Arrays.<ReactPackage>asList(
                    new MainReactPackage(),
                    new RNSyanImagePickerPackage(),
                    new SmartRefreshLayoutPackage(),
                    new RealmReactPackage(),
                    new BaiduMapPackage(),
                    new RNDeviceInfo(),
                    new ImageResizerPackage(),
                    new SvgPackage(),
                    new RNFSPackage(),
                    new ReactNativeWheelPickerPackage(),
                    new FastImageViewPackage(),
                    new SplashScreenPackage(),
                    new OrientationPackage(),
                    new UpgradePackage(),
                    new JPushPackage(SHUTDOWN_TOAST, SHUTDOWN_LOG)
            );
        }

        @Override
        protected String getJSMainModuleName() {
            return "index";
        }
    };

    @Override
    public ReactNativeHost getReactNativeHost() {
        return mReactNativeHost;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        SoLoader.init(this, /* native exopackage */ false);
        // CrashReport.initCrashReport(getApplicationContext(), "4df855846b", false);
        CrashReport.initCrashReport(getApplicationContext(), "c012c198bf", false);
    }
}
